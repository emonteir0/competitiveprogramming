// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: FNDI's Staircase
// Level: 1
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2518

#include<stdio.h>
#include<math.h>

main()
{
    int a, b, c, d;
    while(scanf("%d %d %d %d", &a, &b, &c, &d) == 4)
    printf("%.4lf\n", (sqrt(b*b+c*c) * a * d) / 10000.0);
    return 0;
}
