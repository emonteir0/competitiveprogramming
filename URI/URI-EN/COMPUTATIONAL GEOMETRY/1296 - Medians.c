// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Medians
// Level: 6
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1296

#include<stdio.h>
#include<math.h>

double absol(double x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	double ma,mb,mc,a,b,c;
	while(scanf("%lf %lf %lf",&ma,&mb,&mc)==3)
	{
		a=(-12*ma*ma+24*mb*mb+24*mc*mc)/27;
		b=(24*ma*ma-12*mb*mb+24*mc*mc)/27;
		c=(24*ma*ma+24*mb*mb-12*mc*mc)/27;
		if(a>0&&b>0&&c>0)
		{
			a=sqrt(a);
			b=sqrt(b);
			c=sqrt(c);
			if(a<(b+c)&&a>absol(b-c))
				printf("%.3f\n",sqrt((a+b+c)*(a-b+c)*(a+b-c)*(-a+b+c))/4);
			else
				printf("-1.000\n");
		}
		else
			printf("-1.000\n");
	}
	return 0;
}
