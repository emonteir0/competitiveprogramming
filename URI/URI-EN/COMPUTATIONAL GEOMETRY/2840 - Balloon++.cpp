// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Balloon++
// Level: 1
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2840

#include<bits/stdc++.h>

using namespace std;

main()
{
	int R, V;
	scanf("%d %d", &R, &V);
	printf("%d\n", int(floor((V/((4.0/3.0)*3.1415*R*R*R)))));
	return 0;
}
