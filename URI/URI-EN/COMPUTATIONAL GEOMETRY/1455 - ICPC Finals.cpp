// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: ICPC Finals
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1455

#include<bits/stdc++.h>
#define SQ2 cos(M_PI_4l)
#define notInCircle(r, c, p) ((c.y-p.y)*(c.y-p.y)+(c.x-p.x)*(c.x-p.x)>r)

using namespace std;
  
struct point{
    double x,y;
};

point rotate(point a, int dir)
{
	point b;
	b.x = SQ2*(a.x-dir*a.y);
	b.y = SQ2*(a.x+dir*a.y);
	return b;
}
  
int main(){
    point p0,p1,p2,p3,C;    
    double m1,m2,r;
    int n,i,j,k,s,l=1;
    vector<point> vet;
    while((scanf("%d",&n)==1)&&(n!=0)){
        s=n;
        if(s>1)
        {
			while(n--){
				scanf("%lf %lf",&p1.x,&p1.y);
				vet.push_back(p1);
			}                
			p0=p1=vet[0];
			p2=vet[1];        
			C.x=(p1.x+p2.x)/2;
			C.y=(p1.y+p2.y)/2;
			r=(C.x-p1.x)*(C.x-p1.x)+(C.y-p1.y)*(C.y-p1.y);        
			  
			for(i=2; i<s; i++)
			{
				if(notInCircle(r,C,vet[i])){                
					p1=p0;
					p2=vet[i];                
					C.x=(p1.x+p2.x)/2;
					C.y=(p1.y+p2.y)/2;
					r=(C.x-p1.x)*(C.x-p1.x)+(C.y-p1.y)*(C.y-p1.y);                
					for(j=1; j<i; j++){                    
						if(notInCircle(r,C,vet[j])){                        
							p1=vet[i];
							p2=vet[j];
							C.x=(p1.x+p2.x)/2;
							C.y=(p1.y+p2.y)/2;
							r=(C.x-p1.x)*(C.x-p1.x)+(C.y-p1.y)*(C.y-p1.y);                                        
							for(k=0; k<j; k++){
								if(notInCircle(r,C,vet[k]))
								{                            
									p1=vet[i];
									p2=vet[j];
									p3=vet[k];
									if((p1.y == p2.y) || (p3.y == p2.y) || (p1.x == p2.x) || (p3.x == p2.x))
									{
										p1 = rotate(p1,1);
										p2 = rotate(p2,1);
										p3 = rotate(p3,1);
										m1=(p1.y-p2.y)/(p1.x-p2.x);
										m2=(p3.y-p2.y)/(p3.x-p2.x);
										C.x=(m1*m2*(p3.y-p1.y)+m1*(p2.x+p3.x)-m2*(p1.x+p2.x))/(2*(m1-m2));
										C.y=(-1/m1)*(C.x-(p1.x+p2.x)/2)+(p1.y+p2.y)/2;
										r=(C.x-p1.x)*(C.x-p1.x)+(C.y-p1.y)*(C.y-p1.y);
										C = rotate(C,-1);
									}
									else
									{
										m1=(p1.y-p2.y)/(p1.x-p2.x);
										m2=(p3.y-p2.y)/(p3.x-p2.x);
										C.x=(m1*m2*(p3.y-p1.y)+m1*(p2.x+p3.x)-m2*(p1.x+p2.x))/(2*(m1-m2));
										C.y=(-1/m1)*(C.x-(p1.x+p2.x)/2)+(p1.y+p2.y)/2;
										r=(C.x-p1.x)*(C.x-p1.x)+(C.y-p1.y)*(C.y-p1.y);
									}                             
								}
							}
						}
					}
				}
			}
		}
		else
		{
			scanf("%lf %lf",&C.x,&C.y);
			r = 0;
		}
        printf("Instancia %d\n%.2lf %.2lf %.2lf\n\n",l++,C.x,C.y,sqrt(r));
        vet.clear();
    }
	return 0;    
}

