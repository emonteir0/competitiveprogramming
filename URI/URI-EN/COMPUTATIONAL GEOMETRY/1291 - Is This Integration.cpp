// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Is This Integration?
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1291

#include<stdio.h>
#include<math.h>

main()
{
	double a;
	while(scanf("%lf",&a)!=EOF)
		printf("%.3f %.3f %.3f\n",a*a*(4*M_PI+12-12*sqrt(3))/12,a*a*(-12+6*sqrt(3)+M_PI)/3,a*a*(12-2*M_PI-3*sqrt(3))/3);
	return 0;
}
