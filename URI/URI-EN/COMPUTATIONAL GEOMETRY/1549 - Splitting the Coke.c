// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Splitting the Coke
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1549

#include<stdio.h>
#include<math.h>

int C;
double N,L,b,B,H,x;

double f(double h)
{
	double l=M_PI*b*b*(h*h*h+3*h*h*x+3*h*x*x)/x/x/3-L/N;
	return l;
}

double absol(double a)
{
	if(a<0)
		return -a;
	return a;
}

int sig(double a)
{
	if(a<0)
		return -1;
	return 1;
}

main()
{
	double h,la,lb,lc,fc,fa,fb;
	scanf("%d",&C);
	while(C--)
	{
		scanf("%lf %lf %lf %lf %lf",&N,&L,&b,&B,&H);
		if(b!=B)
		{
			if(3*L>=N*M_PI*H*(B*B+B*b+b*b))
			{
				printf("%.2lf\n",H);
			}
			else
			{
				x=b*H/(B-b);
				la=0;
				lb=H;
				while(absol(la-lb)>1e-5)
				{
					fa=f(la);
					fb=f(lb);
					lc=(la+lb)/2;
					fc=f(lc);
					if(sig(fc)*sig(fb)==-1)
					{
						la=lc;
					}
					if(sig(fc)*sig(fa)==-1)
					{
						lb=lc;
					}
				}	
			}
			printf("%.2lf\n",la);
		}
		else
		{
			if(L>N*M_PI*B*B*H)
			{
				printf("%.2lf\n",H);
			}
			else
			{
				printf("%.2lf\n",(L/N/M_PI/B/B));
			}
		}
	}
	return 0;
}
