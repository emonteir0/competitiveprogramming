// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hipercampo
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2665

#include<bits/stdc++.h>

using namespace std;

int memo[101][101];

struct st
{
	double ang1, ang2;
};

bool cmp(st st1, st st2)
{
	return st1.ang1 < st2.ang1;
}

int N;
st vet[101];

int solve(int x, int y)
{
	if(y == N+1)
		return 0;
	if(memo[x][y] != -1)
		return memo[x][y];
	memo[x][y] = solve(x, y+1);
	if(vet[x].ang1 < vet[y].ang1 && vet[x].ang2 > vet[y].ang2)
		memo[x][y] = max(memo[x][y], solve(y, y+1) + 1);
	return memo[x][y];
}


main()
{
	int i, j, Xa, Xb, x, y;
	scanf("%d %d %d", &N, &Xa, &Xb);
	vet[0].ang2 = acos(-1);
	for(i = 1; i <= N; i++)
	{
		scanf("%d %d", &x, &y);
		vet[i].ang1 = acos((x-Xa)/sqrt((x-Xa)*(x-Xa)+y*y));
		vet[i].ang2 = acos((x-Xb)/sqrt((x-Xb)*(x-Xb)+y*y));
	}
	sort(vet, vet+N+1, cmp);
	for(i = 0; i <= N; i++)
		for(j = 0; j <= N; j++)
			memo[i][j] = -1;
	printf("%d\n", solve(0, 1));
 	return 0;
}
