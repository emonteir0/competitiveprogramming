// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Toboggan of Marbles
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1223

#include<stdio.h>
#include<math.h>

double absol(double x)
{
	if(x<0)
		return -x;
	return x;
}

double sig(double x)
{
	if(x>0)
		return 1;
	return -1;
}

main()
{
	double D,m,b,dist,xant,yant,yi,xf,yf,H,L,de;
	int i,N;
	while(scanf("%d",&N)==1&&N)
	{
		scanf("%lf %lf",&L,&H);
		scanf("%lf %lf %lf",&yi,&xf,&yf);
		D=L-xf;
		for(i=1;i<N;i++)
		{
			xant=xf;
			yant=yf;
			scanf("%lf %lf %lf",&yi,&xf,&yf);
			if(i%2==0)
			{
				m=(yf-yi)/(xf-0);
				b=yi;
				dist=L-xf;
				de=((xf-xant)*(xf-xant)+(yf-yant)*(yf-yant))*sig(xant-xf);
			}
			else
			{
				m=(yf-yi)/(xf-L);
				b=yi-m*L;
				dist=xf;
				de=((xf-xant)*(xf-xant)+(yf-yant)*(yf-yant))*sig(xf-xant);	
			}
			if(dist<D)
				D=dist;
			
			if(de>D)
			{
				dist=sqrt((xant-xf)*(xant-xf)+(yant-yf)*(yant-yf));
				if(dist<D)
					D=dist;
			}
			else
			{
				dist=absol((yant-m*xant-b)/sqrt(m*m+1));
				if(dist<D)
					D=dist;
			}
		}
		printf("%.2f\n",D);
	}
	return 0;
}
