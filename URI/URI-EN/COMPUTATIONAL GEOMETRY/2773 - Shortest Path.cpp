// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shortest Path
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2773

#include<bits/stdc++.h>
#define ll long long
using namespace std;

double sqr(ll a)
{
	return a*a;
}

double dist(ll a, ll b, ll c, ll d)
{
	return sqrt(sqr(a-c) + sqr(b-d));
}

main()
{
	ll Xi, Yi, Xf, Yf, Xl, Yl, Xr, Yr, V;
	double r;
	while(scanf("%lld %lld %lld %lld %lld", &Xi, &Yi, &Xf, &Yf, &V) == 5)
	{
		scanf("%lld %lld %lld %lld", &Xl, &Yl, &Xr, &Yr);
		r = Xr-Xl + min(dist(Xi, Yi, Xl, Yr) + dist(Xr, Yr, Xf, Yf),
						dist(Xi, Yi, Xl, Yl) + dist(Xr, Yl, Xf, Yf));
		printf("%.1lf\n", r/V);
	}
	return 0;
}
