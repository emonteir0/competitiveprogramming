// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Curo Railroad
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1504

#include<stdio.h>
#include<stdlib.h>
 
typedef struct a
{
    long long int x,y;
    char q;
}tipo;
 
tipo vet[100000];
 
long long int x,y,acumx,acumy;
 
int cmp(const void* a, const void* b)
{
	tipo* struc1=(tipo*)a;
	tipo* struc2=(tipo*)b;
    long long int x1=struc1->x;
    long long int x2=struc2->x;
    long long int y1=struc1->y;
    long long int y2=struc2->y;
    char op;
    op=struc1->q-struc2->q;
    if(op!= 0) return op;  
    return y1*x2-y2*x1;
}
 
 
main()
{
    int n,i;
    char u=0,q,c;
    while(scanf("%d",&n)==1)
    {
        if(n%2==0)
        {
            acumx=0;
            acumy=0;
            c=0;
            for(i=0;i<n;i++)
            {
                scanf("%lld %lld",&x,&y);
                vet[i].x=x*n;
                vet[i].y=y*n;
                acumx+=x;
                acumy+=y;
            }
            for(i=0;i<n;i++)
            {
                vet[i].x-=acumx;
                vet[i].y-=acumy;
                x=vet[i].x;
                y=vet[i].y;
                if(x>0&&y==0)
                    q=1;
                else if(x>0&&y>0)
                    q=2;
                else if(x==0&&y>0)
                    q=3;
                else if(x<0&&y>0)
                    q=4;
                else if(x<0&&y==0)
                    q=5;
                else if(x<0&&y<0)
                    q=6;
                else if(x==0&&y<0)
                    q=7;
                else
                    q=8;
                vet[i].q=q;
            }
           	qsort(vet,n,sizeof(vet[0]),cmp);
           	x=n/2;
           	u=0;
           	for(i=0;i<n;i++)
         	{
                if(vet[i].x*vet[(x+i)%n].y<=vet[i].y*vet[(x+i)%n].x&&vet[(x+i-1)%n].y*vet[i].x>vet[i].y*vet[(x+i-1)%n].x)
                {
                    u=1;
                    break;
                }
                while((vet[i].q==vet[(i+1)%n].q)&&(vet[(i+1)%n].y*vet[i].x-vet[i].y*vet[(i+1)%n].x==0))
            		i++;
            }
            printf("%s\n",u==1?"YES":"NO");
        }
        else
        {
            for(i=0;i<n;i++) scanf("%lld %lld",&x,&y);
            printf("NO\n");
        }
    }
    return 0;
}
