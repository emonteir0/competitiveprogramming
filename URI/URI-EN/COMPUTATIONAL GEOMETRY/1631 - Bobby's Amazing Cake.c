// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bobby's Amazing Cake
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1631

#include<stdio.h>
#include<math.h>

double absol(double a)
{
	if(a<0)
		return -a;
	return a;
}

main()
{
	int vet[100][2],x,y,i,j,x1,y1,n,a,b;
	double d,d2;
	while(scanf("%d",&n)==1&&n>0)
	{
		d=500000;
		x=0;
		y=0;
		for(i=0;i<n;i++)
		{
			scanf("%d %d",&vet[i][0],&vet[i][1]);
			x+=vet[i][0];
			y+=vet[i][1];
		}
		for(i=0;i<n-1;i++)
		{
			for(j=i+1;j<n;j++)
			{
				x1=x-(n-1)*vet[i][0]-vet[j][0];
				y1=y-(n-1)*vet[i][1]-vet[j][1];
				a=vet[i][0]-vet[j][0];
				b=vet[i][1]-vet[j][1];
				d2=absol(b*x1-a*y1)/sqrt(a*a+b*b);
				if(d2<d)
				{
					d=d2;
				}
			}
		}
		printf("%.3lf\n",d);
	}
	return 0;
}
