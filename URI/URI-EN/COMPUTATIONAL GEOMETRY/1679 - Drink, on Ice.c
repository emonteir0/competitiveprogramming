// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Drink, on Ice
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1679

#include<stdio.h>

main()
{
	double a,b,c,d,Q1,Q2,Q3,Q4;
	while(scanf("%lf %lf %lf %lf",&a,&b,&c,&d)==4&&(a||b||c||d))
	{
		Q1=a*c*4.19;
		Q2=-b*d*2.09;
		Q3=a*335;
		Q4=b*335;
		if(Q1==Q2)
			printf("%.1lf g of ice and %.1lf g of water at 0.0 C\n",b,a);
		else if(Q1>=(Q2+Q4))
			printf("0.0 g of ice and %.1lf g of water at %.1lf C\n",a+b,(Q1-(Q2+Q4))/(a+b)/4.19);
		else if((Q1>Q2)&&(Q1<Q2+Q4))
			printf("%.1f g of ice and %.1lf g of water at 0.0 C\n",b-(Q1-Q2)/335,a+(Q1-Q2)/335);
		else if(Q2>=(Q1+Q3))
			printf("%.1lf g of ice and 0.0 g of water at %.1lf C\n",a+b,-(Q2-(Q1+Q3))/(a+b)/2.09);
		else if((Q2>Q1)&&(Q2<Q1+Q3))
			printf("%.1lf g of ice and %.1lf g of water at 0.0 C\n",b-(Q1-Q2)/335,a+(Q1-Q2)/335);
	}
	return 0;
}
