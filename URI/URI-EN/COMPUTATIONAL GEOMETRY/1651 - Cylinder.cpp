// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cylinder
// Level: 6
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1651

#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

double PI = 2.0 * acos(0.0);
double w, h, r, best,x,r_max;

int main()
{
	while (scanf("%lf %lf", &w, &h) == 2 && w > 0.5) {
		if (w > h)
		{
			w+=h;
			h=w-h;
			w-=h;
		}
		best = 0;
		r = w/(2*PI);
		x = PI * r*r *(h - 2*r);

		r_max = min(w/2.0, h / (2*PI + 2));
		best = max(x,PI * r_max * r_max * w);
		printf("%.3f\n", best);
	}
}

