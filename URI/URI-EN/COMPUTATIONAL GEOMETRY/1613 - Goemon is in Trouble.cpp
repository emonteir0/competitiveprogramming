// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Goemon is in Trouble
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1613

#include<bits/stdc++.h>

typedef struct
{
	int a0, b0, c0, a1, b1, c1;
} parede;

typedef struct
{
	int x, y;
} ponto;

parede vet[10001];
ponto p;

using namespace std;

int sqr(int x)
{
	return x*x;
}

bool cmp(parede a, parede b)
{
	return a.a0>b.a0;
}

int cross(ponto q, int i)
{
	return vet[i].a0*q.x+vet[i].b0*q.y+vet[i].c0;
}

int cross2(ponto q, int i)
{
	return vet[i].a1*q.x+vet[i].b1*q.y+vet[i].c1;
}

int busca(ponto q, int N)
{
	int inf = 0, sup = N-1, mid, v;
	while(inf<=sup)
	{
		mid = (inf+sup)/2;
		v = cross(q, mid);
		if(v == 0)
			return mid;
		else if(v < 0)
			sup = mid-1;
		else
			inf = mid+1;
	}
	while(cross(q, mid) < 0 && mid > 0)
		mid--;
	return mid;
}

main()
{
	int K, xe, ye, N, M, i, j, a, b, cont;
	double v0, v1;
	scanf("%d", &K);
	while(K--)
	{
		cont = 0;
		scanf("%d %d", &xe, &ye);
		scanf("%d", &N);
		for(i=0;i<N;i++)
		{
			scanf("%d %d", &a, &b);
			vet[i].a0 = ye-a;
			vet[i].b0 = -xe;
			vet[i].c0 = xe*a;
			vet[i].a1 = ye-b;
			vet[i].b1 = -xe;
			vet[i].c1 = xe*b;
		}
		sort(vet,vet+N,cmp);
		scanf("%d", &M);
		for(i=0;i<M;i++)
		{
			scanf("%d %d", &p.x, &p.y);
			j = busca(p, N);
			cont += (cross(p, j) > 0) && (cross2(p, j) < 0);
		}
		printf("%d\n", cont);
	}
	return 0;
}
