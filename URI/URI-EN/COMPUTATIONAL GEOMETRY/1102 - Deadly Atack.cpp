// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Deadly Atack
// Level: 8
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1102

#include<stdio.h>
#include<math.h>
#define pi acos(-1.0)
#define pi_2 asin(1.0)


int sig(double x)
{
	if(x>0)
		return 1;
	return -1;
}

double absol(double x)
{
	if(x<0)
		return -x;
	return x;
}

double X,Y,R,P,Q,A,T;

void solve(double a, double b, char op, double *x1, double *y1, double *x2, double *y2)
{
	if(op)
	{
		 *y1=(b*sqrt(R*R*a*a + R*R*b*b - a*a*Q*Q + 2*a*b*P*Q - b*b*P*P) + a*a*Q - a*b*P)/(a*a + b*b);
 		 *y2=-(b*sqrt(R*R*a*a + R*R*b*b - a*a*Q*Q + 2*a*b*P*Q - b*b*P*P) - a*a*Q + a*b*P)/(a*a + b*b);
 		 *x1=P+a*(*y1-Q)/b;
 		 *x2=P+a*(*y2-Q)/b;
	}
	else
	{
		  *x1=sqrt(R*R*a*a - a*a*Q*Q)/a;
 		  *x2=-sqrt(R*R*a*a - a*a*Q*Q)/a;
 		  *y1=Q;
 		  *y2=Q;
	}
}

void angulos(double *theta1,double *theta2)
{
	double a,b,x,y,l,k;
	if(P!=0)
	{
		b=(R*Q + P*sqrt(- R*R + P*P + Q*Q))/(P*P + Q*Q);
		a=(R-Q*b)/P;
		y=Q-R*b;
		x=P-R*a;
		l=atan(y/x);
		if(x>0)
			l+=pi;
		if(x==0&&y<0)
			l+=pi;
		if(l<0)
			l+=2*pi;
		b=(R*Q - P*sqrt(- R*R + P*P + Q*Q))/(P*P + Q*Q);
		a=(R-Q*b)/P;
		y=Q-R*b;
		x=P-R*a;
		k=atan(y/x);
		if(x>0)
			k+=pi;
		if(x==0&&y<0)
			k+=pi;
		if(k<0)
			k+=2*pi;
	}
	else
	{
		b=R/Q;
		a=-R*sqrt(1-b*b);
		b=Q-R*b;
		l=atan(b/a);
		if(l>0)
		k=pi-l;
		else
			k=-pi-l;
		if(l<0)
			l+=2*pi;
		if(k<0)
			k+=2*pi;
	}
	*theta1=l;
	*theta2=k;
	if(*theta2<*theta1)
	{
		*theta2+=*theta1;
		*theta1=*theta2-*theta1;
		*theta2-=*theta1;
	}
}

double difang(double a1, double a2)
{
	double a,b,c,d,ang;
	a=cos(a1);
	b=sin(a1);
	c=cos(a2);
	d=sin(a2);
	ang=acos(a*c+b*d);
	if(ang<0)
		ang+=pi;
	return ang;
}

main()
{
	double ang1,ang2,angx,a1,b1,a2,b2,x1,y1,x2,y2,x3,y3,x4,y4,d1,d2,theta,theta2,t1,t2;
	double anglimite1,anglimite2;
	char op, u1, u2;
	while(scanf("%lf %lf %lf",&X,&Y,&R)==3&&(X||Y||R))
	{
		scanf("%lf %lf %lf %lf",&P,&Q,&A,&T);
		P-=X;
		Q-=Y;
		op=0;
		ang1=(A-T/2.0)*pi/180.0;
		ang2=(A+T/2.0)*pi/180.0;
		if(ang2>2*pi)
		{
			ang2-=2*pi;
			ang1-=2*pi;
			op=1;
		}
		if(ang1<0)
		{
			op=1;
		}
		if(P!=0)
		{
			angx=atan(Q/P);
			if(P>0)
			{
				angx+=pi;
			}
		}
		else
		{
			angx=pi_2*(2*(Q<0)-1);
		}	
		if(op==0)
		{
			angx+=2*pi*(angx<0);
		}
		if(op==1)
		{
			angx-=2*pi*(angx>pi);
		}
		a1=cos(ang1);
		b1=sin(ang1);
		a2=cos(ang2);
		b2=sin(ang2);
		d1=absol(-b1*P+a1*Q)/sqrt(a1*a1+b1*b1);
		d2=absol(-b2*P+a2*Q)/sqrt(a2*a2+b2*b2);
		u1=(d1<=R);
		u2=(d2<=R);
		t1=-1;
		t2=-1;
		if(u1)
		{
			solve(a1,b1,b1!=0,&x1,&y1,&x2,&y2);
			if(b1!=0)
			{
				t1=sig(y1-Q)/sig(b1);
			}
			else
			{
				t1=sig(x1-P)/sig(a1);
			}
		}
		if(u2)
		{
			solve(a2,b2,b2!=0,&x3,&y3,&x4,&y4);
			if(b2!=0)
			{
				t2=sig(y3-Q)/sig(b2);
			}
			else
			{
				t2=sig(x3-P)/sig(a1);
			}
		}
		angulos(&anglimite1,&anglimite2);
		if(anglimite2>=anglimite1+pi)
		{
			anglimite2-=2*pi;
			anglimite2+=anglimite1;
			anglimite1=anglimite2-anglimite1;
			anglimite2-=anglimite1;
		}
		if(t1==1&&t2==-1)
		{
			theta=atan2(absol(x1*y2-x2*y1), (x1*x2+y1*y2));
			if(theta<0)
				theta+=2*pi;
			if(theta>pi)
				theta-=pi;
				
			if(difang(ang1,anglimite2)<=(difang(ang1,anglimite1)))
				printf("%.1lf\n",theta*R*R/2-absol(x1*y2-y1*x2)/2+1e-4);
			else
				printf("%.1lf\n",pi*R*R-theta*R*R/2+absol(x1*y2-y1*x2)/2+1e-4);
		}
		if(t1==-1&&t2==1)
		{
			theta=atan2(absol(x3*y4-x4*y3),(x3*x4+y3*y4));
			if(theta<0)
				theta+=2*pi;
			if(theta>pi)
				theta-=pi;
				
			if(difang(ang2,anglimite2)>=difang(ang2,anglimite1))
				printf("%.1lf\n",theta*R*R/2-absol(x3*y4-y3*x4)/2+1e-4);
			else
				printf("%.1lf\n",pi*R*R-theta*R*R/2+absol(x3*y4-y3*x4)/2+1e-4);
		}
		if(t1==1&&t2==1)
		{
			theta=atan2(absol(x1*y2-x2*y1), (x1*x2+y1*y2));
			if(theta<0)
				theta+=2*pi;
			if(theta>pi)
				theta-=pi;
			theta2=atan2(absol(x3*y4-x4*y3), (x3*x4+y3*y4));
			if(theta2<0)
				theta2+=2*pi;
			if(theta2>pi)
				theta2-=pi;
				
			if(ang1<=angx&&ang2>=angx)
				printf("%.1lf\n",pi*R*R-((R*R*(theta+theta2)-absol(x1*y2-y1*x2)-absol(x3*y4-y3*x4))/2)+1e-4);
			else if(ang1>=angx&&ang2>=angx)
				printf("%.1lf\n",((theta*R*R-absol(x1*y2-y1*x2))+(-theta2*R*R+absol(x3*y4-y3*x4)))/2+1e-4);
			else if(ang1<=angx&&ang2<=angx)
				printf("%.1lf\n",((theta2*R*R-absol(x3*y4-y3*x4))+(-theta*R*R+absol(x1*y2-y1*x2)))/2+1e-4);
		}
		if(t1==-1&&t2==-1)
		{
			if(angx>=ang1&&angx<=ang2)
				printf("%.1lf\n",pi*R*R+1e-4);
			else
				printf("0.0\n");
		}
	}
	return 0;
}

