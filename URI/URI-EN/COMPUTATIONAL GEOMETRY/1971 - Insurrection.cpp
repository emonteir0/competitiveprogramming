// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Insurrection
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1971

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define TPI 2*acos(-1)

typedef struct
{
	int x, y;
} ponto;

ponto vet[4];
double ang[4];

int AreaTri(ponto a, ponto b, ponto c)
{
	return abs(a.x*(b.y-c.y)-a.y*(b.x-c.x)+b.x*c.y-b.y*c.x);
}

bool pointInTriangle(ponto p, ponto a, ponto b, ponto c)
{
	return AreaTri(a,b,c) == AreaTri(a,b,p)+AreaTri(a,p,c)+AreaTri(p,b,c);
}

bool pointInTriangles(ponto p, int i)
{
	if(i==1)
		return pointInTriangle(p, vet[0], vet[1], vet[2]) || pointInTriangle(p, vet[0], vet[1], vet[3]);
	if(i==2)
		return pointInTriangle(p, vet[0], vet[2], vet[1]) || pointInTriangle(p, vet[0], vet[2], vet[3]);
	return pointInTriangle(p, vet[0], vet[3], vet[1]) || pointInTriangle(p, vet[0], vet[3], vet[2]);
}
 
bool pointInPolygon(ponto p)
{
	int i;
	double ma = 0, me = TPI;
	for(i=1;i<4;i++)
	{
		ang[i] = acos((vet[i].x-vet[0].x)/sqrt(pow(vet[i].x-vet[0].x,2)+pow(vet[i].y-vet[0].y, 2)));
		if(ang[i]<0)	ang[i] += TPI;
		ma = ma > ang[i] ? ma : ang[i];
		me = me < ang[i] ? me : ang[i];
	}
	for(i=1;i<4;i++)
	{
		if(ang[i] != ma && ang[i] != me)
			break;
	}
	return pointInTriangles(p,i);
}
 
bool line()
{
	int xa = vet[3].x - vet[0].x;
	int ya = vet[3].y - vet[0].y;
	int xb = vet[2].x - vet[0].x;
	int yb = vet[2].y - vet[0].y;
	int xc = vet[1].x - vet[0].x;
	int yc = vet[1].y - vet[0].y;
	return xa*yb+ya*xc+xb*yc==yb*xc+xa*yc+xb*ya;
} 
 
 int main()
 {
	 int i;
	 ponto p;
	 for(i=0;i<4;i++)
		 scanf("%d %d",&vet[i].x, &vet[i].y);
	 scanf("%d %d", &p.x, &p.y);
	 printf("%s\n",pointInPolygon(p) && !line()?"\\O/\n | \n/ \\":" O>\n<| \n/ >");
	 return 0;
 }

