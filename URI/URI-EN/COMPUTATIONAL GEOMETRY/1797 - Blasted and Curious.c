// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Blasted and Curious
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1797

#include<stdio.h>
#include<math.h>

int sqr(int a, int b)
{
	return a*a + b*b;
}

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

double max(double a, double b)
{
	return (a > b) ? a : b;
}
  
main()
{
    int AL, x1, x2, y1, y2, L1, L2;
    double d, alpha;
    int N;
    char bol = 1;
    scanf("%d %d %d %d %d", &N, &AL, &x1, &y1, &L1);
    N--;
    while(N--)
    {
        scanf("%d %d %d", &x2, &y2, &L2);
		if(bol)
		{
			d = sqrt(sqr(x2-x1,y2-y1));
			if(x2 == x1)
				d -= ((double)(L1+L2))/2;
			else if(y2 == y1)
				d -= ((double)(L1+L2))/2;
			else
			{
				alpha = atan(absol(((double)(y2-y1))/(x2-x1)));
				d -= max((L1+L2)/(2*cos(alpha)), (L1+L2)/(2*sin(alpha)));
			}
			if(d>AL)
			{
				printf("OUCH\n");
				bol = 0;
			}
			x1=x2;y1=y2;L1=L2;
		}
    }
    if(bol)
		printf("YEAH\n");
    return 0;
}

