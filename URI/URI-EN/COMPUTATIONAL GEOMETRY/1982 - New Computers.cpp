// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: New Computers
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1982

#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

typedef struct
{
	int x, y;
} ponto;

ponto pontos[3000];

bool cmp(ponto p1, ponto p2)
{
	
	return p1.y<p2.y;
}

int diff(ponto p1, ponto p2)
{
	return !(p1.x==p2.x && p1.y==p2.y);
}

main()
{
	int i,k,iat,proxi,n,vxa,vya,x,y;
	ponto pontoinicial, pontoatual;
	double modulo,cosang,cosangmax,dist;
	ponto vet[3000];
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		for(i=0;i<n;i++)
			scanf("%d %d",&pontos[i].x,&pontos[i].y);
		sort(pontos,pontos+n,cmp);
		modulo=1;
		vxa=-1;
		vya=0;
		pontoinicial=pontos[0];
		cosangmax=-1;
		for(i=1;i<n;i++)
		{
			x=pontos[i].x-pontoinicial.x;
			y=pontos[i].y-pontoinicial.y;
			cosang=(x*vxa+y*vya)/sqrt(x*x+y*y);
			if(cosang>cosangmax)
			{
				iat=i;
				cosangmax=cosang;
			}
		}
		vxa=pontos[iat].x-pontoinicial.x;
		vya=pontos[iat].y-pontoinicial.y;
		modulo=sqrt(vxa*vxa+vya*vya);
		pontoatual=pontos[iat];
		vet[0]=pontoinicial;
		vet[1]=pontoatual;
		k=2;
		while(diff(pontoatual,pontoinicial))
		{
			cosangmax=-1;
			for(i=0;i<n;i++)
			{
				if(diff(pontos[i],pontoatual)&&diff(pontos[i],vet[k-2]))
				{
					x=pontos[i].x-pontoatual.x;
					y=pontos[i].y-pontoatual.y;
					cosang=(x*vxa+y*vya)/modulo/sqrt(x*x+y*y);
					if(cosang>cosangmax)
					{
						iat=i;
						cosangmax=cosang;
					}
				}
			}
			vxa=pontos[iat].x-pontoatual.x;
			vya=pontos[iat].y-pontoatual.y;
			modulo=sqrt(vxa*vxa+vya*vya);
			pontoatual=pontos[iat];
			vet[k++]=pontoatual;
		}
		dist=0;
		for(i=1;i<k;i++)
		{
			x=vet[i].x-vet[i-1].x;
			y=vet[i].y-vet[i-1].y;
			dist+=sqrt(x*x+y*y);
		}
		printf("Tera que comprar uma fita de tamanho %.2lf.\n",dist);
	}
	return 0;
}
