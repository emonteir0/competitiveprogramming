// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Helping Uncle Cláudio
// Level: 1
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2158

#include<stdio.h>
#define ll unsigned long long int

main()
{
	int k=1;
	ll a, b, c;
	while(scanf("%lld %lld",&a,&b)==2)
	{
		c = (5*a+6*b)/2;
		printf("Molecula #%d.:.\nPossui %lld atomos e %lld ligacoes\n\n",k++,c+2-a-b,c);
	}
}

