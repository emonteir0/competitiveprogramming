// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Myachowski, Russian Football
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1204

#include<stdio.h>
#include<math.h>

double Dx,Dy,a,b;

char sig(double x)
{
	if(x<0) return -1;
	if(x>0)	return 1;
	return 0;
}

void solve(double *x,double*y)
{
	double x1,x2,x0,y0;
	x0=*x;
	y0=*y;
	x1=(a*(Dy*Dy*a*x0 + Dx*b*sqrt(Dx*Dx*b*b - Dx*Dx*y0*y0 + 2*Dx*Dy*x0*y0 + Dy*Dy*a*a - Dy*Dy*x0*x0) - Dx*Dy*a*y0))/(Dx*Dx*b*b + Dy*Dy*a*a);
 	x2=-(a*(Dx*b*sqrt(Dx*Dx*b*b - Dx*Dx*y0*y0 + 2*Dx*Dy*x0*y0 + Dy*Dy*a*a - Dy*Dy*x0*x0) - Dy*Dy*a*x0 + Dx*Dy*a*y0))/(Dx*Dx*b*b + Dy*Dy*a*a);
 	if(Dx!=0)
 	{
 		if(sig(x1-x0)==sig(Dx))
 		{
 			*y=y0+(x1-x0)*Dy/Dx;
 			*x=x1;
 		}
 		else
 		{
 			*y=y0+(x2-x0)*Dy/Dx;
 			*x=x2;
 		}
 	}
 	else
 	{
		x1=(b*sqrt(Dy*Dy*a*a - Dy*Dy*x0*x0))/(Dy*a);
		x2=-(b*sqrt(Dy*Dy*a*a - Dy*Dy*x0*x0))/(Dy*a);
	 	if(sig(x1-y0)==sig(Dy))
	 		*y=x1;
	 	else *y=x2;
 	}
 	printf("%.3lf %.3lf\n",*x,*y);
}

void reflexao(double x,double y)
{
	double c,d,e,f;
	d=-x*b*b;
	c=y*a*a;
	e=(c*Dx+d*Dy)*c/(c*c+d*d);
	f=(c*Dx+d*Dy)*d/(c*c+d*d);
	Dx=2*e-Dx;
	Dy=2*f-Dy;
}

main()
{
	double x0,y0;
	while(scanf("%lf %lf %lf %lf %lf %lf",&x0,&y0,&Dx,&Dy,&a,&b)==6)
	{
		solve(&x0,&y0);
		reflexao(x0,y0);
		solve(&x0,&y0);
	}
	return 0;
}
