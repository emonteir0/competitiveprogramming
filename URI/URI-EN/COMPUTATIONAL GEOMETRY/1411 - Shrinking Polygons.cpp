// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shrinking Polygons
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1411

#include<bits/stdc++.h>

using namespace std;

set<int> valores, valores2;

int vet[10001];

main()
{
	int i, j, k, N, x, best, cont, total, div, at;
	while(scanf("%d", &N) == 1 && N)
	{
		best = 999999999;
		valores = valores2;
		for(i = 1; i <= N; i++)
		{
			scanf("%d", &x);
			vet[i] = vet[i-1] + x;
			valores.insert(vet[i]);
		}
		total = vet[N];
		for(k = 1; k*k <= total; k++)
		{
			if(total % k == 0)
			{
				div = total/k;
				for(i = 1; vet[i] <= k; i++)
				{
					at = vet[i];
					cont = 1;
					while(1)
					{
						if(valores.find(at += k) == valores.end())
							break;
						cont++;
					}
					if(cont == div && cont >= 3)
						best = min(best, N-cont);
				}
				for(i = 1; vet[i] <= div; i++)
				{
					at = vet[i];
					cont = 1;
					while(1)
					{
						if(valores.find(at += div) == valores.end())
							break;
						cont++;
					}
					if(cont == k && cont >= 3)
						best = min(best, N-cont);
				}
			}
		}
		if(best == 999999999)
			printf("-1\n");
		else
			printf("%d\n", best);
	}
	return 0;
}
