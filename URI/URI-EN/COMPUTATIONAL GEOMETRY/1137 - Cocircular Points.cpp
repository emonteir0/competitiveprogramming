// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cocircular Points
// Level: 6
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1137

#include<bits/stdc++.h>

using namespace std;

int x[100], y[100];

int colinear(int i, int j, int k)
{
	return (x[i] *(y[j]-y[k]) - y[i] * (x[j] - x[k]) + x[j]*y[k] - y[j]*x[k]) == 0;
}

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

int comp(double a, double b)
{
	return absol(a-b) <= 1e-3;
}

int main()
{
	int i, j, k, l, N, ma, cont;
	double A, B, C, D, E, F, r, xc, yc;
	while(scanf("%d", &N) == 1 && N)
	{
		if(N <= 2)
			printf("%d\n", N);
		else
		{
			ma = 2;
			for(i = 0; i < N; i++)
				scanf("%d %d", &x[i], &y[i]);
			for(i = 0; i < N; i++)
				for(j = i+1; j < N; j++)
					for(k = j+1; k < N; k++)
					{
						if(colinear(i, j, k))
							continue;
						A = 2.0*(x[i]-x[j]);
						B = 2.0*(y[i]-y[j]);
						C = 2.0*(x[i]-x[k]);
						D = 2.0*(y[i]-y[k]);
						E = (x[i]*x[i] - x[j]*x[j] + y[i]*y[i] - y[j]*y[j]);
						F = (x[i]*x[i] - x[k]*x[k] + y[i]*y[i] - y[k]*y[k]);
						xc = (D*E-B*F)/(A*D-B*C);
						yc = (C*E-A*F)/(B*C-D*A);
						r = (x[i]-xc)*(x[i]-xc) + (y[i]-yc)*(y[i]-yc);
						cont = 0;
						for(l = 0; l < N; l++)
							cont += comp((x[l]-xc)*(x[l]-xc) + (y[l]-yc)*(y[l]-yc), r);
						ma = max(ma, cont);
					}
			printf("%d\n", ma);
		}
	}
	return 0;
}
