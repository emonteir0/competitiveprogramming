// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elevator
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1124

#include<stdio.h>

main()
{
	int a,b,c,R1,R2;
	while(scanf("%d %d %d %d",&a,&b,&R1,&R2)==4&&(a||b||R1||R2))
	{
		c=(a<b)?a:b;
		if((c>=2*R1)&&(c>=2*R2))
		{
			if(((b-R1-R2)*(b-R1-R2)+(a-R1-R2)*(a-R1-R2))>=(R1+R2)*(R1+R2))
				printf("S\n");
			else
				printf("N\n");
		}
		else
			printf("N\n");
	}
	return 0;
}
