// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tribol
// Level: 1
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1875

#include<stdio.h>

int val(char c)
{
	if(c=='R')
		return 0;
	if(c=='G')
		return 1;
	return 2;
}

main()
{
	int x,y,z,n,m,g1,g2,f;
	char c1,c2;
	scanf("%d%*c",&n);
	while(n--)
	{
		x=y=z=0;
		scanf("%d%*c",&m);
		while(m--)
		{
			scanf("%c %c%*c",&c1,&c2);
			g1=val(c1);
			g2=val(c2);
			if((g2-g1)==1||(g1-g2)==2)
				f=2;
			else 
				f=1;
			if(c1=='R')
				x+=f;
			else if(c1=='G')
				y+=f;
			else
				z+=f;
		}
		if((x==y)&&(y==z))
			printf("trempate\n");
		else if(((x==y)&&(x>z))||((y==z)&&(y>x))||((x==z)&&(x>y)))
			printf("empate\n");
		else
		{
			if((x>y)&&(x>z))
				printf("red\n");
			else if((y>z)&&(y>x))
				printf("green\n");
			else
				printf("blue\n");
		}
	}
	return 0;
}
