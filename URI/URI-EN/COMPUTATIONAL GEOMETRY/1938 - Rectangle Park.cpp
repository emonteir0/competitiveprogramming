// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rectangle Park
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1938

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x, y;
} ponto;

ponto vet[3000];

bool cmp(ponto a, ponto b)
{
	if(a.x != b.x)
		return a.x < b.x;
	else
		return a.y < b.y;
}

main()
{
	int i, j, N, y1, y0, cont = 0;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d %d", &vet[i].x, &vet[i].y);
	sort(vet, vet+N, cmp);
	for(i = 1; i < N; i++)
	{
		y1 = vet[i].y;
		stack<int> a, b;
		for(j = 0; j < i; j++)
		{
			y0 = vet[j].y;
			if(y0 > y1)
			{
				while(!a.empty())
				{
					if(a.top()>y0)
						a.pop();
					else
						break;
				}
				a.push(y0);
			}
			else
			{
				while(!b.empty())
				{
					if(b.top()<y0)
						b.pop();
					else
						break;
				}
				b.push(y0);
			}
		}
		cont += a.size() + b.size();
	}
	printf("%d\n", cont);
	return 0;
}
