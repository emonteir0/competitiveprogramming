// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Onion Layers
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1464

#include<cstdio>
#include<cmath>
#include<vector>
#include<algorithm>

using namespace std;

typedef struct
{
	int x, y;
} ponto;

vector<ponto> pontos;

bool cmp(ponto p1, ponto p2)
{
	
	return p1.y<p2.y;
}

int diff(ponto p1, ponto p2)
{
	return !(p1.x==p2.x && p1.y==p2.y);
}

main()
{
	int i,j,k,iat,proxi,n,vxa,vya,x,y,cont;
	ponto p,pontoinicial, pontoatual;
	double modulo,cosang,cosangmax,dist,distmin;
	int vet[3000];
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		for(i=0;i<n;i++)
		{
			scanf("%d %d",&p.x,&p.y);
			pontos.push_back(p);
		}
		sort(pontos.begin(),pontos.end(),cmp);
		cont=0;
		while(pontos.size()>2)
		{
			n=pontos.size();
			modulo=1;
			vxa=-1;
			vya=0;
			pontoinicial=pontos[0];
			cosangmax=-1;
			for(i=1;i<n;i++)
			{
				x=pontos[i].x-pontoinicial.x;
				y=pontos[i].y-pontoinicial.y;
				cosang=(x*vxa+y*vya)/sqrt(x*x+y*y);
				if(cosang>=cosangmax)
				{
					if(cosang!=cosangmax)
					{
						iat=i;
						distmin=x*x+y*y;
						cosangmax=cosang;
					}
					else
					{
						dist=x*x+y*y;
						if(distmin>dist)
						{
							iat=i;
							distmin=dist;
							cosangmax=cosang;
						}
					}
				}
			}
			vxa=pontos[iat].x-pontoinicial.x;
			vya=pontos[iat].y-pontoinicial.y;
			modulo=sqrt(vxa*vxa+vya*vya);
			pontoatual=pontos[iat];
			vet[0]=0;
			vet[1]=iat;
			k=2;
			while(diff(pontoatual,pontoinicial))
			{
				cosangmax=-1;
				for(i=0;i<n;i++)
				{
					if(diff(pontos[i],pontoatual))
					{
						x=pontos[i].x-pontoatual.x;
						y=pontos[i].y-pontoatual.y;
						cosang=(x*vxa+y*vya)/modulo/sqrt(x*x+y*y);
						if(cosang>=cosangmax)
						{
							if(cosang!=cosangmax)
							{
								iat=i;
								distmin=x*x+y*y;
								cosangmax=cosang;
							}
							else
							{
								dist=x*x+y*y;
								if(distmin>dist)
								{
									iat=i;
									distmin=dist;
									cosangmax=cosang;
								}
							}
						}
					}
				}
				vxa=pontos[iat].x-pontoatual.x;
				vya=pontos[iat].y-pontoatual.y;
				modulo=sqrt(vxa*vxa+vya*vya);
				pontoatual=pontos[iat];
				vet[k++]=iat;
			}
			for(i=0;i<k-1;i++)
			{
				for(j=i+1;j<k-1;j++)
				{
					if(vet[i]<vet[j])
						vet[j]--;
				}
				pontos.erase(pontos.begin()+vet[i]);
			}
			cont++;
		}
		printf("%s\n",(cont%2==0)?"Do not take this onion to the lab!":"Take this onion to the lab!");
		pontos.clear();
	}
	return 0;
}
