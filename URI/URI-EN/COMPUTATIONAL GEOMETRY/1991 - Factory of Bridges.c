// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factory of Bridges
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1991

#include<stdio.h>
#include<math.h>
#define EPS 1e-5

/*
 * 20
4 N 1 3 5 7
4 S 2 2 4 8
 */

double A;

typedef struct
{
	double Racum;
	double R;
	char dir;
} raio;


raio R1[1001], R2[1001];


double min(double a, double b)
{
	return (a < b) ? a : b;
}

double sqr(double x)
{
	return x*x;
}

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

double func(double x, int i, int j)
{
	double a = x-R1[i-1].Racum-R1[i].R;
	double b = x-R2[j-1].Racum-R2[j].R;
	if(a > R1[i].R)
		a = 2*R1[i].R - a;
	if(b > R2[j].R)
		b = 2*R1[j].R - b;
	double c = A + (R1[i].dir ? 1 : -1) * sqrt(sqr(R1[i].R)-sqr(a));
	double d = (R2[j].dir ? 1 : -1) * sqrt(sqr(R2[j].R) - sqr(b));
	return c - d;
}

double ternarySearch(double left, double right, int i, int j)
{
	double leftThird, rightThird;
	if (absol(right - left) < EPS)
		return (left + right)/2;
	leftThird = (2*left + right)/3;
	rightThird = (left + 2*right)/3;
	if (func(leftThird, i, j) > func(rightThird, i, j))
		return ternarySearch(leftThird, right, i, j); 
	return ternarySearch(left, rightThird, i, j);
}


main()
{
	char dir, op;
	int i, j, N, M;
	double dist, x, v;
	while(scanf("%lf%*c", &A) && A != -1)
	{
		dist = A;
		scanf("%d %c%*c",&N, &op);
		dir = (op == 'N');
		scanf("%lf%*c", &x);
		R1[1].Racum = 2*x;
		R1[1].R = x;
		R1[1].dir = dir;
		for(i = 2; i <= N; i++)
		{ 
			dir = !dir;
			scanf("%lf%*c", &x);
			R1[i].Racum = 2*x + R1[i-1].Racum;
			R1[i].R = x;
			R1[i].dir = dir;
		}
		scanf("%d %c%*c",&M, &op);
		dir = (op == 'N');
		scanf("%lf%*c", &x);
		R2[1].Racum = 2*x;
		R2[1].R = x;
		R2[1].dir = dir;
		for(i = 2; i <= M; i++)
		{ 
			dir = !dir;
			scanf("%lf%*c", &x);
			R2[i].Racum = R2[i-1].Racum + 2*x;
			R2[i].R = x;
			R2[i].dir = dir;
		}
		i = j = 1;
		x = 0;
		while(!((i > N) || (j > M)))
		{
			v = ternarySearch(x, x + min(R1[i].Racum - x, R2[j].Racum - x), i, j);
			dist = min(dist, func(v, i, j));
			if(R1[i].Racum < R2[j].Racum)
				x = R1[i++].Racum;
			else if(R1[i].Racum > R2[j].Racum)
				x = R2[j++].Racum;
			else
			{
				x = R1[i++].Racum;
				j++;
			}
		}
		printf("%.2lf\n", dist);
	}
	return 0;
}

