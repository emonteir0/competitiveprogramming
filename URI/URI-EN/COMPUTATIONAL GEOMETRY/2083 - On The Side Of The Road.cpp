// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: On The Side Of The Road
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2083

#include<bits/stdc++.h>
#define ll long long

using namespace std;

map<pair<ll, ll>, set<int> > Mapa;
map<pair<ll, ll>, set<int> >::iterator it;
set<int>::iterator sit;

int val[1001];

ll X[1001], Y[1001];

pair<ll, ll> ponto(int i, int j)
{
	ll a, b, md;
	if(Y[j] > Y[i])
		swap(j, i);
	a = -Y[i]*(X[i]-X[j]) + X[i]*(Y[i]-Y[j]);
	b = (Y[i]-Y[j]);
	md = __gcd(abs(a), abs(b));
	return {a/md, b/md};
}

main()
{
	int N, x, ans = 1;
	val[0] = 1;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%lld %lld", &X[i], &Y[i]);
	for(int i = 0; i < N; i++)
		for(int j = i+1; j < N; j++)
		{
			if(Y[i] != Y[j])
				Mapa[ponto(i, j)].insert(j);
		}
	for(it = Mapa.begin(); it != Mapa.end(); it++)
	{
		x = (it->second).size();
		/*printf("%lld/ %lld: ", (it->first).first, (it->first).second);
		for(sit = (it->second).begin(); sit != (it->second).end(); sit++)
			printf("%d ", *sit);
		printf("\n");*/
		if(!val[x])
		{
			ans++;
			val[x] = 1;
		}
	}
	printf("%d\n", ans);
	return 0;
}
