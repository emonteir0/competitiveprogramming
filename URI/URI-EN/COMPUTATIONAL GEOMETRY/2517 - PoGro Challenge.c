// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: PoGro Challenge
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2517

#include<stdio.h>
#include<math.h>
#define EPS 1e-5

double A, B, C, xc, yc;

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

double sqr(double x)
{
	return x*x;
}

double fx(double x1)
{
    return -sqr(x1)/A - x1/B + C;
}

double func(double x1)
{
	return sqrt(sqr(x1-xc) + sqr(fx(x1) - yc));
}

double ternarySearch(double left, double right)
{
	double leftThird, rightThird;
	if (absol(right - left) < EPS)
		return (left + right)/2;
	leftThird = (2*left + right)/3;
	rightThird = (left + 2*right)/3;
	if (func(leftThird) > func(rightThird))
		return ternarySearch(leftThird, right); 
	return ternarySearch(left, rightThird);
}


main()
{
	double last, z;
	while(scanf("%lf %lf %lf", &A, &B, &C) == 3)
	{
		scanf("%lf %lf %lf", &xc, &yc, &last);
		printf("%.2lf\n", func(ternarySearch(0, last)));
	}
	return 0;
}

