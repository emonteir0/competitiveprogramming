// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Oh no, a Black Hole!
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1783

#include<stdio.h>
 
main()
{
    double x1,y1,x12,y12,x2,y2,x22,y22,a,b,c,d,e,f,xc,yc;
    int i,n;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%lf %lf %lf %lf %lf %lf %lf %lf",&x1,&y1,&x2,&y2,&x12,&y12,&x22,&y22);
        a=2*(x12-x1);
        b=2*(y12-y1);
        c=2*(x22-x2);
        d=2*(y22-y2);
        e=x12*x12+y12*y12-x1*x1-y1*y1;
        f=x22*x22+y22*y22-x2*x2-y2*y2;
        xc=(e*d-f*b)/(a*d-b*c)+1e-12;
        yc=(e*c-f*a)/(b*c-d*a)+1e-12;
        printf("Caso #%d: %.2lf %.2lf\n",i,xc,yc);
    }
    return 0;
}
