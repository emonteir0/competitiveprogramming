// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lasers
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2104

#include<cstdio>
#include<cmath>
#include<vector>
#include<algorithm>
#include<set>

using namespace std;

struct ponto
{
	double x, y;
	bool operator<(const ponto& rhs) const
    {
        if (x != rhs.x)
			return x < rhs.x;
		return y < rhs.y;
    }
};

vector<ponto> pontos;
set<ponto> conjunto, conjunto2;

bool cmp(ponto p1, ponto p2)
{
	
	return p1.y<p2.y;
}

int diff(ponto p1, ponto p2)
{
	return !(p1.x==p2.x && p1.y==p2.y);
}

double Area(ponto A, ponto B, ponto C)
{
	double a = A.x * (B.y-C.y) - A.y * (B.x - C.x) + (B.x*C.y - C.x*B.y);
	return (a > 0)? a/2 : -a/2;
}

main()
{
	int i,j,k,iat,n,cont;
	ponto p,pontoinicial, pontoatual;
	double modulo,cosang,cosangmax,dist,distmin;
	double X0, Y0, Z0, X, Y, Z, area, vxa, vya,x,y;
	int vet[3000];
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		scanf("%lf %lf %lf", &X0, &Y0, &Z0);
		for(i=0;i<n;i++)
		{
			scanf("%lf %lf %lf",&X, &Y, &Z);
			p.x = X - (Z*(X-X0))/(Z-Z0);
			p.y = Y - (Z*(Y-Y0))/(Z-Z0);
			conjunto.insert(p);
		}
		for(set<ponto>::iterator it = conjunto.begin(); it != conjunto.end(); ++it)
			pontos.push_back(*it);
		sort(pontos.begin(),pontos.end(),cmp);
		cont=0;
		n=pontos.size();
		modulo=1;
		vxa=-1;
		vya=0;
		pontoinicial=pontos[0];
		cosangmax=-1;
		for(i=1;i<n;i++)
		{
			x=pontos[i].x-pontoinicial.x;
			y=pontos[i].y-pontoinicial.y;
			cosang=(x*vxa+y*vya)/sqrt(x*x+y*y);
			if(cosang>=cosangmax)
			{
				if(cosang!=cosangmax)
				{
					iat=i;
					distmin=x*x+y*y;
					cosangmax=cosang;
				}
				else
				{
					dist=x*x+y*y;
					if(distmin>dist)
					{
						iat=i;
						distmin=dist;
						cosangmax=cosang;
					}
				}
			}
		}
		vxa=pontos[iat].x-pontoinicial.x;
		vya=pontos[iat].y-pontoinicial.y;
		modulo=sqrt(vxa*vxa+vya*vya);
		pontoatual=pontos[iat];
		vet[0]=0;
		vet[1]=iat;
		k=2;
		while(diff(pontoatual,pontoinicial))
		{
			cosangmax=-1;
			for(i=0;i<n;i++)
			{
				if(diff(pontos[i],pontoatual))
				{
					x=pontos[i].x-pontoatual.x;
					y=pontos[i].y-pontoatual.y;
					cosang=(x*vxa+y*vya)/modulo/sqrt(x*x+y*y);
					if(cosang>=cosangmax)
					{
						if(cosang!=cosangmax)
						{
							iat=i;
							distmin=x*x+y*y;
							cosangmax=cosang;
						}
						else
						{
							dist=x*x+y*y;
							if(distmin>dist)
							{
								iat=i;
								distmin=dist;
								cosangmax=cosang;
							}
						}
					}
				}
			}
			vxa=pontos[iat].x-pontoatual.x;
			vya=pontos[iat].y-pontoatual.y;
			modulo=sqrt(vxa*vxa+vya*vya);
			pontoatual=pontos[iat];
			vet[k++]=iat;
		}
		p.x = 0;
		p.y = 0;
		for(i = 0; i < k-1; i++)
		{
			p.x += pontos[vet[i]].x;
			p.y += pontos[vet[i]].y;
		}
		p.x /= (k-1);
		p.y /= (k-1);
		area = 0;
		j = k-2;
		for(i = 0; i < k-1; i++)
		{
			area += Area(p, pontos[vet[i]], pontos[vet[j]]);
			j = i;
		}
		printf("%.2lf\n", area);
		conjunto = conjunto2;
		pontos.clear();
	}
	return 0;
}
