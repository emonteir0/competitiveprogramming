// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangle Energy
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1560

#include<stdio.h>

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

typedef struct
{
	int x,y;
}ponto;

int det(ponto a, ponto b, ponto c)
{
	return abs(a.x*b.y-a.x*c.y-a.y*b.x+a.y*c.x+b.x*c.y-b.y*c.x);
}

ponto pretos[100],brancos[100];

main()
{
	int N,M,i,j,k,l,cont,triang;
	ponto a,b,c,d;
	while(scanf("%d %d",&N,&M)==2)
	{
		for(i=0;i<N;i++)
			scanf("%d %d",&pretos[i].x,&pretos[i].y);
		for(i=0;i<M;i++)
			scanf("%d %d",&brancos[i].x,&brancos[i].y);
		cont=0;
		for(i=0;i<N;i++)
		{
			a=pretos[i];
			for(j=i+1;j<N;j++)
			{
				b=pretos[j];
				for(k=j+1;k<N;k++)
				{
					triang=0;
					c=pretos[k];
					for(l=0;l<M;l++)
					{
						d=brancos[l];
						if(det(a,b,c)==det(a,b,d)+det(a,d,c)+det(d,b,c))
							triang++;
					}
					cont+=triang*triang;
				}
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
