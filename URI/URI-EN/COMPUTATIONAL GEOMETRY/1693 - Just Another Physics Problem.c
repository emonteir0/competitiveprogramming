// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Just Another Physics Problem
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1693

#include<stdio.h>
#include<math.h>
#define ll unsigned long long int 
 
main()
{
    int R,H,V;
    double G;
    while(scanf("%d %d %d %lf",&R,&H,&V,&G)==4)
        printf("%s\n",(G*R*R<=(sqrt(R*R+H*H)+H)*V*V?"Y":"N"));
    return 0;
}

