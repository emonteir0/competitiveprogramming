// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shrinking Polygons Kit
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2245

#include<bits/stdc++.h>
#define ll long long
#define INF 1000000000000000000LL

using namespace std;

struct pt
{
	ll x, y;
	pt()
	{
	}
	pt(ll x, ll y)
	{
		this->x = x;
		this->y = y;
	}
};

int N;
pt vet[100001];
pt pts[100001][2];
pt A, B;
ll dp[100001][2][2][2][2];
ll dp2[100001][2][2][2][2];

ll cross(pt O, pt A, pt B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

ll solve(int x, int a, int b, int c, int d)
{
	ll r = INF;
	if(x == N)
		return (cross(pts[x-2][b], pts[x-1][a], pts[0][c]) >= 0) && (cross(pts[x-1][a], pts[0][c], pts[1][d]) >= 0)? 0 : INF;
	if(dp[x][a][b][c][d] != -1)
		return dp[x][a][b][c][d];
	for(int i = 0; i < 2; i++)
	{
		if(cross(pts[x-2][b], pts[x-1][a], pts[x][i]) >= 0)
			r = min(r, cross(pts[0][c], pts[x-1][a], pts[x][i]) + solve(x+1, i, a, c, d));
	}
	return dp[x][a][b][c][d] = r;
}

ll solve2(int x, int a, int b, int c, int d)
{
	ll area, r = INF;
	if(x == N)
		return (cross(pts[x-2][b], pts[0][c], pts[x-1][a]) >= 0) && (cross(pts[x-1][a], pts[1][d], pts[0][c]) >= 0) ? 0 : INF;
	if(dp2[x][a][b][c][d] != -1)
		return dp2[x][a][b][c][d];
	for(int i = 0; i < 2; i++)
	{
		if(cross(pts[x-2][b], pts[x][i], pts[x-1][a]) >= 0)
			r = min(r, cross(pts[0][c], pts[x][i], pts[x-1][a]) + solve2(x+1, i, a, c, d));
	}
	return dp2[x][a][b][c][d] = r;
}

main()
{
	ll me = INF, area;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%lld %lld", &vet[i].x, &vet[i].y);
	scanf("%lld %lld %lld %lld", &A.x, &A.y, &B.x, &B.y);
	for(int i = 0; i < N; i++)
	{
		pts[i][0] = pt((vet[i].x+A.x), (vet[i].y+A.y));
		pts[i][1] = pt((vet[i].x+B.x), (vet[i].y+B.y));
		for(int j = 0; j < 2; j++)
			for(int k = 0; k < 2; k++)
				for(int l = 0; l < 2; l++)
					for(int m = 0; m < 2; m++)
					{
						dp[i][j][k][l][m] = -1;
						dp2[i][j][k][l][m] = -1;
					}
	}
	for(int i = 0; i < 2; i++)
		for(int j = 0; j < 2; j++)
		{
			area = solve(2, j, i, i, j);
			me = min(me, area ? area : INF);
			area = solve2(2, j, i, i, j);
			me = min(me, area ? area : INF);
		}
	printf("%lld.%03lld\n", me/8, 125 * (me % 8));	
	return 0;
}
