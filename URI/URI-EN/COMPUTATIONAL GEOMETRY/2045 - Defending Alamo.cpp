// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Defending Alamo
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2045

#include<bits/stdc++.h>

using namespace std;

int N;
int X[1000], Y[1000];

int ccw(int x1, int y1, int x2, int y2)
{
	return x1*y2 - x2*y1;
}

int pointInPolygon(int x, int y) 
{
	int windingNumber = 0;
	for(int i = 0; i < N; i++) 
	{
		if (x == X[i] && y == Y[i]) 
			return 0;
		int j = (i+1)%N;
		if(Y[i] == y && Y[j] == y) 
		{
			if(min(X[i], X[j]) <= x && x <= max(X[i], X[j])) 
				return 0;
		} 
		else 
		{
			bool below = Y[i] < y;
			if (below != (Y[j] < y)) 
			{
				int orientation = ccw(X[i]-x, Y[i]-y, X[j]-x, Y[j]-y);
				if (orientation == 0) 
					return 0;
				if (below == (orientation > 0)) 
					windingNumber += below ? 1 : -1;
			}
		}
	}
	return windingNumber == 0 ? 1 : -1;
}

main()
{
	int M, x, y, Bx, By, t = 1, ord;
	while(scanf("%d", &N) == 1 && N)
	{
		for(int i = 0; i < N; i++)
			scanf("%d %d", &X[i], &Y[i]);
		scanf("%d %d", &Bx, &By);
		ord = pointInPolygon(Bx, By);
		scanf("%d", &M);
		printf("Instancia %d\n", t++);
		for(int i = 1; i <= M; i++)
		{
			scanf("%d %d", &x, &y);
			int a = pointInPolygon(x, y);
			printf("soldado %d %s\n", i, a == ord || a == 0 ? "defender" : "espanhol");
		}
		printf("\n");
	}
	return 0;
}
