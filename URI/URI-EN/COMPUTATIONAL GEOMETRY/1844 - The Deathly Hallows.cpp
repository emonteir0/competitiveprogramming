// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Deathly Hallows
// Level: 6
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1844

#include <bits/stdc++.h>
#define MAXN 500502
#define MAX 999999999
 
using namespace std;
 
struct edge 
{
	int u, v;
	double w;
};

int N, M;
double total;
double p[MAXN], l[MAXN];
edge e[MAXN];

bool cmp(edge u, edge v)
{
	return u.w < v.w;
}

int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v;
	double w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}


struct ponto
{
	double x, y;
};

struct seg
{
	ponto p0, p1;
	double vx, vy, mod;
	double a, b, c;
};

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

double sqr(double x)
{
	return x*x;
}

double dot(ponto v1, ponto v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}

bool inRange(ponto p, seg v)
{
	ponto e1, e2;
	double recArea, val;
	e1.x = v.vx;
	e1.y = v.vy;
	e2.x = p.x - v.p0.x;
	e2.y = p.y - v.p0.y;
	recArea = dot(e1, e1);
	val = dot(e1, e2);
	return (val > 0 && val < recArea);
}

double distpr(ponto p, seg v)
{
	return absol(p.x*v.a + p.y*v.b + v.c)/v.mod;
}

double distrr(seg v1, seg v2)
{
	double me = MAX;
	if(inRange(v1.p0, v2))
		me = min(me, distpr(v1.p0, v2));
	if(inRange(v1.p1, v2))
		me = min(me, distpr(v1.p1, v2));
	if(inRange(v2.p0, v1))
		me = min(me, distpr(v2.p0, v1));
	if(inRange(v2.p1, v1))
		me = min(me, distpr(v2.p1, v1));
	return me;
}

double distpp(ponto a, ponto b)
{
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y));
}

bool ccw(ponto A, ponto B, ponto C)
{
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x);
}

bool intersect(seg v1, seg v2)
{
	return ccw(v1.p0, v2.p0, v2.p1) != ccw(v1.p1, v2.p0, v2.p1) && ccw(v1.p0, v1.p1, v2.p0) != ccw(v1.p0, v1.p1, v2.p1);
}

seg vet[1001];

main()
{
	int i, j;
	double me;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		p[i] = i;
		l[i] = 1;
		scanf("%lf %lf %lf %lf", &vet[i].p0.x, &vet[i].p0.y, &vet[i].p1.x, &vet[i].p1.y);
		vet[i].vx = vet[i].p1.x - vet[i].p0.x;
		vet[i].vy = vet[i].p1.y - vet[i].p0.y;
		vet[i].mod = sqrt(vet[i].vx*vet[i].vx + vet[i].vy*vet[i].vy);
		vet[i].a = -vet[i].vy;
		vet[i].b = vet[i].vx;
		vet[i].c = -vet[i].a * vet[i].p1.x - vet[i].b * vet[i].p1.y;
		for(j = 0; j < i; j++)
		{
			if(intersect(vet[i], vet[j]))
				me = 0;
			else
			{
				me = distrr(vet[i], vet[j]);
				me = min(me, distpp(vet[i].p0, vet[j].p0));
				me = min(me, distpp(vet[i].p1, vet[j].p0));
				me = min(me, distpp(vet[i].p0, vet[j].p1));
				me = min(me, distpp(vet[i].p1, vet[j].p1));
			}
			e[M].u = i;
			e[M].v = j;
			e[M++].w = me;
		}
	}
	sort(e, e+M, cmp);
	kruskal();
	printf("%d\n", (int)ceil(total));
	return 0;
}
