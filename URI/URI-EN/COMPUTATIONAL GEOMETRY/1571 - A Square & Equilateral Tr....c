// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: A Square & Equilateral Tr...
// Level: 7
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1571

#include<stdio.h>
#include<math.h>

main()
{
	long double x,c1=(sqrt(6)-sqrt(2)),c2=sqrt(2)/sqrt(3),c3=2*sqrt(2)/(sqrt(6)+sqrt(3)),c4=1/(1+1/sqrt(3)),c5=(2+sqrt(3))/(5+sqrt(3)),c6=(3+sqrt(3))/9,c8=(1+3*sqrt(3))/13;
	while(scanf("%Lf",&x)==1)
		printf("%.10Lf %.10Lf %.10Lf %.10Lf %.10Lf %.10Lf %.10Lf\n",x*c1,x*c2,x*c3,x*c4,x*c5,x*c6,x*c8);
	return 0;
}

