// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Treasure
// Level: 3
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2561

#include<bits/stdc++.h>

using namespace std;

int xmin, xmax, ymin, ymax;
int mat[1001][1001];

main()
{
	int a, b, c, d, e, f, g, h, i, j, x0, y0, x1, y1, bol;
	while(scanf("%d %d %d %d %d %d %d %d", &a, &b, &c, &d, &e, &f, &g, &h) == 8)
	{
		xmin = min(a, e);
		ymin = min(b, f);
		xmax = max(c, g);
		ymax = max(d, h);
		bol = 1;
		for(i = xmin; i <= xmax; i++)
			for(j = ymin; j <= ymax; j++)
				mat[i][j] = 0;
		for(i = a; i <= c; i++)
			for(j = b; j <= d; j++)
				mat[i][j] += 1;
		for(i = e; i <= g; i++)
			for(j = f; j <= h; j++)
				mat[i][j] += 1;
		for(i = xmin; i <= xmax; i++)
			for(j = ymin; j <= ymax; j++)
			{
				if(mat[i][j] == 2)
				{
					x1 = i;
					y1 = j;
					if(bol)
					{
						x0 = i;
						y0 = j;
						bol = 0;
					}
				}
			}
		if(bol == 1)
			printf("inexistente\n");
		else
		{
			if(x0 == x1 && y0 == y1)
				printf("ponto\n");
			else if(x0 == x1 || y0 == y1)
				printf("linha\n");
			else
				printf("%s\n", (x1-x0)*(y1-y0) <= 10 ? "adequada" : "grande");
		}
	}
	return 0;
}
