// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Largest and Smallest Box
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1294

#include<stdio.h>
#include<math.h>

main()
{
	double w,h;
	while(scanf("%lf %lf",&w,&h)==2&&(w||h))
		printf("%.3f 0.000 %.3f\n",((w+h)-sqrt(w*w-w*h+h*h))/6,((w<h)?w:h)/2);
	return 0;
}
