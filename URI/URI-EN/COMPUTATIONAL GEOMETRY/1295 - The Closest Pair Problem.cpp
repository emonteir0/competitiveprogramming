// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Closest Pair Problem
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1295

/* Closest Pair of Points

Encontra o par de pontos mais próximo em um plano 2D

Complexidade: O(NlogN)

*/

#include <bits/stdc++.h>
#define MAXN 10001
#define MAX 9999.99999

using namespace std;

struct pt
{
	double x;
	double y;
	bool operator<(const pt& pt2) const
	{
		if(x != pt2.x)
			return x < pt2.x;
		return y < pt2.y;
	}
};

pt pts[MAXN];

double Distance(pt p1, pt p2)
{
	double d = sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
	if(d > MAX)
		d = MAX + 1.0;
	return d;
}

double Closest_Pair(int a, int b)
{
	int i, j, k;
	double d1, d2, d;
	double xp;
	if(a == b)
		return MAX+1.0;
	else if(b-a == 1)
		return Distance(pts[b], pts[a]);
	else 
	{
		d1 = Closest_Pair(a,(a+b)/2);
		d2 = Closest_Pair((a+b)/2+1,b);
		d = min(d1,d2);
		j = (a+b)/2;
		xp = pts[j].x;
		do
		{
			k = (a+b)/2 + 1;
			while(xp - pts[k].x < d && k <= b)
			{
				d1 = Distance(pts[k],pts[j]);
				d = min(d,d1);
				k++;
			}
			j--;
		}while(xp - pts[j].x < d && j >= a);
		return d;
	}
}



int main()
{
	int N;
	double d;
	while(scanf("%d", &N) == 1 && N)
	{
		for(int i = 0; i < N; i++)
			scanf("%lf %lf",&pts[i].x, &pts[i].y);
		sort(pts, pts+N);
		d = Closest_Pair(0,N-1);
		if(d > MAX)
			printf("INFINITY\n");
		else
			printf("%.4lf\n", d);
	}
    return 0;
}

