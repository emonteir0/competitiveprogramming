// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Equidistance
// Level: 2
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1721

#include<cstdio>
#include<map>
#include<string>
#include<cstring>
#include<cmath>


using namespace std;

typedef struct a
{
	double theta,phi;
} tipo;

double absol(double x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{ 
	tipo struc1,struc2,struc3;
	string a;
	char b[31],c[31],d[31];
	double x,y,z,t,t1,t2,t3,p,p1,p2,p3,pi=acos(-1.0);
	map <string,tipo> Mapa;
	while((scanf("%s%*c",b)==1)&&(strcmp(b,"#")!=0))
	{
		scanf("%lf %lf%*c",&p,&t);
		p=(p*pi)/180.0;
		t=(t*pi)/180.0;
		struc1.phi=p;
		struc1.theta=t;
		a=b;
		Mapa[a]=struc1;
	}
	while(scanf("%s%*c",b)==1&&strcmp(b,"#")!=0)
	{
		scanf("%s%*c%s%*c",c,d);
		if((Mapa.find(b)!=Mapa.end())&&(Mapa.find(c)!=Mapa.end())&&(Mapa.find(d)!=Mapa.end()))
		{
			struc1=Mapa[b];
			struc2=Mapa[c];
			struc3=Mapa[d];
			t1=struc1.theta;
			t2=struc2.theta;
			t3=struc3.theta;
			p1=struc1.phi;
			p2=struc2.phi;
			p3=struc3.phi;
			x=(-cos(t1)*cos(p1)+cos(t2)*cos(p2));
			y=(sin(t1)*cos(p1)-sin(t2)*cos(p2));
			z=(sin(p1)-sin(p2));
			p=x*x+y*y+z*z;
			t=asin((-x*cos(t3)*cos(p3)+y*sin(t3)*cos(p3)+z*sin(p3))/sqrt(p));
			if(p!=0)
				printf("%s is %.0lf km off %s/%s equidistance.\n",d,absol(t*6378.0+1e-3)+1e-3,b,c);
			else
				printf("%s is 0 km off %s/%s equidistance.\n",d,b,c);
		}
		else
			printf("%s is ? km off %s/%s equidistance.\n",d,b,c);
	}
	return 0; 
}

