// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fire Flowers
// Level: 2
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1039

#include<stdio.h>

main()
{
	int a, b, c, d, e, f;
	while(scanf("%d %d %d %d %d %d",&a,&b,&c,&d,&e,&f)==6)
		printf("%s\n",(((a-d)>=0)&&((a-d)*(a-d))>=((b-e)*(b-e)+(c-f)*(c-f)))?"RICO":"MORTO");
	return 0;
}
