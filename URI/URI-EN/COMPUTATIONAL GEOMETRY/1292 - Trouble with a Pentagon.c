// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trouble with a Pentagon
// Level: 4
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1292

#include<stdio.h>
#include<math.h>

main()
{
	double l;
	while(scanf("%lf",&l)==1)
		printf("%.10f\n",l*sin(0.6*M_PI)/sin(0.35*M_PI));
	return 0;
}
