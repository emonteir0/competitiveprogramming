// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ask the Geographer
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2521

#include<stdio.h>
#include<math.h>

double ang(double x)
{
	return M_PI * (x / 180);
}

main()
{
	int r, a, b;
	double X, Y, Z;
	while(scanf("%d %d %d", &r, &a, &b) == 3)
	{
		X = (double)r * cos(ang(a)) * sin(ang(b)) + 1e-5;
		Y = (double)r * sin(ang(a)) + 1e-5;
		Z = (double)-r * cos(ang(a)) * cos(ang(b)) + 1e-5;
		printf("%.2lf %.2lf %.2lf\n", X, Y, Z); 
	}
	return 0;
}
