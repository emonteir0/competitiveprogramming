// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Romeo & Juliet!
// Level: 5
// Category: COMPUTATIONAL GEOMETRY
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1293

#include<stdio.h>
#include<math.h>
#define CONST M_PI/180

main()
{
	double a, b, c, d, e, f;
	while(scanf("%lf %lf %lf %lf %lf %lf", &a, &b, &c, &d, &e, &f) == 6)
		printf("%.3lf\n", sqrt((a-c)*(a-c)+(b-d)*(b-d))*(tan((90-e)*CONST)+tan((90-f)*CONST)));
	return 0;
}

