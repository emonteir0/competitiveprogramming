// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Harbin is Too Much Money!
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2103

#include<bits/stdc++.h>
#define ll long long
#define MAXN 10000
#define MOD 1300031
#define INV2 650016

using namespace std;

typedef struct
{
	ll s;
	int p;
} duo;

vector< vector< pair<int, int> > > adj(MAXN+1);

ll ret[MAXN+1];
int vet[MAXN+1];
char vis[MAXN+1];
duo duos[MAXN+1];

void dfs(int ind)
{
	int i, j, p = 0;
	duo dua;
	vector<int> folhas;
	ll sum = 0, f = 0;
	vis[ind] = 1;
	for(i = 0; i < adj[ind].size(); i++)
	{
		j = adj[ind][i].first;
		if(!vis[j])
		{
			dfs(j);
			ret[j] += adj[ind][i].second * duos[j].p;
			sum += ret[j];
			p += duos[j].p;
			folhas.push_back(j);
		}
	}
	for(i = 0; i < folhas.size(); i++)
	{
		dua = duos[folhas[i]];
		f += (p-dua.p) * ret[folhas[i]] + dua.p * (sum - ret[folhas[i]]);
	}
	f %= MOD;
	dua.s = ((sum + f*INV2) % MOD + MOD)%MOD;
	dua.p = p+1;
	duos[ind] = dua;
	ret[ind] = sum;
}

main()
{
	int i, j, a, b, x, N, T;
	ll sum = 0;
	scanf("%d", &T);
	while(T--)
	{
		sum = 0;
		scanf("%d", &N);
		vis[1] = 0;
		for(i = 1; i <= N; i++)
			adj[i].clear();
		for(i = 2; i <= N; i++)
		{
			vis[i] = 0;
			scanf("%d %d %d", &a, &b, &x);
			adj[a].push_back(make_pair(b,x));
			adj[b].push_back(make_pair(a,x));
		}
		dfs(1);
		for(i = 1; i <= N; i++)
			sum += duos[i].s;
		printf("%lld\n", sum%MOD);
	}
	return 0;
}
