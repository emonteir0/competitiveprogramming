// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Me Master
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2117

#include <bits/stdc++.h>
#define LIMPA_BUFFER while (getchar() != '\n')

using namespace std;

const int MAXN = 1001;
vector<int> adj[MAXN];
vector<bool> used(MAXN), visit(MAXN);
int match[MAXN], dist[MAXN];

void bfs(int n1, int n2) 
{
  	fill(dist, dist + n1, -1);
  	queue<int> q;
  	for (int u = 0; u < n1; u++)
	{
    	if (!used[u]) 
		{
	      	q.push(u);
	      	dist[u] = 0;
    	}
  	}
  	while (!q.empty()) 
	{
    	int u = q.front();
    	q.pop();
    	for (int j = 0; j < (int)adj[u].size(); j++)
		{
      		int v = match[adj[u][j]];
      		if (v >= 0 && dist[v] < 0)
			{
        		dist[v] = dist[u] + 1;
        		q.push(v);
      		}
    	}
  	}
}

bool dfs(int u) 
{
  	visit[u] = true;
  	for (int j = 0; j < (int)adj[u].size(); j++) 
	{
    	int v = match[adj[u][j]];
    	if (v < 0 || (!visit[v] && dist[v] == dist[u] + 1 && dfs(v)))
		{
      		match[adj[u][j]] = u;
      		used[u] = true;
      		return true;
    	}
  	}
  	return false;
}

int hopcroft_karp(int n1, int n2) 
{
	fill(match, match + n2, -1);
	fill(used.begin(), used.end(), false);
	int res = 0;
	for (;;) 
	{
    	bfs(n1, n2);
    	fill(visit.begin(), visit.end(), false);
    	int f = 0;
    	for (int u = 0; u < n1; u++) 
		{
      		if (!used[u] && dfs(u)) 
			{
        		f++;
      		}
    	}
    	if (f == 0) 
		{
      		return res;
    	}
    	res += f;
  	}
  	return res;
}

int letras[1000][26];
char str[141];

main()
{
	int N, M, x;
	char op;
	scanf("%d %d", &N, &M);
	for(int j = 0; j < N; j++)
	{
		LIMPA_BUFFER;
		scanf("%[A-Z a-z]", str);
		for(int i = 0; str[i]; i++)
		{
			if(str[i] >= 'a' && str[i] <= 'z')
				letras[j][str[i]-'a']++;
			if(str[i] >= 'A' && str[i] <= 'Z')
				letras[j][str[i]-'A']++;
		}
	}
	for(int j = 0; j < M; j++)
	{
		LIMPA_BUFFER;
		scanf("%c %d", &op, &x);
		for(int i = 0; i < N; i++)
			if(letras[i][op-'A'] >= x)
				adj[i].push_back(j);
	}
	printf("%d\n", hopcroft_karp(N, M));
	return 0;
}
