// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Transmissão de Energia
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2300

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(101);
int vis[101];

void dfs(int u)
{
	int i;
	vis[u] = 1;
	for(i = 0; i < (int)G[u].size(); i++)
		if(!vis[G[u][i]])
			dfs(G[u][i]);
}

main()
{
	int i, k = 1, N, M, u, v;
	while(scanf("%d %d", &N, &M) == 2 && (N||M))
	{
		for(i = 1; i <= N; i++)
		{
			G[i].clear();
			vis[i] = 0;
		}
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[u].push_back(v);
			G[v].push_back(u);
		}
		dfs(1);
		for(i = 2; i <= N; i++)
			if(vis[i] == 0)
				break;
		printf("Teste %d\n%s\n\n", k++, (i == N+1) ? "normal" : "falha");
	}
	return 0;
}
