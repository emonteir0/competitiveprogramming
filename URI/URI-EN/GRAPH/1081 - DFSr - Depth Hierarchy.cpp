// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: DFSr - Depth Hierarchy
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1081

#include<bits/stdc++.h>

using namespace std;

int N, M, t;
int G[21][21];
int vis[21];

int dfs(int u, int n)
{
	int ok = 0;
	vis[u] = t;
	for(int v = 1; v <= N; v++)
	{
		if(G[u][v])
		{
			ok = 1;
			if(vis[v] == t)
				printf("%*c%d-%d\n", n, ' ', u, v);
			else
			{
				printf("%*c%d-%d pathR(G,%d)\n", n, ' ', u, v, v);
				dfs(v, n+2);
			}
		}
	}
	return ok;
}

main()
{
	int T, u, v;
	scanf("%d", &T);
	while(T--)
	{
		printf("Caso %d:\n", ++t);
		scanf("%d %d", &N, &M);
		for(int i = 0; i <= N; i++)
			for(int j = 0; j <= N; j++)
				G[i][j] = 0;
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[u][v] = 1;
		}
		for(int i = 0; i <= N; i++)
		{
			if(vis[i] != t)
			{
				if(dfs(i, 2))
					printf("\n");
			}
		}
	}
	return 0;
}
