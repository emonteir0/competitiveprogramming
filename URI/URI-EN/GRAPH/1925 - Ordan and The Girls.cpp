// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ordan and The Girls
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1925

#include<bits/stdc++.h>

using namespace std;

char mat[1001][1001];

int color[1001][1001];
int colorlen[1000001];

int cor = 1;

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

bool inRange(int x, int N, int y, int M)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

main()
{
	int i, j, k, N, M, s, x, y, x2, y2, best = -1, ix, iy;
	queue< pair<int, int> > fila, fila2;
	set<int> conjunto, conjunto2;
	set<int>::iterator it;
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
		scanf("%s%*c", mat[i]);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
		{
			if(mat[i][j] == 'n' && color[i][j] == 0)
			{
				s = 1;
				color[i][j] = cor;
				fila = fila2;
				fila.push(make_pair(i, j));
				while(!fila.empty())
				{
					x = fila.front().first;
					y = fila.front().second;
					fila.pop();
					for(k = 0; k < 4; k++)
					{
						x2 = x + dx[k];
						y2 = y + dy[k];
						if(inRange(x2, N, y2, M))
						{
							if(!color[x2][y2] && mat[x2][y2] == 'n')
							{
								s++;
								color[x2][y2] = cor;
								fila.push(make_pair(x2, y2));
							}
						}
					}
				}
				colorlen[cor++] = s;
			}
		}
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			//printf("%d ", color[i][j]);
			if(mat[i][j] == '*')
			{
				conjunto = conjunto2;
				for(k = 0; k < 4; k++)
				{
					x = i + dx[k];
					y = j + dy[k];
					if(inRange(x, N, y, M))
					{
						if(mat[x][y] == 'n')
							conjunto.insert(color[x][y]);
					}
				}
				s = 0;
				for(it = conjunto.begin(); it != conjunto.end(); it++)
					s += colorlen[*it];
				if(s > best)
				{
					ix = i;
					iy = j;
					best = s;
				}
			}
		}
		//printf("\n");
	}
	printf("%d,%d\n", ix+1, iy+1);
	//printf("%d\n", best);
	return 0;
}
