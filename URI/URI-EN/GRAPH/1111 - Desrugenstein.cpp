// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Desrugenstein
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1111

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int mat[101][101];
int dx[4] = {100, -100, -1, 1};
int N, sz;
void printmat()
{
	for(int i = 0; i < sz; i++)
	{
		printf("%2d: ", i);
		for(int j = 0; j < sz; j++)
				printf(" %2d", mat[i][j] == INF ? -1 : mat[i][j]);
		printf("\n");
	}
	printf("\n\n");
}

main()
{
	int M, x, y, xi, yi, xf, yf;
	while(scanf("%d", &N) == 1 && N)
	{
		dx[0] = N;
		dx[1] = -N;
		sz = N*N;
		for(int i = 0; i < sz; i++)
			for(int j = 0; j < sz; j++)
				mat[i][j] = i == j ? 0 : INF;
		for(int i = (N-1)*N; i >= 0; i-=N)
		{
			for(int j = 0; j < N; j++)
			{
				for(int k = 0; k < 4; k++)
				{
					scanf("%d", &x);
					if(x)
					{
						//printf("%d -> %d\n", i+j, i+j+dx[k]);
						mat[i+j][i+j+dx[k]] = 1;
						//printf("(%d, %d) -> (%d, %d)\n", j, i/N, (i+j+dx[k])%N, (i+j+dx[k])/N);
					}
				}
			}
		}
		//printmat();
		
		for(int k = 0; k < sz; k++)
			for(int i = 0; i < sz; i++)
				for(int j = 0; j < sz; j++)
					if(mat[i][j] > mat[i][k] + mat[k][j])
						mat[i][j] = mat[i][k] + mat[k][j];
		//printmat();
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d %d %d", &xi, &yi, &xf, &yf);
			x = yi*N + xi;
			y = yf*N + xf;
			if(mat[x][y] != INF)
				printf("%d\n", mat[x][y]);
			else
				printf("Impossible\n");
		}
		printf("\n");
	}
	return 0;
}

