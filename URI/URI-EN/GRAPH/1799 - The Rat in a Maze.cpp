// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Rat in a Maze
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1799

#include<stdio.h>
#include<map>
#include<string>
 
using namespace std;
 
int mat[4000][4000];
int vis[4000];
int dis[4000];
 
map<string,int> conj;
map<string,int>::iterator it;
 
int N;
     
int dijkstra (int Vi, int Vf)
{
    for(int i=0;i<N;i++)
    {
        dis[i]=0x7fffffff;
        vis[i]=0;
    }
    dis[Vi] = 0;
    int t,i;
    for (t = 0;t<N;t++)
    {
        int v = -1;
        for (i = 0;i <N;i++)
            if (vis[i]==0 && (v < 0 ||dis[i] <dis[v]))
                v = i;
        vis[v] = 1;
        for (i = 0;i <N;i++)
            if (mat[v][i]!=0 && (dis[i] >dis[v] +mat[v][i]))
                dis[i] =dis[v] + mat[v][i];
    }
    return dis[Vf];
}
 
main()
{
    int N2,i,j;
    char a[101],b[101];
    string c,d;
    scanf("%d %d",&N,&N2);
    j=0;
    for(i=0;i<N2;i++)
    {
        scanf("%s %s",&a,&b);
        c=a;
        if(conj.find(c)==conj.end())
        {
            conj[c]=j++;
        }
        d=b;
        if(conj.find(d)==conj.end())
        {
            conj[d]=j++;
        }
        mat[conj[c]][conj[d]]=1;
        mat[conj[d]][conj[c]]=1;
    }
    printf("%d\n",dijkstra(conj["Entrada"],conj["*"])+dijkstra(conj["*"],conj["Saida"]));
    return 0;
}
