// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Knight in 3D Chess
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1952

#include<cstdio>
#include<queue>

using namespace std;

typedef struct
{
	int x,y,z,cont;
}tipo;

queue<tipo> fila;

int mat[100][100][100];

main()
{
	int i,a,b,c,x,y,z,x2,y2,z2;
	int movx[24]={1,1,-1,-1,2,2,-2,-2,  2,-2,0,0,2,-2,0,0,        1,-1,0,0,1,-1,0,0};
	int movy[24]={2,-2,2,-2,1,-1,1,-1,  0,0,2,-2,0,0,2,-2,        0,0,1,-1,0,0,1,-1};
	int movz[24]={0,0,0,0,0,0,0,0,      1,1,1,1,-1,-1,-1,-1,      2,2,2,2,-2,-2,-2,-2};
	tipo t,u;
	scanf("%d %d %d %d %d %d %d %d %d",&a,&b,&c,&t.x,&t.y,&t.z,&x,&y,&z);
	t.x--;t.y--;t.z--;x--;y--;z--;t.cont=0;
	fila.push(t);
	mat[t.x][t.y][t.z]=1;
	while(!fila.empty())
	{
		t=fila.front();
		fila.pop();
		if(t.x==x && t.y==y && t.z==z)
		{
			printf("%d\n",t.cont);
			return 0;
		}
		t.cont++;
		for(i=0;i<24;i++)
		{
			x2=t.x+movx[i];
			y2=t.y+movy[i];
			z2=t.z+movz[i];
			if(x2>=0 && x2<a && y2>=0 && y2<b && z2>=0 && z2<c)
			{
				if(mat[x2][y2][z2]==0)
				{
					u.x=x2;
					u.y=y2;
					u.z=z2;
					u.cont=t.cont;
					fila.push(u);
					mat[x2][y2][z2]=1;
				}
			}
		}
	}
	return 0;
}
