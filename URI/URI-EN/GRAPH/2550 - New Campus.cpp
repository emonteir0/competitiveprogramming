// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: New Campus
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2550

#include <bits/stdc++.h>
#define maxn 500000
 
using namespace std;
 
struct edge 
{
	int u, v, w;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}
 
int total;
int N, M, p[maxn], l[maxn];
edge e[maxn];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (u > v)
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
int kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
    return j >= N;
}
 
int main() 
{
    while (scanf("%d %d", &N, &M) == 2 && (N||M)) 
	{
        for (int i = 0; i < N; i++)
		{
			p[i] = i;
			l[i] = 1;
		}
 		total = 0;
        for (int i = 0; i < M; i++) 
		{
            scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
            e[i].u--;
            e[i].v--;
        }
        sort(e, e+M, cmp);
        kruskal();
    	if(l[0] == N)
 			printf("%d\n", total);
 		else
 			printf("impossivel\n");
    }
}
