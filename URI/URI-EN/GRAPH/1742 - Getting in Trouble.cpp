// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Getting in Trouble
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1742

#include<bits/stdc++.h>
#define ll unsigned long long int
#define INFINITO 1000000007
#define MOD 1000000007

using namespace std;

int N, M;
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};
ll valor[1001][1001];
char mat[1001][1001][4]; //UDLR
int dist[1001][1001];

bool inRange(int x, int y)
{
	return x>=0 && x<=N && y>=0 && y<=M;
}

typedef struct
{
	int x, y, dist;
} ponto;

queue<ponto> fila, fila2;
ponto p, p2;

main()
{
	int i, j, k;
	int H, x1, y1, x2, y2, x, y;
	int mode;
	while(scanf("%d %d %d",&N,&M,&H)==3)
	{
		for(i=0;i<=N;i++)
			for(j=0;j<=M;j++)
				for(k=0;k<4;k++)
					mat[i][j][k] = 0;
		while(H--)
		{
			scanf("%d %d %d %d",&x1,&y1,&x2,&y2);
			for(i=x1+1;i<x2;i++)
				for(j=y1+1;j<y2;j++)
					for(k=0;k<4;k++)
						mat[i][j][k] = 1;
			for(i=x1+1;i<x2;i++)
			{
				mat[i][y1][2] = 1;
				mat[i][y2][3] = 1;
			}
			for(j=y1+1;j<y2;j++)
			{
				mat[x1][j][0] = 1;
				mat[x2][j][1] = 1;
			}
		}
		scanf("%d",&H);
		while(H--)
		{
			fila = fila2;
			scanf("%d %d %d %d",&x1,&y1,&x2,&y2);
			for(i = 0; i <= N; i++)
				for(j = 0; j <= M; j++)
				{
					dist[i][j] = INFINITO;
					valor[i][j] = 0;
				}
			valor[x1][y1] = 1;
			dist[x1][y1] = 0;
			p.x = x1;
			p.y = y1;
			p.dist = 0;
			fila.push(p);
			while(!fila.empty())
			{
				p = fila.front();
				x = p.x;
				y = p.y;
				if (x == x2 && y == y2)
					break;
				fila.pop();
				p2 = p;
				p2.dist = p.dist + 1;
				for(i = 0; i < 4; i++)
				{
					p2.x = x + dx[i];
					p2.y = y + dy[i];
					if(inRange(p2.x, p2.y))
					{
						if(mat[x][y][i] == 0)
						{
							if(p2.dist < dist[p2.x][p2.y])
							{
								dist[p2.x][p2.y] = p2.dist;
								valor[p2.x][p2.y] = valor[x][y];
								fila.push(p2);
							}
							else if(p2.dist == dist[p2.x][p2.y])
								valor[p2.x][p2.y] = (valor[p2.x][p2.y] + valor[x][y]) % MOD;
						}
					}
				}
			}
			printf("%lld\n", valor[x2][y2]);
		}
		printf("\n");
	}
	return 0;
}

