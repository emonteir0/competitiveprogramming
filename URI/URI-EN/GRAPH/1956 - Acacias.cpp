// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Acacias
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1956

#include <bits/stdc++.h>
#define ll long long
#define maxn 10002
#define maxp 1000002
 
using namespace std;
 
struct edge 
{
	int u, v, w;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}
 
set<int> povos;
ll total;
int N, M, p[maxn], l[maxn];
edge e[maxp];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}
 
int main() 
{
	int i, j, z;
    scanf("%d", &N);
    for (i = 1; i <= N; i++)
	{
		p[i] = i;
		l[i] = 1;
	}
	for(i = 1; i < N; i++)
	{
		scanf("%d", &z);
		for(j = M; j < M + z; j++)
		{
			e[j].u = i;
			scanf("%d %d", &e[j].v, &e[j].w);
		}
		M += z;
	}
	total = 0;
    sort(e, e+M, cmp);
	kruskal();
	for(i = 1; i <= N; i++)
		povos.insert(id(i));
	printf("%d %lld\n", povos.size(), total);
}
