// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Route Change
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1123

#include<bits/stdc++.h>
#define INFINITO 2147483647
#define MAX 1000

using namespace std;


set< pair<int,int> > fila, fila2;
pair<int,int> p;
int mat[MAX+1][MAX+1];
bool con[MAX+1][MAX+1], visitado[MAX+1];
int distancia[MAX+1];
map<string, int> Mapa, Mapa2;

char inRota(int x, int C)
{
	return x>=0 && x<C-1;
}

void dijkstra(int x, int C, int N)
{
	int a, b, i;
	for(i = 0; i < N; i++)
	{
		visitado[i] = false;
		distancia[i] = INFINITO;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		b = p.second;
		if(!visitado[b])
		{
			visitado[b] = true;
			if(inRota(b, C))
			{
				i = b+1;
				if(!visitado[i] && mat[b][i] + a < distancia[i])
					fila.insert(make_pair((distancia[i]=mat[b][i]+a),i));
			}
			else
			{
				for(i = 0; i< N; i++)
				{
					if(con[b][i])
						if(!visitado[i] && mat[b][i] + a < distancia[i])
							fila.insert(make_pair((distancia[i]=mat[b][i]+a),i));
				}
			}
		}
	}
}

int main()
{
	int N,M,C,K,i,j,U,V,P;
	while(scanf("%d %d %d %d", &N, &M, &C, &K) == 4 && (N||M||C||K))
	{
		for(i=0;i<N;i++)
			for(j=0;j<N;j++)
				con[i][j] = false;
		while(M--)
		{
			scanf("%d %d %d",&U,&V,&P);
			con[U][V] = true;
			con[V][U] = true;
			mat[U][V] = P;
			mat[V][U] = P;
		}
		dijkstra(K,C,N);
		printf("%d\n",distancia[C-1]);
	}
	return 0;
}

