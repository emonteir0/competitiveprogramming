// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Design Space
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2683

#include <bits/stdc++.h>
#define _ ios_base::sync_with_stdio(0);
#define maxn 1000002
 
using namespace std;
 
struct edge 
{
	int u, v, w;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}
 
int total, total2;
int N, p[maxn], p2[maxn], l[maxn], l2[maxn];
edge e[maxn];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < N && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}

int id2(int x)
{
	return (x != p2[x] ? p2[x] = id2(p2[x]) : p2[x]);
}
 
void join2(int u, int v)
{
    if (l2[u] < l2[v])
		swap(u, v);
    p2[v] = u;
	l2[u] += l2[v];
}
 

void kruskal2() 
{
	int i, j, u, v, w;
    for (i = N-1, j = 1; i >= 0 && j < N; i--) 
	{
        u = id2(e[i].u);
		v = id2(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join2(u, v);
            total2 += w;
			j++;
        }
    }
}
 
int main() 
{
	int i;
    scanf("%d", &N); 
    for (i = 0; i < N; i++)
	{
		p[i] = i;
		p2[i] = i;
		l[i] = 1;
		l2[i] = 1;
	}
	total = 0;
    for (i = 0; i < N; i++) 
	{
        scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
        e[i].u--;
        e[i].v--;
    }
    sort(e, e+N, cmp);
	kruskal();
	kruskal2();
	printf("%d\n%d\n", total2, total);
}
