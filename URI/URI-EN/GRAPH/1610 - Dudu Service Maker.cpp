// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dudu Service Maker
// Level: 2
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1610

#include<bits/stdc++.h>

using namespace std;


char vis[10001], vis2[10001];
vector< vector<int> > G(10001), G2(10001);
vector<int> indices;

void dfs(int x)
{
	int i;
	vis[x] = 1;
	for(i = 0; i < G[x].size(); i++)
		if(!vis[G[x][i]])
			dfs(G[x][i]);
	indices.push_back(x);
}

int dfs2(int x)
{
	int i, ans = 1;
	vis2[x] = 1;
	for(i = 0; i < G2[x].size(); i++)
		if(!vis2[G2[x][i]])
			ans += dfs2(G2[x][i]);
	return ans;
}

main()
{
	int i, N, M, T, x, y, ans;
	char b;
	scanf("%d", &T);
	while(T--)
	{
		ans = 0;
		scanf("%d %d", &N, &M);
		indices.clear();
		for(i = 1; i <= N; i++)	
		{
			G[i].clear();
			G2[i].clear();
			vis[i] = 0;
			vis2[i] = 0;
		}
		while(M--)
		{
			scanf("%d %d", &x, &y);
			G[x].push_back(y);
			G2[y].push_back(x);
		}
		for(i = 1; i <= N; i++)
			if(!vis[i])
				dfs(i);
		for(i = N-1; i >= 0; i--)
			if(!vis2[indices[i]])
				ans |= (dfs2(indices[i]) > 1);
		printf("%s\n", ans ? "SIM" : "NAO");
	}
	return 0;
}
