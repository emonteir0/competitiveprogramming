// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: My T-Shirt Suits Me
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1362

#include <bits/stdc++.h>

using namespace std;

struct edge { int u, v, rev, cap, f; };

const int MAXN = 40, INF = 0x3f3f3f3f;
vector<edge> adj[MAXN];

void add_edge(int u, int v, int cap) 
{
	adj[u].push_back((edge){u, v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){v, u, (int)adj[u].size() - 1, 0, 0});
}

int edmonds_karp(int nodes, int source, int sink) 
{
	int max_flow = 0;
	for (;;)
	{
		vector<edge*> pred(nodes, (edge*)0);
		queue<int> q;
		q.push(source);
		while (!q.empty() && !pred[sink]) 
		{
			int u = q.front();
			q.pop();
			for (int j = 0; j < (int)adj[u].size(); j++) 
			{
				edge &e = adj[u][j];
				if (!pred[e.v] && e.cap > e.f)
				{
					pred[e.v] = &e;
					q.push(e.v);
				}
			}
		}
		if (!pred[sink]) 
		{
			break;
		}
		int flow = INF;
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			flow = min(flow, pred[u]->cap - pred[u]->f);
		}
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			pred[u]->f += flow;
			adj[pred[u]->v][pred[u]->rev].f -= flow;
		}
		max_flow += flow;
	}
	return max_flow;
}

int N, M;

map<string, int> Mapa;

int main() 
{
	char camisa[5], camisa2[5];
	int T;
	Mapa["L"] = 2;
	Mapa["XL"] = 3;
	Mapa["XXL"] = 4;
	Mapa["S"] = 5;
	Mapa["XS"] = 6;
	Mapa["M"] = 7;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &M);
		for(int i = 2; i < 8; i++)
			add_edge(i, 1, N/6);
		for(int i = 8; i < M+8; i++)
		{
			scanf("%s %s", camisa, camisa2);
			add_edge(0, i, 1);
			add_edge(i, Mapa[camisa], 1);
			add_edge(i, Mapa[camisa2], 1);
		}
		printf("%s\n", (M == edmonds_karp(M+8, 0, 1)) ? "YES" : "NO");
		for(int i = 0; i < M+8; i++)
			adj[i].clear();
	}
	return 0;
}
