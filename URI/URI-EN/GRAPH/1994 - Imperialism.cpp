// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Imperialism
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1994

#include<bits/stdc++.h>
#define MAXL 20
#define MAXN 50001

using namespace std;

typedef struct
{
	int x,i;
}par;


int nivel[MAXN];
int maxi,imax;
map<int,set<int> > Mapa, Mapa2;
queue<int> fila,fila2;
set<int> conjunto;
set<int>::iterator it;

int DFS(int U)
{
	int V;
	imax=0;
	fila=fila2;
	fila.push(U);
	while(!fila.empty()){
		U=fila.front();
		fila.pop();
		conjunto=Mapa[U];
		for(it=conjunto.begin();it!=conjunto.end();++it)
		{
			V=*it;
			if(nivel[V]==-1)
			{
				nivel[V]=nivel[U]+1;
				if(nivel[V]>maxi)
				{
					maxi=nivel[V];
					imax=V;
				}
				fila.push(V);
			}
		}
	}
}

main()
{
	int N,i,j,x,y,cont=0;
	while((scanf("%d",&N)==1)&&(N!=-1))
	{
		Mapa=Mapa2;
		for(i=2;i<=N;i++)
		{
			scanf("%d",&x);
			Mapa[x].insert(i);
			Mapa[i].insert(x);
		}
		maxi=0;
		for(i=2;i<=N;i++)
			nivel[i]=-1;
		nivel[1]=0;
		DFS(1);
		maxi=0;
		for(i=1;i<=N;i++)
			nivel[i]=-1;
		nivel[imax]=0;
		DFS(imax);
		printf("%.0f\n",ceil((float)(maxi)/2));
	}
	return 0;
}
