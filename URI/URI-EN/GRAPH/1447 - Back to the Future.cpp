// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Back to the Future
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1447

#include<bits/stdc++.h>
#define ll long long
#define REP(i, N) for(int i = 0; i < N; i++)
using namespace std;

namespace Mcmf
{
	const int MAXV = 1000100;
	const int MAXE = 1000100;
	const ll oo = 5e15;
	
	int V, E;
	int last[MAXV], how[MAXV]; ll dist[MAXV], pi[MAXV];
	int next[MAXE], from[MAXE], adj[MAXE]; ll cap[MAXE], cost[MAXE];
	
	struct cmpf 
	{
		bool operator () (int a, int b) 
		{
		  if (dist[a] != dist[b]) return dist[a] < dist[b];
		  	return a < b;
		}
	};
	
	set<int, cmpf> S;
	
	void init(int n)
	{
		V = n;
		E = 0;
		REP(i, V)
			last[i] = -1;
	}
	
	void edge(int x, int y, ll c, ll w) 
	{
		from[E] = x; adj[E] = y; cap[E] = c; cost[E] = +w; next[E] = last[x]; last[x] = E++;
		from[E] = y; adj[E] = x; cap[E] = 0; cost[E] = -w; next[E] = last[y]; last[y] = E++;
	}

	pair<ll, ll> run(int src, int sink) 
	{
		ll total = 0;
		ll flow = 0;
		
		REP(i, V) pi[i] = oo;
		pi[src] = 0;
	
		
		for (;;) 
		{
			bool done = true;
			REP(x, V) 
			{
				for (int e = last[x]; e != -1; e = next[e])
				{
					if (cap[e] && pi[adj[e]] > pi[x] + cost[e])
					{
						pi[adj[e]] = pi[x] + cost[e];
						done = false;
					}
				}
			}
			if(done)
				break;
		}
		
		for (;;) 
		{
			REP(i, V) dist[i] = oo;
			S.clear();
			
			dist[src] = 0;
			S.insert(src);
			
			while (!S.empty()) 
			{
				int x = *S.begin();
				S.erase(S.begin());
				if (x == sink) break;
				
				for (int e = last[x]; e != -1; e = next[e]) 
				{
					if (cap[e] == 0) continue;
					
					int y = adj[e];
					ll val = dist[x] + pi[x] + cost[e] - pi[y];
					
					if (val < dist[y]) 
					{
						S.erase(y);
						dist[y] = val;
						how[y] = e;
						S.insert(y);
					}
				}
			}
			if (dist[sink] >= oo / 2) break;
			
			ll aug = cap[how[sink]];
			for (int i = sink; i != src; i = from[how[i]])
				aug = min(aug, cap[how[i]]);
			
			for (int i = sink; i != src; i = from[how[i]]) 
				{
				cap[how[i]] -= aug;
				cap[how[i]^1] += aug;
				total += cost[how[i]] * aug;
			}
			flow += aug;
			
			REP(i, V)
				pi[i] = min(pi[i] + dist[i], oo);
		}
	
		return {total, flow};
	}
}

int st[10000], en[10000];
ll cost[10000];

int main()
{
	int N, M, D, K, t = 0;
	pair<ll, ll> p;
	while(scanf("%d %d", &N, &M) != EOF)
	{
		Mcmf::init(N+1);
		for(int i = 0; i < M; i++)
			scanf("%d %d %lld", &st[i], &en[i], &cost[i]);
		scanf("%d %d", &D, &K);
		Mcmf::edge(0, 1, D, 0);
		for(int i = 0; i < M; i++)
		{
			Mcmf::edge(st[i], en[i], K, cost[i]);
			Mcmf::edge(en[i], st[i], K, cost[i]);
		}
		p = Mcmf::run(0, N);
		printf("Instancia %d\n", ++t);
		if(p.second != D)
			printf("impossivel\n\n");
		else
			printf("%lld\n\n", p.first);
	}
	return 0;
}
