// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mrs. Montagny's Table
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2131

#include<bits/stdc++.h>

using namespace std;

int N, t = 1;
vector<int> G[101];
int cores[101];

int dfs(int x, int cor)
{
	int v;
	if(cores[x] != 0)
		return cores[x] == cor;
	else
		cores[x] = cor;
	int r = 1;
	for(int i = 0; i < G[x].size(); i++)
	{
		v = G[x][i];
		r &= (dfs(v, cor == 1 ? 2 : 1));
	}
	return r;
}

main()
{
	int r, M, x, y;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			cores[i] = 0;
		}
		while(M--)
		{
			scanf("%d %d", &x, &y);
			G[x].push_back(y);
			G[y].push_back(x);
		}
		r = 1;
		for(int i = 1; i <= N; i++)
			if(cores[i] == 0)
				r &= dfs(i, 1);
		printf("Instancia %d\n%s\n\n", t++, r ? "sim" : "nao");
	}
	
	return 0;
}
