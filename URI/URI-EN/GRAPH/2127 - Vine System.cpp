// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vine System
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2127

#include <bits/stdc++.h>
#define ll long long
#define maxn 1001
#define maxp 2000001
 
using namespace std;
 
struct edge 
{
	int u, v, w;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}
 
ll total;
int N, M, p[maxn], l[maxn];
edge e[maxp];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}
 
int main() 
{
	int i, k = 1;
    while (scanf("%d %d", &N, &M) == 2) 
	{
        for (i = 0; i <= N; i++)
		{
			p[i] = i;
			l[i] = 1;
		}
 		total = 0;
        for (i = 0; i < M; i++) 
            scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
        sort(e, e+M, cmp);
    	kruskal();
 		printf("Instancia %d\n%lld\n\n", k++, total);
    }
}
