// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trucks
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1476

#include<bits/stdc++.h>
#define MAXN 100005
#define MAXL 18
#define INF 1001001001
#define ll long long

using namespace std;

int N, M;

struct Edge
{
	int u, v, w;
};

vector<int> G[MAXN], W[MAXN];

int p[MAXN], l[MAXN];
Edge e[MAXN];

int ancestral[MAXN][MAXL], valores[MAXN][MAXL], nivel[MAXN];

bool cmp(Edge a, Edge b)
{
	return a.w > b.w;
}

inline int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
inline void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		if(l[u] < l[v])
			swap(u, v);
		w = e[i].w;
        if (u != v)
		{
            p[v] = u;
            l[u] += l[v];
			j++;
			u = e[i].u;
			v = e[i].v;
			G[u].push_back(v);
			W[u].push_back(w);
			G[v].push_back(u);
			W[v].push_back(w);
        }
    }
}

inline void dfs(int u)
{
	int i, v, w;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		w = W[u][i];
		if(nivel[v] == -1)
		{
			nivel[v] = nivel[u] + 1;
			ancestral[v][0] = u;
			valores[v][0] = w;
			dfs(v);
		}
	}
}

inline void build()
{
	int i, j;
    for(j = 1; j < MAXL; j++)
		for(i = 1; i <= N; i++)
			if(ancestral[i][j-1] != 0)
			{
				ancestral[i][j] = ancestral[ancestral[i][j-1]][j-1];
				valores[i][j] = min(valores[i][j-1], valores[ancestral[i][j-1]][j-1]);
			}
}

inline int LCA(int u, int v)
{   
	int i, ans = INF;
    if(nivel[u] < nivel[v]) swap(u, v);
    for(i = MAXL-1; i >= 0; i--)
        if(nivel[u] - (1<<i) >= nivel[v])
        {
        	ans = min(ans, valores[u][i]);
            u = ancestral[u][i];
        }
    if(u == v) 
		return ans;
    for(i = MAXL-1; i >= 0; i--)
        if((ancestral[u][i] != 0) && (ancestral[u][i] != ancestral[v][i]))
		{
			ans = min(ans, valores[u][i]);
            u = ancestral[u][i];
            ans = min(ans, valores[v][i]);
            v = ancestral[v][i];
        }    
    return min(ans, min(valores[u][0], valores[v][0]));
}

main()
{
	int Q, u, v;
	while(scanf("%d %d %d", &N, &M, &Q) == 3)
	{
		for(int i = 1; i <= N; i++)
		{
			p[i] = i;
			l[i] = 1;
			nivel[i] = -1;
			for(int j = 0; j < MAXL; j++)
			{
				ancestral[i][j] = 0;
				valores[i][j] = 0;	
			}
		}
		for(int i = 0; i < M; i++)
			scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
		sort(e, e+M, cmp);
		kruskal();
		nivel[1] = 1;
		dfs(1);
		build();
		while(Q--)
		{
			scanf("%d %d", &u, &v);
			printf("%d\n", LCA(u, v));
		}
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
	}
	return 0;
}
