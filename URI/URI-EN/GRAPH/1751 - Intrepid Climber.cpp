// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Intrepid Climber
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1751

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(100001), W(100001);
char visit[100001];


void dfs(int u)
{
	int i, v;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		dfs(v);
		visit[u] |= visit[v];
	}
}

int solve(int u, char flag)
{
	int i, v, w, ret = 0;
	if(!visit[u])
		return 0;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		w = W[u][i];
		if(visit[v])
		{
			if(flag)
				ret += solve(v, 1) + w;
			else
				ret = max(ret, solve(v, 0) + w);
		}
	}
	return ret;
}

main()
{
	int i, N, M, u, v, w, x, tot = 0, ma = 0;
	scanf("%d %d", &N, &M);
	for(i = 1; i < N; i++)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(v);
		W[u].push_back(w);
	}
	while(M--)
	{
		scanf("%d", &x);
		visit[x] = 1;
	}
	dfs(1);
	//printf("%d %d\n", solve(1, 1), solve(1, 0));
	printf("%d\n", solve(1, 1) - solve(1, 0));
	return 0;
}
