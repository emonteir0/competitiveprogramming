// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Slush Fund
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2173

#include <bits/stdc++.h>
#define maxn 200002
#define ll long long
 
using namespace std;
 
struct edge 
{
	int u, v, w;
};

ll total;
int N, M, p[maxn], p2[maxn], l[maxn], l2[maxn];
edge e[maxn], e2[maxn];

bool cmp(edge u, edge v)
{
	return u.w < v.w;
}
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 

int id2(int x)
{
	return (x != p2[x] ? p2[x] = id2(p2[x]) : p2[x]);
}
 
void join2(int u, int v)
{
    if (l2[u] < l2[v])
		swap(u, v);
    p2[v] = u;
	l2[u] += l2[v];
}

void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0; i < M; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total -= w;
        }
        j = M - 1 - i;
        u = id2(e[j].u);
		v = id2(e[j].v);
		w = e[j].w;
        if (u != v)
		{
            join2(u, v);
            total += w;
        }
    }
}
 
int main() 
{
	int i;
    while (scanf("%d %d", &N, &M) == 2 && (N||M)) 
	{
        for (i = 1; i <= N; i++)
		{
			p[i] = p2[i] = i;
			l[i] = l2[i] = 1;
		}
 		total = 0;
        for (i = 0; i < M; i++) 
            scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
        sort(e, e+M, cmp);
    	kruskal();
 		printf("%lld\n", total);
    }
}
