// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Teletransport
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1713

#include<stdio.h>
#define ll unsigned long long int
#define MOD 10000

int tam;
int vet[101];
int F[101][101], M[101][101];
ll G[101][101];

void multiply();
void multiply2();
   
void power(int n)
{
	if(n==0||n==1)
	  return;		
	power(n/2);
	multiply2();
	if(n%2==1)
	 multiply();
}
   
void multiply()
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*M[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2()
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*F[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}


main()
{
	int i, j, N, x, y, a, b, c, d;
	while(scanf("%d %d",&tam,&N)==2)
	{
		scanf("%d %d", &x, &y);
		for(i = 0; i < tam; i++)
			for(j = 0; j < tam; j++)
				F[i][j] = M[i][j] = 0;
		for(i = 0; i < tam; i++)
		{
			scanf("%d %d %d %d", &a, &b, &c, &d);
			F[i][a-1] = ++M[i][a-1];
			F[i][b-1] = ++M[i][b-1];
			F[i][c-1] = ++M[i][c-1];
			F[i][d-1] = ++M[i][d-1];
		}
		if(N == 0)
			printf("%d\n", x==y);
		else
		{
			power(N);
			printf("%d\n", F[x-1][y-1]);
		}
	}
	return 0;
}

