// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elias and Golias
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1592

#include<bits/stdc++.h>
#define INF 1125899906842624LL
#define ll long long
using namespace std;

int N, K;
vector<int> G[1001];
vector<ll> W[1001];

ll dist[1001];

void dijkstra()
{
	int a, b, v;
	ll w;
	priority_queue<pair< int, pair<ll, int> > > fila;
	for(int i = 0; i < N; i++)
			dist[i] = INF;
	fila.push(make_pair(0, make_pair(0, 0)));
	while(!fila.empty())
	{
		b = -fila.top().first;
		w = -fila.top().second.first;
		a = fila.top().second.second;
		fila.pop();
		if(w < dist[a])
		{
			dist[a] = w;
			if(b+2 <= K)
			{
				for(int i = 0; i < G[a].size(); i++)
				{
					v = G[a][i];
					fila.push(make_pair(-b-1, make_pair(-w-W[a][i], v)));
				}
			}
		}
	}
}

main()
{
	int T, M, u, v;
	ll w;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d %d", &N, &M, &K);
		for(int i = 0; i < N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
		while(M--)
		{
			scanf("%d %d %lld", &u, &v, &w);
			G[u].push_back(v);
			W[u].push_back(w);		
		}
		dijkstra();
		if(dist[N-1] >= INF)
			printf("-1\n");
		else
			printf("%lld\n", dist[N-1]);
	}
	return 0;
}
