// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Juntando Capitais
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2885

#include<bits/stdc++.h>
#define INF 1e50

using namespace std;

int N, K;
int X[101], Y[101];
double adj[101][101];
double dp[101][1024];

double solve()
{
	int lim = 1 << K;
	double me = INF;
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < lim; j++)
			dp[i][j] = INF;
	}
	for(int i = 0; i < K; i++)
		dp[i][1<<i] = 0;
	for(int mask = 1; mask < lim; mask++)
	{
		for(int i = 0; i < N; i++)
		{
			for(int ss = mask; ss > 0; ss = (ss-1)&mask)
				dp[i][mask] = min(dp[i][mask], dp[i][ss] + dp[i][mask-ss]);
			if(dp[i][mask] != INF)
				for(int j = K; j < N; j++)
					dp[j][mask] = min(dp[j][mask], dp[i][mask] + adj[i][j]);
		}
	}
	for(int i = 0; i < N; i++)
		me = min(me, dp[i][lim-1]);
	return me;
}

int main()
{
	scanf("%d %d", &N, &K);
	for(int i = 0; i < N; i++)
		scanf("%d %d", &X[i], &Y[i]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			adj[i][j] = sqrt((X[i]-X[j])*(X[i]-X[j]) + (Y[i]-Y[j])*(Y[i]-Y[j]));
	printf("%.5lf\n", solve());
	return 0;
}
