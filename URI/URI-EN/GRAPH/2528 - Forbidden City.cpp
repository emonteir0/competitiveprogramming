// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Forbidden City
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2528

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

vector<int> G[1001];
int dist[1001];

main()
{
	int N, M, u, v, C, R, E;
	queue<int> fila;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			dist[i] = INF;
		}
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[u].push_back(v);
			G[v].push_back(u);
		}
		scanf("%d %d %d", &C, &R, &E);
		dist[C] = 0;
		fila = queue<int>();
		fila.push(C);
		while(!fila.empty())
		{
			u = fila.front();
			fila.pop();
			if(u == R)
				break;
			for(int i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				if(dist[v] > dist[u]+1 && v != E)
				{
					dist[v] = dist[u]+1;
					fila.push(v);
				}
			}
		}
		printf("%d\n", dist[R]);
	}
	return 0;
}
