// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bicho da Goiaba
// Level: 2
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2485

#include<bits/stdc++.h>

using namespace std;

queue< pair<int, int> > fila, fila2;

int mat[101][101];
int dist[101][101];

main()
{
	int T, N, M, x, y, i, j, best;
	pair<int, int> p;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &M);
		for(i = 1; i <= N; i++)
			for(j = 1; j <= M; j++)
			{
				dist[i][j] = 999999;
				scanf("%d", &mat[i][j]);
			}
		scanf("%d %d", &x, &y);
		best = 0;
		dist[x][y] = 0;
		fila.push(make_pair(x, y));
		while(!fila.empty())
		{
			p = fila.front();
			fila.pop();
			x = p.first;
			y = p.second;
			if(mat[x][y] == 2)
				continue;
			mat[x][y] = 2;
			for(i = x-1; i <= x+1; i++)
				for(j = y-1; j <= y+1; j++)
				{
					if(!((x == i) && (y == j)))
					{
						if(i >= 1 && i <= N && j >= 1 && j <= M)
							if(mat[i][j] == 1)
							{
								fila.push(make_pair(i, j));
								dist[i][j] = min(dist[i][j], dist[x][y] + 1);
								best = max(best, dist[i][j]); 
							}
					}
				}
		}
		printf("%d\n", best);
	}
	return 0;
}
