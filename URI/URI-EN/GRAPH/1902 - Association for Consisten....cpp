// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Association for Consisten...
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1902

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;
int G[200001];
int vis[200001];
int cont;

void dfs(int x)
{
	if(vis[x] == 2)
		return;
	if(vis[x] == 1)
	{
		cont++;
		return;
	}
	vis[x] = 1;
	dfs(G[x]);
	vis[x] = 2;
}


main()
{
	char str[11], str2[11];
	int K = 0;
	while(scanf("%s %s", str, str2) == 2)
	{
		if(Mapa.count(str) <= 0)
			Mapa[str] = K++;
		if(Mapa.count(str2) <= 0)
			Mapa[str2] = K++;
		G[Mapa[str]] = Mapa[str2];
	}
	for(int i = 0; i < K; i++)
		if(vis[i] == 0)
			dfs(i);
	printf("%d\n", cont);
	return 0;
}
