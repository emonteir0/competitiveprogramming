// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flea Circus
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2529

#include<bits/stdc++.h>
#define rand32 (rand() & 0x7fff) | ((rand() & 0x7fff) << 15)
using namespace std;

set<int> priorities;

typedef struct item * pitem;
struct item {
    int prior, val, cnt, s1;
    bool rev;
    pitem l, r;
    item(int x)
    {
    	this->prior = rand32;
    	this->val = x;
    	this->s1 = x&1;
		this->cnt = 1;
    	this->rev = false;
    	this->l = NULL;
    	this->r = NULL;
    }
};

int cnt (pitem it) 
{
    return it ? it->cnt : 0;
}

void upd_cnt (pitem it) 
{
    if (it)
        it->cnt = cnt(it->l) + cnt(it->r) + 1;
}

void push (pitem it) 
{
    if (it && it->rev) 
	{
        it->rev = false;
        swap (it->l, it->r);
        if (it->l)  it->l->rev ^= 1;
        if (it->r)  it->r->rev ^= 1;
    }
}

void operation(pitem t)
{
	if(!t)
		return;
	push(t->l);
	push(t->r);
	t->s1 = (t->val)&1;
	if(t->l)
		t->s1 += (t->l)->s1; 
	if(t->r)
		t->s1 += (t->r)->s1;
}

void merge (pitem & t, pitem l, pitem r) 
{
    push (l);
    push (r);
    if (!l || !r)
        t = l ? l : r;
    else if (l->prior > r->prior)
        merge (l->r, l->r, r),  t = l;
    else
        merge (r->l, l, r->l),  t = r;
    upd_cnt (t);
    operation(t);
}

void split (pitem t, pitem & l, pitem & r, int key, int add = 0) {
    if (!t)
        return void( l = r = 0 );
    push (t);
    int cur_key = add + cnt(t->l);
    if (key <= cur_key)
    {
    	r = t;
        split (t->l, l, t->l, key, add);
    }
    else
    {
    	l = t;
        split (t->r, t->r, r, key, add + 1 + cnt(t->l));
	}
	upd_cnt (t);
    operation(t);
}

void Treverse (pitem t, int l, int r) 
{
    pitem t2, t3;
    split (t, t, t2, r+1);
    split (t, t, t3, l);
    t3->rev ^= 1;
    merge (t, t, t3);
    merge (t, t, t2);
}

void Tinsert(pitem & t, int x, int key)
{
	pitem t2, new_item;
	new_item = new item(x);
	split(t, t, t2, key);
	merge(t, t, new_item);
	merge(t, t, t2);
}

void Tchange (pitem & t, int key, int x, int add = 0) 
{
	if(!t)
		return;
	push(t);
	int cur_key = add+cnt(t->l);
	if(key == cur_key)
		t->val = x;
	else if(key < cur_key)
		Tchange(t->l, key, x, add);
	else
		Tchange(t->r, key, x, add+1+cnt(t->l));
	operation(t);
}

void Tprint (pitem t, int add = 0) 
{
    if (!t)
		return;
    push(t);
    Tprint(t->l, add);
    printf("(%d, %d) ", t->val, add+cnt(t->l));
    Tprint(t->r, add+1+cnt(t->l));
}

int Tcont(pitem t, int l, int r)
{
	int x;
	pitem t2, t3;
	split(t, t, t2, r+1);
	split(t, t, t3, l);
	x = t3->s1;
	merge(t, t, t3);
	merge(t, t, t2);
	return x;
}

void Tclear(pitem t)
{
	if(t->l)
		Tclear(t->l);
	if(t->r)
		Tclear(t->r);
	t = NULL;
}

int main()
{
	int N, M, x, y;
	char op;
	pitem t;
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		scanf("%d", &x);
		t = new item(x);
		for(int i = 1; i < N; i++)
		{
			scanf("%d%*c", &x);
			Tinsert(t, x, i);
		}
		while(M--)
		{
			scanf("%c %d %d%*c", &op, &x, &y);
			if(op == 'S')
				Tchange(t, x-1, y);
			if(op == 'E')
				printf("%d\n", (y-x+1) - Tcont(t, x-1, y-1));
			if(op == 'O')
				printf("%d\n", Tcont(t, x-1, y-1));
			if(op == 'I')
				Treverse(t, x-1, y-1);
			//Tprint(t);
			//printf("\n");
		}
		Tclear(t);
	}
	return 0;
}
