// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Design Labirints
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1076

#include<bits/stdc++.h>

using namespace std;

set< pair<int, int> > conjunto, conjunto2;


main()
{
	int T, N, x, y;
	scanf("%d", &T);
	while(T--)
	{
		conjunto = conjunto2;
		scanf("%*d %*d %d", &N);
		while(N--)
		{
			scanf("%d %d", &x, &y);
			conjunto.insert(make_pair(min(x, y), max(x, y)));
		}
		printf("%u\n", 2*conjunto.size());
	}
	return 0;
}
