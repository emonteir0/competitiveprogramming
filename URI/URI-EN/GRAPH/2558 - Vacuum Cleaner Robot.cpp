// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vacuum Cleaner Robot
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2558

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int N, M, G, K, ma;
int x[11], y[11];
int xs, ys;
int xr, yr;

int dx[4] = {1, -1, 0, 0};
int dy[4] = {0, 0, 1, -1};

int dist[11][11];
int distancia[100][100];

char mat[100][101];

struct st
{
	int x, y;
	st(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

bool inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

void bfs(st source)
{
	int x2, y2;
	queue<st> fila;
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			distancia[i][j] = INF;
	distancia[source.x][source.y] = 0;
	fila.push(source);
	while(!fila.empty())
	{
		source = fila.front();
		fila.pop();
		for(int i = 0; i < 4; i++)
		{
			x2 = source.x + dx[i];
			y2 = source.y + dy[i];
			if(inRange(x2, y2) && mat[x2][y2] != '#' && distancia[x2][y2] == INF)
			{
				distancia[x2][y2] = distancia[source.x][source.y] + 1;
				fila.push(st(x2, y2));
			}
		}
	}
}

void compute()
{
	for(int i = 0; i < G+2; i++)
	{
		bfs(st(x[i], y[i]));
		for(int j = 0; j < G+2; j++)
			dist[i][j] = distancia[x[j]][y[j]];
	}
}


void contar(int x, int bit, int& sum)
{
	//printf("%d %d %d\n", x, bit, sum);
	if(x == 1)
	{
		if(K >= sum)
			ma = max(ma, __builtin_popcount(bit)-1);
		return;
	}
	for(int i = 1; i < G+2; i++)
	{
		if(!(bit & (1 << i)))
		{
			sum += dist[x][i];
			contar(i, bit | (1 << i), sum);
			sum -= dist[x][i];
		}
	}
}


main()
{
	int xs, ys, k, sum;
	while(scanf("%d %d", &G, &K) == 2)
	{
		k = 2;
		scanf("%d %d", &M, &N);
		for(int i = 0; i < N; i++)
		{
			scanf("%s", mat[i]);
			for(int j = 0; j < M; j++)
			{
				if(mat[i][j] == 'R')
				{
					x[0] = i;
					y[0] = j;
				}
				if(mat[i][j] == 'S')
				{
					x[1] = i;
					y[1] = j;
				}
				if(mat[i][j] == '*')
				{
					x[k] = i;
					y[k++] = j;
				}
			}
		}
		compute();
		sum = 0;
		ma = -1;
		contar(0, 0, sum);
		printf("%d\n", ma);
	}
	return 0;
}
