// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: What Headquarter?
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2118

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int dist[100001];
int ind[100001];
vector<int> G[100001];

int M[100001];
int T[100001];

int best(int x, int y)
{
	if(M[x] != M[y])
		return M[x] > M[y] ? x : y;
	return T[x] > T[y] ? x : y;
}

main()
{
	int L, S, Q, A, u, v;
	queue<int> fila;
	scanf("%d %d %d %d", &L, &S, &Q, &A);
	for(int i = 1; i <= L; i++)
	{
		dist[i] = INF;
		ind[i] = 0;
	}
	for(int i = 1; i <= S; i++)
	{
		scanf("%d %d %d", &u, &M[i], &T[i]);
		dist[u] = 0;
		if(ind[u] == 0)
			fila.push(u);
		ind[u] = best(ind[u], i);
	}
	while(A--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	while(!fila.empty())
	{
		u = fila.front();
		fila.pop();
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u]+1)
			{
				dist[v] = dist[u]+1;
				ind[v] = ind[u];
				fila.push(v);
			}
			else if(dist[v] == dist[u]+1)
				ind[v] = best(ind[u], ind[v]);
		}
	}
	while(Q--)
	{
		scanf("%d", &u);
		if(ind[u] == 0)
			printf("Noic\n");
		else
			printf("%d\n", ind[u]);
	}
	return 0;
}
