// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel's Graph
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2768

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int dist[101][101][101];


main()
{
	int N, M, Q, u, v, t, w, T = 0;
	while(scanf("%d %d", &N, &M) == 2)
	{
			for(int i = 0; i <= N; i++)
				for(int j = 0; j <= N; j++)
					dist[i][j][0] = (i == j) ? 0 : INF;
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			dist[u][v][0] = min(dist[u][v][0], w);
			dist[v][u][0] = min(dist[v][u][0], w);
		}
		for(int k = 1; k <= N; k++)
			for(int i = 1; i <= N; i++)
				for(int j = 1; j <= N; j++)
				{
					//printf("%d %d %d\n", i, j, k);
					//printf("%d %d\n", dist[i][j][k-1], dist[i][k][k-1]+dist[k][j][k-1]);
					dist[i][j][k] = min(dist[i][j][k-1], dist[i][k][k-1]+dist[k][j][k-1]);
				}
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d %d %d", &u, &v, &t);
			if(dist[u][v][t] != INF)
				printf("%d\n", dist[u][v][t]);
			else
				printf("-1\n");
		}
	}
	return 0;
}
