// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Agent 004
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1806

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

vector< vector<int> > Gb(10001), Gc(10001);
vector< vector<int> > Wb(10001), Wc(10001);
vector<int> db(10001), dc(10001);
int N;

int dijkstra(vector< vector<int> >& G, vector< vector<int> >& W, vector<int>& dist, int st)
{
	int ma = 0, me = INF;
	priority_queue< pair<int, int> > pq;
	int u, v, w;
	for(int i = 1; i <= N; i++)
		dist[i] = INF;
	dist[st] = 0;
	pq.push(make_pair(0, st));
	while(!pq.empty())
	{
		w = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(w > dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u] + W[u][i])
			{
				dist[v] = dist[u] + W[u][i];
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
	for(int i = 1; i <= N; i++)
	{
		if(i == st)
			continue;
		me = min(me, dist[i]);
		ma = max(ma, dist[i]);
	}
	return ma-me;
}

int criminosos[10001];

main()
{
	int M1, M2, Nc, u, v, w, st, en, cont = 0;
	scanf("%d %d %d %d", &N, &M1, &M2, &Nc);
	while(M1--)
	{
		scanf("%d %d %d", &u, &v, &w);
		Gb[u].push_back(v);
		Wb[u].push_back(w);
		Gb[v].push_back(u);
		Wb[v].push_back(w);
		Gc[u].push_back(v);
		Wc[u].push_back(w);
		Gc[v].push_back(u);
		Wc[v].push_back(w);
	}
	while(M2--)
	{
		scanf("%d %d %d", &u, &v, &w);
		Gc[u].push_back(v);
		Wc[u].push_back(w);
		Gc[v].push_back(u);
		Wc[v].push_back(w);
	}
	for(int i = 0; i < Nc; i++)
		scanf("%d", &criminosos[i]);
	scanf("%d %d", &st, &en);
	dijkstra(Gb, Wb, db, en);
	dijkstra(Gc, Wc, dc, en);
	for(int i = 0; i < Nc; i++)
		cont += (dc[criminosos[i]] <= db[st]);
	printf("%d\n", cont);
	return 0;
}
