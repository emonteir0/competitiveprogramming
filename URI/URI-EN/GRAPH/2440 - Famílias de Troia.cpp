// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Famílias de Troia
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2440

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(50001);
int vis[50001];

int dfs(int x)
{
	int i;
	vis[x] = 1;
	for(i = 0; i < G[x].size(); i++)
		if(!vis[G[x][i]])
			dfs(G[x][i]);
}


main()
{
	int i, N, M, x, y, cont = 0;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d", &x, &y);
		G[x].push_back(y);
		G[y].push_back(x);
	}
	for(i = 1; i <= N; i++)
		if(!vis[i])
		{
			cont++;
			dfs(i);
		}
	printf("%d\n", cont);
	return 0;
}

