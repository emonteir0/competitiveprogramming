// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Telecom Company
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1539

#include<bits/stdc++.h>
#define INF 1e50

using namespace std;

double dist[101][101];

int X[101], Y[101], R[101];

int sqr(int x)
{
	return x*x;
}

main()
{
	int N, x, y, Q, d;
	double sqd;
	while(scanf("%d", &N) == 1 && N)
	{
		for(int i = 1; i <= N; i++)
		{
			scanf("%d %d %d", &X[i], &Y[i], &R[i]);
			dist[i][i] = 0;
			for(int j = 1; j < i; j++)
			{
				d = sqr(X[i]-X[j]) + sqr(Y[i]-Y[j]);
				sqd = sqrt(d);
				dist[i][j] = (d <= R[i]*R[i]) ? sqd : INF;
				dist[j][i] = (d <= R[j]*R[j]) ? sqd : INF;
			}
		}
		for(int k = 1; k <= N; k++)
			for(int i = 1; i <= N; i++)
				for(int j = 1; j <= N; j++)
					dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d %d", &x, &y);
			if(dist[x][y] > 1e40)
				printf("-1\n");
			else
				printf("%.0lf\n", floor(dist[x][y]));
		}
	}
	return 0;
}
