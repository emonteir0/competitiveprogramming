// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Popularity in the Facebook
// Level: 8
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1462

#include<bits/stdc++.h>
#define ll long long
#define MAX 100000
#define INF 100000000000

using namespace std;

int A[MAX+1];
ll acum[MAX+1];

int main()
{
	int i, k, x, N;
	ll s;
	while(scanf("%d", &N) == 1)
	{
		s = 0;
		for(i = 1; i <= N; i++)
			scanf("%d", &A[i]);
		sort(A+1, A+N+1);
		for(i = 1; i <= N; i++)
			acum[i] = acum[i-1] + A[i];
		if(acum[N] & 1)
			printf("impossivel\n");
		else
		{
			s = 0;
			for(i = N; i >= 1; i--)
			{	
				k = N + 1 - i;
				s += A[i];
				x = upper_bound(A+1, A+N-k+1, k) - A;
				//printf("%d\n", x);
				//printf("%lld <= %lld + %lld + %lld\n", s, (k - 1LL) * k, k * (N - k + 1LL - x), acum[x-1]);
				if(s > (k - 1LL) * k + k * (N-k+1LL - x) + acum[x-1])
					break;
			}
			printf("%s\n", (i == 0) ? "possivel" : "impossivel");
		}
	}
	return 0;
}
