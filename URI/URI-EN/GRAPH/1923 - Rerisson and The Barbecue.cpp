// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rerisson and The Barbecue
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1923

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int dist[1001];

vector<int> G[1001];
map<int, string> Mapa;
map<string, int> Indices;
set<string> convidados;

main()
{
	int N, M, k = 0, ind, u, v;
	char str[21], str2[21];
	scanf("%d %d", &N, &M);
	while(N--)
	{
		scanf("%s %s", str, str2);
		if(Indices.count(str) <= 0)
		{
			Indices[str] = k;
			Mapa[k++] = str;
		}
		if(Indices.count(str2) <= 0)
		{
			Indices[str2] = k;
			Mapa[k++] = str2;
		}
		G[Indices[str]].push_back(Indices[str2]);
		G[Indices[str2]].push_back(Indices[str]);
	}
	queue<int> fila;
	if(Indices.count("Rerisson") <= 0)
		printf("0\n");
	else
	{
		ind = Indices["Rerisson"];
		for(int i = 0; i < k; i++)
			dist[i] = INF;
		dist[ind] = 0;
		fila.push(ind);
		while(!fila.empty())
		{
			u = fila.front();
			fila.pop();
			for(int i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				if(dist[v] == INF)
				{
					dist[v] = dist[u]+1;
					fila.push(v);
				}
			}
		}
		for(int i = 0; i < k; i++)
		{
			if(i == ind)
				continue;
			if(dist[i] <= M)
				convidados.insert(Mapa[i]);
		}
		printf("%d\n", convidados.size());
		for(set<string>::iterator it = convidados.begin(); it != convidados.end(); it++)
			printf("%s\n", it->c_str());
	}
	return 0;
}
