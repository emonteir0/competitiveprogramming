// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Manyfile
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2545

#include<bits/stdc++.h>

using namespace std;

int N;
int mat[1001][1001];
int depend[1001];
queue<int> sources;


main()
{
	int i, j, k, x, M;
	while(scanf("%d", &N) == 1)
	{
		k = 0;
		for(i = 1; i <= N; i++)
		{
			depend[i] = 0;
			for(j = 1; j <= N; j++)
				mat[i][j] = 0;
		}
		for(i = 1; i <= N; i++)
		{
			scanf("%d", &M);
			if(M)
			{
				depend[i] += M;
				while(M--)
				{
					scanf("%d", &x);
					mat[x][i] = 1;
				}
			}
			else
				sources.push(i);
		}
		while(!sources.empty())
		{
			k++;
			while(!sources.empty())
			{
				x = sources.front();
				sources.pop();
				depend[x]--;
				for(i = 1; i <= N; i++)
					if(mat[x][i])
					{
						depend[i]--;
						mat[x][i] = 0;
					}
			}
			for(i = 1; i <= N; i++)
				if(depend[i] == 0)
					sources.push(i);
		}
		for(i = 1; i <= N; i++)
			if(depend[i] > 0)
				break;
		printf("%d\n", i == N+1 ? k : -1);
	}
	return 0;
}
