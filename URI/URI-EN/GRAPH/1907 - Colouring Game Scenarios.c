// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colouring Game Scenarios
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1907

#include<stdio.h>

int N, M;
char mat[1024][1024];
int dx[]={1,-1,0,0};
int dy[]={0,0,1,-1};


void dfs(int i, int j)
{
	int x, y, k;
	for(k=0;k<4;k++)
	{
		x = i+dx[k];
		y = j+dy[k];
		if(x>=0 && x<N && y>=0 && y<M && mat[x][y] == '.')
		{
			mat[x][y]='o';
			dfs(x,y);
		}
	}
}

main()
{
	int i, j, cont;
	scanf("%d %d%*c",&N,&M);
	for(i=0;i<N;i++)
	{
		for(j=0;j<M;j++)
			scanf("%c",&mat[i][j]);
		scanf("%*c");
	}
	for(i=0;i<N;i++)
	{
		for(j=0;j<M;j++)
		{
			if(mat[i][j]=='.')
			{
				mat[i][j]='o';
				cont++;
				dfs(i,j);
			}
		}
	}
	printf("%d\n",cont);
	return 0;
}

