// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cairo's Market
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2073

#include<bits/stdc++.h>
#define MAXN 4005

using namespace std;

int maX, maY, meX, meY;
int tree[MAXN][MAXN];

int sum_y(int vx, int vy, int tly, int try_, int ly, int ry) {
    if (ly > ry) 
        return 0;
    if (ly == tly && try_ == ry)
        return tree[vx][vy];
    int tmy = (tly + try_) / 2;
    return max(sum_y(vx, vy*2, tly, tmy, ly, min(ry, tmy)),
          sum_y(vx, vy*2+1, tmy+1, try_, max(ly, tmy+1), ry));
}

int sum_x(int vx, int tlx, int trx, int lx, int rx, int ly, int ry) {
    if (lx > rx)
        return 0;
    if (lx == tlx && trx == rx)
        return sum_y(vx, 1, meY, maY, ly, ry);
    int tmx = (tlx + trx) / 2;
    return max(sum_x(vx*2, tlx, tmx, lx, min(rx, tmx), ly, ry), 
	sum_x(vx*2+1, tmx+1, trx, max(lx, tmx+1), rx, ly, ry));
}

void update_y(int vx, int lx, int rx, int vy, int ly, int ry, int x, int y, int new_val) {
    if (ly == ry) {
        if (lx == rx)
            tree[vx][vy] = new_val;
        else
            tree[vx][vy] = max(tree[vx*2][vy], tree[vx*2+1][vy]);
    } else {
        int my = (ly + ry) / 2;
        if (y <= my)
            update_y(vx, lx, rx, vy*2, ly, my, x, y, new_val);
        else
            update_y(vx, lx, rx, vy*2+1, my+1, ry, x, y, new_val);
        tree[vx][vy] = max(tree[vx][vy*2], tree[vx][vy*2+1]);
    }
}

void update_x(int vx, int lx, int rx, int x, int y, int new_val) {
    if (lx != rx) {
        int mx = (lx + rx) / 2;
        if (x <= mx)
            update_x(vx*2, lx, mx, x, y, new_val);
        else
            update_x(vx*2+1, mx+1, rx, x, y, new_val);
    }
    update_y(vx, lx, rx, 1, meY, maY, x, y, new_val);
}


int x[100001], y[100001];

int main()
{
	int T, N, A;
	scanf("%d", &T);
	while(T--)
	{
		maX = maY = 0;
		meX = meY = 1001;
		for(int i = 0; i < MAXN; i++)
			for(int j = 0; j < MAXN; j++)
				tree[i][j] = 0;
		scanf("%d", &N);
		for(int i = 0; i < N; i++)
		{
			scanf("%d %d", &x[i], &y[i]);
			maX = max(maX, x[i]);
			meX = min(meX, x[i]);
			maY = max(maY, y[i]);
			meY = min(meY, y[i]);
		}
		for(int i = N-1; i >= 0; i--)
		{
			A = sum_x(1, meX, maX, x[i], maX, y[i], maY);
			update_x(1, meX, maX, x[i], y[i], A+1);
		}
		printf("%d\n", tree[1][1]);
	}
	return 0;
}
