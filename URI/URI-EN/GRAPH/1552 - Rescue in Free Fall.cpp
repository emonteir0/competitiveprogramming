// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rescue in Free Fall
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1552

#include <bits/stdc++.h>
#define MAXN 200002
 
using namespace std;
 
struct edge 
{
	int u, v;
	double w;
};

struct pontos
{
	int x, y;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}

int N, M;
double total;
double p[MAXN], l[MAXN];
edge e[MAXN];
pontos vet[501];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v;
	double w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}

double sqr(double x)
{
	return x*x;
}
 
int main() 
{
	int i, j, T;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
        for (i = 0; i < N; i++)
		{
			scanf("%d %d", &vet[i].x, &vet[i].y);
			p[i] = i;
			l[i] = 1;
		}
		M = 0;
 		total = 0;
        for(i = 0; i < N; i++)
        {
        	for(j = i+1; j < N; j++)
        	{
        		e[M].u = i;
        		e[M].v = j;
        		e[M].w = sqrt(sqr(vet[i].x-vet[j].x)+sqr(vet[i].y-vet[j].y));
        		M++;
        	}
        }
        sort(e, e+M, cmp);
    	kruskal();
 		printf("%.2lf\n", total/100);
    }
}
