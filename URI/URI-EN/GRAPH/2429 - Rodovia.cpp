// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rodovia
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2429

#include<bits/stdc++.h>

using namespace std;


char vis[10001], vis2[10001];
vector< vector<int> > G(10001), G2(10001);
vector<int> indices;

void dfs(int x)
{
	int i;
	vis[x] = 1;
	for(i = 0; i < G[x].size(); i++)
		if(!vis[G[x][i]])
			dfs(G[x][i]);
	indices.push_back(x);
}

void dfs2(int x)
{
	int i;
	vis2[x] = 1;
	for(i = 0; i < G2[x].size(); i++)
		if(!vis2[G2[x][i]])
			dfs2(G2[x][i]);
}

main()
{
	int i, N, x, y, ans = 0;
	char b;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d %d", &x, &y);
		G[x].push_back(y);
		G2[y].push_back(x);
	}
	for(i = 1; i <= N; i++)
		if(!vis[i])
			dfs(i);
	for(i = N-1; i >= 0; i--)
	{
		if(!vis2[indices[i]])
		{
			ans++;
			dfs2(indices[i]);
		}	
	}
	printf("%s\n", (ans == 1) ? "S" : "N");
	return 0;
}
