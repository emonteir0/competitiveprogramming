// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Nemesis
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1972

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;


set< pair<int, pair<int, int> > > fila, fila2;
pair<int, pair<int, int> > p;
char mat[501][501];
bool visitado[501][501];
int N, M;
int distancia[501][501];
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};

bool inRange(int a, int b)
{
	return a >= 1 && a <= N && b >= 1 && b <= M;
}

int value(char c)
{
	if (c == '#')
		return INFINITO;
	if (c == '.' || c == 'E' || c == 'H')
		return 0;
	return c - '0';
}

void dijkstra(int xh, int yh)
{
	int a, x, y, x2, y2, i, j, v;
	pair<int, int> pos;
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= M; j++)
		{
			visitado[i][j] = 0;
			distancia[i][j] = INFINITO;
		}
	}
	distancia[xh][yh] = 0;
	fila.insert(make_pair(0,make_pair(xh,yh)));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		pos = p.second;
		x = pos.first;
		y = pos.second;
		if(!visitado[x][y])
		{
			visitado[x][y] = true;
			for(i = 0; i < 4; i++)
			{
				x2 = x+dx[i];
				y2 = y+dy[i];
				if(inRange(x2,y2))
				{
					v = value(mat[x2][y2]);
					if(!visitado[x2][y2] && (v + a < distancia[x2][y2]) && v != INFINITO)
						fila.insert(make_pair((distancia[x2][y2]=v+a),make_pair(x2,y2)));
				}
			}
		}
	}
}

int main()
{
	int xh, yh, xe, ye, i, j;
	scanf("%d %d%*c",&N,&M);
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= M; j++)
		{
			scanf("%c",&mat[i][j]);
			if(mat[i][j] == 'H')
			{
				xh = i;
				yh = j;
			}
			if(mat[i][j] == 'E')
			{
				xe = i;
				ye = j;
			}
		}
		scanf("%*c");
	}
	dijkstra(xh,yh);
	if(distancia[xe][ye] == INFINITO)
		printf("ARTSKJID\n");
	else
		printf("%d\n",distancia[xe][ye]);	
	return 0;
}

