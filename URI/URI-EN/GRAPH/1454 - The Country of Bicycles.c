// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Country of Bicycles
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1454

#include<stdio.h>
#define INFINITO 2147483647

int min(int a, int b)
{
	return (a < b) ? a : b;
}

int max(int a, int b)
{
	return (a > b) ? a : b;
}

int mat[101][101];


int main()
{
	int N, M, i, j, x, y, h, k, K = 1;
	while(scanf("%d %d",&N,&M) == 2 && (N||M))
	{
		printf("Instancia %d\n", K++);
		for(i = 1; i <= N; i++)
			for(j = 1; j<=N; j++)
			{
				mat[i][j] = INFINITO;
			}
		for(i = 1; i <= M; i++)
		{
			scanf("%d %d %d", &x, &y, &h);
			mat[x][y] = mat[y][x] = h;
		}
		for(k = 1; k <= N; k++)
		{
			for(i = 1; i <= N; i++)
			{
				if( i != k && mat[i][k] < INFINITO )
				{
					for(j = 1; j <= N; j++)
						mat[i][j] = min(mat[i][j], max(mat[i][k], mat[k][j]));
				}
			}
		}
		for(i = 1; i <= N; i++)
			mat[i][i] = 0;
		scanf("%d", &k);
		while(k--)
		{
			scanf("%d %d", &x,&y);
			printf("%d\n", mat[x][y]);
		}
		printf("\n");
	}
	return 0;
}

