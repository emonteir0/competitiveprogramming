// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factors and Multiples
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1056

#include<bits/stdc++.h>

using namespace std;

int N, M;
int G[100][100];
int match[100], vis[100], x[100], y[100];
  
int bpm(int u)
{
    for (int v = 0; v < N; v++)
    {
        if (G[u][v] && !vis[v])
        {
            vis[v] = 1;
            if (match[v] < 0 || bpm(match[v]))
            {
                match[v] = u;
                return 1;
            }
        }
    }
    return 0;
}

int maxBPM()
{
	int i, j, ans = 0;
	for(i = 0; i < N; i++)
   		match[i] = -1;
    for(i = 0; i < M; i++)
    {
        for(j = 0; j < N; j++)
        	vis[j] = 0;
        ans += bpm(i);
    }
    return ans;
}

int main()
{
	int i, j, t, T;
	scanf("%d", &T);
	for(t = 1; t <= T; t++)
	{
		scanf("%d", &M);
		for(i = 0; i < M; i++)
			scanf("%d", &x[i]);
		scanf("%d", &N);
		for(j = 0; j < N; j++)
		{
			scanf("%d", &y[j]);
			for(i = 0; i < M; i++)
			{
				if(x[i])
					G[i][j] = y[j] % x[i] == 0;
				else
					G[i][j] = y[j] == 0;
			}
		}
		printf("Case %d: %d\n", t, maxBPM());
	}
    return 0;
}
