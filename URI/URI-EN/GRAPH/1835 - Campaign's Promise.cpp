// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Campaign's Promise
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1835

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > adj(10001);
char vis[10001];

void dfs(int x)
{
	int i, j;
	vis[x] = 1;
	for(i = 0 ; i < adj[x].size(); i++)
	{
		j = adj[x][i];
		if(!vis[j])
			dfs(j);
	}
}

main()
{
	int i, T, k, N, M, x, y, cont;
	scanf("%d", &T);
	for(k = 1; k <= T; k++)
	{
		scanf("%d %d", &N, &M);
		for(i = 1; i <= N; i++)
		{
			adj[i].clear();
			vis[i] = 0;
		}
		while(M--)
		{
			scanf("%d %d", &x, &y);
			adj[x].push_back(y);
			adj[y].push_back(x);
		}
		cont = -1;
		for(i = 1; i <= N; i++)
		{
			if(!vis[i])
			{
				cont++;
				dfs(i);
			}
		}
		printf("Caso #%d: ", k);
		if(cont == 0)
			printf("a promessa foi cumprida\n");
		else
			printf("ainda falta(m) %d estrada(s)\n", cont);
	}
	return 0;
}
