// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel's Palindrome
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2816

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007

using namespace std;

int N;

char str[100001];
int vis[100001];
vector<int> G[100001];


char op(char a, char b)
{
	if(a == '!' || b == '!')
		return '!';
	if(a == '*')
		return b;
	if(b == '*')
		return a;
	if(a == b)
		return a;
	else
		return '!';
}

char dfs(int x)
{
	vis[x] = 1;
	char c = str[x];
	for(int i = 0; i < G[x].size(); i++)
	{
		if(!vis[G[x][i]])
			c = op(c, dfs(G[x][i]));
	}
	return c;
}

int solve(int i)
{
	char p = dfs(i);
	if(p == '*')
		return 26;
	if(p == '!')
		return 0;
	return 1;
}

main()
{
	int M, u, v;
	ll ans = 1;
	scanf("%d %d", &N, &M);
	scanf("%s", str);
	for(int i = 0; i < N; i++)
		G[i].push_back(N-1-i);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		u--, v--;
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for(int i = 0; i < N; i++)
	{
		if(!vis[i])
			ans *= solve(i);
		if(ans >= MOD)
			ans %= MOD;
	}
	printf("%lld\n", ans%MOD);
	return 0;
}
