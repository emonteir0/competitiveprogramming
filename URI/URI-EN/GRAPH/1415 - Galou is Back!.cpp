// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Galou is Back!
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1415

#include<stdio.h>

char mat[100][101];
char vis[100][100];

int N, M;
int dx[] = {-1, -1, 0, 0, 1, 1}, dy[] = {0, 1, -1, 1, -1, 0};

void dfs(int x, int y, int flag)
{
	int i, x2, y2;
	if(vis[x][y] & flag)
		return;
	vis[x][y] |= flag;
	flag = (flag == 1) ? 2 : 1;
	for(i = 0; i < 6; i++)
	{
		x2 = x + dx[i];
		y2 = y + dy[i];
		if(x2 >= 0 && x2 < N && y2 >= 0 && y2 < M)
			if(mat[x2][y2] == 'I' || mat[x2][y2] == '*')
				dfs(x2, y2, flag);
	}
}

main()
{
	int i, j;
	while(scanf("%d %d%*c", &N, &M) == 2 && (N||M))
	{
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
				vis[i][j] = 0;
		for(i = 0; i < N; i++)
			scanf("%s", mat[i]);
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
				if(mat[i][j] == 'I')
					dfs(i, j, 1);
		printf("\n");
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
				if(mat[i][j] == '.')
					printf(".");
				else if(vis[i][j] == 0)
					printf("F");
				else if(vis[i][j] == 1)
					printf("(");
				else if(vis[i][j] == 2)
					printf(")");
				else
					printf("B");
			printf("\n");
		}
	}
	return 0;
}
