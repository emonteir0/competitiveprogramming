// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sensate
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2657

#include <bits/stdc++.h>
#define MAXN 10002

using namespace std;

int N, M, p[MAXN], l[MAXN], val[MAXN], habilidade[MAXN];
map<string, int> Mapa;
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
	int aux;
    if (l[u] < l[v])
	{
		aux = u;
		u = v;
		v = aux;
	}
    p[v] = u;
    l[u] += l[v];
	val[u] = max(val[u], val[v]);
}
 
int main() 
{
	int i, u, v, Q;
	char nome[11], nome2[11];
    scanf("%d %d %d%*c", &N, &M, &Q);
    for (i = 1; i <= N; i++)
	{
		scanf("%s %d%*c", nome, &habilidade[i]);
		p[i] = i;
		l[i] = 1;
		val[i] = habilidade[i];
		Mapa[nome] = i;
	}
    while(M--)
	{ 
        scanf("%s %s%*c",  nome, nome2);
        u = id(Mapa[nome]);
        v = id(Mapa[nome2]);
        if(u != v)
        	join(u, v);
    }
    while(Q--)
    {
    	scanf("%s%*c", nome);
    	i = Mapa[nome];
    	u = id(i);
    	printf("%s\n", (l[u] == 1) || (val[u] <= 5) || (habilidade[i] >= 5) ? "S" : "N");
    }
}
