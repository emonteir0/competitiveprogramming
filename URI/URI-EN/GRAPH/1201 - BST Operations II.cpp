// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: BST Operations II
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1201

#include<bits/stdc++.h>

using namespace std;

struct node
{
	int val;
	int adj[2];
	node()
	{
		val = 0;
		adj[0] = -1;
		adj[1] = -1;
	}	
};

node G[1000001];
int sz = 0, first;

void infixa(int p)
{
	if(G[p].adj[0] != -1)
		infixa(G[p].adj[0]);
	if(G[p].val != 0)
	{
		if(!first)
			printf(" ");
		printf("%d", G[p].val);
		first = 0;
	}
	if(G[p].adj[1] != -1)
		infixa(G[p].adj[1]);
}

void posfixa(int p)
{
	if(G[p].adj[0] != -1)
		posfixa(G[p].adj[0]);
	if(G[p].adj[1] != -1)
		posfixa(G[p].adj[1]);
	if(G[p].val != 0)
	{
		if(!first)
			printf(" ");
		printf("%d", G[p].val);
		first = 0;
	}
}

void prefixa(int p)
{
	if(G[p].val != 0)
	{
		if(!first)
			printf(" ");
		printf("%d", G[p].val);
		first = 0;
	}
	if(G[p].adj[0] != -1)
		prefixa(G[p].adj[0]);	
	if(G[p].adj[1] != -1)
		prefixa(G[p].adj[1]);
}

void addnode(int p, int x)
{
	if(x < G[p].val)
	{
		if(G[p].adj[0] == -1)
		{
			G[p].adj[0] = ++sz;
			G[sz] = node();
			G[sz].val = x;
		}
		else
			addnode(G[p].adj[0], x);
	}
	if(x > G[p].val)
	{
		if(G[p].adj[1] == -1)
		{
			G[p].adj[1] = ++sz;
			G[sz] = node();
			G[sz].val = x;
		}
		else
			addnode(G[p].adj[1], x);
	}
}

int pesquisanode(int p, int x)
{
	if(G[p].val == x)
		return 1;
	else if(x > G[p].val)
	{
		if(G[p].adj[1] == -1)
			return 0;
		return pesquisanode(G[p].adj[1], x);
	}
	else
	{
		if(G[p].adj[0] == -1)
			return 0;
		return pesquisanode(G[p].adj[0], x);
	}
}

void removenode(int pai, int p, int x)
{
	if(G[p].val == x)
	{
		if(G[p].adj[0] == -1 && G[p].adj[1] == -1)
		{
			if(x < G[pai].val)
				G[pai].adj[0] = -1;
			else
				G[pai].adj[1] = -1;
		}
		else if(G[p].adj[1] == -1)
		{
			if(x < G[pai].val)
				G[pai].adj[0] = G[p].adj[0];
			else
				G[pai].adj[1] = G[p].adj[0];
		}
		else if(G[p].adj[0] == -1)
		{
			if(x < G[pai].val)
				G[pai].adj[0] = G[p].adj[1];
			else
				G[pai].adj[1] = G[p].adj[1];
		}
		else
		{
			int p2 = G[p].adj[0], pai2 = p;
			while(G[p2].adj[1] != -1)
			{
				pai2 = p2;
				p2 = G[p2].adj[1];
			}
			if(pai2 == p)
			{
				G[p2].adj[1] = G[p].adj[1];
				if(x < G[pai].val)
					G[pai].adj[0] = p2;
				else
					G[pai].adj[1] = p2;
			}
			else
			{
				G[pai2].adj[1] = G[p2].adj[0];
				G[p2].adj[0] = G[p].adj[0];
				G[p2].adj[1] = G[p].adj[1];
				//G[pai].adj[0] ou G[pai].adj[1]
				if(x < G[pai].val)
					G[pai].adj[0] = p2;
				else
					G[pai].adj[1] = p2;
			}
		}
		return;
	}
	else if(x > G[p].val)
	{
		if(G[p].adj[1] == -1)
			return;
		removenode(p, G[p].adj[1], x);
	}
	else
	{
		if(G[p].adj[0] == -1)
			return;
		removenode(p, G[p].adj[0], x);
	}
}

string str;

main()
{
	int x;
	while(cin >> str)
	{
		if(str == "INFIXA")
		{
			first = 1;
			infixa(0);
			printf("\n");
		}
		else if(str == "POSFIXA")
		{
			first = 1;
			posfixa(0);
			printf("\n");
		}
		else if(str == "PREFIXA")
		{
			first = 1;
			prefixa(0);
			printf("\n");
		}
		else
		{
			cin >> x;
			if(str == "I")
				addnode(0, x);
			else if(str == "P")
				printf("%d %s\n", x, pesquisanode(0, x) ? "existe" : "nao existe");
			else
				removenode(-1, 0, x);
		}
	}
	return 0;
}
