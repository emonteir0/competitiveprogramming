// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Attacking Rooks
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1490

#include <bits/stdc++.h>

using namespace std;

const int MAXN = 101;
vector<int> adj[MAXN*MAXN];
vector<bool> used(MAXN*MAXN), visit(MAXN*MAXN);
int match[MAXN*MAXN], dist[MAXN*MAXN];
char mat[MAXN][MAXN];
int x[MAXN][MAXN], y[MAXN][MAXN];

void bfs(int n1, int n2) 
{
  	std::fill(dist, dist + n1, -1);
  	std::queue<int> q;
  	for (int u = 0; u < n1; u++)
	{
    	if (!used[u]) 
		{
	      	q.push(u);
	      	dist[u] = 0;
    	}
  	}
  	while (!q.empty()) 
	{
    	int u = q.front();
    	q.pop();
    	for (int j = 0; j < (int)adj[u].size(); j++)
		{
      		int v = match[adj[u][j]];
      		if (v >= 0 && dist[v] < 0)
			{
        		dist[v] = dist[u] + 1;
        		q.push(v);
      		}
    	}
  	}
}

bool dfs(int u) 
{
  	visit[u] = true;
  	for (int j = 0; j < (int)adj[u].size(); j++) 
	{
    	int v = match[adj[u][j]];
    	if (v < 0 || (!visit[v] && dist[v] == dist[u] + 1 && dfs(v)))
		{
      		match[adj[u][j]] = u;
      		used[u] = true;
      		return true;
    	}
  	}
  	return false;
}

int hopcroft_karp(int n1, int n2) 
{
	fill(match, match + n2, -1);
	fill(used.begin(), used.end(), false);
	int res = 0;
	for (;;) 
	{
    	bfs(n1, n2);
    	fill(visit.begin(), visit.end(), false);
    	int f = 0;
    	for (int u = 0; u < n1; u++) 
		{
      		if (!used[u] && dfs(u)) 
			{
        		f++;
      		}
    	}
    	if (f == 0) 
		{
      		return res;
    	}
    	res += f;
  	}
  	return res;
}

int main() 
{
	int N, N1, N2;
	while(scanf("%d", &N) == 1)
	{
		N1 = 0;
		N2 = 0;
		for(int i = 0; i < N; i++)
			scanf("%s", mat[i]);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				if(mat[i][j] == 'X')
					continue;
				if(j == 0 || mat[i][j-1] == 'X')
					y[i][j] = N2++;
				else
					y[i][j] = y[i][j-1];
				if(i == 0 || mat[i-1][j] == 'X')
					x[i][j] = N1++;
				else
					x[i][j] = x[i-1][j];
				adj[x[i][j]].push_back(y[i][j]);
			}
		}
		printf("%d\n", hopcroft_karp(N1, N2));
		for(int i = 0; i < N1; i++)
			adj[i].clear();
	}
	return 0;
}
