// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Chaves
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1915

#include <bits/stdc++.h>
#define MAXN 101

using namespace std;

vector<int> adj[MAXN];
vector<bool> used(MAXN), visit(MAXN);
int match[MAXN], dist[MAXN];
int dp[MAXN][MAXN];

string pessoas[MAXN], presentes[MAXN];

int lcs(string &s1, string &s2)
{
	int N, M;
	N = s1.length();
	M = s2.length();
	for(int i = 0; i <= N; i++)
		for(int j = 0; j <= M; j++)
		{
			if((i == 0) || (j == 0))
				dp[i][j] = 0;
			else if(s1[i-1] == s2[j-1])
				dp[i][j] = dp[i-1][j-1] + 1;
			else
				dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
		}
	return (N+M-2*dp[N][M])%5 == 0;
}


void bfs(int n1, int n2) 
{
  	fill(dist, dist + n1, -1);
  	queue<int> q;
  	for (int u = 0; u < n1; u++)
	{
    	if (!used[u]) 
		{
	      	q.push(u);
	      	dist[u] = 0;
    	}
  	}
  	while (!q.empty()) 
	{
    	int u = q.front();
    	q.pop();
    	for (int j = 0; j < (int)adj[u].size(); j++)
		{
      		int v = match[adj[u][j]];
      		if (v >= 0 && dist[v] < 0)
			{
        		dist[v] = dist[u] + 1;
        		q.push(v);
      		}
    	}
  	}
}

bool dfs(int u) 
{
  	visit[u] = true;
  	for (int j = 0; j < (int)adj[u].size(); j++) 
	{
    	int v = match[adj[u][j]];
    	if (v < 0 || (!visit[v] && dist[v] == dist[u] + 1 && dfs(v)))
		{
      		match[adj[u][j]] = u;
      		used[u] = true;
      		return true;
    	}
  	}
  	return false;
}

int hopcroft_karp(int n1, int n2) 
{
	fill(match, match + n2, -1);
	fill(used.begin(), used.end(), false);
	int res = 0;
	for (;;) 
	{
    	bfs(n1, n2);
    	fill(visit.begin(), visit.end(), false);
    	int f = 0;
    	for (int u = 0; u < n1; u++) 
      		if (!used[u] && dfs(u)) 
        		f++;
    	if (f == 0) 
      		return res;
    	res += f;
  	}
  	return res;
}

char str[MAXN];
int mark[26];

int main()
{
	int N, M;
	char c;
	while((scanf("%d %d", &N, &M) == 2) && (N||M))
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%s", str);
			pessoas[i] = str;
			for(int j = 0; j < 26; j++)
				mark[j] = 0;
			for(int j = 0; j < pessoas[i].length(); j++)
			{
				c = pessoas[i][j]-'A';
				pessoas[i][j] = 26*mark[c]+c+1;
				mark[c]++;
			}
		}
		for(int i = 0; i < M; i++)
		{
			scanf("%s", str);
			presentes[i] = str;
			for(int j = 0; j < 26; j++)
				mark[j] = 0;
			for(int j = 0; j < presentes[i].length(); j++)
			{
				c = presentes[i][j]-'A';
				presentes[i][j] = 26*mark[c]+c+1;
				mark[c]++;
			}
		}
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				if(lcs(pessoas[i], presentes[j]))
					adj[i].push_back(j);
		printf("P = %.2lf\n", double(100*hopcroft_karp(N, M))/N);
		for(int i = 0; i < N; i++)
			adj[i].clear();
	}
	return 0;
}
