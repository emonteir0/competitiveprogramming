// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: BST Operations I
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1200

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct No
{
	char value;
	struct No *left;
	struct No *right;
} node;

int flag = 0;

void inserir(node **raiz, char value)
{
	if(*raiz == NULL)
	{
		*raiz = (node*) malloc(sizeof(node));
		(*raiz)->left = NULL;
		(*raiz)->right = NULL;
		(*raiz)->value = value;
	}
	else
	{
		if(value < (*raiz)->value)
			inserir(&((*raiz)->left), value);
		else
			inserir(&((*raiz)->right), value);
	}
}

void prefixa(node **raiz)
{
	if(*raiz != NULL)
	{
		if(!flag)
		{
			printf("%c", (*raiz)->value);
			flag = 1;
		}
		else
			printf(" %c", (*raiz)->value);
		prefixa(&((*raiz)->left));
		prefixa(&((*raiz)->right));
	}
}

void infixa(node **raiz)
{
	if(*raiz != NULL)
	{
		infixa(&((*raiz)->left));
		if(!flag)
		{
			printf("%c", (*raiz)->value);
			flag = 1;
		}
		else
			printf(" %c", (*raiz)->value);
		infixa(&((*raiz)->right));
	}
}

void posfixa(node **raiz)
{
	if(*raiz != NULL)
	{
		posfixa(&((*raiz)->left));
		posfixa(&((*raiz)->right));
		if(!flag)
		{
			printf("%c", (*raiz)->value);
			flag = 1;
		}
		else
			printf(" %c", (*raiz)->value);
	}
}

void buscar(node *raiz, char value)
{
	if(raiz == NULL)
		printf("%c nao existe\n", value);
	else if(raiz->value == value)
		printf("%c existe\n", value);
	else if(raiz->value > value)
		buscar(raiz->left, value);
	else
		buscar(raiz->right, value);
}

main()
{
	int N, M, x, i;
	char message[11];
	node* arvore;
	arvore = NULL;	
	while(gets(message) != NULL)
	{
		if(strlen(message)!=3)
		{
			flag = 0;
			if(message[1] == 'N')
			{
				infixa(&arvore);
				printf("\n");
			}
			else if(message[1] == 'R')
			{
				prefixa(&arvore);
				printf("\n");
			}
			else
			{
				posfixa(&arvore);
				printf("\n");
			}
		}
		else
		{
			if(message[0] == 'I')
				inserir(&arvore, message[2]);
			else
				buscar(arvore, message[2]);
		}
	}
	return 0;
}

