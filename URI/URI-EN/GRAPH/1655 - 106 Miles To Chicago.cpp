// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: 106 Miles To Chicago
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1655

#include<bits/stdc++.h>

using namespace std;

double M[101][101];

main()
{
	int N, Q, u, v, w;
	while(scanf("%d", &N) && N)
	{
		scanf("%d", &Q);
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
				M[i][j] = i == j;
		while(Q--)
		{
			scanf("%d %d %d", &u, &v, &w);
			M[u][v] = M[v][u] = w/100.0;
		}
		for(int k = 1; k <= N; k++)
			for(int i = 1; i <= N; i++)
				for(int j = 1; j <= N; j++)
					M[i][j] = max(M[i][j], M[i][k]*M[k][j]);
		printf("%.6lf percent\n", 100*M[1][N]);
	}
}
