// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Level Order Tree Traversal
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1466

#include<bits/stdc++.h>

using namespace std;

vector<int> ans;

struct node
{
    int data;
    struct node* left, *right;
};


void printGivenLevel(struct node* root, int level);
int height(struct node* node);
 

void printLevelOrder(struct node* root)
{
    int h = height(root);
    int i;
    for (i=1; i<=h; i++)
        printGivenLevel(root, i);
}
 

void printGivenLevel(struct node* root, int level)
{
    if (root == NULL)
        return;
    if (level == 1)
        ans.push_back(root->data);
    else if (level > 1)
    {
        printGivenLevel(root->left, level-1);
        printGivenLevel(root->right, level-1);
    }
}
 
int height(struct node* node)
{
    if (node==NULL)
        return 0;
    else
    {
        int lheight = height(node->left);
        int rheight = height(node->right);
 
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}
 
void inserir(node **raiz, int value)
{
	if(*raiz == NULL)
	{
		*raiz = (node*) malloc(sizeof(node));
		(*raiz)->left = NULL;
		(*raiz)->right = NULL;
		(*raiz)->data = value;
	}
	else
	{
		if(value < (*raiz)->data)
			inserir(&((*raiz)->left), value);
		else
			inserir(&((*raiz)->right), value);
	}
}

int main()
{
	int T, N, i, j, x;
    node *arvore;
 	scanf("%d", &T);
 	for(i = 1; i <= T; i++)
 	{
 		arvore = NULL;
 		ans.clear();
 		scanf("%d", &N);
 		while(N--)
 		{
 			scanf("%d", &x);
 			inserir(&arvore, x);
 		}
 		printLevelOrder(arvore);
 		printf("Case %d:\n%d", i, ans[0]);
 		for(j = 1; j < ans.size(); j++)
 			printf(" %d", ans[j]);
 		printf("\n\n");
 	}
    return 0;
}
