// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Analógimôn Types
// Level: 2
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2562

#include <bits/stdc++.h>
using namespace std;

#define vi vector <int>
#define vvi vector <vi>
#define pb push_back

vi vis;
vvi adj;
int ans;

void dfs(int u) {
	ans++;
	int sz = adj[u].size();
	for (int j = 0; j < sz; j++) {
		int v = adj[u][j];
		if (!vis[v]) {
			vis[v] = 1;
			dfs(v);
		}
	}
}

int main() {
	int n, m, u, v, e;
	while (cin >> n >> m) {
		ans = 0;
		adj = vvi(n+1);
		vis = vi(n+1);
		while (m--) {
			cin >> u >> v;
			adj[u].pb(v);
			adj[v].pb(u);
		}
		cin >> e;
		vis[e] = 1;
		dfs(e);
		cout << ans << endl;
	}
	return 0;
}
