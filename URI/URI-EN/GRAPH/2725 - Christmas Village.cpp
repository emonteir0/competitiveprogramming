// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Village
// Level: 1
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2725

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int X[1001], Y[1001], p[1001], l[1001];
int N, M, total;

struct edge 
{
	int u, v, w;
};
 
bool cmp(edge u, edge v)
{
	return u.w < v.w;
}

vector< edge > e;

int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            join(u, v);
            total += w;
			j++;
        }
    }
}

main()
{
	int i, j, T, K;
	edge aux;
	scanf("%d", &T);
	while(T--)
	{
		total = 0;
		e.clear();
		scanf("%d %d", &N, &K);
		for(i = 0; i < N; i++)
		{
			p[i] = i;
			l[i] = 1;
			scanf("%d %d", &X[i], &Y[i]);
			for(j = 0; j < i; j++)
			{
				aux.u = i;
				aux.v = j;
				aux.w = __gcd(abs(X[i]-X[j]), abs(Y[i]-Y[j])) - 1;
				e.push_back(aux);
			}
		}
		sort(e.begin(), e.end(), cmp);
		M = e.size();
		kruskal();
		printf("%lld\n", ((ll)total)*K);
	}
	return 0;
}
