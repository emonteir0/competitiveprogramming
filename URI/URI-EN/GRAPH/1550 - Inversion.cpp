// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Inversion
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1550

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x,cnt;
}tipo;

bitset<10000> bits,bits2;
queue<tipo> fila,fila2;

int inv(int x)
{
	int y=0;
	while(x>0)
	{
		y=10*y+(x%10);
		x/=10;
	}
	return y;
}

main()
{
	int a,b,N,cnt=0;
	tipo c,d;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %d",&a,&b);
		bits=bits2;
		fila=fila2;
		c.x=a;
		c.cnt=0;
		fila.push(c);
		bits[a]=1;
		while(!fila.empty())
		{
			c=fila.front();
			fila.pop();
			if(c.x==b)
				break;
			d.x=c.x+1;
			d.cnt=c.cnt+1;
			if((d.x<10000)&&(bits[d.x]==0))
			{
				bits[d.x]=1;
				fila.push(d);
			}
			d.x=inv(c.x);
			if((d.x<10000)&&bits[d.x]==0)
			{
				bits[d.x]=1;
				fila.push(d);
			}
		}
		printf("%d\n",c.cnt);
	}
	return 0;
}
