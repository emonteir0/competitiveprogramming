// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Uncle Tom’s Inherited Land
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1330

#include <bits/stdc++.h>

using namespace std;

const int MAXN = 101;
vector<int> adj[MAXN*MAXN];
vector<bool> used(MAXN*MAXN), visit(MAXN*MAXN);
int match[MAXN*MAXN], dist[MAXN*MAXN];
int mat[MAXN][MAXN], val[MAXN][MAXN];

void bfs(int n1, int n2) 
{
  	std::fill(dist, dist + n1, -1);
  	std::queue<int> q;
  	for (int u = 0; u < n1; u++)
	{
    	if (!used[u]) 
		{
	      	q.push(u);
	      	dist[u] = 0;
    	}
  	}
  	while (!q.empty()) 
	{
    	int u = q.front();
    	q.pop();
    	for (int j = 0; j < (int)adj[u].size(); j++)
		{
      		int v = match[adj[u][j]];
      		if (v >= 0 && dist[v] < 0)
			{
        		dist[v] = dist[u] + 1;
        		q.push(v);
      		}
    	}
  	}
}

bool dfs(int u) 
{
  	visit[u] = true;
  	for (int j = 0; j < (int)adj[u].size(); j++) 
	{
    	int v = match[adj[u][j]];
    	if (v < 0 || (!visit[v] && dist[v] == dist[u] + 1 && dfs(v)))
		{
      		match[adj[u][j]] = u;
      		used[u] = true;
      		return true;
    	}
  	}
  	return false;
}

int hopcroft_karp(int n1, int n2) 
{
	fill(match, match + n2, -1);
	fill(used.begin(), used.end(), false);
	int res = 0;
	for (;;) 
	{
    	bfs(n1, n2);
    	fill(visit.begin(), visit.end(), false);
    	int f = 0;
    	for (int u = 0; u < n1; u++) 
		{
      		if (!used[u] && dfs(u)) 
			{
        		f++;
      		}
    	}
    	if (f == 0) 
		{
      		return res;
    	}
    	res += f;
  	}
  	return res;
}

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int N, int y, int M)
{
	return x >= 1 && x <= N && y >= 1 && y <= M;
}

int main() 
{
	int N, M, K, N1, N2, ma, x, y;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		ma = (N*M+1)/2;
		N1 = 0;
		N2 = 0;
		scanf("%d", &K);
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= M; j++)
				mat[i][j] = 1;
		for(int i = 0; i < K; i++)
		{
			scanf("%d %d", &x, &y);
			mat[x][y] = 0;
		}	
		for(int i = 1; i <= N; i++)
		{
			for(int j = i&1 ? 1 : 2; j <= M; j += 2)
			{
				if(mat[i][j] == 1)
					val[i][j] = N1++;
				else
					val[i][j] = -1;
			}
			for(int j = i&1 ? 2 : 1; j <= M; j += 2)
			{
				if(mat[i][j] == 1)
					val[i][j] = N2++;
				else
					val[i][j] = -1;
			}
		}
		for(int i = 0; i < N1; i++)
			adj[i].clear();
		for(int i = 1; i <= N; i++)
		{
			for(int j = i&1; j <= M; j += 2)
			{
				if(val[i][j] != -1)
				{
					for(int k = 0; k < 4; k++)
					{
						if(inRange(i+dx[k], N, j+dy[k], M) && val[i+dx[k]][j+dy[k]] != -1)
						{
							adj[val[i][j]].push_back(val[i+dx[k]][j+dy[k]]);
						}
					}
				}
			}
		}
		printf("%d\n", hopcroft_karp(N1, N2));
	}
	return 0;
}
