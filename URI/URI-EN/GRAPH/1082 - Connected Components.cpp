// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Connected Components
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1082

#include<bits/stdc++.h>

using namespace std;

int mat[26][26];
int vis[26];

vector< set<int> > vet(26);
set<int> vazio;
set<int>::iterator it;

void dfs(int ind, int Q)
{
	int i;
	vet[Q].insert(ind);
	vis[ind] = 1;
	for(i = 0; i < 26; i++)
		if(mat[ind][i] && !vis[i])
			dfs(i, Q);
}

main()
{
	int k, T, i, j, N, M, L;
	char a, b;
	scanf("%d%*c", &T);
	for(k = 1; k <= T; k++)
	{
		L = 0;
		printf("Case #%d:\n", k);
		for(i = 0; i < 26; i++)
		{
			for(j = 0; j < 26; j++)
			{
				mat[i][j] = 0;
				vis[i] = 0;
			}
			vet[i] = vazio;
		}
		scanf("%d %d%*c", &N, &M);
		for(i = 0; i < M; i++)
		{
			scanf("%c %c%*c", &a, &b);
			mat[a-'a'][b-'a'] = mat[b-'a'][a-'a'] = 1; 
		}
		for(i = 0; i < N; i++)
		{
			if(!vis[i])
				dfs(i, L++);
		}
		for(i = 0; i < L; i++)
		{
			for(it = vet[i].begin(); it != vet[i].end(); ++it)
				printf("%c,", *it + 'a');
			printf("\n");
		}
		printf("%d connected components\n\n", L); 
	}
	return 0;
}

