// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Barrel Travel
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1757

#include <bits/stdc++.h>
#define _ ios_base::sync_with_stdio(0);
#define maxn 200002
 
using namespace std;

set<int> conjunto, conjunto2;

struct edge 
{
	int u, v;
};

int N, M, p[maxn], l[maxn];
edge e[maxn];
int adj[100][100];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
    if (l[u] < l[v])
		swap(u, v);
    p[v] = u;
	l[u] += l[v];
}
 
void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
        if (u != v)
		{
            join(u, v);
			j++;
        }
    }
}
 
int main() 
{
	int i, j, T, x, y;
	scanf("%d", &T);
	while(T--)
	{
	    scanf("%d %d", &N, &M);
	    for (i = 0; i < N; i++)
		{
			p[i] = i;
			l[i] = 1;
			for(j = 0; j < N; j++)
				adj[i][j] = 1;
			adj[i][i] = 0;
		}
	    while (M--)
		{ 
	        scanf("%d %d", &x, &y);
	        adj[x][y] = adj[y][x] = 0;
	    }
	    M = 0;
	    for(i = 0; i < N; i++)
	    {
	    	for(j = 0; j < N; j++)
	    	{
	    		if(adj[i][j])
	    		{
	    			e[M].u = i;
	    			e[M++].v = j;
	    		}
	    	}
	    }
		kruskal();
		conjunto = conjunto2;
		for(i = 0; i < N; i++)
			conjunto.insert(id(i));
		printf("%d\n", conjunto.size()-1);
	}
}
