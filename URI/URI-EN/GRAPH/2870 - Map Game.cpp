// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Map Game
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2870

#include<bits/stdc++.h>

using namespace std;

vector<int> G[100001];
int vis[100001];

int dfs(int x)
{
	int ans = 1;
	vis[x] = 1;
	for(int i = 0; i < G[x].size(); i++)
	{
		if(!vis[G[x][i]])
		{
			ans += dfs(G[x][i]);
		}
	}
	return ans;
}

main()
{
	int N, M, u, v, cnt = 0;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for(int i = 1; i <= N; i++)
		if(!vis[i])
			cnt += dfs(i)&1;
	printf("%s\n", cnt ? "Junior" : "Thiago");
	return 0;
}
