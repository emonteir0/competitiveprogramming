// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multicampus UFFS
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1908

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x,cnt;
} tipo;

queue<tipo> fila;
bitset<1001> bits;
bitset<10001> bits2;
map<int,set<int> > Maparotas, Mapapontos;
set<int> conjunto,conjunto2;
set<int>::iterator it,it2;

main()
{
	int a,b,i,j,x,y,n;
	char c;
	bool bolean=true;
	tipo struc1,struc2;
	scanf("%d %d",&a,&b);
	for(i=0;i<b;i++)
	{
		scanf("%d",&n);
		for(j=0;j<n;j++)
		{
			scanf("%d",&x);
			Maparotas[i].insert(x);
			Mapapontos[x].insert(i);
		}
	}
	struc1.x=1;
	struc1.cnt=1;
	fila.push(struc1);
	while(!fila.empty() && bolean)
	{
		struc1=fila.front();
		fila.pop();
		conjunto=Mapapontos[struc1.x];
		struc2.cnt=struc1.cnt+1;
		for(it=conjunto.begin();it!=conjunto.end();++it)
		{
			x=*it;
			if(bits[x]==0)
			{
				bits[x]=1;
				conjunto2=Maparotas[x];
				for(it2=conjunto2.begin();it2!=conjunto2.end();++it2)
				{
					y=*it2;
					if(y==a)
					{
						bolean = false;
						break;
					}
					if((Mapapontos[y].size()>1)&&(bits2[y]==0))
					{
						bits2[y]=1;
						struc2.x=y;
						fila.push(struc2);
					}
				}
			}
		}
	}
	printf("%d\n",struc1.cnt);
	return 0;
}
