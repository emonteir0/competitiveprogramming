// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Boss
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1469

#include<bits/stdc++.h>
#define INF 101
using namespace std;

int t;
int relacao[501];
int vis[501];
int val[501];
vector<int> G[501];

int dfs(int u)
{
	int v, idade = INF;
	vis[u] = t;
	for(int i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		if(vis[v] < t)
		{
			idade = min(idade, val[relacao[v]]);
			idade = min(idade, dfs(v));
		}
	}
	return idade;
}

main()
{
	int N, M, Q, u, v, x, y;
	char op;
	while(scanf("%d %d %d%*c", &N, &M, &Q) == 3)
	{
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			relacao[i] = i;
			scanf("%d%*c", &val[i]);
		}
		while(M--)
		{
			scanf("%d %d%*c", &u, &v);
			G[v].push_back(u);
		}
		while(Q--)
		{
			scanf("%c", &op);
			if(op == 'T')
			{
				scanf("%d %d%*c", &u, &v);
				for(x = 1; x <= N; x++)
					if(relacao[x] == u)
						break;
				for(y = 1; y <= N; y++)
					if(relacao[y] == v)
						break;
				swap(relacao[x], relacao[y]);
			}
			else
			{
				scanf("%d%*c", &u);
				t++;
				for(x = 1; x <= N; x++)
					if(relacao[x] == u)
						break;
				v = dfs(x);
				if(v == INF)
					printf("*\n");
				else
					printf("%d\n", v);
			}
		}
	}
	return 0;
}
