// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Search Tree
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1195

#include<stdio.h>
#include<stdlib.h>

typedef struct No
{
	int value;
	struct No *left;
	struct No *right;
} node;

void inserir(node **raiz, int value)
{
	if(*raiz == NULL)
	{
		*raiz = (node*) malloc(sizeof(node));
		(*raiz)->left = NULL;
		(*raiz)->right = NULL;
		(*raiz)->value = value;
	}
	else
	{
		if(value < (*raiz)->value)
			inserir(&((*raiz)->left), value);
		else
			inserir(&((*raiz)->right), value);
	}
}

void prefixa(node **raiz)
{
	if(*raiz != NULL)
	{
		printf(" %d", (*raiz)->value);
		prefixa(&((*raiz)->left));
		prefixa(&((*raiz)->right));
	}
}

void infixa(node **raiz)
{
	if(*raiz != NULL)
	{
		infixa(&((*raiz)->left));
		printf(" %d", (*raiz)->value);
		infixa(&((*raiz)->right));
	}
}

void posfixa(node **raiz)
{
	if(*raiz != NULL)
	{
		posfixa(&((*raiz)->left));
		posfixa(&((*raiz)->right));
		printf(" %d", (*raiz)->value);
	}
}

main()
{
	int N, M, x, i;
	node* arvore;
	scanf("%d", &N);
	for(i=1;i<=N;i++)
	{
		scanf("%d", &M);
		arvore = NULL;
		while(M--)
		{
			scanf("%d", &x);
			inserir(&arvore, x);
		}
		printf("Case %d:\nPre.:", i);
		prefixa(&arvore);
		printf("\nIn..:");
		infixa(&arvore);
		printf("\nPost:");
		posfixa(&arvore);
		printf("\n\n");
	}
	return 0;
}

