// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Costa
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2419

#include<stdio.h>

int N, M;
char mat[1000][1001];

int test(int x, int y)
{
	if(x == 0 || x == N-1 || y == 0 || y == M-1)
		return 1;
	return !((mat[x+1][y] == '#') && (mat[x-1][y] == '#') && (mat[x][y+1] == '#') && (mat[x][y-1] == '#'));
}


main()
{
	int i, j, cont = 0;
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			if(mat[i][j] == '#')
				cont += test(i, j);
	printf("%d\n", cont);
	return 0;
}

