// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Lizard's Icosahedrons
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2155

#include<bits/stdc++.h>
#define EPS 1e-6

using namespace std;

int X[7], Y[7];

double dp[7][128];

double dist(int i, int j)
{
	return sqrt((X[i]-X[j])*(X[i]-X[j]) + (Y[i]-Y[j])*(Y[i]-Y[j]));
}

double tsp(int x, int bit)
{
	double r = 1e50;
	if(bit == 127)
		return dp[x][bit] = dist(x, 3);
	if(dp[x][bit] != -1)
		return dp[x][bit];
	for(int i = 0; i < 7; i++)
		if(!(bit & (1 << i)))
			r = min(r, tsp(i, bit | (1 << i)) + dist(i, x));
	return dp[x][bit] = r;
}

main()
{
	int bit, T, u, x, y, p;
	double d;
	vector<int> ans;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		scanf("%d %d", &X[3], &Y[3]);
		for(int i = 0; i < 7; i++)
			for(int j = 0; j < 128; j++)
				dp[i][j] = -1;
		for(int i = 0; i < 6; i++)
		{
			scanf("%d %d %d", &u, &x, &y);
			X[u-1] = x;
			Y[u-1] = y;
		}
		bit = 8;
		p = 3;
		d = tsp(3, 8);
		ans.clear();
		while(bit != 127)
		{
			for(int i = 0; i < 7; i++)
				if(!(bit & (1 << i)) && abs(dp[p][bit] - (dp[i][bit | (1<<i)] + dist(p, i))) <= EPS)
				{
					ans.push_back(i+1);
					p = i;
					bit |= (1<<i);
					break;
				}
		}
		printf("Caso %d:\n4->", t);
		for(int i = 0; i < ans.size(); i++)
			printf("%d->", ans[i]);
		printf("4: %.5lf\n", d);
	}
	return 0;
}
