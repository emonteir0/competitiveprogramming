// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Violence in Syldavia
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2357

#include<bits/stdc++.h>

using namespace std;

int t;
int vis[100001];
vector<int> G[100001];

int dfs(int u, int pai)
{
	int v, ans = 1;
	if(vis[u] >= t)
		return 0;
	vis[u] = t;
	for(int i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		if(v == pai)
			continue;
		ans &= dfs(v, u);
	}
	return ans;
}

main()
{
	int N, M, x, y, bol;
	while(scanf("%d %d", &N, &M) == 2)
	{
		t++;
		bol = 1;
		for(int i = 1; i <= N; i++)
			G[i].clear();
		while(M--)
		{
			scanf("%d %d", &x, &y);
			if(x == y)
				bol = 0;
			G[x].push_back(y);
			G[y].push_back(x);
		}
		if(!bol)
			printf("Inseguro\n");
		else
		{
			for(int i = 1; i <= N; i++)
				if(vis[i] < t)
					bol &= dfs(i, 0);
			printf("%s\n", bol ? "Seguro" : "Inseguro");
		}
	}
	return 0;
}
