// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kids' Wishes
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1322

#include<bits/stdc++.h>

using namespace std;

int t, cont;
int X[1000001], Y[1000001];
set<int> conjunto;
vector<int> compressao;
int sz[2000001], con[2000001][2];
int vis[2000001];

int id(int X)
{
	return lower_bound(compressao.begin(), compressao.end(), X) - compressao.begin();
}

int dfs(int u)
{
	int v, r = sz[u] == 2;
	cont++;
	vis[u] = t;
	for(int i = 0; i < sz[u]; i++)
	{
		v = con[u][i];
		if(vis[v] < t)
			r &= dfs(v);
	}
	return r;
}

main()
{
	int x, y, N, M, stop;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		t++;
		stop = 0;
		conjunto.clear();
		for(int i = 0; i < M; i++)
		{
			scanf("%d %d", &X[i], &Y[i]);
			conjunto.insert(X[i]);
			conjunto.insert(Y[i]);
		}
		compressao.assign(conjunto.begin(), conjunto.end());
		for(int i = 0; i < compressao.size(); i++)
			sz[i] = 0;
		for(int i = 0; i < M; i++)
		{
			x = id(X[i]);
			y = id(Y[i]);
			if(sz[x] == 2)
			{
				if(y != con[x][0] && y != con[x][1])
				{
					stop = 1;
					printf("N\n");
					break;
				}
			}
			else
			{
				if(sz[x] == 1)
				{
					if(con[x][0] != y)
						con[x][sz[x]++] = y;
				}
				else
					con[x][sz[x]++] = y;
			}
			if(sz[y] == 2)
			{
				if(x != con[y][0] && x != con[y][1])
				{
					stop = 1;
					printf("N\n");
					break;
				}
			}
			else
			{
				if(sz[y] == 1)
				{
					if(con[y][0] != x)
						con[y][sz[y]++] = x;
				}
				else
					con[y][sz[y]++] = x;
			}
		}
		if(!stop)
		{
			for(int i = 0; i < compressao.size(); i++)
			{
				if(vis[i] < t)
				{
					cont = 0;
					if(dfs(i))
					{
						stop = 1;
						printf("%s\n", (cont == N) ? "Y" : "N");
						break;
					}
				}	
			}
			if(!stop)
				printf("Y\n");
		}
	}
	return 0;
}
