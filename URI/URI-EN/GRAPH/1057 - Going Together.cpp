// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Going Together
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1057

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

struct pt
{
	int x, y;
	pt()
	{
	}
	pt(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
	bool operator==(const pt pt2)
	{
		return x == pt2.x && y == pt2.y;
	}
};

struct node
{
	pt pta, ptb, ptc;
	node()
	{
	}
	node(pt pta, pt ptb, pt ptc)
	{
		this->pta = pta;
		this->ptb = ptb;
		this->ptc = ptc;
	}
};

int N;
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

char mat[11][11];
int dist[11][11][11][11][11][11];

int inRange(pt ptx)
{
	return ptx.x >= 0 && ptx.x < N && ptx.y >= 0 && ptx.y < N;
}

main()
{
	bool boola, boolb, boolc;
	int T, d;
	queue<node> fila;
	node nodeS;
	pt pta, pta2, ptb, ptb2, ptc, ptc2;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		scanf("%d", &N);
		for(int i = 0; i < N; i++)
			scanf("%s", mat[i]);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				if(mat[i][j] == 'A')
					pta = pt(i, j);
				if(mat[i][j] == 'B')
					ptb = pt(i, j);
				if(mat[i][j] == 'C')
					ptc = pt(i, j);
			}
		}
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				for(int k = 0; k < N; k++)
					for(int l = 0; l < N; l++)
						for(int m = 0; m < N; m++)
							for(int n = 0; n < N; n++)
								dist[i][j][k][l][m][n] = INF;
		dist[pta.x][pta.y][ptb.x][ptb.y][ptc.x][ptc.y] = 0;
		fila = queue<node>();
		fila.push(node(pta, ptb, ptc));
		while(!fila.empty())
		{
			nodeS = fila.front();
			pta = nodeS.pta;
			ptb = nodeS.ptb;
			ptc = nodeS.ptc;
			d = dist[pta.x][pta.y][ptb.x][ptb.y][ptc.x][ptc.y];
			if(mat[pta.x][pta.y] == 'X' && mat[ptb.x][ptb.y] == 'X' && mat[ptc.x][ptc.y] == 'X')
				break;
			fila.pop();
			for(int i = 0; i < 4; i++)
			{
				pta2 = pt(pta.x+dx[i], pta.y+dy[i]);
				ptb2 = pt(ptb.x+dx[i], ptb.y+dy[i]);
				ptc2 = pt(ptc.x+dx[i], ptc.y+dy[i]);
				boola = inRange(pta2);
				boolb = inRange(ptb2);
				boolc = inRange(ptc2);
				if(!boola || mat[pta2.x][pta2.y] == '#')
					pta2 = pta;
				if(!boolb || mat[ptb2.x][ptb2.y] == '#')
					ptb2 = ptb;
				if(!boolc || mat[ptc2.x][ptc2.y] == '#')
					ptc2 = ptc;
				while(1)
				{
					if(pta2 == ptb2)
					{
						if(pta2 == pta)
							ptb2 = ptb;
						else
							pta2 = pta;
						continue;
					}
					if(pta2 == ptc2)
					{
						if(pta2 == pta)
							ptc2 = ptc;
						else
							pta2 = pta;
						continue;
					}
					if(ptb2 == ptc2)
					{
						if(ptb2 == ptb)
							ptc2 = ptc;
						else
							ptb2 = ptb;
						continue;
					}
					break;
				}
				if(pta == pta2 && ptb == ptb2 && ptc == ptc2)
					continue;
				if(dist[pta2.x][pta2.y][ptb2.x][ptb2.y][ptc2.x][ptc2.y] == INF)
				{
					dist[pta2.x][pta2.y][ptb2.x][ptb2.y][ptc2.x][ptc2.y] = d + 1;
					fila.push(node(pta2, ptb2, ptc2));
				}
			}
		}
		if(fila.empty())
			printf("Case %d: trapped\n", t);
		else
			printf("Case %d: %d\n", t, dist[pta.x][pta.y][ptb.x][ptb.y][ptc.x][ptc.y]);
	}
	return 0;
}
