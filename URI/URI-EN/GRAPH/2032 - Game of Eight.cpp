// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of Eight
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2032

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

struct st
{
	int mat[3][3], zerox, zeroy;
	st(){}
	st(int a, int b, int c, int d, int e, int f, int g, int h, int i)
	{
		int mat2[3][3];
		mat2[0][0] = a;
		mat2[0][1] = b;
		mat2[0][2] = c;
		mat2[1][0] = d;
		mat2[1][1] = e;
		mat2[1][2] = f;
		mat2[2][0] = g;
		mat2[2][1] = h;
		mat2[2][2] = i;
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
			{
				this->mat[i][j] = mat2[i][j];
				if(mat2[i][j] == 0)
				{
					this->zerox = i;
					this->zeroy = j;
				}
			}
	}
	bool operator<(st rhs) const
    {
        for(int i = 0; i < 3; i++)
        	for(int j = 0; j < 3; j++)
        		if(mat[i][j] != rhs.mat[i][j])
        			return mat[i][j] < rhs.mat[i][j];
        return 0;
    }
    bool operator==(st rhs) const
    {
    	for(int i = 0; i < 3; i++)
    		for(int j = 0; j < 3; j++)
    			if(mat[i][j] != rhs.mat[i][j])
    				return 0;
    	return 1;
    }
    void matprint()
    {
    	printf("\n");
    	for(int i = 0; i < 3; i++)
    	{
    		for(int j = 0; j < 3; j++)
    			printf("%d", mat[i][j]);
    		printf("\n");
    	}
    }
};

int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, -1, 1};
st x, x2, inicial = st(1, 2, 3, 4, 5, 6, 7, 8, 0), final;

map<st, int> distancia;

int inRange(int x, int y)
{
	return x >= 0 && x < 3 && y >=0 && y < 3;
}

int change(st x, st &x2, int k)
{
	if(!inRange(x.zerox+dx[k], x.zeroy+dy[k]))
		return 0;
	x2 = x;
	x2.mat[x2.zerox][x2.zeroy] = x2.mat[x2.zerox+dx[k]][x2.zeroy+dy[k]];
	x2.zerox += dx[k];
	x2.zeroy += dy[k];
	x2.mat[x2.zerox][x2.zeroy] = 0;
	return 1;
}

main()
{
	int dist;
	string str, str2, str3;
	vector<st> ans;
	queue<st> fila;
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	while(cin >> str >> str2 >> str3)
	{
		final = st(str[0]-'0', str[1]-'0', str[2]-'0', str2[0]-'0', str2[1]-'0', str2[2]-'0', str3[0]-'0', str3[1]-'0', str3[2]-'0');
		fila = queue<st>();
		ans.clear();
		distancia.clear();
		fila.push(inicial);
		distancia[inicial] = 0;
		while(!fila.empty())
		{
			x = fila.front();
			fila.pop();
			if(x == final)
				break;
			for(int i = 0; i < 4; i++)
			{
				if(!change(x, x2, i))
					continue;
				if(distancia.count(x2) <= 0)
				{
					distancia[x2] = distancia[x]+1;
					fila.push(x2);
				}
			}
		}
		if(distancia.count(final) <= 0)
			printf("Problema sem solucao\n");
		else
		{
			printf("Quantidade minima de passos = %d", distancia[final]);
			if(distancia[final] == 0)
				printf("\n");
			x = final;
			while(distancia[x] != 0)
			{
				for(int k = 0; k < 4; k++)
				{
					if(!change(x, x2, k))
						continue;
					if(distancia.count(x2) <= 0)
						continue;
					if(distancia[x2] == distancia[x] - 1)
					{
						x = x2;
						ans.push_back(x);
						break;
					}
				}
			}
			for(int k = 0; k < ans.size(); k++)
				ans[k].matprint();
		}
	}
	return 0;
}
