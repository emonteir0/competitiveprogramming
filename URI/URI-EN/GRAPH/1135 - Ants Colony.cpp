// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ants Colony
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1135

#include<bits/stdc++.h>
#define MAXL 20
#define MAXN 100001

using namespace std;


int vet[MAXN], pai[MAXN], nivel[MAXN], ancestral[MAXN][MAXL];
long long altura[MAXN];
vector< vector<int> > G(MAXN), W(MAXN);


void dfs(int u)
{
	int i, v;
	for(i = 0; i < (int)G[u].size(); i++)
	{
		v = G[u][i];
		if(nivel[v] == -1)
		{
			pai[v] = u;
			nivel[v] = nivel[u] + 1;
			altura[v] = altura[u] + W[u][i];
			dfs(v);
		}
	}
}

int LCA(int u, int v)
{    
    if(nivel[u] < nivel[v]) swap(u, v);
    for(int i = MAXL-1;i >= 0;i--)
        if(nivel[u] - (1<<i) >= nivel[v])
            u = ancestral[u][i];
    if(u == v) return u; 
    for(int i = MAXL-1;i >= 0;i--)
        if((ancestral[u][i] != -1) && (ancestral[u][i] != ancestral[v][i])){
            u = ancestral[u][i];
            v = ancestral[v][i];
        }          
    return pai[u];
}

int main()
{
	int N, M, i, j, x, y;
	while(scanf("%d",&N) == 1 && N)
	{
		for(i=0; i < N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
		for(i=1; i < N; i++)
		{
			scanf("%d %d",&x, &y);
			G[x].push_back(i);
			W[x].push_back(y);
		}
		for(i=1; i<N; i++)
		{
			nivel[i] = -1;
			altura[i] = -1;
		}
		dfs(0);
		for (i = 0; i < N; i++)
          for (j = 1; j < MAXL; j++)
              ancestral[i][j] = -1;
		for(i = 0;i < N;i++)
			ancestral[i][0] = pai[i];
		for(j = 1;j < MAXL;j++)
			for(i = 0;i < N;i++)
				if(ancestral[i][j-1] != -1)
					ancestral[i][j] = ancestral[ancestral[i][j-1]][j-1];
		scanf("%d", &M);
		scanf("%d %d", &x, &y);
		printf("%lld",altura[x]+altura[y]-2*altura[LCA(x, y)]);
		while(--M)
		{
			scanf("%d %d", &x, &y);
			printf(" %lld",altura[x]+altura[y]-2*altura[LCA(x, y)]);
		}
		printf("\n");
	}
	return 0;
}

