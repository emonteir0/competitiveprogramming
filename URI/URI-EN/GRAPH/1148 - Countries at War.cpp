// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Countries at War
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1148

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;

set< pair<int,int> > fila;
pair<int,int> p;
int mat[501][501];
bool con[501][501], visitado[501];
int distancia[501];

void dijkstra(int x, int N)
{
	int a, b, i;
	for(i = 1; i <= N; i++)
	{
		visitado[i] = false;
		distancia[i] = INFINITO;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		b = p.second;
		if(!visitado[b])
		{
			visitado[b] = true;
			for(i = 1; i<= N; i++)
			{
				if(con[b][i])
				{
					if(!visitado[i] && mat[b][i] + a < distancia[i])
						fila.insert(make_pair((distancia[i]=mat[b][i]+a),i));
				}
			}
		}
	}
}

int main()
{
	int N, M, i, j, x, y, h, k;
	while(scanf("%d %d",&N,&M) == 2 && (N||M))
	{
		for(i = 1; i <= N; i++)
			for(j = 1; j<=N; j++)
			{
				con[i][j] = false;
				mat[i][j] = INFINITO;
			}
		for(i = 1; i <= M; i++)
		{
			scanf("%d %d %d", &x, &y, &h);
			con[x][y] = true;
			mat[x][y] = h;
			if(con[y][x])
				mat[x][y] = mat[y][x] = 0;
		}
		scanf("%d", &k);
		while(k--)
		{
			scanf("%d %d", &x,&y);
			dijkstra(x,N);
			if(distancia[y] == INFINITO)
				printf("Nao e possivel entregar a carta\n");
			else
				printf("%d\n", distancia[y]);
		}
		printf("\n");
	}
	return 0;
}

