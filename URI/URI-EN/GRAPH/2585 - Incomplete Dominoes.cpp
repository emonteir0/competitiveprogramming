// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Incomplete Dominoes
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2585

#include<bits/stdc++.h>

using namespace std;

main()
{
	int i, N, a, b, ma, cont = 1, vet[10], x;
	while(scanf("%d", &N) == 1)
	{
		ma = 1;
		for(i = 0; i < N; i++)
		{
			scanf("%d %d", &a, &b);
			vet[i] = 10*a + b;
		}
		sort(vet, vet+N);
		do
		{
			x = vet[0];
			cont = 1;
			for(i = 1; i < N; i++)
			{
				if(x%10 == vet[i]/10)
					cont++;
				else
				{
					ma = max(ma, cont);
					cont = 1;
				}
				x = vet[i];
			}
			ma = max(ma, cont);
		}while(next_permutation(vet, vet+N));
		printf("%d\n", ma);
	}
	return 0;
}
