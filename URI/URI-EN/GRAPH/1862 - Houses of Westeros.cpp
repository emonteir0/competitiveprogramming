// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Houses of Westeros
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1862

#include<bits/stdc++.h>

using namespace std;

map<string, set<int> > Mapa;
map<string, set<int> >::iterator it;
vector<int> ans;
string str;

int main()
{
	int i, j, c, N;
	char b = 1, linha[1001];
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%s", linha);
		str = linha;
		if(Mapa.find(str) == Mapa.end())
		{
			c = 0;
			for(j = 0; j < N; j++)
			{
				if(str[j] == 'S')
				{
					Mapa[str].insert(j);
					c++;
				}
			}
			ans.push_back(c);
		}
		if(Mapa[str].find(i) == Mapa[str].end())
			b = 0;
		else
			Mapa[str].erase(i);
	}
	for(it = Mapa.begin(); it != Mapa.end(); ++it)
	{
		b &= (it->second).size() == 0;
	}
	if(b)
	{
		sort(ans.begin(), ans.end(), greater<int>());
		printf("%d\n%d", ans.size(), ans[0]);
		for(i = 1; i < (int)ans.size(); i++)
			printf(" %d", ans[i]);
		printf("\n");
	}
	else
		printf("-1\n");
	return 0;
}
