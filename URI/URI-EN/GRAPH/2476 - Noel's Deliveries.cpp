// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Noel's Deliveries
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2476

#include<bits/stdc++.h>
#define MAXN 100000
#define MAXL 17
 
using namespace std;

struct query
{
	int L, R, A, i, BL;
};

int block, res;
int vis[MAXN+1];
int freq[MAXN+1];
int ans[MAXN+1];
query queries[MAXN+1];

vector< vector<int> > G(MAXN+1);
int A[MAXN+1], B[MAXN+MAXN+1];


int st[MAXN+1], en[MAXN+1];
int nivel[MAXN+1], ancestral[MAXN+1][MAXL];
int timer;
 
int k = 1;
unordered_map<string, int> Mapa;

bool cmp(query x, query y)
{
    if (x.BL != y.BL)
        return x.BL < y.BL;
    return x.R < y.R;
}
 
void dfs(int u)
{
	int i, v;
	B[++timer] = u;
	st[u] = timer;
	for(i = 1; i < MAXL; i++)
		ancestral[u][i] = ancestral[ancestral[u][i-1]][i-1];
	for(i = 0; i < (int)G[u].size(); i++)
	{
		v = G[u][i];
		if(nivel[v] == -1)
		{
			ancestral[v][0] = u;
			nivel[v] = nivel[u] + 1;
			dfs(v);
		}
	}
	B[++timer] = u;
	en[u] = timer;
}
 
int LCA(int u, int v)
{    
    if(nivel[u] < nivel[v]) 
		swap(u, v);
    for(int i = MAXL-1;i >= 0;i--)
        if(nivel[u] - (1<<i) >= nivel[v])
            u = ancestral[u][i];
    if(u == v)
		return u; 
    for(int i = MAXL-1;i >= 0;i--)
        if(ancestral[u][i] != ancestral[v][i])
		{
            u = ancestral[u][i];
            v = ancestral[v][i];
        }          
    return ancestral[u][0];
}

void check(int x)
{
	if ((vis[x]) && (--freq[A[x]] == 0)) res--; 
	else if ((!vis[x]) && (freq[A[x]]++ == 0)) res++;
	vis[x] = !vis[x];
}

void MO(int N, int M)
{
	int i, L, R, curL = queries[0].L, curR = queries[0].L-1;
	res = 0;
	
	sort(queries, queries+M, cmp);
	for(i = 0; i < M; i++)
	{
		L = queries[i].L, R = queries[i].R;
        
		while (curL < L) 
			check(B[curL++]);
		while (curL > L)
			check(B[--curL]);
		while (curR < R)
			check(B[++curR]);
		while (curR > R)
			check(B[curR--]);

        if(queries[i].A)
    		check(queries[i].A);
        	
        ans[queries[i].i] = res;
        
    	if(queries[i].A)
    		check(queries[i].A);
    	
	}
}

int main()
{
	int i, N, M, lca, u, v;
	char msg[21];
	scanf("%d %d%*c", &N, &M);
	for(i = 1; i <= N; i++)
	{
		nivel[i] = -1;
		scanf("%s", msg);
		if(Mapa.find(msg) == Mapa.end())
			Mapa[msg] = k++;
		A[i] = Mapa[msg];
	}
	for(i = 1; i < N; i++)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	nivel[1] = 1;
	dfs(1);
	block = (int)sqrt(N+N);
	for(i = 0; i < M; i++)
	{
		scanf("%d %d", &u, &v);
		queries[i].i = i;
		lca = LCA(u, v);
		if(st[u] > st[v])
			swap(u, v);
		if(lca == u)
		{
			queries[i].L = st[u];
			queries[i].R = st[v];
		}
		else
		{
			queries[i].L = en[u];
			queries[i].R = st[v];
			queries[i].A = lca;
		}
		queries[i].BL = queries[i].L/block;
	}
	MO(N, M);
	for(i = 0; i < M; i++)
		printf("%d\n", ans[i]);
	return 0;
}
