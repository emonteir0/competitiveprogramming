// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Expression Tree
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1463

#include<bits/stdc++.h>

using namespace std;

map<char, int> prec;
stack<char> opStack;
vector<char> postfixlist;
string str;
int ind;
int sz;
char topToken;
vector<char> niveis[101];

struct node
{
	char val;
	int adj[2];
	node()
	{
		val = 0;
		adj[0] = -1;
		adj[1] = -1;
	}
};

node tree[101];

int operand(char c)
{
	if(c >= 'a' && c <= 'z')
		return 1;
	if(c >= 'A' && c <= 'Z')
		return 1;
	if(c >= '0' && c <= '9')
		return 1;
	return 0;
}

void filltree(int x)
{
	char ch = postfixlist[ind];
	ind--;
	tree[x].val = ch;
	if(!operand(ch))
	{
		if(tree[x].adj[1] == -1)
		{
			tree[x].adj[1] = sz;
			tree[sz++] = node();
		}
		filltree(tree[x].adj[1]);
		if(tree[x].adj[0] == -1)
		{
			tree[x].adj[0] = sz;
			tree[sz++] = node();
		}
		filltree(tree[x].adj[0]);
	}
}

void dfs(int x, int level)
{
	niveis[level].push_back(tree[x].val);
	if(operand(tree[x].val))
		return;
	dfs(tree[x].adj[0], level+1);
	dfs(tree[x].adj[1], level+1);
}

main()
{
	prec['*'] = 3;
	prec['/'] = 3;
	prec['+'] = 2;
	prec['-'] = 2;
	prec['('] = 1;
	while(getline(cin, str))
	{
		if(!postfixlist.empty())
			printf("\n");
		postfixlist.clear();
		for(int i = 0; i < str.length(); i++)
		{
			if(str[i] == ' ')
				continue;
			else if(operand(str[i]))
				postfixlist.push_back(str[i]);
			else if(str[i] == '(')
				opStack.push(str[i]);
			else if(str[i] == ')')
			{
				topToken = opStack.top();
				opStack.pop();
				while(topToken != '(')
				{
					postfixlist.push_back(topToken);
					topToken = opStack.top();
					opStack.pop();
				}
			}
			else
			{
				while(!opStack.empty() && prec[opStack.top()] >= prec[str[i]])
				{
					postfixlist.push_back(opStack.top());
					opStack.pop();
				}
				opStack.push(str[i]);
			}
		}
		while(!opStack.empty())
		{
			postfixlist.push_back(opStack.top());
			opStack.pop();
		}
		sz = 1;
		ind = postfixlist.size()-1;
		tree[0] = node();
		filltree(0);
		for(int i = 0; i <= 100; i++)
			niveis[i].clear();
		dfs(0, 0);
		for(int k = 0; niveis[k].size(); k++)
		{
			printf("Nivel %d: ", k);
			for(int i = 0; i < niveis[k].size(); i++)
				printf("%c", niveis[k][i]);
			printf("\n");
		}
	}
	return 0;
}
