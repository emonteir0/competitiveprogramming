// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: I Can't be Late
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1977

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;


set< pair<int,int> > fila, fila2;
pair<int,int> p;
int mat[501][501];
bool con[501][501], visitado[501];
int distancia[501];
map<string, int> Mapa, Mapa2;

void dijkstra(int x, int N)
{
	int a, b, i;
	for(i = 1; i <= N; i++)
	{
		visitado[i] = false;
		distancia[i] = INFINITO;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		b = p.second;
		if(!visitado[b])
		{
			visitado[b] = true;
			for(i = 1; i<= N; i++)
			{
				if(con[b][i])
					if(!visitado[i] && mat[b][i] + a < distancia[i])
						fila.insert(make_pair((distancia[i]=mat[b][i]+a),i));
			}
		}
	}
}


int main()
{
	int min, N, M, x, k, a, b, tot, i, j;
	char vet[21], vet2[21];
	string str, str2;
	while(scanf("%d %d %d%*c", &min, &N, &M) == 3 && (N||M||min))
	{
		k = 1;
		for(i=1;i<=500;i++)
			for(j=1;j<=500;j++)
				con[i][j] = false;
		while(N--)
		{
			scanf("%s %s %d%*c",vet,vet2,&x);
			str = vet;
			str2 = vet2;
			if(Mapa[str] == 0)
				Mapa[str] = k++;
			if(Mapa[str2] == 0)
				Mapa[str2] = k++;
			a = Mapa[str];
			b = Mapa[str2];
			con[a][b] = true;
			mat[a][b] = x;
		}
		dijkstra(Mapa["varzea"], M);
		tot = distancia[Mapa["alto"]] + ((min > 30)? 50 : 30);
		printf("%02d:%02d\n%s\n",(17+tot/60)%24,tot%60,tot>60?"Ira se atrasar":"Nao ira se atrasar");
		Mapa = Mapa2;
	}
	return 0;
}

