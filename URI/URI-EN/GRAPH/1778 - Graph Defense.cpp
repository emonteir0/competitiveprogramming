// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Graph Defense
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1778

#include<bits/stdc++.h>
#define INFINITO 1000000000
 
using namespace std;
 
vector < int > val(1001), dist(1001), adj[1001];

int N, M, F;
 
void dijkstra(int x)
{
    int a, b, c, i;
    priority_queue< pair<int, int>, vector< pair<int, int> >, greater< pair<int, int> > > fila;
    fila.push(make_pair(dist[x] = 0, x));
    while(!fila.empty())
    {
        pair<int, int> p = fila.top();
        fila.pop();
        a = p.first;
        b = p.second;
        if(a > dist[b])
        	continue;
        for(i = 0; i < adj[b].size(); i++)
        {
            c = adj[b][i];
            if(val[c] + dist[b] < dist[c])
                fila.push(make_pair((dist[c] = val[c] + dist[b]),c));
        }
    }
}

void bfs(int src, int atk, int lim)
{
	int u, v, sz;
    vector<int> range(N+1, INFINITO);
    range[src] = 0;
    queue <int> q;
    q.push(src);
 
    while (!q.empty())
    {
        u = q.front(); q.pop();
        sz = (int)adj[u].size();
 
        if (range[u]>lim) return;
        if (u==F) continue;
 
        val[u] += atk;
 
        for (int j=0; j<sz; j++)
        {
            v = adj[u][j];
            if (range[u] + 1 < range[v])
            {
                range[v] = range[u] + 1;
                q.push(v);
            }}}}
 
main()
{
    int T, i, j, k, u, v, x, cont;
    scanf("%d", &T);
    for(k = 1; k <= T; k++)
    {
        scanf("%d %d %d", &N, &M, &F);
        cont = 0;
        for(i = 0; i <= N; i++)
        {
            adj[i].clear();
            val[i] = 0;
            dist[i] = INFINITO;
        }
        while(M--)
        {
            scanf("%d %d", &u, &v);
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        scanf("%d", &M);
        while(M--)
        {
            scanf("%d %d %d", &u, &v, &x);
            bfs(u, v, x);
        }
        val[F] = 0;
        dijkstra(F);
        scanf("%d", &M);
        while(M--)
        {
            scanf("%d %d", &u, &v);
            cont += (v > dist[u]);
        }
        printf("Caso #%d: %d\n", k, cont);
    }
    return 0;
}
