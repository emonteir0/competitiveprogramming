// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Zak Galou
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1409

#include<bits/stdc++.h>
#define INF 1000000000000LL
#define ll long long
using namespace std;

int N, M, Ga, K;
int tam[1000], cost[1000];
ll  dist[1001], monster[1001], val[1001];

vector< vector<int> > G(1001);

void knapsack()
{
	int i, j;
	for(i = 1; i <= 1000; i++)
		monster[i] = INF;
	for(j = 0; j < M; j++)
		for(i = 1; i <= 1000; i++)
			if(i >= tam[j])
				monster[i] = min(monster[i], monster[i-tam[j]]+cost[j]);
			else
				monster[i] = min(monster[i], 0LL+cost[j]);
}

void dijkstra()
{
	int i, u, v;
	ll w;
	priority_queue< pair<ll, int> > pq;
	for(i = 2; i <= N; i++)
		dist[i] = INF;
	dist[1] = val[1];
	pq.push(make_pair(-val[1], 1));
	while(!pq.empty())
	{
		w = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(w > dist[u])
			continue;
		for(i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u] + val[v])
			{
				dist[v] = dist[u] + val[v];
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
}

main()
{
	int i, u, v;
	while(scanf("%d %d %d %d", &M, &N, &Ga, &K) != EOF && (N||M||Ga||K))
	{
		for(i = 1; i <= N; i++)
		{
			G[i].clear();
			val[i] = 0;
		}
		for(i = 0; i < M; i++)
			scanf("%d %d", &cost[i], &tam[i]);
		knapsack();
		while(Ga--)
		{
			scanf("%d %d", &u, &v);
			G[u].push_back(v);
			G[v].push_back(u);
		}
		while(K--)
		{
			scanf("%d %d", &u, &v);
			val[u] += monster[v];
		}
		dijkstra();
		if(dist[N] == INF)
			printf("-1\n");
		else
			printf("%lld\n", dist[N]);
	}
	return 0;
}
