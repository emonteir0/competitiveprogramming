// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Infinity Gems
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2838

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N, M;

struct st
{
	int x, y, bita, bitb;
	st()
	{
	}
	st(int x, int y, int b, int c)
	{
		this->x = x;
		this->y = y;
		this->bita = b;
		this->bitb = c;
	}
};

int dist[50][50][16][32];
char mat[51][51];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

main()
{
	int x, y, bita, bitb, x2, y2, bita2, bitb2;
	queue<st> fila;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(int i = 0; i < N; i++)
		for(int j =0; j < M; j++)
			if(mat[i][j] == 'T')
			{
				x = i;
				y = j;
				break;
			}
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			for(int k = 0; k < 16; k++)
				for(int l = 0; l < 32; l++)
					dist[i][j][k][l] = INF;
	dist[x][y][0][0] = 0;
	fila.push(st(x, y, 0, 0));
	while(!fila.empty())
	{
		x = fila.front().x;
		y = fila.front().y;
		bita = fila.front().bita;
		bitb = fila.front().bitb;
		fila.pop();
		//printf("%d %d %d %d\n", x, y, bita, bitb);
		if(bitb == 31)
			break;
		for(int i = 0; i < 4; i++)
		{
			//printf("%d\n", i);
			x2 = x + dx[i];
			y2 = y + dy[i];
			bita2 = bita;
			bitb2 = bitb;
			if(inRange(x2, y2))
			{
				if(mat[x2][y2] == '#')
					continue;
				if((mat[x2][y2] >= 'A' && mat[x2][y2] <= 'D') && !((1<<(mat[x2][y2]-'A'))&bita))
				{
					//printf("%d %d!!!\n", mat[x2][y2], bita);
					continue;
				}
				if(mat[x2][y2] >= 'a' && mat[x2][y2] <= 'd')
					bita2 = bita2 | (1 << (mat[x2][y2]-'a'));
				if(mat[x2][y2] == 'p')
					bitb2 = bitb2 | 1;
				if(mat[x2][y2] == 't')
					bitb2 = bitb2 | 2;
				if(mat[x2][y2] == 'm')
					bitb2 = bitb2 | 4;
				if(mat[x2][y2] == 'e')
					bitb2 = bitb2 | 8;
				if(mat[x2][y2] == 'r')
					bitb2 = bitb2 | 16;
				if(dist[x2][y2][bita2][bitb2] > dist[x][y][bita][bitb]+1)
				{
					dist[x2][y2][bita2][bitb2] = dist[x][y][bita][bitb]+1;
					fila.push(st(x2, y2, bita2, bitb2));
				}
			}
		}
	}
	if(bitb != 31)
		printf("Gamora\n");
	else
		printf("%d\n", dist[x][y][bita][bitb]);
	return 0;
}
