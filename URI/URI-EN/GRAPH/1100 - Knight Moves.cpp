// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Knight Moves
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1100

#include<cstdio>
#include<queue>

typedef struct a{
	int x, y, d;
}b;

b c,g;

using namespace std;

queue<b> fila,fila2;

main()
{
	int i,x1,x2,y1,y2,dx[]={1,1,2,2,-1,-1,-2,-2},dy[]={2,-2,1,-1,2,-2,1,-1};
	char c1,c2;
	while(scanf("%c%d%*c%c%d%*c",&c1,&y1,&c2,&y2)==4)
	{
		x1=c1-'a'+1;
		x2=c2-'a'+1;
		c.x=x1;
		c.y=y1;
		c.d=0;
		fila.push(c);
		while(!fila.empty())
		{
			c=fila.front();
			fila.pop();
			if(c.x==x2&&c.y==y2)
			{
				printf("To get from %c%d to %c%d takes %d knight moves.\n",c1,y1,c2,y2,c.d);
				break;
			}
			for(i=0;i<8;i++)
			{
				g.x=c.x+dx[i];
				g.y=c.y+dy[i];
				g.d=c.d+1;
				if((g.x>0)&&(g.x<9)&&(g.y>0)&&(g.y<9))
				{
					fila.push(g);
				}
			}
		}
		fila=fila2;
	}
	return 0;
}
