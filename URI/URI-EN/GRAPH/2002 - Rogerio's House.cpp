// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rogerio's House
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2002

#include<bits/stdc++.h>
#define INFINITO 10000000000000000
#define ll unsigned long long int
#define MAX 9223372036854775807

using namespace std;

ll mat[101][101];
bool visitado[101][101];
ll distancia[101][101];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

set< pair<ll, pair<int, int> > > fila, fila2;
pair <ll, pair<int, int> > p;
pair <int, int> pos, pos0 = make_pair(1,1);

ll xorsum(ll N)
{
	return (N & (N % 2LL ? 0LL : MAX)) | ( ((N & 2LL)>>1) ^ (N & 1LL) );
}

ll xsum(ll a, ll b)
{
	return xorsum(max(a, b)) ^ xorsum(min(a, b)-1);
}

void dijkstra(int N, int M)
{
	int b, i, j, x, y, x2, y2;
	ll u, a;
	distancia[1][1] = 0;
	fila.insert(make_pair(0, pos0));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		pos = p.second;
		x = pos.first;
		y = pos.second;
		if(!visitado[x][y])
		{
			visitado[x][y] = true;
			for(i = 0; i< 4; i++)
			{
				x2 = x + dx[i];
				y2 = y + dy[i];
				if(x2 >= 1 && x2 <= N && y2 >= 1 && y2 <= M)
				{
					if(!visitado[x2][y2])
					{
						u = xsum(mat[x2][y2], mat[x][y]);
						if(!visitado[x2][y2] && (u+a) < distancia[x2][y2])
							fila.insert(make_pair((distancia[x2][y2] = u + a), make_pair(x2,y2)));
							//printf("(%d, %d) -> (%d, %d) = %d\n", x, y, x2, y2, u);
					}
				}
			}
		}
	}
}

main()
{
	int i, j, N, M;
	while(scanf("%d %d", &N, &M)==2)
	{
		for(i=1;i<=N;i++)
			for(j=1;j<=M;j++)
			{
				scanf("%lld", &mat[i][j]);
				visitado[i][j] = false;
				distancia[i][j] = INFINITO;
			}
		dijkstra(N, M);
		printf("%lld\n", distancia[N][M]);
	}
	return 0;
}
