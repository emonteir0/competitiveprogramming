// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Little Red-Cap
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2085

#include<bits/stdc++.h>
#define ll long long
using namespace std;

vector< pair<int, int> > G[100001], Ga[100001]; 

int N;
int vis[100001];
ll caminhos[100001];
ll dp[100001];

void dfs(int x)
{
	vis[x] = 1;
	if(x == N)
		return;
	for(int i = 0; i < G[x].size(); i++)
	{
		if(!vis[G[x][i].first])
		{
			dfs(G[x][i].first);
		}
		caminhos[x] += caminhos[G[x][i].first]*G[x][i].second;
	}
}

ll solve(int x)
{
	if(vis[x] == 2)
		return dp[x];
	vis[x] = 2;
	ll r = 0;
	for(int i = 0; i < G[x].size(); i++)
		r = max(r, solve(G[x][i].first));
	r += caminhos[x];
	return dp[x] = r;
}

main()
{
	int M, u, v;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		Ga[u].push_back({v, 1});
	}
	for(int i = 1; i <= N; i++)
		sort(Ga[i].begin(), Ga[i].end());
	for(int i = 1; i <= N; i++)
	{
		for(auto p: Ga[i])
		{
			if(G[i].size())
			{
				if(G[i].back().first == p.first)
					G[i].back().second++;
				else
					G[i].push_back(p);
			}
			else
				G[i].push_back(p);
			//printf("%d: (%d, %d)\n", i, G[i].back().first, G[i].back().second);
		}
	}
	caminhos[N] = 1;
	dfs(1);
	/*for(int i = 1; i <= N; i++)
		printf("%d -> %lld\n", i, caminhos[i]);*/
	printf("%lld\n", solve(1));
	return 0;
}
