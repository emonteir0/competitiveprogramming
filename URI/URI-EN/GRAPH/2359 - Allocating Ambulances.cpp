// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Allocating Ambulances
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2359

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

vector<int> G[1001], W[1001];

int N, M, K;
int dist[1001];


int dijkstra()
{
	int r = 0, u, v, c, w;
	priority_queue<pair<int, int> > fila;
	for(int i = 1; i <= N; i++)
			dist[i] = INF;
	while(K--)
	{
		scanf("%d", &u);
		dist[u] = 0;
		fila.push(make_pair(0, u));
	}
	while(!fila.empty())
	{
		w = -fila.top().first;
		u = fila.top().second;
		fila.pop();
		if(w > dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			w = W[u][i];
			if(dist[u] + w < dist[v])
			{
				dist[v] = dist[u] + w;
				fila.push(make_pair(-dist[v], v));
			}
		}
	}
	for(int i = 1; i <= N; i++)
		r = max(r, dist[i]);
	return r;
}

main()
{
	int u, v, w, x;
	while(scanf("%d %d %d", &N, &M, &K) == 3)
	{
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			G[u].push_back(v);
			W[u].push_back(w);
			G[v].push_back(u);
			W[v].push_back(w);
		}
		printf("%d\n", dijkstra());
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
	}
	return 0;
}
