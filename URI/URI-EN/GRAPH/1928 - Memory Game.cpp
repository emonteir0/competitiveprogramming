// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Memory Game
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1928

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x,i;
}par;


int vet[50001],pai[50001],nivel[50001];
map<int,set<int> > Mapa;
queue<int> fila;
set<int> conjunto;
set<int>::iterator it;
par vet2[50001];

void DFS(int U)
{
	int V;
	fila.push(U);
	while(!fila.empty()){
		U=fila.front();
		fila.pop();
		conjunto=Mapa[U];
		for(it=conjunto.begin();it!=conjunto.end();++it)
		{
			V=*it;
			if(nivel[V]==-1)
			{
				pai[V]=U;
				nivel[V]=nivel[U]+1;
				fila.push(V);
			}
		}
	}
}

bool cmp(par a, par b)
{
	return a.x>b.x;
}

int LCA(int a, int b)
{
	while(a!=b)
		if(nivel[a]>nivel[b]) a=pai[a];
		else b=pai[b];
	return a;
}

main()
{
	int N,i,x,y,cont=0;
	scanf("%d",&N);
	for(i=1;i<=N;i++)
	{
		scanf("%d",&vet[i]);
		vet2[i-1].x=vet[i];
		vet2[i-1].i=i;
	}
	for(i=1;i<N;i++)
	{
		scanf("%d %d",&x,&y);
		Mapa[x].insert(y);
		Mapa[y].insert(x);
	}
	for(i=2;i<=N;i++)
		nivel[i]=-1;
	DFS(1);
	sort(vet2,vet2+N,cmp);
	for(i=0;i<N/2;i++)
		cont+=nivel[vet2[2*i].i]+nivel[vet2[2*i+1].i]-2*nivel[LCA(vet2[2*i].i,vet2[2*i+1].i)];
	printf("%d\n",cont);
	return 0;
}
