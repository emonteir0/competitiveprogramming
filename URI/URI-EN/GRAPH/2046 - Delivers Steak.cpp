// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Delivers Steak
// Level: 2
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2046

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N, M;
int X[100001], Y[100001];
int dist[1001][1001];
int indices[1001][1001];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

struct st
{
	int x, y;
	st()
	{
	}
	st(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

int empate(int ind1, int ind2)
{
	if(X[ind1] != X[ind2])
		return X[ind1] < X[ind2] ? ind1 : ind2;
	return Y[ind1] < Y[ind2] ? ind1 : ind2;
}

int inRange(int x, int y)
{
	return x > 0 && x <= 1000 && y > 0 && y <= 1000;
}

main()
{
	int K, L, t = 1, x, y;
	queue<st> fila;
	st stc;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		if(t > 1)
			printf("\n");
		printf("Instancia %d\n", t++);
		fila = queue<st>();
		
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= M; j++)
			{
				dist[i][j] = INF;
				indices[i][j] = -1;
			}
		scanf("%d", &K);
		for(int i = 0; i < K; i++)
		{
			scanf("%d %d", &X[i], &Y[i]);
			stc.x = X[i];
			stc.y = Y[i];
			dist[stc.x][stc.y] = 0;
			indices[stc.x][stc.y] = i;
			fila.push(stc);
		}
		while(!fila.empty())
		{
			stc = fila.front();
			fila.pop();
			for(int k = 0; k < 4; k++)
			{
				x = stc.x+dx[k];
				y = stc.y+dy[k];
				if(!inRange(x, y))
					continue;
				if(dist[x][y] > dist[stc.x][stc.y] + 1)
				{
					dist[x][y] = dist[stc.x][stc.y]+1;
					indices[x][y] = indices[stc.x][stc.y];
					fila.push(st(x, y));
				}
				else if(dist[x][y] == dist[stc.x][stc.y] + 1)
					indices[x][y] = empate(indices[stc.x][stc.y], indices[x][y]);
			}
		}
		scanf("%d", &L);
		while(L--)
		{
			scanf("%d %d", &x, &y);
			printf("%d %d\n", X[indices[x][y]], Y[indices[x][y]]);
		}
	}
	return 0;
}
