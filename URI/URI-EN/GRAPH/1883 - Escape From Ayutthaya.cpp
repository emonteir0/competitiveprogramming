// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escape From Ayutthaya
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1883

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int dist[1000][1000];
int dist2[1000][1000];

char mat[1000][1001];

int N, M;

bool inRange(int x, int y)
{
	return x >=0 && x < N && y >=0 && y < M;
}

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

main()
{
	int T, x, y, x2, y2, xe, ye;
	queue<pair<int, int> > fila, fila2;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &M);
		fila = queue<pair<int, int> >();
		fila2 = queue<pair<int, int> >();
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				dist[i][j] = INF+1;
				dist2[i][j] = INF;
			}
		for(int i = 0; i < N; i++)
		{
			scanf("%s", mat[i]);
			for(int j = 0; j < M; j++)
			{
				if(mat[i][j] == 'F')
				{
					dist[i][j] = 0;
					fila.push(make_pair(i, j));
				}
				if(mat[i][j] == 'S')
				{
					dist2[i][j] = 0;
					fila2.push(make_pair(i, j));
				}
				if(mat[i][j] == 'E')
				{
					xe = i;
					ye = j;
				}
			}
		}
		while(!fila.empty())
		{
			x = fila.front().first;
			y = fila.front().second;
			fila.pop();
			for(int i = 0; i < 4; i++)
			{
				x2 = x + dx[i];
				y2 = y + dy[i];
				if(inRange(x2, y2) && mat[x2][y2] != '#' && dist[x2][y2] == INF+1)
				{
					dist[x2][y2] = dist[x][y] + 1;
					fila.push(make_pair(x2, y2));
				}
			}
		}
		while(!fila2.empty())
		{
			x = fila2.front().first;
			y = fila2.front().second;
			fila2.pop();
			for(int i = 0; i < 4; i++)
			{
				x2 = x + dx[i];
				y2 = y + dy[i];
				if(inRange(x2, y2) && mat[x2][y2] != '#' && dist2[x2][y2] == INF)
				{
					dist2[x2][y2] = dist2[x][y] + 1;
					if(dist[x2][y2] > dist2[x2][y2])
						fila2.push(make_pair(x2, y2));
				}
			}
		}
		printf("%s\n", dist2[xe][ye] != INF && dist[xe][ye] > dist2[xe][ye] ? "Y" : "N");
	}
	return 0;
}
