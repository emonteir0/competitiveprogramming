// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Imposto Real
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2666

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(10001);
vector< vector<int> > W(10001);

int ans, K;
int e[10001];
int vis[10001];

int dfs(int u)
{
	int i, v, x = e[u], y;
	vis[u] = 1;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		if(!vis[v])
		{
			y = dfs(v);
			x += y;
			ans += W[u][i]*((y+K-1)/K);
		}
	}
	return x;
}

main()
{
	int i, N, x, u, v, w;
	scanf("%d %d %*d", &N, &K);
	for(i = 2; i <= N; i++)
		scanf("%d", &e[i]);
	for(i = 2; i <= N; i++)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(v);
		G[v].push_back(u);
		W[u].push_back(w);
		W[v].push_back(w);
	}
	dfs(1);
	printf("%d\n", 2*ans);
	return 0;
}
