// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: City in the Center
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2676

#include<bits/stdc++.h>
#define INF 1000000000

using namespace std;

int min(int a, int b)
{
	return (a < b) ? a : b;
}

struct st
{
	int i, x;
};

int N;
int mat[101][101];
st tot[101];

void floydwarshall()
{
	int i, j, k;
	for(k = 1; k <= N; k++)
		for(i = 1; i <= N; i++)
			for(j = 1; j <= N; j++)
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);

}

bool cmp(st st1, st st2)
{
	if(st1.x != st2.x)
		return st1.x < st2.x;
	return st1.i < st2.i;
}

main()
{
	int i, j, M, u, v, w;
	while(scanf("%d %d", &N, &M) == 2 && (N||M))
	{
		for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
				mat[i][j] = INF;
			mat[i][i] = 0;
			tot[i].i = i;
			tot[i].x = 0;
		}
		for(i = 1; i <= M; i++)
		{
			scanf("%d %d %d", &u, &v, &w);
			mat[u][v] = w;
			mat[v][u] = w;
		}
		/*for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
				printf("%d ", mat[i][j]);
			printf("\n");
		}
		printf("\n\n");*/
		floydwarshall();
		/*for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
				printf("%d ", mat[i][j]);
			printf("\n");
		}*/
		for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= N; j++)
				tot[i].x += mat[i][j];
			//printf("%d\n", tot[i].x);
		}
		sort(tot+1, tot+N+1, cmp);
		for(i = 1; i <= N; i++)
		{
			if(tot[i].x != tot[1].x)
				break;
			printf("%d ", tot[i].i);
		}
		printf("\n");
	}
	return 0;
}
