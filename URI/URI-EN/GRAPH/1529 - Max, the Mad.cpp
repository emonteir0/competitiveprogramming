// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Max, the Mad
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1529

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

vector<int> G[21], W[21];

int N, M, T;
int custo[21];
int dist[21][51];


int dijkstra()
{
	int r = INF, u, v, c, w;
	priority_queue< pair<int, pair<int, int> > > fila;
	for(int i = 1; i <= N; i++)
		for(int j = 0; j <= T; j++)
			dist[i][j] = INF;
	dist[1][T] = 0;
	fila.push(make_pair(0, make_pair(1, T)));
	while(!fila.empty())
	{
		w = -fila.top().first;
		u = (fila.top().second).first;
		c = (fila.top().second).second;
		fila.pop();
		if(w > dist[u][c])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			w = W[u][i];
			for(int j = c; j <= T; j++)
			{
				if(j >= w && dist[u][c] + custo[u] * (j-c) < dist[v][j-w])
				{
					dist[v][j-w] = dist[u][c] + custo[u] * (j-c);
					fila.push(make_pair(-dist[v][j-w], make_pair(v, j-w)));
				}
			}
		}
	}
	for(int i = 0; i <= T; i++)
		r = min(r, dist[N][i]);
	return r;
}

main()
{
	int u, v, w, x;
	while(scanf("%d %d %d", &N, &M, &T) == 3 && N)
	{
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			if(w <= T)
			{
				G[u].push_back(v);
				W[u].push_back(w);
				G[v].push_back(u);
				W[v].push_back(w);
			}
		}
		for(int i = 1; i <= N; i++)
			scanf("%d", &custo[i]);
		x = dijkstra();
		if(x == INF)
			printf("-1\n");
		else
			printf("%d\n", x);
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
	}
	return 0;
}
