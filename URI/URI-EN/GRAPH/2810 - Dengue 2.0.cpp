// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dengue 2.0
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2810

#include<bits/stdc++.h>
#define INF 1e50

using namespace std;

int N, M, lim;
int X[16], Y[16];
double dp[6][16][1024];
double dist[16][16];
int vis[6][16][1024];

int sqr(int x)
{
	return x*x;
}

void calc()
{
	for(int i = 0; i < N+M; i++)
		for(int j = i+1; j < N+M; j++)
			dist[i][j] = dist[j][i] = sqrt(sqr(X[i]-X[j])+sqr(Y[i]-Y[j]));
}

double solve(int x, int y, int bit)
{
	if(x == M)
		return bit == lim ? 0 : INF;
	if(vis[x][y][bit])
		return dp[x][y][bit];
	vis[x][y][bit] = 1;
	double r = solve(x+1, x+1+N, bit) + dist[y][x+N];
	for(int i = 0; i < N; i++)
		if(!(bit & (1 << i)))
			r = min(r, solve(x, i, bit | (1 << i)) + dist[y][i]);
	return dp[x][y][bit] = r;
}

main()
{
	scanf("%d %d", &N, &M);
	for(int i = N; i < N+M; i++)
		scanf("%d %d", &X[i], &Y[i]);
	for(int i = 0; i < N; i++)
		scanf("%d %d", &X[i], &Y[i]);
	lim = (1 << N) - 1;
	calc();
	printf("%.2lf\n", solve(0, N, 0));
	return 0;
}
