// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dreams, Believe Them!
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2081

#include<bits/stdc++.h>
using namespace std;

char dist[51][51][51][51];

struct st
{
	int x1, y1, x2, y2;
	st()
	{
	}
	st(int x1, int y1, int x2, int y2)
	{
		this->x1 = x1;
		this->y1 = y1;
		this->x2 = x2;
		this->y2 = y2;
	}
};

int N, M;

bool inRange(int x, int y)
{
	return x >= 0 && x < N && y >=0 && y < M;
}

char mat[50][51], mat2[50][51];

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

main()
{
	int T, xa, ya, xa2, ya2, xb, yb, xb2, yb2, xaf, yaf, xbf, ybf;
	st s;
	queue<st> fila;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &M);
		for(int i = 0; i < N; i++)
		{
			scanf("%s", mat[i]);
			for(int j = 0; j < M; j++)
			{
				if(mat[i][j] == 'R')
				{
					xa = i;
					ya = j;
				}
				if(mat[i][j] == 'F')
				{
					xaf = i;
					yaf = j;
				}
			}
		}
		for(int i = 0; i < N; i++)
		{
			scanf("%s", mat2[i]);
			for(int j = 0; j < M; j++)
			{
				if(mat2[i][j] == 'R')
				{
					xb = i;
					yb = j;
				}
				if(mat2[i][j] == 'F')
				{
					xbf = i;
					ybf = j;
				}
			}
		}
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				for(int k = 0; k < N; k++)
					for(int l = 0; l < M; l++)
						dist[i][j][k][l] = 51;
		fila = queue<st>();
			fila.push(st(xa, ya, xb, yb));
		dist[xa][ya][xb][yb] = 0;
		while(!fila.empty())
		{
			s = fila.front();
			xa = s.x1;
			ya = s.y1;
			xb = s.x2;
			yb = s.y2;
			fila.pop();
			if(xa == xaf && xb == xbf && ya == yaf && yb == ybf)
				break;
			for(int i = 0; i < 4; i++)
			{
				xa2 = xa + dx[i];
				ya2 = ya + dy[i];
				xb2 = xb + dx[i];
				yb2 = yb + dy[i];
				if(!inRange(xa2, ya2) || !inRange(xb2, yb2))
					continue;
				if(mat[xa2][ya2] == 'B' || mat2[xb2][yb2] == 'B')
					continue;
				if(mat[xa2][ya2] == '#' && mat2[xb2][yb2] == '#')
					continue;
				if(mat[xa2][ya2] == '#')
				{
					xa2 = xa;
					ya2 = ya;
				}
				if(mat2[xb2][yb2] == '#')
				{
					xb2 = xb;
					yb2 = yb;
				}
				if(dist[xa2][ya2][xb2][yb2] == 51)
				{
					dist[xa2][ya2][xb2][yb2] = dist[xa][ya][xb][yb] + 1;
					fila.push(st(xa2, ya2, xb2, yb2));
				}
			}
		}
		if(dist[xaf][yaf][xbf][ybf] <= 50)
			printf("%d\n", dist[xaf][yaf][xbf][ybf]);
		else
			printf("impossivel\n");
	}
	return 0;
}
