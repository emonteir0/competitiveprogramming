// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pre, In and Post
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1194

#include<bits/stdc++.h>

using namespace std;

struct No
{
	int value;
	string str;
	No *left;
	No *right;
};

map<char, int> Mapa, Mapa2;

typedef No node;


void posfixa(node **raiz)
{
	if(*raiz != NULL)
	{
		posfixa(&((*raiz)->left));
		posfixa(&((*raiz)->right));
		printf("%c", (*raiz)->str[0]);
	}
}

int valor(string str)
{
	int i, tam, me = 2001;
	tam = str.length();
	for(i = 0; i < tam; i++)
		me = min(me, Mapa[str[i]]);
	return me;
}

void buscar(node *raiz, char c, int value)
{
	if((int)raiz->value == value)
	{
		string a, b;
		int x = raiz->str.find(c);
		if(x != 0)
		{
			a = raiz->str.substr(0, x);
			raiz->left = new node;
			(raiz->left)->left = NULL;
			(raiz->left)->right = NULL;
			(raiz->left)->str = a;
			(raiz->left)->value = valor(a);
		}
		if(x != (int)(raiz->str.length()-1))
		{
			b = raiz->str.substr(x+1, -1);
			raiz->right = new node;
			(raiz->right)->left = NULL;
			(raiz->right)->right = NULL;
			(raiz->right)->str = b;
			(raiz->right)->value = valor(b);
		}
		raiz->str = c;
		return;
	}
	if(raiz->left != NULL)
		buscar(raiz->left, c, value);
	if(raiz->right != NULL)
		buscar(raiz->right, c, value);
}

main()
{
	char palavra1[53], palavra2[53];
	int i, N, M;
	node* arvore;
	string str;
	scanf("%d%*c", &M);
	while(M--)
	{
		Mapa = Mapa2;
		scanf("%d %s %s%*c", &N, palavra1, palavra2);
		for(i = 0; i < N; i++)
			Mapa[palavra1[i]] = i;
		arvore = new node;
		arvore->left = NULL;
		arvore->right = NULL;
		arvore->str = palavra2;
		arvore->value = 0;
		for(i = 0; i < N; i++)
			buscar(arvore, palavra1[i], i);
		posfixa(&arvore);
		printf("\n");
	}
	return 0;
}
