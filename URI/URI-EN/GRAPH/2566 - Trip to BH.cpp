// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trip to BH
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2566

#include <bits/stdc++.h>
using namespace std;

#define ii pair <int, int>
#define vi vector <int>
#define vii vector <ii>
#define vvi vector <vi>
#define vvii vector <vii>
#define pb push_back
#define INF ((int)1e9)

int n;

int dijkstra(vvii& adj) {
	vi dist(n+1, INF);
	priority_queue <ii, vii, greater <ii> > pq;
	dist[1] = 0;
	pq.push(ii(dist[1], 1));
	
	while (!pq.empty()) {
		ii cur = pq.top(); pq.pop();
		int u = cur.second;
		if (cur.first > dist[u]) {
			continue;
		}
		if (u == n) {
			return dist[u];
		}
		int sz = adj[u].size();
		for (int j = 0; j < sz; j++) {
			int v = adj[u][j].first;
			int w = adj[u][j].second;
			if (dist[u] + w < dist[v]) {
				dist[v] = dist[u] + w;
				pq.push(ii(dist[v], v));
			}
		}
	}
	return INF;
}

int main() {
	ios_base::sync_with_stdio(0);
	int m, u, v, b, w, e;
	while (cin >> n >> m) {
		vvii ga(n+1);
		vvii gb(n+1);
		while (m--) {
			cin >> u >> v >> b >> w;
			if (b == 0) {
				ga[u].pb(ii(v, w));
			} else {
				gb[u].pb(ii(v, w));
			}
		}
		int ans = min(dijkstra(ga), dijkstra(gb));
		cout << ans << endl;
	}
	return 0;
}
