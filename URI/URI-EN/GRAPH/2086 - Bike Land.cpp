// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bike Land
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2086

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;

set< pair<int,int> > fila;
pair<int,int> p;
int mat[101][101];
bool con[101][101], visitado[101];
int distancia[101];

void dijkstra(int x, int N)
{
	int a, b, i;
	for(i = 1; i <= N; i++)
	{
		visitado[i] = false;
		distancia[i] = INFINITO;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		b = p.second;
		if(!visitado[b])
		{
			visitado[b] = true;
			for(i = 1; i<= N; i++)
				if(con[b][i])
					if(!visitado[i] && max(a,mat[b][i]) < distancia[i])
						fila.insert(make_pair((distancia[i]=max(a,mat[b][i])),i));
		}
	}
}

int main()
{
	int N, M, i, j, x, y, h, k;
	while(scanf("%d %d",&N,&M) == 2 && (N||M))
	{
		for(i = 1; i <= N; i++)
			for(j = 1; j<=N; j++)
			{
				con[i][j] = false;
				mat[i][j] = INFINITO;
			}
		for(i = 1; i <= M; i++)
		{
			scanf("%d %d %d", &x, &y, &h);
			con[x][y] = con[y][x] = true;
			mat[x][y] = mat[y][x] = h;
		}
		scanf("%d", &k);
		while(k--)
		{
			scanf("%d %d", &x,&y);
			dijkstra(x,N);
			printf("%d\n", distancia[y]);
		}
	}
	return 0;
}

