// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lazy Jumping Frog
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1384

#include<bits/stdc++.h>
#define INF 1000000000

using namespace std;

queue< pair<int, int> > fila, fila2;

int mat[1001][1001];
int dist[1001][1001];

int dx[24] = {-2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2};
int dy[24] = {-2, -2, -2, -2, -2, -1, -1, -1, -1, -1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2};
int cost[24] = {7, 6, 5, 6, 7, 6, 3, 2, 3, 6, 5, 2, 2, 5, 6, 3, 2, 3, 6, 7, 6, 5, 6, 7};

int N, M, Q;

char inRange(int x, int y)
{
    return (x > 0) && (x <= N) && (y > 0) && (y <= M);
}

void dijkstra(int xi, int yi)
{
    int i, j, x, y, x2, y2, w;
    priority_queue< pair<int, pair<int, int> > > pq;
    for(i = 1; i <= N; i++)
        for(j = 1; j <= M; j++)
            dist[i][j] = INF;
    dist[xi][yi] = 0;
    pq.push(make_pair(0, make_pair(xi, yi)));
    while(!pq.empty())
    {
        w = -pq.top().first;
        x = ((pq.top()).second).first;
        y = ((pq.top()).second).second;
        pq.pop();
        if(w > dist[x][y])
            continue;
        for(i = 0; i < 24; i++)
        {
            x2 = x + dx[i];
            y2 = y + dy[i];
            if(inRange(x2, y2))
            {
                if((dist[x2][y2] > dist[x][y] + cost[i]) && !mat[x2][y2])
                {
                    dist[x2][y2] = dist[x][y] + cost[i];
                    pq.push(make_pair(-dist[x2][y2], make_pair(x2, y2)));
                }
            }
        }
    }
}

main()
{
    int i, j, x, y, x2, y2;
    int xi, yi, xf, yf;
    while(scanf("%d %d", &N, &M) == 2 && (N||M))
    {
        for(i = 1; i <= N; i++)
            for(j = 1; j <= M; j++)
                mat[i][j] = 0;
        scanf("%d %d %d %d", &xi, &yi, &xf, &yf);
        scanf("%d", &Q);
        while(Q--)
        {
            scanf("%d %d %d %d", &x, &y, &x2, &y2);
            for(i = x; i <= x2; i++)
                for(j = y; j <= y2; j++)
                    mat[i][j] = 1;
        }
        dijkstra(xi, yi);
        if(dist[xf][yf] == INF)
            printf("impossible\n");
        else
            printf("%d\n", dist[xf][yf]);
    }
    return 0;
}
