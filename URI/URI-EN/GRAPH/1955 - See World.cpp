// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: See World
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1955

#include<bits/stdc++.h>

using namespace std;

int N;
int mat[1000][1000];
int cores[1000];

int dfs(int x, int cor)
{
	if(cores[x] != 0)
		return cores[x] == cor;
	else
		cores[x] = cor;
	int r = 1;
	for(int i = 0; i < N; i++)
		if(mat[x][i] == 0)
			r &= (dfs(i, cor == 1 ? 2 : 1));
	return r;
}

main()
{
	int r = 1;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < N; j++)
			scanf("%d", &mat[i][j]);
	}
	for(int i = 0; i < N; i++)
		if(cores[i] == 0)
			r &= dfs(i, 1);
	printf("%s\n", r ? "Bazinga!" : "Fail!");
	return 0;
}
