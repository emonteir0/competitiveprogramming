// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fiber Network
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1738

#include<bits/stdc++.h>

using namespace std;

int M[201][201];

void printval(int x)
{
	for(int i = 0; i < 26; i++)
	{
		if(x & (1 << i))
			printf("%c", 'a'+i);
	}
	printf("\n");
}

main()
{
	int N, a, b;
	string str;
	while((cin >> N) && N)
	{
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
				M[i][j] = 0;
		while((cin >> a >> b) && a)
		{
			cin >> str;
			for(int i = 0; i < str.length(); i++)
				M[a][b] |= 1 << (str[i]-'a');
		}
		for(int k = 1; k <= N; k++)
			for(int i = 1; i <= N; i++)
				for(int j = 1; j <= N; j++)
					M[i][j] |= M[i][k]&M[k][j];
		while((cin >> a >> b) && a)
		{
			if(M[a][b] == 0)
				printf("-\n");
			else
				printval(M[a][b]);
		}
		printf("\n");
	}
	return 0;
}

