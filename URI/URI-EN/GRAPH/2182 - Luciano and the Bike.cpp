// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Luciano and the Bike
// Level: 3
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2182

#include<bits/stdc++.h>
 
using namespace std; 
 
int N;
int visitado[10001];

vector< vector< pair<int, int> > > G(10001);
 
typedef struct
{
    int ind;
    double media;
} valor;
 
void DFS(int ind, int* ma, int* buracos, int* q)
{
    int i;
    *ma = *ma > ind? *ma : ind;
    visitado[ind] = 1;
    for(i = 0; i < G[ind].size(); i++)
    {
		*q += 1;
		*buracos += G[ind][i].second;
		if(!visitado[G[ind][i].first])
			DFS(G[ind][i].first, ma, buracos, q);
    }
}
 
valor atual;
 
main()
{
    int M, i, x, y, v, ma, buracos, q;
    double media;
    atual.ind = 0;
    atual.media = 999999999;
    scanf("%d %d", &N, &M);
    for(i=1;i<=M;i++)
    {
        scanf("%d %d %d", &x, &y, &v);
        G[x].push_back(make_pair(y, v));
        G[y].push_back(make_pair(x, v));
    }
    for(i = 1; i <= N; i++)
    {
        if(!visitado[i])
        {
            q = 0;
            buracos = 0;
            ma = i;
            DFS(i, &ma, &buracos, &q);
            if(q == 0)
                q++;
            media = ((double)buracos)/q;
            if(media < atual.media)
            {
                atual.media = media;
                atual.ind = ma;
            }
            else if(media == atual.media && ma > atual.ind)
                atual.ind = ma;
        }
    }
    printf("%d\n", atual.ind);
    return 0;
}

