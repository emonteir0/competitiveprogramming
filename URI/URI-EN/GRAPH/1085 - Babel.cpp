// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Babel
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1085

#include<bits/stdc++.h>
#define INF 999999999

using namespace std;

int k, tam;
vector< vector<int> > G(200001), W(200001);
map<string, int> Mapa, Mapa2;
int distancia[200001], vis[200001];
string str, str2;


void dijkstra(int s)
{
	int i, u, v, w;
	priority_queue< pair<int, int> > fila;
	for(i = 0; i <= tam; i++)
	{
		vis[i] = 0;
		distancia[i] = INF;
	}
	distancia[0] = 0;
	fila.push(make_pair(0, s));
	while(!fila.empty())
	{
		u = fila.top().second;
		w = -fila.top().first;
		fila.pop();
		if(!vis[u])
		{
			vis[u] = 1;
			for(i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				if(!vis[v] && distancia[v] > W[u][i] + w)
				{
					distancia[v] = W[u][i] + w;
					fila.push(make_pair(-distancia[v], v));
				}
			}
		}
	}
}

main()
{
	int N, i, x, y, z, me;
	char msg[101], msg2[101], msg3[101];
	while(scanf("%d%*c", &N) == 1 && N)
	{
		Mapa = Mapa2;
		k = 2;
		scanf("%s %s", msg, msg2);
		str = msg;
		str2 = msg2;
		Mapa[str] = 0;
		Mapa[str2] = 1;
		while(N--)
		{
			scanf("%s %s %s", msg, msg2, msg3);
			str = msg;
			str2 = msg2;
			if(Mapa.find(str) == Mapa.end())
				Mapa[str] = k++;
			if(Mapa.find(str2) == Mapa.end())
				Mapa[str2] = k++;
			x = Mapa[str];
			y = Mapa[str2];
			z = strlen(msg3);
			for(i = 1; i <= 26; i++)
				if(i != msg3[0]-'a'+1)
				{
					G[26*x+i].push_back(26*y+msg3[0]-'a'+1);
					W[26*x+i].push_back(z);
					G[26*y+i].push_back(26*x+msg3[0]-'a'+1);
					W[26*y+i].push_back(z);
				}
			if(x == 0)
			{
				G[0].push_back(26*y+msg3[0]-'a'+1);
				W[0].push_back(z);
			}
			if(y == 0)
			{
				G[0].push_back(26*x+msg3[0]-'a'+1);
				W[0].push_back(z);
			}
		}
		tam = 26*(k+1);
		dijkstra(0);
		me = INF;
		for(i = 27; i <= 52; i++)
			me = min(me, distancia[i]);
		if(me == INF)
			printf("impossivel\n");
		else
			printf("%d\n", me);
		for(i = 0; i <= tam; i++)
		{
			G[i].clear();
			W[i].clear();
		}
	}
	return 0;
}
