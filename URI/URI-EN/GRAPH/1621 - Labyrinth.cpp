// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Labyrinth
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1621

#include<bits/stdc++.h>

using namespace std;

int N, M, ma, indx, indy;
int nivel[500][500];
char mat[500][501];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};


int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

int dfs(int x, int y)
{
	int x2, y2;
	for(int k = 0; k < 4; k++)
	{
		x2 = x + dx[k];
		y2 = y + dy[k];
		if(inRange(x2, y2) && mat[x2][y2] == '.' && nivel[x2][y2] == -1)
		{
			nivel[x2][y2] = nivel[x][y]+1;
			if(nivel[x2][y2] > ma)
			{
				ma = nivel[x2][y2];
				indx = x2;
				indy = y2;
			}
			dfs(x2, y2);
		}
	}
}

main()
{
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%s", mat[i]);
			for(int j = 0; j < M; j++)
				nivel[i][j] = -1;
		}
		ma = 0;
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < M; j++)
			{
				if(mat[i][j] == '.')
				{
					nivel[i][j] = 0;
					dfs(i, j);
					i = N;
					j = M;
				}
			}
		}
		ma = 0;
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				nivel[i][j] = -1;
		nivel[indx][indy] = 0;
		dfs(indx, indy);
		printf("%d\n", ma);
	}
	return 0;
}
