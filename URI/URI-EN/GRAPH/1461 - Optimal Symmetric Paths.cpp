// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Optimal Symmetric Paths
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1461

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
#define INF 1001001001

using namespace std;

int N;
int mat[100][100];
int dist[100][100];
ll dp[100][100];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >=0 && x < N && y >= 0 && (x+y) < N;
}


void dijkstra()
{
	int x, y, x2, y2, w;
	priority_queue< pair<int, pair<int, int> > > fila;
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
		{
			dist[i][j] = INF;
			dp[i][j] = 0;
		}
	dist[0][0] = 0;
	dp[0][0] = 1;
	fila.push(make_pair(0, make_pair(0, 0)));
	while(!fila.empty())
	{
		w = -fila.top().first;
		x = fila.top().second.first;
		y = fila.top().second.second;
		fila.pop();
		if(w > dist[x][y])
			continue;
		for(int i = 0; i < 4; i++)
		{
			x2 = x + dx[i];
			y2 = y + dy[i];
			if(inRange(x2, y2))
			{
				if(dist[x2][y2] > dist[x][y] + mat[x2][y2])
				{
					dist[x2][y2] = dist[x][y] + mat[x2][y2];
					dp[x2][y2] = dp[x][y];
					fila.push(make_pair(-dist[x2][y2], make_pair(x2, y2)));
				}
				else if(dist[x2][y2] == dist[x][y] + mat[x2][y2])
				{
					dp[x2][y2] += dp[x][y];
					if(dp[x2][y2] >= MOD)
						dp[x2][y2] -= MOD;
				}
			}
		}
	}
}

main()
{
	int me;
	ll sum;
	while(scanf("%d", &N) == 1 && N)
	{
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				scanf("%d", &mat[i][j]);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N-i-1; j++)
				mat[i][j] += mat[N-1-j][N-1-i];
		/*for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N-i; j++)
				printf("%d ", mat[i][j]);
			printf("\n");
		}*/
		dijkstra();
		me = INF;
		for(int i = 0; i < N; i++)
		{
			if(dist[i][N-1-i] < me)
			{
				me = dist[i][N-1-i];
				sum = dp[i][N-1-i];
			}
			else if(dist[i][N-1-i] == me)
				sum += dp[i][N-1-i];
		}
		printf("%lld\n", sum % MOD);
	}
	return 0;
}
