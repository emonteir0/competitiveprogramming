// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trip Programming
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2731

#include<bits/stdc++.h>
#define INF 200000000
using namespace std;

vector<int> G[16];
vector<int> W[16];
int dist[16];
int pai[16];
int N, M;

void dijkstra(int x)
{
	int u, v;
	int w;
	priority_queue< pair<int, int> > pq;
	for(int i = 1; i <= N; i++)
	{
		pai[i] = 0;
		dist[i] = INF;
	}
	dist[x] = 0;
	pq.push(make_pair(0, x));
	while(!pq.empty())
	{
		w = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(dist[u] < w)
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > w + W[u][i])
			{
				pai[v] = u;
				dist[v] = w + W[u][i];
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
}


main()
{
	vector<int> ans;
	int S, p, u, v, w;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			G[u].push_back(v);
			G[v].push_back(u);
			W[u].push_back(w);
			W[v].push_back(w);
		}
		scanf("%d", &S);
		dijkstra(S);
		p = 1;
		ans.clear();
		while(p != 0)
		{
			ans.push_back(p);
			p = pai[p];
		}
		if(dist[1] > 120)
			printf("It will be %d minutes late. Travel time - %d - best way -", dist[1]-120, dist[1]);
		else
			printf("Will not be late. Travel time - %d - best way -", dist[1]);
		for(int i = ans.size()-1; i >= 0; i--)
			printf(" %d", ans[i]);
		printf("\n");
	}
	return 0;
}
