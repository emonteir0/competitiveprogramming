// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Linhas de Metrô
// Level: 2
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2887

#include<bits/stdc++.h>
#define MAXN 100005
#define MAXL 18
#define INF 1001001001
#define ll long long

using namespace std;

vector<int> G[MAXN];

int N;
int ancestral[MAXN][MAXL], nivel[MAXN];

void dfs(int u)
{
	int v, w;
	for(int i = 0; i < (int)G[u].size(); i++)
	{
		v = G[u][i];
		if(nivel[v] == -1)
		{
			nivel[v] = nivel[u] + 1;
			ancestral[v][0] = u;
			dfs(v);
		}
	}
}

void buildLCA(int raiz)
{
	for(int i = 1; i <= N; i++)
		nivel[i] = -1;
	nivel[raiz] = 0;
	dfs(raiz);
	for(int i = 1; i <= N; i++)
		for(int j = 1; j < MAXL; j++)
		{
			ancestral[i][j] = 0;
		}
    for(int j = 1; j < MAXL; j++)
		for(int i = 1; i <= N; i++)
			if(ancestral[i][j-1] != 0)
			{
				ancestral[i][j] = ancestral[ancestral[i][j-1]][j-1];
			}
}

int LCA(int u, int v)
{   
	int ans = INF;
    if(nivel[u] < nivel[v]) swap(u, v);
    for(int i = MAXL-1; i >= 0; i--)
        if(nivel[u] - (1<<i) >= nivel[v])
        {
        	u = ancestral[u][i];
        }
    if(u == v)
		//return u; caso queira apenas o LCA
		return u;
    for(int i = MAXL-1; i >= 0; i--)
        if((ancestral[u][i] != 0) && (ancestral[u][i] != ancestral[v][i]))
		{
            u = ancestral[u][i];
            v = ancestral[v][i];
        }    
    //return ancestral[u][0]; caso queira apenas o LCA
    return ancestral[u][0];
}

int dist(int x, int y)
{
	return nivel[x]+nivel[y]-2*nivel[LCA(x,y)];
}

main()
{
	int Q, u, v, A, B, C, D, AB, CD, AC, AD, BC, BD;
	scanf("%d %d", &N, &Q);
	for(int i = 1; i < N; i++)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	buildLCA(1);
	while(Q--)
	{
		scanf("%d %d %d %d", &A, &B, &C, &D);
		AB = LCA(A, B);
		CD = LCA(C, D);
		AC = LCA(A, C);
		AD = LCA(A, D);
		BC = LCA(B, C);
		BD = LCA(B, D);
		/*printf("AB - %d\n", AB);
		printf("CD - %d\n", CD);
		printf("AC - %d\n", AC);
		printf("AD - %d\n", AD);
		printf("BC - %d\n", BC);
		printf("BD - %d\n", BD);*/
		if(((AB > AC) && (AB > AD) && (AB > BC) && (AB > BD)) || ((CD > AC) && (CD > AD) && (CD > BC) && (CD > BD)))
			printf("0\n");
		else
		{
			printf("%d\n", abs(dist(AC, BD)-dist(AD, BC))+1);
		}
	}
	return 0;
}
