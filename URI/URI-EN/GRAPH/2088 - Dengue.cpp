// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dengue
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2088

#include<bits/stdc++.h>
#define INF 1e18

using namespace std;

int x[16], y[16];
double memo[16][65536];
double dist[16][16];


double tsp(int u, int mask, int n) 
{
    if (mask == (1<<n)-1) 
		return dist[u][0];
    double &r = memo[u][mask];
	if (r != -1) 
		return r;
    r = INF;
    for (int v = 0; v < n; v++)
        if (!(mask&(1<<v)))
            r = min(r, tsp(v, mask|(1<<v), n)+dist[u][v]);
    return r;
}

int sqr(int a)
{
	return a*a;
}

main()
{
	int i, j, N, lim;
	while(scanf("%d", &N) == 1 && N)
	{
		lim = 1<<(N+1);
		for(i = 0; i <= N; i++)
		{
			scanf("%d %d", &x[i], &y[i]);
			dist[i][i] = 0;
			for(j = 0; j < i; j++)
				dist[i][j] = dist[j][i] = sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
			for(j = 0; j < lim; j++)
				memo[i][j] = -1;
		}
		printf("%.2lf\n", tsp(0, 0, N+1));
	}
	return 0;
}
