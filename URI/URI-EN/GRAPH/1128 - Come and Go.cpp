// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Come and Go
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1128

#include<bits/stdc++.h>

using namespace std;


char vis[10001], vis2[10001];
vector< vector<int> > G(10001), G2(10001);
vector<int> indices;

void dfs(int x)
{
	int i;
	vis[x] = 1;
	for(i = 0; i < G[x].size(); i++)
		if(!vis[G[x][i]])
			dfs(G[x][i]);
	indices.push_back(x);
}

int dfs2(int x)
{
	int i, ans = 1;
	vis2[x] = 1;
	for(i = 0; i < G2[x].size(); i++)
		if(!vis2[G2[x][i]])
			ans += dfs2(G2[x][i]);
	return ans;
}

main()
{
	int N, M, x, y, z, ans;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		ans = 0;
		indices.clear();
		for(int i = 1; i <= N; i++)	
		{
			G[i].clear();
			G2[i].clear();
			vis[i] = 0;
			vis2[i] = 0;
		}
		while(M--)
		{
			scanf("%d %d %d", &x, &y, &z);
			G[x].push_back(y);
			G2[y].push_back(x);
			if(z == 2)
			{
				G[y].push_back(x);
				G2[x].push_back(y);
			}
		}
		for(int i = 1; i <= N; i++)
			if(!vis[i])
				dfs(i);
		for(int i = N-1; i >= 0; i--)
			if(!vis2[indices[i]])
				ans += (dfs2(indices[i]) > 0);
		printf("%d\n", ans == 1);
	}
	return 0;
}
