// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Honorable Gift
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1782

#include<bits/stdc++.h>
#define MAXN 100005
#define MAXL 18
#define ll long long

using namespace std;

int N, M;

struct Edge
{
	int u, v, w;
};

int p[MAXN];
ll l[MAXN];
map<int, ll> valores, valores2;
map<int, ll>::iterator it;
Edge e[MAXN+MAXN];

bool cmp(Edge a, Edge b)
{
	return a.w < b.w;
}

inline int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
inline void kruskal() 
{
	int i, j, u, v, w;
	ll last;
	valores[0] = last = N;
	last = N;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
		if(l[u] < l[v])
			swap(u, v);
		if(valores.count(w) <= 0)
			valores[w] = last;
        if (u != v)
		{
			valores[w] -= (l[u]*(l[u]+1))/2 + (l[v]*(l[v]+1))/2;
            p[v] = u;
            l[u] += l[v];
			valores[w] += (l[u]*(l[u]+1))/2;
			last = valores[w];
         	j++;
		}
    }
}

main()
{
	int T, Q, x;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		scanf("%d", &N);
		valores = valores2;
		for(int i = 1; i <= N; i++)
		{
			p[i] = i;
			l[i] = 1;
		}
		M = N-1;
		for(int i = 0; i < M; i++)
			scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
		sort(e, e+M, cmp);
		kruskal();
		printf("Caso #%d:", t);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d", &x);
			it = valores.upper_bound(x);
			it--;
			printf(" %lld", it->second);
		}
		printf("\n");
	}
	return 0;
}
