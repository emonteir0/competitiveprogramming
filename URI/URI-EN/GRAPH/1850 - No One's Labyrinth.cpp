// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: No One's Labyrinth
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1850

#include<bits/stdc++.h>
#define INF 999999999
using namespace std;

int N, M;
int dist[100][100][128];
int vis[100][100][128];
char mat[100][101];

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

struct pos
{
	int x, y, bit, d;
	pos(){}
	pos(int x, int y, int bit, int d)
	{
		this->x = x;
		this->y = y;
		this->bit = bit;
		this->d = d;
	}
};

main()
{
	int x, y, bit, bit2, d, x2, y2;
	queue<pos> fila;
	//scanf("%d%*c", &N);
	/*for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);*/
	while(scanf("%s", mat[N]) != EOF)
		N++;
	//printf("%d\n", N);
	M = strlen(mat[0]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
		{
			if(mat[i][j] == '@')
			{
				x = i;
				y = j;
			}
			for(int k = 0; k < 128; k++)
				dist[i][j][k] = INF;
		}
	dist[x][y][0] = 0;
	fila.push(pos(x, y, 0, 0));
	while(!fila.empty())
	{
		x = fila.front().x;
		y = fila.front().y;
		bit = fila.front().bit;
		d = fila.front().d;
		if(mat[x][y] == '*')
			break;
		fila.pop();
		if(vis[x][y][bit])
			continue;
		vis[x][y][bit] = 1;
		for(int k = 0; k < 4; k++)
		{
			x2 = x + dx[k];
			y2 = y + dy[k];
			if(inRange(x2, y2))
			{
				if(mat[x2][y2] == '#')
					continue;
				else if(mat[x2][y2] >= 'a' && mat[x2][y2] <= 'g')
				{
					bit2 = bit | (1 << (mat[x2][y2]-'a'));
					if(dist[x2][y2][bit2] > d+1)
						fila.push(pos(x2, y2, bit2, dist[x2][y2][bit2] = d+1));
				}
				else if(mat[x2][y2] >= 'A' && mat[x2][y2] <= 'G')
				{
					if((bit & (1 << (mat[x2][y2] - 'A'))) && dist[x2][y2][bit] > d+1)
						fila.push(pos(x2, y2, bit, dist[x2][y2][bit] = d+1));
				}
				else if(dist[x2][y2][bit] > d+1)
					fila.push(pos(x2, y2, bit, dist[x2][y2][bit] = d+1));
			}
		}
	}
	if(fila.empty())
		printf("--\n");
	else
		printf("%d\n", d);
	return 0;
}
