// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Almost Shortest Path
// Level: 7
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1391

#include<bits/stdc++.h>
#define MAXN 500
#define MAX 100000000

using namespace std;

int N, M;

vector< vector< int > > G(MAXN+1), G2(MAXN+1);
int distancia[MAXN+1], distancia2[MAXN+1], vis[MAXN+1];
int adj[MAXN+1][MAXN+1];

void dijkstra(int x)
{
	int i, u, v, w, d;
	set<pair<int, int> > fila;
	pair<int, int> p;
	for(i = 0; i < N; i++)
	{
		vis[i] = 0;
		distancia[i] = MAX;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0, x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		u = p.second;
		d = p.first;
		if(!vis[u])
		{
			vis[u] = 1;
			for(i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				w = adj[u][v];
				if(!vis[v] && d + w < distancia[v])
					fila.insert(make_pair(distancia[v] = d+w, v));
			}	
		}
	}
}

void dijkstra2(int x)
{
	int i, u, v, w, d;
	set<pair<int, int> > fila;
	pair<int, int> p;
	for(i = 0; i < N; i++)
	{
		vis[i] = 0;
		distancia2[i] = MAX;
	}
	distancia2[x] = 0;
	fila.insert(make_pair(0, x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		u = p.second;
		d = p.first;
		if(!vis[u])
		{
			vis[u] = 1;
			for(i = 0; i < G2[u].size(); i++)
			{
				v = G2[u][i];
				w = adj[v][u];
				if(!vis[v] && d + w < distancia2[v])
					fila.insert(make_pair(distancia2[v] = d+w, v));
			}	
		}
	}
}

main()
{
	int i, j, u, v, w, s, d;
	while(scanf("%d %d", &N, &M) == 2 && (N||M))
	{
		scanf("%d %d", &s, &d);
		for(i = 0; i < N; i++)
		{
			G[i].clear();
			G2[i].clear();
			for(j = 0; j < N; j++)
				adj[i][j] = MAX;
		}
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			G[u].push_back(v);
			G2[v].push_back(u);
			adj[u][v] = w;
		}
		dijkstra(s);
		dijkstra2(d);
		for(i = 0; i < N; i++)
		{
			if(distancia[i] + distancia2[i] == distancia[d])
			{
				adj[s][i] = MAX;
				adj[i][d] = MAX;
			}
		}
		dijkstra(s);
		dijkstra2(d);
		printf("%d\n", distancia[d] == MAX ? -1 : distancia[d]);
	}
	return 0;
}
