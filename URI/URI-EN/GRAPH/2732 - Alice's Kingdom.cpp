// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alice's Kingdom
// Level: 5
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2732

#include<bits/stdc++.h>

using namespace std;

char G[400][401];
int vis[400][401];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};
int N, M;

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}


main()
{
	int x, y, x2, y2, ma = 0, cont;
	queue< pair<int, int> > fila;
	scanf("%d %d%*c", &N, &M);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			scanf("%c%*c", &G[i][j]);
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			if(!vis[i][j] && G[i][j] == 'C')
			{
				fila.push(make_pair(i, j));
				cont = 0;
				while(!fila.empty())
				{
					x = fila.front().first;
					y = fila.front().second;
					fila.pop();
					if(vis[x][y])
						continue;
					cont++;
					vis[x][y] = 1;
					for(int k = 0; k < 4; k++)
					{
						x2 = x + dx[k];
						y2 = y + dy[k];
						if(inRange(x2, y2) && G[x2][y2] == 'C' && !vis[x2][y2])
						{
							fila.push(make_pair(x2, y2));	
						}
					}
				}
				ma = max(ma, cont);
			}
		}
	}
	printf("%d\n", ma);
	return 0;
}
