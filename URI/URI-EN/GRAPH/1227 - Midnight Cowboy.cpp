// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Midnight Cowboy
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1227

#include<bits/stdc++.h>

using namespace std;

long double A[100][100];
long double B[100], ans[100];
int vis[100];

vector<int> G[100];

void dfs(int source, int barato, int caro)
{
	int v, tam;
	vis[source] = 1;
	A[source][source] = G[source].size();
	if(source == barato)
	{
		B[source] = G[source].size();
		return;
	}
	if(source == caro)
		return;
	tam = G[source].size();
	for(int i = 0; i < G[source].size(); i++)
	{
		v = G[source][i];
		A[source][v] = -1;
		if(!vis[v])
			dfs(v, barato, caro);
	}
}

void solve(int sz, int source, int barato, int caro)
{
	int i, j, k, ind;
	long double k1, k2;
	for(i = 0; i < sz; i++)
	{
		vis[i] = 0;
		for(j = 0; j < sz; j++)
			A[i][j] = 0;
		A[i][i] = 1;
		B[i] = 0;
	}
	dfs(source, barato, caro);
	/*printf("[");
	for(int i = 0; i < sz; i++)
	{
		printf("%.6Lf", A[i][0]);
		for(int j = 1; j < sz; j++)
		{
			printf(" %.6Lf", A[i][j]);
		}
		printf(";");
	}
	printf("]\n[");
	for(int i = 0; i < sz; i++)
		printf("%.6Lf;", B[i]);
	printf("]\n");*/
	for(i = 0; i < sz; i++)
	{
		for(j = i; j < sz; j++)
			if(A[j][i] != 0)
			{
				swap(A[j], A[i]);
				swap(B[j], B[i]);
				break;
			}
		if(i >= sz)
			break;
		for(j = i+1; j < sz; j++)
		{
			if(A[j][i] != 0)
			{
				k1 = A[i][i];
				k2 = A[j][i];
				for(k = i; k < sz; k++)
				{
					A[j][k] = A[j][k]/k2 - A[i][k]/k1;
				}
				A[j][i] = 0;
				B[j] = B[j]/k2 - B[i]/k1;
			}
		}
	}
	/*for(i = 0; i < sz; i++)
	{
		for(j = 0; j < sz; j++)
			printf("%.4lf ", A[i][j]);
		printf("= %.4lf\n", B[i]);
	}*/
	for(i = sz; i--;)
	{
		ans[i] = B[i];
		for(j = i+1; j < sz; j++)
			ans[i] -= ans[j]*A[i][j];
		ans[i] /= A[i][i];
	}
	printf("%.6Lf\n", ans[source]+1e-9);
}

main()
{
	int N, s, b, c, u, v;
	while(scanf("%d %d %d %d", &N, &s, &b, &c) == 4 && N)
	{
		for(int i = 0; i < N; i++)
			G[i].clear();
		for(int i = 1; i < N; i++)
		{
			scanf("%d %d", &u, &v);
			u--, v--;
			G[u].push_back(v);
			G[v].push_back(u);
		}
		solve(N, s-1, b-1, c-1);
	}
	return 0;
}
