// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Family Tree
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2854

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;
vector<int> G[100001];
int vis[100001];

string str, str2, f;

void dfs(int x)
{
	if(vis[x])
		return;
	vis[x] = 1;
	for(int i = 0; i < G[x].size(); i++)
		if(!vis[G[x][i]])
			dfs(G[x][i]);
}

main()
{
	int N, M, K = 0;
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	cin >> N >> M;
	while(M--)
	{
		cin >> str >> f >> str2;
		if(Mapa.count(str) == 0)
			Mapa[str] = K++;
		if(Mapa.count(str2) == 0)
			Mapa[str2] = K++;
		G[Mapa[str]].push_back(Mapa[str2]);
		G[Mapa[str2]].push_back(Mapa[str]);
	}
	int cnt = 0;
	for(int i = 0; i < K; i++)
	{
		if(!vis[i])
		{
			dfs(i);
			cnt++;
		}
	}
	printf("%d\n", cnt);
	return 0;
}
