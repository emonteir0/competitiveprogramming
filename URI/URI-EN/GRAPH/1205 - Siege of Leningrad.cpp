// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Siege of Leningrad
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1205

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;


vector<int> G[1001];
int val[1001];
int dist[1001];

int N, M, B;
double p;

void dijkstra(int s)
{
	int u, v, w;
	priority_queue< pair<int, int> > pq;
	for(int i = 1; i <= N; i++)
		dist[i] = INF;
	dist[s] = val[s];
	pq.push(make_pair(0, s));
	while(!pq.empty())
	{
		w = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(w > dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(val[v] + dist[u] < dist[v])
			{
				dist[v] = val[v] + dist[u];
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
}

double expbin(double x, int e)
{
	double y = 1;
	for(int i = 1; i <= e; i <<= 1)
	{
		if(i&e)
			y *= x;
		x = x*x;
	}
	return y;
}

main()
{
	int A, x, s, t, u, v;
	while(scanf("%d %d %d %lf", &N, &M, &B, &p) == 4)
	{
		for(int i = 1; i <= N; i++)
			G[i].clear(), val[i] = 0;
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[u].push_back(v);
			G[v].push_back(u);
		}
		scanf("%d", &A);
		while(A--)
		{
			scanf("%d", &x);
			val[x]++;
		}
		scanf("%d %d", &s, &t);
		dijkstra(s);
		if(dist[t] > B)
			printf("0.000\n");
		else
			printf("%.3lf\n", expbin(p, dist[t]));
	}
	return 0;
}
