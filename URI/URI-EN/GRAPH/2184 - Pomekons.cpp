// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pomekons
// Level: 6
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2184

#include<bits/stdc++.h>
#define MAXL 20
#define MAXN 100002
#define MAXM 5001

using namespace std;


int pai[MAXN], nivel[MAXN], ancestral[MAXN][MAXL], valores[MAXN][MAXL], ans, vet[MAXM], vet2[MAXM];
int mochila[MAXM][1001];
vector< vector<int> > G(MAXN);
map< pair<int, int>, int> W;


void dfs(int u)
{
	int i, v;
	for(i = 0; i < (int)G[u].size(); i++)
	{
		v = G[u][i];
		if(nivel[v] == -1)
		{
			pai[v] = u;
			nivel[v] = nivel[u] + 1;
			dfs(v);
		}
	}
}

int LCA(int u, int v)
{   
	int val1 = 0, val2 = 0; 
    if(nivel[u] < nivel[v]) swap(u, v);
    for(int i = MAXL-1;i >= 0;i--)
        if(nivel[u] - (1<<i) >= nivel[v])
        {
        	val1 = max(val1, valores[u][i]);
            u = ancestral[u][i];
        }
    if(u == v) 
	{
		ans = val1;
		return u;
	} 
    for(int i = MAXL-1;i >= 0;i--)
        if((ancestral[u][i] != -1) && (ancestral[u][i] != ancestral[v][i]))
		{
			val1 = max(val1, valores[u][i]);
            u = ancestral[u][i];
            val2 = max(val2, valores[v][i]);
            v = ancestral[v][i];
        }   
	ans = max(val1, val2);
	ans = max(ans, valores[u][0]);
	ans = max(ans, valores[v][0]);    
    return pai[u];
}

int main()
{
	int N, M, T, i, j, x, y, w;
	scanf("%d %d",&N, &T);
	for(i = 1; i < N; i++)
	{
		scanf("%d %d %d",&x, &y, &w);
		x--; y--;
		G[x].push_back(y);
		G[y].push_back(x);
		W[make_pair(x,y)] = w;
		W[make_pair(y,x)] = w;
	}
	for(i = 1; i < N; i++)
		nivel[i] = -1;
	dfs(0);
	for (i = 0; i < N; i++)
      for (j = 1; j < MAXL; j++)
          ancestral[i][j] = -1;
	for(i = 0; i < N; i++)
	{
		ancestral[i][0] = pai[i];
		valores[i][0] = W[make_pair(pai[i], i)];
	}
	for(j = 1; j < MAXL; j++)
		for(i = 0; i < N; i++)
			if(ancestral[i][j-1] != -1)
			{
				ancestral[i][j] = ancestral[ancestral[i][j-1]][j-1];
				valores[i][j] = max(valores[i][j-1], valores[ancestral[i][j-1]][j-1]);
			}
	scanf("%d", &M);
	for(i = 1; i <= M; i++)
	{
		scanf("%d %d", &x, &y);
		x--; y--;
		vet[i] = nivel[x] + nivel[y] - 2*nivel[(LCA(x,y))] + 1;
		vet2[i] = ans;
	}
	
	for(i = 1;i <= M; i++)
	{
		for(j = 0;j <= T;j++)
		{
			if(j >= vet[i])
			{
				if(mochila[i-1][j-vet[i]] + vet2[i] > mochila[i-1][j])
					mochila[i][j]=mochila[i-1][j-vet[i]] + vet2[i];
				else
					mochila[i][j] = mochila[i-1][j];
			}
			else
				mochila[i][j] = mochila[i-1][j];
		}
	}
	if(mochila[M][T])
		printf("%d\n", mochila[M][T]);
	else
		printf("-1\n");
	return 0;
}

