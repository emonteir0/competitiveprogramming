// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: K-th Path
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2809

#include<bits/stdc++.h>
#define INF 1001001001001001
#define ll long long
using namespace std;

int N, K;
int pai[10001];
map<int, int> spais;
vector< pair<int, int> > G[10001];

ll dist[10001][111];

void dijkstra(int start)
{
	bool cond1, cond2, cond3;
	int a, b, v, w, nxt;
	ll d, f;
	priority_queue<pair< ll, pair<int, int> > > fila;
	for(int i = 1; i <= N; i++)
		for(int j = 0; j <= 100; j++)
			dist[i][j] = INF;
	dist[start][0] = 0;
	fila.push(make_pair(0, make_pair(start, 0)));
	while(!fila.empty())
	{
		d = -fila.top().first;
		a = fila.top().second.first;
		b = fila.top().second.second;
		fila.pop();
		if(d > dist[a][b])
			continue;
		cond1 = spais.count(a) > 0; 	
		for(int i = 0; i < G[a].size(); i++)
		{
			v = G[a][i].first;
			w = G[a][i].second;
			cond2 = spais.count(v) > 0;
			cond3 = i == 0 || G[a][i].first != G[a][i-1].first;
			if(cond1 && cond2 && cond3 && (spais[a]+1 == spais[v]))
			{
				if(dist[v][b] > dist[a][b] + w)
				{
					dist[v][b] = dist[a][b] + w;
					fila.push(make_pair(-dist[v][b], make_pair(v, b)));
				}
			}
			else
			{
				nxt = min(b+1, 100);
				if(dist[v][nxt] > dist[a][b] + w)
				{
					dist[v][nxt] = dist[a][b] + w;
					fila.push(make_pair(-dist[v][nxt], make_pair(v, nxt)));
				}
			}
		}
	}
}

char str[1000001];
string s;

main()
{
	int M, Q, u, v, w, x, y, start, end;
	scanf("%d %d %d%*c", &N, &M, &Q);
	scanf("%d %d%*c", &start, &end);
	gets(str);
	s = str;
	stringstream ss(s);
	while(ss >> s)
	{
		pai[K] = atoi(s.c_str());
		spais[pai[K]] = K;
		K++;
	}
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(make_pair(v,w));
	}
	for(int i = 1; i <= N; i++)
		sort(G[i].begin(), G[i].end());
	dijkstra(start);
	for(int i = 1; i <= N; i++)
		for(int j = 100; j--; )
			dist[i][j] = min(dist[i][j], dist[i][j+1]);
	while(Q--)
	{
		scanf("%d %d", &x, &y);
		printf("%s\n", abs(dist[end][x]-dist[end][0]) > y ? "NAO": "SIM");
	}
	return 0;
}
