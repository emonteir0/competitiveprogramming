// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Separate Rooms
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1979

#include<bits/stdc++.h>
#include<strstream>

using namespace std;

vector<int> G[101];
int cores[101];

int dfs(int x, int cor)
{
	if(cores[x] != 0)
		return cores[x] == cor;
	else
		cores[x] = cor;
	int r = 1;
	for(int i = 0; i < G[x].size(); i++)
		r &= (dfs(G[x][i], cor == 1 ? 2 : 1));
	return r;
}


main()
{
	int N, x, y, r;
	string str;
	while(scanf("%d%*c", &N) == 1 && N)
	{
		for(int i = 1; i <= N; i++)
		{
			cores[i] = 0;
			G[i].clear();
			scanf("%d%*c", &x);
			getline(cin, str);
			istrstream lstr(str.c_str());
			while(lstr >> y)
			{
				G[x].push_back(y);
				G[y].push_back(x);
			}
		}
		r = 1;
		for(int i = 1; i <= N; i++)
			if(cores[i] == 0)
				r &= dfs(i, 1);
		printf("%s\n", r ? "SIM" : "NAO");
	}
	return 0;
}
