// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Penalization
// Level: 4
// Category: GRAPH
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2225

#include <bits/stdc++.h>
using namespace std;

#define INF 1000000000
#define MAX 100000

int N;
int m, x, dist[15][15], memo[15][1<<15][6];


int tsp(int u, int mask, int k) 
{
    if (mask == (1<<N)-1) 
		return 0;
	int &r = memo[u][mask][k];
    if (r != -1)
		return r;
    r = INF;
    for (int v = 0; v < N; v++)
    {
        if(!(mask&(1<<v)))
        {
            r = min(r, tsp(v, mask|(1<<v), k)+dist[u][v]);
            if(k > 0)
            	r = min(r, tsp(v, mask|(1<<v),k-1));
        }
    }
    return r;
}

int main() 
{
	int T, M, K, u, v, w, y;
	scanf("%d", &T);
    while (T--) 
	{
        scanf("%d %d %d", &N, &M, &K);
        for (int i = 0; i < N; i++)
            for (int j = i; j < N; j++)
                dist[i][j] = dist[j][i] = (i!=j)*INF;
        while (M--)
		{
            scanf("%d %d %d", &u, &v, &w);
            u--;v--;
            dist[u][v] = dist[v][u] = min(dist[u][v], w);
        }
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                for (int k = 0; k < N; k++)
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
        for(int i = 0; i < N; i++)
        	for(int j = 0; j < (1<<N); j++)
        		for(int k = 0; k <= K; k++)
        			memo[i][j][k] = -1;
        y = tsp(0, 1, K);
        printf("%d\n", y < 10000000 ? y : -1);
    }
    return 0;
}

