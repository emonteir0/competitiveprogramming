// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help!
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1367

#include<bits/stdc++.h>

using namespace std;

map<char,int> Mapa,Mapa2;

main()
{
	int i,n,x,cont,acum;
	char c,str[16];
	while(scanf("%d%*c",&n)==1&&n)
	{
		cont=0;
		acum=0;
		for(i=0;i<n;i++)
		{
			scanf("%c %d %s%*c",&c,&x,str);
			if(Mapa[c]<=0)
			{
				if(str[0]=='i')
					Mapa[c]-=20;
				else
				{
					cont++;
					acum+=x-Mapa[c];
					Mapa[c]=1;
				}
			}
		}
		printf("%d %d\n",cont,acum);
		Mapa=Mapa2;
	}
	return 0;
}
