// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Tree
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1768

#include<stdio.h>

main()
{
	int n,i,j,k;
	while(scanf("%d",&n)==1)
	{
		k=(n+n%2)/2;
		for(i=0;i<k;i++)
		{
			for(j=0;j<k-i-1;j++)
			{
				printf(" ");
			}
			for(j=0;j<2*i+1;j++)
				printf("*");
			printf("\n");
		}
		for(i=0;i<k-1;i++)
			printf(" ");
		printf("*\n");
		for(i=0;i<k-2;i++)
			printf(" ");
		printf("***\n\n");
	}
	return 0;
}
