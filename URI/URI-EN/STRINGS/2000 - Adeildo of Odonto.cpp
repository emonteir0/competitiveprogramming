// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Adeildo of Odonto
// Level: 6
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2000

#include<bits/stdc++.h>
#define ll long long
using namespace std;

struct state {
    int len, link;
    int next[26];
};
  
const int MAXLEN = 100001;
state st[MAXLEN*2];
ll dp[MAXLEN*2];
char str[MAXLEN];
int sz, last;
ll substrings;

void cleargraph(int x)
{
	for(int i = 0; i < 26; i++)
		st[x].next[i] = 0;
}  

void clonegraph(int x, int y)
{
	for(int i = 0; i < 26; i++)
		st[x].next[i] = st[y].next[i];
}

void sa_init() 
{
    sz = last = 0;
    st[0].len = 0;
    st[0].link = -1;
    cleargraph(0);
    substrings = 0;
    ++sz;
} 

void sa_extend (char c) 
{
	c -= 'a';
    int cur = sz++;
    cleargraph(cur);
    st[cur].len = st[last].len + 1;
    int p;
    for (p=last; p!=-1 && !st[p].next[c]; p=st[p].link)
        st[p].next[c] = cur;
    if (p == -1)
    {
        st[cur].link = 0;
	}
	else 
	{
        int q = st[p].next[c];
        if (st[p].len + 1 == st[q].len)
        {
            st[cur].link = q;
    	}
		else 
		{
            int clone = sz++;
            st[clone].len = st[p].len + 1;
            clonegraph(clone, q);
            st[clone].link = st[q].link;
            for (; p!=-1 && st[p].next[c]==q; p=st[p].link)
                st[p].next[c] = clone;
            st[q].link = st[cur].link = clone;
        }
    }
    last = cur;
}

ll sa_count(int ver)
{  
	ll  tp = 1;
	if(dp[ver])
		return dp[ver];
   for(int i=0;i<26;i++)
     if( st[ver].next[i] )
         tp += sa_count(st[ver].next[i]);
  
   dp[ver]=tp;
   return dp[ver];
}

main()
{
	int N;
	ll ans;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%s", str);
		sa_init();
		for(int i = 0; str[i]; i++)
			sa_extend(str[i]);
		
		for(int i = 0; i <= sz; i++)
			dp[i] = 0;
		ans = sa_count(0);
		scanf("%s", str);
		sa_init();
		for(int i = 0; str[i]; i++)
			sa_extend(str[i]);
		for(int i = 0; i <= sz; i++)
			dp[i] = 0;
		ans -= sa_count(0);
		printf("%s\n", (ans == 0) ? "s": "n");
	}
	return 0;
}
