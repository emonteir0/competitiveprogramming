// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Evergreen Trick
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2722

#include<bits/stdc++.h>

using namespace std;

main()
{
	int i, j, k, N;
	char str[101], str2[101];
	int tam, tam2;
	scanf("%d%*c", &N);
	while(N--)
	{
		gets(str);
		gets(str2);
		tam = strlen(str);
		tam2 = strlen(str2);
		i = 0;
		j = 0;
		while(i != tam)
		{
			k = i;
			for(; i < tam && i < k+2; i++)
				printf("%c", str[i]);
			k = j;
			for(; j < tam2 && j < k+2; j++)
				printf("%c", str2[j]);
		}
		printf("\n");
	}
	return 0;
}
