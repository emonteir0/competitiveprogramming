// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How Many Substrings?
// Level: 9
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1530

#include<bits/stdc++.h>
#define ll long long
using namespace std;

struct state {
    int len, link;
    map<char,int>next;
};
  
const int MAXLEN = 200001;
char str[MAXLEN];
state st[MAXLEN*2];
int sz, last;
ll substrings;
  
void sa_init() 
{
    sz = last = 0;
    st[0].len = 0;
    st[0].link = -1;
    st[0].next.clear();
    substrings = 0;
    ++sz;
}
  
void sa_extend (char c) 
{
    int cur = sz++;
    st[cur].next.clear();
    st[cur].len = st[last].len + 1;
    int p;
    for (p=last; p!=-1 && !st[p].next.count(c); p=st[p].link)
        st[p].next[c] = cur;
    if (p == -1)
    {
        st[cur].link = 0;
        substrings += st[cur].len;
	}
	else 
	{
        int q = st[p].next[c];
        if (st[p].len + 1 == st[q].len)
        {
            st[cur].link = q;
            substrings += st[cur].len - st[q].len;
    	}
		else 
		{
            int clone = sz++;
            st[clone].len = st[p].len + 1;
            st[clone].next = st[q].next;
            st[clone].link = st[q].link;
            substrings += st[clone].len - st[st[clone].link].len;
            for (; p!=-1 && st[p].next[c]==q; p=st[p].link)
                st[p].next[c] = clone;
            substrings -= st[q].len - st[st[q].link].len;
            st[q].link = st[cur].link = clone;
            substrings += st[q].len - st[st[q].link].len;
            substrings += st[cur].len - st[st[cur].link].len;
        }
    }
    last = cur;
}

main()
{
	while(scanf("%s", str) == 1)
	{
		sa_init();
		for(int i = 0; str[i]; i++)
		{
			if(str[i] != '?')
				sa_extend(str[i]);
			else
				printf("%lld\n", substrings);
		}
	}
	return 0;
}
