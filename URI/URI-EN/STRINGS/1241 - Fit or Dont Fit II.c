// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fit or Dont Fit II
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1241

#include<stdio.h>
#include<string.h>

main()
{
	int n,n1,n2;
	char a[1001],b[1001];
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%s %s",a,b);
		n1=strlen(a);
		n2=strlen(b);
		if(n1>=n2)
		{
			if(strstr(a,b)==a+n1-n2)
				printf("encaixa\n");
			else
				printf("nao encaixa\n");
		}
		else
		{
			printf("nao encaixa\n");
		}
	}
	return 0;
}
