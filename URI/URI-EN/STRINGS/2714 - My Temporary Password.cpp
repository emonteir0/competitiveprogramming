// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: My Temporary Password
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2714

#include<bits/stdc++.h>
#define ll long long

using namespace std;

main()
{
	int i, N, tam;
	ll x;
	char msg[101];
	scanf("%d", &N);
	while(N--)
	{
		scanf("%s", msg);
		tam = strlen(msg);
		if(tam != 20)
			printf("INVALID DATA\n");
		else
		{
			if(msg[0] == 'R' && msg[1] == 'A')
			{
				x = 0;
				for(i = 2; i < tam; i++)
				{
					if(msg[i] > '9' || msg[i] < '0')
						break;
					x = 10*x + msg[i] - '0';
				}
				if(i != tam)
					printf("INVALID DATA\n");
				else
				{
					if(x)
						printf("%lld\n", x);
					else
						printf("INVALID DATA\n");
				}
			}
			else
				printf("INVALID DATA\n");
		}
	}
	return 0;
}
