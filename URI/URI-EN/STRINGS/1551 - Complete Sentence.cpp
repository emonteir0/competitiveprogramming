// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Complete Sentence
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1551

#include<bits/stdc++.h>

using namespace std;

set<char> conjunto,conjunto2;

main()
{
	int N,i,tam;
	char vet[1001];
	scanf("%d%*c",&N);
	while(N--)
	{
		gets(vet);
		tam=strlen(vet);
		for(i=0;i<tam;i++)
		{
			if(vet[i]>='a'&&vet[i]<='z')
				conjunto.insert(vet[i]);
		}
		tam=conjunto.size();
		if(tam==26)			printf("frase completa\n");
		else if(tam>=13)	printf("frase quase completa\n");
		else				printf("frase mal elaborada\n");
		conjunto=conjunto2;
	}
	return 0;
}
