// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Letter Frequency
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1255

#include<stdio.h>

int vet[26];

main()
{
	char c;
	int N,i,ma;
	scanf("%d%*c",&N);
	while(N--)
	{
		while(scanf("%c",&c)&&c!='\n')
		{
			if(c>='a'&&c<='z')
				vet[c-='a']++;
			if(c>='A'&&c<='Z')
				vet[c-='A']++;
		}
		ma=vet[0];
		for(i=1;i<26;i++)
		{
			ma=ma>vet[i]?ma:vet[i];
		}
		for(i=0;i<26;i++)
		{
			if(vet[i]==ma)
				printf("%c",i+'a');
			vet[i]=0;
		}
		printf("\n");
	}
	return 0;
}
