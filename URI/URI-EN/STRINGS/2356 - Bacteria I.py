# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Bacteria I
# Level: 1
# Category: STRINGS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2356

while True:
	try:
		x = raw_input()
		y = raw_input()
		print "Resistente" if x.find(y) != -1 else "Nao resistente"
	except:
		break

