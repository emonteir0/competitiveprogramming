// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Combiner
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1238

#include<stdio.h>
#include<string.h>
#include<math.h>
main()
{
	int i,n,x,y;
	char a[1000],b[1000],c[1000];
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%s %s",a,b);
		x=strlen(a);
		y=strlen(b);
		if(x>y)
			strcpy(c,a);
		else
			strcpy(c,b);
		for(i=0;i<((x<y)?x:y);i++)
			printf("%c%c",a[i],b[i]);
		x=strlen(c);
		for(;i<x;i++)
			printf("%c",c[i]);
		printf("\n");
	}
	return 0;
}
