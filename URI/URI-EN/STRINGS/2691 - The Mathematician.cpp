// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Mathematician
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2691

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N, x, y;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d%*c%d", &x, &y);
		if(x == y)
		{
			for(int i = 5; i <= 10; i++)
				printf("%d x %d = %d\n", x, i, x*i);
		}
		if(x != y)
		{
			for(int i = 5; i <= 10; i++)
				printf("%d x %d = %d && %d x %d = %d\n", x, i, x*i, y, i, y*i);
		}
	}
	return 0;
}
