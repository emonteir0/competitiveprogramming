// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: L as in Atreus!?
// Level: 5
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2825

#include<bits/stdc++.h>

using namespace std;

int pd[101][101];

int lev(string& a, string& b)
{
	int x, c, N = a.length(), M = b.length();
	for(int i = 0; i <= N; i++)
		pd[i][0] = i;
	for(int i = 0; i <= M; i++)
		pd[0][i] = i;
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= M; j++)
		{
			c = a[i-1] == b[j-1] ? 0 : 1;
			x = min(min(pd[i-1][j]+1, pd[i][j-1]+1), pd[i-1][j-1]+c);
			pd[i][j] = x;
		}
	/*for(int i = 0; i <= N; i++)
	{
		for(int j = 0; j <= M; j++)
			printf("%d ", pd[i][j]);
		printf("\n");
	}*/
	return pd[N][M];
}

int K;
string vet[1001], str;

void solve(string& a)
{
	int me = 100001, x, ime;
	for(int i = 0; i < K; i++)
	{
		x = lev(a, vet[i]);
		if(me > x)
		{
			me = x;
			ime = i;
		}
	}
	cout << vet[ime];
}

main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	int M;
	cin >> K;
	for(int i = 0; i < K; i++)
		cin >> vet[i];
	cin >> M;
	cin >> str;
	solve(str);
	for(int i = 1; i < M; i++)
	{
		cout << " ";
		cin >> str;
		solve(str);
	}
	cout << endl;
	return 0;
}
