// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Justifier
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1273

#include<stdio.h>
#include<string.h>

main()
{
	int i,n,tam,x;
	char mat[50][51],u=0;
	while(scanf("%d",&n)==1&&n>0)
	{
		tam=0;
		for(i=0;i<n;i++)
		{
			scanf("%s",mat[i]);
			x=strlen(mat[i]);
			if(x>tam)
			{
				tam=x;
			}
		}
		if(u==0)
		{
			u=1;
		}
		else
		{			
			printf("\n");
		}
		for(i=0;i<n;i++)
		{
			printf("%*s\n",tam,mat[i]);
		}
	}
	return 0;
}
