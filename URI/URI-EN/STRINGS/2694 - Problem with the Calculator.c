// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Problem with the Calculator
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2694

#include<stdio.h>

main()
{
	int N, i, x, y;
	char msg[20];
	scanf("%d%*c", &N);
	while(N--)
	{
		y = 0;
		x = 0;
		scanf("%s", msg);
		for(i = 0; msg[i]; i++)
		{
			if(msg[i] >= '0' && msg[i] <= '9')
				x = 10*x + msg[i]-'0';
			else
			{
				y += x;
				x = 0;
			}
		}
		y += x;
		printf("%d\n", y);
	}
	return 0;
}
