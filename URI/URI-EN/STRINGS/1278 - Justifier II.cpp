// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Justifier II
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1278

#include<bits/stdc++.h>

using namespace std;

vector<string> v;

main()
{
	int N,i,tam,x,j,k=0;
	char vet[101];
	string str,str2;
	while(scanf("%d%*c",&N)&&(N!=0))
	{
		if(k==1)	printf("\n");
		else k=1;
		tam=0;
		for(i=0;i<N;i++)
		{
			gets(vet);
			str=vet;
			stringstream ss(str);
			ss >> str;
			str2=str;
			while(ss >> str)
			{
				str2+=" ";
				str2+=str;
			}
			tam=max(tam,(int)str2.length());
			v.push_back(str2);
		}
		for(i=0;i<N;i++)
		{
			str=v[i];
			x=tam-str.length();
			for(j=0;j<x;j++)
				printf(" ");
			printf("%s\n",str.c_str());
		}
		v.clear();
	}
	return 0;
}
