// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Adoption Fair
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2535

#include<bits/stdc++.h>

using namespace std;

string str;
string str2;
string str3;

main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	int N, c, x, f;
	while(cin >> N)
	{
		c = 0;
		while(N--)
		{
			while(getline(cin, str) && (!str.length()));
			while(getline(cin, str2) && (!str2.length()));
			while(getline(cin, str3) && (!str3.length()));
			if(str == "cachorro")
			{
				stringstream ss;
				ss << str3;
				x = f = 0;
				while(ss >> str3)
				{
					if(str3[0] == str2[0])
						f = 1;
					x++;
				}
				if(f && x > 1)
					c++;
			}
		}
		cout << c << endl;
	}
	return 0;
}
