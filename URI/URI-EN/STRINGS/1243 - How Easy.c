// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How Easy
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1243

#include<stdio.h>
#include<string.h>

int main(){
    int qp,t,slen,i,saw,d,e,f;
    char word[101];
    while (gets(word)){
        t=strlen(word);
        saw=qp=0;
        for (i=0;i<t;i++){
            slen=0;
            e=0;
            f=0;
            while(!(word[i]==' '||word[i]==0)&&i<=t)
            {
                d=((word[i]>='a')&&(word[i]<='z'))||((word[i]>='A')&&(word[i]<='Z'));
                e+=(word[i]=='.');
                if((e>=1)&&(!((word[i+1]==' ')||(word[i+1]==0))))
                {
                    f=1;
                }
                f+=1-(d||e==1);
                slen+=d;
                i++;
            }
            saw+=slen*((e<=1)&&(f==0));
            qp+=(e<=1)&&(f==0);
        }
        t=saw/(qp+(qp==0));
        if (t<=3) printf("250\n");
        else if (t==4 || t==5) printf("500\n");
        else printf("1000\n");
    }
    return 0;
}
