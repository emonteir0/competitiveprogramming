// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Caesar Cipher
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1253

#include<stdio.h>
#include<string.h>


main()
{
	int N,i,x,n;
	char vet[51];
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%s %d%*c",vet,&x);
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			printf("%c",'A'+((vet[i]-'A'-x)%26+26)%26);
		}
		printf("\n");
	}
	return 0;
}
