// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Laboratório de Biotecnologia
// Level: 8
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2669

#include<bits/stdc++.h>
#define pi acos(-1.0)
using namespace std;

struct cpl
{
	float a, b;
};

struct cpl2
{
	double a, b;
};

cpl operator *(const cpl &x, const cpl &y)
{
	cpl z;
	z.a = x.a*y.a - x.b*y.b;
	z.b = x.a*y.b + x.b*y.a;
	return z;
}

cpl operator +(const cpl &x, const cpl &y)
{
	cpl z;
	z.a = x.a + y.a;
	z.b = x.b + y.b;
	return z;
}

cpl operator -(const cpl &x, const cpl &y)
{
	cpl z;
	z.a = x.a - y.a;
	z.b = x.b - y.b;
	return z;
}

cpl2 operator *(const cpl2 &x, const cpl &y)
{
	cpl2 z;
	z.a = x.a*y.a - x.b*y.b;
	z.b = x.a*y.b + x.b*y.a;
	return z;
}

cpl2 operator *(const cpl2 &x, const cpl2 &y)
{
	cpl2 z;
	z.a = x.a*y.a - x.b*y.b;
	z.b = x.a*y.b + x.b*y.a;
	return z;
}

cpl2 operator +(const cpl2 &x, const cpl2 &y)
{
	cpl2 z;
	z.a = x.a + y.a;
	z.b = x.b + y.b;
	return z;
}

cpl2 operator -(const cpl2 &x, const cpl2 &y)
{
	cpl2 z;
	z.a = x.a - y.a;
	z.b = x.b - y.b;
	return z;
}


cpl X[8388620], Y[8388620]; 
int ac[100001];


void fft(cpl A[], int N, int lg)
{
	int i, j, k, m, m2 = 1;
	cpl2 w, wm, u, t, v;
	for(i = 1, j = 0; i < N; i++)
	{
		int bit = N >> 1;
		for(; j >= bit; bit>>=1) 
			j -= bit;
		j += bit;
		if (i < j) 
			swap(A[i], A[j]);
	}
	
	for(i = 1; i <= lg; i++)
	{
		m = m2 << 1;
		wm.a = cos(-2*pi/m);
		wm.b = sin(-2*pi/m);
		for(k = 0; k < N; k += m)
		{
			w.a = 1;
			w.b = 0;
			for(j = 0; j < m2; j++)
			{
				t = w*A[k + j + m2];
				u.a = A[k + j].a;
				u.b = A[k + j].b;
				v = u + t;
				A[k+j].a = v.a;
				A[k+j].b = v.b;
				v = u - t;
				A[k+j+m2].a = v.a;
				A[k+j+m2].b = v.b;
				w = w * wm;
			}
		}
		m2 = m;
	}
}

void ifft(cpl A[], int N, int lg)
{
	int i, j, k, m, m2 = 1;
	cpl2 w, wm, u, t, v;
	for(i = 1, j = 0; i < N; i++)
	{
		int bit = N >> 1;
		for(; j >= bit; bit>>=1) 
			j -= bit;
		j += bit;
		if (i < j) 
			swap(A[i], A[j]);
	}
	for(i = 1; i <= lg; i++)
	{
		m = m2 << 1;
		wm.a = cos(2*pi/m);
		wm.b = sin(2*pi/m);
		for(k = 0; k < N; k += m)
		{
			w.a = 1;
			w.b = 0;
			for(j = 0; j < m2; j++)
			{
				t = w*A[k + j + m2];
				u.a = A[k + j].a;
				u.b = A[k + j].b;
				v = u + t;
				A[k+j].a = v.a;
				A[k+j].b = v.b;
				v = u - t;
				A[k+j+m2].a = v.a;
				A[k+j+m2].b = v.b;
				w = w * wm;
			}
		}
		m2 = m;
	}
}

int main()
{
	int i, N = 0, ans = 0, tam, lg, dx, dx2;
	double t2;
	char c;
	while(scanf("%c", &c) == 1 && c != '\n')
	{
		ac[N+1] = ac[N] + c - 'a' + 1;
		N++;
	}
	tam = 1;
	lg = 0;
	dx = ac[N]+1;
	dx2 = dx + dx;
	while(tam <= dx2)
	{
		tam <<= 1;
		lg++;
	}
	for(i = 0; i <= N; i++)
	{
		X[ac[i]].a = 1;
		X[ac[i]].b = 0;
		Y[ac[N]-ac[i]].a = 1;
		Y[ac[N]-ac[i]].b = 0;
	}
	fft(X, tam, lg);
	fft(Y, tam, lg);
	for(i = 0; i < tam; i++)
		X[i] = X[i] * Y[i];
	ifft(X, tam, lg);
	t2 = tam/4;
	for(i = dx; i < dx2; i++)
		ans += X[i].a > t2;
	printf("%d\n", ans); 
	return 0;
}
