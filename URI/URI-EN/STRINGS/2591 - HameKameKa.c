// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: HameKameKa
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2591

#include<stdio.h>

main()
{
	int i, N, ca, cb;
	char vet[201];
	scanf("%d%*c", &N);
	while(N--)
	{
		gets(vet);
		ca = cb = 0;
		for(i = 0; ; i++)
			if(vet[i] == 'h')
				break;
		i++;
		for(; ; i++)
		{
			if(vet[i] != 'a')
				break;
			ca++;
		}
		for(; ; i++)
			if(vet[i] == 'k')
				break;
		i++;
		for(; ; i++)
		{
			if(vet[i] != 'a')
				break;
			cb++;
		}
		printf("k");
		ca *= cb;
		for(i = 0; i < ca; i++)
			printf("a");
		printf("\n");
	}
	return 0;
}
