// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grace Hopper, Grandma of ...
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2728

#include<bits/stdc++.h>

using namespace std;

int lower(char c)
{
	if(c >= 'a' && c <= 'z')
		return c;
	if(c >= 'A' && c <= 'Z')
		return c-'A'+'a';
	return c;
}

char vet[] = "cobol";

main()
{
	int p, N;
	string str;
	while(cin >> str)
	{
		p = 0;
		while(str.find("-") != -1)
			str.replace(str.begin()+str.find("-"), str.begin()+str.find("-")+1, " ");
		stringstream ss;
		ss << str;
		int k = 0;
		while(ss >> str)
		{
			if(!((lower(str[0]) == vet[k] || lower(str[str.length()-1]) == vet[k])))
			{
				printf("BUG\n");
				break;
			}
			k++;
		}
		while(ss >> str);
		if(k == 5)
			printf("GRACE HOPPER\n");
	}
	return 0;
}
