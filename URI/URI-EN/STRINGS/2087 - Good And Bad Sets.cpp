// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Good And Bad Sets
// Level: 4
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2087

#include<bits/stdc++.h>

using namespace std;

struct node
{
	int val, tam;
	int adj[26];
	node()
	{
		val = 0;
		tam = 0;
		for(int i = 0; i < 26; i++)
			adj[i] = -1;
	}
};

node trie[1010000];
string str[100000];
int sz, bol;

void add(string &str)
{
	int p = 0;
	for(int i = 0; i < str.length(); i++)
	{
		if(trie[p].val)
			bol = 0;
		if(trie[p].adj[str[i]-'a'] == -1)
		{
			trie[p].tam++;
			trie[p].adj[str[i]-'a'] = sz;
			trie[sz++] = node();
		}
		p = trie[p].adj[str[i]-'a'];
	}
	if(trie[p].val)
		bol = 0;
	trie[p].val = 1;
}


main()
{
	int N;
	while(cin >> N && N)
	{
		sz = 1;
		bol = 1;
		trie[0] = node();
		for(int i = 0; i < N; i++)
			cin >> str[i];
		sort(str,str+N);
		for(int i = 0; i < N && bol; i++)
			add(str[i]);
		printf("%s\n", bol ? "Conjunto Bom" : "Conjunto Ruim");
	}
	return 0;
}
