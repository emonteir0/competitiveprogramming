// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Spiral
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1868

#include<stdio.h>

main()
{
	int i,j,l,x,y,N,d;
	while(scanf("%d",&N)==1&&(N!=0))
	{
		x=N/2;
		y=N/2;
		for(i=0;i<N;i++)
		{
			for(j=0;j<N;j++)
				printf("%c",((i==y)&&(j==x))?'X':'O');
			printf("\n");
		}
		printf("@\n");
		d=0;
		while((x!=(N-1))&&(y!=(N-1)))
		{
			d++;
			for(l=0;l<d;l++)
			{
				x++;
				for(i=0;i<N;i++)
				{
					for(j=0;j<N;j++)
						printf("%c",((i==y)&&(j==x))?'X':'O');
					printf("\n");
				}
				printf("@\n");
			}
			for(l=0;l<d;l++)
			{
				y--;
				for(i=0;i<N;i++)
				{
					for(j=0;j<N;j++)
						printf("%c",((i==y)&&(j==x))?'X':'O');
					printf("\n");
				}
				printf("@\n");
			}
			d++;
			for(l=0;l<d;l++)
			{
				x--;
				for(i=0;i<N;i++)
				{
					for(j=0;j<N;j++)
						printf("%c",((i==y)&&(j==x))?'X':'O');
					printf("\n");
				}
				printf("@\n");
			}
			for(l=0;l<d;l++)
			{
				y++;
				for(i=0;i<N;i++)
				{
					for(j=0;j<N;j++)
						printf("%c",((i==y)&&(j==x))?'X':'O');
					printf("\n");
				}
				printf("@\n");
			}	
		}
		if(N>1)
		{
			d=N-1;
			for(l=0;l<d;l++)
			{
				x++;
				for(i=0;i<N;i++)
				{
					for(j=0;j<N;j++)
						printf("%c",((i==y)&&(j==x))?'X':'O');
					printf("\n");
				}
				printf("@\n");
			}
		}
	}
	return 0;
}
