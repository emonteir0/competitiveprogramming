// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Usernames
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2549

#include <bits/stdc++.h>
using namespace std;

int main() {
	int n, m, ans;
	while (cin >> n >> m) {
		ans = 0;
		cin.ignore();
		unordered_map <string, int> v;
		for (int i = 0; i < n; i++) {
			string s, k="";
			getline(cin, s);
			if (s[0] != ' ') {
				k += s[0];
			}
			for (int j = 1; j < (int)s.size(); j++) {
				if (s[j-1]==' ' && s[j]!=' ') {
					k += s[j];
				}
			}
			if (v[k]++) {
				ans++;
			}
		}
		cout << ans << endl;
	}
	
	
	return 0;
}
