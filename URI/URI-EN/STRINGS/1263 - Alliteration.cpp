// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alliteration
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1263

#include<bits/stdc++.h>

using namespace std;

int func(char a, char b)
{
	if((a>='a')&&(a<='z'))
		a-='a'-'A';
	if((b>='a')&&(b<='z'))
		b-='a'-'A';
	return a==b;
}

main()
{
	int cont;
	char vet[201];
	string a,b;
	char c,d;
	while(gets(vet)!=NULL)
	{
		a=vet;
		c='1';
		d='2';
		cont=0;
		while(a.find(" ")!=-1)
		{
		    b=a.substr(0,a.find(" "));
		    a.erase(0,a.find(" ")+1);
		    cont+=func(b[0],c)*(d!=c);
		    d=c;
		    c=b[0];
		}
		cont+=func(a[0],c)*(d!=c);
		printf("%d\n",cont);
	}
	return 0;
}
