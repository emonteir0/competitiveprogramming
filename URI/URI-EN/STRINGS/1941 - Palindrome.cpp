// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Palindrome
// Level: 4
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1941

#include<bits/stdc++.h>

using namespace std;

typedef pair<int,int> ii;

ii operator+(const ii a, const ii b)
{
	return ii(a.first+b.first, a.second+b.second);
}

char palavra[2001];
ii memo[2001][2001], vazio;
int val[2001];

ii solve(int l, int r)
{
	
	if (l > r)
		return ii(0, 0);
	if(l == r)
		return ii(val[l], 1);
	if(l + 1 == r)
		return (palavra[l] == palavra[r])? ii(val[l]+val[r], 2) : ii(max(val[l], val[r]), 1);
	if(memo[l][r] != vazio)
		return memo[l][r];
	if(palavra[l] == palavra[r])
		memo[l][r] = ii(val[l] + val[r], 2) + solve(l+1, r-1);
	return memo[l][r] = max(solve(l+1, r), max(solve(l, r-1), memo[l][r]));
}

main()
{
	int i, j, N, M, x;
	vazio = ii(-1, -1);
	scanf("%s%*c", palavra);
	N = strlen(palavra);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
			memo[i][j] = vazio;
	scanf("%d%*c", &M);
	while(M--)
	{
		scanf("%d%*c", &x);
		val[x-1] = 1;
	}
	printf("%d\n", solve(0, N-1).second);
	return 0;
}

