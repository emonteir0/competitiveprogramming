// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: LED
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1168

#include<stdio.h>

main()
{
	int n,cont=0;
	char c,vet[10]={6,2,5,5,4,5,6,3,7,6};
	scanf("%d%*c",&n);
	while(n--)
	{
		cont=0;
		while(scanf("%c",&c)==1&&c!='\n')
			cont+=vet[c-'0'];
		printf("%d leds\n",cont);
	}
	return 0;
}
