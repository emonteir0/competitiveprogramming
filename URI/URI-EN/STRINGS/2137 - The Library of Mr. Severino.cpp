// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Library of Mr. Severino
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2137

#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

typedef struct
{
	char str[5];
} palavra;

palavra vet[1000];

bool cmp(palavra a, palavra b)
{
	return strcmp(a.str,b.str)<0;
}

main()
{
	int i, n;
	while(scanf("%d%*c",&n)==1)
	{
		for(i=0;i<n;i++)
			scanf("%s%*c", vet[i].str);
		sort(vet, vet+n, cmp);
		for(i=0;i<n;i++)
			printf("%s\n", vet[i].str);
	}
	return 0;
}

