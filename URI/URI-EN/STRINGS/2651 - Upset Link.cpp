// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Upset Link
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2651

#include<bits/stdc++.h>

using namespace std;

char f(char c)
{
	if(c >= 'a' && c <= 'z')
		return c-'a'+'A';
	return c;
}

main()
{
	int i;
	string str;
	cin >> str;
	for(i = 0; i < str.size(); i++)
		str[i] = f(str[i]);
	printf("%s\n", str.find("ZELDA") == -1 ? "Link Tranquilo" : "Link Bolado");
	return 0;
}
