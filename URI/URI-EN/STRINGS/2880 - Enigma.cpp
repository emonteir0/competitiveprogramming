// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Enigma
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2880

#include<bits/stdc++.h>

using namespace std;

char str[20001], str2[20001];
int match[10001];

main()
{
	int s1, s2, len;
	scanf("%s %s", str, str2);
	s1 = strlen(str);
	s2 = strlen(str2);
	int ans = len = s1-s2+1;
	for(int i = 0; str2[i]; i++)
	{
		for(int j = i; j < i+len; j++)
		{
			//printf("%c %c\n", str[j], str2[i]);
			if(str[j] == str2[i])
			{
				if(!match[j-i])
				{
					//printf("%d %d\n", i, j);
					ans--;
					match[j-i] = 1;
				}
			}
		}
	}
	printf("%d\n", ans);
	return 0;
}
