// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Letter Range
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1276

#include<stdio.h>
#include<string.h>


bool caracteres[26];

main()
{
	char vet[51],c,k;
	int i,n;
	while(gets(vet)!=NULL)
	{
		memset(caracteres,false,26);
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			if(vet[i]>='a'&&vet[i]<='z')
				caracteres[vet[i]-'a']=true;
		}
		k=0;
		for(i=0;i<26;i++)
		{
			if(caracteres[i]==true)
			{
				if(k==0)
				{
					printf("%c:",i+'a');
					k=1;
				}
				else
					printf(", %c:",i+'a');
				for(;i<26;i++)
				{
					if(caracteres[i]==true)
						c=i;
					else
						break;
				}
				printf("%c",c+'a');
			}
		}
		printf("\n");
	}
	return 0;
}
