// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alien Vowels
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2150

#include<cstdio>
#include<cstring>
#include<set>

using namespace std;

set<char> conj, conj2;

char palavra1[1001], palavra2[1001];

main()
{
	int i, cont, tam;
	while(scanf("%s%*c",palavra1)==1)
	{
		gets(palavra2);
		cont = 0;
		tam = strlen(palavra1);
		for(i=0;i<tam;i++)
			conj.insert(palavra1[i]);
		tam = strlen(palavra2);
		for(i=0;i<tam;i++)
			cont += (conj.find(palavra2[i])!=conj.end());
		printf("%d\n",cont);
		conj = conj2;
	}
	return 0;
}

