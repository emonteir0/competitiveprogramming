// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mirror Sequence
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2157

#include<stdio.h>

void rev(int n)
{
	while(n > 0)
	{
		printf("%d",n%10);
		n/=10;
	}
}

main()
{
	int i, j, x1, x2, n;
	scanf("%d",&n);
	for(i = 0; i < n; i++)
	{
		scanf("%d %d",&x1,&x2);
		for(j=x1;j<=x2;j++)
			printf("%d",j);
		for(j=x2;j>=x1;j--)
			rev(j);
		printf("\n");
	}
	return 0;
}

