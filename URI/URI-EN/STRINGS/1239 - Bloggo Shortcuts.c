// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bloggo Shortcuts
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1239

#include<stdio.h>
#include<string.h>


main()
{
	char vet[51],a=0,b=0;
	int i,n;
	while(gets(vet)!=NULL)
	{
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			if(vet[i]=='*')
			{
				printf("%s",a==0?"<b>":"</b>");
				a=1-a;
			}
			else if(vet[i]=='_')
			{
				printf("%s",b==0?"<i>":"</i>");
				b=1-b;
			}
			else
				printf("%c",vet[i]);
		}
		printf("\n");
	}
	return 0;
}

