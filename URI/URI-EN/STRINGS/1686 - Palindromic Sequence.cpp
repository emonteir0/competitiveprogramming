// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Palindromic Sequence
// Level: 7
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1686

#include<bits/stdc++.h>
#define ll long long
using namespace std;

int P[1000001];
char s[1000001];
char T[3000001];

int prepare() 
{
	int k = 1;
	T[0] = '^';
	for (int i = 0; s[i]; i++)
	{
		T[k++] = '#';
		T[k++] = s[i];
	}
	T[k++] = '#';
	T[k++] = '$';
	T[k] = 0;
	return k;
}
 
void buildManacher() 
{
	
	int n = prepare();
	int C = 0, R = 0;
	for (int i = 1; i < n-1; i++) 
	{
		int i_mirror = 2*C-i;
	
		P[i] = (R > i) ? min(R-i, P[i_mirror]) : 0;
	
		while (T[i + 1 + P[i]] == T[i - 1 - P[i]])
			P[i]++;
	
		if (i + P[i] > R) 
		{
			C = i;
			R = i + P[i];
		}
	}

	int maxLen = 0;
	int centerIndex = 0;
	for (int i = 1; i < n-1; i++) 
	{
		if (P[i] > maxLen) 
		{
			maxLen = P[i];
			centerIndex = i;
		}
	}
	//return s.substr((centerIndex - 1 - maxLen)/2, maxLen);
}

int N;
ll vet[1000001];

ll getMaxArea()
{
    ll max_area = 0, area;
    int tp, i = 0;
    stack<int> pilha;
    while (i < N)
    {
        if (pilha.empty() || vet[pilha.top()] <= vet[i])
            pilha.push(i++);
        else
        {
            tp = pilha.top();
            pilha.pop();
            area = vet[tp] * (pilha.empty() ? i : i - pilha.top() - 1);
            if (max_area < area)
                max_area = area;
        }
    }
    while (!pilha.empty())
    {
        tp = pilha.top();
        pilha.pop();
        area = vet[tp] * (pilha.empty() ? i : i - pilha.top() - 1);
 
        if (max_area < area)
            max_area = area;
    }
 
    return max_area;
}

int main()
{
	int i, j, n, d, end, lim, step;
	ll ans;
	while(scanf("%d %d", &n, &d) != EOF && (n || d))
	{
		ans = 0;
		scanf("%s", s);
		buildManacher();
		end = min(2*d, 2*n);
		lim = 2*n;
		step = 2*d;
		for(i = 2; i <= end; i++)
		{
			N = 0;
			for(j = i; j <= lim; j += step)
				vet[N++] = min(P[j], i&1 ? 2*(d/2) : 2*(d-1)/2 + 1);
			ans = max(ans, getMaxArea());
		}
		cout << ans << endl;
	}
	return 0;
}
