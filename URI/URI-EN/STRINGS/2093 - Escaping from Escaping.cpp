// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escaping from Escaping
// Level: 7
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2093

#include<bits/stdc++.h>

using namespace std;

char str[100001];
int vis[33554432];


main()
{
	int c, N = 1, dig = 0, num;
	scanf("%s", str);
	while(1)
	{
		N <<= 1;
		dig++;
		c = N-1;
		num = 0;
		for(int i = 0; i < dig; i++)
			num = (num << 1) + str[i]-'0';
		vis[num] = dig;
		for(int i = dig; str[i]; i++)
		{
			num <<= 1;
			if(num >= N)
				num -= N;
			num += str[i]-'0';
			if(vis[num] < dig)
			{
				vis[num] = dig;
				c--;
				if(c == 0)
					break;
			}
		}
		if(c != 0)
			return !printf("%d\n", dig);
	}
	return 0;
}
