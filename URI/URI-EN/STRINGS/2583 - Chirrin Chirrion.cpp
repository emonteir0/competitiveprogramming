// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Chirrin Chirrion
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2583

#include<bits/stdc++.h>

using namespace std;

set<string> conjunto, conjunto2;
set<string>::iterator it;

char msg[101], msg2[101];

string str;

main()
{
	int T, N;
	scanf("%d", &T);
	while(T--)
	{
		conjunto = conjunto2;
		scanf("%d%*c", &N);
		while(N--)
		{
			scanf("%s %s", msg, msg2);
			str = msg;
			if(strcmp(msg2, "chirrin") == 0)
				conjunto.insert(str);
			else if(strcmp(msg2, "chirrion") == 0)
				conjunto.erase(str);
		}
		printf("TOTAL\n");
		for(it = conjunto.begin(); it != conjunto.end(); ++it)
			printf("%s\n", it->c_str());
	}
	return 0;
}

