// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Characters
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2108

#include<stdio.h>
#include<string.h>

main()
{
	char palavra[101], op, mapalavra[101];
	int k=0, ma=0, tam;
	while(scanf("%s%c",palavra,&op)==2)
	{
		if(palavra[0] == '0' && palavra[1] == 0)
			break;
		if(k++>0)
			printf("-");
		tam = strlen(palavra);
		printf("%d", tam);
		if(op=='\n')
		{
			k = 0;
			printf("\n");
		}
		if(tam >= ma)
		{
			ma = tam;
			strcpy(mapalavra,palavra);
		}
	}
	printf("\nThe biggest word: %s\n", mapalavra);
	return 0;
}

