// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Huaauhahhuahau
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2242

#include<stdio.h>

main()
{
    int i, tam = -1;
    char mensagem[51], mensagem2[51], c;
    scanf("%s", mensagem);
    for(i=0;mensagem[i];i++)
    {
        c = mensagem[i];
        if((c=='a')||(c=='e')||(c=='i')||(c=='o')||(c=='u'))
            mensagem2[++tam] = c;
    }
    for(i=0;i<=tam;i++)
    {
        if(mensagem2[i]!=mensagem2[tam-i])
        {
            printf("N\n");
            return 0;
        }
    }
    printf("S\n");
    return 0;
}

