// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pomekon Collection
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2174

#include<bits/stdc++.h>

using namespace std;

set<string> pomekons;

main()
{
	int N;
	char palavra[1001];
	string str;
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%s",palavra);
		str = palavra;
		pomekons.insert(str);
	}
	printf("Falta(m) %d pomekon(s).\n",151-pomekons.size());
}

