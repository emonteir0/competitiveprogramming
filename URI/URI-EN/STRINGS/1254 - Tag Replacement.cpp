// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tag Replacement
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1254

#include<bits/stdc++.h>

using namespace std;

string a, b, c, d;
vector<int> ans;

string lowerstr(string &s)
{
	string t = s;
	for(int i = 0; i < (int)t.length(); i++)
	{
		if(t[i] >= 'A' && t[i] <= 'Z')
			t[i] = t[i]-'A'+'a';
	}
	return t;
}

char palavra[51];

int main()
{
	int x, k;
	while(gets(palavra))
	{
		a = palavra;
		gets(palavra);
		b = palavra;
		gets(palavra);
		c = palavra;
		d = lowerstr(c);
		ans.clear();
		for(int i = 0; i < (int)d.length(); i++)
		{
			if(d[i] == '<')
			{
				for(int j = i+1; j < (int)d.length(); j++)
				{
					if(d[j] == '>')
					{
						x = i;
						while(1)
						{
							x = d.find(lowerstr(a), x);
							if(x != (int)d.npos && x < j)
							{
								ans.push_back(x);
								x = x+a.length();
							}
							else
								break;
						}
						i = j;
						break;
					}
				}
			}
		}
		k = 0;
		for(int i = 0; i < (int)c.length(); i++)
		{
			if(k < (int)ans.size() && ans[k] == i)
			{
				printf("%s", b.c_str());
				i += a.length()-1;
				k++;
			}
			else
				printf("%c", c[i]);
		}
		printf("\n");
	}
	return 0;
}
