// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Compare Substring
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1237

#include<bits/stdc++.h>

using namespace std;

main()
{
	char X[51],Y[51];
	int n,m,i,j,k,l,x,maximo;
	while(gets(X)!=NULL)
	{ 
		gets(Y);
		n = strlen(X);
		m = strlen(Y); 
		maximo=0;  
		for(i=1;i<=n;i++)
		  for(j=1;j<=m;j++)
		    if(X[i-1]==Y[j-1])
		    {
		    	k=i;
		    	l=j;
		    	x=0;
		    	while(X[k-1]==Y[l-1])
		    	{
		    		k++;
		    		l++;
		    		x++;
		    		if((j>m)||(k>n))
		    			break;
		    	}
		    	maximo=max(x,maximo);
			}
		printf("%d\n",maximo);
	}
	return 0;
}

