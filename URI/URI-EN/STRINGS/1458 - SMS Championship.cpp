// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: SMS Championship
// Level: 4
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1458

#include<bits/stdc++.h>
#define INF 1e50

using namespace std;


char str[141], str2[282];
double dp[282][11][11];
double dist[10][10];
int N;

int grupo[26] = {2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 9};
int press[26] = {1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4};

double solve(int pos, int esq, int dir)
{
	if(pos == N)
		return 0;
	if(dp[pos][esq][dir] != -1)
		return dp[pos][esq][dir];
	double ans = INF;
	if(str2[pos] >= 'a' && str2[pos] <= 'z')
		ans = min(solve(pos+1, grupo[str2[pos]-'a'], dir) + dist[esq][grupo[str2[pos]-'a']], solve(pos+1, esq, grupo[str2[pos]-'a']) + dist[dir][grupo[str2[pos]-'a']]) + press[str2[pos]-'a']*0.2;
	else if(str2[pos] == ' ')
		ans = min(solve(pos+1, 0, dir) + dist[esq][0], solve(pos+1, esq, 0) + dist[dir][0]) + 0.2;
	else
		ans = min(solve(pos+1, 1, dir) + dist[esq][1], solve(pos+1, esq, 1) + dist[dir][1]) + 0.2;
	return dp[pos][esq][dir] = ans;	
}

int sqr(int x)
{
	return x*x;
}

main()
{
	int k;
	for(int i = 2; i <= 9; i++)
		for(int j = 2; j <= 9; j++)
		{
			int x = i-1, y = j-1;
			dist[i][j] = dist[j][i] = sqrt(sqr(abs(x%3 - y%3)) + sqr(abs(x/3 - y/3))) / 30.0;
		}
	for(int i = 2; i <= 9; i++)
	{
		int x = i-1;
		dist[i][0] = dist[0][i] = sqrt(sqr(abs(x%3 - 1)) + sqr(abs(x/3 - 3))) / 30.0;
		dist[i][1] = dist[1][i] = sqrt(sqr(abs(x%3 - 2)) + sqr(abs(x/3 - 3))) / 30.0;
	}
	dist[0][1] = dist[1][0] = 1 / 30.0;
	/*for(int i = 0; i <= 9; i++)
	{
		for(int j = 0; j <= 9; j++)
			printf("%2.0lf ", dist[i][j]*dist[i][j]);
		printf("\n");
	}*/
	while(gets(str) != NULL)
	{
		str2[0] = str[0];
		N = 1;
		for(int i = 1; str[i]; i++)
		{
			if(str[i] >= 'a' && str[i] <= 'z')
				if(grupo[str[i]-'a'] == grupo[str[i-1]-'a'])
					str2[N++] = '#';
			str2[N++] = str[i];
		}
		str2[N] = 0;
		//printf("%s\n", str2);
		for(int i = 0; str2[i]; i++)
			for(int j = 0; j < 10; j++)
				for(int k = 0; k < 10; k++)
					dp[i][j][k] = -1;
		printf("%.2lf\n", solve(0, 4, 6));
	}
	return 0;
}
