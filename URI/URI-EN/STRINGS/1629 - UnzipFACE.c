// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: UnzipFACE
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1629

#include<stdio.h>

main()
{
	int i,N,cont,conta,contb;
	char vet[1001];
	while((scanf("%d%*c",&N)==1)&&(N!=0))
	{
		while(N--)
		{
			cont=0;
			conta=0;
			contb=0;
			scanf("%s%*c",vet);
			for(i=0;vet[i]!=0;i++)
			{
				if(i%2==0)
					conta+=vet[i]-'0';
				else
					contb+=vet[i]-'0';
			}
			while(conta!=0)
			{
				cont+=conta%10;
				conta/=10;
			}
			while(contb!=0)
			{
				cont+=contb%10;
				contb/=10;
			}
			printf("%d\n",cont);
		}
	}
	return 0;
}

