// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Zero means Zero
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1871

#include<stdio.h>
#include<string.h>
 
main()
{
    unsigned int a,b,c;
    int i,k;
    char d[10];
    while(scanf("%u %u",&a,&b)==2&&(a||b))
    {
    	c=a+b;
    	k=0;
    	while(c>=1)
    	{
    		if(c%10!=0)
    		{
    			d[k++]=(c%10)+'0';
    		}
    		c/=10;
    	}
    	for(i=k-1;i>=0;i--)
    		printf("%c",d[i]);
    	printf("\n");
    }
    return 0;
}
