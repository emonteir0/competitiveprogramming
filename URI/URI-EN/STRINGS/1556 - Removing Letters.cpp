// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Removing Letters
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1556

#include<bits/stdc++.h>

using namespace std;

int vis[1001];
int adj[1001][26];
int sz;

void dfs(char* str, int tam, int v)
{
	str[tam] = 0;
	if(tam != 0)
		printf("%s\n", str);
	for(int i = 0; i < 26; i++)
	{
		if(adj[v][i] != -1)
		{
			str[tam] = i+'a';
			dfs(str, tam+1, adj[v][i]);
		}
	}
}

main()
{
	string str;
	char str2[1001];
	while(cin >> str)
	{
		sz = 1;
		for(int i = 0; i < 26; i++)
			adj[0][i] = -1;
		for(int i = 0; i < str.length(); i++)
		{
			int sz2 = sz;
			for(int j = 0; j < sz2; j++)
				if(adj[j][str[i]-'a'] == -1)
				{
					adj[j][str[i]-'a'] = sz;
					for(int k = 0; k < 26; k++)
						adj[sz][k] = -1;
					sz++;
				}
		}
		dfs(str2, 0, 0);
		printf("\n");
	}
	return 0;
}
