// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: ABC
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2336

#include<stdio.h>
#define MOD 1000000007
#define ll long long

main()
{
	char msg[1001];
	ll val;
	int i;
	char c;
	while(scanf("%s", msg) == 1)
	{
		val = 0;
		for(i = 0; (c = msg[i]) != 0; i++)
			val = (26*val + c-'A')%MOD;
		printf("%lld\n", val);
	}
	return 0;
}
