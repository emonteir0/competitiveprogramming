// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Not One More Canadian Game!
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2132

#include<stdio.h>
#define ll long long

main()
{
	int i, k = 1;
	ll x;
	char msg[61];
	while(gets(msg) != NULL)
	{
		x = 0;
		for(i = 0; msg[i]; i++)
			x = (x<<1) + msg[i]-'a';
		printf("Palavra %d\n%lld\n\n", k++, x);
	}
	return 0;
}

