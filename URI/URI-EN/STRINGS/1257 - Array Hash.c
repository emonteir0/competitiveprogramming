// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Hash
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1257

#include<stdio.h>
#include<string.h>

main()
{
	int n,m,x,i,j,cont;
	char vet[51];
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%d%*c",&m);
		cont=0;
		for(i=0;i<m;i++)
		{
			scanf("%s",vet);
			x=strlen(vet);
			cont+=x*(x-1)/2+i*x;
			for(j=0;j<x;j++)	cont+=vet[j]-'A';
		}
		printf("%d\n",cont);
	}
	return 0;
}
