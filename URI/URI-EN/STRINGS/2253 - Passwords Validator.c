// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Passwords Validator
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2253

#include<stdio.h>
#include<string.h>

main()
{
	int tam, b1, b2, b3, b4, i;
	char mensagem[1001], c;
	while(gets(mensagem) != NULL)
	{
		b1 = 0;
		b2 = 0;
		b3 = 0;
		b4 = 1;
		tam = strlen(mensagem);
		if(tam >= 6 && tam <= 32)
		{
			for(i = 0; i < tam; i++)
			{
				c = mensagem[i];
				b1 |= (c>='a') && (c<='z');
				b2 |= (c>='A') && (c<='Z');
				b3 |= (c>='0') && (c<='9');
				b4 &= (((c>='a') && (c<='z')) || ((c>='A') && (c<='Z')) || ((c>='0') && (c<='9')));
			}
			printf("%s\n", (b1 && b2 && b3 && b4)? "Senha valida.":"Senha invalida.");
		}
		else
			printf("Senha invalida.\n");
	}
	return 0;
}
