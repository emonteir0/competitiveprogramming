// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Advancing Letters
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1607

#include<stdio.h>
#include<string.h>

int absol(int a)
{
	if(a<0)
		return -a;
	return a;
}

char vet[10001];

main()
{
	int N,n2,i,cont,x;
	char c;
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%s%*c",&vet);
		cont=0;
		n2=strlen(vet);
		for(i=0;i<n2;i++)
		{
			scanf("%c",&c);
			x=c-vet[i];
			cont+=x+26*(x<0);
		}
		printf("%d\n",cont);
	}
	return 0;
}
