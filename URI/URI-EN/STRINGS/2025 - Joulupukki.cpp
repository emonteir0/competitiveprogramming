// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Joulupukki
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2025

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N, x;
	string str;
	scanf("%d%*c", &N);
	while(N--)
	{
		getline(cin, str);
		x = -1;
		while(str.find("oulupukk", x+1) != str.npos)
		{
			x = str.find("oulupukk", x+1);
			if(x > 0 && x+8 < str.length() )
			{
				str[x-1] = 'J';
				str[x+8] = 'i';
			}
		}
		cout << str << endl;
	}
	return 0;
}
