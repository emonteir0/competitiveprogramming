// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: OBI URI
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2062

#include<stdio.h>
#include<string.h>

main()
{
	int n,i;
	char vet[21];
	scanf("%d%*c",&n);
	scanf("%s%*c",vet);
	if(strlen(vet)==3)
	{
		if(vet[0] == 'O' && vet[1] == 'B')
			printf("OBI");
		else if(vet[0] == 'U' && vet[1] == 'R')
			printf("URI");
		else
			printf("%s",vet);
	}
	else
		printf(" %s",vet);
	for(i=1;i<n;i++)
	{
		scanf("%s%*c",vet);
		if(strlen(vet)==3)
		{
			if(vet[0] == 'O' && vet[1] == 'B')
				printf(" OBI");
			else if(vet[0] == 'U' && vet[1] == 'R')
				printf(" URI");
			else
				printf(" %s",vet);
		}
		else
			printf(" %s",vet);
	}
	printf("\n");
	return 0;
}

