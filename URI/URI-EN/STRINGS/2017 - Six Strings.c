// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Six Strings
// Level: 5
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2017

#include<stdio.h>
#include<string.h>

char msg[100001], msg2[100001];

main()
{
	int i, j, ind, K, tam, best, cont;
	scanf("%s%*c%d", msg, &K);
	tam = strlen(msg);
	best = K;
	for(i = 1; i <= 5; i++)
	{
		scanf("%s", msg2);
		cont = 0;
		for(j = 0; j < tam; j++)
			cont += msg[j] != msg2[j];
		if(cont < best)
		{
			ind = i;
			best = cont;
		}
	}
	if(best == K)	printf("-1\n");
	else	printf("%d\n%d\n", ind, best);
	return 0;
}
