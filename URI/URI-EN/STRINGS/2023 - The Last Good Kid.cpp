// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Last Good Kid
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2023

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<string>
#include<locale>

using namespace std;

typedef struct
{
	string str,str2;
}dupla;

dupla criancas[1001];

void toupper(char* str)
{
	int i;
	for(i=0;str[i]!=0;i++)
		if((str[i]>='a')&&(str[i]<='z'))
			str[i]=str[i]-'a'+'A';
}

bool cmp(dupla a, dupla b)
{
	return strcmp(a.str2.c_str(),b.str2.c_str())<0;
}

int main() 
{
	char nome[101];
	int i,n=0;
	while(gets(nome)!=NULL)
	{
		criancas[n].str=nome;
		toupper(nome);
		criancas[n++].str2=nome;
	}
	sort(criancas,criancas+n,cmp);
	printf("%s\n",criancas[n-1].str.c_str());
	return 0;
}
