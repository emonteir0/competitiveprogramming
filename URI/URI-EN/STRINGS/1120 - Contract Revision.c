// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Contract Revision
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1120

#include<stdio.h>

main()
{
	int n,i;
	char vet[101],b;
	while((scanf("%d %s%*c",&n,vet)==2)&&(n||(vet[0]!='0')))
	{
		b=0;
		for(i=0;vet[i]!=0;i++)
		{
			if(vet[i]!=n+'0')
			{
				if(vet[i]!='0')
				{
					printf("%c",vet[i]);
					b=1;
				}
				else if(b==1)
					printf("0");
			}
		}
		if(b==0)
			printf("0");
		printf("\n");
	}
	return 0;
}

