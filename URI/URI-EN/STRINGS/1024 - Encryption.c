// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Encryption
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1024

#include<stdio.h>
#include<string.h>

main()
{
	int n,i,x;
	char v[1001];
	scanf("%d%*c",&n);
	while((n--)>0)
	{
		gets(v);
		x=strlen(v);
		for(i=x-1;i>=(x/2+x%2);i--)
			printf("%c",v[i]+3*((v[i]>='a'&&v[i]<='z')||(v[i]>='A'&&v[i]<='Z')));
		i++;
		while(i--)
			printf("%c",v[i]+3*((v[i]>='a'&&v[i]<='z')||(v[i]>='A'&&v[i]<='Z'))-1);
		printf("\n");
	}
	return 0;
}
