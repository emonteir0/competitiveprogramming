// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: One-Two-Three
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1332

#include<stdio.h>
#include<string.h>

main()
{
	int n,nx;
	char vet[6];
	scanf("%d%*c",&n);
	while(n--)
	{
		gets(vet);
		nx=strlen(vet);
		if(((vet[0]=='o')+(vet[1]=='n')+(vet[2]=='e'))>1)
			printf("1\n");
		else if(((vet[0]=='t')+(vet[1]=='w')+(vet[2]=='o'))>1)
			printf("2\n");
		else if(((vet[0]=='t')+(vet[1]=='h')+(vet[2]=='r')+(vet[3]=='e')+(vet[4]=='e'))>3)
			printf("3\n");
	}
	return 0;
}
