// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Chinese Whispers
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1448

#include<stdio.h>
main()
{
	int i,k,n,cont,cont2,a,b,c;
	char x;
	char vet[101],vet2[101],vet3[101];
	scanf("%d%*c",&n);
	for(k=1;k<=n;k++)
	{
		cont=0;
		cont2=0;
		gets(vet);
		gets(vet2);
		gets(vet3);
		x=vet[0];
		for(i=0;x!=0;i++)
		{
			cont+=(vet2[i]==x);
			cont2+=(vet3[i]==x);
			x=vet[i+1];
		}
		printf("Instancia %d\n",k);
		if(cont==cont2)
		{
			x=vet[0];
			c=1;
			for(i=0;x!=0;i++)
			{
				a=(vet2[i]==x);
				b=(vet3[i]==x);
				if(a&&(!b))
				{
					printf("time 1\n\n");
					c=0;
					break;
				}
				if((!a)&&b)
				{
					printf("time 2\n\n");
					c=0;
					break;
				}
				x=vet[i+1];
			}
			if(c==1)
				printf("empate\n\n");
		}
		else
		{
			if(cont>cont2)
				printf("time 1\n\n");
			else
				printf("time 2\n\n");
		}
	}
	return 0;
}

