// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Number to Name
// Level: 5
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1846

#include<stdio.h>

void criarnum(int n)
{
	char vet[10][21]={"","cento","duzentos","trezentos","quatrocentos","quinhentos","seiscentos","setecentos","oitocentos","novecentos"};
	char vet2[10][21]={"","um","dois","tres","quatro","cinco","seis","sete","oito","nove"};
	char vet3[10][21]={"","onze","doze","treze","quatorze","quinze","dezesseis","dezessete","dezoito","dezenove"};
	char vet4[10][21]={"","","vinte","trinta","quarenta","cinquenta","sessenta","setenta","oitenta","noventa"};
	if(n==0)
	{
		printf("zero");
		return;
	}
	if(n==100)
	{
		printf("cem");
		return;
	}
	if(n>100)
	{
		printf("%s",vet[n/100]);
		if((n%100)>0)
			printf(" e ");
		else
			return;
	}
	n=n%100;
	if(n<10)
	{
		printf("%s",vet2[n]);
	}
	else if(n==10)
		printf("dez");
	else if(n<20)
	{
		printf("%s",vet3[n-10]);
	}
	else
	{
		printf("%s",vet4[n/10]);
		n=n%10;
		if(n>0)
			printf(" e ");
		printf("%s",vet2[n]);
	}
}



main()
{
	int n,n1,n2;
	while(scanf("%d",&n)==1)
	{
		if(n==0)
			printf("zero\n");
		else
		{
			n1=n/1000;
			n2=n%1000;
			if(n1>0)
			{
				if(n1!=1)
				{
					criarnum(n1);
					printf(" mil");
				}
				else
					printf("mil");
			}
			if(n2!=0)
			{
				if(n1>0&&n2<100)
					printf(" e ");
				else if(n1>0&&n2>=100&&n2%100!=0)
					printf(" ");
				else if(n1>0&&n2>=100&&n2%100==0)
					printf(" e ");
				criarnum(n2);
			}
			printf("\n");
			n=n%10;
		}
	}
	return 0;
}
