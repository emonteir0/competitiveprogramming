// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Voice Dialing
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2678

#include<stdio.h>

char msg[1001];

main()
{
	int i;
	while(gets(msg) != NULL)
	{
		for(i = 0; msg[i]; i++)
		{
			if(((msg[i] >= '0') && (msg[i] <= '9')) || (msg[i] == '*') || (msg[i] == '#'))
				printf("%c", msg[i]);
			else if((msg[i] >= 'A') && (msg[i] <= 'C'))
				printf("2");
			else if((msg[i] >= 'D') && (msg[i] <= 'F'))
				printf("3");
			else if((msg[i] >= 'G') && (msg[i] <= 'I'))
				printf("4");
			else if((msg[i] >= 'J') && (msg[i] <= 'L'))
				printf("5");
			else if((msg[i] >= 'M') && (msg[i] <= 'O'))
				printf("6");
			else if((msg[i] >= 'P') && (msg[i] <= 'S'))
				printf("7");
			else if((msg[i] >= 'T') && (msg[i] <= 'V'))
				printf("8");
			else if((msg[i] >= 'W') && (msg[i] <= 'Z'))
				printf("9");
			else if((msg[i] >= 'a') && (msg[i] <= 'c'))
				printf("2");
			else if((msg[i] >= 'd') && (msg[i] <= 'f'))
				printf("3");
			else if((msg[i] >= 'g') && (msg[i] <= 'i'))
				printf("4");
			else if((msg[i] >= 'j') && (msg[i] <= 'l'))
				printf("5");
			else if((msg[i] >= 'm') && (msg[i] <= 'o'))
				printf("6");
			else if((msg[i] >= 'p') && (msg[i] <= 's'))
				printf("7");
			else if((msg[i] >= 't') && (msg[i] <= 'v'))
				printf("8");
			else if((msg[i] >= 'w') && (msg[i] <= 'z'))
				printf("9");
		}
		printf("\n");
	}
	return 0;
}
