// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multiple Reading
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1262

#include<stdio.h>
#include<string.h>

main()
{
	int i,n,x,cont,cont2;
	char vet[51];
	while(scanf("%s",vet)==1)
	{
		scanf("%d%*c",&x);
		n=strlen(vet);
		cont=0;
		for(i=0;i<n;i++)
		{
			if(vet[i]=='W')
				cont++;
			else if(vet[i]=='R')
			{
				cont2=1;
				i++;
				while(1)
				{
					if(i==n)	break;
					if(vet[i]=='R')	cont2++;
					else	 break;
					i++;
				}
				i--;
				cont+=(cont2/x)+(cont2%x!=0);
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
