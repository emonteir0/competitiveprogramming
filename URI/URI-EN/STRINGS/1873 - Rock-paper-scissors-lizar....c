// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rock-paper-scissors-lizar...
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1873

#include<stdio.h>
#include<string.h>
 
main()
{
    int n,i,x,y,k;
    char vet[11],vet2[11],palavras[5][11]={"tesoura","papel","pedra","lagarto","spock"};
    scanf("%d%*c",&n);
    for(k=1;k<=n;k++)
    {
        scanf("%s %s",vet,vet2);
        for(i=0;i<5;i++)
        {
            if(strcmp(vet,palavras[i])==0)
                x=i;
            if(strcmp(vet2,palavras[i])==0)
                y=i;        
        }
        if(x==y)
            printf("empate\n");
        else if(((x+1)%5==y)||((x+3)%5==y))
            printf("rajesh\n");
        else
            printf("sheldon\n");
    }
    return 0;
}
