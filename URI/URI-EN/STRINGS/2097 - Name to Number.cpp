// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Name to Number
// Level: 6
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2097

#include<bits/stdc++.h>
#define ll long long
using namespace std;

map<string, ll> Mapa, Mapa2;

main()
{
	ll numero, casas;
	string str;
	Mapa["um"] = 1;
	Mapa["dois"] = 2;
	Mapa["tres"] = 3;
	Mapa["quatro"] = 4;
	Mapa["cinco"] = 5;
	Mapa["seis"] = 6;
	Mapa["sete"] = 7;
	Mapa["oito"] = 8;
	Mapa["nove"] = 9;
	Mapa["dez"] = 10;
	Mapa["onze"] = 11;
	Mapa["doze"] = 12;
	Mapa["treze"] = 13;
	Mapa["quatorze"] = 14;
	Mapa["quinze"] = 15;
	Mapa["dezesseis"] = 16;
	Mapa["dezessete"] = 17;
	Mapa["dezoito"] = 18;
	Mapa["dezenove"] = 19;
	Mapa["vinte"] = 20;
	Mapa["trinta"] = 30;
	Mapa["quarenta"] = 40;
	Mapa["cinquenta"] = 50;
	Mapa["sessenta"] = 60;
	Mapa["setenta"] = 70;
	Mapa["oitenta"] = 80;
	Mapa["noventa"] = 90;
	Mapa["cem"] = 100;
	Mapa["cento"] = 100;
	Mapa["duzentos"] = 200;
	Mapa["trezentos"] = 300;
	Mapa["quatrocentos"] = 400;
	Mapa["quinhentos"] = 500;
	Mapa["seiscentos"] = 600;
	Mapa["setecentos"] = 700;
	Mapa["oitocentos"] = 800;
	Mapa["novecentos"] = 900;
	Mapa2["mil"]      = 1000;
	Mapa2["milhao"]   = 1000000;
	Mapa2["milhoes"]  = 1000000;
	Mapa2["bilhao"]   = 1000000000;
	Mapa2["bilhoes"]  = 1000000000;
	Mapa2["trilhao"]  = 1000000000000;
	Mapa2["trilhoes"] = 1000000000000;
	while(getline(cin, str))
	{
		stringstream ss;
		ss << str;
		numero = 0;
		casas = 0;
		while(ss >> str)
		{
			if(Mapa.count(str) > 0)
				casas += Mapa[str];
			if(Mapa2.count(str) > 0)
			{
				numero += Mapa2[str]*max(1LL, casas);
				casas = 0;
			}
		}
		numero += casas;
		printf("%lld\n", numero);
	}
	return 0;
}
