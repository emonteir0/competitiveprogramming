// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stutterer Digitizer
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2815

#include<bits/stdc++.h>

using namespace std;

char palavras[30];

main()
{
	int K = 0;
	while(scanf("%s", palavras) == 1)
	{
		if(K)
			printf(" ");
		K = 1;
		if(strlen(palavras) >= 4 && palavras[0] == palavras[2] && palavras[1] == palavras[3])
			printf("%s", palavras+2);
		else
			printf("%s", palavras);
	}
	printf("\n");
	return 0;
}
