// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Etaoin Shrdlu
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1737

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	string str;
	int c;
} tipo;

map<string,int> Mapa,Mapa2;
map<string,int>::iterator it;
vector<tipo> v;

bool cmp(tipo a, tipo b)
{
	if(a.c!=b.c) return a.c>b.c;
	return strcmp(a.str.c_str(),b.str.c_str())<0;
}

main()
{
	int i,N,cont,tam;
	string str,str2="12";
	tipo t;
	char vet[101];
	while(scanf("%d%*c",&N)==1&&(N!=0))
	{
		Mapa=Mapa2;
		cont=0;
		v.clear();
		str="";
		for(i=0;i<N;i++)
		{
			gets(vet);
			str+=vet;
		}
		str2[0]=str[0];
		tam=str.length();
		cont=tam-1;
		for(i=1;i<tam;i++)
		{
			str2[1]=str[i];
			Mapa[str2]++;
			str2[0]=str[i];
		}
		for(it=Mapa.begin();it!=Mapa.end();++it)
		{
			t.str=it->first;
			t.c=it->second;
			v.push_back(t);
		}
		sort(v.begin(),v.end(),cmp);
		tam=min((int)v.size(),5);
		for(i=0;i<tam;i++)
			printf("%s %d %.6lf\n",v[i].str.c_str(),v[i].c,((double)v[i].c)/cont);
		printf("\n");
	}
	return 0;
}
