// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hidden Message
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1272

#include<bits/stdc++.h>

using namespace std;

set<string> conjunto,conjunto2;

main()
{
	int N,i,n;
	string str;
	char vet[101];
	scanf("%d%*c",&N);
	while(N--)
	{
		gets(vet);
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			if(!(vet[i]>='a'&&vet[i]<='z'))
				vet[i]=' ';
		}
		str=vet;
		stringstream ss(vet);
		while(ss >> str)
			printf("%c",str[0]);
		printf("\n");
	}
	return 0;
}
