// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Complete Naebbirac’s Sequ...
// Level: 5
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2697

#include<bits/stdc++.h>

using namespace std;

vector<int> ffrequencias[10001];
int mfrequencias[1001];
map<int, int>::iterator it;

main()
{
	int i, N, M, x, sz;
	scanf("%d %d", &N, &M);
	if(M%N == 0)
		sz = M/N;
	else if(M%N == 1)
		sz = (M-1)/N;
	else if(M%N == N-1)
		sz = (M+1)/N;
	else
		printf("*\n");
	for(i = 0; i < M; i++)
	{
		scanf("%d", &x);
		mfrequencias[x]++;
	}
	for(i = 1; i <= N; i++)
		ffrequencias[mfrequencias[i]].push_back(i);
	if(M%N == 0)
	{
		if(ffrequencias[sz].size() == N-2 && ffrequencias[sz-1].size() == 1 && ffrequencias[sz+1].size() == 1)
			printf("-%d +%d\n", ffrequencias[sz+1][0], ffrequencias[sz-1][0]);
		else
			printf("*\n");
	}
	else if(M%N == 1)
	{
		if(ffrequencias[sz].size() == N-1 && ffrequencias[sz+1].size() == 1)
			printf("-%d\n", ffrequencias[sz+1][0]);
		else
			printf("*\n");
	}
	else if(M%N == N-1)
	{
		if(ffrequencias[sz].size() == N-1 && ffrequencias[sz-1].size() == 1)
			printf("+%d\n", ffrequencias[sz-1][0]);
		else
			printf("*\n");
	}
	return 0;
}
