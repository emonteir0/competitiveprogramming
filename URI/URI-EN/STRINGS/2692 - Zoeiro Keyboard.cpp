// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Zoeiro Keyboard
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2692

#include<bits/stdc++.h>

using namespace std;

char vet[10001], vet2[10001];

main()
{
	int i, j, N, M;
	char a, b, frase[1001];
	for(i = 0; i < 128; i++)
		vet[i] = i;
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
		cin >> vet[i] >> vet2[i];
	while(M--)
	{
		scanf(" %[^\n]s", frase);
		for(i = 0; frase[i]; i++)
		{
			for(j = 0; j < N; j++)
			{
				if(frase[i] == vet[j])
					frase[i] = vet2[j];
				else if(frase[i] == vet2[j])
					frase[i] = vet[j];
			}
		}
		printf("%s\n", frase);
	}
	return 0;
}
