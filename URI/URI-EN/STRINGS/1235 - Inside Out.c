// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Inside Out
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1235

#include<stdio.h>
#include<string.h>

main()
{
	int n,N,i;
	char vet[101];
	scanf("%d%*c",&N);
	while(N--)
	{
		gets(vet);
		n=strlen(vet);
		for(i=(n/2)-1;i>=0;i--)
			printf("%c",vet[i]);
		for(i=n-1;i>=(n/2);i--)
			printf("%c",vet[i]);
		printf("\n");
	}
	return 0;
}
