// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Abracadabra
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2484

#include<stdio.h>
#include<string.h>

main()
{
	char msg[101];
	int i, j, tam;
	while(scanf("%s", msg) == 1)
	{
		tam = strlen(msg);
		for(i = 0; i < tam; i++)
		{
			for(j = 0; j < i; j++)
				printf(" ");
			for(j = 0; j < tam-1-i; j++)
				printf("%c ", msg[j]);
			printf("%c\n", msg[j]);
		}
		printf("\n");
	}
	return 0;
}

