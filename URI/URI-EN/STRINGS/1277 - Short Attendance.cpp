// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Short Attendance
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1277

#include<bits/stdc++.h>

using namespace std;

main()
{
	string str[100];
	char vet[101];
	int N,M,i,j,a,b,k;
	scanf("%d%*c",&N);
	while(N--)
	{
		k=0;
		scanf("%d%*c",&M);
		for(i=0;i<M;i++)
		{
			scanf("%s",vet);
			str[i]=vet;
		}
		for(i=0;i<M;i++)
		{
			scanf("%s",vet);
			a=0;
			b=0;
			for(j=0;j<strlen(vet);j++)
			{
				if(vet[j]=='A')
					b++;
				if(vet[j]=='P')
				{
					a++;b++;
				}
			}
			if(a*4<3*b)
			{
				printf("%s%s",(k==1)?" ":"",str[i].c_str());
				k=1;
			}
		}
		printf("\n");
	}
	return 0;
}
