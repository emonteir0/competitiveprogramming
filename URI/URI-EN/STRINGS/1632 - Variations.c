// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Variations
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1632

#include<stdio.h>
#include<string.h>

main()
{
	int N,i,tam,x;
	char vet[1001];
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%s",vet);
		tam=strlen(vet);
		x=1;
		for(i=0;i<tam;i++)
		{
			if((vet[i]=='a')||(vet[i]=='A')||(vet[i]=='E')||(vet[i]=='e')||(vet[i]=='i')||(vet[i]=='I')||(vet[i]=='O')||(vet[i]=='o')||(vet[i]=='s')||(vet[i]=='S'))
				x*=3;
			else
				x*=2;	
		}
		printf("%d\n",x);
	}
	return 0;
}
