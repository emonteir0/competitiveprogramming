// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: R+L=J
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2557

#include<bits/stdc++.h>

using namespace std;

main()
{
    int i;
    char op;
    string str, stra, strb, strc;
    while(cin >> str)
    {
        for(i = 0; str[i] != 0; i++)
        {
            if(str[i] == 'L')
            {
                op = 'L';
                break;
            }
            if(str[i] == 'R')
            {
                op = 'R';
                break;
            }
            if(str[i] == 'J')
            {
                op = 'J';
                break;
            }
        }
        stra = str.substr(0, str.find('+'));
        strb = str.substr(str.find('+')+1, str.find('=')-str.find('+')-1);
        strc = str.substr(str.find('=')+1);
        if(op == 'L')
            printf("%d\n", atoi(strc.c_str())-atoi(stra.c_str()));
        else if(op == 'R')
            printf("%d\n", atoi(strc.c_str())-atoi(strb.c_str()));
        else
            printf("%d\n", atoi(stra.c_str())+atoi(strb.c_str()));
    }
    return 0;
}
