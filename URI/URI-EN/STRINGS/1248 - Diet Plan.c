// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Diet Plan
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1248

#include<stdio.h>
#include<string.h>

int vet[26];

main()
{
	int i,n,tam,c,b;
	char str[27];
	scanf("%d%*c",&n);
	while(n--)
	{
		b=1;
		for(i=0;i<26;i++)
			vet[i]=0;
		gets(str);
		tam=strlen(str);
		for(i=0;i<tam;i++)
		{
			c=str[i]-'A';
			if(vet[c]==0)
				vet[c]=1;
		}
		gets(str);
		tam=strlen(str);
		for(i=0;i<tam;i++)
		{
			c=str[i]-'A';
			if(vet[c]==0)
				b=0;
			else
				vet[c]--;
		}
		if(b==0)
			gets(str);
		else
		{
			gets(str);
			tam=strlen(str);
			for(i=0;i<tam;i++)
			{
				c=str[i]-'A';
				if(vet[c]==0)
					b=0;
				else
					vet[c]--;
			}
		}
		if(b==0)
			printf("CHEATER\n");
		else
		{
			for(i=0;i<26;i++)
				if(vet[i]==1)
					printf("%c",i+'A');
			printf("\n");
		}
	}
	return 0;
}
