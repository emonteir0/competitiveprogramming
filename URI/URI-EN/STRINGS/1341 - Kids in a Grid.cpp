// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kids in a Grid
// Level: 7
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1341

#include<bits/stdc++.h>

using namespace std;

int N, M;
char str1[400], str2[400];
int dp[401][401];

int lcs()
{
	for(int i = 0; i <= N; i++)
		for(int j = 0; j <= M; j++)
		{
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(str1[i-1] == str2[j-1])
				dp[i][j] = dp[i-1][j-1] + 1;
			else
				dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
		}
	return dp[N][M];
}

char mat[21][21];

main()
{
	int z, T, H, W, x0, y0;
	char c;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		scanf("%d %d", &H, &W);
		for(int i = H-1; i >= 0; i--)
			scanf("%s", mat[i]);
		scanf("%d %d %d%*c", &N, &x0, &y0);
		N++;
		x0--;
		y0--;
		str1[0] = mat[x0][y0];
		for(int i = 1; i < N; i++)
		{
			scanf("%c", &c);
			if(c == 'N')		x0++;
			else if(c == 'S')	x0--;
			else if(c == 'E')	y0++;
			else				y0--;
			str1[i] = mat[x0][y0];
		}
		scanf("%d %d %d%*c", &M, &x0, &y0);
		M++;
		x0--;
		y0--;
		str2[0] = mat[x0][y0];
		for(int i = 1; i < M; i++)
		{
			scanf("%c", &c);
			if(c == 'N')		x0++;
			else if(c == 'S')	x0--;
			else if(c == 'E')	y0++;
			else				y0--;
			str2[i] = mat[x0][y0];
		}
		z = lcs();
		printf("Case %d: %d %d\n", t, N-z, M-z);
	}
	return 0;
}
