// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Growing Strings
// Level: 9
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1141

#include<bits/stdc++.h>
#define REP(i, x) for(int i = 0; i < x; i++)
#define ALPHA 26
#define MAX 1001001
using namespace std;

int V;
int trie[MAX][ALPHA];
int fn[MAX];
int term[MAX];
int st[MAX];
int dp[10001];

int node() 
{
	REP(i, ALPHA)
		trie[V][i] = 0;
	fn[V] = 0;
	return V++;
}

void insert(string s, int id) 
{
	int t = 0;
	REP(i, (int)s.length())
	{
		int c = s[i] - 'a';
		if (trie[t][c] == 0)
		{
			trie[t][c] = node();
			term[trie[t][c]] = 0;
		}
		t = trie[t][c];
	}
	term[t] = 1;
	st[t] = id;
}

void init_aho() 
{
	queue<int> Q;
	REP(i, ALPHA)
		if(trie[0][i])
			Q.push(trie[0][i]);
	while (!Q.empty()) 
	{
		int t = Q.front(); Q.pop();

		REP(c, ALPHA) 
		{
			int x = trie[t][c];
			if (x) 
			{
				Q.push(x);
				fn[x] = fn[t];
				while (fn[x] && trie[fn[x]][c] == 0)
					fn[x] = fn[fn[x]];
				if (trie[fn[x]][c])
					fn[x] = trie[fn[x]][c];
				//printf("%d %d\n", fn[x], term[fn[x]]);
				if(term[fn[x]])
				{
					//printf("%d!!!!!\n", x);
					term[x] |= 2;
				}
			}
			else
				trie[t][c] = trie[fn[t]][c];
		}
	}
}


bool cmp(string s, string t)
{
	return s.size() < t.size();
}

vector<string> palavras;

int search()
{
	int ma = 0, v, x;
	REP(i, (int)palavras.size())
	{
		dp[i] = 1;
		v = 0;
		//cout << palavras[i] << endl;
		REP(j, (int)palavras[i].length())
		{
			v = trie[v][palavras[i][j]-'a'];
			x = v;
			if(j == palavras[i].length()-1)
			    x = fn[x];
			//printf("%d\n", x);
			while(term[x])
			{
				//printf("LOOP\n");
				//printf("%d %d\n", j, x);
				//cout << palavras[st[x]] << "!\n";
				if(term[x] & 1)
					dp[i] = max(dp[i], 1 + dp[st[x]]);
				x = fn[x];
			}
		}
		ma = max(ma, dp[i]);
	}
	return ma;
}

int main()
{
	int N;
	string s;
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	while((cin >> N) && N)
	{
		palavras.clear();
		V = 0;
		node();
		REP(i, N)
		{
			cin >> s;
			palavras.push_back(s);
		}
		sort(palavras.begin(), palavras.end(), cmp);
		REP(i, N)
			insert(palavras[i], i);
		init_aho();
		//REP(i, V)
			//cout << i << ": " << term[i] << endl;
		cout << search() << endl;
	}
	return 0;
}

