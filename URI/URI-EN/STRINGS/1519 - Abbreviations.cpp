// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Abbreviations
// Level: 5
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1519

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	string str;
	int w;
}tipo;

map<char,string> Mapa,Mapa2;
map<char,string>::iterator it;
vector<string> v;
vector<tipo> VT;
map<string,int> C;
map<string,int>::iterator Cit;

bool cmp(tipo a, tipo b)
{
	return a.w<b.w;
}

main()
{
	int i,n=0,w;
	tipo a;
	char c,vet[10001];
	string str;	
	while((gets(vet)!=NULL)&&(strcmp(vet,".")!=0))
	{
		str=vet;
		stringstream ss(str);
		while(ss >> str)
		{
			n++;
			v.push_back(str);
			C[str]+=(str.length()-2);
		}
		for(Cit=C.begin();Cit!=C.end();++Cit)
		{
			a.str=Cit->first;
			a.w=Cit->second;
			VT.push_back(a);
		}
		sort(VT.begin(),VT.end(),cmp);
		for(i=0;i<VT.size();i++)
		{
			if(VT[i].w>0)
				Mapa[(VT[i].str)[0]]=VT[i].str;
		}
		str=v[0];
		if(Mapa.find(str[0])!=Mapa.end())
		{
			if(Mapa[str[0]]==str)
				printf("%c.",str[0]);
			else
				printf("%s",str.c_str());
		}
		else
			printf("%s",str.c_str());
		for(i=1;i<n;i++)
		{
			str=v[i];
			if(Mapa.find(str[0])!=Mapa.end())
			{
				if(Mapa[str[0]]==str)
					printf(" %c.",str[0]);
				else
					printf(" %s",str.c_str());
			}
			else
				printf(" %s",str.c_str());
		}
		printf("\n%d\n",Mapa.size());
		for(it=Mapa.begin();it!=Mapa.end();++it)
			printf("%c. = %s\n",it->first,it->second.c_str());
		n=0;
		v.clear();
		VT.clear();
		C.clear();
		Mapa=Mapa2;
	}
	return 0;
}
