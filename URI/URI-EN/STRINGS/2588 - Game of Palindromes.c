// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of Palindromes
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2588

#include<stdio.h>
#include<string.h>

int vet[26];

main()
{
	int i, j, cont;
	char msg[1001];
	while(scanf("%s", msg) == 1)
	{
		cont = 0;
		memset(vet, 0, 26*sizeof(int));
		for(i = 0; msg[i] ; i++)
			vet[msg[i]-'a']++;
		for(i = 0; i < 26; i++)
			if(vet[i]&1)
				cont++;
		if(cont == 0)
			printf("0\n");
		else
			printf("%d\n",cont-1);
	}
	return 0;
}
