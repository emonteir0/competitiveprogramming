// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Numbers of Ahmoc
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2049

#include<bits/stdc++.h>
#define MAXL 11
using namespace std;

string P, T;
int b[MAXL];

void kmpPre()
{
	int i = 0, j = -1, m = P.length();
	b[0] = -1;
	while( i < m)
	{
		while(j >= 0 && P[i] != P[j]) j = b[j];
		i++, j++;
		b[i] = j;
	}
}

int kmpSearch(const string &T)
{
	int i = 0, j = 0, n = T.length(), m = P.length();
	while(i < n)
	{
		while(j >= 0 && T[i] != P[j]) j = b[j];
		i++, j++;
		if(j == m)
		{
			return 1;
			j = b[j];
		}
	}
	return 0;
}

int main()
{
	int t = 1;
	while(cin >> P)
	{
		if(P == "0")
			break;
		if(t > 1)
			printf("\n");
		kmpPre();
		cin >> T;
		printf("Instancia %d\n%s\n", t++, kmpSearch(T) ? "verdadeira" : "falsa");
	}
	return 0;
}
