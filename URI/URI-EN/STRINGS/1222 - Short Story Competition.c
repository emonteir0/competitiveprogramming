// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Short Story Competition
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1222

#include<stdio.h>
#include<string.h>

main()
{
	char vet[71];
	int N,L,C,i,tam,contp,contl,contc;
	while(scanf("%d %d %d%*c",&N,&L,&C)==3)
	{
		contp=0;
		contl=0;
		scanf("%s",vet);
		tam=strlen(vet);
		contc=tam;
		for(i=1;i<N;i++)
		{
			scanf("%s",vet);
			tam=strlen(vet);
			if(tam+contc+(contc!=0)<=C)
				contc+=tam+(contc!=0);
			else
			{
				contc=tam;
				contl++;
				if(contl==L)
				{
					contl=0;
					contp++;
				}
			}
		}
		if((contl!=0)||(contc!=0))
			contp++;
		printf("%d\n",contp);
	}
	return 0;
}
