// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Image
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1516

#include<stdio.h>

char mat[50][50];

main()
{
	int n,m,n2,m2,i,j;
	while((scanf("%d %d%*c",&n,&m)==2)&&(n||m))
	{
		for(i=0;i<n;i++)
		{
			for(j=0;j<m;j++)
				scanf("%c",&mat[i][j]);
			scanf("%*c");
		}
		scanf("%d %d%*c",&n2,&m2);
		for(i=0;i<n2;i++)
		{
			for(j=0;j<m2;j++)
				printf("%c",mat[i/(n2/n)][j/(m2/m)]);
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}

