// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rot13
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1249

#include<stdio.h>
#include<string.h>


main()
{
	int i,n;
	char vet[51],c;
	while(gets(vet))
	{
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			c=vet[i];
			if(c>='a'&&c<='z')
				vet[i]='a'+(c-'a'+13)%26;
			if(c>='A'&&c<='Z')
				vet[i]='A'+(c-'A'+13)%26;
		}
		printf("%s\n",vet);
	}
	return 0;
}
