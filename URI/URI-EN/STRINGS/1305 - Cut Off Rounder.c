// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cut Off Rounder
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1305

#include<stdio.h>
#include<string.h>

main()
{
	long long int i1,f1,f2;
	int j,k,n,n2;
	char num[11],num2[5];
	while(scanf("%s%*c%*c%*c%s%*c",num,num2)==2)
	{
		f1=0;
		i1=0;
		f2=0;
		n=strlen(num);
		n2=strlen(num2);
		for(j=0;j<n2;j++)
			f2=10*f2+num2[j]-'0';
		for(j=0;j<n;j++)
		{
			if(num[j]=='.')
			{
				j++;
				break;
			}
			i1=10*i1+num[j]-'0';
		}
		for(k=j;k<n;k++)
		{
			f1=10*f1+num[k]-'0';
		}
		for(k=0;k<n2-n+j;k++)
			f1*=10;
		for(k=0;k<n-j-n2;k++)
			f2*=10;
		printf("%lld\n",i1+(f1>=f2));
	}
	return 0;
}
