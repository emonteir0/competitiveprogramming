// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Guga and the String
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1781

#include<stdio.h>
#include<string.h>

main()
{
	int t,i,k=1,n,op,x,tam,tamv,tamc,contv,contc,vo,co;
	char c,palavra[10001],vogais[10000],consoantes[10000];
	scanf("%d%*c",&t);
	while(t--)
	{
		scanf("%s",palavra);
		printf("Caso #%d:\n",k++);
		for(tam=0,tamv=0,tamc=0;palavra[tam]!=0;tam++)
		{
			c=palavra[tam];
			if((c =='a') || (c =='e') || (c =='i') || (c =='o') || (c =='u'))
				vogais[tamv++]=c;
			else consoantes[tamc++]=c;
		}
		scanf("%d%*c",&n);
		contv=0;
		contc=0;
		while(n--)
		{
			scanf("%d%*c",&op);
			if(op!=2)
			{
				scanf("%d%*c",&x);
				if(op==0)
					contv+=x;
				else
					contc+=x;
			}
			else
			{
				for(i=0, vo=0, co=0;i<tam;i++)
				{
					c=palavra[i];
					if((c=='a') || (c=='e') || (c=='i') || (c=='o') || (c=='u'))
						printf("%c",vogais[((((vo++)+tamv-contv)%tamv)+tamv)%tamv]);
					else
						printf("%c",consoantes[((((co++)+tamc-contc)%tamc)+tamc)%tamc]);
				}
				printf("\n");
			}
		}
	}
	return 0;
}
