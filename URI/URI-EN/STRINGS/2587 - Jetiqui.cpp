// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jetiqui
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2587

#include<bits/stdc++.h>

using namespace std;


main()
{
	string str, str2, str3;
	int N, k;
	int v[2];
	cin >> N;
	while(N--)
	{
		int k = 0;
		cin >> str >> str2 >> str3;
		for(int i = 0; i < str3.size(); i++)
			if(str3[i] == '_')
				v[k++] = i;
		printf("%s\n", ((str[v[0]] == str2[v[1]]) || (str[v[1]] == str2[v[0]])) ? "Y" : "N");
	}
	return 0;
}
