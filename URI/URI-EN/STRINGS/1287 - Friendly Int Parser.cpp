// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Friendly Int Parser
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1287

#include<cstdio>
#include<cstring>
#include<string>

using namespace std;

int comparar (char* v)
{
	if(strlen(v)==10)
		return strcmp(v,"2147483647")>0;
	else if(strlen(v)<10)
		return 0;
	return 1;
}

main()
{
	int i,b;
	string a;
	char v[51];
	while(gets(v))
	{
		a=v;
		b=0;
		for(i=0;i<a.length();i++)
		{
			if(a[i]=='o'||a[i]=='O')
				a[i]='0';
			else if(a[i]=='l')
				a[i]='1';
			else if(a[i]==','||a[i]==' ')
				a.erase(i--,1);
			else if(a[i]>='0'&&a[i]<='9');
			else
				b=1;
		}
		while((a[0]=='0')&&(a.length()>1))
			a.erase(0,1);
		strcpy(v,a.c_str());
		if((a.length()==0)||comparar(v)||b)	printf("error\n");
		else										printf("%s\n",a.c_str());
	}
	return 0;
}
