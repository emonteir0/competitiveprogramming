// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tug of War
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1586

#include<stdio.h>

int valores[100000];
char nomes[100000][11];

int main()
{
	long long int timeA,timeB;
	int sumA,sumB,i,j,N,N2;
	while(scanf("%d%*c",&N)==1&&(N!=0))
	{
		timeA=timeB=0;
		N2 = N/2;
		for(i=sumA=sumB=0;i<N;i++)
		{
			scanf("%s",nomes[i]);
			valores[i]=0;
			for(j=0;nomes[i][j]!=0;j++)
				valores[i]+=nomes[i][j];
			if(i<N2)
			{
				timeA+=(N2-i)*valores[i];
				sumA+=valores[i];
			}
			else
			{
				timeB+=(i-N2+1)*valores[i];
				sumB+=valores[i];
			}
		}
		i=N2-1;
		if(timeA==timeB)
			printf("%s\n",nomes[i]);
		else if(timeA>timeB)
		{
			while(timeA>=timeB)
			{
				if(timeA==timeB)
				{
					printf("%s\n",nomes[i]);
					break;
				}
				timeA-=sumA;
				sumA-=valores[i];
				sumB+=valores[i];
				timeB+=sumB;
				i--;
			}
			if(timeA!=timeB)
				printf("Impossibilidade de empate.\n");
		}
		else
		{
			i++;
			while(timeA<=timeB)
			{
				if(timeA==timeB)
				{
					printf("%s\n",nomes[i-1]);
					break;
				}
				else
				{
					timeB-=sumB;
					sumA+=valores[i];
					sumB-=valores[i];
					timeA+=sumA;
					i++;
				}
			}
			if(timeA!=timeB)
				printf("Impossibilidade de empate.\n");
		}
	}
	return 0;
}

