// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dancing Sentence
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1234

#include<stdio.h>

void imprimir(char x, char *op)
{
	if(*op==1)
	{
		if(x>='a'&&x<='z')		printf("%c",x+'A'-'a');
		else if(x>='A'&&x<='Z')		printf("%c",x);
		else					printf(" ");
	}
	else if(*op==0)
	{
		if(x>='a'&&x<='z')		printf("%c",x);
		else if(x>='A'&&x<='Z')	printf("%c",x-'A'+'a');
		else					printf(" ");
	}
	if(x!=' ')
		*op=!(*op);
}

main()
{
	char op,x;
	while(scanf("%c",&x)==1)
	{
		op=1;
		imprimir(x,&op);
		while(scanf("%c",&x)==1&&x!='\n')
		{
			imprimir(x,&op);
		}
		printf("\n");
	}
	return 0;
}
