// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pudim
// Level: 3
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2824

#include<bits/stdc++.h>

using namespace std;

char s1[5001], s2[5001];
int dp[5001][5001];


int lcs()
{
	int N = strlen(s1), M = strlen(s2);
	for(int i = 0; i <= N; i++)
		for(int j = 0; j <= M; j++)
		{
			if((i == 0) || (j == 0))
				dp[i][j] = 0;
			else if(s1[i-1] == s2[j-1])
				dp[i][j] = dp[i-1][j-1] + 1;
			else
				dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
		}
	return dp[N][M];
}


main()
{
	scanf("%s", s1);
	scanf("%s", s2);
	printf("%d\n", lcs());
	return 0;
}
