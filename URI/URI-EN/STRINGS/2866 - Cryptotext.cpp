// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cryptotext
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2866

#include<bits/stdc++.h>

using namespace std;

string str, ans;

bool up(char c)
{
	return c >= 'A' && c <= 'Z';
}

main()
{
	int N;
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	cin >> N;
	while(N--)
	{
		cin >> str;
		ans = "";
		for(int i = 0; i < str.length(); i++)
			if(!up(str[i]))
				ans += str[i];
		reverse(ans.begin(), ans.end());
		cout << ans << endl;
	}
	return 0;
}
