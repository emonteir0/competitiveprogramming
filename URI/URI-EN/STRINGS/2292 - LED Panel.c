// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: LED Panel
// Level: 1
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2292

#include<stdio.h>
#define ll unsigned long long
main()
{
	int K, i, tam;
	ll x, s, y, N;
	char mensagem[61], c;
	scanf("%d%*c", &K);
	while(K--)
	{
		tam = 0;
		s = 0;
		x = 1;
		scanf("%s %lld%*c", mensagem, &N);
		for(i = 0; c = mensagem[i]; i++)
		{
			s += (c == 'O')?x:0;
			tam++;
			x <<= 1;
		}
		s = (s+N)&(x-1);
		while(tam--)
		{
			printf("%s", s&1?"O":"X");
			s>>=1;
		}
		printf("\n");
	}
	return 0;
}
