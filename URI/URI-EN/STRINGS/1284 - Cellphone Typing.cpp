// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cellphone Typing
// Level: 6
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1284

#include<bits/stdc++.h>

using namespace std;

struct node
{
	int val;
	int tam;
	int adj[26];
	node()
	{
		tam = 0;
		val = 0;
		for(int i = 0; i < 26; i++)
			adj[i] = -1;
	}
};

int sz, ans;
node trie[2000001];
string str;

void add(string &str)
{
	int p = 0;
	for(int i = 0; i < str.length(); i++)
	{
		if(trie[p].adj[str[i]-'a'] == -1)
		{
			trie[p].tam++;
			trie[sz] = node();
			trie[p].adj[str[i]-'a'] = sz++;
		}
		p = trie[p].adj[str[i]-'a'];
	}
	trie[p].val = 1;
}

void dfs(int x, int level)
{
	if(trie[x].val)
		ans += level;
	for(int i = 0; i < 26; i++)
	{
		if(trie[x].adj[i] != -1)
			dfs(trie[x].adj[i], level+((trie[x].tam > 1) || trie[x].val));
	}
}

main()
{
	int N;
	while(cin >> N)
	{
		ans = 0;
		sz = 1;
		trie[0] = node();
		for(int i = 0; i < N; i++)
		{
			cin >> str;
			add(str);
		}
		for(int i = 0; i < 26; i++)
			if(trie[0].adj[i] != -1)
				dfs(trie[0].adj[i], 1);
		printf("%.2lf\n", double(ans)/N);
	}
	return 0;
}
