// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Matring
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1803

#include<stdio.h>
 
int M,L,val[81],n;
 
main()
{
    int i,j;
    char x;
    scanf("%c",&x);
    M=x-'0';
    while(1)
    {
        scanf("%c",&x);
        if(x!='\n')
        {
            val[n++]=x-'0';
        }
        else break;
    }
    L=val[--n];
    for(j=0;j<3;j++)
    {
        scanf("%c",&x);
        M=M*10+x-'0';
        for(i=0;i<n;i++)
        {
            scanf("%c",&x);
            val[i]=val[i]*10+x-'0';
        }
        scanf("%c%*c",&x);
        L=L*10+x-'0';
    }
    for(i=0;i<n;i++)
    {
        printf("%c",(M*val[i]+L)%257);
    }
    printf("\n");
    return 0;
}
