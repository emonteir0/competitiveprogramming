// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kickback Sum
// Level: 2
// Category: STRINGS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1898

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

main()
{
	int i,tam=0,tam2=0,tam3=0;
	char c,vet[61],vet2[61],cpf[12],val1[18],val2[18];
	gets(vet);
	gets(vet2);
	for(i=0;;i++)
	{
		c=vet[i];
		if(c==0)
			break;
		if(c>='0' && c<='9')
			cpf[tam++]=c;
		if(tam==11)
		{
			i++;
			break;
		}
	}
	for(;;i++)
	{
		c=vet[i];
		if(c==0)
			break;
		if((c>='0' && c<='9') || c=='.')
			val1[tam2++]=c;
	}
	for(i=0;;i++)
	{
		c=vet2[i];
		if(c==0)
			break;
		if((c>='0' && c<='9') || c=='.')
			val2[tam3++]=c;
	}
	cpf[11]=0;
	for(i=0;;i++)
	{
		c=val1[i];
		if(c==0)
			break;
		if(c=='.')
		{
			tam2 = tam2 > i+3 ? i+3 : tam2;
			break;
		}
	}
	for(i=0;;i++)
	{
		c=val2[i];
		if(c==0)
			break;
		if(c=='.')
		{
			tam3 = tam3 > i+3 ? i+3 : tam3;
			break;
		}
	}
	val1[tam2]=0;
	val2[tam3]=0;
	printf("cpf %s\n%.2f\n",cpf,atof(val1)+atof(val2));
	return 0;
}

