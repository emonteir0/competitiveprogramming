// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tour in FdI
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2553

#include<bits/stdc++.h>
#define ll int
using namespace std;

int N;
unordered_map<ll, int> memo[40], memo2;
int vet1[40], vet2[40];

int solve(int x, int y, int z)
{
	ll p = ((ll)x << 10) +  y;
	if(z == N)
		return 0;
	if(memo[z].find(p) != memo[z].end())
		return memo[z][p];
	memo[z][p] = solve(x, y, z+1);
	if(x+vet1[z] >= 0 && y + vet2[z] >= 0)
		memo[z][p] = max(solve(x+vet1[z],y+vet2[z], z+1) + 1, memo[z][p]);
	return memo[z][p];
}

main()
{
	int i, j, k, A, B;
	while(scanf("%d %d %d", &N, &A, &B) == 3)
	{
		for(i = 0; i < N; i++)
		{
			memo[i].clear();
			scanf("%d %d", &vet1[i], &vet2[i]);
		}
		printf("%d\n", solve(A, B, 0));
	}
	return 0;
}
