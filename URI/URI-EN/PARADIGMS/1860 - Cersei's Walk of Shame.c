// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cersei's Walk of Shame
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1860

#include<stdio.h>
#include<math.h>
#define EPS 1e-4

int N;
double x[50000], y[50000];

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

double sqr(double x)
{
	return x*x;
}

double min(double a, double b)
{
	return (a < b) ? a : b;
}

double max(double a, double b)
{
	return (a > b) ? a : b;
}

double func(double xl)
{
	int i;
	double dist = 0;
	for(i = 0; i < N; i++)
		dist = max(dist, sqrt(sqr(x[i]-xl)+sqr(y[i])));
	return dist;
}

double ternarySearch(double left, double right)
{
	double leftThird, rightThird;
	if (absol(right - left) < EPS)
		return (left + right)/2;
	leftThird = (2*left + right)/3;
	rightThird = (left + 2*right)/3;
	if (func(leftThird) > func(rightThird))
		return ternarySearch(leftThird, right); 
	return ternarySearch(left, rightThird);
}


main()
{
	int i;
	double last, z;
	scanf("%d %lf", &N, &last);
	for(i = 0; i < N; i++)
		scanf("%lf %lf", &x[i], &y[i]);
	z = ternarySearch(0, last);
	printf("%.2lf %.2lf\n", z, func(z));
	return 0;
}

