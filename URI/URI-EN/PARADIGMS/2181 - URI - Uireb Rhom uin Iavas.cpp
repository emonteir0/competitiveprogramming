// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: URI - Uireb Rhom uin Iavas
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2181

#include<bits/stdc++.h>

using namespace std;

struct elfo
{
	double d;
	int id;
};

elfo vet[100001];
elfo vazio;
double acum[100001];
int vid[100001];

bool cmp(elfo a, elfo b)
{
	return a.id < b.id;
}


main()
{
	int i,j,k,n, p, maxp, ant;
	double maxmoney;
	scanf("%d %d", &n, &k);
	for(i=0;i<n;i++)
		scanf("%d", &vet[i].id);
	for(i=0;i<n;i++)
		scanf("%lf", &vet[i].d);
	sort(vet, vet+n, cmp);
	acum[0] = vet[0].d;
	for(i=1;i<n;i++)
		acum[i] = acum[i-1] + vet[i].d;
	p = 1;
	maxmoney = 0;
	maxp = 0;
	for(i = 0; i < n; i++)
		vid[i] = vet[i].id;
	for(i=0;i<n;i++)
	{
		//j = busca(vet[i].id + k, n);
		j = upper_bound(vid, vid+n, vet[i].id + k) - vid - 1;
		if(i!=0 && j-i+1 != ant-1)
			p++;
		ant = j-i+1;
		maxp = max(ant, maxp);
		maxmoney = max(maxmoney,acum[j]-acum[i]+vet[i].d);
		
	}
	printf("%d %d %.2lf\n", p, maxp, maxmoney);
	return 0;
}

