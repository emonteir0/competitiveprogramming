// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Radars
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2599

#include<stdio.h>
#include<math.h>
#define MOD 1000000007
#define ll long long
int memo[10001];
int valores[1000];
int N, M;

int solve(int x)
{
	int i;
	ll ans = 0;
	if(x <= 0)
		return 1;
	if(memo[x] != -1)
		return memo[x];
	for(i = 0; i < M; i++)
			ans += solve(x-valores[i]);
	return memo[x] = ans % MOD;
}

main()
{
	int i;
	scanf("%*d");
	while(scanf("%d %d", &N, &M) != EOF)
	{
		for(i = 0; i <= N; i++)
			memo[i] = -1;
		for(i = 0; i < M; i++)
			scanf("%d", &valores[i]);
		printf("%d\n", solve(N));
	}
	return 0;
}
