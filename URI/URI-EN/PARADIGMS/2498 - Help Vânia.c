// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Vânia
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2498

#include<stdio.h>


int best[1010][100];
int valor[1010];
int tam[1010];

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int i, j, N, M, K = 0;
	while((scanf("%d %d", &N, &M))&& (N||M))
	{
		for(i = 1; i <= N; i++)
			scanf("%d %d", &tam[i], &valor[i]);
		for(i = 1; i <= N; i++)
			for(j = 1; j <= M; j++)
				if(j >= tam[i])
					best[i][j] = max(best[i-1][j-tam[i]] + valor[i], best[i-1][j]);
				else
					best[i][j] = best[i-1][j];
		printf("Caso %d: %d\n", ++K, best[N][M]);
	}
	return 0;
}
