// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dynamic Frog
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1054

#include <bits/stdc++.h>
#define ll long long
#define MAXN 405
#define INF 1001001001

using namespace std;

struct edge{int v, rev; ll cap, f;};
vector<edge> adj[MAXN];
int dist[MAXN], ptr[MAXN];

void add_edge(int u, int v, int cap)
{
	adj[u].push_back((edge){v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){u, (int)adj[u].size()-1, 0, 0});
}	

bool dinic_bfs(int nodes, int source, int sink)
{
	fill(dist, dist+nodes, -1);
	dist[source] = 0;
	queue<int> q;
	q.push(source);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		for(int j = 0; j < (int)adj[u].size(); j++)
		{
			edge &e = adj[u][j];
			if(dist[e.v] < 0 && e.f < e.cap)
			{
				dist[e.v] = dist[u]+1;
				q.push(e.v);
			}
		}
	}
	return dist[sink] >= 0;
}

ll dinic_dfs(int u, ll f, int sink)
{
	if(u == sink)
		return f;
	for(; ptr[u] < (int)adj[u].size(); ptr[u]++)
	{
		edge &e = adj[u][ptr[u]];
		if(dist[e.v] == dist[u] + 1 && e.f < e.cap)
		{
			ll flow = dinic_dfs(e.v, min(f, e.cap-e.f), sink);
			if(flow > 0)
			{
				e.f += flow;
				adj[e.v][e.rev].f -= flow;
				return flow;
			}
		}
	}
	return 0;
}

ll dinic(int nodes, int source, int sink)
{
	ll flow, max_flow = 0;
	while(dinic_bfs(nodes, source, sink))
	{
		fill(ptr, ptr+nodes, 0);
		while((flow = dinic_dfs(source, INF, sink)) != 0)
			max_flow += flow;
	}
	return max_flow;
}

char op[MAXN];
int d[MAXN];
int N, D;

int valor(int x)
{
	return 2-(op[x] == 'S');
}

int solve()
{
	int lo = 1, hi = D, mid, best = D, res, sz = 2*N+3;
	if(N == 0)
		return D;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		for(int i = 0; i < sz-2; i+=2)
			add_edge(i, i+1, valor(i));
		for(int i = 1; i < sz; i += 2)
		{
			for(int j = i+1; j < sz; j += 2)
			{
				if(d[j] <= d[i]+mid)
				{
					add_edge(i, j, min(valor(i), valor(j)));
				}
			}
		}
		res = dinic(sz, 0, 2*N+2);
		//printf("%d %d!!\n", mid, res);
		if(res == 2)
		{
			best = mid;
			hi = mid-1;
		}
		else
		{
			lo = mid+1;
		}
		for(int i = 0; i < sz; i++)
			adj[i].clear();
	}
	return best;
}

int main() 
{
	int T;
	scanf("%d", &T);
	op[0] = op[1] = 'K';
	d[0] = d[1] = 0;
	for(int t = 1; t <= T; t++)
	{
		scanf("%d %d", &N, &D);
		d[2*N+2] = D;
		op[2*N+2] = 'K';
		for(int i = 2; i <= 2*N; i+=2)
		{
			scanf(" %c-%d", &op[i], &d[i]);
			op[i+1] = op[i];
			d[i+1] = d[i];
		}
		printf("Case %d: %d\n", t, solve());
	}
	return 0;
}
