// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Transport of Solar Panels
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1579

#include<stdio.h>

int valores[100];

int main()
{
	int i,k,N,A,B,max,acum,min,x,c;
	scanf("%d",&k);
	while(k--)
	{
		scanf("%d %d %d",&N,&A,&B);
		scanf("%d",&valores[0]);
		for(i=1,acum=valores[0],max=valores[0];i<N;i++)
		{
			scanf("%d",&valores[i]);
			acum+=valores[i];
			max=(max>valores[i])?max:valores[i];
		}
		min=max;
		max=acum;
		while(min<max)
		{
			x=min+(max-min)/2;
			c=1;
			for(acum=0,i=0;i<N;i++)
			{
				if(acum+valores[i]<=x)
					acum+=valores[i];
				else
				{
					acum=valores[i];
					c++;
				}
			}
			if(c<=A)
				max=x;
			else
				min=x+1;
		}
		printf("%d $%d\n",min,min*A*B);
	}
	return 0;
}

