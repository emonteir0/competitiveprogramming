// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Modificando SAT
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2888

#include<bits/stdc++.h>

using namespace std;

bitset<2000> mat[2000];
int ans[2000];
char resp[2001], resp2[2001];

int N, M;

char str[101];

void build(int ind)
{
	int val;
	ans[ind] = 1;
	for(int i = 0; str[i]; i++)
	{
		if(str[i] == 'x')
		{
			val = 0;
			i++;
			for(; str[i]; i++)
			{
				if(str[i] >= '0' && str[i] <= '9')
					val = 10*val + str[i]-'0'; 
				else
					break;
			}
			val = M-val;
			if(mat[ind][val] == 1)
				mat[ind].reset(val);
			else
				mat[ind].set(val);
		}
		if(str[i] == 't')
		{
			ans[ind] ^= 1;
		}
	}
	//cout << matriz[ind].to_string() << ' ' << ans[ind] << endl;
}

void solve()
{
	int i, t = 0;
	for(int j = 0; j < M; j++)
	{
		for(i = t; i < N; i++)
			if(mat[i][j])
			{
				swap(mat[i], mat[t]);
				swap(ans[i], ans[t]);
				break;
			}
		if(i >= N)
			continue;
		for(i = t+1; i < N; i++)
			if(mat[i][j])
			{
				mat[i] ^= mat[t];
				ans[i] ^= ans[t];
			}
		t++;
	}
	//for(int i = 0; i < N; i++)
		//cout << mat[i].to_string() << ' ' << ans[i] << endl;
}

int main()
{
	vector<int> pre;
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	scanf("%d %d%*c", &N, &M);
	for(int i = 0; i < N; i++)
	{
		gets(str);
		build(i);
	}
	solve();
	for(int i = N; i--;)
	{
		pre.clear();
		for(int j = 0; j < M; j++)
			if(mat[i][j])
			{
				if(resp[j] != 0)
					ans[i] ^= resp[j] == 'T';
				else
					pre.push_back(j);
			}
		if(pre.size())
		{
			for(int j = pre.size()-1; j > 0; j--)
			{
				resp[pre[j]] = 'T';
				ans[i] ^= 1;
			}
			resp[pre[0]] = ans[i] ? 'T' : 'F';
			ans[i] = 0;
		}
		if(ans[i])
			return !printf("impossible\n");
	}
	for(int i = 0; i < M; i++)
		if(resp[i] == 0)
			resp[i] = 'T';
	for(int i = 0; i < M; i++)
	{
		resp2[i] = resp[M-1-i];
	}
	printf("%s\n", resp2);
	return 0;
}
