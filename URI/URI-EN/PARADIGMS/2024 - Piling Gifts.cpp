// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Piling Gifts
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2024

#include<bits/stdc++.h>
#define ll long long
#define INFN -1000000000LL
using namespace std;

int N;
int X[100001], Y[1000001], Z[100001];
ll dp[100001][3];

ll solve(int x, int r)
{
	int la, lb;
	if(x > N)
		return 0;
	if(dp[x][r] != -1)
		return dp[x][r];
	dp[x][r] = INFN;
	if(r == 0)
	{
		la = X[x-1];
		lb = Y[x-1];
	}
	if(r == 1)
	{
		la = X[x-1];
		lb = Z[x-1];
	}
	if(r == 2)
	{
		la = Y[x-1];
		lb = Z[x-1];
	}
	if(((la >= X[x]) && (lb >= Y[x])) || ((lb >= X[x]) && (la >= Y[x])))
		dp[x][r] = max(dp[x][r], solve(x+1, 0) + Z[x]);	
	if(((la >= X[x]) && (lb >= Z[x])) || ((lb >= X[x]) && (la >= Z[x])))
		dp[x][r] = max(dp[x][r], solve(x+1, 1) + Y[x]);
	if(((la >= Y[x]) && (lb >= Z[x])) || ((lb >= Y[x]) && (la >= Z[x])))
		dp[x][r] = max(dp[x][r], solve(x+1, 2) + X[x]);
	//printf("%d %d %lld\n", x, r, dp[x][r]);
	return dp[x][r];
}

main()
{
	int i;
	ll x;
	X[0] = Y[0] = Z[0] = 1000000;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		scanf("%d %d %d", &X[i], &Y[i], &Z[i]);
		dp[i][0] = dp[i][1] = dp[i][2] = -1;
	}
	x = solve(1, 0);
	printf("%lld\n", x <= 0 ? -1 : x);
	return 0;
}

