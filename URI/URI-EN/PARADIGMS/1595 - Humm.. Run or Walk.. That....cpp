// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Humm.. Run or Walk.. That...
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1595

#include<bits/stdc++.h>

using namespace std;

int vet[100001];

main()
{
	int i, S, C, R, N;
	double acum;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d", &S, &C, &R);
		acum = 0;
		for(i = 0; i < S; i++)
		{
			scanf("%d", &vet[i]);
			acum += 1.0/vet[i];
		}
		sort(vet, vet+S);
		for(i = 0; i < C; i++)
			acum -= ((double)R)/(vet[i]*(vet[i]+R));
		printf("%.2lf\n", acum);
	}
	return 0;
}

