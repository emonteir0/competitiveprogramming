// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ginástica
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2664

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
using namespace std;

ll pd[51][100001];

main()
{
	int i, j, T, M, N;
	ll ac = 0;
	scanf("%d %d %d", &T, &M, &N);
	for(i = M; i <= N; i++)
		pd[0][i] = 1;
	for(i = 1; i < T; i++)
	{
		for(j = M; j <= N; j++)
			pd[i][j] = (pd[i-1][j-1] + pd[i-1][j+1]) % MOD;
	}
	for(i = M; i <= N; i++)
		ac += pd[T-1][i];
	printf("%lld\n", ac%MOD);
	return 0;
}
