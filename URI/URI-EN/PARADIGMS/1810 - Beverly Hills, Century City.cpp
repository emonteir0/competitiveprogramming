// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Beverly Hills, Century City
// Level: 10
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1810

#include<bits/stdc++.h>
#define MAXN 181
using namespace std;


int f[MAXN], p[MAXN], h[MAXN], d[MAXN], c[MAXN];
int H[MAXN], D[MAXN];


main()
{
	int N, t = 0, r;
	while (scanf("%d", &N) == 1 && N) 
	{
	
		for (int i = 1; i <= N; i++)
			scanf("%d %d %d %d", &f[i], &p[i], &h[i], &d[i]);
		
		for (int j = 1; j <= N; j++) 
		{
			c[j] = p[j];
			for (int i = j; i <= N; i++) 
				c[j] += h[i];
		}
		
		D[0] = r = 0;
		for (int i = 1; i <= N; i++)
			D[i] = D[i-1] + d[i];
		for (int i = 1; i <= N; i++)
			r += h[i] * D[i];
		H[0] = 0;
		for (int i = 1; i <= N; i++) 
		{
			H[i] = H[0] + f[1] + c[1] * (D[i] - D[0]);
			for (int j = 2; j <= i; j++)
				H[i] = min (H[i], H[j-1] + f[j] + c[j] * (D[i] - D[j-1]));
		}
		
		printf ("Instancia #%d\n%d\n\n", ++t, H[N]-r);
	}
	return 0;
}
