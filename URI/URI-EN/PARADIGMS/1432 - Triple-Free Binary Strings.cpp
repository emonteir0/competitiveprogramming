// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triple-Free Binary Strings
// Level: 8
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1432

#include<bits/stdc++.h>
#define REP(i, x) for(int i = 0; i < x; i++)
#define ALPHA 2
#define MAX 36000
#define ll long long

using namespace std;

int V;
int trie[MAX][ALPHA];
int fn[MAX];
int term[MAX];
int st[MAX];

int node() 
{
	REP(i, ALPHA)
		trie[V][i] = 0;
	fn[V] = 0;
	return V++;
}

void insert(string& s, int id) 
{
	int t = 0;
	REP(i, (int)s.length())
	{
		int c = s[i] - '0';
		if (trie[t][c] == 0)
		{
			trie[t][c] = node();
			term[trie[t][c]] = 0;
		}
		t = trie[t][c];
	}
	term[t] = 1;
	st[t] = id;
}

void init_aho() 
{
	queue<int> Q;
	REP(i, ALPHA)
		if(trie[0][i])
			Q.push(trie[0][i]);
	while (!Q.empty()) 
	{
		int t = Q.front(); Q.pop();

		REP(c, ALPHA) 
		{
			int x = trie[t][c];
			if (x) 
			{
				Q.push(x);
				fn[x] = fn[t];
				while (fn[x] && trie[fn[x]][c] == 0)
					fn[x] = fn[fn[x]];
				if (trie[fn[x]][c])
					fn[x] = trie[fn[x]][c];
				if(term[fn[x]])
				{
					term[x] |= 2;
				}
			}
			else
				trie[t][c] = trie[fn[t]][c];
		}
	}
}

void build()
{
	int x, k = 0;
	string s;
	char str[11];
	V = 0;
	node();
	for(int i = 1, tam = 2; i <= 10; i++, tam <<= 1)
	{
		for(int j = 0; j < tam; j++)
		{
			x = j;
			for(int k = 0; k < i; k++)
			{
				str[k] = (x&1) + '0';
				x >>= 1;
			}
			str[i] = 0;
			s = str;
			s = ((s+str)+str);
			//cout << s.size() << endl;
			insert(s, k++);
		}
	}
	init_aho();
}

int N, t;
string str;

int vis[31][36000];
int dp[31][36000];


ll solve(int x, int n)
{
	int y = 0, z = 0;
	if(term[n])
		return 0;
	if(x == N)
		return 1;
	if(vis[x][n] == t)
		return dp[x][n];
	vis[x][n] = t;
	ll r = 0;
	if(str[x] != '*')
	{
		y = trie[n][str[x]-'0'];
		r = solve(x+1, y);
	}
	else
	{
		y = trie[n][0];
		z = trie[n][1];
		r = solve(x+1, y) + solve(x+1, z);
	}
	return dp[x][n] = r;
}

main()
{
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	build();
	while((cin >> N) && N)
	{
		t++;
		cin >> str;
		cout << "Case " << t << ": " << solve(0, 0) << endl;
	}
	return 0;
}
