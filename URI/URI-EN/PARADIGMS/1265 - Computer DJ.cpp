// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Computer DJ
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1265

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int N, Q;
char palavra[51][1001];
int number[63];

int num(ll a, int dig, int sz)
{
	int k = sz;
	for(int i = 0; i < sz; i++)
		number[i] = 0;
	while(a && k)
	{
		number[--k] = a%N;
		a /= N;
	}
	return number[dig];
}

int solve(ll x)
{
	ll y = N, f;
	int z = 1, g;
	while(x >= y*z)
	{
		x -= y*z;
		y *= N;
		z++;
	}
	f = x/z;
	g = x%z;
	//printf("%d %d %d\n", f, g, z);
	return num(f, g, z);
}


main()
{
	ll x;
	while(scanf("%d %d", &N, &Q) == 2 && N)
	{
		for(int i = 0; i < N; i++)
			scanf("%s", palavra[i]);
		while(Q--)
		{
			scanf("%lld", &x);
			if(N == 1)
				printf("%s\n", palavra[0]);
			else
				printf("%s\n", palavra[solve(x-1)]);
		}
		printf("\n");
	}
	return 0;
}
