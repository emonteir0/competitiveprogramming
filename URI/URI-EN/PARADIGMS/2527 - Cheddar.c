// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cheddar
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2527

#include<stdio.h>
#include<math.h>
#define ll unsigned long long int

double mat[1002][6001];
double sum[1002][6001];

main()
{
	int i, j, k, n, x, y;
	mat[1][1] = 1.0/6;
	mat[1][2] = 1.0/6;
	mat[1][3] = 1.0/6;
	mat[1][4] = 1.0/6;
	mat[1][5] = 1.0/6;
	mat[1][6] = 1.0/6;
	for(i=2;i<=1000;i++)
		for(j=2;j<=6000;j++)
			for(k=1;k<=6;k++)
				if(j>k)
					mat[i][j] += mat[i-1][j-k]/6;
	for(i = 1; i <= 1000; i++)
		for(j = 1; j <= 6000; j++)
			sum[i][j] = sum[i][j-1] + mat[i][j-1];
	while(scanf("%d %d", &x, &y) != EOF)
		printf("%.4lf\n",1.0 - sum[x][y]);
	return 0;
}
