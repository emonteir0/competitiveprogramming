// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Assembling
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2106

#include<stdio.h>

int N;
int mat[18][18];
int memo[18][1<<18];

int min(int a, int b)
{
	return (a<b)? a: b;
}

int solve(int k, int bitsy)
{
	int i, me = 999999999;
	if(k == N)
		return 0;
	if(memo[k][bitsy] != -1)
		return memo[k][bitsy];
	for(i = 0; i < N; i++)
		if((bitsy & 1<<i)==0)
			me = min(me, solve(k+1, bitsy | (1<<i)) + mat[i][k]);
	return memo[k][bitsy] = me;
}

main()
{
	int i, j, tam;
	while(scanf("%d", &N) == 1 && N)
	{
		tam = 1<<N;
		for(i=0;i<N;i++)
		{
			for(j=0;j<N;j++)
				scanf("%d", &mat[i][j]);
			for(j=0;j<tam;j++)
				memo[i][j] = -1;
		}
		printf("%d\n", solve(0, 0));
	}
	return 0;
}

