// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Balancing Gifts
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2723

#include<bits/stdc++.h>

using namespace std;

int N;
int vet[10001];
int memo[10001][11];

int solve(int x, int y)
{
	if(x == N)
		return 1;
	if(memo[x][y] != -1)
		return memo[x][y];
	int s = 0;
	if(y - vet[x] >= 0)
		s |= solve(x+1, y-vet[x]);
	if(y + vet[x] <= 10)
		s |= solve(x+1, y+vet[x]);
	return memo[x][y] = s;
}

main()
{
	int i, j, T;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		for(i = 0; i < N; i++)
			for(j = 0; j < 11; j++)
				memo[i][j] = -1;
		printf("%s\n", solve(0, 5) ? "Feliz Natal!" : "Ho Ho Ho!");
	}
	return 0;
}
