// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Candy
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1372

#include<bits/stdc++.h>

using namespace std;

int N, M;
int mat[1000001];
int linha[100001];
int dp[100001];

int val(int i, int j)
{
	return i*M+j;
}

int solve(int x, int y)
{
	if(y >= M)
		return 0;
	if(y == M-1)
		return mat[val(x, y)];
	if(dp[y] != -1)
		return dp[y];
	return dp[y] = max(solve(x, y+2)+mat[val(x, y)], solve(x, y+3)+mat[val(x, y+1)]);
}

int solve2(int x)
{
	if(x >= N)
		return 0;
	if(x == N-1)
		return linha[x];
	if(dp[x] != -1)
		return dp[x];
	return dp[x] = max(solve2(x+2) + linha[x], solve2(x+3) + linha[x+1]);
}

main()
{
	int sz;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		sz = M*N;
		for(int i = 0; i < sz; i++)
			scanf("%d", &mat[i]);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < M; j++)
				dp[j] = -1;
			linha[i] = solve(i, 0);
		}
		for(int j = 0; j < N; j++)
			dp[j] = -1;
		printf("%d\n", solve2(0));
	}
	return 0;
}
