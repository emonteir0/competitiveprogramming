// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Walk of Fame
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1777

#include<bits/stdc++.h>

using namespace std;

int st[10001], en[10001];
int vet[10001];

map<int, int> Mapa, Mapa2;
map<int, int>::iterator it;

main()
{
	int T, t, N, M, x, cont;
	scanf("%d", &T);
	for(t = 1; t <= T; t++)
	{
		Mapa = Mapa2;
		scanf("%*d %d", &N);
		for(int i = 0; i < N; i++)
			scanf("%d %d", &st[i], &en[i]);
		scanf("%d", &M);
		for(int i = 0; i < M; i++)
		{
			scanf("%d", &x);
			Mapa[x]++;
		}
		cont = 0;
		for(int i = 0; i < N; i++)
		{
			for(it = Mapa.lower_bound(st[i]); it != Mapa.upper_bound(en[i]); it++)
			{
				cont += it->second;
			}
			Mapa.erase(Mapa.lower_bound(st[i]), Mapa.upper_bound(en[i]));
		}
		printf("Caso #%d: %d\n", t, cont);
	}
	return 0;
}
