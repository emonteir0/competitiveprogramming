// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Manufacture of Presents
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2475

#include<stdio.h>
#define ll long long
#define INF (ll)1e18

ll F[501][10001], acum[10001];
int P[501][10001];

ll cost(int i, int j)
{
	if (i > j)
		return 0;
	return (acum[j]-acum[i-1])*(j-i+1);
}

void fill(int g, int l1, int l2, int p1, int p2) 
{
    int lm, k;
	if (l1 > l2) 
		return;
	lm = (l1+l2)/2;
    P[g][lm] = -1;
    F[g][lm] = INF;
    for (k = p1; k <= p2; k++) 
	{
        ll new_cost = F[g-1][k] + cost(k+1,lm);
        if (F[g][lm] > new_cost) 
		{
            F[g][lm] = new_cost;
            P[g][lm] = k;
        }
    }
	fill(g, l1, lm-1, p1, P[g][lm]);
    fill(g, lm+1, l2, P[g][lm], p2);
}  

main()
{
	int i, N, M;
	ll x;
	scanf("%d %d", &N, &M);
	for(i = 1; i <= N; i++)
	{
		scanf("%lld", &x);
		acum[i] = acum[i-1] + x;
		F[1][i] = cost(1, i);
	}
	for(i = 2; i <= M; i++)
		fill(i, 1, N, 1, N);
	printf("%lld\n", F[M][N]);
	return 0;
}
