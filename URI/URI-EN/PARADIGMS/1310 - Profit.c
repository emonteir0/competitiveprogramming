// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Profit
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1310

#include<stdio.h>

int vet[50];

main()
{
	int i,n,val,x,maxateagora,maxaqui;
	while(scanf("%d",&n)==1)
	{
		scanf("%d",&val);
		for(i=0;i<n;i++)
		{
			scanf("%d",&x);
			vet[i]=x-val;
		}
		maxateagora=0;
		maxaqui=0;
		for(i=0;i<n;i++)
		{
			maxaqui = maxaqui+vet[i]>0?maxaqui+vet[i]:0;
        	maxateagora = maxateagora>maxaqui?maxateagora:maxaqui;
		}
		printf("%d\n",maxateagora>0?maxateagora:0);
	}
	return 0;
}
