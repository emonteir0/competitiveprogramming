// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fofão of Persia
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2042

#include<bits/stdc++.h>
#define ll long long
#define INF -9999999999999999LL

using namespace std;

ll memo[101][1001];
vector< vector<int> > G(101);
pair<int, int> adj[101][101];
int dest;

ll solve(int u, int sol)
{
	ll ans = INF;
	pair<int, int> p;
	if(sol < 0)
		return INF;
	if(memo[u][sol] != -1)
		return memo[u][sol];
	if(u == dest)
		return memo[u][sol] = 0;
	for(int i = 0; i < G[u].size(); i++)
	{
		p = adj[u][G[u][i]];
		ans = max(ans, solve(G[u][i], sol-p.second) + p.first);
	}
	return memo[u][sol] = ans;
}

main()
{
	int i, j, N, M, sol, source, x, y, val, val2;
	ll res;
	scanf("%d %d %d %d %d", &N, &M, &source, &dest, &sol);
	for(i = 0; i <= N; i++)
		for(j = 0; j <= sol; j++)
			memo[i][j] = -1;
	while(M--)
	{
		scanf("%d %d %d %d", &x, &y, &val, &val2);
		adj[x][y] = adj[y][x] = make_pair(val, val2);
		G[x].push_back(y);
		G[y].push_back(x);
	}
	res = solve(source, sol);
	printf("%lld\n", res >= 0 ? res : -1);
	return 0;
}
