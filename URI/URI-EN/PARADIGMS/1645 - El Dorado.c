// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: El Dorado
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1645

#include<stdio.h>
#define ll long long

ll dp[100][100][101];
int vet[100];

ll solve(int a, int p, int q)
{
	int i;
	ll s = 0;
	//printf("%d\n", q);	
	if(q == 1)
		return dp[a][p][q] = vet[a] < vet[p];
	if(dp[a][p][q] != -1)
		return dp[a][p][q];
	if(vet[a] >= vet[p])
		return dp[a][p][q] = 0;
	for(i = 0; i < a; i++)
		s += solve(i, a, q-1);
	return dp[a][p][q] = s;
		
}

main()
{
	int i, j, k, N, K;
	ll sum;
	while(scanf("%d %d", &N, &K) == 2 && (N||K))
	{
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < N; j++)
				for(k = 1; k <= K; k++)
					dp[i][j][k] = -1;
			scanf("%d", &vet[i]);
		}
		if(K == 1)
			printf("%d\n", N);
		else
		{
			sum = 0;
			for(i = 0; i < N; i++)
				for(j = 0; j < i; j++)
					sum += solve(j, i, K-1);
			printf("%lld\n", sum);
		}
	}
	return 0;
}
