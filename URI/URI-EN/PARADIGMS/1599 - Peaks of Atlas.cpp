// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Peaks of Atlas
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1599

#include<bits/stdc++.h>

using namespace std;

int N, M;
int mat[1002][1002];
int dx[] = {1, 1,  1, 0,  0, -1, -1, -1};
int dy[] = {1, 0, -1, 1, -1,  1,  0, -1};

int inRange(int x, int y)
{
	return x >= 1 && x <= N && y >= 1 && y <= M;
}

main()
{
	int k;
	vector< pair<int, int> > ans;
	while(scanf("%d %d", &N, &M) == 2)
	{
		ans.clear();
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= M; j++)
				scanf("%d", &mat[i][j]);
			
		for(int i = 1; i <= N; i++)	
			for(int j = 1; j <= M; j++)
			{
				for(k = 0; k < 8; k++)
				{
					if(!inRange(i+dx[k], j+dy[k]))
						continue;
					if(mat[i+dx[k]][j+dy[k]] >= mat[i][j])
						break;
				}
				if(k == 8)
					ans.push_back(make_pair(i, j));
			}
		if(ans.size() == 0)
			printf("-1\n\n");
		else
		{
			sort(ans.begin(), ans.end());
			for(int i = 0; i < ans.size(); i++)
				printf("%d %d\n", ans[i].first, ans[i].second);
			printf("\n"); 
		}
	}
	return 0;
}
