// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: St. Petersburg Bridges
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1203

#include<stdio.h>

int vet[101];
int valores[11000];

main()
{
	int i, j, n, k, tot, a, b, x;
	valores[0] = 1;
	while(scanf("%d %d", &n, &k) == 2)
	{
		tot = 2*k;
		for(i = 1; i <= n; i++)
			vet[i] = 0;
		for(i = 0; i < k; i++)
		{
			scanf("%d %d", &a, &b);
			vet[a]++;
			vet[b]++;
		}
		for(i = 1; i <= k; i++)
			valores[i] = 0;
		for(i = 1; i <= n; i++)
		{
			x = vet[i];
			for(j = k; j >= x; j--)
				if(valores[j-x])	valores[j] = 1;
		}
		printf("%s\n", valores[k]?"S":"N");
	}
	return 0;
}
