// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Playing with Transformations
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2363

#include<stdio.h>
#define ll long long
#define MOD 1000000007
#define INV2 500000004

ll n;

ll max(ll a, ll b)
{
	return (a > b) ? a : b;
}

ll min(ll a, ll b)
{
	return (a < b) ? a : b;
}

ll intersecao(ll ind1, ll ind2, ll a, ll b)
{
	ll c = max(a, ind1);
	ll d = min(b, ind2);
	if(c > d)
		return 0;
	return ((MOD-(d-c+1))%MOD + MOD) % MOD;
}

ll f(ll ind1, ll ind2, ll a, ll b)
{
	ll v1, v2;
	char x = (ind1 >= a) &&  (ind1 <= b);
	char y = (ind2 >= a) &&  (ind2 <= b);
	if(intersecao(ind1, ind2, a, b) == 0 || ind1 < 0 || ind2 > n)
		return 0;
	if(x && y)
		return (((((ind2-ind1+1)%MOD)*((ind2-ind1+2)%MOD))%MOD)*INV2)%MOD;
	if((ind1 & 1) != (ind2 & 1))
	{
		v1 = (2*f(ind1, (ind1+ind2)/2, a, b))%MOD;
		v2 = (2*f((ind1+ind2)/2 + 1, ind2, a, b) +  + intersecao((ind1+ind2)/2 + 1, ind2, a, b))%MOD;
	}
	else
	{
		v1 = (2*f(ind1, (ind1+ind2)/2 - 1, a, b))%MOD;
		v2 = (2*f((ind1+ind2)/2, ind2, a, b)  + intersecao((ind1+ind2)/2, ind2, a, b))%MOD;
	}
	//printf("%lld - %lld / ESQ = %lld / DIR = %lld\n", ind1, ind2, v1, v2);
	return (v1+v2)%MOD;
}


main()
{
	ll a, b;
	while(scanf("%lld %lld %lld", &n, &a, &b) == 3)
		printf("%lld\n", f(1, n, a, b));
	return 0;
}

