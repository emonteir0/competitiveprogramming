// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Optimal Production of Gre...
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1210

#include<bits/stdc++.h>

using namespace std;

int N, I, M, P;
int dp[2001][2001];
int custo[2001];
int venda[2001];

int solve(int x, int y)
{
	int r;
	if(x == N)
		return dp[x][y] = 0;
	if(dp[x][y] != -1)
		return dp[x][y];
	if(y > 0)
		r = solve(x+1, 1) - venda[y] + P + custo[0];
	if(y < M)
		r = min(r, solve(x+1, y+1) + custo[y]);
	return dp[x][y] = r;
}

main()
{
	int total, p;
	vector<int> ans;
	while(scanf("%d %d %d %d", &N, &I, &M, &P) == 4)
	{
		ans.clear();
		for(int i = 0; i < M; i++)
			scanf("%d", &custo[i]);
		for(int i = 1; i <= M; i++)
			scanf("%d", &venda[i]);
		for(int i = 0; i < N; i++)
			for(int j = 0; j <= M; j++)
				dp[i][j] = -1;
		printf("%d\n", total = solve(0, I));
		p = I;
		for(int i = 0; i < N; i++)
		{
			if(dp[i+1][1] - venda[p] + P + custo[0] == total)
			{
				total -= -venda[p] + P + custo[0];
				p = 1;
				ans.push_back(i+1);
			}
			else
			{
				total -= custo[p];
				p++;
			}
		}
		if(ans.size() == 0)
			printf("0\n");
		else
		{
			printf("%d", ans[0]);
			for(int i = 1; i < ans.size(); i++)
				printf(" %d", ans[i]);
			printf("\n");
		}
	}
	return 0;
}
