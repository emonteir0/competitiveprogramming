// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: It's a Trap Bino
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2481

//Convex Hull Trick:
//dp[i] = min{dp[j] + m[j]*x[i]} j < i
//Minimizar dp[i] escolhendo uma reta
#include<bits/stdc++.h>
#define ll long long

using namespace std;

struct line
{
	ll m, n;
    line(){}
    line(ll _m, ll _n):
        m(_m), n(_n){}
};

int X[100001], C[100001];
ll dp[100001];
line hull[100001];
int hullSize, hullPtr;

vector<line> E;

ll y(int idx, ll x) {
	//printf("%d %lld %lld %lld\n", idx, hull[idx].m, hull[idx].n, hull[idx].m*x + hull[idx].n);
    return hull[idx].m*x + hull[idx].n;
}

double intersection(line t, line r) {
    double num = double(r.n - t.n);
    double den = double(t.m - r.m);
    return num / den;
}

void insertline(line l)
{
	while(hullSize >= 2 && ( intersection(l, hull[hullSize-2]) < intersection(hull[hullSize-1], hull[hullSize-2]) ) ) {
        if (hullPtr == hullSize - 1) hullPtr--;
        hullSize--;
    }
    hull[ hullSize++ ] = l;
}

ll query(int x)
{
	while(hullPtr+1 < hullSize && y(hullPtr, x) < y(hullPtr+1, x)) hullPtr++;
    	return y(hullPtr, x);
}

main()
{
	int i,j, N, T;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		for(i = 1; i <= N; i++)
			scanf("%d %d", &X[i], &C[i]);
		hullSize = hullPtr = 0;
		for(i = 1; i <= N; i++)
		{
			//for(j = 1; j < i; j++)
			dp[i] = max(0LL, query(X[i])) - (1LL*X[i]*X[i] + C[i]);
				//dp[i] = max(dp[i], dp[j] + X[j]*X[i]);
			//dp[i] -= (1LL*X[i]*X[i] + C[i]);
			insertline(line(X[i], dp[i]));
		}
		dp[N] += 1LL*X[N]*X[N];
		if(dp[N] > 0)
			printf("Ganha %lld\n", dp[N]);
		else
			printf("Perde %lld\n", -dp[N]);
	}
	return 0;
}
