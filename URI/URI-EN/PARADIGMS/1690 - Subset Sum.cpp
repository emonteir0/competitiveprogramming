// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Subset Sum
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1690

#include<bits/stdc++.h>

using namespace std;

int vet[10001];


main()
{
	int i, N, K;
	long long res;
	scanf("%d", &K);
	while(K--)
	{
		scanf("%d", &N);
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		res = 1;
		for(i = 0; i < N && vet[i] <= res; i++)
			res += vet[i];
		printf("%lld\n", res);
	}
	return 0;
}

