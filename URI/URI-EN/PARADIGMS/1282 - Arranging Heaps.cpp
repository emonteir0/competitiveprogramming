// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Arranging Heaps
// Level: 9
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1282

//Convex Hull Trick 2D:
//dp[i][j] = min{dp[i-1][k] + m[k]*x[j]} k < j
//Minimizar dp[i] escolhendo uma reta

#include<bits/stdc++.h>
#define ll long long
#define INF 1e18

using namespace std;

struct line
{
	ll m, n;
    line(){}
    line(ll _m, ll _n):
        m(_m), n(_n){}
};

int X[1001], W[1001];
ll dp[1001][1001], sumW[1001], sumXW[1001];
line hull[1001];
int hullSize, hullPtr;


ll y(int idx, ll x) {
    return hull[idx].m*x + hull[idx].n;
}

double intersection(line t, line r) {
    double num = double(r.n - t.n);
    double den = double(t.m - r.m);
    return num / den;
}

void insertline(line l)
{
	while(hullSize >= 2 && ( intersection(l, hull[hullSize-2]) < intersection(hull[hullSize-1], hull[hullSize-2]) ) ) {
        if (hullPtr == hullSize - 1) hullPtr--;
        hullSize--;
    }
    hull[ hullSize++ ] = l;
}

ll query(int x)
{
	while(hullPtr+1 < hullSize && y(hullPtr, x) > y(hullPtr+1, x)) hullPtr++;
    	return y(hullPtr, x);
}

main()
{
	int i, j, N, K;
	while(scanf("%d %d", &N, &K) == 2)
	{
		for(i = 1; i <= N; i++)
		{
			scanf("%d %d", &X[i], &W[i]);
			sumW[i] = sumW[i-1] + W[i];
			sumXW[i] = sumXW[i-1] + X[i]*W[i];
			for(j = 1; j <= K; j++)
				dp[i][j] = INF;
		}
		for(i = 1; i <= N; i++)
			dp[1][i] = X[i]*sumW[i] - sumXW[i];
		for(i = 2; i <= K; i++)
		{
			hullPtr = hullSize = 0;
			for(j = i; j <= N; j++)
			{
				insertline(line(-sumW[j-1], dp[i-1][j-1]+sumXW[j-1]));
				dp[i][j] = query(X[j]) + X[j]*sumW[j] - sumXW[j];	
			}
		}
		printf("%lld\n", dp[K][N]);
	}
	
	return 0;
}
