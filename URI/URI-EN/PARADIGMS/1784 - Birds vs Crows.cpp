// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Birds vs Crows
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1784

#include<bits/stdc++.h>

using namespace std;


int N;
int vet[9], vet2[9];
int G[9][9];
int dp[9][512];

int solve(int x, int bit)
{
	if(x == N)
		return 0;
	if(dp[x][bit] != -1)
		return dp[x][bit];
	int r = 999999999, v = 1;
	for(int i = 0; i < N; i++, v <<= 1)
	{
		if(!(bit & v))
			r = min(r, max(G[x][i], solve(x+1, bit | v)));
	}
	return dp[x][bit] = r;
}

main()
{
	int T, lim;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		scanf("%d", &N);
		lim = 1 << N;
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &vet2[i]);
			for(int j = 0; j < N; j++)
				G[j][i] = (vet[j]/__gcd(vet[j], vet2[i])) * vet2[i];
			for(int j = 0; j < lim; j++)
				dp[i][j] = -1;
		}
		printf("Caso #%d: %d\n", t, solve(0, 0));
	}
	return 0;
}
