// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Apples
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1517

#include<stdio.h>
int N;
int memo[1001][1001];
int x[1001], y[1001], t[1001];

int max(int a, int b)
{
	return (a>b)? a:b;
}

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

int solve(int i, int j)
{
	if(j == N+1)
		return 0;
	if(memo[i][j] != -1)
		return memo[i][j];
	if(t[j]-t[i] >= max(abs(x[j]-x[i]), abs(y[j]-y[i])))
		return memo[i][j] = max(solve(j, j+1) + 1, solve(i, j+1));
	return memo[i][j] = solve(i, j+1);
}


main()
{
	int i, j;
	while(scanf("%*d %*d %d", &N) == 1 && N)
	{
		for(i = 1; i <= N; i++)
			scanf("%d %d %d", &x[i], &y[i], &t[i]);
		scanf("%d %d", &x[0], &y[0]);
		for(i = 0; i <= N; i++)
			for(j = 0; j <= N; j++)
				memo[i][j] = -1;
		printf("%d\n", solve(0,1));
	}
	return 0;
}
