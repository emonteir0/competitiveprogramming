// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Convert Kilometers to Miles
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1643

#include<bits/stdc++.h>

using namespace std;

int fib[50];

main()
{
	int i, lim, T, x, y;
	fib[0] = 1;
	fib[1] = 2;
	for(i = 2; ; i++)
	{
		fib[i] = fib[i-1] + fib[i-2];
		if(fib[i] > 25000)
			break;
	}
	lim = i-2;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &x);
		y = 0;
		for(i = lim; i >= 1; i--)
		{
			if(x >= fib[i])
			{
				x -= fib[i];
				y += fib[i-1];
			}
		}
		printf("%d\n", y);
	}
	return 0;
}
