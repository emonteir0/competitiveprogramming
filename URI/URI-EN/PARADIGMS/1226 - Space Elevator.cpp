// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Space Elevator
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1226

#include<stdio.h>
#include<string.h>
#define ll long long
#define llu unsigned long long


char numero[20];
ll memo[20][2][2];

ll solve(int dig, int maior, int one)
{
	int i;
	if(!numero[dig])
		return 1;
	ll &r = memo[dig][maior][one];
	if(r != -1)
		return r;
	r = 0;
	for(i = 0; i <= 9; i++)
	{	
		if(maior && (i + '0') > numero[dig])
			break;
		if((one && i == 3) || (i == 4))
			continue;
		r += solve(dig+1, maior && ((i + '0') == numero[dig]), i == 1);
	}
	return r;
}

main()
{
	llu x, y, lo, hi, mid, ans;
	while(scanf("%llu", &x) == 1)
	{
		lo = 0;
		hi = 10000000000000000000ULL;
		while(lo <= hi)
		{
			mid = lo + (hi-lo)/2;
			sprintf(numero, "%llu", mid);
			memset(memo, -1, sizeof(memo));
			y = solve(0, 1, 0);
			if(y == x+1)
			{
				ans = mid;
				hi = mid-1;
			}
			else if(y < x+1)
				lo = mid+1;
			else
				hi = mid-1;
		}
		printf("%llu\n", ans);
	}
	return 0;
}
