// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Freshman Beats Veteran?
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1892

#include<bits/stdc++.h>
#define ll long long

using namespace std;

string vet[100001];
int tree[100001];
int N;
vector<string> numeros;

void update(int i, int val)
{
	++i;
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}


main()
{
	char str[11];
	int x, y;
	ll ans = 0;
	while(scanf("%d", &N) == 1)
	{
	    numeros.clear();
		ans = 0;
		for(int i = 0; i < N; i++)
		{
			tree[i+1] = 0;
			scanf("%s", &str);
			vet[i] = str;
			numeros.push_back(vet[i]);
		}
		sort(numeros.begin(), numeros.end());
		for(int i = N-1; i >= 0; i--)
		{
			x = lower_bound(numeros.begin(), numeros.end(), vet[i])-numeros.begin();
			ans += sum(x-1);
			update(x, 1);
		}
		printf("%lld\n", ans);
	}
	return 0;
}
