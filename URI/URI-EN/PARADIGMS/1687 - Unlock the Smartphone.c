// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Unlock the Smartphone
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1687

#include<stdio.h>
#include<stdlib.h>
#define ll unsigned long long int
#define MOD 1300031

int F[225][225], M[225][225];
ll G[225][225];

int mdc(int a, int b)
{
	int aux;
	if(a == 1 && b == 0)
		return 1;
	if(a == 0 && b == 1)
		return 1;
	if(a == 0 || b == 0)
		return 0;
	while(b != 0)
	{
		aux = b;
		b = a%b;
		a = aux;
	}	
	return a == 1;
}

void multiply(int tam);
void multiply2(int tam);
   
void power(int tam, ll n)
{
	int i,j;
	if(n==0||n==1)
		return;	
	power(tam, n/2);
	multiply2(tam);
	if(n%2==1)
		multiply(tam);
}
   
void multiply(int tam)
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*M[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2(int tam)
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*F[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}


int sum(int tam, ll K)
{
	int i, j;
	ll soma = 0;
	power(tam, K-1);
	for(i=0;i<tam;i++)
		for(j=0;j<tam;j++)
			soma += F[i][j];
	return soma%MOD;
}


main()
{
	int i, j, k, l, N, N2, x, y;
	ll K;
	while(scanf("%d %lld", &N, &K)==2 && (N||K))
	{
		N2 = N*N;
		for(i=0;i<N;i++)
			for(j=0;j<N;j++)
				for(k=0;k<N;k++)
					for(l=0;l<N;l++)
					{
						x = i*N+j;
						y = k*N+l;
						F[x][y] = M[x][y] = mdc(abs(i-k),abs(j-l));
					}
		if(K == 1)
			printf("%d\n", N2);
		else
			printf("%d\n", sum(N2, K));
	}
	return 0;
}
