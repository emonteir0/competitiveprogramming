// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Helping Gust-Avô
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2220

#include<bits/stdc++.h>

using namespace std;

char str[1000001], str2[1000001];

int N, M;
vector<int> MA[26];
vector<int> MI[26];
vector<int>::iterator it;

char minusc(char c)
{
	if(c >= 'A' && c <= 'Z')
		return 0;
	return 1;
}

int teste(int x)
{
	int p = 0, ch;
	for(int i = 0; i < M; i++)
	{
		if(minusc(str2[i]))
		{
			ch = str2[i]-'a';
			for(int j = 0; j < x; j++)
			{
				it = lower_bound(MI[ch].begin(), MI[ch].end(), p);
				if(it == MI[ch].end())
					return 0;
				p = (*it)+1;
			}
		}
		else
		{
			ch = str2[i]-'A';
			for(int j = 0; j < x; j++)
			{
				it = lower_bound(MA[ch].begin(), MA[ch].end(), p);
				if(it == MA[ch].end())
					return 0;
				p = (*it)+1;
			}
		}
	}
	return 1;
}

main()
{
	int T, lo, hi, mid, ans;
	scanf("%d%*c", &T);
	while(T--)
	{
		for(int i = 0; i < 26; i++)
		{
			MA[i].clear();
			MI[i].clear();
		}
		gets(str);
		gets(str2);
		for(N = 0; str[N]; N++)
		{
			if(minusc(str[N]))
				MI[str[N]-'a'].push_back(N);
			else
				MA[str[N]-'A'].push_back(N);
		}
		M = strlen(str2);
		if(M == 0)
		{
			printf("0\n");
			continue;
		}
		lo = 0;
		hi = N/M;
		while(lo <= hi)
		{
			mid = (lo+hi)/2;
			if(teste(mid))
			{
				ans = mid;
				lo = mid+1;
			}
			else
				hi = mid-1;
		}
		printf("%d\n", ans);
	}
	return 0;
}
