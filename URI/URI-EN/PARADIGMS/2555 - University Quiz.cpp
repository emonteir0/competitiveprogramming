// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: University Quiz
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2555

#include<bits/stdc++.h>

using namespace std;

int N, K;
int val[1001], prob[1001];
double pd[1001][1001];

double solve(int x, int y)
{
	if(x == N)
		return 0;
	if(pd[x][y] != -1)
		return pd[x][y];
	double r = (prob[x]*(val[x]+solve(x+1, y)))/100;
	if(y)
		r = max(r, val[x]+solve(x+1, y-1));
	return pd[x][y] = r;
}

main()
{
	while(scanf("%d %d", &N, &K) == 2)
	{
		for(int i = 0; i <= N; i++)
			for(int j = 0; j <= K; j++)
				pd[i][j] = -1;
		for(int i = 0; i < N; i++)
			scanf("%d", &val[i]);
		for(int i = 0; i < N; i++)
			scanf("%d", &prob[i]);
		printf("%.2lf\n", solve(0, K));
	}
	return 0;
}
