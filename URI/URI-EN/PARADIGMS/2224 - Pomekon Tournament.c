// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pomekon Tournament
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2224

#include<stdio.h>
#include<math.h>

int pot[17], tam, N;
double memo[65536], mat[16][16];
int x[16], y[16];

int sqr(int x)
{
	return x*x;
}

double min(double a, double b)
{
	return (a<b)?a:b;
}

double solve(int bit)
{
	int i, j, bit2;
	double val = 9999999999;
	if (bit == tam)
		return 0;
	if(memo[bit] != -1)
		return memo[bit];
	for(i = 0; i < N; i++)
		if((bit & pot[i]) == 0)
		{
			bit2 = bit | pot[i];
			for(j = 0; j < N; j++)
				if((bit2 & pot[j]) == 0)
					val = min(val, mat[i][j] + solve(bit2 | pot[j]));
		}
	return memo[bit] = val;
}

main()
{
	int K, i, j;
	scanf("%d", &K);
	j = 1;
	for(i = 0; i <= 16; i++)
	{
		pot[i] = j;
		j <<= 1;
	}
	while(K--)
	{
		scanf("%d", &N);
		tam = pot[N] - 1;
		for(i = 0; i <= tam; i++)
			memo[i] = -1;
		for(i = 0; i < N; i++)
		{
			scanf("%d %d", &x[i], &y[i]);
			for(j = 0; j < i; j++)
				mat[i][j] = mat[j][i] = sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
		}
		printf("%.2lf\n", solve(0));
	}
	return 0;
}
