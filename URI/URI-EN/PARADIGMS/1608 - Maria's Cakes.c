// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Maria's Cakes
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1608

#include<stdio.h>

int ingredientes[101];

main()
{
	int N, i, D, I, B, ma, tot, K, a, b;
	scanf("%d", &N);
	while(N--)
	{
		ma = 0;
		scanf("%d %d %d", &D, &I, &B);
		for(i = 0; i < I; i++)
			scanf("%d", &ingredientes[i]);
		for(i = 0; i < B; i++)
		{
			tot = 0;
			scanf("%d", &K);
			while(K--)
			{
				scanf("%d %d", &a, &b);
				tot += b*ingredientes[a];
			}
			tot = D/tot;
			ma = ma > tot? ma : tot;
		}
		printf("%d\n", ma);
	}
	return 0;
}
