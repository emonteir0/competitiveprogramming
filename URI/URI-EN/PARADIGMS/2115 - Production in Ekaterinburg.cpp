// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Production in Ekaterinburg
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2115

#include<bits/stdc++.h>

using namespace std;

pair<int, int> vet[100001];

main()
{
	int N, t;
	while(scanf("%d", &N) == 1)
	{
		t = 1;
		for(int i = 0; i < N; i++)
			scanf("%d %d", &vet[i].first, &vet[i].second);
		sort(vet, vet+N);
		for(int i = 0; i < N; i++)
		{
			if(t >= vet[i].first)
				t += vet[i].second;
			else
				t = vet[i].first + vet[i].second;
		}
		printf("%d\n", t);
	}
	return 0;
}
