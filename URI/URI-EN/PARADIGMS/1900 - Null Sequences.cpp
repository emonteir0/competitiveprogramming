// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Null Sequences
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1900

#include<bits/stdc++.h>
#define ll long long
using namespace std;

map<ll, int> Mapa;

main()
{
	int N, x;
	ll soma = 0, s = 0;
	Mapa[0] = 1;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d", &x);
		s += x;
		soma += Mapa[s];
		Mapa[s]++;
	}
	printf("%lld\n",soma);
	return 0;
}
