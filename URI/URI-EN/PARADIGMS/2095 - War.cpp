// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: War
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2095

#include<bits/stdc++.h>

using namespace std;

int tree[1000001];
int N;

void update(int i, int val)
{
	++i;
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

set<int> conjunto;
vector<int> numeros;

int id(int X)
{
	return lower_bound(numeros.begin(), numeros.end(), X) - numeros.begin();
}

int vet[1000001], vet2[1000001];

int busca(int val, int lo, int hi)
{
	int mid, ans, y;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		y = sum(mid);
		if(y >= val)
		{
			ans = mid;
			hi = mid-1;
		}
		else
			lo = mid+1;
	}
	return ans;
}

main()
{
	int M, x, a, b, cont = 0;
	scanf("%d", &M);
	for(int i = 0; i < M; i++)
	{
		scanf("%d", &vet[i]);
		conjunto.insert(vet[i]);
	}
	for(int i = 0; i < M; i++)
	{
		scanf("%d", &vet2[i]);
		conjunto.insert(vet2[i]);
	}
	numeros.assign(conjunto.begin(), conjunto.end());
	N = numeros.size();
	for(int i = 0; i < M; i++)
		update(id(vet2[i]), 1);
	for(int i = 0; i < M; i++)
	{
		/*for(int j = 0; j < N; j++)
			printf("%d ", sum(j)-sum(j-1));
		printf("\n");*/
		x = id(vet[i]);
		a = sum(x);
		b = sum(N-1);
		if(b == a)
			update(busca(1, 0, x), -1);
		else
		{
			cont++;
			update(busca(a+1, x+1, N-1), -1);
		}
	}
	printf("%d\n", cont);
	return 0;
}
