// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Eleven
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1494

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
using namespace std;

char msg[101];

int acum[10];
int sz;

ll dp[10][11][101][101];
ll C[101][101];

ll solve(int pos, int res, int na, int nb)
{
	ll ans = 0;
	//printf("%d %d %d %d\n", pos, res, na, nb);
	if(pos == 10)
		return res == 0;
	if(dp[pos][res][na][nb] != -1)
		return dp[pos][res][na][nb];
	for(int i = 0; i <= acum[pos]; i++)
		if((na-i) >= 0 && (nb+i-acum[pos]) >= 0)
			ans += (((C[na][i] * C[nb][acum[pos]-i]) % MOD) * solve(pos+1, ((res + pos * (i + i - acum[pos]))%11 +11)%11, na-i, nb+i-acum[pos])) % MOD;
	return dp[pos][res][na][nb] = ans % MOD;
}

main()
{
	ll ans;
	C[0][0] = 1;
	for(int i = 1; i <= 100; i++)
	{
		C[i][0] = 1;
		for(int j = 1; j <= i; j++)
		{
			C[i][j] = C[i-1][j-1] + C[i-1][j];
			if(C[i][j] > MOD)
				C[i][j] -= MOD;
		}
	}
	while(scanf("%s", msg) == 1)
	{
		for(int i = 0; i < 10; i++)
			acum[i] = 0;
		for(sz = 0; msg[sz]; sz++)
			acum[msg[sz]-'0']++;
		for(int i = 0; i < 10;i++)
			for(int j = 0; j < 11; j++)
				for(int k = 0; k <= sz; k++)
					for(int l = 0; l <= sz; l++)
						dp[i][j][k][l] = -1;
		ans = solve(0, 0, sz - sz/2, sz/2);
		for(int i = 0; i < 10;i++)
			for(int j = 0; j < 11; j++)
				for(int k = 0; k <= sz; k++)
					for(int l = 0; l <= sz; l++)
						dp[i][j][k][l] = -1;
		if(acum[0])
		{
			sz--;
			acum[0]--;
			ans -= solve(0, 0, sz - sz/2, sz/2);
			if(ans < 0)
				ans += MOD;
		}
		printf("%lld\n", ans);
	}
	return 0;
}

