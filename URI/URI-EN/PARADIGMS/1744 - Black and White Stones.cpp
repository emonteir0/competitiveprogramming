// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Black and White Stones
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1744

#include<bits/stdc++.h>
#define ll long long
#define INF 1LL<<60

using namespace std;

char str[5001];

main()
{
	int N, pos, b = 0;
	ll A, B, ans;
	scanf("%lld %lld", &A, &B);
	scanf("%s", str);
	for(N = 0; str[N]; N++)
		if(str[N] == 'B')
			b++;
	if(b == 0)
		printf("0\n");
	else
	{
		ans = INF;
		for(int i = 0; i <= b; i++)
		{
			ll custo = i*A;
			int c = i, z = b-i, p, j;
			for(j = 0; (j < N) && c; j++)
			{
				if(str[j] == 'W')
					c--;
				else
					z--;
			}
			if(c)
				break;
			p = j;
			//printf("%d\n", p);
			for(int j = p; (j < N) && z; j++)
			{
				if(str[j] == 'B')
				{
					custo += (A-B)*(j-p);
					z--;
					p++;
				}
			}
			//printf("%d %lld\n", i, custo);
			ans = min(ans, custo);
		}
		printf("%lld\n", ans);
	}
	return 0;
}
