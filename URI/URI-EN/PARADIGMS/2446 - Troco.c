// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Troco
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2446

#include<stdio.h>

int valores[100001];

main()
{
	int i, n, m, x;
	valores[0] = 1;
	scanf("%d %d", &n, &m);
	while((m--)>0)
	{
		scanf("%d", &x);
		for(i = n; i >= x; i--)
			if(valores[i-x])
				valores[i] = 1;
		if(valores[n])
			break;
	}
	while((m--)>0)
		scanf("%*d");
	printf("%c\n", valores[n]? 'S': 'N');
	return 0;
}

