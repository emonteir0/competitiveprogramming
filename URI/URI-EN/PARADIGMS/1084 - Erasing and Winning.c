// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Erasing and Winning
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1084

#include<stdio.h>
#include<string.h>
#define max 100001

int main()
{
	char a[max];
	int i,N,D,j;
	while(scanf("%d %d%*c",&N,&D)==2&&N&&D)
	{	
		if(N==0&&D==0)
			break;
		scanf("%c",&a[0]);
			for(i=1;i<N;i++)
			{
				scanf("%c",&a[i]);
				while(a[i]>a[i-1]&&(D>0)&&(i>0))
				{
					i--;
					a[i]=a[i+1];
					D--;
					N--;
				}
			}
			for(i=0;i<N-D;i++)
					printf("%c",a[i]);
			printf("\n");		
	}
	return 0;
}
