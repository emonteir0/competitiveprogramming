// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Partition of The Herd
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1601

#include<bits/stdc++.h>
#define ll long long
#define INF 1001001001001001LL

using namespace std;

ll vet[1001], ivet[1001], acum[1001], acacum[1001];

ll custo(int x, int y)
{
	if(x >= y)
		return 0;
	return (ivet[y] - ivet[x]) - x*(acum[y]-acum[x]) - (acacum[y-1] - acacum[x-1]) + (y-x)*acum[x-1];
}

ll custo2(int x, int y, int z)
{
	if(y > z)
		return 0;
	return custo(y, z) + x*(acum[z]-acum[y-1])-(z-y+1)*acum[x];
}

main()
{
	int N;
	ll s, me;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 1; i <= N; i++)
			scanf("%d", &vet[i]);
		sort(vet+1, vet+N+1);
		for(int i = 1; i <= N; i++)
		{
			ivet[i] = ivet[i-1] + i*vet[i];
			acum[i] = acum[i-1] + vet[i];
			acacum[i] = acacum[i-1] + acum[i];
		}
		me = INF;
		for(int i = 1; i < N; i++)
		{
			for(int j = i+1; j <= N; j++)
			{
				s = custo(1, i) + custo(i+1, j) + custo2(i, j+1, N);
				me = min(me, s);
			}
		}
		printf("%lld\n", me);
	}
	return 0;
}
