// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Board With Prizes
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1543

#include<stdio.h>

int n, m, k;
char padroes[101][101];
int mat[101][101];
int valores[101][101];
int memo[101][101];

int max(int a, int b)
{
	return (a>b)? a : b;
}

int possivel(int a, int b)
{
	int i;
	
	for(i = 1; i <= m; i++)
	{
		if((padroes[a][i] == padroes[b][i]) && (padroes[a][i] != '.'))
			return 0;
	}
	return 1;
}

int solve(int i, int padrao)
{
	int j, ma = -999999999;
	if(i == n + 1)
		return 0;
	if(memo[i][padrao] != -1)
		return memo[i][padrao];
	for(j = 1; j <= k; j++)
	{
		//printf("(%d,%d) = %d\n",padrao,j,possivel(padrao,j));
		if(possivel(padrao,j))
		{
			//printf("v(%d, %d) = %d\n", i, j, valores[i][j]);
			ma = max(ma, solve(i+1, j) + valores[i][j]);
		}
	}
	//printf("(%d, %d) = %d\n", i, padrao, ma);
	return memo[i][padrao] = ma;
}


main()
{
	int i, j, l;
	for(i = 0; i <= 100; i++)
		padroes[0][i] = '.';
	while(scanf("%d %d%*c", &n, &m) == 2 && (n||m))
	{
		for(i = 1; i <= n; i++)
		{
			valores[i][0] = 0;
			for(j = 1; j <= m; j++)
			{
				valores[i][j] = 0;
				scanf("%d%*c", &mat[i][j]);
			}
		}
		for(j = 0; j <= m; j++)
			valores[0][j] = 0;
		scanf("%d%*c", &k);
		for(i = 1; i <= k; i++)
		{
			for(j = 1; j <= m; j++)
			{
				scanf("%c", &padroes[i][j]);
				for(l = 1; l <= n; l++)
				{
					if(padroes[i][j] == '+')
						valores[l][i] += mat[l][j];
					if(padroes[i][j] == '-')
						valores[l][i] -= mat[l][j];
				}
			}
			scanf("%*c");
		}
		for(i = 0; i <= n; i++)
			for(j = 0; j <= k; j++)
				memo[i][j] = -1;
		printf("%d\n", solve(1, 0));
	}
	return 0;
}
