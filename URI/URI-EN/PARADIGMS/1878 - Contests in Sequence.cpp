// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Contests in Sequence
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1878

#include<bits/stdc++.h>
#define ll long long

using namespace std;

set<ll> conju, conju2;

int N, M;
int pot[11][4];
ll vet[11];

void solve(int n, ll x)
{
	int i;
	if(n == N+1)
		conju.insert(x);
	else
		for(i = 1; i <= M; i++)
			solve(n+1, x + i*vet[n]);
}


int main()
{
	int i, j;
	for(i = 1; i <= 10; i++)
		pot[i][0] = 1;
	for(i = 1; i <= 10; i++)
		for(j = 1; j <= 3; j++)
			pot[i][j] = i*pot[i][j-1];
	while(scanf("%d %d", &N, &M) == 2)
	{
		conju = conju2;
		for(i = 1; i <= N; i++)
			scanf("%lld", &vet[i]);
		solve(1, 0);
		//printf("%d %d\n", conju.size(), pot[M][N]);
		printf("%s\n", conju.size() == pot[M][N] ? "Lucky Denis!" : "Try again later, Denis...");
	}
	return 0;
}
