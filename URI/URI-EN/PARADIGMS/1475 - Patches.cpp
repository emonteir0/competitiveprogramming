// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Patches
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1475

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N, C, T1, T2;
int dp[2001];
int vet[2001];

int solve(int x, int lim)
{
	int x1, x2;
	if(x >= lim)
		return 0;
	if(dp[x] != -1)
		return dp[x];
	x1 = upper_bound(vet+x, vet+lim, vet[x]+T1)-vet;
	x2 = upper_bound(vet+x, vet+lim, vet[x]+T2)-vet;
	return dp[x] = min(solve(x1, lim)+T1, solve(x2, lim)+T2);
}

main()
{
	int me;
	while(scanf("%d %d %d %d", &N, &C, &T1, &T2) == 4)
	{
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		for(int i = 0; i < N; i++)
			vet[N+i] = C + vet[i];
		me = INF;
		for(int i = 0; i < N; i++)
		{
			for(int j = i; j <= N+i; j++)
				dp[j] = -1;
			me = min(me, solve(i, N+i));
		}
		printf("%d\n", me);
	}
	return 0;
}
