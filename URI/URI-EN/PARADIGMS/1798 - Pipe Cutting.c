// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pipe Cutting
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1798

#include <stdio.h>
 
/* Problem
 * Data     */
int N,CAP;
int size[2001];
int profit[2001];

int knapsack();
 
int main()
{
   int i;
   scanf("%d %d",&N,&CAP);
   for(i=0;i<N;i++)
    scanf("%d %d",&size[i],&profit[i]);
   printf("%d\n", knapsack(CAP));
   return 0;
}
 
int knapsack()
{
   int max = 0;
   int maxCap = CAP+1;
   int maxProfitCap[CAP+1];
   maxProfitCap[0] = 0; 
   int capacity, i;
   for ( capacity=1 ; (capacity<=CAP) ; ++capacity )
   {
      maxProfitCap[capacity] = 0;
      for ( i=0 ; (i<N) ; ++i )
      {
         if ( size[i] > capacity )
            continue;
         int previousSize = ( capacity - size[i] >= 0 ) ? capacity - size[i] : 0;
         int profitWithItem = maxProfitCap[ previousSize ] + profit[i];
         if ( profitWithItem > maxProfitCap[capacity] )
            maxProfitCap[capacity] = profitWithItem;
      }
   } 
   return maxProfitCap[CAP];
}
