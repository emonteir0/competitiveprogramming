// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elegant Permuted Sum
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1055

#include<bits/stdc++.h>

using namespace std;

int vet[50];

main()
{
	int i, k, N, T, m, e, d, s1, s2, a1, a2;
	scanf("%d", &T);
	for(k = 1; k <= T; k++)
	{
		s1 = s2 = 0;
		scanf("%d", &N);
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		e = 1;
		d = N-1;
		a1 = vet[0];
		a2 = vet[0];
		m = 1;
		while(1)
		{
			if(e > d)
				break;
			else if(e+1 <= d)
			{
				if(m)
				{
					if(a1 < a2)
					{
						s1 += abs(vet[d]-a1) + abs(vet[d-1]-a2);
						a1 = vet[d];
						a2 = vet[d-1];
					}
					else
					{
						s1 += abs(vet[d]-a2) + abs(vet[d-1]-a1);
						a1 = vet[d-1];
						a2 = vet[d];
					}
					d -= 2;
				}
				else
				{
					if(a1 > a2)
					{
						s1 += abs(vet[e]-a1) + abs(vet[e+1]-a2);
						a1 = vet[e];
						a2 = vet[e+1];
					}
					else
					{
						s1 += abs(vet[e]-a2) + abs(vet[e+1]-a1);
						a1 = vet[e+1];
						a2 = vet[e];
					}
					e += 2;
				}
				m = !m;
			}
			else
			{
				s1 += max(abs(a1-vet[e]), abs(a2-vet[e]));
				break;
			}
		}
		e = 0;
		d = N-2;
		a1 = vet[N-1];
		a2 = vet[N-1];
		m = 0;
		while(1)
		{
			if(e > d)
				break;
			else if(e+1 <= d)
			{
				if(m)
				{
					if(a1 < a2)
					{
						s2 += abs(vet[d]-a1) + abs(vet[d-1]-a2);
						a1 = vet[d];
						a2 = vet[d-1];
					}
					else
					{
						s2 += abs(vet[d]-a2) + abs(vet[d-1]-a1);
						a1 = vet[d-1];
						a2 = vet[d];
					}
					d -= 2;
				}
				else
				{
					if(a1 > a2)
					{
						s2 += abs(vet[e]-a1) + abs(vet[e+1]-a2);
						a1 = vet[e];
						a2 = vet[e+1];
					}
					else
					{
						s2 += abs(vet[e]-a2) + abs(vet[e+1]-a1);
						a1 = vet[e+1];
						a2 = vet[e];
					}
					e += 2;
				}
				m = !m;
			}
			else
			{
				s2 += max(abs(a1-vet[e]), abs(a2-vet[e]));
				break;
			}
		}
		printf("Case %d: %d\n", k, max(s1, s2));
	}
	return 0;
}
