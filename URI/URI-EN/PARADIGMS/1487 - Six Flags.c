// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Six Flags
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1487

#include<stdio.h>

int K[601];
int V[601], X[601];

main()
{
	int N,M,i,j,k=1;
	while(scanf("%d %d",&N,&M)==2 && (N||M))
	{
		K[0] = 0;
		for(i=1;i<=N;i++)
			scanf("%d %d",&V[i],&X[i]);
		for(i=1;i<=M;i++)
		{
			K[i] = 0;
			for(j=1;j<=N;j++)
			{
				if(V[j] <= i)
				{
					int val = K[i-V[j]] + X[j];
					K[i] = (K[i]<val)?val:K[i];
				}
			}
		}
		printf("Instancia %d\n%d\n\n",k++,K[M]);
	}
	return 0;
}

