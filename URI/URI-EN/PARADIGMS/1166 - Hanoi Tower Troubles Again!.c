// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hanoi Tower Troubles Again!
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1166

#include<stdio.h>

int vet[50],valores[51],quad[3601];

void quadrados()
{
	int i;
	for(i=0;i<=60;i++)
		quad[i*i]=1;
}

int hanoi(int x)
{
	int i,j=1;
	for(i=0;i<x;i++)
		vet[i]=0;
	while(1)
	{
		for(i=0;i<x;i++)
		{
			if(vet[i]!=0)
			{
				if(quad[vet[i]+j])
				{
					vet[i]=j;
					j++;
					break;
				}
			}
			else
			{
				vet[i]=j;
				j++;
				break;
			}
		}
		if(i==x)
			return j-1;
	}
}

main()
{
	int i,n;
	quadrados();
	for(i=1;i<=50;i++)
		valores[i]=hanoi(i);
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&i);
		printf("%d\n",valores[i]);
	}
	return 0;
}
