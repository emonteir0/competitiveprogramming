// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even or Odd
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2677

#include<stdio.h>

int vet[1001];
int memo[1001][1001];

int max(int x, int y)
{
	return (x > y) ? x : y;
}

int min(int x, int y)
{
	return (x < y) ? x : y;
}

int solve(int x, int y)
{
	if(x >= y)
		return 0;
	if(memo[x][y] != -1)
		return memo[x][y];
	return memo[x][y] = max(min(solve(x+1, y-1), solve(x+2, y)) + vet[x], min(solve(x+1, y-1), solve(x, y-2)) + vet[y]);
}

main()
{
	int i, j, x, N;
	while((scanf("%d", &N) == 1) && N)
	{
		for(i = 0; i <= 2*N; i++)
			for(j = 0; j <= 2*N; j++)
				memo[i][j] = -1;
		for(i = 0; i < 2*N; i++)
		{
			scanf("%d", &x);
			vet[i] = (x & 1) == 0;
		}
		printf("%d\n", solve(0, 2*N-1));
	}
	return 0;
}
