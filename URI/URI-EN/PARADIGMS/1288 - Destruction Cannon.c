// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Destruction Cannon
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1288

#include<stdio.h>


int best[51][101];
int valor[51];
int tam[51];

main()
{
	int n, n2, i, j, limite, x;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&n2);
		for(i=1;i<=n2;i++)
			scanf("%d %d",&valor[i],&tam[i]);
		scanf("%d",&limite);
		for(i=1;i<=n2;i++)
		{
			for(j=0;j<=limite;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]]+valor[i]>best[i-1][j])
					{
						best[i][j]=best[i-1][j-tam[i]]+valor[i];
					}
					else
					{
						best[i][j]=best[i-1][j];
					}
				}
				else
				{
					best[i][j]=best[i-1][j];
				}
			}
		}
		scanf("%d",&x);
		printf("%s\n",(best[n2][limite]>=x)?"Missao completada com sucesso":"Falha na missao");
	}
	return 0;
}
