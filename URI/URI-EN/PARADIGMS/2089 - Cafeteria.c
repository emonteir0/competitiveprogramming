// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cafeteria
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2089

#include<stdio.h>

int valores[100001];

main()
{
	int i, j, n, v, x;
	valores[0] = 1;
	while(scanf("%d %d", &v, &n) == 2 && (v||n))
	{
		for(i = 1; i<= v; i++)
			valores[i] = 0;
		while(n--)
		{
			scanf("%d", &x);
			for(i=v;i>=x;i--)
				valores[i] |= valores[i-x];
		}
		printf("%s\n", valores[v]?"sim": "nao");
	}
	return 0;
}
