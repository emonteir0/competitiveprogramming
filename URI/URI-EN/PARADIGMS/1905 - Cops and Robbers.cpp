// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cops and Robbers
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1905

#include<bits/stdc++.h>

using namespace std;

int vis[5][5], mat[5][5];
int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x < 5 && y >= 0 && y < 5;
}

void dfs(int x, int y, int t)
{
	int x2, y2;
	vis[x][y] = t;
	for(int k = 0; k < 4; k++)
	{
		x2 = x + dx[k];
		y2 = y + dy[k];
		if(inRange(x2, y2) && !mat[x2][y2] && vis[x2][y2] < t)
			dfs(x2, y2, t);
	}
}

main()
{
	int T;
	scanf("%d", &T);
	for(int t = 1; t <= T; t++)
	{
		for(int i = 0; i < 5; i++)
			for(int j = 0; j < 5; j++)
				scanf("%d", &mat[i][j]);
		dfs(0, 0, t);
		printf("%s\n", vis[4][4] == t ? "COPS": "ROBBERS");
			
	}
	return 0;
}
