// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Red Lights
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2480

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int id, N, M;
int matv[22][100200];
char matc[22][100200];
int ptr[22], aux[22], aux2[22], tree[22];
pair<int, int> ptr2[22];

int val(char c)
{
	if(c == 'S')
		return 0;
	if(c == 'G')
		return 1;
	if(c == 'W')
		return 2;
	return 3;
}

bool cmp(pair<int, int> a, pair<int, int> b)
{
	if(matc[a.first][id] != matc[b.first][id])
		return val(matc[a.first][id]) < val(matc[b.first][id]);
	if(matv[a.first][id] != matv[b.first][id])
		return matv[a.first][id] < matv[b.first][id];
	return a.second < b.second;
}

void update(int i, int val)
{
	++i;
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

char comp(int a, int b)
{
	if(matv[a][id] == matv[b][id] && matc[a][id] == matc[b][id])
		return 0;
	return 1;
}

int countinv()
{
	int i, k = 0, ans = 0;
	for(i = 0; i < N; i++)
	{
		if(i && comp(ptr2[i].first, ptr2[i-1].first))
			k++;
		update(k, 1);
		aux[ptr2[i].first] = k;
	}
	for(i = 0; i < N; i++)
		aux2[i] = aux[ptr[i]];
	for(; i--; )
	{
		ans += sum(N-1)-sum(aux2[i]);
		update(aux2[i], -1);
	}
	return ans;
}

main()
{
	int i, j, t = 0;
	ll s, s2;
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		s = s2 = 0;
		if(t)
			printf("\n");
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
			{
				scanf("[%d]%c%*c", &matv[i][j], &matc[i][j]);
				if(matc[i][j] == 'R')
					s2 += matv[i][j];
			}
		for(i = 0; i < N; i++)
			ptr[i] = i;
		for(id = 0; id < M; id++)
		{
			for(i = 0; i < N; i++)
				ptr2[i] = make_pair(ptr[i], i);
			sort(ptr2, ptr2+N, cmp);
			s += countinv();
			for(int i = 0; i < N; i++)
				ptr[i] = ptr2[i].first;
		}
		printf("Instance %02d:\n%lld %lld\n", ++t, s, s2);
	}
	return 0;
}
