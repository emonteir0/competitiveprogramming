// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Allowed Factors
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2501

#include<stdio.h>
#define ll long long
#define MAX 9223372036854775807

ll min(ll x, ll y)
{
	return (x < y) ? x : y;
}

main()
{
	int i, j, N, K, primes[100], ind[100];
	ll value[100], h[100001];
	h[0] = 1;
	while(scanf("%d %d", &N, &K) == 2 && (N||K))
	{
		for(i = 0; i < N; i++)
		{
			scanf("%d", &primes[i]);
			value[i] = primes[i];
			ind[i] = 0;
		}
		for(i = 1; i <= K; i++)
		{
			h[i] = MAX;
			for(j = 0; j < N; j++)
				h[i] = min(h[i], value[j]);
			for(j = 0; j < N; j++)
				if(h[i] == value[j])
					value[j] = h[++ind[j]] * primes[j];
		}
		printf("%lld\n", h[K]);
	}
	return 0;
}
