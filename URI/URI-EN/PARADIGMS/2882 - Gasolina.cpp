// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Gasolina
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2882

#include <bits/stdc++.h>

using namespace std;

struct edge { int v, rev, cap, f; };

const int MAXN = 3000, INF = 0x3f3f3f3f;
vector<edge> adj[MAXN];
int dist[MAXN], ptr[MAXN];
int vA[MAXN], vR[MAXN];

void add_edge(int u, int v, int cap)
{
	adj[u].push_back((edge){v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){u, (int)adj[u].size() - 1, 0, 0});
}

bool dinic_bfs(int nodes, int source, int sink) 
{
	fill(dist, dist + nodes, -1);
	dist[source] = 0;
	queue<int> q;
	q.push(source);
	while (!q.empty()) 
	{
		int u = q.front();
		q.pop();
		for (int j = 0; j < (int)adj[u].size(); j++) 
		{
			edge &e = adj[u][j];
			if (dist[e.v] < 0 && e.f < e.cap) 
			{
				dist[e.v] = dist[u] + 1;
				q.push(e.v);
			}
		}
	}
	return dist[sink] >= 0;
}

int dinic_dfs(int u, int f, int sink) 
{
	if (u == sink)
		return f;
	for (; ptr[u] < (int)adj[u].size(); ptr[u]++) 
	{
		edge &e = adj[u][ptr[u]];
		if (dist[e.v] == dist[u] + 1 && e.f < e.cap) 
		{
			int flow = dinic_dfs(e.v, min(f, e.cap - e.f), sink);
			if (flow > 0) 
			{
				e.f += flow;
				adj[e.v][e.rev].f -= flow;
				return flow;
			}
		}
	}
	return 0;
}

int dinic(int nodes, int source, int sink) 
{
	int flow, max_flow = 0;
	while (dinic_bfs(nodes, source, sink)) 
	{
		fill(ptr, ptr + nodes, 0);
		while ((flow = dinic_dfs(source, INF, sink)) != 0) 
			max_flow += flow;
	}
	return max_flow;
}

int P, R, C, sumA, sumR;
int nodes, source, target;
vector< pair< int, pair<int, int> > > arestas;

int ok(int x)
{
	//printf("%d!!!\n", x);
	for(int i = 0; i < nodes; i++)
		adj[i].clear();
	for(int i = 1; i <= P; i++)
		add_edge(source, i, vA[i]);
	for(int i = 1; i <= R; i++)
		add_edge(i+P, target, vR[i]);
	for(int i = 0; i < arestas.size(); i++)
	{
		if(arestas[i].first > x)
			break;
		//printf("%d %d\n", arestas[i].second.first, arestas[i].second.second);
		add_edge(arestas[i].second.first, arestas[i].second.second, INF);
	}
	//printf("\n");
	int fluxo = dinic(nodes, source, target);
	return sumA == fluxo;
}

int solve()
{
	int lo = 1, hi = 1000000, mid, best = -1;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		if(ok(mid))
		{
			best = mid;
			hi = mid-1;
		}
		else
		{
			lo = mid+1;
		}
	}
	return best;
}

int main()
{
	int u, v, w;
	scanf("%d %d %d", &P, &R, &C);
	nodes = P+R+2;
	source = 0;
	target = P+R+1;
	for(int i = 1; i <= P; i++)
	{
		scanf("%d", &vA[i]);
		sumA += vA[i];
	}
	for(int i = 1; i <= R; i++)
	{
		scanf("%d", &vR[i]);
		sumR += vR[i];
	}
	while(C--)
	{
		scanf("%d %d %d", &u, &v, &w);
		arestas.push_back({w, {u, v+P}});
	}
	sort(arestas.begin(), arestas.end());
	printf("%d\n", solve());
	return 0;
}
