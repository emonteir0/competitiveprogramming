// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Disjoint Water Supply
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1493

#include<bits/stdc++.h>

using namespace std;

vector<int> G[100001];
int dp[1001][1001];

int solve(int x, int y)
{
	if(x == 1)
		return 1;
	if(x == y)
		return 0;
	if(dp[x][y] != -1)
		return dp[x][y];
	dp[x][y] = 0;
	for(int i = G[y].size(); i--;)
		if(solve(min(x, G[y][i]), max(x, G[y][i])))
		{
			dp[x][y] = 1;
			break;
		}
	return dp[x][y];
}

main()
{
	int N, M, u, v, sum;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 1; i <= N; i++)
			G[i].clear();
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[v].push_back(u);
		}
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
				dp[i][j] = -1;
		sum = N-1;
		for(int i = 2; i <= N; i++)
			for(int j = i+1; j <= N; j++)
				sum += solve(i, j);
		printf("%d\n", sum);
	}
	return 0;
}
