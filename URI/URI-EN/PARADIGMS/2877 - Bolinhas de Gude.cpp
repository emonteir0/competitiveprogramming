// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bolinhas de Gude
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2877

#include<bits/stdc++.h>

using namespace std;

int grundy[101][101];
int aux[301];

int mex(int x, int y)
{
	int z = min(x, y);
	for(int i = 0; i <= 300; i++)
		aux[i] = 0;
	for(int i = 1; i < x; i++)
		if(x-i != y)
		aux[grundy[x-i][y]] = 1;
	for(int i = 1; i < y; i++)
		if(x != y-i)
		aux[grundy[x][y-i]] = 1;
	for(int i = 1; i < z; i++)
		if(x != y)
		aux[grundy[x-i][y-i]] = 1;
	for(int i = 0; i <= 300; i++)
		if(!aux[i])
			return i;
	return 300;
}

main()
{
	int N, x, y, ans = 0;
	for(int i = 0; i <= 100; i++)
		for(int j = 0; j <= 100; j++)
		grundy[i][j] = mex(i, j);
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d", &x, &y);
		if(x == y)
			return !printf("Y\n");
		ans ^= grundy[x][y];
	}
	printf("%s\n", ans ? "Y" : "N");
	return 0;
}
