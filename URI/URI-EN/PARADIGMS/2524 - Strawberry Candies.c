// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Strawberry Candies
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2524

#include<stdio.h>

int vet[1000], vet2[1000];

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int N, M, i, j, x;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(i = 0; i < N; i++)
			vet[i] = 0;
		vet[0] = 1;
		while(M--)
		{
			scanf("%d", &x);
			for(i = 0; i < N; i++)
				vet2[i] = vet[i];
			for(i = 0; i < N; i++)
				if(vet[i] != 0)
				{
					j = (i+x)%N;
					vet2[j] = max(vet2[j], vet[i]+1);
				}
			for(i = 0; i < N; i++)
				vet[i] = vet2[i];
		}
		printf("%d\n", vet[0]-1);
	}
	return 0;
}
