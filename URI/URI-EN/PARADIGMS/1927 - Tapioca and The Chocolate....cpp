// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tapioca and The Chocolate...
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1927

#include<bits/stdc++.h>

using namespace std;

int T;
int dp[21][21][1001];
vector<int> trufas[21][21];
int dx[] = {0, 1, -1, 0, 0};
int dy[] = {0, 0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x <= 20 && y >= 0 && y <= 20;
}

int solve(int x, int y, int t)
{
	int r = 0, s = 0, x2, y2;
	if(!inRange(x, y))
		return 0;
	if(t == T+1)
		return 0;
	if(dp[x][y][t] != -1)
		return dp[x][y][t];
	if(binary_search(trufas[x][y].begin(), trufas[x][y].end(), t))
	{
		r += upper_bound(trufas[x][y].begin(), trufas[x][y].end(), t)
		- lower_bound(trufas[x][y].begin(), trufas[x][y].end(), t);
	}
	for(int k = 0; k < 5; k++)
	{
		if(inRange(x+dx[k], y+dy[k]))
			s = max(s, solve(x+dx[k], y+dy[k], t+1));
	}
	return dp[x][y][t] = r+s;
}

main()
{
	int N, x, y, z;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d", &x, &y, &z);
		trufas[x][y].push_back(z);
		T = max(T, z);
	}
	for(int i = 0; i <= 20; i++)
		for(int j = 0; j <= 20; j++)
		{
			sort(trufas[i][j].begin(), trufas[i][j].end());
			for(int k = 0; k <= T; k++)
				dp[i][j][k] = -1;
		}
	printf("%d\n", solve(6, 6, 0));
	return 0;
}
