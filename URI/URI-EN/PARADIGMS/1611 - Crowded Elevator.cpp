// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Crowded Elevator
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1611

#include<bits/stdc++.h>
using namespace std;

int vet[500001];

main()
{
	int T, M, C, N, ans;
	scanf("%d", &T);
	while(T--)
	{
		ans = 0;
		scanf("%*d %d %d", &C, &M);
		for(int i = 0; i < M; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+M);
		N = M-1;
		while(N >= 0)
		{
			ans += 2*vet[N];
			N -= C;
		}
		printf("%d\n", ans);
	}
	return 0;
}
