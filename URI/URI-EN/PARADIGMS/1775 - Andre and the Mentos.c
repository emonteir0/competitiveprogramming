// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Andre and the Mentos
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1775

#include<stdio.h>

int memo[1001][1001];
int vet[1001];

int min(int a, int b)
{
	return (a<b)? a : b;
}

int solve(int i, int j)
{
	if(i>j)
		return 0;
	if(memo[i][j] != -1)
		return memo[i][j];
	if(vet[i]==vet[j])
		return memo[i][j] = 1 + solve(i+1,j-1);
	return memo[i][j] = min(solve(i+1,j), solve(i, j-1)) + 1;
}

main()
{
	int i, j, k, K, N;
	scanf("%d", &K);
	for(k = 1; k <= K; k++)
	{
		scanf("%d", &N);
		for(i = 1; i <= N; i++)
			scanf("%d", &vet[i]);
		for(i = 1; i<=N;i++)
		{
			for(j = 1; j<=N; j++)
				memo[i][j] = -1;
			memo[i][i] = 1;
		}
		printf("Caso #%d: %d\n",k,solve(1,N));
	}
	return 0;
}
