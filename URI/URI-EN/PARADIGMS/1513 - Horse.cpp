// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Horse
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1513

#include<bits/stdc++.h>
#define INF (int)1e9

using namespace std;

int memo[16][65536];
int dist[16][16];


int tsp(int u, int mask, int n) 
{
    if (mask == (1<<n)-1) 
		return dist[u][0];
    int &r = memo[u][mask];
	if (r != -1) 
		return r;
    r = INF;
    for (int v = 0; v < n; v++)
        if (!(mask&(1<<v)))
            r = min(r, tsp(v, mask|(1<<v), n)+dist[u][v]);
    return r;
}

int N, M;
int x[16], y[16];
char mat[101][101];
int distancia[101][101];

int inRange(int X, int Y)
{
	return (X >= 0) && (X < N) && (Y >= 0) && (Y < M);
}

void dijkstra(int u)
{
	int i, j, k, w, X, Y, X2, Y2;
	priority_queue< pair<int, pair<int, int> > > pq;
	int dx[] = {1, 1, -1, -1, 2, 2, -2, -2};
	int dy[] = {2, -2, 2, -2, 1, -1, 1, -1};
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			distancia[i][j] = INF;
	distancia[x[u]][y[u]] = 0;
	pq.push(make_pair(0, make_pair(x[u], y[u])));
	while(!pq.empty())
	{
		w = -(pq.top()).first;
		X = ((pq.top()).second).first;
		Y = ((pq.top()).second).second;
		pq.pop();
		if(w > distancia[X][Y])
			continue;
		for(k = 0; k < 8; k++)
		{
			X2 = X + dx[k];
			Y2 = Y + dy[k];
			if(inRange(X2, Y2))
			{
				if(mat[X2][Y2] != '#' && distancia[X2][Y2] > distancia[X][Y] + 1)
				{
					distancia[X2][Y2] = distancia[X][Y] + 1;
					pq.push(make_pair(-distancia[X2][Y2], make_pair(X2, Y2)));
				}
			}
		}
	}
}

main()
{
	int i, j, K, LIM;
	while(scanf("%d %d %d%*c", &N, &M, &K) == 3 && (N||M||K))
	{
		LIM = K;
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
			{
				scanf("%c", &mat[i][j]);
				if(mat[i][j] == 'P')
				{
					x[K] = i;
					y[K--] = j;
					mat[i][j] = '.';
				}
				else if(mat[i][j] == 'C')
				{
					x[0] = i;
					y[0] = j;
					mat[i][j] = '.';
				}
			}
			scanf("%*c");
		}
		for(i = 0; i <= LIM; i++)
		{
			dijkstra(i);
			for(j = 0; j <= LIM; j++)
				dist[i][j] = distancia[x[j]][y[j]];
			for(j = 0; j < 1<<(LIM+1); j++)
				memo[i][j] = -1;
		}
		printf("%d\n", tsp(0, 0, LIM+1));
	}
	return 0;
}
