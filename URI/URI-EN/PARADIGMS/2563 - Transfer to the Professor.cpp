// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Transfer to the Professor
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2563

#include <bits/stdc++.h>
using namespace std;

#define vi vector <int>
#define vvi vector <vi>

int main() {
	ios_base::sync_with_stdio(0);
	int n, m;
	while (cin >> n >> m) {
		vi v(n+1), w(n+1);
		vvi best(n+1, vi(m+1));
		for (int i = 1; i <= n; i++) {
			cin >> v[i];
		}
		for (int i = 1; i <= n; i++) {
			cin >> w[i];
		}
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				best[i][j] = best[i-1][j];
				if (j >= w[i]) {
					best[i][j] = max(best[i][j], best[i-1][j-w[i]]+v[i]);
				}
			}
		}
		cout << best[n][m] << endl;
	}
	
	return 0;
}
