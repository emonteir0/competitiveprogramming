// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: First Contact
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1970

#include<bits/stdc++.h>

using namespace std;

int N, K;
int vet[100];
int cap[3];
int vis[101][51][51][51];
int dp[101][51][51][51];


int solve(int x, int c1, int c2, int c3)
{
	if(x == N)
		return 0;
	if(vis[x][c1][c2][c3])
		return dp[x][c1][c2][c3];
	vis[x][c1][c2][c3] = 1;
	dp[x][c1][c2][c3] = solve(x+1, c1, c2, c3);
	if(c1 >= vet[x])
		dp[x][c1][c2][c3] = max(dp[x][c1][c2][c3], solve(x+1, c1-vet[x], c2, c3)+vet[x]);
	if(c2 >= vet[x])
		dp[x][c1][c2][c3] = max(dp[x][c1][c2][c3], solve(x+1, c1, c2-vet[x], c3)+vet[x]);
	if(c3 >= vet[x])
		dp[x][c1][c2][c3] = max(dp[x][c1][c2][c3], solve(x+1, c1, c2, c3-vet[x])+vet[x]);
	return dp[x][c1][c2][c3];
}

main()
{
	scanf("%d %d", &N, &K);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(int i = 0; i < K; i++)
		scanf("%d", &cap[i]);
	if(K == 1)
		printf("%d\n", solve(0, cap[0], 0, 0));
	if(K == 2)
		printf("%d\n", solve(0, cap[0], cap[1], 0));
	if(K == 3)
		printf("%d\n", solve(0, cap[0], cap[1], cap[2]));
	return 0;
}
