// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Diego and Hammer Game
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1922

#include<bits/stdc++.h>

using namespace std;

int x[1000], y[1000];
set<int> sets[1001], resposta;
set<int>::iterator it;

bool cmp(set<int> a, set<int> b)
{
	return a.size() > b.size();
}

main()
{
	int N, M, sum = 0;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d", &x[i], &y[i]);
		for(int j = x[i]; j <= y[i]; j += x[i])
			sets[j].insert(i);
	}
	while(M--)
	{
		sort(sets, sets+1000, cmp);
		resposta = sets[0];
		sum += resposta.size();
		for(int i = 0; i <= 1000; i++)
		{
			for(it = resposta.begin(); it != resposta.end(); it++)
				sets[i].erase(*it);
		}
	}
	printf("%d\n", sum);
	return 0;
}
