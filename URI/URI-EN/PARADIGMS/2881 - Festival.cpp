// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Festival
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2881

#include<bits/stdc++.h>
#define INFN -1001001001

using namespace std;

struct show
{
	int st, en, val, pot;
	show()
	{
	}
	show(int x)
	{
		st = x;
	}
	show(int s, int e, int v, int p)
	{
		st = s;
		en = e;
		val = v;
		pot = p;
	}
	bool operator<(const show& sh) const
	{
		return st < sh.st;
	}
};

vector<show> shows;
int dp[1000][1024];
int vis[1000][1024];
int lim;

int solve(int x, int mask)
{
	if(x == int(shows.size()))
		return mask == lim ? 0 : INFN;
	if(vis[x][mask])
		return dp[x][mask];
	vis[x][mask] = 1;
	int r = solve(x+1, mask);
	int y = lower_bound(shows.begin(), shows.end(), show(shows[x].en)) - shows.begin();
	r = max(r, solve(y, mask | shows[x].pot) + shows[x].val);
	return dp[x][mask] = r;
}

int main()
{
	int N, M, u, v, w;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			shows.push_back(show(u, v, w, 1<<i));
		}
	}
	lim = (1 << N)-1;
	sort(shows.begin(), shows.end());
	u = solve(0, 0);
	if(u < 0)
		printf("-1\n");
	else
		printf("%d\n", u);
	return 0;
}
