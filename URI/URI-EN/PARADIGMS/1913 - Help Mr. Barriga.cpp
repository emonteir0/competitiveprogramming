// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Mr. Barriga
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1913

#include<bits/stdc++.h>
#define MAX 1000

using namespace std;

struct tipo
{
	int h, r, cor;
};

int N;
int memo[MAX+1][MAX+1];
tipo vet[MAX+1];

bool cmp(tipo a, tipo b)
{
	return a.r > b.r;
}

int func(int a, int b)
{
	if(b == 4)
		return 1;
	else
		return (a+1)%4 != b;
}

int solve(int a, int b)
{
	int ra, ca, h, rb, cb;
	if(a == N+1)
		return 0;
	if(memo[a][b] != -1)
		return memo[a][b];
	ra = vet[a].r, h = vet[a].h, ca = vet[a].cor;
	rb = vet[b].r, cb = vet[b].cor;
	if(rb > ra && func(ca, cb))
		return memo[a][b] = max(solve(a+1, b), solve(a+1, a) + h);
	else
		return memo[a][b] = solve(a+1, b);
}

main()
{
	int i, j;
	char mensagem[11];
	vet[0].r = 1001, vet[0].h = 0, vet[0].cor = 4;
	while(scanf("%d%*c", &N) == 1 && N)
	{
		for(j = 0; j <= N; j++)
			memo[0][j] = -1;
		for(i = 1; i <= N; i++)
		{
			for(j = 0; j <= N; j++)
				memo[i][j] = -1;
			scanf("%d %d %s%*c", &vet[i].h, &vet[i].r, mensagem);
			if(mensagem[3] == 'M')//VERMELHO
				vet[i].cor = 0;
			else if(mensagem[3] == 'A')//LARANJA
				vet[i].cor = 1;
			else if(mensagem[3] == 'L')//AZUL
			 	vet[i].cor = 2;
			else//VERDE
				vet[i].cor = 3;
		}
		sort(vet, vet+N+1, cmp);
		printf("%d centimetro(s)\n", solve(1, 0));
	}
	return 0;
}
