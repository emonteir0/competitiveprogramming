// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel's Matrix
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2778

#include<bits/stdc++.h>
#define INF 1001001001
#define INFN -1001001001

using namespace std;

int N, M, X, Y;
int dx[] = {1, 0, 0};
int dy[] = {0, 1, -1};
int mat[101][101];
int pd[101][101][21][21][3];

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

int solve(int x, int y, int k , int l, int w)
{
	int x2, y2, k2, l2, r = INF;
	if(x == N-1 && y == M-1)
		return mat[x][y];
	if(pd[x][y][k][l][w] != INFN)
		return pd[x][y][k][l][w];
	k2 = k + (mat[x][y] == 0);
	l2 = l + (mat[x][y] < 0);
	if(k2 > X || l2 > Y)
		return pd[x][y][k][l][w] = INF;
	for(int i = 0; i < 3; i++)
	{
		if(i+w == 3)
			continue;
		x2 = x + dx[i];
		y2 = y + dy[i];
		if(inRange(x2, y2))
			r = min(r, solve(x2, y2, k2, l2, i));
	}
	return pd[x][y][k][l][w] = r+mat[x][y];
}

main()
{
	int a;
	while(scanf("%d %d %d %d", &N, &M, &X, &Y) == 4)
	{
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				scanf("%d", &mat[i][j]);
		for(int i = 0; i <= N; i++)
			for(int j = 0; j <= M; j++)
				for(int k = 0; k <= 20; k++)
					for(int l = 0; l <= 20; l++)
						for(int w = 0; w < 3; w++)
							pd[i][j][k][l][w] = INFN;
		a = solve(0, 0, 0, 0, 0);
		if(a >= 1100000)
			printf("Impossivel\n");
		else
			printf("%d\n", a);
	}
	return 0;
}
