// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Philosopher's Stone
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1838

#include<bits/stdc++.h>

using namespace std;

int n, tf;

typedef struct
{
	int t0, t1;
} uso;

uso vet[1001];
int memo[3601][1001];

bool cmp(uso a, uso b)
{
	return a.t0<b.t0;
}

int tempo(int t0, int i)
{
	if(t0 == tf || i == n)
		return 0;
	if(memo[t0][i] != -1)
		return memo[t0][i];
	if(t0 <= vet[i].t0)
		return memo[t0][i] = max(tempo(vet[i].t1, i+1)+vet[i].t1-vet[i].t0, tempo(vet[i+1].t0, i+1));
	else
		return memo[t0][i] = tempo(t0, i+1);
		
}

main()
{
	int i, j;
	scanf("%d", &n);
	for(i = 0; i < n; i++)
		scanf("%d %d", &vet[i].t0, &vet[i].t1);
	sort(vet, vet+n, cmp);
	for(i=vet[0].t0;i<=3600;i++)
		for(j=0;j<=1000;j++)
			memo[i][j] = -1;
	tf = vet[n-1].t1;
	printf("%d\n", tempo(vet[0].t0,0));
	return 0;
}
