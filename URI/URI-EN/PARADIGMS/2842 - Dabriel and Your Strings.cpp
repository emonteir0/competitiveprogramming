// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel and Your Strings
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2842

#include<bits/stdc++.h>

using namespace std;

char str1[1001], str2[1001];
int dp[1001][1001];

int lcs()
{
	int N = strlen(str1), M = strlen(str2);
	for(int i = 0; i <= N; i++)
	{
		for(int j = 0; j <= M; j++)
		{
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(str1[i-1] == str2[j-1])
				dp[i][j] = dp[i-1][j-1]+1;
			else
				dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
		}
	}
	return N+M-dp[N][M];
}


main()
{
	scanf("%s %s", str1, str2);
	printf("%d\n", lcs());
	return 0;
}
