// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Isosceles
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2243

#include<stdio.h>

int min(int a, int b)
{
	return (a<b)?a:b;
}

int max(int a, int b)
{
	return (a>b)?a:b;
}

int esq[50001], dir[50002], vet[50001];

main()
{
	int i, N, ma = 0;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
		scanf("%d", &vet[i]);
	esq[1] = 1;
	dir[N] = 1;
	for(i = 2; i <= N; i++)
		esq[i] = min(esq[i-1]+1, vet[i]);
	for(i = N; i--;)
		dir[i] = min(dir[i+1]+1, vet[i]);
	for(i = 1; i <= N; i++)
		ma = max(ma, min(esq[i], dir[i]));
	printf("%d\n", ma);
	return 0;
}
