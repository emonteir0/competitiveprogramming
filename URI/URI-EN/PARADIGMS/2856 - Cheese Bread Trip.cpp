// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cheese Bread Trip
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2856

#include<bits/stdc++.h>
#define fi first
#define se second

using namespace std;

int pd[2][1001];
int N, M;
vector< pair<int, int> > vG, vB, vC;

int knapsack(vector<pair<int, int> > &v1, vector<pair<int, int> > &v2)
{
	int ant = 0, at = 1, ma = 0;
	for(int i = 0; i <= 1000; i++)
		pd[0][i] = pd[1][i] = 0;
	for(auto r:v1)
	{
		at = !ant;
		for(int j = 0; j <= M; j++)
		{
			pd[at][j] = pd[ant][j];
			if(j >= r.se)
				pd[at][j] = max(pd[at][j], pd[ant][j-r.se] + r.fi);
		}
		ant = at;
	}
	for(auto r:v2)
	{
		at = !ant;
		for(int j = 0; j <= M; j++)
		{
			pd[at][j] = pd[ant][j];
			if(j >= r.se)
				pd[at][j] = max(pd[at][j], pd[ant][j-r.se] + r.fi);
		}
		ant = at;
	}
	int ans = 0;
	for(int i = 0; i <= M; i++)
		ans = max(ans, max(pd[0][i], pd[1][i]));
	return ans;
}

int main()
{
	int x, y, ma = 0;
	char op;
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	cin >> N >> M;
	while(N--)
	{
		cin >> x >> op >> y;
		if(op == 'G')
			vG.push_back({x, y});
		else if(op == 'B')
			vB.push_back({x, y});
		else if(op == 'C')
			vC.push_back({x, y});
	}
	ma = max(ma, knapsack(vB, vG));
	ma = max(ma, knapsack(vC, vG));
	ma = max(ma, knapsack(vB, vC));
	printf("%d\n", ma);
	return 0;
}
