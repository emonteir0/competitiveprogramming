// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sibi-Xor
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2811

#include<bits/stdc++.h>
#define llu unsigned long long
#define ll long long
#define MOD 1000000007

using namespace std;

llu vet[2001];
ll pot[2001];
ll comb[2001][2001];
int cont[64];

void calc()
{
	pot[0] = 1;
	for(int i = 1; i < 64; i++)
		pot[i] = (pot[i-1]*2)%MOD;
}

main()
{
	int N;
	ll sum = 0;
	llu z;
	calc();
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%llu", &vet[i]);
	for(int j = 0; j < 64; j++)
	{
		z = 1LLU << j;
		for(int i = 0; i < N; i++)
			cont[j] += (z&vet[i]) == z;
	}
	for(int j = 0; j < 64; j++)
	{
		for(int i = 1; i <= N; i++)
		{
			//printf("%d %d -> %lld\n", j, i, comb[cont[j]][i])
			if(cont[j] >= i)
			sum = (sum + ((cont[j]-i)&i ? 0 : pot[j]))%MOD;
		}
	}
	printf("%lld\n", sum);
	return 0;
}
