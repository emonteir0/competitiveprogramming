// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Funny Game
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2818

#include<bits/stdc++.h>

using namespace std;

char str[100001];
int N, t;
int dp[100001][6][2];
int vis[100001][6][2];

int solve(int x, int y, int z)
{
	if(str[x] == 0)
		return z ? (y ? 9999999: 0) : 9999999;
	if(vis[x][y][z] == t)
		return dp[x][y][z];
	vis[x][y][z] = t;
	int ans = 9999999;
	ans = min(ans, solve(x+1, y, z)+1);
	if(str[x] != '0' || z == 1)
		ans = min(ans, solve(x+1, (y*10+str[x]-'0')%6, 1));
	//printf("(%d, %d, %d) -> %d\n", x, y, z, ans);
	return dp[x][y][z] = ans;	
}

main()
{
	int x, M;
	scanf("%d", &N);
	for(t = 1; t <= N; t++)
	{
		scanf("%s", str);
		M = strlen(str);
		x = solve(0, 0, 0);
		for(int i = 0; i < M; i++)
			if(str[i] == '0')
			{
				x = min(x, M-1);
				break;
			}
		if(x > 100000)
			printf("Cilada\n");
		else
			printf("%d\n", x);
	}
	return 0;
}
