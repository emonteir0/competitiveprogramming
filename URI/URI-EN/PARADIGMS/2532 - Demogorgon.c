// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Demogorgon
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2532

#include<stdio.h>

int N, M;
int memo[1001][2001];
int dano[1001], custo[1001];

int min(int a, int b)
{
	return (a < b) ? a : b;
}

int solve(int x, int y)
{
	if(y <= 0)
		return 0;
	if(x == N)
		return 999999999;
	if(memo[x][y] != -1)
		return memo[x][y];
	return memo[x][y] = min(solve(x+1, y-dano[x]) + custo[x], solve(x+1, y));
}

main()
{
	int i, j, x;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(i = 0; i < N; i++)
			scanf("%d %d", &dano[i], &custo[i]);
		for(i = 0; i < N; i++)
			for(j = 1; j <= M; j++)
				memo[i][j] = -1;
		x = solve(0, M);
		if(x > 9999999)
			printf("-1\n");
		else
			printf("%d\n", x);
	}
	return 0;
}
