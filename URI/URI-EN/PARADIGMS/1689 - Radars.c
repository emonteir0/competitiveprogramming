// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Radars
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1689

#include<stdio.h>

int val[1000001], memo[1000001], pos[1000001];
int N, K, lim;

int max(int a, int b)
{
	return (a > b) ? a : b;
}

int solve(int x)
{
	if(x > lim)
		return 0;
	if(memo[x] != -1)
		return memo[x];
	return memo[x] = max(solve(x+1), solve(x+K) + val[x]);
}

main()
{
	int i, T, x;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &K);
		for(i = 0; i <= 1000000; i++)
		{
			memo[i] = -1;
			val[i] = 0;
		}
		for(i = 0; i < N; i++)
			scanf("%d", &pos[i]);
		lim = pos[N-1];
		for(i = 0; i < N; i++)
		{
			scanf("%d", &x);
			val[pos[i]] = max(x, val[pos[i]]);
		}
		printf("%d\n", solve(1));
	}
	return 0;
}


