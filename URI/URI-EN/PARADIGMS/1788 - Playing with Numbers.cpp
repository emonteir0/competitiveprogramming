// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Playing with Numbers
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1788

#include<bits/stdc++.h>
#define MAXN 10000000
#define SQN 3164
using namespace std;


int isprime[MAXN+1];
int prime_cont;

void crivo()
{
	int i;
	isprime[0] = 1;
	isprime[1] = 1;
	prime_cont++;
	for(i = 4; i <= MAXN; i+=2)
		isprime[i] = 1;
	for(i = 3; i <= SQN; i += 2)
	{
		if(!isprime[i])
		{
			prime_cont++;
			for(int j = i*i; j <= MAXN; j += i)
				isprime[j] = 1;
		}
	}
	for(; i <= MAXN; i += 2)
		if(!isprime[i])
			prime_cont++;
}

int N, M;
int mat[1001][1001];
int dp[1001][1001][6];

int solve(int x, int y, int z)
{
	int r = 0;
	if(x >= N || x < 0 || y >= M || y < 0)
		return 0;
	if(dp[x][y][z] != -1)
		return dp[x][y][z];
	if(!isprime[mat[x][y]] && z)
		r = max(solve(x+1, y, z-1), solve(x, y-1, z-1));
		if((x < N-1) && mat[x+1][y] < mat[x][y])
			r = max(r, solve(x+1, y, z));
		if((y > 0) && mat[x][y-1] < mat[x][y])
			r = max(r, solve(x, y-1, z));
	return dp[x][y][z] = r+1;
}

int solve2(int x, int y, int z)
{
	int r = 0;
	if(x >= N || x < 0 || y >= M || y < 0)
		return 0;
	if(dp[x][y][z] != -1)
		return dp[x][y][z];
	if(!isprime[mat[x][y]] && z)
		r = max(solve2(x-1, y, z-1), solve2(x, y+1, z-1));
	if((x > 0) && mat[x-1][y] < mat[x][y])
		r = max(r, solve2(x-1, y, z));
	if((y < M-1) && mat[x][y+1] < mat[x][y])
		r = max(r, solve2(x, y+1, z));
	return dp[x][y][z] = r+1;
}

main()
{
	int K, ma;
	char op;
	crivo();
	while(scanf("%d %d %d%*c", &N, &M, &K) == 3 && N)
	{
		ma = 0;
		scanf("%c", &op);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				scanf("%d", &mat[i][j]);
				for(int k = 0; k < 6; k++)
					dp[i][j][k] = -1;
			}
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				ma = max(ma, op == 'R' ? solve(i, j, K) : solve2(i, j, K));
		printf("%d\n", ma);	
	}
	return 0;
}
