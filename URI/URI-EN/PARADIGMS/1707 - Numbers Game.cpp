// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Numbers Game
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1707

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll v[11], num[11];
ll sum[11], A[11], B[11];


ll func(int a)
{
	int i, x, k = 1;
	v[0] = 0;
	num[0] = 0;
	for(i = 1; i <= a%10; i+=2)
	{
		v[0] += i;
		num[0]++;
	}
	a /= 10;
	while(a)
	{
		x = a % 10;
		v[k] = sum[x]*A[k] + x*num[k-1] + x*B[k] + v[k-1];
		num[k] = num[k-1] + x*A[k];
		k++;
		a /= 10;
	}
	return v[k-1];
}

main()
{
	int i, a, b;
	for(i = 1; i <= 10; i++)
		sum[i] = sum[i-1] + i-1;
	A[1] = 5;
	for(i = 2; i <= 10; i++)
		A[i] = A[i-1] * 10;
	B[1] = 1+3+5+7+9;
	for(i = 2; i <= 10; i++)
		B[i] = 10*B[i-1] + sum[10]*A[i-1];
	while(scanf("%d %d", &a, &b) == 2)
	{
		if(a > b)
			swap(a, b);
		printf("%lld\n", func(b) - func(a-1));
	}
	return 0;
}

