// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Input and Output
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2361

#include<stdio.h>

main()
{
	int N, K, i;
	while(scanf("%d %d", &N, &K) == 2 && (N||K))
	{
		for(i = 1; i < K; i++)
			printf("%d ", i);
		for(i = N; i > K; i--)
			printf("%d ", i);
		printf("%d\n", K);
	}
	return 0;
}

