// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Santa Claus Bag
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1767

#include<stdio.h>

typedef struct a
{
	int peso, num, brinks;
}saco;


saco best[301][51], padrao;
int valor[301];
int tam[301];

main()
{
	int n, n2, i, j, x;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&n2);
		padrao.brinks=0;
		padrao.num=n2;
		padrao.peso=0;
		best[0][0]=padrao;
		for(i=1;i<=n2;i++)
		{
			scanf("%d %d",&valor[i],&tam[i]);
			best[i][0]=padrao;
		}
		for(i=0;i<=50;i++)
		{
			best[0][i]=padrao;
		}
		for(i=1;i<=n2;i++)
		{
			for(j=0;j<=50;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]].brinks+valor[i]>best[i-1][j].brinks)
					{
						best[i][j].peso=tam[i]+best[i-1][j-tam[i]].peso;
						best[i][j].brinks=best[i-1][j-tam[i]].brinks+valor[i];
						best[i][j].num=best[i-1][j-tam[i]].num-1;
					}
					else
					{
						best[i][j]=best[i-1][j];
					}
				}
				else
				{
					best[i][j]=best[i-1][j];
				}
			}
		}
		printf("%d brinquedos\nPeso: %d kg\nsobra(m) %d pacote(s)\n\n",best[n2][50].brinks,best[n2][50].peso,best[n2][50].num);
	}
	return 0;
}
