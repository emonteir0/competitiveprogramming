// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Seu Madruga
// Level: 1
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1912

#include<stdio.h>
#define EPS 5e-5

int vet[100000];

main()
{
	int i,N,A,max,acum;
	double inf,sup,mid,sum;
	while((scanf("%d %d",&N,&A)==2)&&(N||A))
	{
		max=0;
		for(i=0,acum=0;i<N;i++)
		{
			scanf("%d",&vet[i]);
			max=max>vet[i]?max:vet[i];
			acum+=vet[i];
		}
		inf=0;
		sup=max;
		sum=0;
		if(acum==A)
			printf(":D\n");
		else if (acum<A)
			printf("-.-\n");
		else
		{
			while(1)
			{
				sum=0;
				mid=(inf+sup)/2;
				for(i=0;i<N;i++)
					sum+=vet[i]>mid?vet[i]-mid:0;
				if(sum+EPS>=A && sum-EPS<=A)
					break;
				if(sum>A)
					inf=mid;
				else
					sup=mid;
			}
			printf("%.4lf\n",mid);
		}
	}
	return 0;
}

