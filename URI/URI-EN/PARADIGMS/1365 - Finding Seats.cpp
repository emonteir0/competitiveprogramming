// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Finding Seats
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1365

#include<bits/stdc++.h>

using namespace std;

int N, M, K, best;

char mat[301][301];
int acum[301][301];

int solve()
{
	int lim, sum, sz;
	for(int k = 1; k <= M; k++)
	{
		lim = best/k;
		if(lim == 0)
			return best;
		for(int j = 0; j <= M-k; j++)
		{
			sum = 0;
			sz = 0;
			for(int i = 1; i <= N; i++)
			{
				if(sz <= lim)
				{
					sum += acum[i][j+k]-acum[i][j];
					sz++;
				}
				else
				{
					sum -= acum[i-sz][j+k]-acum[i-sz][j];
					sum += acum[i][j+k]-acum[i][j];
				}
				while(sum >= K)
				{
					best = min(best, sz*k);
					sz--;
					sum -= acum[i-sz][j+k]-acum[i-sz][j];
				}
			}
		}
	}
	return best;
}

int main()
{
	while(scanf("%d %d %d", &N, &M, &K) != EOF && N)
	{
		best = N*M;
		for(int i = 1; i <= N; i++)
			scanf("%s", mat[i]);
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= M; j++)
				acum[i][j] = (mat[i][j-1]=='.')+ acum[i][j-1];
		printf("%d\n", solve());
	}
	return 0;
}
