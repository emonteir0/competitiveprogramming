// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Critical Wave
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1027

#include<bits/stdc++.h>
#define INFN -999999999

using namespace std;

vector<int> G[65536];

int test(int p, int op, int x)
{
	int ind;
	if(op == 0)
	{
		ind = upper_bound(G[p-1].begin(), G[p-1].end(), x)-G[p-1].begin();
		if(ind == G[p-1].size())
			return 0;
		return 1 + test(p, 1, G[p-1][ind]);
	}
	if(op == 1)
	{
		ind = upper_bound(G[p+1].begin(), G[p+1].end(), x)-G[p+1].begin();
		if(ind == G[p+1].size())
			return 0;	
		return 1 + test(p, 0, G[p+1][ind]);
	}
}

main()
{
	int N, x, y, ma;
	while(scanf("%d", &N) == 1)
	{
		ma = 0;
		for(int i = 0; i < 65536; i++)
			G[i].clear();
		while(N--)
		{
			scanf("%d %d", &x, &y);
			G[y+32768].push_back(x);
		}
		for(int i = 0 ; i < 65536; i++)
			sort(G[i].begin(), G[i].end());
		for(int i = 0; i < 65536; i++)
		{
			if(i >= 1)
				ma = max(ma, test(i, 0, INFN));
			if(i < 65535)
				ma = max(ma, test(i, 1, INFN));
		}
		printf("%d\n", ma);
	}
	return 0;
}
