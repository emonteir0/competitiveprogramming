// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Prom
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1616

#include<stdio.h>
#define MOD 1000000007
#define ll unsigned long long int

int mat[1001][1001];

main()
{
	int i, j, n, m;
	while(scanf("%d %d", &n, &m)==2 && (n||m))
	{
		for(i=0;i<=n;i++)
			mat[i][1] = 0;
		for(i=0;i<=m;i++)
			mat[1][i] = 0;
		mat[1][1] = m;
		for(i=2;i<=n;i++)
			for(j=1;j<=m;j++)
				mat[i][j] = (((ll)(m-j+1))*mat[i-1][j-1]+((ll)j)*mat[i-1][j]) % MOD;
		printf("%d\n", mat[n][m]);
	}
	return 0;
}
