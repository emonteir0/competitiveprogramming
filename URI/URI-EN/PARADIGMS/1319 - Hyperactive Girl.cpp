// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hyperactive Girl
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1319

#include<bits/stdc++.h>

using namespace std;

struct stc
{
	int st, en;
};

bool cmp(stc a, stc b)
{
	if(a.st != b.st)
		return a.st < b.st;
	return a.en < b.en;
}

int M, N;
stc vet[102];
int pd[101][101][102];

int solve(int x, int y, int z)
{
	if(x == N+1)
		return vet[y].en == M;
	if(pd[x][y][z] != -1)
		return pd[x][y][z];
	int r = solve(x+1, y, z);
	if((vet[y].en >= vet[x].st) && (vet[x].st > vet[y].st) && (vet[x].en > vet[y].en) && (vet[x].st > vet[z].en))
		r += solve(x+1, x, y);
	if(r >= 100000000)
		r -= 100000000;
	//printf("pd[%d][%d][%d] = %d\n", x, y, z, r);
	pd[x][y][z] = r;
	return r;
}


main()
{
	vet[0].st = -1;
	while(scanf("%d %d", &M, &N) == 2 && M)
	{
		for(int i = 1; i <= N; i++)
			scanf("%d %d", &vet[i].st, &vet[i].en);
		sort(vet+1, vet+N+1, cmp);
		vet[N+1].en = -1;
		for(int i = 0; i <= N; i++)
			for(int j = 0; j <= N; j++)
				for(int k = 0; k <= N+1; k++)
					pd[i][j][k] = -1;
		printf("%d\n", solve(1, 0, N+1));
	}
	return 0;
}
