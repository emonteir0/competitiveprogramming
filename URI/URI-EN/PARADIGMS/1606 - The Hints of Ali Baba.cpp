// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Hints of Ali Baba
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1606

#include<bits/stdc++.h>

using namespace std;

int vis[100001];
vector<int> ans;

main()
{
	int N, M, T = 1, x;
	while(scanf("%d %d", &N, &M) == 2)
	{
		ans.clear();
		while(N--)
		{
			scanf("%d", &x);
			if(vis[x] < T)
			{
				vis[x] = T;
				ans.push_back(x);
			}
		}
		printf("%d", ans[0]);
		for(int i = 1; i < (int)ans.size(); i++)
			printf(" %d", ans[i]);
		printf("\n");
		T++;
	}
	return 0;
}
