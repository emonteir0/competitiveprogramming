// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Buses
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1474

#include <stdio.h>
#define MOD 1000000
#define ll long long int
  
void multiply(int F[2][2]);

void multiply2(int F[2][2]);
  
void power(int F[2][2], ll n);

int F[2][2];
int M[2][2]; 
ll G[2][2];
  
void power(int F[2][2], ll n)
{
  if(n==0||n==1)
      return; 
    power(F, n/2);
    multiply2(F);
  if(n&1)
     multiply(F);
}
  
void multiply(int F[2][2])
{
    int i,j,k;
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
        {
        	G[i][j] = 0;
            for(k=0;k<2;k++)
                G[i][j]+=(((ll)F[i][k])*M[k][j]);
        }
                
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2(int F[2][2])
{
    int i,j,k;  
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
        {	
        	G[i][j] = 0;
            for(k=0;k<2;k++)
                G[i][j]+=(((ll)F[i][k])*F[k][j]);
        }
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            F[i][j]=G[i][j]%MOD;
}
   
int main()
{
	ll a, b, c;
	while(scanf("%lld %lld %lld",&a, &b, &c)==3)
	{
		b = b%MOD;
		c = c%MOD;
		F[0][0] = M[0][0] = b;
		F[0][1] = M[0][1] = c;
		F[1][0] = M[1][0] = 1;
		F[1][1] = M[1][1] = 0;
		if(a == 5)
			printf("%06lld\n", b);
		else
		{
			power(F, a/5-1);
			printf("%06lld\n", (b*F[0][0]+F[0][1])%MOD);
		}
	}
	return 0;
}
