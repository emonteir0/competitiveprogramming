// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Wizard is Relentless
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2713

#include<bits/stdc++.h>

using namespace std;

int N, K, dano;
int D[1001];
double memo[1001][101];
double X[1001];

double solve(int x, int m)
{
	if(x > N)
		return 0;
	double &r = memo[x][m];
	if(r != -1)
		return r;
	r = solve(x+1, m) + X[x]/dano;
	if(m > 0)
		r = min(r, solve(x+1, m-1) + X[x]/(dano+D[x]));
	return r;
}

main()
{
	int i, j;
	while(scanf("%d %d %d", &N, &K, &dano) == 3)
	{
		for(i = 0; i <= N; i++)
			for(j = 0; j <= K; j++)
				memo[i][j] = -1;
		for(i = 1; i <= N; i++)
			scanf("%lf %d", &X[i], &D[i]);
		printf("%.4lf\n", solve(1, K));
	}
	return 0;
}
