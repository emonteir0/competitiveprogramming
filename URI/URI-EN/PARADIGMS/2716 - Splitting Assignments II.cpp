// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Splitting Assignments II
// Level: 6
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2716

#include<bits/stdc++.h>
#define TOT 300000

using namespace std;

int vet[TOT+1];


main()
{
	int i, N, x, ma;
	vet[0] = 1;
	while(scanf("%d", &N) == 1)
	{
		ma = 0;
		while(N--)
		{
			scanf("%d", &x);
			ma += x;
			for(i = ma; i >= x; i--)
				vet[i] |= vet[i-x];
		}
		for(i = ma/2; i <= ma; i++)
			if(vet[i])
				break;
		printf("%d\n", abs(2*i-ma));
		for(i = 1; i <= ma; i++)
			vet[i] = 0;
	}
	return 0;
}
