// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Röntgen Sequences
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2080

#include<stdio.h>
#include<string.h>

char num[2000001], num2[2000001];

main()
{
	int n, x, cont, k, i, j, z, tam;
	char c;
	scanf("%d%*c",&n);
	for(j=0;j<n;j++)
	{
		if(j>0)
			printf("\n");
		scanf("%s %d%*c",num,&x);
		printf("%s\n",num);
		while(x--)
		{
			c = num[0];
			tam = strlen(num);
			cont = 1;
			k=0;
			strcpy(num2,"");
			for(i=1;i<tam;i++)
			{
				if(num[i] == c)
					cont++;
				else
				{
					z = 10;
					while(cont >= z)
						z*=10;
					z /= 10;
					while(z>=1)
					{
						num2[k++] = cont/z + '0';
						cont%=z;
						z/=10;
					}
					num2[k++] = c;
					c = num[i];
					cont = 1;
				}
			}
			z = 10;
			while(cont >= z)
				z*=10;
			z/= 10;
			while(z>=1)
			{
				num2[k++] = cont/z + '0';
				cont%=z;
				z/=10;
			}
			num2[k++] = c;
			num2[k] = 0;
			printf("%s\n",num2);
			strcpy(num,num2);
		}
	}
	return 0;
}

