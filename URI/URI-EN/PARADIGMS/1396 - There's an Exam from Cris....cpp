// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: There's an Exam from Cris...
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1396

#include<bits/stdc++.h>

using namespace std;

struct st
{
	string nome;
	int ind;
	st()
	{
	}
	st(string nome, int ind)
	{
		this->nome = nome;
		this->ind = ind;
	}
};

bool cmp(st a, st b)
{
	return a.nome < b.nome;
}

st vet[101];
int bagunca[101];

main()
{
	int N, K, t = 0;
	char str[21];
	while((scanf("%d %d", &N, &K) != EOF) && N)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%s", str);
			vet[i].nome = str;
			vet[i].ind = i;
		}
		sort(vet, vet+N, cmp);
		for(int i = 0; i < N; i++)
			bagunca[vet[i].ind] = i;
		int p = 0;
		while(p < N && (K > 0))
		{
			for(; p < N; p++)
			{
				if(p != bagunca[p])
					break;
			}
			int me = bagunca[p], ime = p;
			for(int i = p+1; i < N && i <= p+K; i++)
			{
				if(me > bagunca[i])
				{
					me = bagunca[i];
					ime = i;
				}
			}
			for(int i = ime; i > p; i--)
			{
				swap(bagunca[i], bagunca[i-1]);
				K--;
			}
			p++;
		}
		printf("Instancia %d\n", ++t);
		for(int i = 0; i < N; i++)
			printf("%s ", vet[bagunca[i]].nome.c_str());
		printf("\n\n");
	}
	return 0;
}
