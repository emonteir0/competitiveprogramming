// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Annoying Painting Tool
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1649

#include<bits/stdc++.h>

using namespace std;

int BIT[101][101];
char mat[101][101];
int N, M;

void updateBIT(int x, int y0, int val)
{
	int y;
	for(; x <= N; x += (x & -x))
	{
		y = y0;
		for(; y <= M; y += (y & -y))
			BIT[x][y] ^= val;
	}
	return;
}

int getSum(int x, int y0)
{
	int sum = 0, y;
	for(; x > 0; x -= (x &-x))
	{
		y = y0;
		for(; y > 0; y -= (y&-y))
			sum ^= BIT[x][y];
	}
	return sum;
}

void printmat()
{
	for(int i = 1; i <= N; i++)
		{
			for(int j = 1; j <= M; j++)
				printf("%d ", getSum(i, j));
			printf("\n");
		}
	printf("\n");
}

main()
{
	int R, C, cont, xa, ya, xb, yb, f, v;
	while(scanf("%d %d %d %d", &N, &M, &R, &C) == 4 && N)
	{
		cont = 0;
		
		for(int i = 0; i < N; i++)
			scanf("%s", mat[i]);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				xa = xb = i+1;
				ya = yb = j+1;
				v = mat[i][j]-'0';
				updateBIT(xa, ya, v);
				updateBIT(xa, yb+1, v);
				updateBIT(xb+1, ya, v);
				updateBIT(xb+1, yb+1, v);
			}
		
		for(int i = 1; i <= N-R+1; i++)
			for(int j = 1; j <= M-C+1; j++)
			{
				if(getSum(i, j))
				{
					//printf("%d %d\n", i, j);
					cont++;
					xa = i;
					ya = j;
					xb = i+R-1;
					yb = j+C-1;
					updateBIT(xa, ya, 1);
					updateBIT(xa, yb+1, 1);
					updateBIT(xb+1, ya, 1);
					updateBIT(xb+1, yb+1, 1);
					//printmat();
				}
			}
		f = 1;
		for(int i = 1; i <= N && f; i++)
			for(int j = 1; j <= M && f; j++)
				if(getSum(i, j))
				{
					f = 0;
					printf("-1\n");
				}
		if(f)
			printf("%d\n", cont);
		for(int i = 0; i <= N; i++)
			for(int j = 0; j <= M; j++)
				BIT[i][j] = 0;
	}
	return 0;
}

