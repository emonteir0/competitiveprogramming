// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stack Game
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1522

#include<stdio.h>

int n, veta[100], vetb[100], vetc[100];
int memo[101][101][101];


int pilha(int a, int b, int c)
{
	int z = 0;
	if(a==n && b==n && c==n)
		return 1;
	if(memo[a][b][c] != -1)
		return memo[a][b][c];
	if(a<n && b<n && c<n)
		z |= pilha(a+1,b+1,c+1) && ((veta[a]+vetb[b]+vetc[c])%3 == 0);
	if(a<n && b<n)
		z |= pilha(a+1,b+1,c) && ((veta[a]+vetb[b])%3 == 0);
	if(a<n && c<n)
		z |= pilha(a+1,b,c+1) && ((veta[a]+vetc[c])%3 == 0);
	if(b<n && c<n)
		z |= pilha(a,b+1,c+1) && ((vetb[b]+vetc[c])%3 == 0);
	if(a<n)
		z |= pilha(a+1,b,c) && (veta[a]%3 == 0);
	if(b<n)
		z |= pilha(a,b+1,c) && (vetb[b]%3 == 0);
	if(c<n)
		z |= pilha(a,b,c+1) && (vetc[c]%3 == 0);
	return memo[a][b][c] = z;
}

main()
{
	int i, j, k;
	while(scanf("%d", &n)==1 && n)
	{
		for(i=0;i<n;i++)
			scanf("%d %d %d", &veta[i], &vetb[i], &vetc[i]);
		for(i=0;i<=n;i++)
			for(j=0;j<=n;j++)
				for(k=0;k<=n;k++)
					memo[i][j][k] = -1;
		printf("%d\n", pilha(0,0,0));
	}
	return 0;
}
