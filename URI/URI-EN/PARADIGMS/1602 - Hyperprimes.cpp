// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hyperprimes
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1602

#define MAX 2000000
#define MAX2 1000
#include<bits/stdc++.h>

using namespace std;
  
bitset<MAX2+2> isprime;
int prime[MAX2+2], prime_cont=0;
int lim = MAX2;
int vet[MAX+1],vet2[MAX+1];
   
void primo()
{  
    isprime.set();
    for(int i=4; i<=lim; i+=4)
    	isprime.reset(i);
    for(int i=3; i<=lim; i+=2)
    {
        if(isprime[i])
        {     
            for(int j = i + i; j <= lim; j+= i)
                isprime.reset(j);
        }
    }
}
 
void crivo(int b)
{
    int i,j,max=sqrt(b);
    for(i=2;i<=max;i++)
    {
        for(j=i+i;j<=b;j+=i)
        {
            if(i*i<j)
            vet[j]+=2;
            if(i*i==j)
            vet[j]+=1;
        }
    }
}


 
main()
{
    int i,n,max=0;
    vet[1]=-1;
    primo();
    crivo(MAX);
    vet2[2]=1;
    for(i=3;i<=2000000;i++)
    {
    	vet2[i]=vet2[i-1];
    	if(isprime[vet[i]+2])
    		vet2[i]++;
    }
    while(scanf("%d",&n)==1)
    	printf("%d\n",vet2[n]);
    return 0;
}
