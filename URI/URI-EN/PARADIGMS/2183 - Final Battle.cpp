// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Final Battle
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2183

#include<bits/stdc++.h>
#define INF (int)1e8

using namespace std;

int N, K;
int cvalue[128], dvalue[128];
char mat[101][101];
char palavra[301];
int memo[101][101][32][11];
int dx[] = {1, 0}, dy[] = {0, 1};

int solve(int x, int y, int key, int remain)
{
	int r = INF, i, x2, y2;
	if((x == N) && (y == N))
		return 0;
	if(memo[x][y][key][remain] != -1)
		return memo[x][y][key][remain];
	if(mat[x][y] == '#')
		return r;
	key |= cvalue[mat[x][y]];
	for(i = 0; i < 2; i++)
	{
		x2 = x + dx[i];
		y2 = y + dy[i];
		if((x2 > N) || (y2 > N))
			continue;
		if(key == 31)
			r = min(r, solve(x2, y2, 0, K));
		else
		{
			if(remain)
				r = min(r, solve(x2, y2, key, remain-1));
			else
				r = min(r, solve(x2, y2, key, 0) + dvalue[mat[x][y]]);
		}
	}
	return memo[x][y][key][remain] = r;
}

main()
{
	int i, j, k, l, x, tam;
	scanf("%d %d%*c", &N, &K);
	cvalue['P'] = 1;
	cvalue['O'] = 2;
	cvalue['W'] = 4;
	cvalue['E'] = 8;
	cvalue['R'] = 16;
	for(i = '1'; i <= '9'; i++)
		dvalue[i] = i-'0';
	for(i = 1; i <= N; i++)
	{
		gets(palavra);
		k = 1;
		tam = strlen(palavra);
		for(j = 0; j < tam; j++)
			if(palavra[j] != ' ')
				mat[i][k++] = palavra[j];
		for(j = 1; j <= N; j++)
			for(k = 0; k < 32; k++)
				for(l = 0; l <= K; l++)
					memo[i][j][k][l] = -1;
	}
	x = solve(1, 1, 0, 0);
	printf("%d\n", (x >= INF) ? -1 : x+1);
	return 0;
}
