// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cameras
// Level: 8
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1978

#include<bits/stdc++.h>
#define MAXN 10001
#define INF 1001001001
#define EPS 1e-6
#define ll long long

using namespace std;

int N, L, A;
int t;

pair<double, double> vet[MAXN];
int dp[MAXN], vis[MAXN];

int solve(int x)
{
	if(x == N)
		return 0;
	if(vis[x] == t)
		return dp[x];
	vis[x] = t;
	int r = INF;
	for(int i = x+1; i <= N; i++)
	{
		if(vet[i].first > vet[x].second && abs(vet[i].first - vet[x].second) > EPS)
			break;
		r = min(r, solve(i)+1);
	}		
	return dp[x] = r;
}

main()
{
	int ans;
	ll X, Y;
	double alcance;
	while(scanf("%d %d %d", &N, &L, &A) == 3 && N)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%lld %lld", &X, &Y);
			alcance = sqrt(Y*Y-A*A);
			vet[i].first  = min(double(L), max(0.0, X-alcance));
			vet[i].second = min(X+alcance, double(L));
		}
		sort(vet, vet+N);
		vet[N].first = L-abs(1e-5);
		vet[N].second = L;
		//for(int i = 0; i <= N; i++)
			//printf("%.2lf %.2lf\n", vet[i].first, vet[i].second);
		if(abs(vet[0].first) > EPS)
		{
			printf("Tera que comprar mais cameras.\n");
			continue;
		}
		ans = INF;
		t++;
		for(int i = 0; i < N; i++)
		{
			if(abs(vet[i].first) > EPS)
				break;
			ans = min(ans, solve(i));
		}
		if(ans >= INF)
			printf("Tera que comprar mais cameras.\n");
		else
			printf("%d\n", ans);
	}
	return 0;
}
