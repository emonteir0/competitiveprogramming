// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Wine Trading in Gergovia
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1661

#include<stdio.h>

main()
{
	int N;
	long long int a,b,v;
	while( ( scanf("%d",&N) == 1 ) && ( N != 0 ) )
	{
		N--;
		v=0;
		scanf("%lld",&a);
		while(N--)
		{
			scanf("%lld",&b);
			if(a<0)
				v-=a;
			else
				v+=a;
			a+=b;
		}
		printf("%lld\n",v);
	}
}

