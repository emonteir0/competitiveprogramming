// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Motoboy
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1286

#include<stdio.h>


int best[21][31];
int valor[21];
int tam[21];

main()
{
	int n2, i, j, limite, x;
	while(scanf("%d",&n2)==1&&n2>0)
	{
		scanf("%d",&limite);
		for(i=1;i<=n2;i++)
			scanf("%d %d",&valor[i],&tam[i]);
		for(i=1;i<=n2;i++)
		{
			for(j=0;j<=limite;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]]+valor[i]>best[i-1][j])
					{
						best[i][j]=best[i-1][j-tam[i]]+valor[i];
					}
					else
					{
						best[i][j]=best[i-1][j];
					}
				}
				else
				{
					best[i][j]=best[i-1][j];
				}
			}
		}
		printf("%d min.\n",best[n2][limite]);
	}
	return 0;
}
