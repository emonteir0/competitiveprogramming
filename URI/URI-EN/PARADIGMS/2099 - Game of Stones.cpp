// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of Stones
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2099

#include<bits/stdc++.h>
#define MOD 1000000007

using namespace std;

int N;
int dp[1001][1001][2];
//int dp[3][3][2];


int solve(int x, int lst, int p)
{
	int r = 0;
	if(x == 0)
		return p != (N&1); 
	if(dp[x][lst][p] != -1)
		return dp[x][lst][p];
	for(int i = lst; i <= x; i++)
	{
		r += solve(x-i, i, !p);
		if(r >= MOD)
			r -= MOD;
	}
	return dp[x][lst][p] = r;
}

main()
{
	scanf("%d", &N);
	for(int i = 0; i <= N; i++)
		dp[0][i][!(N&1)] = 1;
	for(int i = 1; i <= N; i++)
	{
		for(int k = 0; k < 2; k++)
			for(int j = 1; j <= i; j++)
				for(int l = j; l <= i; l++)
				{
					dp[i][j][k] += dp[i-l][l][!k];
					if(dp[i][j][k] >= MOD)
						dp[i][j][k] -= MOD;
				}
	}
	printf("%d\n", dp[N][1][0]);
	return 0;
}
