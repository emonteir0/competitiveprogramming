// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ice Statues Festival
// Level: 5
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1034

#include<stdio.h>

int valores[25], moedas[1000001];

main()
{
	int i,j,k,N,M;
	scanf("%d",&k);
	while(k--)
	{
		scanf("%d %d",&M,&N);
		for(i=0;i<M;i++)
			scanf("%d",&valores[i]);
	    moedas[0] = 0;
	    for(i=1; i<=N; i++) 
		{
	        moedas[i] = 1000000;
	        for(j=0; j<M; j++) 
			{
	            if(i-valores[j] >= 0) 
	                moedas[i] = (moedas[i]<moedas[i-valores[j]]+1)?moedas[i]:moedas[i-valores[j]]+1;
	        }
	    }
	    printf("%d\n",moedas[N]);
	}
	return 0;
}
