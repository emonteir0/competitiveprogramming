// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Tree
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2026

#include<stdio.h>


int best[101][1001];
int valor[101];
int tam[101];

main()
{
	int n,n2, i, j, k, limite, x;
	scanf("%d",&n2);
	for(k=1;k<=n2;k++)
	{
		scanf("%d",&n);
		scanf("%d",&limite);
		best[0][0]=0;
		for(i=1;i<=n;i++)
		{
			scanf("%d %d",&valor[i],&tam[i]);
			best[i][0]=0;
		}
		for(i=0;i<=limite;i++)
		{
			best[0][i]=0;
		}
		for(i=1;i<=n;i++)
		{
			for(j=0;j<=limite;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]]+valor[i]>best[i-1][j])
					{
						best[i][j]=best[i-1][j-tam[i]]+valor[i];
					}
					else
					{
						best[i][j]=best[i-1][j];
					}
				}
				else
				{
					best[i][j]=best[i-1][j];
				}
			}
		}
		printf("Galho %d:\nNumero total de enfeites: %d\n\n",k,best[n][limite]);
	}
	return 0;
}
