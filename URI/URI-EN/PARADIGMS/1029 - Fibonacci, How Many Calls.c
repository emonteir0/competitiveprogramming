// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibonacci, How Many Calls?
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1029

#include<stdio.h>

main()
{
	int n,x;
	scanf("%d",&n);
	while(n--)
	{ 
		scanf("%d",&x);
		switch(x)
		{
			case 1:printf("fib(1) = 0 calls = 1\n");break;
			case 2:printf("fib(2) = 2 calls = 1\n");break;
			case 3:printf("fib(3) = 4 calls = 2\n");break;
			case 4:printf("fib(4) = 8 calls = 3\n");break;
			case 5:printf("fib(5) = 14 calls = 5\n");break;
			case 6:printf("fib(6) = 24 calls = 8\n");break;
			case 7:printf("fib(7) = 40 calls = 13\n");break;
			case 8:printf("fib(8) = 66 calls = 21\n");break;
			case 9:printf("fib(9) = 108 calls = 34\n");break;
			case 10:printf("fib(10) = 176 calls = 55\n");break;
			case 11:printf("fib(11) = 286 calls = 89\n");break;
			case 12:printf("fib(12) = 464 calls = 144\n");break;
			case 13:printf("fib(13) = 752 calls = 233\n");break;
			case 14:printf("fib(14) = 1218 calls = 377\n");break;
			case 15:printf("fib(15) = 1972 calls = 610\n");break;
			case 16:printf("fib(16) = 3192 calls = 987\n");break;
			case 17:printf("fib(17) = 5166 calls = 1597\n");break;
			case 18:printf("fib(18) = 8360 calls = 2584\n");break;
			case 19:printf("fib(19) = 13528 calls = 4181\n");break;
			case 20:printf("fib(20) = 21890 calls = 6765\n");break;
			case 21:printf("fib(21) = 35420 calls = 10946\n");break;
			case 22:printf("fib(22) = 57312 calls = 17711\n");break;
			case 23:printf("fib(23) = 92734 calls = 28657\n");break;
			case 24:printf("fib(24) = 150048 calls = 46368\n");break;
			case 25:printf("fib(25) = 242784 calls = 75025\n");break;
			case 26:printf("fib(26) = 392834 calls = 121393\n");break;
			case 27:printf("fib(27) = 635620 calls = 196418\n");break;
			case 28:printf("fib(28) = 1028456 calls = 317811\n");break;
			case 29:printf("fib(29) = 1664078 calls = 514229\n");break;
			case 30:printf("fib(30) = 2692536 calls = 832040\n");break;
			case 31:printf("fib(31) = 4356616 calls = 1346269\n");break;
			case 32:printf("fib(32) = 7049154 calls = 2178309\n");break;
			case 33:printf("fib(33) = 11405772 calls = 3524578\n");break;
			case 34:printf("fib(34) = 18454928 calls = 5702887\n");break;
			case 35:printf("fib(35) = 29860702 calls = 9227465\n");break;
			case 36:printf("fib(36) = 48315632 calls = 14930352\n");break;
			case 37:printf("fib(37) = 78176336 calls = 24157817\n");break;
			case 38:printf("fib(38) = 126491970 calls = 39088169\n");break;
			case 39:printf("fib(39) = 204668308 calls = 63245986\n");break;
		}
	}
	return 0;
}

