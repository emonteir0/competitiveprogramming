// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Goão and Stairs
// Level: 4
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2119

#include<bits/stdc++.h>
#define MOD 1000000009

using namespace std;


int N, K;
int valores[1001];
int vis[1001];
int passos[1001];
vector<int> G[10001], W[10001];
int dist[10001];

void dijkstra()
{
	int u, v;
	int d, w, me;
	priority_queue< pair<int, int> > pq;
	dist[0] = 1001001001;
	pq.push(make_pair(dist[0], 0));
	while(!pq.empty())
	{
		d = pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(u == N-1)
			return;
		if(d < dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			w = W[u][i];
			me = min(d, w);
			if(me > dist[v])
			{
				dist[v] = me;
				pq.push(make_pair(dist[v], v));
			}
		}
	}
}

void build()
{
	valores[0] = 1;
	for(int i = 1; i <= 1000; i++)
		for(int j = 0; j < K; j++)
			if(i >= passos[j])
				valores[i] = (valores[i] + valores[i-passos[j]])%MOD;
			else
				break;
}

main()
{
	int M, u, v, w;
	scanf("%d %d %d", &N, &M, &K);
	for(int i = 0; i < K; i++)
		scanf("%d", &passos[i]);
	sort(passos, passos+K);
	build();
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(v);
		G[v].push_back(u);
		W[u].push_back(valores[w]);
		W[v].push_back(valores[w]);
	}
	dijkstra();
	dist[0] = 0;
	printf("%d\n", dist[N-1]);
	return 0;
}
