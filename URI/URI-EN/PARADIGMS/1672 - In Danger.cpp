// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: In Danger
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1672

#include<bits/stdc++.h>

using namespace std;

int solve(int x)
{
	int y;
	if(x <= 2)
		return 0;
	y = solve((x+1)/2);
	if(x&1)
	{
		if(y == 0)
			return x-1;
		else
			return 2*(y-1);
	}
	return 2*y;
}

main()
{
	int pot[7];
	char numero[5];
	int x;
	pot[0] = 1;
	for(int i = 1; i <= 7; i++)
		pot[i] = pot[i-1]*10;
	while(scanf("%s", numero) == 1 && strcmp(numero, "00e0") != 0)
	{
		x = (10*(numero[0]-'0')+(numero[1]-'0'))*pot[(numero[3]-'0')];
		printf("%d\n", solve(x)+1);
	}
	return 0;
}
