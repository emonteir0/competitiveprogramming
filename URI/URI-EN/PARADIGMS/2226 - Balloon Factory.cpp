// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Balloon Factory
// Level: 2
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2226

#include <bits/stdc++.h>
#define ll unsigned long long
using namespace std;

#define MAXN 100001
#define MAXL 20
#define MAXC 10

int term[MAXN], to[MAXN][MAXC], link[MAXN], sz;


void add(char *s)
{
	int cur = 0;
	
	for(int i = 0; s[i]; i++)
	{
		int c = s[i]-'0';
		
		if(!to[cur][c])
			to[cur][c] = sz++;
			
		cur = to[cur][c];
	}
	
	term[cur] = 1;
}

void push_links()
{
	int que[sz];
	int st = 0, fi = 1;
	que[0] = 0;
	
	while(st < fi)
	{
		int V = que[st++];
		int U = link[V];
		
		term[V] |= term[U];
	
		for(int c = 0; c < MAXC; c++)
		{
			if(to[V][c])
			{
				link[to[V][c]] = V ? to[U][c] : 0;
				que[fi++] = to[V][c];
			}
			else
			{
				to[V][c] = to[U][c];
			}
		}
	}
}

int tam;
char number[MAXL], palavra[MAXL];
ll dp[MAXN][MAXL][2][2];

ll solve(int estado, int len, int maior, int zero)
{
	if(term[estado]) return 0;
	if(len == tam) 	 return 1;
	if(dp[estado][len][maior][zero] != -1) return dp[estado][len][maior][zero];
	
	ll ret = 0;
	if(zero && len < tam-1)
		ret += solve(estado, len+1, 0, zero);
	for(int i = zero && len < tam-1; i < 10; i++)
	{
		if((i+'0' > number[len]) && maior)
			break;
		ret = ret + solve(to[estado][i], len+1, maior && (i+'0' == number[len]), 0);
	}
	return dp[estado][len][maior][zero] = ret;
}

int main()
{
	int m;
	ll N;
	while(scanf("%s%*c", &number) == 1 && number[0] != '0')
	{
		sscanf(number, "%llu", &N);
		tam = strlen(number);
		sz = 1;
		for(int i = 0; i < MAXN; i++)
		{
			term[i] = 0;
			link[i] = 0;
			for(int j = 0; j < MAXC; j++)
				to[i][j] = 0;
		}
		scanf("%d%*c", &m);
		while(m--)
		{
			scanf("%s%*c", palavra);
			add(palavra);
		}
		push_links();
		memset(dp, -1, sizeof dp);
		printf("%llu\n", N+1-solve(0, 0, 1, 1));
	}
	return 0;
}
