// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cuarenta and Two
// Level: 3
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1590

#include<cstdio>
#include<algorithm>

using namespace std;

typedef struct
{
	int x,y;
}tipo;

tipo vet[35];

bool cmp(tipo a, tipo b)
{
	return a.y>b.y;
}

int tam;

main()
{
	int N,M,K,i,x,MSB,v;
	tipo aux;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %d",&M,&K);
		for(i=0;i<M;i++)
		{
			scanf("%d",&x);
			vet[i].x=vet[i].y=x;
		}
		sort(vet,vet+M,cmp);
		MSB=1;
		while(MSB<=vet[0].y)
			MSB<<=1;
		MSB>>=1;
		v=0;
		while(MSB!=0)
		{
			tam=0;
			for(i=0;i<M;i++)
			{
				if((vet[i].x & MSB) == MSB)
				{
					aux = vet[tam];
					vet[tam++] = vet[i];
					vet[i] = aux;
				}
			}
			if(tam==K)
				break;
			if(tam>K)
				M=tam;
			for(i=0;i<M;i++)
				vet[i].x%=MSB;
			sort(vet,vet+M,cmp);
			MSB>>=1;
		}
		v=2147483647;
		for(i=0;i<K;i++)
			v&=vet[i].y;
		printf("%d\n",v);
	}
	return 0;
}
