// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hot Potato
// Level: 7
// Category: PARADIGMS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2236

#include<bits/stdc++.h>
#define MAXN 250005

using namespace std;

int N;
int A[50001];
vector<int> tree[MAXN];

void build(int node, int l, int r)
{
	if(l == r)
	{
		tree[node].push_back(A[l]);
		return;
	}
	int mid = (l+r)/2;
	build(node*2, l, mid);
	build(node*2+1, mid+1, r);
	merge(tree[2*node].begin(), tree[2*node].end(), tree[2*node+1].begin(), tree[2*node+1].end(), back_inserter(tree[node]));
}

int query(int node, int start, int end, int l, int r, int x)
{
	if(end < l || start > r || start > end)
		return 0;
	if(l <= start && r >= end)
	{
		//printf("%d!\n", tree[node].size());
		//printf("(%d, %d) < %d: %d\n", start, end, x, upper_bound(tree[node].begin(), tree[node].end(), x) - tree[node].begin());
		return upper_bound(tree[node].begin(), tree[node].end(), x) - tree[node].begin();
	}
	int mid = (start+end)/2;
	return query(node*2, start, mid, l, r, x) + query(node*2+1, mid+1, end, l, r, x);
}

int re[50001];
int proximo[50001];

int dfs(int u) 
{
    if (re[u]) 
		return re[u];
    re[u] = u;
    return re[u] = dfs(proximo[u]);
}

int get_val(int u) 
{
    if (re[u] == u)
		return u;
    return min(u, get_val(proximo[u]));
}

int reach(int u)
{
	if (re[u] == u)
		return A[u];
    re[u] = u;
    return A[u] = min(u, reach(proximo[u]));
}


int best(int left, int right)
{
	int lo, hi, mid, ans1 = 1, x, y, z, sz;
	sz = right-left+1;
	lo = 1, hi = N;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		x = query(1, 1, N, left, right, mid);
	//	printf("%d: %d\n", mid, x);
		if(x+x <= sz)
		{
			y = x;
			ans1 = mid;
			lo = mid+1;
		}
		else
			hi = mid-1;
	}
	if(ans1 != N)
	{
		y = query(1, 1, N, left, right, ans1);
		z = query(1, 1, N, left, right, ans1+1);
	//	printf("%d -> %d | %d -> %d\n", ans1, y, ans1+1, z);
		if(y+y > sz)
			y = sz-y;
		if(y < sz-z)
			return ans1+1;
	}
	lo = 1, hi = ans1;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		x = query(1, 1, N, left, right, mid);
		if(x == y)
		{
			ans1 = mid;
			hi = mid-1;
		}
		else
			lo = mid+1;
	}
	return ans1;
}


int main()
{
	int i, M, l, r;
	scanf("%d %d", &N, &M);
	for(i = 1; i <= N; i++)
		scanf("%d", &proximo[i]);
	for(i = 1; i <= N; i++)
		dfs(i);
	for(i = 1; i <= N; i++)
		if(re[i] == i)
			A[i] = get_val(proximo[i]);
	for(i = 1; i <= N; i++)
		reach(i);
	/*for(int i = 1; i <= N; i++)
		printf("%d -> %d\n", i, A[i]);*/
	build(1, 1, N);
	while(M--)
	{
		scanf("%d %d", &l, &r);
		printf("%d\n", best(l, r));
	}
	return 0;
}
