// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Turtles
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1518

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int x, y;
	char op;
	st(){}
	st(int x, int y, char op)
	{
		this->x = x;
		this->y = y;
		this->op = op;
	}
	bool operator< (st st2) const
	{
		if(this->x != st2.x)
			return this->x < st2.x;
		if(this->y != y)
			return this->y < st2.y;
		return this->op < st2.op;
	}
};

main()
{
	int x, y, xi, yi, X, Y, dx, dy, me, t, ta;
	st vetor[3];
	while(scanf("%d %d%*c", &xi, &yi) != EOF && xi)
	{
		me = 999999;
		for(int i = 0; i < 3; i++)
			scanf("%d %d %c%*c", &vetor[i].x, &vetor[i].y, &vetor[i].op);
		sort(vetor, vetor+3);
		do
		{
			x = xi;
			y = yi;
			t = 0;
			for(int i = 0; i < 3; i++)
			{
				if(vetor[i].op == 'D')
				{
					dx = vetor[i].x + t - x;
					dy = vetor[i].y - y;
					X = abs(dx);
					Y = abs(dy);
					if(dx < 0)
					{
						if(2*X >= Y)
							ta = (X+Y+2)/3;
						else
							ta = (Y-X);
					}
					else
						ta = (X+Y);
					t += ta;
					x = vetor[i].x + t;
					y = vetor[i].y;
				}
				else
				{
					dx = vetor[i].x - x;
					dy = vetor[i].y + t - y;
					X = abs(dx);
					Y = abs(dy);
					if(dy < 0)
					{
						if(2*Y >= X)
						{
							ta = (X+Y+2)/3;
						}
						else
							ta = X-Y;
					}
					else
						ta = (X+Y);
					t += ta;
					x = vetor[i].x;
					y = vetor[i].y + t;
				}
			}
			me = min(me, t);
		}while(next_permutation(vetor, vetor+3));
		printf("%d\n", me);
	}
	return 0;
}
