// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Donor
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2571

#include<stdio.h>
#include<math.h>

main()
{
	long long a;
	double b, c, d;
	while(scanf("%lld %lf %lf %lf", &a, &b, &c, &d) != EOF)
	{
		if(a*c/100.0 < d)
			printf("0\n");
		else
			printf("%.0lf\n", floor((log(d)-log(c/100.0)-log(a))/log(1-b/100.0)));
	}
	return 0;
}
