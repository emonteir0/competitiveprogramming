// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Truco
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2309

#include<bits/stdc++.h>

using namespace std;

int val[14];

main()
{
	int N, a, b, c, d, e, f;
	int ca = 0, cap, cb = 0, cbp;
	val[4] = 1;
	val[5] = 2;
	val[6] = 3;
	val[7] = 4;
	val[11] = 6;
	val[12] = 5;
	val[13] = 7;
	val[1] = 8;
	val[2] = 9;
	val[3] = 10;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d %d %d %d", &a, &b, &c, &d, &e, &f);
		a = val[a];
		b = val[b];
		c = val[c];
		d = val[d];
		e = val[e];
		f = val[f];
		cap = (a >= d) + (b >= e) + (c >= f);
		cbp = (a < d) + (b < e) + (c < f);
		if(cap >= cbp)
			ca++;
		else
			cb++;
	}
	printf("%d %d\n", ca, cb);
	return 0;
}
