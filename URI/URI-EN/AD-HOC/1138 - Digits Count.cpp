// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Digits Count
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1138

#include<bits/stdc++.h>
#define ll long long
using namespace std;

struct digitos
{
	int vis;
	ll dig[10];
	digitos()
	{
		vis = 0;
		for(int i = 0; i < 10; i++)
			dig[i] = 0;
	}
	digitos(int x, int n)
	{
		vis = 0;
		for(int i = 0; i < 10; i++)
			dig[i] = 0;
		dig[x] = n;
	}
	digitos operator+ (digitos dig2) const
	{
		digitos dig3;
		for(int i = 0; i < 10; i++)
			dig3.dig[i] = dig[i] + dig2.dig[i];
		return dig3;
	}
	digitos operator- (digitos dig2) const
	{
		digitos dig3;
		for(int i = 0; i < 10; i++)
			dig3.dig[i] = dig[i] - dig2.dig[i];
		return dig3;
	}
	void printdig()
	{
		for(int i = 0; i < 9; i++)
			printf("%lld ", dig[i]);
		printf("%lld\n", dig[9]);
	}
};

digitos dp[9][2][2], ZERO;
int num;
int pot[10];
char numero[10];
int N;

digitos solve(int x, int maior, int zero)
{
	if(x == N)
		return ZERO;
	if(dp[x][maior][zero].vis == 1)
		return dp[x][maior][zero];
	if(zero)
		dp[x][maior][zero] = solve(x+1, 0, zero);
	for(int i = zero; i < 10; i++)
	{
		if(maior && i > numero[x]-'0')
			break;
		dp[x][maior][zero] = dp[x][maior][zero] + solve(x+1, maior && (i == numero[x]-'0'), 0);
		if(!((x == 0) && (i == 0)))
			dp[x][maior][zero] = dp[x][maior][zero] + digitos(i, (maior && (i == numero[x]-'0')) ? num%pot[x] + 1 : pot[x]);
	}
	dp[x][maior][zero].vis = 1;
	return dp[x][maior][zero];
}

digitos calc(int x)
{
	num = x;
	sprintf(numero, "%d", x);
	for(N = 0; numero[N]; N++)
		dp[N][0][0] = dp[N][0][1] = dp[N][1][0] = dp[N][1][1] = digitos();
	pot[N-1] = 1;
	for(int i = N-2; i >= 0; i--)
		pot[i] = pot[i+1]*10;
	return solve(0, 1, 1);
}

main()
{
	int a, b;
	while(scanf("%d %d", &a, &b) == 2 && a)
	{
		(calc(b)-calc(a-1)).printdig();
	}
	return 0;
}

