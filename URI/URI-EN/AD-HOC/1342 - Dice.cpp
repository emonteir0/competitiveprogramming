// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dice
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1342

#include<bits/stdc++.h>

using namespace std;

int N;
int armad[3];
int armadilhas[100001];
int posicao[11];
int pode[11];

int proximo(int x)
{
	return (x+1 <= N) ? x+1 : 1;
}

main()
{
	int S, M, ind, x, y;
	while(scanf("%d %d", &N, &S) == 2 && N)
	{
		for(int i = 1; i <= N; i++)
		{
			pode[i] = 1;
			posicao[i] = 0;
		}
		for(int i = 0; i < 3; i++)
		{
			scanf("%d", &armad[i]);
			armadilhas[armad[i]] = 1;
		}
		ind = 1;
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d", &x, &y);
			x += y;
			while(1)
			{
				if(pode[ind])
					break;
				pode[ind] = 1;
				ind = proximo(ind);
			}
			posicao[ind] += x;
			if(posicao[ind] > S)
			{
				printf("%d\n", ind);
				break;
			}
			if(armadilhas[posicao[ind]])
				pode[ind] = 0;
			ind = proximo(ind);
		}
		while((M--) > 0)
			scanf("%*d %*d");
		for(int i = 0; i < 3; i++)
			armadilhas[armad[i]] = 0;
	}
	return 0;
}
