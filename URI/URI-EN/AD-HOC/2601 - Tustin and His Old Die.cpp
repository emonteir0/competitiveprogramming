// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tustin and His Old Die
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2601

#include<bits/stdc++.h>

using namespace std;

bool ehnum(char c)
{
	return c >= '0' && c <= '9';
}

main()
{
	int T;
	char a, b, c, d, e, f;
	int x, y;
	scanf("%d%*c", &T);
	while(T--)
	{
		x = 1;
		y = 2;
		cin >> a;
		cin >> b >> c >> d >> e;
		cin >> f;
		if(!ehnum(a) && !ehnum(f))
		{
			x *= y;
			y += 2;
		}
		if(!ehnum(b) && !ehnum(d))
		{
			x *= y;
			y += 2;
		}
		if(!ehnum(c) && !ehnum(e))
		{
			x *= y;
		}
		printf("%d\n", x);
	}
	return 0;
}
