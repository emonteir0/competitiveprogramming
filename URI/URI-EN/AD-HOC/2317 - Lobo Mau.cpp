// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lobo Mau
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2317

#include<bits/stdc++.h>

using namespace std;

int N, M;
char mat[250][251];
int vis[250][251];

int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

main()
{
	int x, x2, y, y2, sheeps, wolves, totalsheeps = 0, totalwolves = 0;
	queue< pair<int, int> > fila;
	scanf("%d %d%*c", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
		{
			if(mat[i][j] != '#' && !vis[i][j])
			{
				fila.push(make_pair(i,j));
				sheeps = 0;
				wolves = 0;
				while(!fila.empty())
				{
					x = fila.front().first;
					y = fila.front().second;
					fila.pop();
					if(vis[x][y])
						continue;
					vis[x][y] = 1;
					if(mat[x][y] == 'k')
						sheeps++;
					if(mat[x][y] == 'v')
						wolves++;
					for(int k = 0; k < 4; k++)
					{
						x2 = x + dx[k];
						y2 = y + dy[k];
						if(inRange(x2, y2) && mat[x2][y2] != '#' && !vis[x2][y2])
							fila.push(make_pair(x2, y2));
					}
				}
				if(sheeps > wolves)
					totalsheeps += sheeps;
				else
					totalwolves += wolves;
			}
		}
	printf("%d %d\n", totalsheeps, totalwolves);
	return 0;
}
