// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rodoido
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2629

#include<bits/stdc++.h>
using namespace std;

#define vd vector <double>
#define vvd vector <vd>
#define FOR(i, n) for (int i = 0; i < n; i++)


vvd mul(vvd a, vvd b) {
	int n = a.size();
	int m = b[0].size();
	vvd c(n, vd(m, 0));
	FOR(i, n) FOR(j, m) FOR(k, n)
		c[i][j] = (c[i][j]+a[i][k]*b[k][j]);
		
	return c;
}

vvd pow(vvd b, int e) {
	int n = b.size();
	vvd x(n, vd(n));
	FOR(i,n) x[i][i] = 1;
	for (; e; b=mul(b, b), e>>=1) {
		if (e&1) x = mul(x, b);
	}
	/*FOR(i, n) {
		FOR(j, n) cout << x[i][j] << ' ' ;
		cout << endl;
	}*/
	return x;
}

int main()
{
	int i, j, Q, a, b, c, n;
	while(scanf("%d", &n) == 1)
	{
		vvd A = vvd(n, vd(n));
		for(i = 0; i < n; i++)
			for(j = 0; j < n; j++)
				scanf("%lf", &A[i][j]);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d %d %d", &a, &b, &c);
			vvd B = pow(A, c);
			printf("%.6lf\n", B[b-1][a-1]);
		}
	}
	return 0;
}
