// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Caçadores de Mitos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2343

#include<stdio.h>


int mat[501][501];

main()
{
	int i, j, Q;
	char ok = 0;
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d %d", &i, &j);
		mat[i][j]++;
	}
	for(i = 1; i <= 500; i++)
		for(j = 1; j <= 500; j++)
			ok |= mat[i][j] > 1;
	printf("%d\n", ok);
	return 0;
}

