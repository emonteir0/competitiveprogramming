// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: URI
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1787

#include<cstdio>
#include<cmath>
#include<algorithm>

double absol(double x)
{
	if(x<0)
		return -x;
	return x;
}

bool cmp(int* a, int* b)
{
	return ((*b)<(*a));
}

main()
{
	int i,n,x,y,z,k,cont1,cont2,cont3,*ma,*vet[3];
	double val1,val2,val3;
	while(scanf("%d",&n)==1&&(n>0))
	{
		cont1=cont2=cont3=0;
		for(i=0;i<n;i++)
		{
			scanf("%d %d %d",&x,&y,&z);
			val1=log(x)/log(2);
			val1=absol(val1-round(val1));
			val2=log(y)/log(2);
			val2=absol(val2-round(val2));
			val3=log(z)/log(2);
			val3=absol(val3-round(val3));
			k=0;
			ma=&k;
			if(val1<0.00005)
			{
				cont1++;
				k+=1;	
			}
			if(*ma<x)
				ma=&x;
			if(val2<0.00005)
			{
				cont2++;
				k+=2;
			}
			if(*ma<y)
				ma=&y;
			if(val3<0.00005)
			{
				cont3++;
				k+=4;			
			}
			if(*ma<z)
				ma=&z;
			if((ma==&x)&&((k&1)==1))
				cont1++;
			if((ma==&y)&&((k&2)==2))
				cont2++;
			if((ma==&z)&&((k&4)==4))
				cont3++;
		}
		vet[0]=&cont1;
		vet[1]=&cont2;
		vet[2]=&cont3;
		std::sort(vet,vet+3,cmp);
		if(*(vet[0])==*(vet[1]))
			printf("URI\n");
		else if(vet[0]==&cont1)
			printf("Uilton\n");
		else if(vet[0]==&cont2)
			printf("Rita\n");
		else
			printf("Ingred\n");
	}
	return 0;
}
