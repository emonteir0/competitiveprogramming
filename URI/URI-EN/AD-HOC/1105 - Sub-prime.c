// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sub-prime
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1105

#include<stdio.h>

int bancos[21];

main()
{
	int i,n,m,x,y,z,b;
	while((scanf("%d %d",&n,&m)==2)&&(n||m))
	{
		for(i=1;i<=n;i++)
		{
			scanf("%d",&x);
			bancos[i]=x;
		}
		b=1;
		for(i=0;i<m;i++)
		{
			scanf("%d %d %d",&x,&y,&z);
			bancos[x]-=z;
			bancos[y]+=z;
		}
		for(i=1;i<=n;i++)
			if(bancos[i]<0)
				b=0;
		printf("%s\n",b?"S":"N");
	}
	return 0;
}
