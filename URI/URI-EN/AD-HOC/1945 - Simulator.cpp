// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Simulator
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1945

#include<bits/stdc++.h>

using namespace std;

map<string,int> mapa;

main()
{
	string str,var;
	int x;
	cin.sync_with_stdio(false);
	while(getline(cin,str))
	{
		stringstream ss(str);
		if(str.find("+")==-1)
		{
			ss >> var;
			ss >> str;
			ss >> str;
			x=atoi(str.c_str());
			mapa[var]=x;
		}
		else
		{
			ss >> var;
			ss >> str;
			ss >> str;
			x=mapa[str];
			ss >> str;
			ss >> str;
			x+=mapa[str];
			mapa[var]=x;
		}
	}
	printf("%d\n",x);
	return 0;
}
