// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vice-Campeão
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2408

#include<bits/stdc++.h>

using namespace std;

int vet[3];

main()
{
	scanf("%d %d %d", &vet[0], &vet[1], &vet[2]);
	sort(vet, vet+3);
	printf("%d\n", vet[1]);
	return 0;
}

