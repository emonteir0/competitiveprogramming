// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ambiguous Permutations
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1663

#include<stdio.h>

int vet[100000];

main()
{
	int n,i;
	char u;
	while(scanf("%d",&n)==1&&n>0)
	{
		u=1;
		for(i=0;i<n;i++)
		{
			scanf("%d",&vet[i]);
		}
		for(i=0;i<n;i++)
		{
			if(vet[vet[i]-1]!=i+1)
			{
				u=0;
				break;
			}
		}
		printf("%s\n",u==1?"ambiguous":"not ambiguous");
	}
	return 0;
}
