// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pulo do Sapo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2392

#include<stdio.h>
 
main()
{
	int i,j,a,b,n,m,vet[100];
	for(i=0;i<100;i++)
	{
		vet[i]=0;
	}
	scanf("%d %d",&a,&b);
	for(i=0;i<b;i++)
	{
		scanf("%d %d",&n,&m);
		for(j=(n-1)%m;j<a;j+=m)
		vet[j]=1;
	}
	for(i=0;i<a;i++)
	printf("%d\n",vet[i]);
	return 0;
}
