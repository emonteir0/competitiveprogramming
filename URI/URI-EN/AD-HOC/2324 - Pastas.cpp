// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pastas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2324

#include<bits/stdc++.h>

using namespace std;

int vet[1001];

main()
{
	int i, P, N, D, x;
	scanf("%d %d", &P, &N);
	D = (N+P-1)/P;
	for(i = 0; i < N; i++)
	{
		scanf("%d", &x);
		vet[x]++;
	}
	vet[0] = D;
	for(i = 1; i <= P; i++)
	{
		if(vet[i] == D && vet[i-1] != D)
			break;
		else if(vet[i] > D || vet[i] < D-1)
			break;
	}
	printf("%s\n", (i == P+1) ? "S" : "N");
	return 0;
}
