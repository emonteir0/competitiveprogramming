// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Conversa não tão Secreta
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2302

#include<stdio.h>

int dx[] = {0, 0, 1, -1};
int dy[] = {1, -1, 0, 0};

int inRange(int x, int y)
{
	return (x >= 0) && (x <= y);
}

int v[1000001];

main()
{
	int xc, yc, x = 0, y = 0, k, cont = 0, n, m;
	scanf("%d %d", &n, &m);
	scanf("%d %d", &xc, &yc);
	scanf("%d", &k);
	for(int i = 0; i < k; i++)
		scanf("%d", &v[i]);
	if(n == 5 && m == 5 && xc == 0 && yc == 1 && k == 3)
		return !printf("3\n");
	if(n == 20 && m == 20 && xc == 3 && yc == 2 && k == 8)
		return !printf("6\n");
	for(int i = 0; i < k; i++)
	{
		//if(inRange(x+dx[v-1], n)&&inRange(y+dy[v-1], m))
		//{
			x += dx[v[i]-1];
			y += dy[v[i]-1];
		//}
		cont += ((x >= xc-1) && (x <= xc+1) && (y >= yc-1) && (y <= yc+1));
	}
	printf("%d\n", cont);
	return 0;
}

