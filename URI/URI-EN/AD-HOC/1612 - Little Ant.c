// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Little Ant
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1612

#include<stdio.h>

main()
{
	int N,x;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&x);
		printf("%d\n",(x+1)/2);
	}
	return 0;
}
