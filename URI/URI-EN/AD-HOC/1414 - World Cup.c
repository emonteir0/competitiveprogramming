// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: World Cup
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1414

#include<stdio.h>


int main()
{
	int M,N,x,cont;
	while((scanf("%d %d%*c",&N,&M)==2)&&(N||M))
	{
		cont=3*M;
		while(N--)
		{
			scanf("%*s %d%*c",&x);
			cont-=x;
		}
		printf("%d\n",cont);
	}
	return 0;
}

