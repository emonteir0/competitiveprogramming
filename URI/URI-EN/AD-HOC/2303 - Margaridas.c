// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Margaridas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2303

#include<stdio.h>

int mat[1000][1000];

int main()
{
	int lin, col;
	int m, n;
	int max;
	int cur;
	int i, j, k, l;

	scanf(" %d %d %d %d", &lin, &col, &m, &n);
	max=0;
	for(i=0; i<lin; i++)
		for(j=0; j<col; j++)
			scanf(" %d", &mat[i][j]);

	for(i=0; i<lin; i+=m)
		for(j=0; j<col; j+=n)
		{
			cur=0;
			for(k=i; k<(i+m); k++)
				for(l=j; l<(j+n); l++)
					cur+=mat[k][l];
			max = (cur>max)?cur:max;
		}
	printf("%d\n", max);
	return 0;
}

