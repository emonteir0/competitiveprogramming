// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cheating
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2530

#include<bits/stdc++.h>

using namespace std;

vector<int> G[1001];


main()
{
	int N, M, x, p, b;
	vector<int>::iterator it;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 0; i <= 1000; i++)
			G[i].clear();
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &x);
			G[x].push_back(i);
		}
		p = 0;
		b = 1;
		while(M--)
		{
			scanf("%d", &x);
			it = lower_bound(G[x].begin(), G[x].end(), p);
			if(it == G[x].end())
				b = 0;
			else
				p = *it;
		}
		printf("%s\n", b ? "sim" : "nao");
	}
	return 0;
}
