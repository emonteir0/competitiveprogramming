// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Reading Books
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1542

#include<stdio.h>

main()
{
	int a,b,c,d;
	double x;
	while(scanf("%d",&a)==1&&a>0)
	{
		scanf("%d %d",&b,&c);
		x=(double)(b*a)/(c-a);
		d=x*c;
		printf("%d pagina%s\n",d,(d>1)?"s":"");
	}
	return 0;
}
