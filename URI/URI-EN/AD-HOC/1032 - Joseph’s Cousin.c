// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Joseph’s Cousin
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1032

#include<stdio.h>

int nextprime(int a)
{
	int i;
	while(1)
	{
		a++;
		for(i=2;i*i<=a;i++)
			if(a%i==0)
			{
				i=0;
				break;
			}
		if(i!=0)
			return a;
	}
}

typedef struct a
{
	int ant,dep;
}tipo;

main()
{
	tipo lista[3501];
	int i,j,n,p=2,inicio,primo[3501];
	for(i=0;i<3501;i++)
	{
		primo[i]=p;
		p=nextprime(p);
	}
	while(scanf("%d",&n)==1&&n)
	{
		for(i=0;i<n;i++)
		{
			lista[i].ant=(i+n-1)%n;
			lista[i].dep=(i+1)%n;
		}
		inicio=n-1;
		for(i=1;i<n;i++)
		{
			p=primo[i-1]%(n-i+1);
			if(p==0)
				p=n-i+1;
			for(j=0;j<p;j++)
				inicio=lista[inicio].dep;			
			lista[lista[inicio].ant].dep=lista[inicio].dep;
			lista[lista[inicio].dep].ant=lista[inicio].ant;
		}
		printf("%d\n",lista[inicio].dep+1);
	}
	return 0;
}
