// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Deli Deli
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1652

#include<cstdio>
#include<cstring>
#include<string>
#include<map>

using namespace std;

map<string,string> Mapa;

main()
{
	int i,tam,n,m;
	char vet[21],vet2[21],a,b;
	string str,str2;
	scanf("%d %d%*c",&n,&m);
	while(n--)
	{
		scanf("%s %s%*c",vet,vet2);
		str=vet;
		str2=vet2;
		Mapa[str]=str2;
	}
	while(m--)
	{
		scanf("%s%*c",vet);
		str=vet;
		if(Mapa.find(str)!=Mapa.end())
			printf("%s\n",Mapa[str].c_str());
		else
		{
			tam=strlen(vet);
			a=vet[tam-1];
			if(a=='y')
			{
				if(tam>1)
				{
					b=vet[tam-2];
					if(!((b=='a') || (b=='e') || (b=='i') || (b=='o') || (b=='u')))
					{
						for(i=0;i<tam-1;i++)
							printf("%c",vet[i]);
						printf("ies\n");
					}
					else
						printf("%ss\n",vet);
				}
				else
					printf("%ss\n",vet);
			}
			else if((a=='s') || (a=='o') || (a=='x'))
			{
				for(i=0;i<tam;i++)
					printf("%c",vet[i]);
				printf("es\n");
			}
			else if(tam>1)
			{
				b=vet[tam-2];
				if((a=='h') && ((b=='c') || (b=='s')))
				{
					for(i=0;i<tam;i++)
						printf("%c",vet[i]);
					printf("es\n");
				}
				else
					printf("%ss\n",vet);
			}
			else
				printf("%ss\n",vet);
		}
	}
	
	return 0;
}

