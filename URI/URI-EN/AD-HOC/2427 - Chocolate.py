# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Chocolate
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2427

from math import floor, log

x = input()
print int(4 ** floor(log(x)/log(2)))

