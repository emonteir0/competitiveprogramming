// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bonus Grade Points
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1758

#include<bits/stdc++.h>

using namespace std;

char msg[100001], msg2[100001];

int status(double x)
{
	if(x < 4)
		return 0;
	if(x < 7)
		return 1;
	return 2;
}

main()
{
	int T, N, M, i, j, s;
	double acum, vet[5];
	char b;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &M, &N);
		for(j = 0; j < N; j++)
		{
			acum = 0;
			for(i = 0; i < M; i++)
			{
				scanf("%lf", &vet[i]);
				acum += vet[i];
			}
			sort(vet, vet+M);
			acum /= M;
			s = status(acum);
			if(s == 0)
				printf("%.2lf\n", acum);
			else
			{
				b = 1;
				for(i = M-1; i >= 0; i--)
				{
					if(vet[i] > acum)
					{
						if(s == status(vet[i]))
						{
							printf("%.2lf\n", vet[i]);
							b = 0;
							break;
						}
					}
					else
						break;
				}
				if(b)
					printf("%.2lf\n", acum);
			}
		}
		
	}
	return 0;
}
