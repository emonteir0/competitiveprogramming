// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Distância de Manhattan
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2437

#include<stdio.h>

int absol(int a)
{
	if(a < 0)
		return -a;
	return a;
}

main()
{
	int a, b, c, d;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	printf("%d\n", absol(a-c) + absol(b-d));
	return 0;
}

