// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Start Grid
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1228

#include<stdio.h>

main()
{
	int i,j,N,vet[24],vet2[24],x,cont;
	while(scanf("%d",&N)==1)
	{
		cont=0;
		for(i=0;i<N;i++)
		{
			scanf("%d",&x);
			vet[x]=i;
		}
		for(i=0;i<N;i++)
		{
			scanf("%d",&vet2[i]);
			for(j=0;j<i;j++)
				cont+=vet[vet2[j]]>vet[vet2[i]];
		}
		printf("%d\n",cont);
	}
	return 0;
}
