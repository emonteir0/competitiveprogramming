// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Postman
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2448

#include<stdio.h>
#include<stdlib.h>

int N;
int vet[45000];

int busca(int x)
{
	int inf = 0, sup = N-1, mid;
	while(inf <= sup)
	{
		mid = (inf+sup)/2;
		if(vet[mid] == x)
			return mid;
		else if(vet[mid] < x)
			inf = mid+1;
		else
			sup = mid;
	}
	return mid;
}


main()
{
	int i, x, indant = 0, ind, M, acum = 0;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	while(M--)
	{
		scanf("%d", &x);
		ind = busca(x);
		acum += abs(indant-ind);
		indant = ind;
	}
	printf("%d\n", acum);
	return 0;
}
