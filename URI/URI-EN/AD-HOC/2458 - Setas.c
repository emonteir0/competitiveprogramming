// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Setas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2458

#include<stdio.h>


int N, cont = 0;
char mat[501][501];
int vis[501][501];
int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1};
char proibido[] = {'A', 'V', '<', '>'};

int inRange(int x, int y)
{
	return (x >= 0) && (x < N) && (y >= 0) && (y < N);
}

void bfs(int x, int y, int op)
{
	int i;
	if(mat[x][y] == proibido[op])
	{
		cont += !vis[x][y];
		vis[x][y] = 1;
	}
	else
		return;
	for(i = 0; i < 4; i++)
	{
		if(inRange(x+dx[i], y+dy[i]))
			if(!vis[x+dx[i]][y+dy[i]])
				bfs(x+dx[i], y+dy[i], i);
	}
}



main()
{
	int i;
	scanf("%d%*c", &N);
	for(i = 0; i < N; i++)
		scanf("%s%*c", mat[i]);
	for(i = 0; i < N; i++)
	{
		if(mat[0][i] == 'A' && !vis[0][i])
			bfs(0, i, 0);
		if(mat[N-1][i] == 'V' && !vis[N-1][i])
			bfs(N-1, i, 1);
		if(mat[i][0] == '<' && !vis[i][0])
			bfs(i, 0, 2);
		if(mat[i][N-1] == '>' && !vis[i][N-1])
			bfs(i, N-1, 3);
	}
	printf("%d\n", N*N - cont);
	return 0;
}

