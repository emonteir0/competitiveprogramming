// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Paper, Scissors, I Love You!
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2847

#include<bits/stdc++.h>

using namespace std;

map<char, int> Mapa;


char str[100001];

main()
{
	int me = 10000000;
	gets(str);
	for(int i = 0; str[i]; i++)
		Mapa[str[i]]++;
	me = min(me, Mapa['I']);
	me = min(me, Mapa['l']);
	me = min(me, Mapa['o']/2);
	me = min(me, Mapa['v']);
	me = min(me, Mapa['e']);
	me = min(me, Mapa['y']);
	me = min(me, Mapa['u']);
	me = min(me, Mapa['!']);
	printf("%d\n", me);
	return 0;
}
