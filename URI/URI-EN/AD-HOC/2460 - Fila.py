# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Fila
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2460

l = []

N = input()
t = raw_input().split(' ')
for x in t:
	l += [int(x)]
M = input()
t = raw_input().split(' ')
for x in t:
	l.remove(int(x))
output = str(l[0])
for i in range(1, len(l)):
	output += ' %d' % l[i]
print output

