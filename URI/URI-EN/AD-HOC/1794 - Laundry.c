// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Laundry
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1794

#include<stdio.h>
 
 
main()
{
    int n,la,lb,sa,sb;
    scanf("%d %d %d %d %d", &n,&la,&lb,&sa,&sb);
    printf("%s\n",(n>=la&&n<=lb&&n>=sa&&n<=sb)?"possivel":"impossivel");
    return 0;
}
