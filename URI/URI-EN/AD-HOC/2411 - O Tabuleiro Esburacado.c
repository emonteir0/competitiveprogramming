// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: O Tabuleiro Esburacado
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2411

#include<stdio.h>

int mat[9][9];
int dx[9] = {0, -2, -1, 1, 2, 2, 1, -1, -2};
int dy[9] = {0, 1, 2, 2, 1, -1, -2, -2, -1};

main()
{
	int N, x = 5, y = 5, v, i = 0;
	mat[3][3] = 1;
	mat[4][6] = 1;
	mat[5][2] = 1;
	mat[5][3] = 1;
	scanf("%d", &N);
	while(N--)
	{
		i++;
		scanf("%d", &v);
		x += dx[v];
		y += dy[v];
		if(mat[x][y])
			break;
	}
	while((N--) > 0)
		scanf("%*d");
	printf("%d\n", i);
	return 0;
}

