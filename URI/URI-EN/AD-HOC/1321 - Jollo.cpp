// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jollo
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1321

#include<bits/stdc++.h>

using namespace std;

int used[53];

int solve(int a, int b, int c, int d, int e, int f)
{
	int vet[3], vet2[3];
	vet[0] = a;
	vet[1] = b;
	vet[2] = c;
	vet2[0] = d;
	vet2[1] = e;
	vet2[2] = f;
	sort(vet, vet+3);
	do
	{
		sort(vet2, vet2+3);
		do
		{
			if((vet2[0] > vet[0]) + (vet2[1] > vet[1]) + (vet2[2] > vet[2]) < 2)
				return 0;
		}while(next_permutation(vet2, vet2+3));
	}while(next_permutation(vet, vet+3));
	return 1;
}

main()
{
	int a, b, c, d, e, f;
	while(scanf("%d %d %d %d %d", &a, &b, &c, &d, &e) != EOF && a)
	{
		used[a] = used[b] = used[c] = used[d] = used[e] = 1;
		for(f = 1; f <= 52; f++)
		{
			if(used[f])	continue;
			if(solve(a, b, c, d, e, f))
			{
				printf("%d\n", f);
				break;
			}
		}
		if(f == 53)
			printf("-1\n");
		used[a] = used[b] = used[c] = used[d] = used[e] = 0;
	}
	return 0;
}

