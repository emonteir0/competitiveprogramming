// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Karnaugh Maps I
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2051

#include<bits/stdc++.h>

using namespace std;

int mat[4][4];

int main()
{
	int T, N, x, a, b, c, d, e, t = 0;
	scanf("%d", &T);
	while(T--)
	{
		if(t)
			printf("\n");
		t = 1;
		x = 0;
		scanf("%d", &N);
		if(N == 2)
		{
			for(int i = 0; i < 4; i++)
			{
				scanf("%d %d - %d", &a, &b, &c);
				x |= 1 + (c == 1);
				mat[b][a] = c;
			}
			printf("Mapa de Karnaugh\n  0 1\n0|%d %d\n1|%d %d\n", mat[0][0], mat[0][1], mat[1][0], mat[1][1]);
			printf("%s\n", x == 3 ? "Contingencia" : (x == 1 ? "Contradicao" : "Tautologia"));
		}
		else if(N == 3)
		{
			for(int i = 0; i < 8; i++)
			{
				scanf("%d %d %d - %d", &a, &b, &c, &d);
				x |= 1 + (d == 1);
				mat[c][a*2+b] = d;
			}
			printf("Mapa de Karnaugh\n  00 01 11 10\n0|%d  %d  %d  %d\n1|%d  %d  %d  %d\n", mat[0][0], mat[0][1], mat[0][3], mat[0][2],
																						  mat[1][0], mat[1][1], mat[1][3], mat[1][2]);
			printf("%s\n", x == 3 ? "Contingencia" : (x == 1 ? "Contradicao" : "Tautologia"));
		}
		else
		{
			for(int i = 0; i < 16; i++)
			{
				scanf("%d %d %d %d - %d", &a, &b, &c, &d, &e);
				x |= 1 + (e == 1);
				mat[c*2+d][a*2+b] = e;
			}
			printf("Mapa de Karnaugh\n   00 01 11 10\n00|%d  %d  %d  %d\n01|%d  %d  %d  %d\n11|%d  %d  %d  %d\n10|%d  %d  %d  %d\n", 
			mat[0][0], mat[0][1], mat[0][3], mat[0][2],
			mat[1][0], mat[1][1], mat[1][3], mat[1][2],
			mat[3][0], mat[3][1], mat[3][3], mat[3][2],
			mat[2][0], mat[2][1], mat[2][3], mat[2][2]);
			printf("%s\n", x == 3 ? "Contingencia" : (x == 1 ? "Contradicao" : "Tautologia"));
		}
	}
	return 0;
}
