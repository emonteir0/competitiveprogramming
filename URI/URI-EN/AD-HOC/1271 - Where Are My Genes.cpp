// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Where Are My Genes
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1271

#include<bits/stdc++.h>

using namespace std;

int fi[1000], se[1000];

main()
{
	int N, M, Q, t = 1, x;
	while(scanf("%d", &N) == 1 && N)
	{
		printf("Genome %d\n", t++);
		scanf("%d", &M);
		for(int i = 0; i < M; i++)
			scanf("%d %d", &fi[i], &se[i]);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d", &x);
			for(int i = 0; i < M; i++)
				if(x >= fi[i] && x <= se[i])
					x = se[i]-(x-fi[i]);
			printf("%d\n", x);
		}
	}
	return 0;
}
