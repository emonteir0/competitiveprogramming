// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Corrida
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2416

#include<stdio.h>

main()
{
    int a, b;
    scanf("%d %d", &a, &b);
    printf("%d\n", a%b);
    return 0;
}
