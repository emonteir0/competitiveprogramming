// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Plantação
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2442

#include<bits/stdc++.h>
#define ll long long
using namespace std;

map<int, int> Mapa;

main()
{
	int i, N, K, x, me, acum = 0;
	ll total = 0, dif = 0;
	char op;
	scanf("%d %d", &K, &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d%*c", &x);
		Mapa[x]++;
		acum += x;
	}
	me = (Mapa.begin())->first;
	while(K-- && N)
	{
		scanf("%c%*c", &op);
		if(op == 'C')
		{
			dif++;
			total += acum + dif*N;
		}
		else
		{
			dif--;
			total += acum + dif*N;
			if(-dif == me)
			{
				N -= Mapa[me];
				acum -= Mapa[me] * me;
				Mapa.erase(me);
				if(N)
					me = (Mapa.begin())->first;
			}
		}
	}
	printf("%lld\n", total);
	return 0;
}
