// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting in Chinese
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2102

#include<stdio.h>

int mat[1001][1001];

void solve(int c)
{
	int N, L, i, j, x, y, v;
	if(c)
		printf("\n");
	scanf("%d %d", &N, &L);
	for(i = 1; i <= N; i++)
		for(j = 1; j <= N; j++)
			mat[i][j] = 0;
	while(L--)
	{
		scanf("%*d %d %d %d", &x, &y, &v);
		mat[x][y] += v;
	}
	for(i = 1; i <= N; i++)
		for(j = 1; j <= N; j++)
			if(mat[i][j] != 0)
				printf("%d %d %d\n", i, j, mat[i][j]);
}


main()
{
	int K;
	scanf("%d", &K);
	K--;
		solve(0);
	while(K--)
		solve(1);
	return 0;
}
