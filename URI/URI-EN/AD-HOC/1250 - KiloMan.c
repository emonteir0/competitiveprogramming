// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: KiloMan
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1250

#include<stdio.h>

int tiros[51];

main()
{
	int i,N,M,cont;
	char pulo;
	scanf("%d%*c",&N);
	while(N--)
	{
		cont=0;
		scanf("%d%*c",&M);
		for(i=0;i<M;i++)
			scanf("%d%*c",&tiros[i]);
		for(i=0;i<M;i++)
		{
			scanf("%c",&pulo);
			if((tiros[i]==1)||(tiros[i]==2))
				cont+=(pulo=='S');
			else
				cont+=(pulo=='J');
		}
		scanf("%*c");
		printf("%d\n",cont);
	}
	return 0;
}
