// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fortune Teller
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2508

#include<stdio.h>
#include<string.h>

char func(char c)
{
	if(c >= 'a' && c <= 'z')
		return c - 'a' + 'A';
	if(c == ' ')
		return 'A' - 1;
	return c;
}

main()
{
	int i, N, M, x, y;
	char msg[101];
	while(gets(msg) != NULL)
	{
		x = 0;
		N = strlen(msg);
		for(i = 0; i < N; i++)
			x += (func(msg[i]) - 'A')%9 + 1;
		y = 0;
		while(x >= 10)
		{
			y = 0;
			while(x > 0)
			{
				y += x%10;
				x /= 10;
			}
			x = y;
		}
		printf("%d\n", x);
	}
	return 0;
}
