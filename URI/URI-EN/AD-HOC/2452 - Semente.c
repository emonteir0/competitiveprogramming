// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Semente
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2452

#include<stdio.h>

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int N, M, d = 0, x, xant;
	scanf("%d %d", &N, &M);
	scanf("%d", &xant);
	d = xant-1;
	while((M--) > 1)
	{
		scanf("%d", &x);
		d = max(d, (x-xant)/2);
		xant = x;
	}
	d = max(d, N-xant);
	printf("%d\n", d);
	return 0;
}

