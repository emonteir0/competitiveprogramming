// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Balé
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2400

#include<stdio.h>

int tree[100001];
int N;

void update(int i, int val)
{
	++i;
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

main()
{
	int i, x, s = 0;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d", &x);
		s += i - sum(x);
		update(x, 1);
	}
	printf("%d\n", s);
	return 0;
}
