// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Trapeziums
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1765

#include<stdio.h>

main()
{
	int n,i,a;
	double b,c;
	while(scanf("%d",&n)==1&&n>0)
	{
		for(i=1;i<=n;i++)
		{
			scanf("%d %lf %lf",&a,&b,&c);
			printf("Size #%d:\nIce Cream Used: %.2f cm2\n",i,2.5*a*(b+c));
		}
		printf("\n");
	}
	return 0;
}
