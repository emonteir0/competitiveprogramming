// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: It's Time to Duel!
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1896

#include<stdio.h>

int N, K, A0, B0, C0;
int vetA[20], vetB[20], vetC[20];

int solve(int a, int b, int Ai, int Bi, int Ci)
{
	if(a == N)
		return 0;
	return solve(a+1,1,Ai+vetA[a], Bi+vetB[a], Ci+vetC[a]) || solve(a+1,b, Ai, Bi, Ci) || ((Ai+vetA[a] == A0) && (Bi+vetB[a] == B0) && (Ci+vetC[a] == C0));
}

main()
{
	int A, B, C;
	scanf("%d %d %d %d", &K, &A0, &B0, &C0);
	while(K--)
	{
		scanf("%d %d %d", &A, &B, &C);
		if((A != A0) && (B != B0) && (C != C0))
		{
			vetA[N] = A;
			vetB[N] = B;
			vetC[N++] = C;
		}
	}
	printf("%s\n", solve(0, 0, 0, 0, 0)? "Y": "N");
	return 0;
}

