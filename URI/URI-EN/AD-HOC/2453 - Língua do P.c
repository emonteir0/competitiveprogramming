// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Língua do P
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2453

#include<stdio.h>
#include<string.h>

main()
{
	int i, tam;
	char mensagem[1001];
	gets(mensagem);
	tam = strlen(mensagem);
	for(i = 0; i < tam; i++)
	{
		if(mensagem[i] == 'p')
			printf("%c", mensagem[++i]);
		else
			printf("%c", mensagem[i]);
	}
	printf("\n");
	return 0;
}

