// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escalonamento Ótimo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2403

#include<bits/stdc++.h>

using namespace std;

int N, M;
vector<int> G[100001];
int depend[100001];
priority_queue<int> sources;


main()
{
	int i, j, u, v;
	vector<int> ans;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		depend[v]++;
	}
	for(i = 0; i < N; i++)
		if(depend[i] == 0)
			sources.push(-i);
	while(!sources.empty())
	{
		u = -sources.top();
		ans.push_back(u);
		sources.pop();
		depend[u]--;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			depend[v]--;
			if(depend[v] == 0)
				sources.push(-v);
		}
	}
	if(ans.size() < N)
		printf("*\n");
	else
		for(int i = 0; i < N; i++)
			printf("%d\n", ans[i]);
	return 0;
}
