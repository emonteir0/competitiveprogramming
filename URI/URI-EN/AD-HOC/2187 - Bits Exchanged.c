// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bits Exchanged
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2187

#include<stdio.h>
 
 
main()
{
	int n,i=1;
	scanf("%d",&n);
	while(1)
	{
		if(n==0)
		break;
		printf("Teste %d\n%d ",i,n/50);
		n=n%50;
		printf("%d ",n/10);
		n=n%10;
		printf("%d ",n/5);
		n=n%5;
		printf("%d\n\n",n);
		scanf("%d",&n);
		i++;
	}
	return 0;
}
