// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Goal Difference
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2191

#include<stdio.h>


main()
{
	int i, N, T = 1, soma, best, ini, inicio, fim, x, y;
	while((scanf("%d", &N) == 1) && N)
	{
		printf("Teste %d\n", T++);
		ini = 1;
		best = -1;
		soma = 0;
		for(i = 1; i <= N; i++)
		{
			scanf("%d %d", &x, &y);
			soma += x-y;
			if(soma < 0)
			{
				soma = 0;
				ini = i+1;
			}
			else if((soma > 0) && ((soma > best) || (soma == best && (i-ini) > (fim-inicio))))
			{
				best = soma;
				fim = i;
				inicio = ini;
			}
		}
		if(best == -1)
			printf("nenhum\n\n");
		else
			printf("%d %d\n\n", inicio, fim);
	}
	return 0;
}

