// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: D as in Daedalus
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2016

#include<stdio.h>

main()
{
	int a,b,c,i,x,y,cont,cont2=0,cont3;
	scanf("%d %d",&a,&b);
	while(b--)
	{
		scanf("%d %d",&c,&y);
		cont=y;
		for(i=1;i<a;i++)
		{
			scanf("%d",&x);
			cont+=x;
		}
		if(cont>c)
		{
			while((cont>c)&&(y>0))
			{
				cont-=y;
				y/=10;
				cont+=y;
			}
			if(cont<=c)
				cont2+=y;
		}
		else
		{
			cont3=-y;
			while((cont<=c)&&(y<10000))
			{
				cont-=y;
				y*=10;
				cont+=y;
			}
			if(cont<=c)
				cont3+=y;
			else
				cont3+=y/10;
			cont2+=cont3;
		}
	}
	printf("%d\n",cont2);
	return 0;
}
