// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Nagol
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2579

#include<stdio.h>

main()
{
	int A, B, C;
	scanf("%*d %d %d %d", &A, &B, &C);
	printf("%s\n", (A*B+C)&1 ? "Esquerda" : "Direita");
	return 0;
}
