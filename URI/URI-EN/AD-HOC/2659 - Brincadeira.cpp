// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brincadeira
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2659

#include<bits/stdc++.h>

using namespace std;

vector<int> indices[1000001];
int A[2000001];
int N, T, torneiras[30];
int valores[30];


int func(int x)
{
    int i, z = 0;
    for(i = 0; i < T; i++)
        z ^= (x & (valores[torneiras[i]])) == (valores[torneiras[i]]);
    x >>= 1;
    x |= z ? valores[N-1] : 0;
    return x;
}

int main()
{
    int i, A0, X, Y;
    scanf("%d %d %d %d %d", &N, &T, &A0, &X, &Y);
    for(i = 0; i < T; i++)
        scanf("%d", &torneiras[i]);
    valores[0] = 1;
    indices[0].push_back(0);
    for(i = 1; i < 30; i++)
        valores[i] = valores[i-1] << 1;
    A[1] = A0%X;
    indices[A[1]].push_back(1);
    if(A[1] == A[0] && Y == 1)
    	printf("0 0\n");
    else
    {
	    for(i = 2; ; i++) // Pode substituir esse 2000000 por 2*max(X, Y)
	    {
	        A0 = func(A0);
	        A[i] = (A[i-1] + A0) % X;
	        indices[A[i]].push_back(i);
	        if(indices[A[i]][0] + Y <= i)
	        {
	            printf("%d %d\n", indices[A[i]][0], i-1);
	            break;
	        }
	    }
	}
    return 0;
}
