// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Upset Fracil
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2005

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int vet[1001];

main()
{
	int N, M, x, cont, ma, mac;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		while(M--)
		{
			scanf("%d", &x);
			ma = 0;
			mac = 0;
			for(int i = 0; i < N; i++)
			{
				if(ma < vet[i])
					ma = vet[i];
				if(mac < vet[i] && x >= vet[i])
					mac = vet[i];
			}
			if(ma == 0)
				continue;
			if(mac != 0)
			{
				for(int i = 0; i < N; i++)
					if(mac == vet[i])
					{
						vet[i] = 0;
						break;
					}
			}
			else
			{
				for(int i = 0; i < N; i++)
					if(ma == vet[i])
					{
						vet[i] -= x;
						break;
					}
			}
		}
		cont = 0;
		for(int i = 0; i < N; i++)
			if(vet[i] == 0)
				cont++;
		printf("%d\n", cont);
	}
	return 0;
}
