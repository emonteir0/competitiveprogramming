// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Automaton Team
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2627

#include<stdio.h>

int max(int a, int b)
{
	return (a > b) ? a : b;
}

int acum[26];
char msg[100001];

main()
{
	int i, j, k, sz, ma;
	while(scanf("%d %d%*c", &sz, &k) == 2)
	{
		scanf("%s", msg);
		for(i = 0; i < 26; i++)
			acum[i] = 0;
		for(i = 0; i < sz; i++)
		{
			ma = 0;
			for(j = 0; j <= msg[i]-'a'; j++)
				ma = max(ma, acum[j]);
			acum[msg[i]-'a'] = ma + 26 - (msg[i]-'a');
		}
		ma = 0;
		for(i = 0; i < 26; i++)
			ma = max(ma, acum[i]);
		printf("%s\n", (ma >= k) ? "Aceita" : "Rejeita");
	}
	return 0;
}
