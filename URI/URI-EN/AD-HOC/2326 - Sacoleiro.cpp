// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sacoleiro
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2326

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N, T;

int preco[30];
char op[30];
int mat[30][30];
int vis[30][101][101];
int me = INF;

void dfs(int cidade, int menino, int menina)
{
	vis[cidade][menino][menina] = 1;
	if(menino || menina)
		me = min(me, abs(menino-menina));
	if(menino+menina+preco[cidade] <= T)
	{
		if(op[cidade] == 'A')
		{
			if(!vis[cidade][menino+preco[cidade]][menina])
				dfs(cidade, menino+preco[cidade], menina);
		}
		else
		{
			if(!vis[cidade][menino][menina+preco[cidade]])
				dfs(cidade, menino, menina+preco[cidade]);
		}
	}
	for(int i = 0; i < N; i++)
	{
		if(mat[cidade][i])
			if(!vis[i][menino][menina])
					dfs(i, menino, menina);
	}
}


int main()
{
	int x, y, M;
	scanf("%d %d%*c", &N, &T);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d %c %d%*c", &x, &preco[i], &op[i], &M);
		while(M--)
		{
			scanf("%d%*c", &y);
			mat[x][y] = 1;
		}
	}
	dfs(0, 0, 0);
	printf("%d\n", me);
	return 0;
}
