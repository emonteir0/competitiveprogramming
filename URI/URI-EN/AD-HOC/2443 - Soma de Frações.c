// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Soma de Frações
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2443

#include<stdio.h>

int gcd(int a, int b)
{
	if(b == 0)
		return a;
	return gcd(b, a%b);
}

main()
{
	int a, b, c, d, num, den, md;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	num = a*d + b*c;
	den = b*d;
	md = gcd(num, den);
	printf("%d %d\n", num/md, den/md);
	return 0;
}

