// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Martian
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1986

#include<bits/stdc++.h>

char palavra[101];

main()
{
	int N;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%x", &palavra[i]);
	palavra[N] = 0;
	printf("%s\n", palavra);
	return 0;
}
