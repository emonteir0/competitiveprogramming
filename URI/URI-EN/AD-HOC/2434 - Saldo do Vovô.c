// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Saldo do Vovô
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2434

#include<stdio.h>

int min(int a, int b)
{
	return (a < b) ? a : b;
}

main()
{
	int n, v, x, minimo;
	scanf("%d %d", &n, &v);
	minimo = v;
	while(n--)
	{
		scanf("%d", &x);
		v += x;
		minimo = min(v, minimo);
	}
	printf("%d\n", minimo);
	return 0;
}

