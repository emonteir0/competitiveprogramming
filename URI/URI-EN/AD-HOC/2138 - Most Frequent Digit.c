// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Most Frequent Digit
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2138

#include<stdio.h>
#include<string.h>

int digitos[10];
char palavra[1002];

main()
{
	int i, tam, max;
	while(scanf("%s",palavra) == 1)
	{
		tam = strlen(palavra);
		max = 0;
		for (i = 0; i < 10; i++)
			digitos[i] = 0;
		for (i = 0; i < tam; i++)
		{
			digitos[palavra[i]-'0']++;
			if (digitos[palavra[i] - '0'] > max)
				max = digitos[palavra[i] - '0'];
		}
		for (i = 9; i>= 0; i--)
		{
			if(digitos[i] == max)
			{
				printf("%d\n", i);
				break;
			}
		}
	}
	return 0;
}

