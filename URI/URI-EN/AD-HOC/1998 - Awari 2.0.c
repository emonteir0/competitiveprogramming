// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Awari 2.0
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1998

#include<stdio.h>
 
int vet[501];
 
main()
{
    int i,j,x,N;
    char b;
    while(scanf("%d",&N)==1&&(N!=-1))
    {
        b=1;
        for(i=1;i<=N;i++)
        {
            scanf("%d",&vet[i]);
            if(vet[i]>i)
                b=0;
        }
        for(i=N;i>=1;i--)
        {
            if(vet[i]%i==0)
            {
                x=vet[i]/i;
                for(j=1;j<i;j++)
                    vet[j]+=x;
            }
            else
            {
                b=0;
                break;
            }
        }
        printf("%s\n",b?"S":"N");
    }
    return 0;
}
