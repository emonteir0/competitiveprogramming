// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vivo ou Morto
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2301

#include<bits/stdc++.h>

using namespace std;

int vet[2][101];

main()
{
	int N, M, k, ind, t = 1, K, resp, x;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		printf("Teste %d\n", t++);
		for(int i = 1; i <= N; i++)
			scanf("%d", &vet[0][i]);
		for(int i = 0; i < M; i++)
		{
			scanf("%d %d", &K, &resp);
			ind = i&1;
			k = 1;
			for(int j = 1; j <= K; j++)
			{
				scanf("%d", &x);
				if(resp == x)
					vet[!ind][k++] = vet[ind][j];
			}
		}
		printf("%d\n\n", vet[M&1][1]);
	}
	return 0;
}
