// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pearls
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1975

#include<set>
#include<string>
#include<vector>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<map>

using namespace std;

typedef struct
{
	string str;
	int x;
} tipo;

set<string> conjunto,conjunto2;
vector<tipo> v;
map<string,bool> Mapa,Mapa2,Mapa3;

bool cmp(tipo a, tipo b)
{
	if(a.x!=b.x)
		return (a.x<b.x);
	return (strcmp(a.str.c_str(),b.str.c_str())>0);
}

main()
{
	int P,A,R,i,cont,tam,max;
	char vet[1001];
	string str;
	tipo t;
	while(scanf("%d %d %d%*c",&P,&A,&R)==3&&(P||A||R))
	{
		while(P--)
		{
			gets(vet);
			str=vet;
			conjunto.insert(str);
			Mapa2[str]=true;
		}
		while(A--)
		{
			cont=0;
			Mapa=Mapa2;
			gets(vet);
			str=vet;
			t.str=str;
			for(i=0;i<R;i++)
			{
				gets(vet);
				str=vet;
				if((conjunto.find(str)!=conjunto.end()))
				{
					cont+=Mapa[str];
					Mapa[str]=false;
				}
			}	
			t.x=cont;
			v.push_back(t);
		}
		tam=v.size()-1;
		sort(v.begin(),v.end(),cmp);
		max=v[tam].x;
		printf("%s",v[tam].str.c_str());
		for(i=tam-1;i>=0;i--)
		{
			if(v[i].x<max)
				break;
			if(strcmp(v[i+1].str.c_str(),v[i].str.c_str())!=0)
			printf(", %s",v[i].str.c_str());
		}
		printf("\n");
		v.clear();
		conjunto=conjunto2;
		Mapa=Mapa3;
		Mapa2=Mapa3;
	}
	return 0;
}

