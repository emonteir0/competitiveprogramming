// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Passa Bolinha
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2465

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x, y;
} posicao;

int mat[101][101];
char cor[101][101];
int dx[]={1,-1,0,0},dy[]={0,0,1,-1};

queue< posicao > fila;
posicao p, p2;

main()
{
	int i, j, k, N, cont = 0, x, y;
	scanf("%d %d %d", &N, &p.x, &p.y);
	for(i = 1; i <= N; i++)
		for(j = 1; j <= N; j++)
			scanf("%d", &mat[i][j]);
	fila.push(p);
	while(!fila.empty())
	{
		p = fila.front();
		fila.pop();
		x = p.x;
		y = p.y;
		if(!cor[x][y])
		{
			cor[x][y] = 1;
			cont++;
			for(k = 0; k < 4; k++)
			{
				p2.x = x+dx[k];
				p2.y = y+dy[k];
				if(p2.x > 0 && p2.x <= N && p2.y > 0 && p2.y <= N)
					if(mat[x][y] <= mat[p2.x][p2.y])
					fila.push(p2);
			}
		}
	}
	printf("%d\n", cont);
	return 0;
}

