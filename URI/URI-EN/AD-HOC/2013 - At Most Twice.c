// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: At Most Twice
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2013

#include<stdio.h>
#include<string.h>

main()
{
	int i, j;
	long long x;
	char str[20], cont[10];
	scanf("%lld", &x);
	sprintf(str, "%lld", x);
	while(1)
	{
		//printf("%s\n", str);
		for(i = 0; i <= 9; i++)
			cont[i] = 0;
		i = 0;
		while(str[i++] == '0');
		i--;
		for(; str[i]; i++)
		{
			cont[str[i]-'0']++;
			if(cont[str[i]-'0'] > 2)
			{
				str[i]--;
				j = i;
				if(str[i] < '0')
				{
					str[i--] = '9';
					while(1)
					{
						str[i]--;
						if(str[i] < '0')
							str[i--] = '9';
						else
							break;
					}
				}
				for(i = j+1; str[i]; i++)
					str[i] = '9';
				i = j;
				break;
			}
		}
		if(!str[i])
			break;
	}
	x = 0;
	for(i = 0; str[i]; i++)
		x = 10*x + str[i] - '0';
	printf("%lld\n", x);
	return 0;
}
