// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hailstone Sequences
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1441

#include<stdio.h>


int vet[15000];

int granizo(int n)
{
	int max=n;
	while(n!=1)
	{
		if(n%2==1)
		{
			n=3*n+1;
			if(n>max)
			{
				max+=n;
				n=max-n;
				max-=n;
			}
		}
		else
			n/=2;
	}
	return max;	
}

main()
{
	int n;
	vet[1]=-1;
	while((scanf("%d",&n)==1) && (n>0))
		printf("%d\n",granizo(n));
	return 0;
}
