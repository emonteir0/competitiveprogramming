// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help the Federation
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1588

#include<cstdio>
#include<map>
#include<string>
#include<algorithm>

using namespace std;

typedef struct
{
	int gols,pontos,vitorias,i;
	string nome;
} tipo;

map <string, tipo> Mapa, Mapa2;
map <string, tipo>::iterator it;

tipo vet[100],vazio;

bool cmp(tipo a, tipo b)
{
	if(a.pontos!=b.pontos)
		return a.pontos>b.pontos;
	if(a.vitorias!=b.vitorias)
		return a.vitorias>b.vitorias;
	if(a.gols!=b.gols)
		return a.gols>b.gols;
	return a.i<b.i;
}

main()
{
	int i,k,N,M,x,y;
	char nome[21],nome2[21];
	string str,str2;
	scanf("%d%*c",&k);
	while(k--)
	{
		scanf("%d %d%*c",&N,&M);
		for(i=0;i<N;i++)
		{
			scanf("%s%*c",nome);
			str=nome;
			vazio.nome=nome;
			vazio.i=i;
			Mapa[nome]=vazio;
		}
		while(M--)
		{
			scanf("%d %s %d %s%*c",&x,nome,&y,nome2);
			str=nome;
			str2=nome2;
			Mapa[str].gols+=x;
			Mapa[str2].gols+=y;
			if(x>y)
			{
				Mapa[str].pontos+=3;
				Mapa[str].vitorias++;
			}
			else if(x<y)
			{
				Mapa[str2].pontos+=3;
				Mapa[str2].vitorias++;
			}
			else
			{
				Mapa[str].pontos++;
				Mapa[str2].pontos++;
			}
		}
		for(i=0,it=Mapa.begin();it!=Mapa.end();++it)
		{
			vet[i]=it->second;
			i++;
		}
		sort(vet,vet+N,cmp);
		for(i=0;i<N;i++)
		{
			printf("%s\n",vet[i].nome.c_str());
		}
		Mapa=Mapa2;
	}
	return 0;
}

