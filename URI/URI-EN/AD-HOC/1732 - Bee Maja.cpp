// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bee Maja
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1732

#include<bits/stdc++.h>

using namespace std;

int X[100001], Y[100001];

main()
{
	int a, b, x, t = 1, k = 3;
	X[1] = 0;
	Y[1] = 0;
	X[2] = a = 0;
	Y[2] = b = 1;
	while(k <= 100000)
	{
		for(int i = 0; i < t && k <= 100000; i++)
		{
			X[k] = --a; 
			Y[k++] = b;
		}
		for(int i = 0; i < t && k <= 100000; i++)
		{
			X[k] = a; 
			Y[k++] = --b;
		}
		for(int i = 0; i < t && k <= 100000; i++)
		{
			X[k] = ++a; 
			Y[k++] = --b;
		}
		for(int i = 0; i < t && k <= 100000; i++)
		{
			X[k] = ++a; 
			Y[k++] = b;
		}
		t++;
		for(int i = 0; i < t && k <= 100000; i++)
		{
			X[k] = a; 
			Y[k++] = ++b;
		}
		for(int i = 0; i < t-1 && k <= 100000; i++)
		{
			X[k] = --a; 
			Y[k++] = ++b;
		}
	}
	while(scanf("%d", &x) == 1)
		printf("%d %d\n", X[x], Y[x]);
	return 0;
}
