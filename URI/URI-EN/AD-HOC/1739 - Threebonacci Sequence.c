// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Threebonacci Sequence
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1739

#include<stdio.h>
#define ll long long int


ll treb[61];
int k=0;

void threebonacci()
{
	ll a=1,b=1,x;
	int r3,r10;
	while(k!=60)
	{
		x=b;
		r3=0;
		while(x!=0)
		{
			r10=x%10;
			if(r10==3)
			{
				treb[k++]=b;
				break;
			}
			r3=(r10%3+r3)%3;
			x/=10;
		}
		if((r3==0)&&(r10!=3))
			treb[k++]=b;
		b=b+a;
		a=b-a;
	}
}


main()
{
	int x;
	threebonacci();
	while(scanf("%d",&x)==1)	printf("%lld\n",treb[x-1]);
	return 0;
}
