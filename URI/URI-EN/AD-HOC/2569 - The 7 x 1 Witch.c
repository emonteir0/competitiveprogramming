// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The 7 x 1 Witch
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2569

#include<stdio.h>

int func(int a)
{
	int vet[10], i, k = 0;
	while(a > 0)
	{
		vet[k++] = ((a % 10) == 7) ? 0 : a % 10;
		a /= 10;
	}
	for(i = k-1; i >= 0; i--)
		a = 10 * a + vet[i];
	return a;
}

main()
{
	int a, b;
	char op;
	scanf("%d %c %d", &a, &op, &b);
	printf("%d\n", (op == 'x') ? func(func(a)*func(b)) : func(func(a)+func(b)));
	return 0;
}
