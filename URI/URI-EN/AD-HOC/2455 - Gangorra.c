// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Gangorra
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2455

#include<stdio.h>

main()
{
	int a, b, c, d, v1, v2;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	v1 = a*b;
	v2 = c*d;
	if(v1 == v2)
		printf("0\n");
	else
		printf("%d\n", 2*(v1<v2)-1);
	return 0;
}

