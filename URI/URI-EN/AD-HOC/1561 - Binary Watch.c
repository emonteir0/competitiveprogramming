// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Watch
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1561

#include<stdio.h>


main()
{
	int h,min;
	while(scanf("%d%*c%d",&h,&min)==2)
	{
		printf(" ____________________________________________\n");
		printf("|                                            |\n");
		printf("|    ____________________________________    |_\n");
		printf("|   |                                    |   |_)\n");
		printf("|   |   8         4         2         1  |   |\n");
		printf("|   |                                    |   |\n");
		printf("|   |   %s         %s         %s         %s  |   |\n",h&8?"o":" ",h&4?"o":" ",h&2?"o":" ",h&1?"o":" ");
		printf("|   |                                    |   |\n");
		printf("|   |                                    |   |\n");
		printf("|   |   %s     %s     %s     %s     %s     %s  |   |\n",min&32?"o":" ",min&16?"o":" ",min&8?"o":" ",min&4?"o":" ",min&2?"o":" ",min&1?"o":" ");
		printf("|   |                                    |   |\n");
		printf("|   |   32    16    8     4     2     1  |   |_\n");
		printf("|   |____________________________________|   |_)\n");
		printf("|                                            |\n");
		printf("|____________________________________________|\n\n");
	}
	return 0;
}

