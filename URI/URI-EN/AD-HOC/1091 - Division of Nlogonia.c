// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Division of Nlogonia
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1091

#include<stdio.h>

main()
{
	int x0,y0,x,y,n;
	while(scanf("%d",&n)==1&&n)
	{
		scanf("%d %d",&x0,&y0);
		while(n--)
		{
			scanf("%d %d",&x,&y);
			x-=x0;
			y-=y0;
			if(x>0&&y>0)		printf("NE\n");
			else if(x<0&&y>0)	printf("NO\n");
			else if(x<0&&y<0)	printf("SO\n");
			else if(x>0&&y<0)	printf("SE\n");
			else 				printf("divisa\n");
		}
	}
	return 0;
}
