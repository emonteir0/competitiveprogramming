// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Parking Lot
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1246

#include<bits/stdc++.h>

using namespace std;

map< int, pair<int, int> > carros, carros2;
set< pair<int, int> > vagas, vagas2;
set< pair<int, int> >::iterator it;
map<int, int> inicio, fim, vazio;

main()
{
	char op;
	int N, M, id, sz, cont;
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		cont = 0;
		carros = carros2;
		vagas = vagas2;
		inicio = fim = vazio;
		vagas.insert(make_pair(0, N));
		inicio[N] = 0;
		fim[0] = N;
		while(M--)
		{
			scanf("%c %d%*c", &op, &id);
			if(op == 'C')
			{
				scanf("%d%*c", &sz);
				for(it = vagas.begin(); it != vagas.end(); it++)
				{
					if(it->second - it->first > sz)
					{
						pair<int, int> p = *it;
						inicio.erase(p.second);
						fim.erase(p.first);
						carros[id] = make_pair(p.first, p.first+sz);
						inicio[p.second] = p.first+sz;
						fim[p.first+sz] = p.second;
						vagas.erase(p);
						vagas.insert(make_pair(p.first+sz, p.second));
						cont++;
						break;
					}
					if(it->second - it->first == sz)
					{
						pair<int, int> p = *it;
						carros[id] = make_pair(p.first, p.second);
						inicio.erase(p.second);
						fim.erase(p.first);
						vagas.erase(p);
						cont++;
						break;
					}
				}
			}
			else
			{
				pair<int, int> p = carros[id];
				int fi = p.first, se = p.second;
				carros.erase(id);
				if(inicio.count(fi) > 0)
				{
					p.first = inicio[fi];
					vagas.erase(make_pair(p.first, fi));
					inicio.erase(fi);
				}
				if(fim.count(se) > 0)
				{
					p.second = fim[se];
					vagas.erase(make_pair(se, p.second));
					fim.erase(se);
				}
				inicio[p.second] = p.first;
				fim[p.first] = p.second;
				vagas.insert(make_pair(p.first, p.second));
			}
		}
		printf("%d\n", 10*cont);
	}
	return 0;
}
