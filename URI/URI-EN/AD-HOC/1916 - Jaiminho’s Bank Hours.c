// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jaiminho’s Bank Hours
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1916

#include<stdio.h>

int absol(int x)
{
	if(x < 0)
		return -x;
	return x;
}

int difft(int a, int b)
{
	if(absol(a-b) <= 5)
		return 0;
	return b-a;
}

main()
{
	int T, N, H1, M1, H2, M2, H3, M3, H4, M4, acum;
	scanf("%d", &T);
	while(T--)
	{
		acum = 0;
		scanf("%d", &N);
		while(N--)
		{
			scanf("%d:%d %d:%d | %d:%d %d:%d", &H1, &M1, &H2, &M2, &H3, &M3, &H4, &M4);
			acum += difft(H1*60+M1, 480) + difft(720, H2*60+M2) + difft(H3*60+M3, 840) + difft(1080, H4*60+M4);
		}
		printf("%s ", ((acum>=0) ? "+" : "-"));
		acum = absol(acum);
		printf("%02d:%02d\n", acum/60, acum%60);
	}
	return 0;
}

