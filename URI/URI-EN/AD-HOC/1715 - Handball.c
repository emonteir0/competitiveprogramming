// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Handball
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1715

#include<stdio.h>

main()
{
	int i,N,M,cont=0,x,g;
	scanf("%d %d",&N,&M);
	while(N--)
	{
		x=1;
		for(i=0;i<M;i++)
		{
			scanf("%d",&g);
			if(g==0)
			x=0;
		}
		cont+=x;
	}
	printf("%d\n",cont);
	return 0;
}
