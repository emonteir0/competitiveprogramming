// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Golnaldinho
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2792

#include<stdio.h>

int tree[1000001];
int N;

void update(int i, int val)
{
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}


int busca(int x)
{
	int lo = 1, hi = N, mid, y, ans;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		y = sum(mid);
		if(y >= x)
		{
			if(y == x)
				ans = mid;
			hi = mid-1;
		}
		else
		{
			lo = mid+1;
		}
	}
	return ans;
}

main()
{
	int i, x, y;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
		update(i, 1);
	for(i = 1; i <= N; i++)
	{
		scanf("%d", &x);
		if(i > 1)
			printf(" ");
		printf("%d", sum(x));
		update(x, -1);
	}
	printf("\n");
	return 0;
}

