// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: In Braille
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1357

#include<stdio.h>
#include<string.h>

int mapa[64];
int vet2[100];

void printa(char x,int y)
{
	y=2-y;
	int c=1<<(2*y+1),d=1<<(2*y);
	printf("%c%c",((c&mapa[x-'0'])==c)?'*':'.',((d&mapa[x-'0'])==d)?'*':'.');
}


main()
{
	int n,x,i,j;
	char op,ch1,ch2,a[101];
	mapa[32]=1;
	mapa[40]=2;
	mapa[48]=3;
	mapa[52]=4;
	mapa[36]=5;
	mapa[56]=6;
	mapa[60]=7;
	mapa[44]=8;
	mapa[24]=9;
	mapa[28]=0;
	mapa[1]=32;
	mapa[2]=40;
	mapa[3]=48;
	mapa[4]=52;
	mapa[5]=36;
	mapa[6]=56;
	mapa[7]=60;
	mapa[8]=44;
	mapa[9]=24;
	mapa[0]=28;
	while(scanf("%d%*c",&n)==1&&n>0)
	{
		scanf("%c%*c",&op);
		if(op=='S')
		{
			gets(a);
			for(j=0;j<3;j++)
			{
				for(i=0;i<n;i++)
				{
					if(i>0)
						printf(" ");
					printa(a[i],j);
				}
				printf("\n");
			}
		}
		if(op=='B')
		{
			for(i=0;i<n;i++)
			{
				vet2[i]=0;
			}
			for(j=2;j>=0;j--)
			{
				for(i=0;i<n;i++)
				{
					scanf("%c%c%*c",&ch1,&ch2);
					vet2[i]+=(ch1=='*')*(1<<(2*j+1))+(ch2=='*')*(1<<(2*j));
				}
			}
			for(i=0;i<n;i++)
				printf("%d",mapa[vet2[i]]);
			printf("\n");
		}
	}
	return 0;
}
