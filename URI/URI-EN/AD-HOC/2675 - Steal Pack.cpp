// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Steal Pack
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2675

#include<bits/stdc++.h>
#define ll long long
using namespace std;

stack<int> fila, fila2;

main()
{
	int N, x, i;
	ll s;
	while(scanf("%d", &N) == 1)
	{
		s = 0;
		fila = fila2;
		while(N--)
		{
			scanf("%d", &x);
			if(!fila.empty())
			{
				while(fila.top() > x)
				{
					s += fila.top();
					fila.pop();
					if(fila.empty())
						break;
				}
			}
			fila.push(x);
		}
		printf("%lld\n", s);
	}
	return 0;
}
