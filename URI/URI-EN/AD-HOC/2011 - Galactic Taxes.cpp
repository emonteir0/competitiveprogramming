// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Galactic Taxes
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2011

#include<bits/stdc++.h>
#define INF 1e18

using namespace std;

int N;
vector< vector<int> > G(1001), A(1001), B(1001);

double dist[1001];

double dijkstra(double t)
{
	int u, v, i;
	double w0, w;
	priority_queue< pair<double, int> > pq;
	for(i = 1; i <= N; i++)
		dist[i] = INF;
	dist[1] = 0;
	pq.push(make_pair(0, 1));
	while(!pq.empty())
	{
		w0 = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(w0 > dist[u])
			continue;
		if(u == N)
			break;
		for(i = 0; i < (int)G[u].size(); i++)
		{
			w = t*A[u][i] + B[u][i];
			v = G[u][i];
			if(dist[v] > w0 + w)
			{
				dist[v] = w0 + w;
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
	return dist[N];
}

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}


main()
{
	int i, M, u, v, a, b;
	double lo = 0, hi = 24*60, lt, rt;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d %d %d", &u, &v, &a, &b);
		G[u].push_back(v);
		G[v].push_back(u);
		A[u].push_back(a);
		A[v].push_back(a);
		B[u].push_back(b);
		B[v].push_back(b);
	}
	for(i = 0; i < 80; i++)
	{
		lt = lo + (hi-lo)/3;
		rt = hi - (hi-lo)/3;
		if(dijkstra(lt) > dijkstra(rt))
			hi = rt;
		else
			lo = lt;
	}
	printf("%.5lf\n", dijkstra(lo));
	return 0;
}
