// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Exchanging Cards
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1104

#include<cstdio>
#include<set>

using namespace std;

int main(){
	int n,n2,x,s;
	set <int> conj,conj2;
	set <int>::iterator it;
	while(scanf("%d %d",&n,&n2)==2&&(n||n2))
	{
		conj.clear();
		conj2.clear();
		while(n--)
		{
			scanf("%d",&x);
			conj.insert(x);
		}
		n=conj.size();
		s=0;
		while(n2--)
		{
			scanf("%d",&x);
			it=conj.find(x);
			if(it!=conj.end())
			{
				s++;
				conj.erase(it);
			}
			if(conj2.find(x)==conj2.end())
				conj2.insert(x);
		}
		n2=conj2.size();
		printf("%d\n",((n<n2)?n:n2)-s);
	}
	return 0;	
}
