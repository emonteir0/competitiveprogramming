// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bomba
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2426

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int dist[1500];
vector<int> G[1500];

int prox(int n)
{
	return n == 2 ? 0 : n+1;
}

int val(int x, int y)
{
	return 3*x+y;
}

main()
{
	queue<int> fila;
	int N, E, S, M, u, v, w;
	scanf("%d %d %d %d", &N, &E, &S, &M);
	for(int i = 0; i < 3*N; i++)
		dist[i] = INF;
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		if(w)
			G[val(u, 0)].push_back(val(v, 1));
		else
		{
			G[val(u, 1)].push_back(val(v, 2));
			G[val(u, 2)].push_back(val(v, 0));
		}
	}
	dist[val(E, 0)] = 0;
	fila.push(val(E, 0));
	while(!fila.empty())
	{
		u = fila.front();
		if(u/3 == S)
			break;
		fila.pop();
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u] + 1)
			{
				dist[v] = dist[u] + 1;
				fila.push(v);
			}
		}
	}
	if(fila.empty())
		printf("*\n");
	else
		printf("%d\n", dist[u]);
	return 0;
}
