// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tustin and His New Die
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2600

#include<stdio.h>

int vet[6];

main()
{
	int i, ans, x;
	scanf("%*d%*c");
	while(scanf("%d %d %d %d %d %d", &vet[0], &vet[2], &vet[1], &vet[3], &vet[4], &vet[5]) != EOF)
	{
		ans = 1;
		for(i = 0; i < 3; i++)
			ans &= vet[i] + vet[5-i] == 7;
		x = 0;
		for(i = 0; i < 6; i++)
			if(vet[i] < 6)
				x |= (1 << vet[i]);
		ans &= x == 62;
		printf("%s\n", ans ? "SIM" : "NAO");
	}
	return 0;
}
