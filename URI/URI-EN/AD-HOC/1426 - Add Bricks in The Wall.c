// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Add Bricks in The Wall
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1426

#include<stdio.h>

int mat[10][10];

main()
{
	int i,j,n;
	scanf("%d",&n);
	while(n--)
	{
		for(i=1;i<=9;i+=2)
			for(j=1;j<=i;j+=2)
				scanf("%d",&mat[i][j]);
		for(i=1;i<=8;i++)
		{
			if(i%2==1)
				for(j=1;j<=i;j+=2)
					mat[i+2][j+1] = (mat[i][j]-mat[i+2][j]-mat[i+2][j+2])/2;
			else
				for(j=1;j<=i;j++)
					mat[i][j] = mat[i+1][j] + mat[i+1][j+1];
		}
		for(i=1;i<=9;i++)
		{
			printf("%d",mat[i][1]);
			for(j=2;j<=i;j++)
				printf(" %d",mat[i][j]);
			printf("\n");
		}
	}
	return 0;
}

