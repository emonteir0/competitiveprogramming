// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Olympic Games
// Level: 7
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2244

#include<bits/stdc++.h>
#define EPS 1e-5
#define INF 1e70

using namespace std;

struct line
{
	int a, b, ind;
	line(){}
	line(int a, int b, int ind)
	{
		this->a = a;
		this->b = b;
		this->ind = ind;
	}
};

struct tempo
{
	long double st, en;
	tempo(){}
	tempo(long double st, long double en)
	{
		this->st = st;
		this->en = en;
	}
};

int N;
vector<line> H, C;
tempo tH[100001], tC[100001];

bool cmp(line l1, line l2)
{
	if(l1.a != l2.a)
		return l1.a < l2.a;
	return l1.b > l2.b;
}

long double intersect(line l1, line l2)
{
	return ((double)(l2.b-l1.b))/(l1.a-l2.a);
}

void convex_hull(vector<line>& lines, int ind)
{
	tempo* tempos;
	long double t;
	vector<line> retas;
	line aux, p;
	tempos = ind ? tC : tH;
	retas.push_back(lines[0]);
	tempos[lines[0].ind].st = -INF;
	tempos[lines[0].ind].en = INF;
	for(int i = 1; i < N; i++)
	{
		p = lines[i];
		if(p.a == retas.back().a)
		{
			tempos[p.ind].st = tempos[p.ind].en = -INF;
			continue;
		}
		while(retas.size() > 1 && intersect(p, retas[retas.size()-1]) <= intersect(p, retas[retas.size()-2]))
		{
			aux = retas.back();
			tempos[aux.ind].st = tempos[aux.ind].en = -INF;
			retas.pop_back();
		}
		t = intersect(p, retas.back());
		tempos[retas.back().ind].en = t;
		retas.push_back(p);
		tempos[p.ind].st = t;
		tempos[p.ind].en = INF;
	}
}

int valido(int ind)
{
	if(tH[ind].st >= tH[ind].en)
		return 0;
	if(tH[ind].en <= 0)
		return 0;
	if(tC[ind].st >= tC[ind].en)
		return 0;
	if(tC[ind].en <= 0)
		return 0;
	return ((tH[ind].st >= tC[ind].st) && (tH[ind].st < tC[ind].en)) ||
		   ((tH[ind].en > tC[ind].st) && (tH[ind].en <= tC[ind].en)) ||
		   ((tC[ind].st >= tH[ind].st) && (tC[ind].st < tH[ind].en)) ||
		   ((tC[ind].en > tH[ind].st) && (tC[ind].en <= tH[ind].en));
}

main()
{
	int x, y, cont = 0;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d", &x, &y);
		H.push_back(line(y, x, i));
		tH[i] = tempo(-INF, -INF);
		scanf("%d %d", &x, &y);
		C.push_back(line(-y, -x, i));
		tC[i] = tempo(-INF, -INF);
	}
	sort(H.begin(), H.end(), cmp);
	sort(C.begin(), C.end(), cmp);
	convex_hull(H, 0);
	convex_hull(C, 1);
	for(int i = 0; i < N; i++)
		cont += valido(i);
	printf("%d\n", cont);
	return 0;
}
