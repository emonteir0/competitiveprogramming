// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cash Roial
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2595

#include<stdio.h>

char mat[1002][1002];

main()
{
	int i, j, T, N, M, C, x, y, cnt;
	scanf("%d%*c", &T);
	while(T--)
	{
		scanf("%d %d %d%*c", &N, &M, &C);
		for(i = 1; i <= N; i++)
		{
			for(j = 1; j <= M; j++)
				scanf("%c", &mat[i][j]);
			scanf("%*c");
		}
		while(C--)
		{
			scanf("%d %d%*c", &x, &y);
			cnt = 0;
			for(i = x-1; i <= x+1; i++)
				for(j = y-1; j <= y+1; j++)
					if(i >= 1 && i <= N && j >= 1 && j <= M && !((i == x) && (j == y)))
						cnt += mat[i][j] == 'T';
			printf("%s\n", cnt >= 5 ? "GRRR" : "GG IZI");
		}
	}
	return 0;
}
