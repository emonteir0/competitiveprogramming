// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shuffling Again
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1981

#define MAX 10000
#define MOD 100000007
#define ll long long int
#include<stdio.h>
#include<string.h>

int val[26];
ll fatoriais[10001],invfat[10001];
   
void fat()
{
	int i;
	fatoriais[0]=1;
	for(i=1;i<=10000;i++)
		fatoriais[i]=(i*fatoriais[i-1])%MOD;
}

ll mdc(ll  a, ll b, ll *x, ll *y) {
  ll xx, yy, d;
  if(b==0)
  {
    *x=1; *y=0;
    return a;
  }
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}

ll inv(ll a)
{
	ll x,y,d;
	if(invfat[a]!=0)
		return invfat[a];
	d = mdc(fatoriais[a],MOD,&x,&y);
	if(x<0)
		x = x+MOD;
	invfat[a]=x;
	return x;
}
  
main()
{
    char vet[MAX+1];
    int i,j,n,c,l,z;           
    ll x;
    fat();
    invfat[0]=1;
    invfat[1]=1;
    while(scanf("%s",vet)==1&&strcmp(vet,"0")!=0)
    {
        for(i=0;i<26;i++)
            val[i]=0;
        n=strlen(vet);
        for(i=0;i<n;i++)
            val[vet[i]-'a']++;
        x=fatoriais[n];
        for(j=0;j<26;j++)
        	x=(x*inv(val[j]))%MOD;
        printf("%llu\n",x);
    }
    return 0;
}
