// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Coffee Harvest
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2871

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N, M, x, ac;
	while(scanf("%d %d", &N, &M) != EOF)
	{
		ac = 0;
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				scanf("%d", &x);
				ac += x;
			}
		printf("%d saca(s) e %d litro(s)\n", ac/60, ac%60);
	}
	return 0;
}
