// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Copa do Mundo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2376

#include<stdio.h>

main()
{
	int i, a, b,vet[8], vet2[4], vet3[2];
	for(i = 0; i < 8; i++)
	{
		scanf("%d %d", &a, &b);
		vet[i] = (a > b)? 2*i : 2*i+1;
	}
	for(i = 0; i < 4; i++)
	{
		scanf("%d %d", &a, &b);
		vet2[i] = (a > b)? vet[2*i] : vet[2*i+1];
	}
	for(i = 0; i < 2; i++)
	{
		scanf("%d %d", &a, &b);
		vet3[i] = (a > b)? vet2[2*i] : vet2[2*i+1];
	}
	scanf("%d %d", &a, &b);
	printf("%c\n", 'A' + ((a > b) ? vet3[0] : vet3[1]));
	return 0;
}
