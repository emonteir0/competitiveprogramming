// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Line-up
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2572

#include<stdio.h>
#define MAXN 200001
#define ll long long
#define MOD 1000000007

int N, M[MAXN][18], logo[MAXN], expo[18], A[MAXN];
int pares[101][2];

void logmap()
{
	int i, x = 1, k = 0;
	for(i = 1; i <= MAXN; i++)
	{
		if(i == x << 1)
		{
			k++;
			x <<= 1;
		}
		logo[i] = k;
	}
}

void expmap()
{
	int i, x = 1;
	for(i = 0; i < 18; i++)
	{
		expo[i] = x;
		x <<= 1;
	}
}

void build()
{
  int i, j, sz;
  for (i = 0; i < N; i++)
      M[i][0] = i;
  sz = logo[N];
  for (j = 1; j <= sz; j++)
      for (i = 0; i + expo[j-1] < N; i++)
          if (A[M[i][j - 1]] > A[M[i + expo[j-1]][j - 1]])
              M[i][j] = M[i][j - 1];
          else
              M[i][j] = M[i + expo[j-1]][j - 1];
}  
 
int rmq(int x, int y)
{
	int k = logo[y-x+1];
	return A[M[x][k]] > A[M[y-expo[k]+1][k]] ? M[x][k] : M[y-expo[k]+1][k];
}

main()
{
	int i, j, K, R, x, y, k, ind, ind2;
	int m;
	char bol;
	logmap();
	expmap();
	scanf("%d %d %d", &N, &K, &R);
	for(i = 0; i < N; i++)
		scanf("%d", &A[i]);
	build();
	while(R--)
	{
		scanf("%d %d", &x, &y);
		x--;
		y--;
		m = 1;
		k = 0;
		bol = 1;
		pares[k][0] = x;
		pares[k++][1] = y;
		for(j = 0; j < K; j++)
		{
			ind = 0;
			for(i = 1; i < k; i++)
			{
				if(pares[i][0] <= pares[i][1])
				{
					if(pares[ind][0] <= pares[ind][1])
					{
						if(A[rmq(pares[i][0], pares[i][1])] > A[rmq(pares[ind][0], pares[ind][1])])
							ind = i;
					}
					else
						ind = i;
				}
			}
			if(pares[ind][0] <= pares[ind][1])
			{
				ind2 = rmq(pares[ind][0], pares[ind][1]);
				if(A[ind2])
				{
					m = ((ll)m * A[ind2]) % MOD;
					bol = 0;
				}
				else
				{
					if(bol)
					{
						m = 0;
						break;
					}
				}
				if(ind2 != pares[ind][1])
				{
					pares[k][0] = ind2+1;
					pares[k++][1] = pares[ind][1];
	 			}
				pares[ind][1] = ind2 - 1;
			}
		}
		printf("%d\n", m);
	}
	return 0;
}
