// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Divisibility by 3
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1987

#include<stdio.h>
#include<string.h>

main()
{
	int i,n,cont;
	char vet[11];
	while(scanf("%d %s",&n,vet)==2)
	{
		cont=0;
		n=strlen(vet);
		for(i=0;i<n;i++)
			cont+=vet[i]-'0';
		printf("%d %s\n",cont,((cont%3)==0)?"sim":"nao");
	}
	return 0;
}
