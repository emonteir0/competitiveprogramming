// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shuffle
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1770

#include<cstdio>
#include<set>

using namespace std;

set <int> conj1,conj2;

main()
{
	int M,N,i,musicas[100],acum,x;
	char u;
	while(scanf("%d %d",&M,&N)==2)
	{
		u=0;
		acum=0;
		for(i=0;i<M;i++)
		{
			scanf("%d",&musicas[i]);
		}
		for(i=0;i<N;i++)
		{
			if(u==0)
			{
				scanf("%d",&x);
				acum+=musicas[x-1];
				conj1.insert(x);
				if(conj1.size()==M)
				{
					u=1;
				}
			}
			else
			{
				scanf("%d",&x);
			}
		}
			printf("%d\n",u?acum:-1);
			conj1=conj2;
	}
	return 0;
}
