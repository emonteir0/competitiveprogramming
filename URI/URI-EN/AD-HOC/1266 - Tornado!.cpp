// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tornado!
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1266

#include<bits/stdc++.h>

using namespace std;

int vet[5001];

main()
{
	int N, x, k, i, f, cont;
	while(scanf("%d", &N) == 1 && N)
	{
		f = 1;
		for(k = 0, i = 0; i < N; i++)
		{
			scanf("%d", &x);
			if(x == 1)
			{
				f = 0;
				vet[k++] = 1;
			}
			else if(x == 0 && f)
				vet[N-1-i] = 0;
			else
				vet[k++] = 0;
		}
		if(vet[0] == 0)
			printf("%d\n", (N+1)/2);
		else
		{
			cont = 0;
			x = 0;
			vet[N] = 1;
			for(i = 1; i <= N; i++)
			{
				if(vet[i] == 1)
				{
					cont += (i-x-1)/2;
					x = i;
				}
			}
			printf("%d\n", cont);
		}
	}
	return 0;
}
