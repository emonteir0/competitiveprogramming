// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Throwing Balls
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1532

#include<stdio.h>


main()
{
	int A,B,i,j,k,acum,cont;
	char u;
	while(scanf("%d %d",&A,&B)==2&&(A||B))
	{
		if(B>=A)
			printf("possivel\n");
		else
		{
			u=0;
			for(i=1;i<=B;i++)
			{
				k=i;
				acum=0;
				cont=0;
				while(1)
				{
					acum+=k;
					cont++;
					if(acum==A)
					{
						u=1;
						i=B;
						break;
					}
					if(cont==k)
					{
						cont=0;
						k--;
					}
					if(k==0)
						break;					
				}
			}
			printf("%s\n",u==1?"possivel":"impossivel");
		}
	}
	return 0;
}
