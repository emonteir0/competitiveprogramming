// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: What is the Fastest?
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2175

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int i;
	double x;
} pessoa;

pessoa vet[3];
vector<string> nomes;

bool cmp(pessoa a, pessoa b)
{
	return a.x < b.x;
}

main()
{
	nomes.push_back("Otavio");
	nomes.push_back("Bruno");
	nomes.push_back("Ian");
	vet[0].i = 0;
	vet[1].i = 1;
	vet[2].i = 2;
	scanf("%f %f %f", &vet[0].x, &vet[1].x, &vet[2].x);
	sort(vet,vet+3,cmp);
	if(vet[0].x == vet[1].x)
		printf("Empate\n");
	else
		printf("%s\n", nomes[vet[0].i].c_str());
	return 0;
}

