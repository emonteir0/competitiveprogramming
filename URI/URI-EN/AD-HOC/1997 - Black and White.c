// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Black and White
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1997

#include<stdio.h>
#include<string.h>

main()
{
	int i,cont;
	char vet[501],vet2[501],a;
	while((scanf("%s %s%*c",vet,vet2)==2)&&(vet[0]!='*'))
	{
		cont=0;
		a=1;
		for(i=0;vet[i]!=0;i++)
		{
			if(vet[i]!=vet2[i])
			{
				cont+=a;
				a=0;
			}
			else
				a=1;
		}
		printf("%d\n",cont);
	}
	return 0;
}

