// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: CEI's Reopening
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2556

#include<bits/stdc++.h>

using namespace std;

int vet[1001];

main()
{
	int N;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		printf("%d %d\n", N/2, vet[N/2] - vet[N/2-1]);
	}
}
