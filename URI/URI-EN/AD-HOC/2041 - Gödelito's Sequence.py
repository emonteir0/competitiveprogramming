# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Gödelito's Sequence
# Level: 2
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2041

a=['3']

for i in range(1,40):
	a.append('')
	cant = a[i-1][0]
	cont = 0
	for c in a[i-1]:
		if c == cant:
			cont += 1
		else:
			a[i] += str(cont) + str(cant)
			cont = 1
		cant = c
	a[i] += str(cont) + str(cant)

while True:
	try:
		x = input()
		print a[x-1]
	except:
		break
