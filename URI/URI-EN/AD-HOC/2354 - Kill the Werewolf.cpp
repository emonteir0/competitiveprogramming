// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kill the Werewolf
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2354

#include <bits/stdc++.h>

using namespace std;

struct edge { int u, v, rev, cap, f; };

const int MAXN = 102, INF = 0x3f3f3f3f;
vector<edge> adj[MAXN];

void add_edge(int u, int v, int cap) 
{
	adj[u].push_back((edge){u, v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){v, u, (int)adj[u].size() - 1, 0, 0});
}

int edmonds_karp(int nodes, int source, int sink) 
{
	int max_flow = 0;
	for (;;)
	{
		vector<edge*> pred(nodes, (edge*)0);
		queue<int> q;
		q.push(source);
		while (!q.empty() && !pred[sink]) 
		{
			int u = q.front();
			q.pop();
			for (int j = 0; j < (int)adj[u].size(); j++) 
			{
				edge &e = adj[u][j];
				if (!pred[e.v] && e.cap > e.f)
				{
					pred[e.v] = &e;
					q.push(e.v);
				}
			}
		}
		if (!pred[sink]) 
		{
			break;
		}
		int flow = INF;
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			flow = min(flow, pred[u]->cap - pred[u]->f);
		}
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			pred[u]->f += flow;
			adj[pred[u]->v][pred[u]->rev].f -= flow;
		}
		max_flow += flow;
	}
	return max_flow;
}

int v1[MAXN], v2[MAXN];

int main() 
{
	int N, source, sink, cont = 0, lobo, fluxo;
	scanf("%d", &N);
	source = 0;
	sink = 2*N+1;
	for(int i = 1; i <= N; i++)
		scanf("%d %d", &v1[i], &v2[i]);
	for(int i = 1; i <= N; i++)
	{
		lobo = 0;
		for(int j = 1; j <= N; j++)
			if((v1[j] == i) || (v2[j] == i))
				lobo++;
		for(int j = 0; j <= sink; j++)
			adj[j].clear();
		for(int j = 1; j <= N; j++)
		{
			if(i != j)
			{
				if((v1[j] != i) && (v2[j] != i) && (i != j))
				{
					add_edge(0, j, 1);
					add_edge(j, v1[j]+N, 1);
					add_edge(j, v2[j]+N, 1);
				}
				add_edge(j+N, sink, lobo-1-((v1[i] == j) || (v2[i] == j)));
			}
		}
		fluxo = edmonds_karp(sink+1, source, sink);
		cont += ((N-lobo-1) > fluxo);
	}
	printf("%d\n", cont);
	return 0;
}
