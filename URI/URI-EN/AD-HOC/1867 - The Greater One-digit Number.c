// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Greater One-digit Number
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1867

#include<stdio.h>
#include<string.h>

main()
{
	int i,n1,n2,c1,c2,x;
	char str1[102],str2[102];
	while(scanf("%s %s",str1,str2)==2&&!((strcmp(str1,"0")==0)&&(strcmp(str2,"0")==0)))
	{
		c1=0;
		c2=0;
		n1=strlen(str1);
		n2=strlen(str2);
		for(i=0;i<n1;i++)
			c1+=str1[i]-'0';
		for(i=0;i<n2;i++)
			c2+=str2[i]-'0';
		x=0;
		while(c1>9)
		{
			while(c1>0)
			{
				x+=c1%10;
				c1/=10;
			}
			c1=x;
		}
		x=0;
		while(c2>9)
		{
			while(c2>0)
			{
				x+=c2%10;
				c2/=10;
			}
			c2=x;
		}
		printf("%d\n",(c1<c2)*2+(c1>c2));
	}
	return 0;
}
