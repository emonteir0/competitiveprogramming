// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: 2048
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1559

#include<stdio.h>

main()
{
	int n,i,j,mat[4][4],vet[4],b,cer;
	char c[4][6]={"DOWN","LEFT","RIGHT","UP"};
	scanf("%d",&n);
	while(n--)
	{
		vet[0]=vet[1]=vet[2]=vet[3]=0;
		b=0;
		cer=0;
		for(i=0;i<4;i++)
			for(j=0;j<4;j++)
			{
				scanf("%d",&mat[i][j]);
				if(mat[i][j]==2048)
					cer=1;
			}	
		for(i=0;i<4;i++)
			for(j=0;j<4;j++)
			{
				if((i+1)<4)
					if((mat[i][j]!=0)&&((mat[i][j]==mat[i+1][j])||(mat[i+1][j]==0)))
						vet[0]=1;
				if((i-1)>=0)
					if((mat[i][j]!=0)&&((mat[i][j]==mat[i-1][j])||(mat[i-1][j]==0)))
						vet[3]=1;
				if((j+1)<4)
					if((mat[i][j]!=0)&&((mat[i][j]==mat[i][j+1])||(mat[i][j+1]==0)))
						vet[2]=1;
				if((j-1)>=0)
					if((mat[i][j]!=0)&&((mat[i][j]==mat[i][j-1])||(mat[i][j-1]==0)))
						vet[1]=1;
			}
		if(cer==0)
		{	
			for(i=0;i<4;i++)
			{
				if(vet[i])
				{
					printf("%s%s",b?" ":"",c[i]);
					b=1;
				}
			}
			printf("%s\n",b?"":"NONE");
		}
		else	printf("NONE\n");
	}
	return 0;
}
