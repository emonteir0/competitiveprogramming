// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tarzan
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2412

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(1000);
int x[1000], y[1000], vis[1000];

int sqr(int x)
{
	return x*x;
}

void dfs(int x)
{
	int i, y;
	vis[x] = 1;
	for(i = 0; i < G[x].size(); i++)
	{
		y = G[x][i];
		if(!vis[y])
			dfs(y);
	}
}

main()
{
	int i, j, N, D, f = 1;
	scanf("%d %d", &N, &D);
	D *= D;
	for(i = 0; i < N; i++)
	{
		scanf("%d %d", &x[i], &y[i]);
		for(j = 0; j < i; j++)
		{
			if(sqr(x[i]-x[j]) + sqr(y[i]-y[j]) <= D)
				G[i].push_back(j), G[j].push_back(i);
		}
	}
	dfs(0);
	for(i = 0; i < N; i++)
		f &= vis[i];
	printf("%s\n", f ? "S" : "N");
	return 0;
}
