// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: TV da Vovó
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2288

#include<stdio.h>

int mat[1001][1001];

main()
{
	int i, j, N, M, x, y, sx, sy, k = 1;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
				scanf("%d", &mat[i][j]);
		sx = sy = 0;
		while(scanf("%d %d", &x, &y) == 2 && (x||y))
		{
			sx += x;
			sy += y;
		}
		sx = (((-sx) % M)+M)%M;
		sy = ((sy%N)+N)%N;
		printf("Teste %d\n", k++);
		for(i = 0; i < N; i++)
		{
			printf("%d", mat[(i+sy)%N][sx%M]);
			for(j = 1; j < M; j++)
				printf(" %d", mat[(i+sy)%N][(j+sx)%M]);
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}

