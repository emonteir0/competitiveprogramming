// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Laser Sculpture
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1107

#include<stdio.h>

main()
{
	int a,b,c,cont=0;
	while(scanf("%d %d",&a,&b)&&(a||b))
	{
		cont=0;
		while(b--)
		{
			scanf("%d",&c);
			
			cont+=(a-c)*((c-a)<0);
			a=c;;
		}
		printf("%d\n",cont);
	}
	return 0;
}
