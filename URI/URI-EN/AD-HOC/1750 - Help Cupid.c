// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Cupid
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1750

#include<stdio.h>
#include<stdlib.h>

int cmp(const void* a, const void* b)
{
	return *((int*)a)-*((int*)b);
}

int absol(int val)
{
	if(val<0)
		return -val;
	return val;
}

int vet[2000];

main()
{
	int N,i,d1,d2,j,x;
	scanf("%d",&N);
	for(i=0;i<N;i++)
		scanf("%d",&vet[i]);
	qsort(vet,N,sizeof(vet[0]),cmp);
	d1=0;
	d2=0;
	for(i=0;i<=N/2-1;i++)
	{
		j=2*i;
		x=absol(vet[j]-vet[(N+j-1)%N]);
		d1+=(12>x)?x:24-x;
		x=vet[j+1]-vet[j];
		d2+=(12>x)?x:24-x;
	}
	printf("%d\n",(d1<d2)?d1:d2);
	return 0;
}
