// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Paired Pairs
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2730

#include<bits/stdc++.h>

using namespace std;

set< pair<int, int> > conjunto, conjunto2;
int vet[200], vet2[200];

main()
{
	int N;
	while(scanf("%d", &N) == 1 && N)
	{
		conjunto = conjunto2;
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		for(int i = 0; i < N; i++)
			scanf("%d", &vet2[i]);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
			{
				if(__gcd(vet[i], vet2[j]) == 1)
				{
					conjunto.insert(make_pair(vet[i], vet2[j]));
					conjunto.insert(make_pair(vet2[j], vet[i]));
				}
			}
		printf("%d\n", conjunto.size());
	}
	return 0;
}
