// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sticker Collector Robot
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1121

#include<stdio.h>

char mat[100][100];
int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};

int main()
{
	int i, j, N, M, K, dir, x0, y0, x1, y1, ans;
	char c;
	while(scanf("%d %d %d%*c", &N, &M, &K) == 3 && (N||M||K))
	{
		ans = 0;
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
			{
				scanf("%c", &c);
				if(c == 'N')
				{
					dir = 0;
					x0 = i;
					y0 = j;
					mat[i][j] = '.';
				}
				else if(c == 'S')
				{
					dir = 2;
					x0 = i;
					y0 = j;
					mat[i][j] = '.';
				}
				else if(c == 'L')
				{
					dir = 1;
					x0 = i;
					y0 = j;
					mat[i][j] = '.';
				}
				else if(c == 'O')
				{
					dir = 3;
					x0 = i;
					y0 = j;
					mat[i][j] = '.';
				}
				else
					mat[i][j] = c;
			}
			scanf("%*c");
		}
		while(K--)
		{
			scanf("%c", &c);
			if(c == 'E')
				dir = (dir+3)&3;
			else if(c == 'D')
				dir = (dir+1)&3;
			else
			{
				x1 = x0 + dx[dir];
				y1 = y0 + dy[dir];
				if(x1 >= 0 && x1 < N && y1 >= 0 && y1 < M)
				{
					if(mat[x1][y1] != '#')
					{
						if(mat[x1][y1] == '*')
						{
							mat[x1][y1] = '.';
							ans++;
						}
						x0 = x1;
						y0 = y1;
					}
				}
			}
		}
		scanf("%*c");
		printf("%d\n", ans);
	}
	return 0;
}
