// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tiro ao Alvo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2432

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll raios[100000];

main()
{
	int i, N, M, x, y;
	ll v, cont = 0;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
	{
		scanf("%lld", &v);
		raios[i] = v*v;
	}
	while(M--)
	{
		scanf("%d %d", &x, &y);
		v = ((ll)x)*x + ((ll)y)*y;
		cont += N - (lower_bound(raios, raios + N, v) - raios);
	}
	printf("%lld\n", cont);
	return 0;
}

