// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Wrath of Khan
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1964

#include<stdio.h>

main()
{
	int C, Va, Vb, Ta, Tb, T, D, Sa, Sb;
	scanf("%d %d %d %d %d", &C, &Va, &Vb, &T, &D);
	C *= 100;
	D *= 100;
	Sa = ((D - 60*Va*T) % C + C) % C;
	Sb = ((D - 60*Vb*T) % C + C) % C;
	printf("%s\n", (Sa*Vb <= Sb*Va) ? "Ana" : "Bia");
	return 0;
}
