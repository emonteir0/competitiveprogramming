// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Phrase
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2874

#include<bits/stdc++.h>

using namespace std;

char ans[100001];
char str[9];

main()
{
	int N, val;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%s", str);
			val = 0;
			for(int j = 0; str[j]; j++)
				val = 2*val + str[j]-'0';
			ans[i] = val;
		}
		ans[N] = 0;
		printf("%s\n", ans);
	}
	return 0;
}
