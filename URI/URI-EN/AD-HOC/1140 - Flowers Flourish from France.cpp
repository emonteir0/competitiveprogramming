// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flowers Flourish from France
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1140

#include<bits/stdc++.h>

using namespace std;

int compare(char a, char b)
{
	if(a>='a'&&a<='z')
		a+='A'-'a';
	if(b>='a'&&b<='z')
		b+='A'-'a';
	return a==b;
}

main()
{
	string str;
	char c, vet[1201];
	bool b;
	while((gets(vet)!=NULL)&&(strcmp(vet,"*")!=0))
	{
		b=true;
		str=vet;
		stringstream ss(str);
		ss >> str;
		c=str[0];
		while(ss >> str)
			b&=compare(c,str[0]);
		printf("%s\n",b?"Y":"N");
	}
	return 0;
}
