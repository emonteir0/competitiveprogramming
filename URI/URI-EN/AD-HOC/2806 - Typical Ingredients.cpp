// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Typical Ingredients
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2806

#include<bits/stdc++.h>

using namespace std;

set<string> ingredientes;

vector< string > comida, vazio;
vector< string > componentes[5001];
vector<int> vis; 

main()
{
	int N, M, mod, cont;
	string str;
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	cin >> N;
	while(N--)
	{
		cin >> str;
		ingredientes.insert(str);
	}
	cin >> N;
	for(int i = 0; i < N; i++)
	{
		vis.push_back(0);
		cin >> str;
		comida.push_back(str);
		cin >> M;
		for(int j = 0; j < M; j++)
		{
			cin >> str;
			componentes[i].push_back(str);
		}
	}
	mod = 1;
	while(mod)
	{
		mod = 0;
		for(int i = 0; i < N; i++)
		{
			if(!vis[i])
			{
				cont = 0;
				for(int j = 0; j < componentes[i].size(); j++)
					cont += (ingredientes.count(componentes[i][j]) > 0);
				if(2*cont > componentes[i].size())
				{
					vis[i] = 1;
					mod = 1;
					ingredientes.insert(comida[i]);
				}
			}
		}
	}
	for(int i = 0; i < N; i++)
		cout << (vis[i] ? "porcao tipica" : "porcao comum") << endl;
	return 0;
}
