// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Head or Tail
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1329

#include<stdio.h>

main()
{
	int N,x,cont,cont2;
	while(scanf("%d",&N)==1&&N>0)
	{
		cont=0;
		cont2=0;
		while(N--)
		{
			scanf("%d",&x);
			if(x==0)	cont++;
			else		cont2++;
		}
		printf("Mary won %d times and John won %d times\n",cont,cont2);
	}
	return 0;
}
