// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dangerous Dive
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1471

#include<bits/stdc++.h>

using namespace std;

bitset<10001> bits,bits2;

main()
{
	int a, b, x, i;
	bool bolean;
	bits2.reset();
	while(scanf("%d %d",&a,&b)==2)
	{
		bolean=true;
		bits=bits2;
		for(i=0;i<b;i++)
		{
			scanf("%d",&x);
			bits.set(x);
		}
		for(i=1;i<=a;i++)
		{
			if(!bits[i])
			{
					printf("%d ",i);
				bolean=false;
			}
		}
		if(bolean)
			printf("*\n");
		else
			printf("\n");
	}
	return 0;
}
