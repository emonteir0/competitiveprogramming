// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colorindo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2405

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x, y;
} posicao;

int mat[201][201];
int dx[]={1,1,1,0,0,-1,-1,-1},dy[]={1, 0, -1, 1, -1, 1, 0, -1};

queue< posicao > fila;
posicao p, p2;

main()
{
	int i, N, M, cont = 0, x, y, x0, y0, Q;
	scanf("%d %d %d %d %d", &N, &M, &x0, &y0, &Q);
	while(Q--)
	{
		scanf("%d %d", &x, &y);
		mat[x][y] = 1;
	}
	p.x = x0;
	p.y = y0;
	fila.push(p);
	cont = 1;
	mat[x0][y0] = 1;
	while(!fila.empty())
	{
		p = fila.front();
		fila.pop();
		x = p.x;
		y = p.y;
		for(i = 0; i < 8; i++)
		{
			p2.x = x+dx[i];
			p2.y = y+dy[i];
			if(p2.x > 0 && p2.x <= N && p2.y > 0 && p2.y <= M)
				if(mat[p2.x][p2.y] == 0)
				{
					mat[p2.x][p2.y] = 1;
					cont++;
					fila.push(p2);
				}
		}
	}
	printf("%d\n", cont);
	return 0;
}

