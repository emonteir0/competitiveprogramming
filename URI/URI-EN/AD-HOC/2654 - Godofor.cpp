// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Godofor
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2654

#include<bits/stdc++.h>

using namespace std;

struct st{
	int a, b, c;
	char msg[101];
};

bool cmp(st st1, st st2)
{
	if(st1.a != st2.a)
		return st1.a > st2.a;
	if(st1.b != st2.b)
		return st1.b > st2.b;
	if(st1.c != st2.c)
		return st1.c < st2.c;
	return strcmp(st1.msg, st2.msg) < 0;
}

st vet[100];

main()
{
	int i, N;
	scanf("%d%*c", &N);
	for(i = 0; i < N; i++)
		scanf("%s %d %d %d%*c", vet[i].msg, &vet[i].a, &vet[i].b, &vet[i].c);
	sort(vet, vet+N, cmp);
	printf("%s\n", vet[0].msg);
	return 0;
}
