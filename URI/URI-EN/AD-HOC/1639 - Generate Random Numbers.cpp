// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Generate Random Numbers
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1639

#include<bits/stdc++.h>

using namespace std;

set<int> conjunto,conjunto2;

main()
{
	int X,cont;
	while(scanf("%d",&X)==1&&(X!=0))
	{
		cont=0;
		while(conjunto.find(X)==conjunto.end())
		{
			conjunto.insert(X);
			X=((X*X)/100)%10000;
			cont++;
		}
		printf("%d\n",cont);
		conjunto=conjunto2;
	}
	return 0;
}
