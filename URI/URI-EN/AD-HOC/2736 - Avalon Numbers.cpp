// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Avalon Numbers
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2736

#include<stdio.h>
#include<string.h>
#define ll long long
#define llu unsigned long long

int e, z, z1, z2;
char numero[20];
ll memo[20][2][2];

ll solve(int dig, int maior, int one)
{
	int i;
	if(!numero[dig])
		return 1;
	ll &r = memo[dig][maior][one];
	if(r != -1)
		return r;
	r = 0;
	for(i = 0; i <= 9; i++)
	{	
		if(maior && (i + '0') > numero[dig])
			break;
		if((one && i == z2) || (i == e))
			continue;
		r += solve(dig+1, maior && ((i + '0') == numero[dig]), i == z1);
	}
	return r;
}

main()
{
	llu x, y, lo, hi, mid, ans;
	while(scanf("%llu %d %d", &x, &e, &z) == 3)
	{
		z1 = z/10;
		z2 = z%10;
		lo = 0;
		hi = 10000000000000000000ULL;
		while(lo <= hi)
		{
			mid = lo + (hi-lo)/2;
			sprintf(numero, "%llu", mid);
			memset(memo, -1, sizeof(memo));
			y = solve(0, 1, 0);
			if(y == x+1)
			{
				ans = mid;
				hi = mid-1;
			}
			else if(y < x+1)
				lo = mid+1;
			else
				hi = mid-1;
		}
		printf("%llu\n", ans);
	}
	return 0;
}
