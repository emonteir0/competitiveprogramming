// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Guess What
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1547

#include<stdio.h>

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	int n,m,i,x,y,diff,id;
	scanf("%d",&n);
	while(n--)
	{
		diff=101;
		id=0;
		scanf("%d %d",&m,&y);
		for(i=1;i<=m;i++)
		{
			scanf("%d",&x);
			if(abs(y-x)<diff)
			{
				diff=abs(y-x);
				id=i;
			}
		}
		printf("%d\n",id);
	}
	return 0;
}
