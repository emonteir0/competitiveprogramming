// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Campeonato
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2833

#include<bits/stdc++.h>

using namespace std;

char partida[4][20] = {"final", "semifinal", "quartas", "oitavas"};

int func(int x, int y)
{
	int z = 8;
	for(int i = 0; i < 4; i++)
	{
		if((x&z) != (y&z))
			return i;
		x &= z-1;
		y &= z-1;
		z >>= 1;
	}
	return 0;
}

main()
{
	int x, y, z;
	for(int i = 0; i < 16; i++)
	{
		scanf("%d", &x);
		if(x == 1)
			y = i;
		if(x == 9)
			z = i;
	}
	printf("%s\n", partida[func(y, z)]);
	return 0;
}
