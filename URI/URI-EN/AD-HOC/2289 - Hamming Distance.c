// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hamming Distance
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2289

#include<stdio.h>



main()
{
	int cont;
	unsigned long long int a, b, c;
	while(scanf("%llu %llu", &a, &b)==2 && (a||b))
	{
		cont = 0;
		c = a^b;
		while(c > 0)
		{
			cont += c&1;
			c >>= 1;
		}
		printf("%d\n", cont);
	}
	return 0;
}
