// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lunar Temperature
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2231

#include<stdio.h>

int vet[10001], vet2[10001];

main()
{
	int ma, me, n, m, i, x, k = 1;
	while(scanf("%d %d", &n, &m) == 2 && (n||m))
	{
		vet[0] = 0;
		for(i=1; i <= m;i++)
		{
			scanf("%d", &vet[i]);
			vet2[i] = vet2[i-1]+vet[i];
		}
		ma = vet2[m];
		me = vet2[m];
		for(;i <= n; i++)
		{
			scanf("%d", &vet[i]);
			vet2[i] = vet2[i-1]+vet[i]-vet[i-m];
			ma = ma > vet2[i]? ma:vet2[i];
			me = me < vet2[i]?me:vet2[i];
		}
		printf("Teste %d\n%d %d\n\n",k++,me/m,ma/m);
	}
    return 0;
}
