// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: SSN 2
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1786

#include<stdio.h>


main()
{
	char a;
	int n1,n2;
	while(1)
	{
		n1=0;
		n2=0;
		if(scanf("%c",&a)==EOF)
			break;
		n1+=(a-'0');
		n2+=9*(a-'0');
		printf("%c",a);
		scanf("%c",&a);
		n1+=2*(a-'0');
		n2+=8*(a-'0');
		printf("%c",a);
		scanf("%c",&a);
		n1+=3*(a-'0');
		n2+=7*(a-'0');
		printf("%c.",a);
		scanf("%c",&a);
		n1+=4*(a-'0');
		n2+=6*(a-'0');
		printf("%c",a);
		scanf("%c",&a);
		n1+=5*(a-'0');
		n2+=5*(a-'0');
		printf("%c",a);
		scanf("%c",&a);
		n1+=6*(a-'0');
		n2+=4*(a-'0');
		printf("%c.",a);
		scanf("%c",&a);
		n1+=7*(a-'0');
		n2+=3*(a-'0');
		printf("%c",a);
		scanf("%c",&a);
		n1+=8*(a-'0');
		n2+=2*(a-'0');
		printf("%c",a);
		scanf("%c%*c",&a);
		n1+=9*(a-'0');
		n2+=a-'0';
		printf("%c-",a);
		n1=(n1%11)%10;
		n2=(n2%11)%10;
		printf("%d%d\n",n1,n2);
	}
	return 0;
}
