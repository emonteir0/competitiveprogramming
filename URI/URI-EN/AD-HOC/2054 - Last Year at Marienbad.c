// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Last Year at Marienbad
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2054

#include<stdio.h>

main()
{
	int i, j, x, v, N, t;
	char bol;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		if(i > 1)
			printf("\n");
		x = t = bol = 0;
		for(j = 0; j < 6; j++)
		{
			scanf("%d", &v);
			if(v > 1)
				bol = 1;
			t += v == 1;
			x ^= v;
		}
		if(bol)
			printf("Instancia %d\n%s\n", i, x ? "sim":"nao");
		else
			printf("Instancia %d\n%s\n", i, t%2 ? "nao":"sim");
	}
	return 0;
}

