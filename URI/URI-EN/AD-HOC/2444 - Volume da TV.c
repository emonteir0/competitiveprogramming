// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Volume da TV
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2444

#include<stdio.h>

int min(int a, int b)
{
	return (a < b) ? a : b;
}

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int acum, n, x;
	scanf("%d %d", &acum, &n);
	while(n--)
	{
		scanf("%d", &x);
		if(x > 0)
			acum = min(acum+x, 100);
		else
			acum = max(acum+x, 0);
	}
	printf("%d\n", acum);
	return 0;
}

