// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Calculadora
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2401

#include<stdio.h>
#include<math.h>
#define ll long long


int cont2, cont3, cont5, cont7;

main()
{
	int i, n, x;
	ll num = 1;
	char op;
	scanf("%d%*c", &n);
	while(n--)
	{
		scanf("%d %c%*c", &x, &op);
		switch(x)
		{
			case 2:
			{
				cont2 += (op == '*')? 1 : - 1;
				break;
			}
			case 3:
			{
				cont3 += (op == '*')? 1 : - 1;
				break;
			}
			case 4:
			{
				cont2 += (op == '*')? 2 : -2;
				break;
			}
			case 5:
			{
				cont5 += (op == '*')? 1 : -1;
				break;
			}
			case 6:
			{
				cont2 += (op == '*')? 1 : - 1;
				cont3 += (op == '*')? 1 : - 1;
				break;
			}
			case 7:
			{
				cont7 += (op == '*')? 1 : - 1;
				break;
			}
			case 8:
			{
				cont2 += (op == '*')? 3 : - 3;
				break;
			}
			case 9:
			{
				cont3 += (op == '*')? 2 : - 2;
				break;
			}
		}
	}
	for(i = 0; i < cont2; i++)
		num *= 2;
	for(i = 0; i < cont3; i++)
		num *= 3;
	for(i = 0; i < cont5; i++)
		num *= 5;
	for(i = 0; i < cont7; i++)
		num *= 7;
	printf("%lld\n", num);
	return 0;
}

