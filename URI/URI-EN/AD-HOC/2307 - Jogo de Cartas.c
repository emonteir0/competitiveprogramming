// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jogo de Cartas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2307

#include<stdio.h>

int vet[100001];

main()
{
	int i, N, x, ans = 1;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		scanf("%d", &x);
		vet[x] = i;
	}
	for(i = 2; i <= N; i++)
	{
		if(vet[i] < vet[i-1])
			ans++;
	}
	printf("%d \n", ans);
	return 0;
}
