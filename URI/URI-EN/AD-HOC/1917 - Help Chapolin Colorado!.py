# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Help Chapolin Colorado!
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1917

def _fib(n):
    if n == 0:
        return (0, 1)
    else:
        a, b = _fib(n // 2)
        c = a * (b * 2 - a)
        d = a * a + b * b
        if n % 2 == 0:
            return (c, d)
        else:
            return (d, c + d)

def fibonacci(n):
    return _fib(n)[0]            

while True:
	a, b = raw_input().split(' ')
	if a == '0' and b == '0':
		break
	print int(a) * fibonacci(int(b) + 2)

