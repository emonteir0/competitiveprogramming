// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jogo da Memória
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2470

#include<bits/stdc++.h>
#define MAXL 20
#define MAXN 50001

using namespace std;

typedef struct
{
	int x,i;
}par;


int vet[MAXN],pai[MAXN],nivel[MAXN],ancestral[MAXN][MAXL];
map<int,set<int> > Mapa;
queue<int> fila;
set<int> conjunto;
set<int>::iterator it;
par vet2[MAXN];

void DFS(int U)
{
	int V;
	fila.push(U);
	while(!fila.empty()){
		U=fila.front();
		fila.pop();
		conjunto=Mapa[U];
		for(it=conjunto.begin();it!=conjunto.end();++it)
		{
			V=*it;
			if(nivel[V]==-1)
			{
				pai[V]=U;
				nivel[V]=nivel[U]+1;
				fila.push(V);
			}
		}
	}
}

bool cmp(par a, par b)
{
	return a.x>b.x;
}

int LCA(int u, int v)
{    
    if(nivel[u] < nivel[v]) swap(u, v);
    for(int i = MAXL-1;i >= 0;i--)
        if(nivel[u] - (1<<i) >= nivel[v])
            u = ancestral[u][i];
    if(u == v) return u; 
    for(int i = MAXL-1;i >= 0;i--)
        if(ancestral[u][i] != -1 && ancestral[u][i] != ancestral[v][i]){
            u = ancestral[u][i];
            v = ancestral[v][i];
        }          
    return pai[u];
}

main()
{
	int N,i,j,x,y,cont=0;
	scanf("%d",&N);
	for(i=1;i<=N;i++)
	{
		scanf("%d",&vet[i]);
		vet2[i-1].x=vet[i];
		vet2[i-1].i=i;
	}
	for(i=1;i<N;i++)
	{
		scanf("%d %d",&x,&y);
		Mapa[x].insert(y);
		Mapa[y].insert(x);
	}
	for(i=2;i<=N;i++)
		nivel[i]=-1;
	DFS(1);
	for(i = 1;i <= N;i++) ancestral[i][0] = pai[i];
    for(j = 1;j < MAXL;j++)
    for(i = 1;i <=   N;i++)
        ancestral[i][j] = ancestral[ ancestral[i][j-1] ][j-1];
	sort(vet2,vet2+N,cmp);
	for(i=0;i<N/2;i++)
		cont+=nivel[vet2[2*i].i]+nivel[vet2[2*i+1].i]-2*nivel[LCA(vet2[2*i].i,vet2[2*i+1].i)];
	printf("%d\n",cont);
	return 0;
}
