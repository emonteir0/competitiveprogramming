// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Voyage Home
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1966

#include<bits/stdc++.h>
#define ll long long

using namespace std;

pair<int, int> vet[100001];

bool cmp(pair<int, int> a, pair<int, int> b)
{
	return a.first*b.second > a.second*b.first;
}

main()
{
	int N;
	ll tot = 0, B;
	scanf("%d %lld", &N, &B);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i].first);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i].second);
	sort(vet, vet+N, cmp);
	for(int i = 0; i < N; i++)
	{
		if(B >= vet[i].second)
		{
			B -= vet[i].second;
			tot += vet[i].first;
		}
		else
		{
			tot += (B*vet[i].first)/vet[i].second;
			break;
		}
	}
	printf("%.0lf\n", floor((-1+sqrt(1+8*tot))/2));
	return 0;
}
