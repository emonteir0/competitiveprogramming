// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Account Book
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1122

#include<bits/stdc++.h>

using namespace std;

int N, F, L;
int vet[41];
int op[41];
int vis[41][80001];
int dp[41][80001];

int solve(int x, int s)
{
	int r, mais, menos;
	if(x == N)
		return s == F;
	if(abs(dp[x][s]) == L)
		return dp[x][s] == L ? 1 : 0;
	mais = solve(x+1, s+vet[x]);
	menos = solve(x+1, s-vet[x]);
	if(op[x] == 0)
	{
		if(mais)
			op[x] = 1;
		if(menos)
			op[x] = 2;
	}
	if(op[x] == 2 && mais)
		op[x] = 3;
	if(op[x] == 1 && menos)
		op[x] = 3;
	r = mais | menos;
	dp[x][s] = r ? L : -L;
	return r;
}

main()
{
	int x, a, b;
	queue< pair<int, int> > fila;
	while(scanf("%d %d", &N, &F) == 2 && N)
	{
		L++;
		F += 40000;
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &vet[i]);
			op[i] = 0;
		}
		dp[N][F] = L;
		x = solve(0, 40000);
		if(x == 0)
			printf("*\n");
		else
		{
			for(int i = 0; i < N; i++)
				printf("%c", (op[i] == 3) ? '?' : (op[i] == 1) ? '+' : '-');
			printf("\n");
		}
	}
	return 0;
}
