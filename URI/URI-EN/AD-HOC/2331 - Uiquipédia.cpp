// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Uiquipédia
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2331

#include<bits/stdc++.h>
#define INF 100010001
using namespace std;

int N;
map<string, int> Mapa;
map<string, int>::iterator it, ita, itd;
vector<int> G[2100];
int dist[2100];

void dijkstra(int x)
{
	int u, v, w;
	priority_queue< pair<int,int> > fila;
	for(int i = 0; i < N; i++)
		dist[i] = INF;
	dist[x] = 0;
	fila.push(make_pair(0, x));
	while(!fila.empty())
	{
		w = -fila.top().first;
		u = fila.top().second;
		fila.pop();
		if(w > dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u]+1)
			{
				dist[v] = dist[u]+1;
				fila.push(make_pair(-dist[v], v));
			}
		}
	}
}

main()
{
	int K;
	string str, str2;
	cin >> K;
	while(K--)
	{
		cin >> str >> str2;
		if(Mapa.count(str) <= 0)
			Mapa[str] = N++;
		if(Mapa.count(str2) <= 0)
			Mapa[str2] = N++;
		G[Mapa[str]].push_back(Mapa[str2]);
	}
	for(it = Mapa.begin(); it != Mapa.end(); it++)
	{
		ita = it;
		itd = it;
		itd++;
		if(it != Mapa.begin())
		{
			ita--;
			G[it->second].push_back(ita->second);
		}
		if(itd != Mapa.end())
		{
			G[it->second].push_back(itd->second);
		}
	}
	cin >> str >> str2;
	dijkstra(Mapa[str]);
	printf("%d\n", dist[Mapa[str2]]);
	return 0;
}
