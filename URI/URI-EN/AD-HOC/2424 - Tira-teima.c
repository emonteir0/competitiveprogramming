// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tira-teima
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2424

#include<stdio.h>

main()
{
	int a, b;
	scanf("%d %d", &a, &b);
	printf("%s\n", ((a >= 0) && (a <= 432) && (b >= 0) && (b <= 468)) ? "dentro": "fora");
	return 0;
}

