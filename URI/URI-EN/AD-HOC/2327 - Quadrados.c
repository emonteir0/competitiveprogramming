// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadrados
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2327

#include<stdio.h>

int linha[10], col[10];
int dp, ds;

main()
{
	int N, i, j, x;
	int v = 1;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < N; j++)
		{
			scanf("%d", &x);
			linha[i] += x;
			col[j] += x;
			if(i == j)
				dp += x;
			if(i + j == N-1)
				ds += x;
		}
	}
	v = dp == ds;
	for(i = 0; i < N; i++)
		v &= (dp == linha[i]) && (dp == col[i]);
	printf("%d\n", v? dp:-1);
	return 0;
}
