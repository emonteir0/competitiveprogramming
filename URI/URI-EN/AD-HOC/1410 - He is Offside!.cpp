// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: He is Offside!
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1410

#include<cstdio>
#include<algorithm>

using namespace std;

int vet[12],vet2[12];

main()
{
	int n,m,i,a,b;
	while((scanf("%d %d",&n,&m)==2)&&(n||m))
	{
		for(i=0;i<n;i++)
			scanf("%d",&vet[i]);
		for(i=0;i<m;i++)
			scanf("%d",&vet2[i]);
		sort(vet,vet+n);
		sort(vet2,vet2+m);
		a=vet[0];
		b=vet2[1];
		if(b>a)
			printf("Y\n");
		else
			printf("N\n");
	}
	return 0;
}
