// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Decifra
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2464

#include<bits/stdc++.h>

using namespace std;

map<char, char> Mapa;

main()
{
	int i;
	char msg[10001], alpha[27], c;
	scanf("%s", alpha);
	for(i = 0; c = alpha[i]; i++)
		Mapa['a'+i] = c;
	scanf("%s", msg);
	for(i = 0; c = msg[i]; i++)
		printf("%c", Mapa[c]);
	printf("\n");
	return 0;
}

