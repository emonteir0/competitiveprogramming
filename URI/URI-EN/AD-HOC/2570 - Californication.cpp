// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Californication
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2570

#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    int n, m, k;
    cin >> n >> m >> k;
    vector <int> l(n, -1), c(m, -1), ans(4);
    for (int i = 0; i < k; i++) {
        char x;
        int y;
        cin >> x >> y;
        if (x == 'L') {
            l[y-1] = i;
        } else {
            c[y-1] = i;
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            int x = max(l[i], c[j]);
            if (~x) {
                ans[x%4]++;
            }
        }
    }

    string name = "RHCP";
    cout << name[0] << ans[0];
    for (int i = 1; i < 4; i++) {
        cout << ' ' << name[i] << ans[i];
    }
    cout << endl;
    return 0;
}
