// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Insatisfaction on Elections
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1615

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int i,votos;
}candidato;


bool cmp(candidato a, candidato b)
{
	return a.votos>b.votos;
}

main()
{	
	candidato v[10],u;
	int N,m,n=0,x,i;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %d",&n,&m);
		u.votos=0;
		for(i=1;i<=n;i++)
		{
			u.i=i;
			v[i-1]=u;
		}
		for(i=0;i<m;i++)
		{
			scanf("%d",&x);
			v[x-1].votos++;
		}
		sort(v,v+n,cmp);
		u=v[0];
		if((u.votos)*2>m)
			printf("%d\n",u.i);
		else
			printf("-1\n");
	}
	return 0;
}
