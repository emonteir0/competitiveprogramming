// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Farm Robot
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2349

#include<stdio.h>

main()
{
	int N, C, S, x, v, cont;
	while(scanf("%d %d %d", &N, &C, &S) == 3)
	{
		v = 1;
		if(S == N)
			S = 0;
		cont = (S == 1);
		while(C--)
		{
			scanf("%d", &x);
			v = (v+N+x)%N;
			cont += (v==S);
		}
		printf("%d\n", cont);
	}
	return 0;
}

