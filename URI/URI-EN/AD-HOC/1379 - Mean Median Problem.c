// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mean Median Problem
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1379

#include<stdio.h>

main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2&&(!((a==0)&&(b==0))))
		printf("%d\n",2*a-b);
	return 0;
}
