// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Robot Instructions
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1574

#include<stdio.h>
#include<string.h>


main()
{
	int i,j,n,N,pos,tam;
	char inst[51],c,vet[100];
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d%*c",&n);
		pos=0;
		for(i=0;i<n;i++)
		{
			gets(inst);
			if(strcmp(inst,"LEFT")==0)
			{
				vet[i]=-1;
				pos--;
			}
			else if(strcmp(inst,"RIGHT")==0)
			{
				vet[i]=1;
				pos++;
			}
			else
			{
				tam=strlen(inst);
				if(tam==9)
				{
					j=inst[8]-'0';
				}
				if(tam==10)
				{
					j=10*inst[8]+inst[9]-11*'0';
				}
				pos+=vet[j-1];
				vet[i]=vet[j-1];
			}
		}
		printf("%d\n",pos);
	}
	return 0;
}
