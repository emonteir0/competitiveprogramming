// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Competição de Chocolate
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2367

#include<stdio.h>
#define MAXN 1000001

int vet[1000001], forbid[1000001];

main()
{
	int i, j, N, K;
	scanf("%d %d", &N, &K);
	for(i = 0; i <= N; i++)
	{
		vet[i] = 0;
		forbid[i] = 0;
	}
	for(i = 0; i < N; i++)
	{
		if(!vet[i])
		{
			for(j = 1; j <= K; j++)
			{
				vet[i+j]++;
				forbid[i+j] = j;
			}
		}
		else if(vet[i] == 1 && i + forbid[i] <= N)
		{
			vet[i+forbid[i]]++;
			forbid[i+forbid[i]] = forbid[i];
		}
	}
	printf("%s\n", vet[i] ? "Paula" : "Carlos");
	return 0;
}
