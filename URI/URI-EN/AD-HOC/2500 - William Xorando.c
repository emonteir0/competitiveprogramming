// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: William Xorando
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2500

#include<stdio.h>

int vet[1000];

main()
{
	int i, N, M, K;
	while(scanf("%d %d %d", &N, &M, &K) == 3 && (N||M||K))
	{
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		printf("%d\n", vet[((N-M+K-1) % N + N ) % N]);
	}
	return 0;
}
