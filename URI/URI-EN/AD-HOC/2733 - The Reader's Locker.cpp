// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Reader's Locker
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2733

#include<bits/stdc++.h>

using namespace std;

main()
{
	int i, N, x, vet[4], cont;
	while(scanf("%d", &N) == 1)
	{
		cont = 0;
		for(i = 0; i < 4; i++)
			vet[i] = 0;
		while(N--)
		{
			scanf("%d", &x);
			for(i = 0; i < 4; i++)
				if(vet[i] == x)
					break;
			if(i == 4)
			{
				cont++;
				for(int i = 2; i >= 0; i--)
					vet[i+1] = vet[i];
				vet[0] = x;
			}
		}
		printf("%d\n", cont);
	}
	return 0;
}
