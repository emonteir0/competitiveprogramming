// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Palíndrome
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2285

#include<bits/stdc++.h>

using namespace std;

int M;
int pd[5001];

char s[2001];
char T[5001];
int P[5001];

int prepare() 
{
	int k = 1;
	T[0] = '^';
	for (int i = 0; s[i]; i++)
	{
		T[k++] = '#';
		T[k++] = s[i];
	}
	T[k++] = '#';
	T[k++] = '$';
	T[k] = 0;
	return k;
}
 
int buildManacher() 
{
	
	int n = prepare();
	int C = 0, R = 0;
	for (int i = 1; i < n-1; i++) 
	{
		int i_mirror = 2*C-i; // equals to i' = C - (i-C)
	
		P[i] = (R > i) ? min(R-i, P[i_mirror]) : 0;
	
		// Attempt to expand palindrome centered at i
		while (T[i + 1 + P[i]] == T[i - 1 - P[i]])
			P[i]++;
	
		// If palindrome centered at i expand past R,
		// adjust center based on expanded palindrome.
		if (i + P[i] > R) 
		{
			C = i;
			R = i + P[i];
		}
	}
	
	// Find the maximum element in P.
	int maxLen = 0;
	int centerIndex = 0;
	for (int i = 1; i < n-1; i++) 
	{
		if (P[i] > maxLen) 
		{
			maxLen = P[i];
			centerIndex = i;
		}
	}
	return n;
	//return s.substr((centerIndex - 1 - maxLen)/2, maxLen);
}

int solve(int x)
{
	if(x > M)
		return 0;
	if(pd[x] != -1)
		return pd[x];
	int r = 10000;
	for(int i = x; i <= M; i += 2)
		if(P[(x+i)/2] >= ((i-x)/2)+1)
			r = min(r, solve(i+2)+1);
	return pd[x] = r;
}

main()
{
	int N, t = 1;
	while(scanf("%d", &N) == 1 && N)
	{
		scanf("%s", s);
		buildManacher();
		M = N+N;
		for(int i = 0; T[i]; i++)
			pd[i] = -1;
		printf("Teste %d\n%d\n\n", t++, solve(2));
	}
	return 0;
}
