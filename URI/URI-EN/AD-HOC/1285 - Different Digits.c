// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Different Digits
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1285

#include<stdio.h>

int digitos(int x)
{
	int vet[10]={0,0,0,0,0,0,0,0,0,0},r;
	while(x!=0)
	{
		r=x%10;
		if(vet[r]==1)
			return 0;
		else
			vet[r]=1;
		x/=10;
	}
	return 1;
}

int vet[5001],vet2[5001];

main()
{
	int i,y,N,M;
	vet[1]=1;
	vet2[1]=1;
	for(i=2;i<=5000;i++)
	{
		y=digitos(i);
		vet[i]=y;
		vet2[i]=vet2[i-1]+y;
	}
	while(scanf("%d %d",&N,&M)==2)
		printf("%d\n",vet2[M]-vet2[N]+vet[N]);
	return 0;
}
