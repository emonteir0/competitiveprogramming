// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Reverse Game
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2512

#include<bits/stdc++.h>
#define MAX 99999

using namespace std;

set<pair<int, int> > fila; 
pair<int, int> p;
int valores[9] = {11, 23, 38, 89, 186, 308, 200, 464, 416};
int visitado[512], distancia[512];

/*
  1   2   4
  8  16  32
 64 128 256


0 -> 1+2+8 = 11
1 -> 1+2+4+16 = 23
2 -> 2+4+32 = 38
3 -> 1+8+16+64 = 89
4 -> 2+8+16+32+128 = 186
5 -> 4+16+32+256 = 308
6 -> 8+64+128 = 200
7 -> 16+64+128+256 = 464
8 -> 32+128+256 = 416
*/


void dijkstra(int x)
{
	int dist, i, z;
	for(i = 0; i < 512; i++)
	{
		distancia[i] = MAX;
		visitado[i] = 0;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(fila.begin());
		dist = p.first;
		x = p.second;
		if(!visitado[x])
		{
			visitado[x] = 1;
			for(i = 0; i < 9; i++)
			{
				z = x ^ valores[i];
				if(!visitado[z] && (dist + 1) < distancia[z])
					fila.insert(make_pair((distancia[z] = (dist+1)), z));
			}
		}
	}
}

main()
{
	int i, N, val, y, A, B, conta = 0, contb = 0;
	char c;
	scanf("%d%*c", &N);
	while(N--)
	{
		val = 0;
		y = 1;
		for(i = 0; i < 9; i++)
		{
			scanf("%c%*c", &c);
			if(c == '*')
				val += y;
			y <<= 1;
		}
		dijkstra(val);
		A = min(distancia[0], distancia[511]);
		scanf("%*c");
		val = 0;
		y = 1;
		for(i = 0; i < 9; i++)
		{
			scanf("%c%*c", &c);
			if(c == '*')
				val += y;
			y <<= 1;
		}
		dijkstra(val);
		B = min(distancia[0], distancia[511]);
		conta += A < B;
		contb += B < A;
		scanf("%*c");
	}
	if(conta == contb)
		printf("Its a draw!\n");
	else
		printf("%s\n", (conta > contb) ? "Fred wins!" : "Jason wins!");
	return 0;
}
