// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Swap It or Not?
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2564

#include<stdio.h>

main()
{
	int N, i, ok, cont, x, y;
	while(scanf("%d", &N) == 1)
	{
		cont = 0;
		ok = 0;
		for(i = 1; i < N; i++)
		{
			scanf("%d %d", &x, &y);
			if(!ok)
			{
				if(x == N)
				{
					cont += !y;
					ok = 1;
				}
			}
			else
				cont += y;
		}
		printf("%d\n", cont);
	}
	return 0;
}
