// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cake Cut
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2015

#include<bits/stdc++.h>
#define ll long long

using namespace std;

struct pt
{
	ll x, y;
	pt(){}
	pt(ll x, ll y)
	{
		this->x = x;
		this->y = y;
	}
};

ll cross(pt O, pt A, pt B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

int N;
pt pts[100010];
ll area[100010];

ll besta(int x, int y, int p)
{
	if(y < 0)
		return area[N-1] - (area[p] - area[x] - abs(cross(pts[0], pts[x], pts[p])));
	return min(area[N-1] - (area[p] - area[x] - abs(cross(pts[0], pts[x], pts[p]))),
			area[p] - area[y] - abs(cross(pts[0], pts[p], pts[y])));
}

ll bestb(int x, int y, int p)
{
	if(y >= N)
		return area[N-1] - (area[x] - area[p] - abs(cross(pts[0], pts[x], pts[p])));
	return min(area[N-1] - (area[x] - area[p] - abs(cross(pts[0], pts[x], pts[p]))),
			area[y] - area[p] - abs(cross(pts[0], pts[p], pts[y])));
}

ll busca(int x)
{
	int lo = 0, hi = x-1, mid, a = x-1;
	ll y, ans;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		y = area[x] - area[mid] - abs(cross(pts[0], pts[mid], pts[x]));
		if(y*2 > area[N-1])
			lo = mid+1;
		else
		{
			a = mid;
			hi = mid-1;
		}
	}
	ans = besta(a, a-1, x);
	lo = x+1, hi = N-1, a = x+1;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		y = area[mid] - area[x] - abs(cross(pts[0], pts[mid], pts[x]));
		if(y*2 > area[N-1])
			hi = mid-1;
		else
		{
			a = mid;
			lo = mid+1;
		}
	}
	ans = min(ans, bestb(a, a+1, x));
	return ans;
}

int main()
{
	ll ma = 0;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%lld %lld", &pts[i].x, &pts[i].y);
	for(int i = 2; i < N; i++)
	{
		area[i] = area[i-1] + cross(pts[0], pts[i-1], pts[i]);
		//printf("%lld\n", area[i]);
	}
	for(int i = 0; i < N; i++)
		ma = max(ma, busca(i));
	printf("%lld %lld\n", ma, area[N-1]-ma);
	return 0;
}
