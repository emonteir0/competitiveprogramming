// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lero-Lero of Mineira
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2798

#include<bits/stdc++.h>

using namespace std;

vector<int> G[101];

char str[101];
int ok[101];
int vis[101];
char texto[1000001];
string ans; 
string palavra[101];

int dfs(int x, int v)
{
	if(v & (1<<x))
		return 0;
	vis[x] = v;
	for(int i = 0; i < (int)G[x].size(); i++)
	{
		if(!dfs(G[x][i], v | (1 << x)))
			return 0;
	}
	return 1;
}

void dfs2(int x)
{
	if(vis[x] == 26)
		return;
	vis[x] = 26;
	if(G[x].size() == 0)
		palavra[x] = char(x+'a');
	for(int i = 0; i < (int)G[x].size(); i++)
	{
		dfs2(G[x][i]);
		palavra[x] += palavra[G[x][i]];
	}
}

int main()
{
	int N, M;
	char c;
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%c %s%*c", &c, str);
		if(c == str[0] && strlen(str) == 1)
			continue;
		for(int i = 0; str[i]; i++)
			G[c-'a'].push_back(str[i]-'a');
	}
	for(int i = 0; i < 26; i++)
	{
		if(dfs(i, 0))
			ok[i] = 1;
		//printf("%d\n", ok[i]);
	}
	for(int i = 0; i < 26; i++)
	{
		if(ok[i])
			dfs2(i);
	}
	scanf("%d", &M);
	scanf("%s", texto);
	for(int i = 0; i < M; i++)
	{
		if(!ok[texto[i]-'a'])
		{
			printf("-1\n");
			return 0;
		}
	}
	for(int i = 0; i < M; i++)
		printf("%s", palavra[texto[i]-'a'].c_str());
	printf("\n");
	return 0;
}

