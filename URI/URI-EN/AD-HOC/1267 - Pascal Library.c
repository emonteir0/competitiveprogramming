// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pascal Library
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1267

#include<stdio.h>

int vet[51];

main()
{
	int i,j,N,M,x,b;
	while((scanf("%d %d",&N,&M)==2)&&(N||M))
	{
		b=0;
		for(i=0;i<N;i++)
			vet[i]=0;
		for(j=0;j<M-1;j++)
		{
			for(i=0;i<N;i++)
			{
				scanf("%d",&x);
				vet[i]+=x;
			}
		}
		for(i=0;i<N;i++)
		{
			scanf("%d",&x);
			vet[i]+=x;
			b|=(vet[i]==M);
		}
		printf("%s\n",b?"yes":"no");
	}
	return 0;
}
