// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Medal Table
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2312

#include<bits/stdc++.h>

typedef struct
{
	int a, b, c;
	char nome[101];
} pais;

bool cmp(pais x, pais y)
{
	if(x.a != y.a)
		return x.a > y.a;
	if(x.b != y.b)
		return x.b > y.b;
	if(x.c != y.c)
		return x.c > y.c;
	return strcmp(x.nome, y.nome) < 0;
}

pais vet[501];

main()
{
	int N, i;
	scanf("%d%*c", &N);
	for(i = 0; i < N; i++)
		scanf("%s %d %d %d", vet[i].nome, &vet[i].a, &vet[i].b, &vet[i].c);
	std::sort(vet, vet+N, cmp);
	for(i = 0; i < N; i++)
		printf("%s %d %d %d\n", vet[i].nome, vet[i].a, vet[i].b, vet[i].c);
	return 0;
}
