// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Blood Groups
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2014

#include <bits/stdc++.h>

using namespace std;

const int MAXN = 201;
vector<int> adj[MAXN];
vector<bool> used(MAXN), visit(MAXN);
int match[MAXN], dist[MAXN];

void bfs(int n1, int n2) 
{
  	std::fill(dist, dist + n1, -1);
  	std::queue<int> q;
  	for (int u = 0; u < n1; u++)
	{
    	if (!used[u]) 
		{
	      	q.push(u);
	      	dist[u] = 0;
    	}
  	}
  	while (!q.empty()) 
	{
    	int u = q.front();
    	q.pop();
    	for (int j = 0; j < (int)adj[u].size(); j++)
		{
      		int v = match[adj[u][j]];
      		if (v >= 0 && dist[v] < 0)
			{
        		dist[v] = dist[u] + 1;
        		q.push(v);
      		}
    	}
  	}
}

bool dfs(int u) 
{
  	visit[u] = true;
  	for (int j = 0; j < (int)adj[u].size(); j++) 
	{
    	int v = match[adj[u][j]];
    	if (v < 0 || (!visit[v] && dist[v] == dist[u] + 1 && dfs(v)))
		{
      		match[adj[u][j]] = u;
      		used[u] = true;
      		return true;
    	}
  	}
  	return false;
}

int hopcroft_karp(int n1, int n2) 
{
	fill(match, match + n2, -1);
	fill(used.begin(), used.end(), false);
	int res = 0;
	for (;;) 
	{
    	bfs(n1, n2);
    	fill(visit.begin(), visit.end(), false);
    	int f = 0;
    	for (int u = 0; u < n1; u++) 
		{
      		if (!used[u] && dfs(u)) 
			{
        		f++;
      		}
    	}
    	if (f == 0) 
		{
      		return res;
    	}
    	res += f;
  	}
  	return res;
}

vector<int> pai[101];
vector<int> filho;

int main() 
{
	int N, Q, x, y;
	scanf("%d %d", &N, &Q);
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &x);
		for(int j = 0; j < x; j++)
		{
			scanf("%d", &y);
			pai[i].push_back(y);
		}
		sort(pai[i].begin(), pai[i].end());
	}
	while(Q--)
	{
		for(int i = 0; i < N; i++)
				adj[i].clear();
		filho.clear();
		scanf("%d", &x);
		for(int i = 0; i < x; i++)
		{
			scanf("%d", &y);
			filho.push_back(y);
		}
		for(int i = x; i < N; i++)
			filho.push_back(0);
		sort(filho.begin(), filho.end());
		for(int j = 0; j < N; j++)
		{
			if(filho[j] == 0)
			{
				for(int i = 0; i < N; i++)
				{
					if(pai[i].size() < N)
						adj[i].push_back(j);
					else
					{
						int t = lower_bound(filho.begin(), filho.end(), 1) - filho.begin();
						if(t == N)
							continue;
						for(int k = t; k < N; k++)
						{
							if(binary_search(pai[i].begin(), pai[i].end(), filho[k]))
							{
								adj[i].push_back(j);
								break;
							}
						}
					}
				}
			}
			else
			{
				for(int i = 0; i < N; i++)
					if(binary_search(pai[i].begin(), pai[i].end(), filho[j]))
						adj[i].push_back(j);
			}
		}
		printf("%s\n", hopcroft_karp(N, N) == N ? "Y" : "N");
	}
	return 0;
}
