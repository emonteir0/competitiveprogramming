// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Guerra por Território
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2420

#include<stdio.h>

int N;
int vet[100001];

int busca(int x)
{
	int inf, sup, mid;
	inf = 1;
	sup = N;
	while(inf <= sup)
	{
		mid = (inf + sup) / 2;
		if(vet[mid] == x)
			return mid;
		else if(vet[mid] > x)
			sup = mid-1;
		else
			inf = mid+1;
	}
	return -1;
}

main()
{
	int i, x;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		scanf("%d", &x);
		vet[i] = vet[i-1] + x;
	}
	printf("%d\n", busca(vet[N]/2));
	return 0;
}

