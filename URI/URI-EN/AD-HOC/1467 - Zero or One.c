// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Zero or One
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1467

#include<stdio.h>
#include<string.h>

char vet[6];

main()
{	
	while(gets(vet))
	{
		if(strcmp(vet,"1 0 0")==0||strcmp(vet,"0 1 1")==0)
			printf("A\n");
		else if(strcmp(vet,"0 1 0")==0||strcmp(vet,"1 0 1")==0)
			printf("B\n");
		else if(strcmp(vet,"0 0 1")==0||strcmp(vet,"1 1 0")==0)
			printf("C\n");
		else
			printf("*\n");
	}
	return 0;
}
