// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grandpa Pepe's Pizza
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1718

#include<stdio.h>

int vet[2000001];
int ac[2000001];
int N, M;

int func(int x, int M)
{
	while(x <= N)
	{
		if(ac[x+M-1] - ac[x-1] == 1)
			x += M;
		else
			return 0;
	}
	return 1;
}

main()
{
	int i, x, C, b, p;
	scanf("%d %d", &N, &C);
	for(i = 0; i < C; i++)
	{
		scanf("%d", &x);
		vet[x] = 1;
		vet[N+x] = 1;
	}
	for(i = 0; i < 2*N; i++)
		ac[i+1] = ac[i] + vet[i];
	M = N/C;
	b = 0;
	for(i = 1; i <= M; i++)
		b |= func(i, M);
	printf("%s\n", b ? "S" : "N");
	return 0;
}
