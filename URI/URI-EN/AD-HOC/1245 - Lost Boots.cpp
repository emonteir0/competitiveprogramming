// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lost Boots
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1245

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int conte,contd;
}tipo;

map<int,tipo> Mapa,Mapa2;
map<int,tipo>::iterator it;

main()
{
	int N,x,cont;
	char c;
	while(scanf("%d%*c",&N)==1)
	{
		while(N--)
		{
			scanf("%d %c%*c",&x,&c);
			if(c=='E')
				Mapa[x].conte++;
			else
				Mapa[x].contd++;
		}
		cont=0;
		for(it=Mapa.begin();it!=Mapa.end();++it)
			cont+=min(it->second.conte,it->second.contd);
		printf("%d\n",cont);
		Mapa=Mapa2;
	}
	return 0;
}
