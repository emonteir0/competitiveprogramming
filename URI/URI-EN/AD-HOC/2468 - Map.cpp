// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Map
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2468

#include <iostream>
#include <cstdio>
#include <bits/stdc++.h>
#define ll long long

using namespace std;

vector< vector<int> > adj(100000);
char vis[100000];

int dfs(int v) 
{
	int i, j, cont = 1;
	vis[v] = true;
	for (i = 0; i < adj[v].size(); i++) 
	{
		j = adj[v][i];
		if (!vis[j])
			cont += dfs(j);
	}
	return cont;
}

int main()
{
	int i, N, x, y, v;
	ll sum;
	scanf("%d", &N);
	for (i = 1; i < N; i++)
	{
		scanf("%d %d %d", &x, &y, &v);
		if(!v)
		{
			adj[--x].push_back(--y);
			adj[y].push_back(x);
		}
	}
	for (i = 0; i < N; i++) 
		vis[i] = false;
	sum = ((ll)N * (N-1)) / 2;
	for (i = 0; i < N; i++)
	if (!vis[i]) 
	{
		v = dfs(i);
		sum -= (ll)v * (v-1) / 2;
	}
	printf("%lld\n", sum);
	return 0;
}
