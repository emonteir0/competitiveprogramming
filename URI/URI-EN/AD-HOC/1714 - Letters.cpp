// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Letters
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1714

#include<bits/stdc++.h>
#define MAX 4294967295
#define uint unsigned int

using namespace std;

typedef struct
{
	int x, y, cont;
} pos;

char mat[101][101];
int con[100][100];
queue< pos > fila, fila2;
pos p0, p1;
int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1};
int N;

bool inRange(int x)
{
	return (x >= 0) && (x <= N);
}

bool valido(char c, int x)
{
	int f;
	if((c >= 'A') && (c <= 'Z'))
	{
		f = 1 << (c-'A');
		return (f & x) == f;
	}
	if((c >= 'a') && (c <= 'z'))
	{
		f = 1 << (c-'a');
		return (f & x) == 0;
	}
	return false;
}

uint solve(int x)
{
	int i;
	fila = fila2;
	p0.x = 0;
	p0.y = 0;
	p0.cont = 1;
	con[0][0] = x;
	fila.push(p0);
	while(!fila.empty())
	{
		p0 = fila.front();
		fila.pop();
		if(p0.x == N && p0.y == N)
			return p0.cont;
		p1.cont = p0.cont+1;
		for(i = 0; i < 4; i++)
		{
			p1.x = p0.x + dx[i];
			p1.y = p0.y + dy[i];
			if(inRange(p1.x) && inRange(p1.y))
				if(con[p1.x][p1.y] < x)
					if(valido(mat[p1.x][p1.y], x))
					{
						fila.push(p1);
						con[p1.x][p1.y] = x;
					}
		}
	}
	return MAX;
}

main()
{
	int i, j;
	uint v = MAX;
	scanf("%d%*c", &N);
	N--;
	for(i = 0; i <= N; i++)
	{
		scanf("%s", mat[i]);
		for(j = 0; j <= N; j++)
			con[i][j] = -1;
	}
	for(i = 0; i < 1023; i++)
		if(valido(mat[0][0], i))
			v = min(v,solve(i));
	printf("%d\n", v);
	return 0;
}
