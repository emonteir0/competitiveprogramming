// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Repositórios
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2325

#include<bits/stdc++.h>

using namespace std;

map<int, int> Mapa, Mapa2;
map<int, int> :: iterator it;

main()
{
	int n, m, a, b;
	scanf("%d %d", &n, &m);
	while(n--)
	{
		scanf("%d %d", &a, &b);
		Mapa[a] = b;
	}
	while(m--)
	{
		scanf("%d %d", &a, &b);
		if(Mapa[a] < b)
			Mapa2[a] = max(Mapa2[a], b);
	}
	for(it = Mapa2.begin(); it != Mapa2.end(); ++it)
		printf("%d %d\n", it->first, it->second);
	return 0;
}

