// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Very Boring Game
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1699

#include<stdio.h>
#include<math.h>


main()
{
	int n,resto;
	long long int x,sqx;
	while((scanf("%d",&n)==1)&&(n>0))
	{
		resto=0;
		while(n--)
		{
			scanf("%lld",&x);
			sqx=sqrt(x);
			if(sqx*sqx==x)
				resto=(resto+1)%2;
		}
		printf("%s\n",(resto==1)?"Annie":"Garen");
	}
	return 0;
}
