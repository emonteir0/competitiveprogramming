// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Runner Pawns
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1343

#include<bits/stdc++.h>

using namespace std;

int N, t;
int vis[8][8][8][256];
int X[8], Y[8];
int pot[8];
int dx[8] = {1, 1, -1, -1, 2, 2, -2, -2}, dy[8] = {2, -2, 2, -2, 1, -1, 1, -1};
int ans;

vector<int> adj[256];

int inRange(int x, int y)
{
	return x >= 0 && x < 8 && y >= 0 && y < 8;
}

struct pos
{
	int x, y, z, w;
	pos(int x, int y, int z, int w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
};

void bfs(int x, int y, int peoes)
{
	int x2, y2, r, rodada, j;
	pos p = pos(0, 0, 0, 0);
	queue<pos> fila;
	fila.push(pos(x, y, 0, peoes));
	while(!fila.empty())
	{
		p = fila.front();
		fila.pop();
		x = p.x;
		y = p.y;
		rodada = p.z;
		peoes = p.w;
		vis[x][y][rodada][peoes] = t;
		r = rodada+1;
		if(peoes == 0)
		{
			ans = rodada;
			return;
		}
		for(j = 0; j < adj[peoes].size(); j++)
			if(Y[adj[peoes][j]]+rodada == 8)
				break;
		if(j != adj[peoes].size())
			continue;
		for(int k = 0; k < 8; k++)
		{
			x2 = x + dx[k];
			y2 = y + dy[k];
			if(inRange(x2, y2))
			{
				for(j = 0; j < adj[peoes].size(); j++)
				{
					if(x2 == X[adj[peoes][j]] && y2 == Y[adj[peoes][j]]+rodada+1)
						break;
				}
				if(j != adj[peoes].size())
					continue;
				for(j = 0; j < adj[peoes].size(); j++)
				{
					if(x2 == X[adj[peoes][j]] && y2 == Y[adj[peoes][j]]+rodada)
					{
						if(vis[x2][y2][r][peoes-pot[adj[peoes][j]]] < t)
						{
							fila.push(pos(x2, y2, r, peoes-pot[adj[peoes][j]]));
						}
						break;
					}
				}
				if(j == adj[peoes].size() && vis[x2][y2][r][peoes] < t)
					fila.push(pos(x2, y2, r, peoes));
			}		
		}
	}
}


int main()
{
	int bits, x;
	pot[0] = 1;
	for(int j = 1; j < 8; j++)
		pot[j] = pot[j-1] << 1;
	for(int i = 1; i < 256; i++)
	{
		for(int j = 0; j < 8; j++)
			if(i & pot[j])
				adj[i].push_back(j);
	}
	while(scanf("%d", &N) == 1 && N)
	{
		t++;
		ans = 9;
		bits = (1<<N) - 1;
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &x);
			x--;
			X[i] = x%8;
			Y[i] = x/8;
		}
		scanf("%d", &x);
		x--;
		bfs(x%8, x/8, bits);
		if(ans == 9)
			printf("impossible\n");
		else
			printf("%d\n", ans);
	}
	return 0;
}
