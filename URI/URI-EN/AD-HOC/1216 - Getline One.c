// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Getline One
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1216

#include<stdio.h>
#include<string.h>

main()
{
	int n=0;
	char s[100];
	double d,acum=0;
	while(gets(s)!=NULL)
	{
		scanf("%lf%*c",&d);
		acum+=d;
		n++;
	}
	printf("%.1lf\n",acum/n);
	return 0;
}
