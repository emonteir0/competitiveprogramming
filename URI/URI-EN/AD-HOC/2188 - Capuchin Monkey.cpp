// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Capuchin Monkey
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2188

#include<bits/stdc++.h>

using namespace std;

int xa, ya, xb, yb;


main()
{
	int N, X, Y, U, V, b, T = 1;
	while(scanf("%d", &N) == 1 && N)
	{
		b = 1;
		printf("Teste %d\n", T++);
		scanf("%d %d %d %d" , &xa, &ya, &xb, &yb);
		while(--N)
		{
			scanf("%d %d %d %d", &X, &Y, &U, &V);
			if(((X >= xa && X <= xb) || (U >= xa && U <= xb) || (xa >= X && xa <= U) || (xb >= X && xb <= U)) && 
			((Y >= yb && Y <= ya) || (V >= yb && V <= ya) || (ya >= V && ya <= Y) || (yb >= V && yb <= Y)))
			{
				xa = max(X, xa);
				xb = min(U, xb);
				ya = min(ya, Y);
				yb = max(yb, V);
			}
			else
				b = 0;
		}
		if(b)
			printf("%d %d %d %d\n\n", xa, ya, xb, yb);
		else
			printf("nenhum\n\n");
	}
	return 0;
}
