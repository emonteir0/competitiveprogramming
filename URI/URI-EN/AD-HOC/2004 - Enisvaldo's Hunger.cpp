// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Enisvaldo's Hunger
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2004

#include<bits/stdc++.h>

using namespace std;

int alimentos[101];

main()
{
	int Q, N, x, sum, y;
	scanf("%d", &Q);
	while(Q--)
	{
		sum = 0;
		for(int i = 1; i <= 100; i++)
			alimentos[i] = 0;
		scanf("%d", &N);
		while(N--)
		{
			scanf("%d %d", &x, &y);
			if(alimentos[x] >= 10 && alimentos[x] <= 100)
			{
				if(y >= 10 && y <= 100)
					alimentos[x] = max(alimentos[x], y);
			}
			else
			{
				if(y >= 10 && y <= 100)
					alimentos[x] = y;
				else
					alimentos[x] = max(alimentos[x], y);
			}
		}
		for(int i = 1; i <= 100; i++)
			sum += alimentos[i];
		printf("%d\n", sum);
	}
	return 0;
}
