// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Selos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2402

#include<stdio.h>
#include<math.h>
#define ll long long

main()
{
	int i, tam;
	ll n;
	scanf("%lld", &n);
	tam = sqrt(n);
	for(i = 2; i <= tam; i++)
		if(n%i == 0)
		{
			printf("S\n");
			return 0;
		}
	printf("N\n");
	return 0;
}

