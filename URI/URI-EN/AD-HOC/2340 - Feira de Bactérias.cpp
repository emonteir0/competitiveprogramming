// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Feira de Bactérias
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2340

#include<bits/stdc++.h>

using namespace std;

struct bacteria
{
	double x;
	int i;
};

bool cmp(bacteria a, bacteria b)
{
	return a.x < b.x;
}


bacteria vet[50001];

main()
{
	int i, N, x, y;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d %d", &x, &y);
		vet[i].x = y*log(x);
		vet[i].i = i;
	}
	sort(vet, vet+N, cmp);
	printf("%d\n", vet[N-1].i);
	return 0;
}

