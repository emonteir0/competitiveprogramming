// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Butterflies
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1901

#include<bits/stdc++.h>

using namespace std;

set<int> conjunto;

int mat[200][200];

main()
{
	int N,i,j,x,y;
	scanf("%d",&N);
	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			scanf("%d",&mat[i][j]);
	for(i=0;i<2*N;i++)
	{
		scanf("%d %d",&x,&y);
		conjunto.insert(mat[x-1][y-1]);
	}
	printf("%d\n",conjunto.size());
	return 0;
}
