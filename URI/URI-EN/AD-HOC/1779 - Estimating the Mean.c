// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Estimating the Mean
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1779

#include<stdio.h>
   
   
main()
{
    int i,k,n,n2,x,ma,cont,xant,contmax;
    scanf("%d",&n);
    for(k=1;k<=n;k++)
    {
        scanf("%d",&n2);
        scanf("%d",&x);
        ma=x;
        xant=x;
        contmax=0;
        cont=1;
        for(i=1;i<n2;i++)
        {
        	scanf("%d",&x);
        	if(x!=xant)
        	{
        		if(x>ma)
        		{
        			cont=1;
        			contmax=cont;
        			ma=x;
        		}
        		if(contmax<cont)
        		{
        			contmax=cont;
	        	}
	        	cont=1;
        	}
        	if(x==ma&&xant==ma)
        	{
        		cont++;
        	}
        	xant=x;
        }
        if(contmax<cont)
        	contmax=cont;
        printf("Caso #%d: %d\n",k,contmax);
    }
    return 0;
}
