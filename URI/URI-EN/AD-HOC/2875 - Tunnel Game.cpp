// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tunnel Game
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2875

#include<bits/stdc++.h>

using namespace std;

int N, M;
char mat[101][101];
int dx[] = {1, 0, -1, 0};
int dy[] = {0, -1, 0, 1};
int resto[] = {0, 1, 2, 3, 0, 1, 2, 3, 0};

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

main()
{
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	int x, y, x2, y2, cnt, dir, dir2;
	while(cin >> N >> M)
	{
		dir = 0;
		cnt = 0;
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				cin >> mat[i][j];
				if(mat[i][j] == 'X')
				{
					x = i;
					y = j;
				}
				else if(mat[i][j] == '1')
					cnt++;
			}
		while(cnt)
		{
			cnt--;
			for(int i = 3; i <= 5; i++)
			{
				dir2 = resto[dir+i];
				if(inRange(x+dx[dir2], y+dy[dir2]))
				{
					x2 = x+dx[dir2];
					y2 = y+dy[dir2];
					if(mat[x2][y2] == '0')
					{
						if(i == 3)
							printf("L ");
						if(i == 5)
							printf("R ");
						printf("F ");
						dir = dir2;
						x = x2;
						y = y2;
						mat[x][y] = '1';
						continue;
					}
				}
			}
		}
		printf("E\n");
	}
	return 0;
}
