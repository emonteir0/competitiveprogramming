// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bingo!
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1136

#include<cstdio>
#include<cstring>

bool vetb[91];

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	int n,m,i,j,a,b,vet[92];
	while((scanf("%d %d",&n,&m)==2)&&(n||m))
	{
		b=n;
		memset(vetb,false,b+1);
		for(i=0;i<m;i++)
		{
			scanf("%d",&vet[i]);
			for(j=0;j<i;j++)
			{
				a=abs(vet[i]-vet[j]);
				if(!vetb[a])
				{
					vetb[a]=true;
					n--;
				}
			}
		}
		printf("%s\n",(n==0)?"Y":"N");
	}
	return 0;
}
