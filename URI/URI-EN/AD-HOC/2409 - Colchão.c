// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colchão
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2409

#include<stdio.h>

int cabe(int a, int b, int c, int d)
{
	return ((a <= c) && (b <= d)) || ((a <= d) && (b <= c));
}

main()
{
	int A, B, C, W, H;
	scanf("%d %d %d %d %d", &A, &B, &C, &W, &H);
	printf("%s\n", (cabe(A, B, W, H) || cabe(A, C, W, H) || cabe(B, C, W, H))? "S": "N");
	return 0;
}

