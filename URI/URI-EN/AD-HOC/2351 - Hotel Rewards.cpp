// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hotel Rewards
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2351

#include<bits/stdc++.h>

using namespace std;

priority_queue<int> fila;

int vet[100000];

int main()
{
	int i, N, K, ans = 0;
	scanf("%d %d", &N, &K);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(;i--;)
	{
		ans += vet[i];
		fila.push(vet[i]);
		if(i % (K+1) == K)
		{
			ans -= fila.top();
			fila.pop();
		}
	}
	printf("%d\n", ans);
	return 0;
}
