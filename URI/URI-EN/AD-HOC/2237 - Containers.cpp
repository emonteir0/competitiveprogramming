// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Containers
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2237

#include<bits/stdc++.h>
#define MAX 2147483647

using namespace std;

struct container
{
	int a, b, c, d, e, f, g, h;
	bool operator<(const container& rhs) const
    {
        if (a != rhs.a)
			return a < rhs.a;
		if (b != rhs.b)
			return b < rhs.b;
		if (c != rhs.c)
			return c < rhs.c;
		if(d != rhs.d)
			return d < rhs.d;
		if(e != rhs.e)
			return e < rhs.e;
		if(f != rhs.f)
			return f < rhs.f;
		if(g != rhs.g)
			return g < rhs.g;
		return h < rhs.h;
    }
    bool operator==(const container& rhs) const
    {
        return (a == rhs.a) && (b == rhs.b) && (c == rhs.c) && (d == rhs.d) && (e == rhs.e) && (f == rhs.f) && (g == rhs.g) && (h == rhs.h);
    }
};

map<container, int> distancia;
map<container, int> visitado;
set< pair<int, container> > fila;
pair<int, container> p;

int change(container *x, int op)
{
	if(op == 1)
	{
		swap(x->a, x->b);
		return x->a + x->b;
	}
	if(op == 2)
	{
		swap(x->a, x->e);
		return x->a + x->e;
	}
	if(op == 3)
	{
		swap(x->b, x->c);
		return x->b + x->c;
	}
	if(op == 4)
	{
		swap(x->b, x->f);
		return x->b + x->f;
	}
	if(op == 5)
	{
		swap(x->c, x->d);
		return x->c + x->d;
	}
	if(op == 6)
	{
		swap(x->c, x->g);
		return x->c + x->g;
	}
	if(op == 7)
	{
		swap(x->d, x->h);
		return x->d + x->h;
	}
	if(op == 8)
	{
		swap(x->e, x->f);
		return x->e + x->f;
	}
	if(op == 9)
	{
		swap(x->f, x->g);
		return x->f + x->g;
	}
	swap(x->g, x->h);
	return x->g + x->h;
}


int dijkstra(container x, container y)
{
	int dist, i, val;
	container z;
	fila.insert(make_pair(0,x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(fila.begin());
		dist = p.first;
		x = p.second;
		if(x == y)
			return dist;
		if(!visitado[x])
		{
			visitado[x] = true;
			for(i = 1; i<= 10; i++)
			{
				z = x;
				val = change(&z, i);
				if(!visitado[z] && val + dist < distancia[z])
					fila.insert(make_pair((distancia[z]=val+dist),z));
			}
		}
	}
	return MAX;
}

main()
{
	int vet[8];
	container x, y, z;
	scanf("%d %d %d %d %d %d %d %d", &y.a, &y.b, &y.c, &y.d, &y.e, &y.f, &y.g, &y.h);
	scanf("%d %d %d %d %d %d %d %d", &z.a, &z.b, &z.c, &z.d, &z.e, &z.f, &z.g, &z.h);
	vet[0] = y.a;
	vet[1] = y.b;
	vet[2] = y.c;
	vet[3] = y.d;
	vet[4] = y.e;
	vet[5] = y.f;
	vet[6] = y.g;
	vet[7] = y.h;
	sort(vet, vet+8);
	do
	{
		x.a = vet[0];
		x.b = vet[1];
		x.c = vet[2];
		x.d = vet[3];
		x.e = vet[4];
		x.f = vet[5];
		x.g = vet[6];
		x.h = vet[7];
		distancia[x] = MAX;
	}while(next_permutation(vet, vet+8));
	distancia[y] = 0;
	printf("%d\n", dijkstra(y, z));
	return 0;
}

