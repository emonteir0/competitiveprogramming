// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Date Difference
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1619

#include<stdio.h>

int absol(int a)
{
	if(a<0)
		return -a;
	return a;
}

main()
{
	int n,i,a2,a1,m2,m1,d2,d1,days1,days2,mes[12]={0,31,59,90,120,151,181,212,243,273,304,334};
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d%*c%d%*c%d %d%*c%d%*c%d",&a1,&m1,&d1,&a2,&m2,&d2);
		a1--;
		a2--;
		days1=a1*365+a1/4+a1/400-a1/100+d1+mes[m1-1];
		days2=a2*365+a2/4+a2/400-a2/100+d2+mes[m2-1];
		a1++;
		a2++;
		if(m1>2&&((a1%4==0&&a1%100!=0)||(a1%400==0)))
			days1++;
		if(m2>2&&((a2%4==0&&a2%100!=0)||(a2%400==0)))
			days2++;
		printf("%d\n",absol(days2-days1));
	}
	return 0;
}
