// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadradinho de 8
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2438

#include<stdio.h>

main()
{
	int N, x, s = 0, r[8]={1,0,0,0,0,0,0,0};
	long long soma = 0;
	char c;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d", &x);
		s = (s+x)%8;
		soma += r[s];
		r[s]++;
	}
	printf("%lld\n",soma);
	return 0;
}
