// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elevator
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2378

#include<stdio.h>

main()
{
	int N, C, E, S, acum = 0, bol = 1;
	scanf("%d %d", &N, &C);
	while(N--)
	{
		scanf("%d %d", &S, &E);
		acum += E-S;
		if(acum > C)
			bol = 0;
	}
	printf("%s\n", bol?"N":"S");
	return 0;
}

