// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Detective Watson
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1533

#include<stdio.h>

main()
{
	int i,x,max,max2,ima,ima2,n;
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		max=0;
		max2=0;
		ima=0;
		ima2=0;
		for(i=1;i<=n;i++)
		{
			scanf("%d",&x);
			if(x>max)
			{
				max2=max;
				ima2=ima;
				max=x;
				ima=i;
			}
			else if(x>max2)
			{
				max2=x;
				ima2=i;
			}
		}
		printf("%d\n",ima2);
	}
	return 0;
}
