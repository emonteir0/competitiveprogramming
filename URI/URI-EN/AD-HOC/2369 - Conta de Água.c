// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Conta de Água
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2369

#include<stdio.h>

main()
{
	int x;
	scanf("%d", &x);
	if(x <= 10)
		printf("7\n");
	else if(x <= 30)
		printf("%d\n", x - 3);
	else if(x <= 100)
		printf("%d\n", 2*x - 33);
	else
		printf("%d\n", 5*x - 333);
	return 0;
}
