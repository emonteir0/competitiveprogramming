// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Torneio
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2447

#include<bits/stdc++.h>
#define MOD 1000000007
#define ll long long

using namespace std;

int vet[65537];
ll fat[65537];

ll modInverse(ll a, ll b) 
{
	return a > 1LL ? b - ((modInverse(b % a, a) * b)/a) : 1LL;
}

ll inv(ll a)
{
	return modInverse(a, MOD);
}

ll comb(int x, int y)
{
	return (((fat[x] * inv(fat[y])) % MOD) * inv(fat[x-y])) % MOD;
}

main()
{
	int N, K, x, y;
	ll ans, sum = 0;
	scanf("%d %d", &N, &K);
	N = 1 << N;
	K = 1 << K;
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	y = vet[0];
	sort(vet, vet+N);
	x = lower_bound(vet, vet+N, y) - vet + 1;
	fat[0] = 1;
	for(int i = 1; i <= 65536; i++)
		fat[i] = (i * fat[i-1]) % MOD;
	if(K > N)
		printf("%lld\n", (x == N) ? fat[N] : 0);
	else
	{
		if(x < K/2 || x == N)
			printf("0\n");
		else
		{
			ans = (((fat[K/2]*fat[K/2])%MOD) * fat[N-K]) % MOD;
			ans = (ans * comb(x-1, K/2-1)) % MOD;
			for(int i = max(K, x+1); i <= N; i++)
				sum += comb(i-K/2-1, K/2-1);
			ans = (ans * (sum%MOD) ) % MOD;
			ans = (ans * 2 * N / K) % MOD;
			printf("%lld\n", ans);
		}
	}
	return 0;
}
