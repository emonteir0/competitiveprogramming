// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Books Catalog
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1802

#include<bits/stdc++.h>

using namespace std;

int vet[100000],indices[5],valores[5][10];

main()
{	
	int a,i,j,x,y=1,z,sum=0;
	for(i=0;i<5;i++)
	{
		scanf("%d",&a);
		indices[i]=a;
		y*=a;
		for(j=0;j<indices[i];j++)
			scanf("%d",&valores[i][j]);
	}
	z=y;
	for(i=0;i<5;i++)
	{
		a=indices[i];
		y/=a;
		for(j=0;j<z;j++)
			vet[j]+=valores[i][(j/y)%a];
	}
	sort(vet,vet+z);
	scanf("%d",&x);
	for(i=z-1;i>z-1-x;i--)
		sum+=vet[i];
	printf("%d\n",sum);
	return 0;
}

