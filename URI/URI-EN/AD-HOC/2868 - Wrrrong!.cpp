// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Wrrrong!
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2868

#include<bits/stdc++.h>

using namespace std;

string str, ans;


main()
{
	int N, A, B, C;
	char op, n;
	cin.tie(NULL);
	ios_base::sync_with_stdio(0);
	cin >> N;
	cin.ignore();
	while(N--)
	{
		cin >> A;
		cin >> op;
		cin >> B;
		cin >> n;
		cin >> C;
		if(op == '+')
			A = abs((A+B)-C);
		if(op == '-')
			A = abs((A-B)-C);
		if(op == 'x')
			A = abs((A*B)-C);
		A = max(A, 2);
		printf("E");
		for(int i = 0; i < A; i++)
			printf("r");
		printf("ou!\n");
	}
	return 0;
}
