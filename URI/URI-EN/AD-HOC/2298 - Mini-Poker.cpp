// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mini-Poker
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2298

#include<bits/stdc++.h>

using namespace std;

int vet[14], num[5];

main()
{
	int i, j, N, acum;
	char op, bol;
	scanf("%d", &N);
	for(j = 1; j <= N; j++)
	{
		op = 0;
		for(i = 0; i < 5; i++)
			scanf("%d", &num[i]);
		for(i = 1; i <= 13; i++)
			vet[i] = 0;
		sort(num, num+5);
		printf("Teste %d\n", j);
		if((num[0]==num[1]-1) && (num[1] == num[2]-1) && (num[2] == num[3]-1) && (num[3] == num[4]-1))
			printf("%d\n\n", 200+num[0]);
		else
		{
			for(i = 0; i < 5; i++)
				vet[num[i]]++;
			for(i = 1; i <= 13; i++)
			{
				if(vet[i] == 2 && op == 0)
					op = 1;
				else if(vet[i] == 2 && op == 1)
					op = 2;
				else if(vet[i] == 2 && op == 3)
					op = 4;
				else if(vet[i] == 3 && op == 0)
					op = 3;
				else if(vet[i] == 3 && op == 1)
					op = 4;
				else if(vet[i] == 4)
					op = 5;
			}
			acum = 0;
			bol = 0;
			if(op <= 1)
				acum = 0;
			if(op == 2)
				acum = 20;
			if(op == 3)
				acum = 140;
			if(op == 4)
				acum = 160;
			if(op == 5)
				acum = 180;
			for(i = 1; i <= 13; i++)
			{
				if(op == 1 && vet[i] == 2)
					acum += i;
				else if((op == 2) && (vet[i] == 2) && !bol)
				{
					bol = 1;
					acum += 2*i;
				}
				else if((op == 2) && (vet[i] == 2) && bol)
					acum += 3*i;
				else if(op > 2 && vet[i] >= 3)
					acum += i;
			}
			printf("%d\n\n", acum);
		}
	}
	return 0;
}

