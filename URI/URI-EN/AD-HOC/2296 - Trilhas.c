// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trilhas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2296

#include<stdio.h>


int max(int a, int b)
{
	return (a > b) ? a : b;
}

int min(int a, int b)
{
	return (a < b) ? a : b;
}

main()
{
	int i, N, M, x, xant, desc, sob, val, minimo = 999999999, imin = 0;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		scanf("%d %d", &M, &xant);
		M--;
		desc = sob = 0;
		while(M--)
		{
			scanf("%d", &x);
			desc += max(x-xant, 0);
			sob += max(xant-x, 0);
			xant = x;
		}
		val = min(sob, desc);
		if(val < minimo)
		{
			minimo = val;
			imin = i;
		}
	}
	printf("%d\n", imin);
	return 0;
}

