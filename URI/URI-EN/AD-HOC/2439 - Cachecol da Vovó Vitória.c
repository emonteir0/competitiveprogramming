// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cachecol da Vovó Vitória
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2439

#include <stdio.h>
#define MOD 1000000007
#define ll long long int
  
void multiply(int F[27][27]);

void multiply2(int F[27][27]);
  
void power(int F[27][27], ll n);

int F[27][27];
int M[27][27];
ll G[27][27];
 
  
void power(int F[27][27], ll n)
{
  if(n == 1)
      return; 
    power(F, n/2);
    multiply2(F);
  if(n % 2)
     multiply(F);
}
  
void multiply(int F[27][27])
{
    int i, j, k;
    for(i = 0; i < 27; i++)
        for(j = 0; j < 27; j++)
            G[i][j] = 0;
            
    for(i = 0; i < 27; i++)
        for(j = 0 ; j < 27; j++)
            for(k = 0; k < 27; k++)
                G[i][j] += (((ll)F[i][k])*M[k][j]);
                
    for(i = 0; i < 27; i++)
        for(j = 0; j < 27; j++)
            F[i][j] = G[i][j]%MOD;
}

void multiply2(int F[27][27])
{
    int i, j, k;
    for(i = 0; i < 27; i++)
        for(j = 0; j < 27; j++)
            G[i][j] = 0;
            
    for(i = 0; i < 27; i++)
        for(j = 0 ; j < 27; j++)
            for(k = 0; k < 27; k++)
                G[i][j] += (((ll)F[i][k])*F[k][j]);
                
    for(i = 0; i < 27; i++)
        for(j = 0; j < 27; j++)
            F[i][j] = G[i][j]%MOD;
}
 
int diferente(int x, int y)
{
	int i;
	for(i = 0; i < 3; i++)
	{
		if(x%3 == y%3)
			return 0;
		x /= 3;
		y /= 3;
	}
	return 1;
} 
   
void build()
{
	int i, j;
	int v[] = {3, 5, 6, 7, 10, 11, 15, 16, 19, 20, 21, 23};
	for(i = 0; i < 12; i++)
		for(j = 0; j < 12; j++)
			if(diferente(v[i], v[j]))
				F[v[i]][v[j]] = M[v[i]][v[j]] = 1;
}   
   
int main()
{
	int i, j;
	ll n, cont = 0;
	build();
	scanf("%lld", &n);
	if(n == 1)
		printf("12\n");
	else
	{
		power(F, n-1);
		for(i = 0; i < 27; i++)
			for(j = 0; j < 27; j++)
				cont += F[i][j];
		printf("%lld\n", cont%MOD);
	}
	return 0;
}

