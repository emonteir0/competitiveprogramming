// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Musical Loop
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1089

#include<stdio.h>

main()
{
	int n,x0,x1,x2,sig,cont,sig0;
	while(scanf("%d",&n)==1&&n)
	{
		scanf("%d",&x0);
		scanf("%d",&x1);
		sig0=2*(x1>x0)-1;
		n-=2;
		cont=1;
		sig=sig0;
		while(n--)
		{
			scanf("%d",&x2);
			if(x2>x1)
			{
				if(sig!=1)
					cont++;
				sig=1;
			}
			if(x2<x1)
			{
				if(sig!=-1)
					cont++;
				sig=-1;
			}
			x1=x2;
		}
		if(sig==sig0)
			cont++;
		printf("%d\n",cont);
	}
	return 0;
}
