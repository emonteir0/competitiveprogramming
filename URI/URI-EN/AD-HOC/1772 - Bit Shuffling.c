// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bit Shuffling
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1772

#include<stdio.h>


main()
{
    unsigned int a,b,res,max,min,v=1;
    int n,i,o;
    char x,y;
    while(scanf("%u %d",&res,&n)==2&&(res||n))
    {
        max=res;
        min=res;
        while(n--)
        {
            scanf("%d %d",&i,&o);
            a=v<<i;
            b=v<<o;
            x=(a&res)==a;
            y=(b&res)==b;
            res+=(x-y)*(b-a);
            if(max<res)    max=res;
            if(min>res)    min=res;
        }
        printf("%u %u %u\n",res,max,min);
    }
    return 0;
}
