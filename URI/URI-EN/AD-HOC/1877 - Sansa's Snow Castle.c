// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sansa's Snow Castle
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1877

#include<stdio.h>

main()
{
	int i, N, K, cp = 1, cv = 0, x, xant, s = 1;
	scanf("%d %d", &N, &K);
	scanf("%d", &xant);
	for(i = 1; i < N; i++)
	{
		scanf("%d", &x);
		if(s)
		{
			if(x < xant)
			{
				s = 0;
				cv++;
			}
		}
		else
		{
			if(x > xant)
			{
				s = 1;
				cp++;
			}
		}
		xant = x;
	}
	printf("%s\n", ((cp == K) && (cv == K)) ? "beautiful" : "ugly");
	return 0;
}
