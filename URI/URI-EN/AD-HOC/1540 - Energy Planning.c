// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Energy Planning
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1540

#include<stdio.h>
#include<math.h>

main()
{
	int n;
	long long int x,y;
	double a,b,c,d;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%lf %lf %lf %lf",&a,&b,&c,&d);
		a=(d-b)/(c-a);
		x=a;
		y=(a-x)*100;
		printf("%lld,%s%lld\n",x,(y<10)?"0":"",y);
	}
	return 0;
}
