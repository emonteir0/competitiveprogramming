// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Focus
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1459

#include<bits/stdc++.h>

using namespace std;

pair<int, int> vet[1000000];

main()
{
	int N, a, b, cont;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 0; i < N; i++)
			scanf("%d %d", &vet[i].first, &vet[i].second);
		sort(vet, vet+N);
		cont = 1;
		a = vet[0].first;
		b = vet[0].second;
		cont = 1;
		for(int i = 1; i < N; i++)
		{
			if(vet[i].first > b)
			{
				cont++;
				a = vet[i].first;
				b = vet[i].second;
			}
			else
			{
				a = max(vet[i].first, a);
				b = min(vet[i].second, b);
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
