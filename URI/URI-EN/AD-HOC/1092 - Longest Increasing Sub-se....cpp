// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Longest Increasing Sub-se...
// Level: 8
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1092

#include<bits/stdc++.h>

using namespace std;

int N, M;
int mat[600][600], mat2[600][600];
int val[600][600];

int getMaxArea()
{
    int max_area = 0, area;
    int tp, i;
    stack<int> pilha;
    for(int j = 0; j < M; j++)
    {
    	i = 0;
	    while (i < N)
	    {
	        if (pilha.empty() || mat2[pilha.top()][j] <= mat2[i][j])
	            pilha.push(i++);
	        else
	        {
	            tp = pilha.top();
	            pilha.pop();
	            area = mat2[tp][j] * (pilha.empty() ? i : i - pilha.top() - 1);
	            area = max(area, min(mat[i][j], mat2[tp][j]) * (1+ (pilha.empty() ? i : i - pilha.top() - 1)));
	            if (max_area < area)
	                max_area = area;
	        }
	    }
	    while (!pilha.empty())
	    {
	        tp = pilha.top();
	        pilha.pop();
	        area = mat2[tp][j] * (pilha.empty() ? i : i - pilha.top() - 1);
	        if (max_area < area)
	            max_area = area;
	    }
	}
    return max_area;
}

int main()
{
	int ans;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		ans = 1;
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				scanf("%d", &val[i][j]);
		for(int i = 0; i < N; i++)
			mat2[i][M-1] = mat[i][M-1] = 1;
		for(int j = M-1; j--; )
		{
			for(int i = 0; i < N; i++)
			{
				mat2[i][j] = mat[i][j] = (val[i][j] < val[i][j+1]) ? mat[i][j+1] + 1 : 1;
				ans = max(ans, mat[i][j]);
			}
		}
		for(int i = 0; i < N-1; i++)
			for(int j = 0; j < M; j++)
				mat2[i][j] = (lower_bound(val[i]+j, val[i]+j+mat2[i][j], val[i+1][j])-(val[i]+j));
		printf("%d\n", max(ans, getMaxArea())); 
	}
	return 0;
}
