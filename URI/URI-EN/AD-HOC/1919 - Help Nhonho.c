// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Nhonho
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1919

#include<stdio.h>

typedef struct
{
	int a,b,c,d,e;
} valores;


valores vet[36][20];
int tam[36];

main()
{
	int i,j,k,l,m,v,N,x;
	valores valor;
	for(v=10;v<=35;v++)
		for(i=0;i<10;i++)
			for(j=i+1;j<10;j++)
				for(k=j+1;k<10;k++)
					for(l=k+1;l<10;l++)
						for(m=l+1;m<10;m++)
							if(i+j+k+l+m==v)
							{
								valor.a=i;
								valor.b=j;
								valor.c=k;
								valor.d=l;
								valor.e=m;
								vet[v][tam[v]++]=valor;
							}
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&x);
		if(x%266664==0)
		{
			x/=266664;
			if(x>=10 && x<=35)
			{
				if(tam[x]==0)
					printf("impossivel\n\n");
				else
				{
					for(i=0;i<tam[x];i++)
					{
						valor = vet[x][i];
						printf("{%d,%d,%d,%d,%d}\n",valor.a,valor.b,valor.c,valor.d,valor.e);
					}
					printf("\n");
				}
			}
			else
				printf("impossivel\n\n");
		}
		else
			printf("impossivel\n\n");
	}
	return 0;
}

