// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: EBCDIC
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1832

#include<bits/stdc++.h>

using namespace std;

map<int,char> mapa;

main()
{
	string str;
	cin.sync_with_stdio(false);
	mapa[100]=' ';
	mapa[201]='a';
	mapa[202]='b';
	mapa[203]='c';
	mapa[204]='d';
	mapa[205]='e';
	mapa[206]='f';
	mapa[207]='g';
	mapa[210]='h';
	mapa[211]='i';
	mapa[221]='j';
	mapa[222]='k';
	mapa[223]='l';
	mapa[224]='m';
	mapa[225]='n';
	mapa[226]='o';
	mapa[227]='p';
	mapa[230]='q';
	mapa[231]='r';
	mapa[242]='s';
	mapa[243]='t';
	mapa[244]='u';
	mapa[245]='v';
	mapa[246]='w';
	mapa[247]='x';
	mapa[250]='y';
	mapa[251]='z';
	mapa[301]='A';
	mapa[302]='B';
	mapa[303]='C';
	mapa[304]='D';
	mapa[305]='E';
	mapa[306]='F';
	mapa[307]='G';
	mapa[310]='H';
	mapa[311]='I';
	mapa[321]='J';
	mapa[322]='K';
	mapa[323]='L';
	mapa[324]='M';
	mapa[325]='N';
	mapa[326]='O';
	mapa[327]='P';
	mapa[330]='Q';
	mapa[331]='R';
	mapa[342]='S';
	mapa[343]='T';
	mapa[344]='U';
	mapa[345]='V';
	mapa[346]='W';
	mapa[347]='X';
	mapa[350]='Y';
	mapa[351]='Z';
	mapa[360]='0';
	mapa[361]='1';
	mapa[362]='2';
	mapa[363]='3';
	mapa[364]='4';
	mapa[365]='5';
	mapa[366]='6';
	mapa[367]='7';
	mapa[370]='8';
	mapa[371]='9';
	while(getline(cin,str))
	{
		stringstream ss(str);
		while(ss>>str)
			printf("%c",mapa[atoi(str.c_str())]);
		printf("\n");
	}
	return 0;
}
