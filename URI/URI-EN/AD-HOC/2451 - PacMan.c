// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: PacMan
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2451

#include<stdio.h>

char mat[100][101];

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int i, j = -1, N, maximo = 0, cont = 0, dir = 1;
	scanf("%d%*c", &N);
	for(i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(i = 0; i < N; i++)
	{
		j += dir;
		for(; (j < N) && (j >= 0); j+= dir)
		{
			if(mat[i][j] == 'o')
				cont++;
			else if(mat[i][j] == 'A')
			{
				maximo = max(maximo, cont);
				cont = 0;
			}
		}
		dir = -dir;
	}
	printf("%d\n", max(maximo, cont));
	return 0;
}

