// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Do Lado Escuro do Código
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2487

#include<bits/stdc++.h>
#define ll long long

using namespace std;


main()
{
	ll vet[63], n, val[1005];
	ll *lim;
	int i, k, tam;
	vet[0] = 1;
	vet[1] = 1;
	for(i = 2; i < 63; i++)
		vet[i] = vet[i-1] << 1;
	while(scanf("%lld %d", &n, &tam) == 2)
	{
		k = 0;
		while(1)
		{
			lim = vet+63;
			lim = upper_bound(vet+1, lim, n)-1;
			while(*lim + tam-1 > n)
				lim--;
			val[k++] = *lim;
			n -= *lim;
			tam--;
			if(tam == 0)
				break;
		}
		val[0] += n;
		printf("%lld", val[0]);
		for(i = 1; i < k; i++)
			printf(" %lld", val[i]);
		printf("\n");
	}
	return 0;
}
