// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: WWW
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2511

#include<bits/stdc++.h>
#define MAX 1000000

using namespace std;

vector<int> ans;

char isprime[MAX+1];
int vet[MAX+1];

void sieve()
{
	int i, j;
	isprime[1] = 1;
	for(i = 4; i <= MAX; i+=2)
		isprime[i] = 1;
	for(i = 3; i <= MAX; i+=2)
		if(!isprime[i])
			for(j = i+i; j <= MAX; j+=i)
				isprime[j] = 1;
}

main()
{
	int N, i;
	sieve();
	while(scanf("%d", &N) == 1)
	{
		for(i = 1; i <= N; i++)
			scanf("%d", &vet[i]);
		ans.clear();
		for(i = 1; i <= N; i++)
			if(!isprime[vet[i]])
				ans.push_back(vet[i]);
		if(ans.size() == 0)
			printf("*\n");
		else
		{
			printf("%d", ans[0]);
			for(i = 1; i < ans.size(); i++)
				printf(" %d", ans[i]);
			printf("\n");
		}
	}
	return 0;
}
