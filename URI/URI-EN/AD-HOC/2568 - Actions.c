// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Actions
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2568

#include<stdio.h>

main()
{
	int a, b, c, d;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	if((a&1) && (d&1))
		printf("%d\n", b+c);
	else if((a&1) && !(d&1))
		printf("%d\n", b);
	else if(!(a&1) && (d&1))
		printf("%d\n", b-c);
	else
		printf("%d\n", b);
	return 0;
}
