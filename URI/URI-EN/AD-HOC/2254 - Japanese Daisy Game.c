// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Japanese Daisy Game
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2254

#include<stdio.h>

main()
{
	int a, b;
	while(scanf("%d", &a)==1)
	{
		b = 0;
		while(a != 0)
		{
			b ^= a&1;
			a >>= 1;
		}
		printf("%s\n", b? "she loves me" : "she loves not");
	}
	return 0;
}

