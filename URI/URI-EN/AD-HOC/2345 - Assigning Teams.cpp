// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Assigning Teams
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2345

#include<bits/stdc++.h>

using namespace std;

int vet[4];

main()
{
	int minimo;
	scanf("%d %d %d %d", &vet[0], &vet[1], &vet[2], &vet[3]);
	minimo = abs(vet[0]+vet[1]-vet[2]-vet[3]);
	sort(vet, vet+4);
	do
	{
		minimo = min(minimo, abs(vet[0]+vet[1]-vet[2]-vet[3]));
	}while(next_permutation(vet, vet+4));
	printf("%d\n", minimo);
	return 0;
}

