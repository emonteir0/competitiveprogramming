// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Building Walls
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2650

#include<stdio.h>
#include<string.h>

main()
{
	int N, H, x, i, j, lim;
	char msg[101], msg2[101];
	scanf("%d %d%*c", &N, &H);
	while(N--)
	{
		scanf("%[A-Z a-z] %d%*c", msg, &x);
		if(x > H)
		{
			i = 0;
			while(msg[i++] == ' ');
			lim = strlen(msg);
			for(; lim--; )
			{
				if(msg[lim] != ' ')
					break;
			}
			for(i--; i <= lim; i++)
				printf("%c", msg[i]);
			printf("\n");
		}
	}
	return 0;
}
