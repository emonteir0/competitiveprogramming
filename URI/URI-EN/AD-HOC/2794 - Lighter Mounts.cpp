// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lighter Mounts
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2794

#include<bits/stdc++.h>

using namespace std;

pair<int, int> vet[100001];

main()
{
	int N;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%d %d", &vet[i].first, &vet[i].second);
	sort(vet, vet+N);
	for(int i = 1; i < N; i++)
	{
		if(vet[i].second >= vet[i-1].second)
			return !printf("N\n");
	}
	printf("S\n");
	return 0;
}

