// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rulk's Punch
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2151

#include<stdio.h>

int mat[101][101];

int absol(int x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	int N, i, j, k, x, y, n, m, dx, dy, val;
	scanf("%d",&N);
	for(k=1;k<=N;k++)
	{
		scanf("%d %d %d %d",&n,&m,&x,&y);
		for(i=1;i<=n;i++)
			for(j=1;j<=m;j++)
				scanf("%d",&mat[i][j]);
		for(i=1;i<=n;i++)
		{
			for(j=1;j<=m;j++)
			{
				dx = absol(x-i);
				dy = absol(y-j);
				val = 10 - (dx>dy?dx:dy);
				val = val>1?val:1;
				mat[i][j] += val;
			}
		}
		printf("Parede %d:\n", k);
		for(i=1;i<=n;i++)
		{
			printf("%d",mat[i][1]);
			for(j=2;j<=m;j++)
				printf(" %d",mat[i][j]);
			printf("\n");
		}
	}
	return 0;
}

