// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Average
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2771

#include<bits/stdc++.h>

using namespace std;

int somas[1000001];
int vet[101];

main()
{
	int N, K, x, ma;
	while(scanf("%d %d", &N, &K) == 2)
	{
		ma = 0;
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		for(int i = 0; i < N; i++)
			for(int j = i+1; j < N; j++)
				for(int k = j+1; k < N; k++)
				{
					x = vet[i]+vet[j]+vet[k];
					somas[x]++;
					ma = max(ma, x);
				}
		for(int i = ma; i >= 0; i--)
		{
			if(somas[i] >= K)
			{
				printf("%.1lf\n", i/3.0);
				break;
			}
			else
				K -= somas[i];
		}
		for(int i = 0; i <= ma; i++)
			somas[i] = 0;
	}
	return 0;
}
