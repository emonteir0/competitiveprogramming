// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Just a Bit Sorted
// Level: 8
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2009

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
using namespace std;


ll acum[5001][5002];

main()
{
	int i, j, N, M, x;
	for(i = 1; i <= 5000; i++)
	{
		acum[i][0] = 0;
		acum[i][1] = 1;
		for(j = 2; j <= i; j++)
		{
			acum[i][j] = ((i-j+1)*(acum[i-1][j-1]-acum[i-1][j-2]) + (j)*(acum[i-1][j]-acum[i-1][j-1]) + acum[i][j-1]) % MOD;
			if(acum[i][j] < 0)
				acum[i][j] += MOD;
		}
		acum[i][j] = acum[i][j-1];
	}
	scanf("%d %d %d", &N, &M, &x);
	printf("%lld", acum[N][min(x, N)]);
	while(--M)
	{		
		scanf("%d", &x);
		printf(" %lld", acum[N][min(x, N)]);
	}
	printf("\n");
	return 0;
}

