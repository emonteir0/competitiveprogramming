// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Telescópio
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2386

#include<stdio.h>

main()
{
	int X, N, Y, cont = 0;
	scanf("%d %d", &X, &N);
	while(N--)
	{
		scanf("%d", &Y);
		if(X*Y >= 40000000)
			cont++;
	}
	printf("%d\n", cont);
	return 0;
}

