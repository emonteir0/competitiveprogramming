// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Queen
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1087

#include<stdio.h>

main()
{
	int a,b,c,d,e,f;
	while(scanf("%d %d %d %d",&a,&b,&c,&d)==4&&(a||b||c||d))
	{
		e=a-c;
		e*=(e<0?-1:1);
		f=b-d;
		f*=(f<0?-1:1);
		printf("%d\n",(e!=0)+(f!=0)-((e==f)&&(e!=0)&&(f!=0)));
	}
	return 0;
}
