# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Desafio do Maior Número
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2414

valores = raw_input().split(' ')
maximo = 0
for valor in valores:
	maximo = max(maximo, int(valor))
print maximo

