// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: States from the North
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2803

#include<bits/stdc++.h>

using namespace std;

set<string> estados;

main()
{
    string str;
	estados.insert("roraima");
	estados.insert("acre");
	estados.insert("amapa");
	estados.insert("amazonas");
	estados.insert("para");
	estados.insert("rondonia");
	estados.insert("tocantins");
	cin >> str;
	if(estados.count(str) > 0)
		printf("Regiao Norte\n");
	else
		printf("Outra regiao\n");
	return 0;
}
