// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dracarys!
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1849

#include<stdio.h>

main()
{
	int A, B, C, D, max=1, min1, min2;
	scanf("%d %d %d %d",&A,&B,&C,&D);
	min1 = A<C? A:C;
	min2 = min1<B+D?min1:B+D;
	max = max>min2?max:min2;
	
	min1 = B<C? B:C;
	min2 = min1<A+D?min1:A+D;
	max = max>min2?max:min2;
	
	min1 = A<D? A:D;
	min2 = min1<B+C?min1:B+C;
	max = max>min2?max:min2;
	
	min1 = B<D? B:D;
	min2 = min1<A+C?min1:A+C;
	max = max>min2?max:min2;
	
	printf("%d\n",max*max);
	return 0;
}

