// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Discovering Password
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2252

#include<bits/stdc++.h>

using namespace std;

struct val
{
	double x;
	int i;
};

val vet[10];

bool cmp(val a, val b)
{
	if(a.x != b.x)
		return a.x > b.x;
	return a.i < b.i;
}

main()
{
	int N, i, K = 1;
	while(scanf("%d", &N) == 1)
	{
		for(i = 0; i < 10; i++)
		{
			vet[i].i = i;
			scanf("%lf", &vet[i].x);
		}
		sort(vet, vet+10, cmp);
		printf("Caso %d: ", K++);
		for(i = 0; i < N; i++)
			printf("%d", vet[i].i);
		printf("\n");
	}
	return 0;
}

