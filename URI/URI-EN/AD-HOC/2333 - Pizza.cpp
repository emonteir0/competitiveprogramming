// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pizza
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2333

#include<stdio.h>
#define INFN -999999999
int A[100001];
int acum[200001];
int tree[800001];

int max(int a, int b)
{
	return (a > b) ? a : b;
}

void update(int node, int start, int end, int x, int val)
{
	if(start == end)
	{
		tree[node] = val;
		return;
	}
	int mid = (start+end)/2;
	if(x <= mid)
		update(2*node, start, mid, x, val);
	else
		update(2*node+1, mid+1, end, x, val);
	tree[node] = max(tree[2*node], tree[2*node+1]);
}

int query(int node, int start, int end, int l, int r)
{
	if(start > r || end < l)
		return INFN;
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start+end)/2;
	return max(query(2*node, start, mid, l, r), query(2*node+1, mid+1, end, l, r)); 
}

main()
{
	int i, N, N2, best = 0;
	scanf("%d", &N);
	N2 = 2*N-1;
	for(i = 0; i < N; i++)
		scanf("%d", &A[i]);
	update(1, 0, N2, 0, A[0]);
	for(i = 1; i < N; i++)
	{
		acum[i] = acum[i-1] + A[i-1];
		update(1, 0, N2, i, acum[i]+A[i]);
	}
	acum[i] = acum[i-1] + A[i-1];
	update(1, 0, N2, i, acum[i] + A[0]);
	for(i = N+1; i <= N2; i++)
	{
		acum[i] = acum[i-1] + A[i-N-1];
		update(1, 0, N2, i, acum[i]+A[i-N]);
	}
	for(i = 0; i < N; i++)
		best = max(best, query(1, 0, N2, i, i+N-1) - acum[i]);
	printf("%d\n", best);
	return 0;
}
