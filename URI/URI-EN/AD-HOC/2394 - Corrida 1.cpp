// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Corrida 1
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2394

#include<bits/stdc++.h>

using namespace std;

struct carro
{
	int x;
	int i;
};

carro vet[100];

bool cmp(carro a, carro b)
{
	return a.x < b.x;
}

main()
{
	int N, M, i, j, x;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
	{
		vet[i].i = i+1;
		for(j = 0; j < M; j++)
		{
			scanf("%d", &x);
			vet[i].x += x;
		}
	}
	sort(vet, vet+N, cmp);
	printf("%d\n", vet[0].i);
	return 0;
}

