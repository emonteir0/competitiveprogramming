// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Batuke
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2179

#include<stdio.h>

int mat[60][60];

main()
{
	int N, N2, cont, cont2 = 1, k = 1, ds = 0, i, j, x, y;
	scanf("%d %d %d", &N, &x, &y);
	for(i=30; i < 30+N; i++)
		for(j = 30; j < 30+N; j++)
			mat[i][j] = k++;
	N2 = N*N;
	x += 30;
	y += 30;
	cont = 2;
	printf("%d", mat[x][y]);
	while(1)
	{
		ds++;
		for(i = 0; i < ds; i++)
		{
			y++;
			if(mat[x][y] != 0)
			{
				printf(" %d", mat[x][y]);
				cont2++;
				if(cont2 == N2)
					break;
			}
			cont++;
		}
		if(cont2 == N2)
			break;
		for(i = 0; i < ds; i++)
		{
			x++;
			if(mat[x][y] != 0)
			{
				printf(" %d", mat[x][y]);
				cont2++;
				if(cont2 == N2)
					break;
			}
			cont++;
		}
		if(cont2 == N2)
			break;
		ds++; 
		for(i = 0; i < ds; i++)
		{
			y--;
			if(mat[x][y] != 0)
			{
				printf(" %d", mat[x][y]);
				cont2++;
				if(cont2 == N2)
					break;
			}
			cont++;
		}
		if(cont2 == N2)
			break;
		for(i = 0; i < ds; i++)
		{
			x--;
			if(mat[x][y] != 0)
			{
				printf(" %d", mat[x][y]);
				cont2++;
				if(cont2 == N2)
					break;
			}
			cont++;
		}
		if(cont2 == N2)
			break;   
	}
	printf("\n%d\n", cont);
	return 0;
}
