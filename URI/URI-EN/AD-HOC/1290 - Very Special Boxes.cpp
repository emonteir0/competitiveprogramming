// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Very Special Boxes
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1290

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int C[3];
int vet[3];

map< pair<pair<int, int>, int>, int> mapa;
map< pair<pair<int, int>, int>, int>::iterator it;

main()
{
	int N, M, X, Y, Z, vol, ans;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		scanf("%d %d %d", &C[0], &C[1], &C[2]);
		mapa.clear();
		sort(C, C+3);
		vol = C[0]*C[1]*C[2];
		for(int i = 0; i < M; i++)
		{
			scanf("%d %d %d", &vet[0], &vet[1], &vet[2]);
			sort(vet, vet+3);
			mapa[make_pair(make_pair(vet[0], vet[1]), vet[2])]++;
		}
		ans = INF;
		for(it = mapa.begin(); it != mapa.end(); it++)
		{
			if(it->second < N)
				continue;
			X = it->first.first.first;
			Y = it->first.first.second;
			Z = it->first.second;
			if(C[0] <= X && C[1] <= Y && C[2] <= Z)
				ans = min(ans, X*Y*Z-vol);
		}
		if(ans == INF)
			printf("impossible\n");
		else
			printf("%d\n", ans);
	}
	return 0;
}
