// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Exposing Corruption
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2008

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int x, y, v;
};

st operator+(const st st1, const st st2)
{
	st st3;
	st3.x = st1.x + st2.x;
	st3.y = st1.y + st2.y;
	st3.v = st1.v + st2.v;
	return st3;
}

int D, P, R, B;
int best[101][10001];
vector< vector<int> > G(201);
int custo[201];
int vis[201];
vector< pair<int, int> > sack[2];

st dfs(int u)
{
	int i, v;
	st ret;
	ret.x = u <= D;
	ret.y = u > D;
	ret.v = custo[u];
	vis[u] = 1;
	for(i = 0; i < (int)G[u].size(); i++)
	{
		v = G[u][i];
		if(!vis[v])
			ret = ret + dfs(v);
	}
	return ret;
}

int mochila(int x)
{
	int i, j, N = sack[x].size(), t, v;	
	for(i = 1; i <= N; i++)
	{
		for(j = 0; j <= B; j++)
		{
			v = sack[x][i-1].first;
			t = sack[x][i-1].second;
			if(j >= t)
			{
				if(best[i-1][j-t] + v > best[i-1][j])
					best[i][j] = best[i-1][j-t]+v;
				else
					best[i][j]=best[i-1][j];
			}
			else
				best[i][j]=best[i-1][j];
		}
	}
	return best[N][B];
}

int main()
{
	int i, u, v;
	st aux;
	scanf("%d %d %d %d", &D, &P, &R, &B);
	for(i = 1; i <= D+P; i++)
		scanf("%d", &custo[i]);
	while(R--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(D+v);
		G[D+v].push_back(u);
	}
	for(i = 1; i <= D+P; i++)
		if(!vis[i])
		{
			aux = dfs(i);
			if(aux.x > aux.y)
				sack[1].push_back(make_pair(aux.x-aux.y, aux.v));
			if(aux.x < aux.y)
				sack[0].push_back(make_pair(aux.y-aux.x, aux.v));
		}
	printf("%d %d\n", D+mochila(0), P+mochila(1));
	return 0;
}
