// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sorting Santa's List of C...
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2479

#include<bits/stdc++.h>

using namespace std;

vector<string> vet;

main()
{
	char palavra[21];
	char op;
	int cont = 0, i, N;
	string str;
	scanf("%d%*c", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%c %s%*c", &op, palavra); 
		vet.push_back(str = palavra);
		cont += op == '+';
	}
	sort(vet.begin(), vet.end());
	for(i = 0; i < vet.size(); i++)
		printf("%s\n", vet[i].c_str());
	printf("Se comportaram: %d | Nao se comportaram: %d\n", cont, N-cont);
	return 0;
}

