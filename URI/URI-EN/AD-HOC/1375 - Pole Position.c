// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pole Position
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1375

#include<stdio.h>

int pos[1000];
int N;

int inRange(int x)
{
	return (x >= 0) && (x < N);
}

main()
{
	int i, x, y;
	while(scanf("%d", &N) == 1 && N)
	{
		for(i = 0; i < N; i++)
			pos[i] = 0;
		for(i = 0; i < N; i++)
		{
			scanf("%d %d", &x, &y);
			if(inRange(i+y))
				if(pos[i+y] == 0)
					pos[i+y] = x;
				else
					break;
			else
				break;
		}
		if(i == N)
		{
			printf("%d", pos[0]);
			for(i = 1; i < N; i++)
				printf(" %d", pos[i]);
			printf("\n");
		}
		else
		{
			printf("-1\n");
			for(++i; i < N; i++)
				scanf("%*d %*d");
		}
	}
	return 0;
}
