// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cápsulas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2832

#include<bits/stdc++.h>

using namespace std;

int N, F;
int vet[100001];

int teste(int x)
{
	int ans = 0;
	for(int i = 0; i < N; i++)
	{
		ans += x/vet[i];
	}
	return ans >= F;
}

int bb()
{
	int lo = 1, hi = 100000000, mid, ans;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		if(teste(mid))
		{
			ans = mid;
			hi = mid-1;
		}
		else
			lo = mid+1;
	}
	return ans;
}

main()
{
	scanf("%d %d", &N, &F);
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &vet[i]);
	}
	printf("%d\n", bb());
	return 0;
}
