// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rangel's Challenge
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2851

#include<bits/stdc++.h>
#define MAXN 100001

using namespace std;

int vet[MAXN], ans[MAXN];

void printar(int x)
{
	if(ans[x] == -1)
		printf("*");
	else
		printf("%d", ans[x]);
}

main()
{
	int N;
	stack<int> fila;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	ans[N-1] = -1;
	fila.push(vet[N-1]);
	for(int i = N-2; i >= 0; i--)
	{
		while(!fila.empty() && fila.top() <= vet[i])
			fila.pop();
		if(!fila.empty())
			ans[i] = fila.top();
		else
			ans[i] = -1;
		fila.push(vet[i]);
	}
	printar(0);
	for(int i = 1; i < N; i++)
	{
		printf(" ");
		printar(i);
	}
	printf("\n");
	return 0;
}
