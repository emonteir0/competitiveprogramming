// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Aventurando-se no Slackline
// Level: 7
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2876

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
#define MAXN 100000

using namespace std;

int N, M, L, R;

int isprime[MAXN+1], mu[MAXN+1];

void crivo()
{
	for(int i = 1; i <= MAXN; i++)
		mu[i] = 1;
	for(int i = 2; i <= MAXN; i += 2)
	{
		isprime[i] = 1;
		int y = i;
		int e = 0;
		while(y%2 == 0)
		{
			y /= 2;
			e++;
		}
		mu[i] = e == 1 ? -mu[i] : 0;
	}
	for(int j = 3; j <= MAXN; j += 2)
	{
		if(!isprime[j])
		{
			for(int i = j; i <= MAXN; i += j)
			{
				isprime[i] = 1;
				int y = i;
				int e = 0;
				while(y%j == 0)
				{
					y /= j;
					e++;
				}
				mu[i] = e == 1 ? -mu[i] : 0;
			}
		}
	}
}


int raiz(ll N)
{
	if(N <= 0)
		return 0;
	int sq = sqrt(N);
	while(((ll)sq)*sq > N)
		sq--;
	while(((ll)sq)*sq <= N)
		sq++;
	return sq-1;
}

ll soma(int lim, int d)
{
	if(lim == 0)
		return 0;
	ll z = M*((ll)lim)-d*((((ll)lim)*(lim+1))/2);
	//printf("%d %d %d -> %lld\n", M, lim, d, z);
	return z%MOD;
}

ll conta(ll K)
{
	ll sum = 0, sum2;
	for(int d = 1; d <= N; d++)
	{
		sum2 = 0;
		for(int i = 1; i <= N/d; i++)
		{
			sum2 += ((N-d*i)*soma(min(M, raiz(K-(((((ll)i)*i)*d)*d)))/d, d))%MOD;
		}
		sum2 %= MOD;
		sum += (((mu[d]*sum2)%MOD)+MOD)%MOD;
		if(sum >= MOD)
			sum -= MOD;
	}
	//printf("%lld\n", sum);
	return sum;
}

ll solve()
{
	ll z = 2*(conta(((ll)R)*R)-conta(((ll)L)*L-1));
	//printf("%lld\n", z);
	if(L == 1)
	{
	    if(N == 1)
	    {
	        z += M-1;
	    }
	    else if(M == 1)
	    {
	        z += N-1;
	    }
	    else
	    {
	        z += N+M-2 + 2*(N*((ll)M)-N-M+1);
	    }
	}
	z = ((z%MOD)+MOD)%MOD;
	return z;
}

main()
{
	crivo();
	scanf("%d %d %d %d", &N, &M, &L, &R);
	printf("%lld\n", solve());
	return 0;
}
