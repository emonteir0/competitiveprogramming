// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grandpa's Rubik
// Level: 9
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1339

#include<stdio.h>

typedef struct
{
	char vet[54];
} cubo;

cubo cube;

int indices[12][8] =
{
	
	{34, 31, 28, 29, 30, 33, 36, 35},//F
	{34, 35, 36, 33, 30, 29, 28, 31},//f
	{10, 11, 12, 15, 18, 17, 16, 13},//B
	{10, 13, 16, 17, 18, 15, 12, 11},//b
	{19, 20, 21, 24, 27, 26, 25, 22},//U
	{19, 22, 25, 26, 27, 24, 21, 20},//u
	{54, 53, 52, 49, 46, 47, 48, 51},//D
	{54, 51, 48, 47, 46, 49, 52, 53},//d
	{1, 2, 3, 6, 9, 8, 7, 4},//L
	{1, 4, 7, 8, 9, 6, 3, 2},//l
	{45, 44, 43, 40, 37, 38, 39, 42},//R
	{45, 42, 39, 38, 37, 40, 43, 44} //r
};

int indices2[12][12] = 
{
	{9, 6, 3, 27, 24, 21, 45, 42, 39, 54, 51, 48}, //F
	{9, 48, 51, 54, 39, 42, 45, 21, 24, 27, 3, 6}, //f
	{19, 22, 25, 1, 4, 7, 46, 49, 52, 37, 40, 43}, //B
	{19, 43, 40, 37, 52, 49, 46, 7, 4, 1, 25, 22}, //b
	{43, 44, 45, 28, 31, 34, 3, 2, 1, 18, 15, 12}, //U
	{43, 12, 15, 18, 1, 2, 3, 34, 31, 28, 45, 44}, //u
	{39, 38, 37, 10, 13, 16, 7, 8, 9, 36, 33, 30}, //D
	{39, 30, 33, 36, 9, 8, 7, 16, 13, 10, 37, 38}, //d
	{16, 17, 18, 25, 26, 27, 34, 35, 36, 48, 47, 46}, //L
	{16, 46, 47, 48, 36, 35, 34, 27, 26, 25, 18, 17}, //l
	{21, 20, 19, 12, 11, 10, 52, 53, 54, 30, 29, 28}, //R
	{21, 28, 29, 30, 54, 53, 52, 10, 11, 12, 19, 20} //r
};

int indleitura[54] = 
{
	16, 13, 10, 17, 14, 11, 18, 15, 12, 7, 4, 1, 25, 22, 19, 43, 40, 37, 52, 49, 46, 8, 5, 2, 26, 23, 20, 44, 41, 38,
	53, 50, 47, 9, 6, 3, 27, 24, 21, 45, 42, 39, 54, 51, 48, 34, 31, 28, 35, 32, 29, 36, 33, 30
};

cubo change(cubo a, int* ind, int* ind2)
{
	int i;
	cubo b = a;
	for(i = 0; i < 54; i++)
		b.vet[i] = a.vet[i];
	for(i = 0; i < 8; i++)
		b.vet[ind[(i+2)%8]-1] = a.vet[ind[i]-1];
	for(i = 0; i < 12; i++)
		b.vet[ind2[(i+3)%12]-1] = a.vet[ind2[i]-1];
	return b;
}

cubo matriz(cubo a, int c)
{
	if(c == 6)
		return change(a, indices[0], indices2[0]);
	if(c == -6)
		return change(a, indices[1], indices2[1]);
	if(c == 5)
		return change(a, indices[2], indices2[2]);
	if(c == -5)
		return change(a, indices[3], indices2[3]);
	if(c == 2)
		return change(a, indices[4], indices2[4]);
	if(c == -2)
		return change(a, indices[5], indices2[5]);
	if(c == 4)
		return change(a, indices[6], indices2[6]);
	if(c == -4)
		return change(a, indices[7], indices2[7]);
	if(c == 1)
		return change(a, indices[8], indices2[8]);
	if(c == -1)
		return change(a, indices[9], indices2[9]);
	if(c == 3)
		return change(a, indices[10], indices2[10]);
	return change(a, indices[11], indices2[11]);
}

void printcube(cubo a)
{
	int i, j;
	for(i = 0; i < 54; i+=9)
	{
		for(j = 0; j < 9; j++)
			printf("%c ", cube.vet[i+j]);
		printf("\n");
	}
}

main()
{
	int i, j, k, N, ind, cont, op, x;
	char c;
	scanf("%d%*c", &N);
	while(N--)
	{
		cont = 0;
		ind = 0;
		while(scanf("%c", &c) == 1)
		{
			if(c == '\n')
				cont++;
			if(cont == 9)
				break;
			if(c >= 'A' && c <= 'Z')
			{
				cube.vet[indleitura[ind]-1] = c;
				ind++;
			}
		}
		//printcube(cube);
		while(scanf("%d%*c", &op)==1 && op)
			cube = matriz(cube, op);
		x = 1;
		for(i = 0; i < 54; i += 9)
		{
			c = cube.vet[i];
			for(j = 1; j < 9; j++)
				x &= (c == cube.vet[i+j]);
		}
		//printcube(cube);
		printf("%s\n", x?"Yes, grandpa!":"No, you are wrong!");
	}
	return 0;
}


