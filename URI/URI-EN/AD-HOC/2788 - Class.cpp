// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Class
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2788

#include<bits/stdc++.h>
#define ll long long
#define INF 1001001001001001001LL
using namespace std;

main()
{
	ll N, num, me = INF, pot[18];
	pot[0] = 1;
	for(int i = 1; i < 18; i++)
		pot[i] = pot[i-1] * 10;
	ll res = 0, teste, M, ok = 0, dig;
	scanf("%lld %lld", &N, &M);
	dig = upper_bound(pot, pot+18, N)-pot;

	res = N%M;
	if(res == 0)
		printf("%lld\n", N);
	else
	{
		for(int i = 1; i <= 6; i++)
		{
			for(int j = 0; j <= i; j++)
			{
				//printf("%d %d %lld?\n", i, j, pot[j]);
				for(int k = 0; k < pot[j]; k++)
				{
					num = (k * pot[dig] + N) * pot[i-j];
					teste = M - (num % M);
					//printf("%d %d %lld %lld*\n", i, j, num, teste);
					if(teste == M)
					{
						ok = 1;
						me = min(me, num);
					}
					else
					{
						//printf("%d %d %lld %d %d\n", i, j, num, teste, (upper_bound(pot, pot+18, teste) - pot));
						if((upper_bound(pot, pot+18, teste) - pot)<= i-j)
						{
							ok = 1;
							//printf("OK\n");
							me = min(me, num+teste);
						}
					}
				}
			}
			if(ok)
			{
				printf("%lld\n", me);
				break;
			}
			//scanf("%*d");
		}
	}
	return 0;
}

