// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Times
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2370

#include<bits/stdc++.h>

using namespace std;

set<string> times[1000];
set<string>::iterator it;

struct cara
{
	string nome;
	int val;
};

cara vet[10000];

bool cmp(cara c1, cara c2)
{
	return c1.val > c2.val;
}

main()
{
	int i, j, N, M;
	char nome[101];
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
	{
		scanf("%s %d%*c", nome, &vet[i].val);
		vet[i].nome = nome;
	}
	sort(vet, vet+N, cmp);
	for(i = 0; i < N; i += M)
		for(j = i; j < i + M && j < N; j++)
			times[j-i].insert(vet[j].nome);
	for(j = 0; j < M; j++)
	{
		printf("Time %d\n", j+1);
		for(it = times[j].begin(); it != times[j].end(); ++it)
			printf("%s\n", it->c_str());
		printf("\n");
	}
	return 0;
}
