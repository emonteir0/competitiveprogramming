// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lençol
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2431

#include<stdio.h>

int X, Y;

int min(int a, int b)
{
	return (a < b) ? a : b;
}

int f(int A, int B)
{
	return ((A >= X) && (B >= Y)) || ((A >= Y) && (B >= X));
}

main()
{
	int A, B, C, D;
	char bol = 0;
	scanf("%d %d %d %d %d %d", &A, &B, &C, &D, &X, &Y);
	bol |= f(A+C, min(B, D));
	bol |= f(A+D, min(B, C));
	bol |= f(B+C, min(A, D));
	bol |= f(B+D, min(A, C));
	bol |= f(A, B);
	bol |= f(C, D);
	printf("%s\n", bol ? "S" : "N");
	return 0;
}

