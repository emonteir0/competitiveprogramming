// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flavious Josephus Legend
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1030

#include<stdio.h>
  
  
typedef struct a
{
    int ant,dep;
}tipo;
  
int lista[10000][2];
int i,n,p,inicio,x,a,b,N,j;
char u; 
  
main()
{   
    scanf("%d",&N);  
    for(j=1;j<=N;j++)
    {
        scanf("%d %d",&n,&p);
        for(i=0;i<n;i++)
        {
            lista[i][0]=(i+n-1)%n;
            lista[i][1]=(i+1)%n;
        }
        inicio=n-1;
        while(1)
        {
            x=p%n;
            for(i=0;i<x;i++)
                inicio=lista[inicio][1];
            a=lista[inicio][0];
            b=lista[inicio][1];
            if(a==b)
                break;
            lista[a][1]=b;
            lista[b][0]=a;  
            n--;  
            inicio=lista[inicio][0]; 
        }
        printf("Case %d: %d\n",j,a+1);
    }
    return 0;
}
