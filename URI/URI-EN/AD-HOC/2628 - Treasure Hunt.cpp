// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Treasure Hunt
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2628

#include <bits/stdc++.h>
using namespace std;

#define INF 1000000000
#define MAX 100000

int n, primes[100500], iscomp[100500], isnode[100500];
int m, x, adj[111][111], dist[20][20], memo[20][1<<20];

inline void crivo()
{
    iscomp[1] = 1;
	long long i, j, prime_cnt = 0;
	primes[prime_cnt++] = 2;
	for(i = 4; i <= MAX; i += 2)
		iscomp[i] = 1;
	for(i = 3; i <= MAX; i += 2) {
		if(!iscomp[i]) {
			primes[prime_cnt++] = i;
			for(j = i*i; j <= MAX; j += i)
				iscomp[j] = 1;
		} } }

int tsp(int u, int mask, int n) {
    if (mask == (1<<n)-1) return dist[u][0];
    if (memo[u][mask] != -1) return memo[u][mask];
    memo[u][mask] = INF;
    for (int v = 0; v < n; v++)
        if (!(mask&(1<<v)))
            memo[u][mask] = min(memo[u][mask],
                    tsp(v, mask|(1<<v), n)+dist[u][v]);
    return memo[u][mask];
}

int main() {
    crivo();
    while (scanf("%d %d", &n, &m) != EOF) {
        int N = n+1;
        vector <int> nodes;
        nodes.push_back(N);
        for (int i = 1; i <= n; i++) {
            scanf("%d", &x);
            if (!iscomp[x]) {
                nodes.push_back(i);
            }
        }

        for (int i = 0; i <= N; i++)
            for (int j = i; j <= N; j++)
                adj[i][j] = adj[j][i] = (i!=j)*INF;

        int u, v, w;
        while (m--) {
            scanf("%d %d %d", &u, &v, &w);
            adj[u][v] = adj[v][u] = min(adj[u][v], w);
        }
		
        for (int k = 1; k <= N; k++)
	        for (int i = 1; i <= N; i++)
	            for (int j = 1; j <= N; j++)
	                    adj[i][j] = min(adj[i][j], adj[i][k]+adj[k][j]);

        /*
        cout << "adj:\n";
        for (int i = 1; i <= N; i++) {
            for (int j = 1; j<= N; j++) {
                cout << adj[i][j] << ' ';
            }
            cout << endl;
        }
        */

        int sz = nodes.size();
              for (int i = 0; i < sz; i++)
            for (int j = i; j < sz; j++)
                dist[i][j] = dist[j][i] = (i!=j)*INF;
      
        for (int i = 0; i < sz; i++)
            for (int j = i; j < sz; j++)
                dist[i][j] = dist[j][i] = adj[nodes[i]][nodes[j]];

        /*
        cout << "nodes:";
        for (int x : nodes) {
            cout << ' ' << x;
        }
        cout << endl;
        cout << "dist:\n";
        for (int i = 0; i < sz; i++) {
            for (int j = 0; j< sz; j++) {
                cout << dist[i][j] << ' ';
            }
            cout << endl;
        }
        */

        for (int i = 0; i < sz; i++)
            for (int j = 0; j <= (1<<sz); j++)
                memo[i][j] = -1;
        printf("%d\n", tsp(0, 1, sz));
    }

    return 0;
}

