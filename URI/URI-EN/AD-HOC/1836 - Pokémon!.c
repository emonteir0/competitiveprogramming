// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pokémon!
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1836

#include<stdio.h>
#include<math.h>


main()
{
	char nome[31];
	double level, ev, base, iv;
	int i,n;
	scanf("%d%*c",&n);
	for(i=1;i<=n;i++)
	{
		scanf("%s %lf%*c",nome,&level);
		printf("Caso #%d: %s nivel %.0lf\n",i,nome,level);
		scanf("%lf %lf %lf%*c",&base,&iv,&ev);
		printf("HP: %.0lf\n",floor((iv+base+sqrt(ev)/8+50)*level/50.0+10));
		scanf("%lf %lf %lf%*c",&base,&iv,&ev);
		printf("AT: %.0lf\n",floor((iv+base+sqrt(ev)/8)*level/50.0+5));
		scanf("%lf %lf %lf%*c",&base,&iv,&ev);
		printf("DF: %.0lf\n",floor((iv+base+sqrt(ev)/8)*level/50.0+5));
		scanf("%lf %lf %lf%*c",&base,&iv,&ev);
		printf("SP: %.0lf\n",floor((iv+base+sqrt(ev)/8)*level/50.0+5));
	}
	return 0;
}
