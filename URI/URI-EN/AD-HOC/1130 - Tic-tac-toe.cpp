// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tic-tac-toe
// Level: 9
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1130

#include<bits/stdc++.h>

using namespace std;
int grundy[10001], vet2[10001];
int N;

string str;

int func(int n)
{
	if(n <= 3)
		return 0;
	return grundy[n-3];
}

int func2(int n)
{
	if(n + 2 > N)
		return 0;
	return grundy[N-n-2];
}

int mex(int n)
{
	int i, vet[n+1];
	for(i = 0; i <= n; i++)
		vet[i] = 0;
	for(i = 1; i <= n; i++)
		vet[func(i) ^ func2 (i)] = 1;
	for(i = 0; i <= n; i++)
		if(!vet[i])
			break;
	return i;
}

int main()
{
	int i, j, a, c;
	for(N = 1; N <= 10000; N++)
		grundy[N] = mex(N);
	while(scanf("%d%*c", &N) == 1 && N)
	{
		cin >> str;
		if(((int)str.find("XX") != -1) || ((int)str.find("X.X") != -1))
			printf("S\n");
		else
		{
			c = 0;
			a = 0;
			for(i = 0; i < N; i++)
				vet2[i] = 0;
			vet2[N] = 1;
			for(i = 0; i < N; i++)
				if(str[i] == 'X')
				{
					for(j = -2; j <= 2; j++)
						if(((i+j) >= 0) && ((i+j) < N)) 
							vet2[i+j] = 1;
				}
			for(i = 0; i <= N; i++)
			{
				if(vet2[i])
				{
					c ^= grundy[a];
					a = 0;
				}
				else
					a++;
			}
			printf("%s\n", c ? "S" : "N");
		}
	}
	return 0;
}

