// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Telemarketing
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2330

#include<bits/stdc++.h>

using namespace std;

set< pair<int, int> > filas;
set<int> livres;
int ligacoes[1001];

main()
{
	int n, m, i, t0 = 0, x, ind;
	scanf("%d %d", &n, &m);
	for(i = 1; i <= n; i++)
		livres.insert(i);
	while(m--)
	{
		scanf("%d", &x);
		if(livres.size() > 0)
		{
			ind = *livres.begin();
			livres.erase(ind);
			filas.insert(make_pair(t0 + x, ind));
			ligacoes[ind]++;
		}
		else
		{
			t0 = (filas.begin())->first;
			ind = (filas.begin())->second;
			filas.erase(filas.begin());
			livres.insert(ind);
			while(filas.size() > 0)
			{
				if((filas.begin())->first != t0)
					break;
				ind = (filas.begin())->second;
				livres.insert(ind);
				filas.erase(filas.begin());
			}
			ind = *livres.begin();
			livres.erase(ind);
			filas.insert(make_pair(t0 + x, ind));
			ligacoes[ind]++; 
		}
	}
	for(i = 1; i <= n; i++)
		printf("%d %d\n", i, ligacoes[i]);
	return 0;
}

