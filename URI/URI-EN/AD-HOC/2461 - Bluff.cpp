// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bluff
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2461

#include<bits/stdc++.h>

using namespace std;

int A[1000];
int B[10000];
set<int> Bvazio;
set<int>::iterator it;

main()
{
	int N, M, b = 1, x;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%d", &A[i]);
	sort(A, A+N);
	for(int i = 0; i < M; i++)
		scanf("%d", &B[i]);
	for(int i = 0; i < M && b; i++)
	{
		if(binary_search(A, A+N, B[i]))
			Bvazio.insert(B[i]);
		else
		{
			for(it = Bvazio.begin(); it != Bvazio.end(); it++)
			{
				x = *it;
				if(B[i]-x < x)
				{
					printf("%d\n", B[i]);
					b = 0;
					break;
				}
				if(Bvazio.find(B[i]-x) != Bvazio.end())
					break;
			}
			Bvazio.insert(B[i]);
		}
	}
	if(b)
		printf("sim\n");
	return 0;
}
