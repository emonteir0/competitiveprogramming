// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Searching for Nessy
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1428

#include<stdio.h>

main()
{
	int N, x, y;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d", &x, &y);
		printf("%d\n", (x/3)*(y/3));
	}
	return 0;
}
