// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tacógrafo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2388

#include<stdio.h>

main()
{
	int N, x, y, acum = 0;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d", &x, &y);
		acum += x*y;
	}
	printf("%d\n", acum);
	return 0;
}

