// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Folding
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2229

#include<stdio.h>

main()
{
    int n, x, k = 1;
	while(scanf("%d", &n) == 1 && n != -1)
		printf("Teste %d\n%d\n\n",k++, ((1<<n)+1)*((1<<n)+1));
    return 0;
}
