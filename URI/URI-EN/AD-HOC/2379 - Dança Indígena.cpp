// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dança Indígena
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2379

#include<bits/stdc++.h>

using namespace std;

int mat[1000][1000];
int val[1000];
int d[1000];


main()
{
	int E, N, me = 1001001001;
	scanf("%d %d", &E, &N);
	for(int i = 0; i < N; i++)
		scanf("%d %d", &val[i], &d[i]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
		{
			if(i == j)
				mat[i][j] = E;
			else
				mat[i][j] = ((val[i]-val[j])*d[j] + E ) % E;
		}
	for(int i = 0 ; i < N; i++)
		sort(mat[i], mat[i]+N);
	for(int i = 0; i < N; i++)
	{
		int j;
		for(j = 1; j < N; j++)
			if(!binary_search(mat[j], mat[j]+N, mat[0][i]))
				break;
		if(j == N)
			me = min(me, mat[0][i]);
	}
	printf("%d\n", me);
	return 0;
}
