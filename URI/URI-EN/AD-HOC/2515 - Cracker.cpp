// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cracker
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2515

#include<bits/stdc++.h>

using namespace std;

int vet[100001], vet2[100001], val[100001];

main()
{
	int i, j, N, x, metade;
	while(scanf("%d", &N) == 1)
	{
		for(i = 1; i <= N; i++)
			scanf("%d", &val[i]);
		for(i = 1; i <= N; i++)
		{
			vet[i] = vet[i-1] + val[i];
			vet2[i] = vet2[i-1] + val[N+1-i];
		}
		metade = vet[N]/2;
		i = upper_bound(vet, vet+N+1, metade) - vet - 1;
		j = upper_bound(vet2, vet2+N+1, metade) - vet2 - 1;
		if(vet[i] > vet2[j])
			printf("%d %d\n", vet[i], vet[N]-vet[i]);
		else
			printf("%d %d\n", vet2[j], vet2[N] - vet2[j]);
	}
	return 0;
}
