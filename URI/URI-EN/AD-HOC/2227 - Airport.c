// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Airport
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2227

#include<stdio.h>

int vet[101];

main()
{
	int n, m, a, b, ma, i, k=1;
	while(scanf("%d %d", &n, &m) == 2 && (n||m))
	{
		for(i=1;i<=n;i++)
			vet[i] = 0;
		while(m--)
		{
			scanf("%d %d", &a, &b);
			vet[a]++;
			vet[b]++;
		}
		ma = 0;
		printf("Teste %d\n", k++);
		for(i = 1; i <= n; i++)
			ma = ma > vet[i]? ma: vet[i];
		for(i=1;i<=n;i++)
			if(vet[i]==ma)
				printf("%d ", i);
		printf("\n\n");
	}
	return 0;
}
