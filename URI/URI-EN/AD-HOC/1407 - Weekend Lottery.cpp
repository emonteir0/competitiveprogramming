// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Weekend Lottery
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1407

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int val, id;
	st()
	{
	}
	st(int val, int id)
	{
		this->val = val;
		this->id = id;
	}
};

st vet[101];

bool cmp(st a, st b)
{
	if(a.val != b.val)
		return a.val < b.val;
	return a.id < b.id;
}

main()
{
	int N, C, M, x;
	while(scanf("%d %d %d", &M, &C, &N) == 3 && N)
	{
		for(int i = 1; i <= N; i++)
			vet[i] = st(0, i);
		while(M--)
		{
			for(int i = 0; i < C; i++)
			{
				scanf("%d",&x);
				vet[x].val++;
			}
		}
		sort(vet+1, vet+N+1, cmp);
		printf("%d", vet[1].id);
		x = 2;
		while(vet[1].val == vet[x].val)
			printf(" %d", vet[x++].id);
		printf("\n");
	}
	return 0;
}
