// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pesos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2831

#include<bits/stdc++.h>

using namespace std;

int vet[100001];

main()
{
	int N;
	scanf("%d", &N);
	for(int i = 1; i <= N; i++)
		scanf("%d", &vet[i]);
	for(int i = 1; i <= N; i++)
	{
		if(vet[i]-vet[i-1] > 8)
			return !printf("N\n");
	}
	printf("S\n");
	return 0;
}
