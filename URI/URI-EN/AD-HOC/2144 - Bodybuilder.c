// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bodybuilder
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2144

#include<stdio.h>

main()
{
	int w1, w2, r, c, cont = 0, acum = 0;
	while(scanf("%d %d %d",&w1,&w2,&r)==3 && (w1||w2||r))
	{
		c = (w1+w2)*(30+r);
		if(60 <= c && c < 780)
			printf("Nao vai da nao\n");
		else if(c < 840)
			printf("E 13\n");
		else if(c < 2400)
			printf("Bora, hora do show! BIIR!\n");
		else if(c < 3600)
			printf("Ta saindo da jaula o monstro!\n");
		else
			printf("AQUI E BODYBUILDER!!\n");
		cont++;
		acum += c;
	}
	if (cont != 0 && ((double)acum)/(60*cont) > 40)
		printf("\nAqui nois constroi fibra rapaz! Nao e agua com musculo!\n");
	return 0;
}

