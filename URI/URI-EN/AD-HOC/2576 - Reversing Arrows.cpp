// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Reversing Arrows
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2576

#include<bits/stdc++.h>
#define MAXN 10000
#define MAX 999999999

using namespace std;

int N, M;

vector< vector< pair<int, int> > > G(MAXN+1);
int distancia[MAXN+1], vis[MAXN+1];

void dijkstra(int x)
{
	int i, u, v, w, d;
	set<pair<int, int> > fila;
	pair<int, int> p;
	for(i = 0; i <= N; i++)
	{
		vis[i] = 0;
		distancia[i] = MAX;
	}
	distancia[x] = 0;
	fila.insert(make_pair(0, x));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		u = p.second;
		d = p.first;
		if(!vis[u])
		{
			vis[u] = 1;
			for(i = 0; i < G[u].size(); i++)
			{
				v = G[u][i].first;
				w = G[u][i].second;
				if(!vis[v] && d + w < distancia[v])
					fila.insert(make_pair(distancia[v] = d+w, v));
			}	
		}
	}
}

main()
{
	int i, j, u, v, s, d, x, y;	
	scanf("%d %d %d %d", &N, &M, &s, &d);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(make_pair(v, 0));
		G[v].push_back(make_pair(u, 1));
	}
	dijkstra(s);
	x = distancia[d];
	dijkstra(d);
	y = distancia[s];
	if(x == y)
		printf("Bibibibika\n");
	else if(x < y)
		printf("Bibi: %d\n", x);
	else
		printf("Bibika: %d\n", y);
	return 0;
}
