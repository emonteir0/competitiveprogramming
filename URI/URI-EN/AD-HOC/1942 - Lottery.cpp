// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lottery
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1942

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll vet[10000];

main()
{
	int N, M, i, j, x, c;
	ll f;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			scanf("%d", &x);
			vet[i] = (vet[i]<<1) + (x&1);
		}
	}
	f = 1LL << (M-1);
	for(c = 0; c < M; c++)
	{
		for(i = c; i < N; i++)
			if(f & vet[i])
			{
				swap(vet[i], vet[c]);
				break;
			}
		if(i >= N)
			break;
		for(i = c+1; i < N; i++)
			if(vet[i] & f)
				vet[i] ^= vet[c];
		f >>= 1;
	}
	printf("%c\n", (c!=M)?'S':((vet[N-1] == 0)?'N':'S'));
	return 0;
}

