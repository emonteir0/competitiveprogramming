// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Carnaval
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2418

#include<stdio.h>

double max(double a, double b)
{
	return (a > b) ? a : b;
}

double min(double a, double b)
{
	return (a < b) ? a : b;
}

main()
{
	double ma = 0, me = 50, x, acum = 0;
	int N = 5;
	while(N--)
	{
		scanf("%lf", &x);
		ma = max(ma, x);
		me = min(me, x);
		acum +=x;
	}
	printf("%.1lf\n", acum-ma-me);
	return 0;
}

