// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Moon Phases
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1893

#include<stdio.h>

main()
{
	int a,b;
	scanf("%d %d",&a,&b);
	if(b>=3&&b<=96)
		printf("%s\n",a>b?"minguante":"crescente");
	else if(b<3)	printf("nova\n");
	else	printf("cheia\n");
	return 0;
}
