// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tetris
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2250

#include<bits/stdc++.h>

using namespace std;

struct val
{
	string nome;
	int x;
};

val vet[1000];

bool cmp(val a, val b)
{
	if(a.x != b.x)
		return a.x > b.x;
	return a.nome < b.nome;
}

main()
{
	int N, i, j, K = 1, xant, iant;
	int valores[12];
	char nome[100];
	while(scanf("%d%*c", &N) == 1 && N)
	{
		for(i = 0; i < N; i++)
		{
			scanf("%s", nome);
			vet[i].x = 0;
			vet[i].nome = nome;
			for(j = 0; j < 12; j++)
			{
				scanf("%d%*c", &valores[j]);
				vet[i].x += valores[j];
			}
			sort(valores, valores+12);
			vet[i].x -= valores[0] + valores[11];
		}
		sort(vet, vet+N, cmp);
		printf("Teste %d\n", K++);
		printf("1 %d %s\n", vet[0].x, vet[0].nome.c_str());
		xant = vet[0].x;
		iant = 1;
		for(i = 1; i < N; i++)
		{
			if(vet[i].x == xant)
				printf("%d %d %s\n", iant, vet[i].x, vet[i].nome.c_str());
			else
				printf("%d %d %s\n", iant = i+1, xant = vet[i].x, vet[i].nome.c_str());
		}
		printf("\n");
	}
	return 0;
}

