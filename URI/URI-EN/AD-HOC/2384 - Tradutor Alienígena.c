// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tradutor Alienígena
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2384

#include<stdio.h>
#include<string.h>
#define MOD 1000000007

int valores[100001];
char chave[101];
char palavra[100001];
int tam, tamchave;

int solve(int pos)
{
	int i, resp = 0;
	if(pos >= tam)
		return 1;
	if(valores[pos] != -1)
		return valores[pos];
	for(i = 1; pos+i <= tam; i++)
	{
		if(i == tamchave)
		{
			if(strncmp(palavra+pos, chave, tamchave) <= 0)
				resp = (resp + solve(pos + i))%MOD;
			break;
		}
		resp = (resp + solve(pos + i))%MOD;
	}
	return valores[pos] = resp;
}

main()
{
	int i;
	scanf("%s%*c", chave);
	scanf("%s%*c", palavra);
	tam = strlen(palavra);
	tamchave = strlen(chave);
	for(i = 0; i < tam; i++)
		valores[i] = (palavra[i] == '0')? 0 : -1;
	printf("%d\n", solve(0));
	return 0;
}

