// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jogo do Labirinto
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2332

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;


set< pair<int, pair<int, int> > > fila, fila2;
pair<int, pair<int, int> > p;
char mat[51][51];
bool visitado[51][51];
int N, M;
int distancia[51][51];
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};

bool inRange(int a, int b)
{
	return a >= 1 && a <= N && b >= 1 && b <= M;
}

void dijkstra()
{
	int a, x, y, x2, y2, i, j, v, f, g;
	pair<int, int> pos;
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= M; j++)
		{
			visitado[i][j] = 0;
			distancia[i][j] = INFINITO;
		}
	}
	distancia[1][1] = 0;
	fila.insert(make_pair(0,make_pair(1,1)));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		pos = p.second;
		x = pos.first;
		y = pos.second;
		if(x == N && y == M)
			break;
		if(!visitado[x][y])
		{
			visitado[x][y] = true;
			for(i = 0; i < 4; i++)
			{
				x2 = x+dx[i];
				y2 = y+dy[i];
				if(inRange(x2,y2))
				{
					f = (mat[x][y] + a)%10;
					g = (mat[x2][y2] + a)%10;
					v = (f + 1 >= g) ? 1 : 11 - g;
					if(!visitado[x2][y2] && (v + a < distancia[x2][y2]))
						fila.insert(make_pair((distancia[x2][y2]=v+a),make_pair(x2,y2)));
				}
			}
		}
	}
}

int main()
{
	int i, j;
	scanf("%d %d", &N, &M);
	for(i = 1; i <= N; i++)
		for(j = 1; j <= M; j++)
			scanf("%d",&mat[i][j]);
	dijkstra();
	printf("%d\n",distancia[N][M]);	
	return 0;
}

