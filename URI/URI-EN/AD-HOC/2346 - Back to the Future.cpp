// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Back to the Future
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2346

#include<bits/stdc++.h>

using namespace std;

vector< vector<int> > G(100001);
set< pair<int, int> > conju;
int deg[100001];

main()
{
	int i, N, M, A, B;
	int d, u, v;
	char bol;
	pair<int, int> p;
	scanf("%d %d %d %d", &N, &M, &A, &B);
	while(M--)
	{
		scanf("%d %d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
		deg[u]++;
		deg[v]++;
	}
	for(i = 1; i <= N; i++)
		conju.insert(make_pair(deg[i], i));
	while(1)
	{
		bol = 1;
		while(conju.size())
		{
			p = *conju.begin();
			d = p.first;
			u = p.second;
			//printf("%d %d\n", d, u);
			if(d >= A)
				break;
			bol = 0;
			for(i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				if(conju.find(make_pair(deg[v], v)) != conju.end())
				{
					conju.erase(make_pair(deg[v], v));
					deg[v]--;
					if(deg[v])
						conju.insert(make_pair(deg[v], v));
				}
			}
			conju.erase(make_pair(deg[u], u));
		}
		while(conju.size())
		{
			p = *(--conju.end());
			d = p.first;
			u = p.second;
			//printf("%d %d\n", d, u);
			if(d < conju.size()-B)
				break;
			bol = 0;
			for(i = 0; i < G[u].size(); i++)
			{
				v = G[u][i];
				if(conju.find(make_pair(deg[v], v)) != conju.end())
				{
					conju.erase(make_pair(deg[v], v));
					deg[v]--;
					if(deg[v])
						conju.insert(make_pair(deg[v], v));
				}
			}
			conju.erase(make_pair(deg[u], u));
		}
		if(bol)
			break;
	}
	printf("%d\n", conju.size());
	return 0;
}
