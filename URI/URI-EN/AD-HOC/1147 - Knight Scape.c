// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Knight Scape
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1147

#include<stdio.h>

int mat[8][8];

main()
{
	int i,j,k=1,x,y,xc,yc,cont,move[8]={1,1,2,2,-1,-1,-2,-2},movd[8]={-2,2,-1,1,-2,2,-1,1};
	char vet[3];
	while((scanf("%s",vet)==1)&&(vet[0]!='0'))
	{
		cont=0;
		yc=vet[0]-'1';
		xc=vet[1]-'a';
		for(i=0;i<8;i++)
		{
			scanf("%s",vet);
			y=vet[0]-'1';
			x=vet[1]-'a';
			if((y>=1)&&(x>=1))
				mat[y-1][x-1]=1;
			if((y>=1)&&(x<=6))
				mat[y-1][x+1]=1;
		}
		for(i=0;i<8;i++)
		{
			y=yc+move[i];
			x=xc+movd[i];
			if((x>=0)&&(x<=7)&&(y>=0)&&(y<=7))
				cont+=(mat[y][x]==0);
		}
		printf("Caso de Teste #%d: %d movimento(s).\n",k++,cont);
		for(i=0;i<8;i++)
			for(j=0;j<8;j++)
				mat[i][j]=0;
	}
	return 0;
}

