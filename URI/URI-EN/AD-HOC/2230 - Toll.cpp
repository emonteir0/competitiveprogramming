// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Toll
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2230

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int G[101][101];

main()
{
	int N, M, I, D, u, v, t = 1;
	while(scanf("%d %d %d %d", &N, &M, &I, &D) == 4 && N)
	{
		printf("Teste %d\n", t++);
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
				G[i][j] = (i == j) ? 0 : INF;
		while(M--)
		{
			scanf("%d %d", &u, &v);
			G[u][v] = G[v][u] = 1;
		}
		for(int k = 1; k <= N; k++)
			for(int i = 1; i <= N; i++)
				for(int j = 1; j <= N; j++)
					G[i][j] = min(G[i][j], G[i][k]+G[k][j]);
		for(int i = 1; i <= N; i++)
		{
			if(i == I)
				continue;
			if(G[I][i] <= D)
				printf("%d ", i);
		}
		printf("\n\n");
	}
	return 0;
}
