// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Power Crisis
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1031

#include<stdio.h>
 
 
typedef struct a
{
    int ant,dep;
}tipo;
 
int lista[100][2];
int i,n,p,inicio,tam,x,a,b;
char u; 
 
main()
{	  
    while(scanf("%d",&n)==1&&n)
    {
    	u=0;
    	p=1;
    	while(u==0)
    	{
        		for(i=0;i<n;i++)
        		{
            		lista[i][0]=(i+n-1)%n;
            		lista[i][1]=(i+1)%n;
        		}
        		inicio=0;
        		tam=n;
        		while(1)
        		{
                	if(inicio==12)
                		break;
                	a=lista[inicio][0];
                	b=lista[inicio][1];
                	if(a==b)
            		{
            			if(b==12)
            				u=1;
            			break;
            		}
            		lista[a][1]=b;
            		lista[b][0]=a;	
            		tam--;
            		x=p%tam;
            		inicio=lista[inicio][0];
            		for(i=0;i<x;i++)
                		inicio=lista[inicio][1];
            	}
            	p++;
        }
        printf("%d\n",p-1);
    }
    return 0;
}
