// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Passing By The PokeStops
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2219

#include<bits/stdc++.h>

using namespace std;

int vet[100002];

main()
{
	int K, N, M, i, ma;
	scanf("%d", &K);
	while(K--)
	{
		scanf("%d %d", &M, &N);
		vet[0] = M;
		for(i = 1; i <= N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N+1);
		ma = vet[0];
		for(i = 1; i <= N; i++)
		{
			if(vet[i] > M)
				break;
			ma = max(ma, vet[i] - vet[i-1]);
		}
		printf("%d\n", ma);
	}
	return 0;
}

