// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Return of The King
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1808

#include<stdio.h>

main()
{
	char c;
	int cont=0,acum=0;
	while(scanf("%c",&c)==1&&c!='\n')
	{
		if(c!='0')
		{
			cont++;
			acum+=c-'0';
		}
		if(c=='0')
			acum+=9;
	}
	printf("%.2lf\n",((double)acum)/cont);
	return 0;
}
