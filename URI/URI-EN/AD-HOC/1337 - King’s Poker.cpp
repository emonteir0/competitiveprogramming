// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: King’s Poker
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1337

#include<bits/stdc++.h>

using namespace std;

void setvencedor(int x)
{
	if(x == 13)
		printf("*\n");
	else
		printf("%d %d %d\n", x+1, x+1, x+1);
}

void parvencedor(int x, int y)
{
	if(y == 13)
		printf("%d %d %d\n", 1, x+1, x+1);
	else
	{
		if(y+1 > x)
			printf("%d %d %d\n", x, x, y+1);
		else if(y+1 < x)
			printf("%d %d %d\n", y+1, x, x);
		else
		{
			if(x == 13)
				printf("1 1 1\n");
			else
				printf("%d %d %d\n",x, x, x+1);
		}
	}
}

main()
{
	int v[3];
	while(scanf("%d %d %d", &v[0], &v[1], &v[2]) == 3 && v[0])
	{
		sort(v, v+3);
		if(v[0] == v[1] && v[1] == v[2])
			setvencedor(v[0]);
		else if(v[0] == v[1])
			parvencedor(v[0], v[2]);
		else if(v[1] == v[2])
			parvencedor(v[1], v[0]);
		else
			printf("1 1 2\n");
	}
	return 0;
}
