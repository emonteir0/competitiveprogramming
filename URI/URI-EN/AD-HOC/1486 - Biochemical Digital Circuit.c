// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Biochemical Digital Circuit
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1486

#include<stdio.h>


main()
{
	int vet[1000],x,n,m,c,cont,i,j;
	while((scanf("%d %d %d",&n,&m,&c)==3)&&(n||m||c))
	{
		cont=0;
		for(i=0;i<m;i++)
			vet[i]=0;
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
			{
				scanf("%d",&x);
				if(x==0)
				{
					cont+=(vet[j]>=c);
					vet[j]=0;
				}
				else
					vet[j]++;
			}
		}
		for(i=0;i<n;i++)
			cont+=(vet[i]>=c);
		printf("%d\n",cont);
	}
	return 0;
}

