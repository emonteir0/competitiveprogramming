// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Drop Out
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1327

#include<bits/stdc++.h>

using namespace std;

char nomes[20][16];
int carta[52];
int ok[20];

main()
{
	int N, ativos, eliminados, me, p;
	while(scanf("%d", &N) == 1 && N)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%s", nomes[i]);
			ok[i] = 1;
		}
		for(int i = 0; i < 52; i++)
			scanf("%d", &carta[i]);
		ativos = N;
		for(int k = ativos; k <= 52; k += ativos)
		{
			me = 14;
			eliminados = 0;
			for(int i = k-ativos; i < k; i++)
				if(carta[i] < me)
					me = carta[i];
			for(int i = k-ativos; i < k; i++)
				if(carta[i] == me)
					eliminados++;
			if(eliminados == ativos)
				break;
			p = 0;
			for(int i = k-ativos; i < k; i++)
			{
				while(ok[p] != 1)
					p++;
				if(carta[i] == me)
					ok[p] = 0;
				p++;
			}
			ativos -= eliminados;
		}
		for(p = 0; p < N; p++)
			if(ok[p])
				printf("%s ", nomes[p]);
		printf("\n");
	}
	return 0;
}
