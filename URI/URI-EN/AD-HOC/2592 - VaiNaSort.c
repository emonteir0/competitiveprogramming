// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: VaiNaSort
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2592

#include<stdio.h>

main()
{
	int i, N, x, b, cont;
	while(scanf("%d", &N) == 1 && N)
	{
		cont = 1;
		while(1)
		{
			b = 1;
			for(i = 1; i <= N; i++)
			{
				scanf("%d", &x);
				b &= (i == x);
			}
			if(b)
				break;
			cont++;
		}
		printf("%d\n", cont);
	}
	return 0;
}
