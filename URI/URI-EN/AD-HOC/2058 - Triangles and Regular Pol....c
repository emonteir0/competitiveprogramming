// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangles and Regular Pol...
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2058

#include<stdio.h>

main()
{
    int n;
    scanf("%d",&n);
    printf("%d\n",n-2);
    return 0;
}
