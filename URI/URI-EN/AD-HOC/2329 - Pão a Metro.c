// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pão a Metro
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2329

#include<stdio.h>

int vet[10000];

int busca(int N, int K, int ma)
{
	int inf = 1, sup = ma, mid, i, cont, sol;
	while(inf <= sup)
	{
		mid = (inf + sup)/2;
		cont = 0;
		for(i = 0; i < N; i++)
			cont += vet[i]/mid;
		if(cont < K)
			sup = mid-1;
		else
		{
			inf = mid+1;
			sol = mid;
		}
	}
	return sol;
}


main()
{
	int N, K, i, ma = 1;
	scanf("%d %d", &K, &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d", &vet[i]);
		ma = (ma > vet[i])? ma: vet[i];
	}
	printf("%d\n", busca(N, K, ma));
	return 0;
}
