// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Prisoner of Azkaban
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1840

#include<bits/stdc++.h>

using namespace std;

vector<char> vet;
map<char, int> Mapa;

struct tipo
{
	int x, i;
};

char nome[4][11];
int chute[4];
tipo val[4];

bool cmp(tipo a, tipo b)
{
	if(a.x != b.x)
		return a.x > b.x;
	return a.i < b.i;
}

int naipeval(char naipe)
{
	if(naipe == 'D')
		return 0;
	if(naipe == 'S')
		return 1;
	if(naipe == 'H')
		return 2;
	return 3;
}

main()
{
	int i, j, N;
	char car, man;
	char carta, naipe;
	vet.push_back('4');
	vet.push_back('5');
	vet.push_back('6');
	vet.push_back('7');
	vet.push_back('Q');
	vet.push_back('J');
	vet.push_back('K');
	vet.push_back('A');
	vet.push_back('2');
	vet.push_back('3');
	scanf("%d %c%*c%*c", &N, &car);
	for(i = 0; i < vet.size(); i++)
		if(vet[i] == car)
		{
			man = *(vet.begin()+(i+1)%vet.size());
			vet.erase(vet.begin()+(i+1)%vet.size());
			break;
		}
	vet.push_back(man);
	for(i = 0; i < vet.size(); i++)
		Mapa[vet[i]] = i;
	for(i = 0; i < 4; i++)
		scanf("%s %d%*c", nome[i], &chute[i]);
	for(j = 0; j < N; j++)
	{
		for(i = 0; i < 4; i++)
		{
			scanf("%c%c%*c", &carta, &naipe);
			val[i].i = i;
			val[i].x = Mapa[carta] + (carta==man) * naipeval(naipe);
		}
		sort(val, val+4, cmp);
		if(val[0].x != val[1].x)
			chute[val[0].i]--;
	}
	for(i = 0; i < 4; i++)
	{
		val[i].x = N-abs(chute[i]);
		val[i].i = i;
	}
	sort(val, val+4, cmp);
	if(val[0].x != val[1].x)
		printf("%s\n", nome[val[0].i]);
	else
		printf("*\n");
	return 0;
}
