// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Móbile
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2323

#include<bits/stdc++.h>

using namespace std;

vector<int> G[10001];
int mobiles[10001];
int N;

int dfs(int u)
{
	int v, x = 0, result = 1;
	mobiles[u] = 1;
	for(int i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		if(mobiles[v] == 0)
			result &= dfs(v);
		if(x)
		{
			if(x != mobiles[v])
				result = 0;
		}
		else
			x = mobiles[v];
		mobiles[u] += mobiles[v]; 
	}
	return result;
}

main()
{
	int N, M, u, v;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d", &v, &u);
		G[u].push_back(v);
	}
	printf("%s\n", dfs(0) ? "bem" : "mal");
	return 0;
}
