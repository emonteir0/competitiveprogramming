// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Matrix Ladder
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2450

#include<stdio.h>

int mat[500][500];

main()
{
	int i, j, k, N, M, op = 0;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			scanf("%d", &mat[i][j]);
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			if(mat[i][j] != 0)
			{
				if(op)
				{
					printf("N\n");
					return 0;
				}
				for(k = i+1; k < N; k++)
					if(mat[k][j] != 0)
					{
						printf("N\n");
						return 0;
					}
				break;
			}
		}
		if(j == M)
			op = 1;
	}
	printf("S\n");
	return 0;
}

