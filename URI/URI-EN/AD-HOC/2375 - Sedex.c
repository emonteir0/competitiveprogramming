// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sedex
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2375

#include<stdio.h>

int min(int a, int b)
{
	return (a < b) ? a : b;
}


main()
{
	int x, a, b, c;
	scanf("%d %d %d %d", &x, &a, &b, &c);
	a = min(a, b);
	a = min(a, c);
	printf("%s\n", (x <= a) ? "S" : "N");
	return 0;
}
