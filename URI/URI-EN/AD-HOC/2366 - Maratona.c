// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Maratona
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2366

#include<stdio.h>

main()
{
	int N, M, x, xant, b = 1;
	scanf("%d %d %d", &N, &M, &xant);
	N--;
	while(N--)
	{
		scanf("%d", &x);
		b &= (x - xant <= M);
		xant = x;
	}
	b &= (42195 <= M + xant);
	printf("%s\n", b ? "S": "N");
	return 0;
}

