// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bakugan
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1419

#include<stdio.h>
#include<string.h>

int vet[1001];

int main()
{
	int n,b,d,c1,c2,i,i1,i2,xant,x;
	while((scanf("%d",&n)==1)&&(n>0))
	{
		i1=9999;
		i2=9999;
		b=1;
		d=0;
		scanf("%d",&xant);
		c1=xant;
		for(i=1;i<n;i++)
		{
			scanf("%d",&x);
			c1+=x;
			if(d==0)
			{
				if(x==xant)
					b++;
				else
					b=1;
				if(b==3)
				{
					i1=i;
					d=1;
				}
			}
			xant=x;
		}
		b=1;
		d=0;
		scanf("%d",&xant);
		c2=xant;
		for(i=1;i<n;i++)
		{
			scanf("%d",&x);
			c2+=x;
			if(d==0)
			{
				if(x==xant)
					b++;
				else
					b=1;
				if(b==3)
				{
					i2=i;
					d=1;
				}
			}
			xant=x;
		}
		if(i1<i2)
			c1+=30;
		if(i2<i1)
			c2+=30;
		if(c1>c2)
			printf("M\n");
		else if(c1==c2)
			printf("T\n");
		else
			printf("L\n");
	}
	return 0;
}

