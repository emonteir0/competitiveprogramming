// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fusões
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2380

#include <stdio.h>
#define maxn 200002

int N, M, p[maxn], l[maxn];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
	int aux;
    if (l[u] < l[v])
	{
		aux = u;
		u = v;
		v = aux;
	}
    p[v] = u;
	l[u] += l[v];
}
 
int main() 
{
	int i, u, v;
	char op;
    scanf("%d %d%*c", &N, &M);
    for (i = 1; i <= N; i++)
	{
		p[i] = i;
		l[i] = 1;
	}
    for (i = 0; i < M; i++)
	{ 
        scanf("%c %d %d%*c", &op, &u, &v);
        u = id(u);
        v = id(v);
        if(op == 'F' && u != v)
        	join(u, v);
        else if(op == 'C')
        	printf("%s\n", (u == v) ? "S" : "N");
    }
}
