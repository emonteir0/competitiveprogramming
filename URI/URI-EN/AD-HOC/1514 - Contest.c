// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Contest
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1514

#include<stdio.h>

int vetc[100],vetp[100],contl;

main()
{
	int a,b,c,d,i,j,N,M,x;
	while((scanf("%d %d",&N,&M)==2) && (N||M))
	{
		a=1;
		b=1;
		c=1;
		d=1;
		for(i=0;i<M;i++)
		{
			vetp[i]=0;
			vetc[i]=0;
		}
		for(i=0;i<N;i++)
		{
			contl=0;
			for(j=0;j<M;j++)
			{
				scanf("%d",&x);
				if(x>0)
				{
					vetc[j]++;
					vetp[j]=1;
					contl++;
				}
			}
			if(contl==M)
				a=0;
			if(contl==0)
				d=0;
		}
		for(i=0;i<M;i++)
		{
			if(vetp[i]==0)
				b=0;
			if(vetc[i]==N)
				c=0;
		}
		printf("%d\n",a+b+c+d);
	}
	return 0;
}
