// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Loud Music
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2814

#include<bits/stdc++.h>

using namespace std;

char mat[31][31];

main()
{
	int T, dB, N, M, xf, yf, xj, yj, dist;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &dB, &N);
		for(int i = 0; i < N; i++)
			scanf("%s", mat[i]);
		M = strlen(mat[0]);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
			{
				if(mat[i][j] == 'F')
					xf = i, yf = j;
				else if(mat[i][j] == 'J')
					xj = i, yj = j;
			}
		dist =  floor(10*sqrt((xf-xj)*(xf-xj)+(yf-yj)*(yf-yj)));
		//printf("%d %d %d %d %d\n",xf, yf, xj, yj, dist);
		printf("%.0lf dBs\n", floor(dB/pow(0.99, dist)));
	}
	return 0;
}
