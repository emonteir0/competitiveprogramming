// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Perfect Choir
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1225

#include<stdio.h>

int vet[10000];

main()
{
	int N,acum,i,x,ma,me,acum2;
	while(scanf("%d",&N)==1)
	{
		acum=0;
		acum2=0;
		ma=-100001;
		me=100001;
		for(i=0;i<N;i++)
		{
			scanf("%d",&x);
			acum+=x;
			if(x>ma)
				ma=x;
			if(x<me)
				me=x;
			vet[i]=x;
		}
		if(acum%N!=0)
			printf("-1\n");
		else
		{
			acum/=N;
				for(i=0;i<N;i++)
					if(vet[i]>acum)
						acum2+=vet[i]-acum;
			printf("%d\n",acum2+1);
		}
	}
	return 0;
}


