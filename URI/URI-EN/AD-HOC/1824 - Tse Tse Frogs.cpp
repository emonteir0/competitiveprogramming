// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tse Tse Frogs
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1824

#include<bits/stdc++.h>

using namespace std;

int t;
int vis[1001][1001];
int mat[1001][1001];
int dx[] = {-1, -1, 0, 1, 1, 1, 0, -1};
int dy[] = {0, 1, 1, 1, 0, -1, -1, -1};

int main()
{
	int N, M, x, y, z, cnt = 0;
	while(scanf("%d %d", &N, &M) == 2 && N)
	{
		printf("Instancia %d\n", ++t);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				scanf("%d", &mat[i][j]);
		x = 0, y = 0, cnt = 0;
		while(1)
		{
			if(x < 0 || x >= N || y < 0 || y >= M || vis[x][y] == t)
				break;
			vis[x][y] = t;
			cnt++;
			z = mat[x][y]-1;
			x += dx[z];
			y += dy[z];
		}
		printf("%s\n\n", cnt == N*M ? "sim" : "nao");
	}
	printf("Instancia %d\nsim\n\n", ++t);
	return 0;
}
