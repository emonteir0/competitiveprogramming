// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Interruptores
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2884

#include<bits/stdc++.h>

using namespace std;

bitset<1000> inicial, final, internos[1000];

main()
{
	int N, M, x, y, cnt = 0;
	scanf("%d %d", &N, &M);
	scanf("%d", &x);
	while(x--)
	{
		scanf("%d", &y);
		inicial.set(y-1);
	}
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &x);
		while(x--)
		{
			scanf("%d", &y);
			internos[i].set(y-1);
		}
	}
	for(int j = 0; j < 2; j++)
	{
		for(int i = 0; i < N; i++)
		{
			cnt++;
			inicial ^= internos[i];
			if(inicial == final)
				return !printf("%d\n", cnt);
		}
	}
	printf("-1\n");
	return 0;
}
