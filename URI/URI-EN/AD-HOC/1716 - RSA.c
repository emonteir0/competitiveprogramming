// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: RSA
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1716

#include<stdio.h>
 
long long int fastexp(long long int a,long long int b,long long int P){
    long long int x;
    if(b==0) return 1;
    if(b==1) return a;
    if(b%2==0)
    {
        x = fastexp(a,b/2,P)%P;
        return (x*x)%P;
    }
    else
        return (a*fastexp(a,b-1,P))%P;
}
 
long long int mdc(long long int  a, long long int b, long long int *x, long long int *y) {
  long long int xx, yy, d;
  if(b==0) {
    *x=1; *y=0;
    return a;
  }
 
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}
 
 
long long int inv5(long long int a,long long P){
  long long int x,y,d;
  d = mdc(a,P,&x,&y);
  if(x<0)
    x = x+P;
  return x;
}
 
 
main()
{
    long long int i,N,C,E,D,tot,M;
    scanf("%lld %lld %lld",&N,&E,&C);
    for(i=3;i*i<=N;i++)
        if(N%i==0)
            tot=(N/i-1)*(i-1);
    D=inv5(E,tot);
    M=fastexp(C,D,N);
    printf("%d\n",M);
    return 0;
}
