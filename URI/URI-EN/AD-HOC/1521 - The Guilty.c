// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Guilty
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1521

#include<stdio.h>

int vet[51];

main()
{
	int n,i;
	while((scanf("%d",&n)==1)&&(n>0))
	{
		for(i=1;i<=n;i++)
			scanf("%d",&vet[i]);
		scanf("%d",&i);
		while(i!=vet[i])
			i=vet[i];
		printf("%d\n",i);
	}
	return 0;
}

