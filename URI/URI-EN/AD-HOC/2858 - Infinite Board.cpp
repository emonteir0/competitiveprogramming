// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Infinite Board
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2858

#include<bits/stdc++.h>

using namespace std;

int mdc(int a, int b, int *x, int *y)
{
	int xx, yy, d;
	if(b==0) 
	{
		*x=1; *y=0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx - a/b*yy;
	return d;
}


int solve(int a, int b)
{
	int x, y, d;
	d = mdc(a, b, &x, &y);
	return abs(x-y);
}

main()
{
	int A, B;
	scanf("%d %d", &A, &B);
	if(__gcd(A, B) != 1)
		printf("IMPOSSIVEL\n");
	else
		printf("%d\n", solve(A, B));
	return 0;
}
