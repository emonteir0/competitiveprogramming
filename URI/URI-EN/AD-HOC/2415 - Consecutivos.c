// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Consecutivos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2415

#include<stdio.h>

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int N, x, xant, maximo = 1, atual = 1;	
	scanf("%d %d", &N, &xant);
	N--;
	while(N--)
	{
		scanf("%d", &x);
		if(x == xant)
			atual++;
		else
		{
			maximo = max(maximo, atual);
			atual = 1;
		}
		xant = x;
	}
	maximo = max(maximo, atual);
	printf("%d\n", maximo);
	return 0;
}

