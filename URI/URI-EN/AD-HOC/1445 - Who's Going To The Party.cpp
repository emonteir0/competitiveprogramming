// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Who's Going To The Party?
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1445

#include<cstdio>
#include<cstring>
#include<queue>
#include<map>
#include<set>

using namespace std;

queue<int> fila,fila2;
map< int, set<int> > Mapa, Mapa2;
set<int> conjunto;
set<int>::iterator it;
bool vet[1001];

main()
{
	int n,x,y,cont;
	while((scanf("%d%*c",&n)==1)&&(n!=0))
	{
		while(n--)
		{
			scanf("(%d,%d)%*c",&x,&y);
			Mapa[x].insert(y);
			Mapa[y].insert(x);
		}
		cont=1;
		vet[1]=true;
		fila.push(1);
		while(!fila.empty())
		{
			conjunto=Mapa[fila.front()];
			fila.pop();
			for(it=conjunto.begin();it!=conjunto.end();++it)
			{
				x=*it;
				if(vet[x]==false)
				{
					vet[x]=true;
					cont++;
					fila.push(x);
				}
			}
		}
		printf("%d\n",cont);
		memset(vet,false,1001);
		fila=fila2;
		Mapa=Mapa2;
	}
	return 0;
}

