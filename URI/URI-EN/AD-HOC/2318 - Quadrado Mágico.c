// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadrado Mágico
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2318

#include<stdio.h>

int x[3], y[3], k, mat[3][3];
int ds, dp, col[3], lin[3];

int max(int a, int b)
{
	return (a>b) ? a : b;
}

main()
{
	int i, j, l, ma = 0, C = 3;
	for(i = 0; i < 3; i++)
		for(j = 0; j < 3; j++)
		{
			scanf("%d", &mat[i][j]);
			lin[i] += mat[i][j];
			col[j] += mat[i][j];
			if(i == j)
				dp += mat[i][j];
			if((i + j) == 2)
				ds += mat[i][j];
			if(mat[i][j] == 0)
			{
				x[k] = i;
				y[k++] = j;
			}
		}
	ma = max(ds, dp);
	for(i = 0; i < 3; i++)
	{
		ma = max(ma, col[i]);
		ma = max(ma, lin[i]);
	}
	if((ds == 0) || (dp == 0))
		ma = max(ma, (col[0]+col[1]+col[2])/2);
	while(C--)
	{
		for(l = 0; l < k; l++)
		{
			if((mat[0][y[l]] == 0) + (mat[1][y[l]] == 0) + (mat[2][y[l]] == 0) == 1)
			{
				mat[x[l]][y[l]] = ma - col[y[l]];
				col[y[l]] += mat[x[l]][y[l]];
				lin[x[l]] += mat[x[l]][y[l]];
				continue;
			}
			if((mat[x[l]][0] == 0) + (mat[x[l]][1] == 0) + (mat[x[l]][2] == 0) == 1)
			{
				mat[x[l]][y[l]] = ma - lin[x[l]];
				col[y[l]] += mat[x[l]][y[l]];
				lin[x[l]] += mat[x[l]][y[l]];
				continue;
			}	
		}
	}
	for(i = 0; i < 3; i++)
		printf("%d %d %d\n", mat[i][0], mat[i][1], mat[i][2]);
	return 0;
}
