// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Treasure Hunt
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2228

#include<bits/stdc++.h>

using namespace std;

int val[100];
int subset[10001];

main()
{
	int t = 0, X, Y, N, sum, cond;
	subset[0] = 1;
	while(scanf("%d %d %d", &X, &Y, &N) == 3 && X)
	{
		sum = X+Y;
		cond = 1;
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &val[i]);
			sum += val[i];
		}
		if(sum&1)
			cond = 0;
		for(int i = 0; i < N; i++)
			for(int j = sum; j >= val[i]; j--)
				subset[j] |= subset[j-val[i]];
		cond &= subset[sum/2-X];
		for(int i = 1; i <= sum; i++)
			subset[i] = 0;
		printf("Teste %d\n%s\n\n", ++t, cond ? "S" : "N");
	}
	return 0;
}
