// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dating On-Line
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2348

#include<bits/stdc++.h>
#define ll long long

int vet[100000], vet2[100000];

using namespace std;

main()
{
	int i, j, k, N, acum;
	while(scanf("%d", &N)==1)
	{
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N, greater<int>());
		vet2[0] = vet[0];
		for(k = 1; k <= N/2; k++)
		{
			if(2*k <= N)
				vet2[N-k] = vet[2*k-1];
			if(2*k <= N-1)
				vet2[k] = vet[2*k];
		}
		j = N-1;
		acum = 0;
		for(i = 0; i < N; i++)
		{
			acum += vet2[j]*vet2[i];
			j = i;
		}
		printf("%.3lf\n", (sin(2*M_PI/N)*acum)/2);
	}	
	return 0;
}

