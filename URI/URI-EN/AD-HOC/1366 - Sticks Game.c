// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sticks Game
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1366

#include<stdio.h>

main()
{
	int N,x,cont;
	while(scanf("%d%*c",&N)==1&&N!=0)
	{
		cont=0;
		while(N--)
		{
			scanf("%*d %d",&x);
			cont+=(x/2);
		}
		printf("%d\n",cont/2);
	}
	return 0;
}
