// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Blocks
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1331

#include<bits/stdc++.h>

using namespace std;

int t, N;
int dp[231][231][231];
int vis[231][231][231];

int vet[231];
vector<int> G[231];
vector<int>::iterator it;
vector< pair<int, int> > V;


void comprime()
{
	int a = vet[0], b = 1;
	V.clear();
	for(int i = 1; i < N; i++)
	{
		if(a == vet[i])
			b++;
		else
		{
			G[a].push_back(V.size());
			V.push_back(make_pair(a, b));
			a = vet[i];
			b = 1;
		}
	}
	G[a].push_back(V.size());
	V.push_back(make_pair(a, b));
	V.push_back(make_pair(0, 0));
}

int solve(int start, int end, int sz)
{
	int cor, z;
	if(start == end)
		return sz*sz;
	if(start > end)
		return 0;
	if(vis[start][end][sz] == t)
		return dp[start][end][sz];
	vis[start][end][sz] = t;
	cor = V[start].first;
	dp[start][end][sz] = (sz+V[start].second)*(sz+V[start].second) + solve(start+1, end, 0);
	it = lower_bound(G[cor].begin(), G[cor].end(), start+2);
	if(it != G[cor].end() && *it <= end)
	{
		z = it-G[cor].begin();
		for(int i = z; i < G[cor].size(); i++)
		{
			z = G[cor][i];
			if(z > end)
				break;
			dp[start][end][sz] = max(dp[start][end][sz], solve(z, end, sz+V[start].second)+solve(start+1, z, 0));
		}
	}
	return dp[start][end][sz];
}

main()
{
	int T;
	scanf("%d", &T);
	for(t = 1; t <= T; t++)
	{
		scanf("%d", &N);
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		comprime();
		printf("Case %d: %d\n", t, solve(0, V.size()-1, 0));
		V.clear();
		for(int i = 0; i <= N; i++)
			G[i].clear();
	}
	return 0;
}
