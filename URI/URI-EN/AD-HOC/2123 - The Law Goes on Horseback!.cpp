// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Law Goes on Horseback!
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2123

#include <bits/stdc++.h>

using namespace std;

struct edge { int u, v, rev, cap, f; };

const int MAXN = 402, INF = 0x3f3f3f3f;
vector<edge> adj[MAXN];

void add_edge(int u, int v, int cap) 
{
	adj[u].push_back((edge){u, v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){v, u, (int)adj[u].size() - 1, 0, 0});
}

int edmonds_karp(int nodes, int source, int sink) 
{
	int max_flow = 0;
	for (;;)
	{
		vector<edge*> pred(nodes, (edge*)0);
		queue<int> q;
		q.push(source);
		while (!q.empty() && !pred[sink]) 
		{
			int u = q.front();
			q.pop();
			for (int j = 0; j < (int)adj[u].size(); j++) 
			{
				edge &e = adj[u][j];
				if (!pred[e.v] && e.cap > e.f)
				{
					pred[e.v] = &e;
					q.push(e.v);
				}
			}
		}
		if (!pred[sink]) 
		{
			break;
		}
		int flow = INF;
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			flow = min(flow, pred[u]->cap - pred[u]->f);
		}
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			pred[u]->f += flow;
			adj[pred[u]->v][pred[u]->rev].f -= flow;
		}
		max_flow += flow;
	}
	return max_flow;
}


int main() 
{
	int t = 1, N, M, K, x, y;
	while(scanf("%d %d %d", &N, &M, &K) == 3)
	{
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &x);
			add_edge(i+2, 1, x);
		}
		for(int i = 0; i < M; i++)
			add_edge(0, N+2+i, 1);
		while(K--)
		{
			scanf("%d %d", &x, &y);
			add_edge(y+N+1, x+1, 1);
		}
		printf("Instancia %d\n%d\n\n", t++, edmonds_karp(N+M+2, 0, 1));
		for(int i = 0; i < N+M+2; i++)
			adj[i].clear();
	}
	return 0;
}
