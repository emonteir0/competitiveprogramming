// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hours and Minutes
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1300

#include <stdio.h>
 
int main() {
    int n;
    while(scanf("%d",&n)==1)    printf("%s\n",((n%6)==0)?"Y":"N");
    return 0;
}
