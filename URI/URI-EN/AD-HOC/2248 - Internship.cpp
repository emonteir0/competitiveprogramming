// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Internship
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2248

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int a, b, i;
} tipo;

tipo vet[100001];

bool cmp(tipo x, tipo y)
{
	if(x.b != y.b)
		return x.b > y.b;
	return x.i < y.i;
}

main()
{
	int n, i, k = 1;
	while(scanf("%d", &n) == 1 && n)
	{
		for(i = 0; i < n; i++)
		{
			scanf("%d %d", &vet[i].a, &vet[i].b);
			vet[i].i = i;
		}
		sort(vet, vet+n, cmp);
		printf("Turma %d\n%d ", k++, vet[0].a);
		for(i = 1; i < n; i++)
			if(vet[i].b == vet[0].b)
				printf("%d ", vet[i].a);
			else
				break;
		printf("\n\n");
	}
	return 0;
}
