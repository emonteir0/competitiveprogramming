// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Puzzle
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1934

#include<bits/stdc++.h>
#define ll long long

using namespace std;

map<string, int> indices;
map<string, int>::iterator it;
string var[100][100];
ll A[200][200], B[200], ans[200];

int main()
{
	int i, j, k, N, M, tam, ind, sz;
	char msg[3];
	string str;
	ll f1, f2;
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			scanf("%s%*c", msg);	
			str = msg;
			var[i][j] = str;
			indices[str] = 1;
		}
		scanf("%lld%*c", &B[i]);
	}
	for(i = 0; i < M; i++)
		scanf("%lld", &B[N+i]);
	for(it = indices.begin(), i = 0; it != indices.end(); it++, i++)
		it->second = i;
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			A[i][indices[var[i][j]]]++;
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			A[N+j][indices[var[i][j]]]++;
	tam = N+M;
	sz = indices.size();
	for(k = 0; k < sz; k++)
	{
		for(i = 0; i < tam; i++)
		{
			ind = -1;
			for(j = 0; j < sz; j++)
			{
				if((ind == -1) && (A[i][j] != 0))
					ind = j;
				else if(A[i][j] != 0)
					break;
			}
			if((j == sz) && (ind != -1))
			{
				ans[ind] = B[i]/A[i][ind];
				for(j = 0; j < tam; j++)
					if(A[j][ind] != 0)
					{
						B[j] -= A[j][ind] * ans[ind];
						A[j][ind] = 0;
					}
				break;
			}
		}
	}
	for(it = indices.begin(), i = 0; it != indices.end(); it++, i++)
		printf("%s %lld\n", it->first.c_str(), ans[i]);
	return 0;
}
