// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Progressões Aritméticas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2391

#include<stdio.h>

main()
{
	int resp = 1, xant, r, x, N;
	char b = 0;
	scanf("%d %d", &N, &xant);
	N--;
	while(N--)
	{
		scanf("%d", &x);
		if(b)
		{
			if(r != x-xant)
			{
				r = x-xant;
				resp++;
				b = 0;
			}
		}
		else
		{
			r = x-xant;
			b = 1;
		}
		xant = x;
	}
	printf("%d\n", resp);
	return 0;
}

