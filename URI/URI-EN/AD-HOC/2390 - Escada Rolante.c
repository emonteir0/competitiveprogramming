// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escada Rolante
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2390

#include<stdio.h>

int min(int a, int b)
{
	return (a < b) ? a : b;
}

main()
{
	int N, x, xant, acum = 0;
	scanf("%d %d", &N, &xant);
	N--;
	while(N--)
	{
		scanf("%d", &x);
		acum += min(10, x-xant);
		xant = x;
	}
	printf("%d\n", 10+acum);
	return 0;
}

