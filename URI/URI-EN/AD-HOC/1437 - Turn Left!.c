// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Turn Left!
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1437

#include<stdio.h>

int x(char *v)
{
	int n=0;
	char *s;
	s=v;
	while(*s!=0)
	{
		n+=(*s=='D')-(*s=='E');
		s++;
	}
	return n;
}

main()
{
	int n,num;
	char v[1001],r[4]={'N','L','S','O'};
	while(scanf("%d%*c",&n)==1&&n!=0)
	{
		gets(v);
		num=(x(v)%4+4)%4;
		printf("%c\n",r[num]);
	}
	return 0;
}
