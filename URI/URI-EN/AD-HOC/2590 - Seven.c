// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Seven
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2590

#include<stdio.h>

int v[4] = {1,7,9,3};

main()
{
	int x;
	scanf("%*d");
	while(scanf("%d", &x) != EOF)
		printf("%d\n", v[x&3]);
	return 0;
}
