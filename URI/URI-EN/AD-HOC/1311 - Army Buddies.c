// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Army Buddies
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1311

#include<stdio.h>

typedef struct
{
	int ant, dep;
}tipo;

tipo vet[100002];

main()
{
	int i, N, K, a, b;
	while(scanf("%d %d",&N,&K)==2 && (N||K))
	{
		if(N>1)
		{
			vet[0].ant = 0;
			vet[0].dep = 0;
			vet[1].ant = 0;
			vet[1].dep = 2;
			vet[N].ant = N-2;
			vet[N].dep = 0;
			vet[N+1].ant = 0;
			vet[N+1].dep = 0;
		}
		else
		{
			vet[0].ant = 0;
			vet[0].dep = 0;
			vet[1].ant = 0;
			vet[1].dep = 0;
			vet[2].ant = 0;
			vet[2].dep = 0;
		}
		for(i=2;i<=N-1;i++)
		{
			vet[i].ant = i-1;
			vet[i].dep= i+1;
		}
		while(K--)
		{
			scanf("%d %d",&a,&b);
			vet[vet[b].dep].ant = vet[a].ant;
			vet[vet[a].ant].dep = vet[b].dep;
			if(vet[a].ant == 0)
				printf("*");
			else
				printf("%d",vet[a].ant);
			if(vet[b].dep == 0)
				printf(" *\n");
			else
				printf(" %d\n",vet[b].dep);
		}
		printf("-\n");
	}
	return 0;
}

