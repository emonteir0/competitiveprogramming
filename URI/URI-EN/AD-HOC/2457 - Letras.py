# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Letras
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2457

c = raw_input()
palavras = raw_input().split(' ')
n = len(palavras)
cont = 0
for palavra in palavras:
	cont += (c in palavra)
print '%.1f' % ((100.0*cont)/n)

