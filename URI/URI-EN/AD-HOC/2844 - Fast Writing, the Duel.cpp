// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fast Writing, the Duel
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2844

#include<bits/stdc++.h>

using namespace std;

char str[100001];

main()
{
	int a, b, c, d, e, f, N, x, y;
	scanf("%d %d %d%*c", &a, &b, &c);
	scanf("%d %d %d%*c", &d, &e, &f);
	gets(str);
	N = strlen(str);
	x = 2*a+b+N*c;
	y = 2*d+e+N*f;
	if(x == y)
		printf("Empate\n");
	if(x < y)
		printf("Matheus\n");
	if(x > y)
		printf("Vinicius\n");
	return 0;
}
