// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: I Went to Market And Boug...
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2090

#include<bits/stdc++.h>
#define ll unsigned long long int

using namespace std;

char palavras[100000][21];

vector<ll> pa;

ll val(int x)
{
	return (((ll)x)*(x+1)) >> 1; 
}

main()
{
	int i, n, k, x;
	for(i = 0; i <= 100000; i++)
		pa.push_back(val(i));
	while(scanf("%d %d%*c",&n,&k)==2 && (n||k))
	{
		for(i = 0; i < n; i++)
			scanf("%s%*c", palavras[i]);
		x = lower_bound(pa.begin(), pa.end(), k) - pa.begin() - 1;
		
		printf("%s\n", palavras[k-pa[x]-1]);
	}
	return 0;
}

