// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Van
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2693

#include<bits/stdc++.h>

using namespace std;

struct st
{
	string str;
	char regiao;
	int dist;
};

map<char, int> Valor;

st vet[100001];

bool cmp(st a, st b)
{
	if(a.dist != b.dist)
		return a.dist < b.dist;
	if(a.regiao != b.regiao)
		return a.regiao < b.regiao;
	return a.str < b.str;
}

main()
{
	int N;
	while(cin >> N)
	{
		for(int i = 0; i < N; i++)
			cin >> vet[i].str >> vet[i].regiao >> vet[i].dist;
		sort(vet, vet+N, cmp);
		for(int i = 0; i < N; i++)
			cout << vet[i].str << endl;	
	}
	return 0;
}
