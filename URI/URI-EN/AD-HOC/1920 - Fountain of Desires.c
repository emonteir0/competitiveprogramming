// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fountain of Desires
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1920

#include<stdio.h>

int sqr(int x)
{
	return x*x;
}

main()
{
	int n, xc, yc, r1, r2, dist1, dist2;
	int a, b, c, d;
	int c1a, c1b, c2a, c2b;
	while(scanf("%d", &n) == 1 && n)
	{
		scanf("%d %d %d %d", &xc, &yc, &r1, &r2);
		c1a = c1b = c2a = c2b = 0;
		while(n--)
		{
			scanf("%d %d %d %d", &a, &b, &c, &d);
			dist1 = sqr(a-xc)+sqr(b-yc);
			dist2 = sqr(c-xc)+sqr(d-yc);
			c1a += dist1 < r1*r1;
			c2a += dist2 < r1*r1;
			c1b += (dist1 >= r1*r1) && (dist1 <= r2*r2);
			c2b += (dist2 >= r1*r1) && (dist2 <= r2*r2);
		}
		if(c1a != c2a)
			printf("%s\n", (c1a > c2a)?"C > P" : "P > C");
		else if(c1b != c2b)
			printf("%s\n", (c1b > c2b)?"C > P" : "P > C");
		else
			printf("C = P\n");
	}
	return 0;
}

