// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How Many Trips Will Noel ...
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2719

#include<bits/stdc++.h>

using namespace std;


main()
{
	int i, T, N, M, x, cont, atual = 0;
	scanf("%d", &T);
	while(T--)
	{
		cont = 1;
		atual = 0;
		scanf("%d %d", &N, &M);
		for(i = 0; i < N; i++)
		{
			scanf("%d", &x);
			if(atual + x > M)
			{
				cont++;
				atual = x;
			}
			else
				atual += x;
		}
		printf("%d\n", cont);
	}
	return 0;
}
