// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Football
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1495

#include<cstdio>
#include<algorithm>

using namespace std;

int vet[100000];

main()
{
	int N,G,S,R,i,k,pontos;
	while(scanf("%d %d",&N,&G)==2)
	{
		pontos=0;
		k=0;
		for(i=0;i<N;i++)
		{
			scanf("%d %d",&S,&R);
			if(S>R)
				pontos+=3;
			else
				vet[k++]=S-R;
		}
		sort(vet,vet+k);
		for(i=k-1;i>=0;i--)
		{
			if(G+vet[i]>0)
			{
				G+=vet[i]-1;
				pontos+=3;
			}
			else if(G+vet[i]==0)
			{
				pontos+=1;
				G=0;
			}
			else
				break;
		}
		printf("%d\n",pontos);
	}	
	return 0;
}

