// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Palindrome
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2865

#include<bits/stdc++.h>
#define MAXN 5000000

using namespace std;

int pal[MAXN+1];

void simula(int x)
{
	int y = x, z = 0;
	while(y)
	{
		z = (z << 1) ^ (y&1);
		y >>= 1;
	}
	pal[x] = x == z;
}

int main()
{
	int N, x;
	for(int i = 0; i <= MAXN; i++)
		simula(i);
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		if(pal[x])
		{
			printf("*\n");
			continue;
		}
		for(int i = 1; ; i++)
		{
			if(pal[x+i])
			{
				printf("+ %d\n", i);
				break;
			}
			if(x-i > 1 && pal[x-i])
			{
				printf("- %d\n", i);
				break;
			}
			if(x*i <= MAXN && pal[x*i])
			{
				printf("x %d\n", i);
				break;
			}
			if(x/i > 0 && pal[x/i])
			{
				printf("/ %d\n", i);
				break;
			}
		}
	}
	return 0;
}
