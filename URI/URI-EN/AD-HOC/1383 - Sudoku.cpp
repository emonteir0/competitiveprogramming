// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sudoku
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1383

#include<cstdio>
#include<set>
#include<vector>

using namespace std;

vector< set<int> > vm,vc,vl;
set<int> vazio;

main()
{
	int n,x,i,j,k,l;
	bool result;
	scanf("%d",&n);
	for(k=1;k<=n;k++)
	{
		result=true;
		vm.clear();
		vc.clear();
		vl.clear();
		for(l=0;l<9;l++)
		{
			vm.push_back(vazio);
			vc.push_back(vazio);
			vl.push_back(vazio);
		}
		for(i=0;i<9;i++)
		{
			for(j=0;j<9;j++)
			{
				scanf("%d",&x);
				vl[i].insert(x);
				vc[j].insert(x);
				vm[3*(i/3)+(j/3)].insert(x);
			}
		}
		for(i=0;i<9;i++)
		{
			if((vm[i].size()<9)||(vc[i].size()<9)||(vl[i].size()<9))
			{
				result=false;
				break;
			}
		}
		printf("Instancia %d\n%s\n\n",k,result?"SIM":"NAO");
		
	}
	return 0;
}

