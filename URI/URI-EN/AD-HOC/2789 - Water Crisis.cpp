// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Water Crisis
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2789

#include<bits/stdc++.h>
#define ll long long
using namespace std;

template<class T>
class heavy_light {
  // Set this to true to store values on edges, false to store values on nodes.
  static const bool VALUES_ON_EDGES = false;

  static T join_values(const T &a, const T &b) {
    return a+b;
  }

  static T join_value_with_delta(const T &v, const T &d, int len) {
    return v+d*len;
  }

  static T join_deltas(const T &d1, const T &d2) {
    return d1+d2;  // For "set" updates, the more recent delta prevails.
  }

  int counter, paths;
  std::vector<std::vector<T> > value, delta;
  std::vector<std::vector<bool> > pending;
  std::vector<std::vector<int> > len;
  std::vector<int> size, parent, tin, tout, path, pathlen, pathpos, pathroot;
  std::vector<int> *adj;

  void dfs(int u, int p) {
    tin[u] = counter++;
    parent[u] = p;
    size[u] = 1;
    for (int j = 0; j < (int)adj[u].size(); j++) {
      int v = adj[u][j];
      if (v != p) {
        dfs(v, u);
        size[u] += size[v];
      }
    }
    tout[u] = counter++;
  }

  int new_path(int u) {
    pathroot[paths] = u;
    return paths++;
  }

  void build_paths(int u, int path) {
    this->path[u] = path;
    pathpos[u] = pathlen[path]++;
    for (int j = 0; j < (int)adj[u].size(); j++) {
      int v = adj[u][j];
      if (v != parent[u]) {
        build_paths(v, (2*size[v] >= size[u]) ? path : new_path(v));
      }
    }
  }

  inline T join_value_with_delta(int path, int i) {
    return pending[path][i]
        ? join_value_with_delta(value[path][i], delta[path][i], len[path][i])
        : value[path][i];
  }

  void push_delta(int path, int i) {
    int d = 0;
    while ((i >> d) > 0) {
      d++;
    }
    for (d -= 2; d >= 0; d--) {
      int l = (i >> d), r = (l ^ 1), n = l/2;
      if (pending[path][n]) {
        value[path][n] = join_value_with_delta(path, n);
        delta[path][l] =
            pending[path][l] ? join_deltas(delta[path][l], delta[path][n])
                             : delta[path][n];
        delta[path][r] =
            pending[path][r] ? join_deltas(delta[path][r], delta[path][n])
                             : delta[path][n];
        pending[path][l] = pending[path][r] = true;
        pending[path][n] = false;
      }
    }
  }

  bool query(int path, int u, int v, T *res) {
    push_delta(path, u += value[path].size()/2);
    push_delta(path, v += value[path].size()/2);
    bool found = false;
    for (; u <= v; u = (u + 1)/2, v = (v - 1)/2) {
      if ((u & 1) != 0) {
        T value = join_value_with_delta(path, u);
        *res = found ? join_values(*res, value) : value;
        found = true;
      }
      if ((v & 1) == 0) {
        T value = join_value_with_delta(path, v);
        *res = found ? join_values(*res, value) : value;
        found = true;
      }
    }
    return found;
  }

  void update(int path, int u, int v, const T &d) {
    push_delta(path, u += value[path].size()/2);
    push_delta(path, v += value[path].size()/2);
    int tu = -1, tv = -1;
    for (; u <= v; u = (u + 1)/2, v = (v - 1)/2) {
      if ((u & 1) != 0) {
        delta[path][u] = pending[path][u] ? join_deltas(delta[path][u], d) : d;
        pending[path][u] = true;
        if (tu == -1) {
          tu = u;
        }
      }
      if ((v & 1) == 0) {
        delta[path][v] = pending[path][v] ? join_deltas(delta[path][v], d) : d;
        pending[path][v] = true;
        if (tv == -1) {
          tv = v;
        }
      }
    }
    for (int i = tu; i > 1; i /= 2) {
      value[path][i/2] = join_values(join_value_with_delta(path, i),
                                     join_value_with_delta(path, i ^ 1));
    }
    for (int i = tv; i > 1; i /= 2) {
      value[path][i/2] = join_values(join_value_with_delta(path, i),
                                     join_value_with_delta(path, i ^ 1));
    }
  }

  inline bool is_ancestor(int parent, int child) {
    return (tin[parent] <= tin[child]) && (tout[child] <= tout[parent]);
  }

 public:
  heavy_light(int n, std::vector<int> adj[], const T &v = T())
      : counter(0), paths(0), size(n), parent(n), tin(n), tout(n), path(n),
        pathlen(n), pathpos(n), pathroot(n), adj(adj) {
    dfs(0, -1);
    build_paths(0, new_path(0));
    value.resize(paths);
    delta.resize(paths);
    pending.resize(paths);
    len.resize(paths);
    for (int i = 0; i < paths; i++) {
      int m = pathlen[i];
      value[i].assign(2*m, v);
      delta[i].resize(2*m);
      pending[i].assign(2*m, false);
      len[i].assign(2*m, 1);
      for (int j = 2*m - 1; j > 1; j -= 2) {
        value[i][j/2] = join_values(value[i][j], value[i][j ^ 1]);
        len[i][j/2] = len[i][j] + len[i][j ^ 1];
      }
    }
  }

  T query(int u, int v) {
    if (VALUES_ON_EDGES && u == v) {
      throw std::runtime_error("No edge between u and v to be queried.");
    }
    bool found = false;
    T res = T(), value;
    int root;
    while (!is_ancestor(root = pathroot[path[u]], v)) {
      if (query(path[u], 0, pathpos[u], &value)) {
        res = found ? join_values(res, value) : value;
        found = true;
      }
      u = parent[root];
    }
    while (!is_ancestor(root = pathroot[path[v]], u)) {
      if (query(path[v], 0, pathpos[v], &value)) {
        res = found ? join_values(res, value) : value;
        found = true;
      }
      v = parent[root];
    }
    if (query(path[u], std::min(pathpos[u], pathpos[v]) + (int)VALUES_ON_EDGES,
              std::max(pathpos[u], pathpos[v]), &value)) {
      res = found ? join_values(res, value) : value;
      found = true;
    }
    if (!found) {
      throw std::runtime_error("Unexpected error: No values found.");
    }
    return res;
  }

  void update(int u, int v, const T &d) {
    if (VALUES_ON_EDGES && u == v) {
      return;
    }
    int root;
    while (!is_ancestor(root = pathroot[path[u]], v)) {
      update(path[u], 0, pathpos[u], d);
      u = parent[root];
    }
    while (!is_ancestor(root = pathroot[path[v]], u)) {
      update(path[v], 0, pathpos[v], d);
      v = parent[root];
    }
    update(path[u], std::min(pathpos[u], pathpos[v]) + (int)VALUES_ON_EDGES,
           std::max(pathpos[u], pathpos[v]), d);
  }
};

int casas[5001];
int cost[5001];
int weight[5001];	
ll pd[5001][1001];	

int main()
{
	int N, M, Q, D, u, v, w;
	scanf("%d %d", &N, &D);
	vector<int> adj[N];
	for(int i = 1; i < N; i++)
	{
		scanf("%d %d", &u, &v);
		u--, v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	//printf("OK\n");
	scanf("%d", &M);
	for(int i = 1; i <= M; i++)
	{
		scanf("%d %d", &casas[i], &cost[i]);
		casas[i]--;
	}
	scanf("%d", &Q);
	heavy_light<ll> hld(N, adj, 0);
	for(int i = 0; i < Q; i++)
	{
		scanf("%d %d %d", &u, &v, &w);
		u--, v--;
		hld.update(min(u, v), max(u, v), w);
	}
	for(int i = 1; i <= M; i++)
	{
		weight[i] = hld.query(casas[i], casas[i]);
		//printf("%d\n", weight[i]);
	}
	for(int i = 1; i <= M; i++)
		for(int j = 1; j <= D; j++)
		{
			pd[i][j] = pd[i-1][j];
			if(j >= cost[i]) 
				pd[i][j] = max(pd[i][j], pd[i-1][j-cost[i]] + weight[i]);
		}
	printf("%lld\n", pd[M][D]);
	return 0;
}

