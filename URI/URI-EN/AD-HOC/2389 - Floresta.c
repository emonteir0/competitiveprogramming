// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Floresta
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2389

#include<stdio.h>
#include<math.h>
#define ll long long

ll func(int x, int y)
{
	return ((ll)x)*y + ((ll)(x-1))*(y-1);
}

int busca(int X, int N)
{
	int inf = 2, sup = N, mid;
	ll val;
	while(inf <= sup)
	{
		mid = (inf+sup)/2;
		val = func(X, mid);
		if(val > N)
			sup = mid-1;
		else if(val < N)
			inf = mid+1;
		else
			return X <= mid;
	}
	return 0;
}

main()
{
	int i, N, cont = 0;
	scanf("%d", &N);
	for(i = 2; i <= sqrt(N); i++)
		cont += busca(i, N);
	printf("%d\n", cont);
	return 0;
}

