// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Formula 1
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1125

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int x, i;
} pos;

pos vet[101];

int mapas[101][101];

bool cmp(pos a, pos b)
{
	if(a.x!=b.x)
		return a.x>b.x;
	return a.i<b.i;
}

main()
{
	int G,P,S;
	int i,j,k;
	int n,x;
	while(scanf("%d %d",&G,&P)==2 && (G||P))
	{
		for(j=1;j<=100;j++)
			for(i=1;i<=P;i++)
				mapas[i][j] = 0;
			
		for(i=1;i<=G;i++)
			for(j=1;j<=P;j++)
			{
				scanf("%d",&x);
				mapas[j][x]++;
			}
			
		scanf("%d",&S);
		
		for(i=1;i<=S;i++)
		{
			scanf("%d",&n);
			for(j=1;j<=P;j++)
			{
				vet[j].x=0;
				vet[j].i=j;
			}	
			for(j=1;j<=n;j++)
			{
				scanf("%d",&x);
				for(k=1;k<=P;k++)
					vet[k].x += mapas[k][j]*x;
			}
			sort(vet+1,vet+P+1,cmp);
			printf("%d",vet[1].i);
			k = 1;
			while(++k <= P)
			{
				if(vet[k].x != vet[1].x)
					break;
				printf(" %d",vet[k].i);
			}
			printf("\n");
		}
	}
	return 0;
}

