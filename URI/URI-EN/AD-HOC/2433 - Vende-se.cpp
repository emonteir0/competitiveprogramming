// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vende-se
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2433

#include<bits/stdc++.h>

using namespace std;

int vet[100000];

main()
{
	int i, N, K, minimo = 999999999;
	scanf("%d %d", &N, &K);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	sort(vet, vet+N);
	for(i = 0; i <= K; i++)
		minimo = min(minimo, vet[i+N-K-1]-vet[i]);
	printf("%d\n", minimo);
	return 0;
}

