// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequência
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2837

#include<bits/stdc++.h>
#define INFN -1001001001

using namespace std;

int N, L, H;
int vet[100001], flag[100001], acum[100001], ida[100001], volta[100001];
int pd[100001][2], vis[100001][2];

int kadaneback(int x)
{
        int ma = 0;
        for(int i = x-1; i > 0 && !flag[i]; i--)
                ma = max(ma, acum[x-1]-acum[i-1]);
        return ma;
}

int kadanefront(int x)
{
        int ma = 0;
        for(int i = x+1; i <= N && !flag[i]; i++)
                ma = max(ma, acum[i]-acum[x]);
        return ma;
}

int tree[400004];

void build(int node, int start, int end)
{
        if(start == end)
        {
                tree[node] = acum[start];
                //printf("%d %d\n", node, tree[node]);
        }
        else
        {
                int mid = (start+end)>>1;
                build(2*node, start, mid);
                build(2*node+1, mid+1, end);
                tree[node] = max(tree[2*node], tree[2*node+1]);
        }
}

int query(int node, int start, int end, int l, int r)
{
        if(start > r || end < l)
                return INFN;
        if(start >= l && end <= r)
                return tree[node];
        int mid = (start+end)>>1;
        return max(query(2*node, start, mid, l, r),
                           query(2*node+1, mid+1, end, l, r));
}

main()
{
        int ma = INFN, f = 0, fi, fj;
        vector<int> flags;
        scanf("%d %d %d", &N, &L, &H);
        for(int i = 1; i <= N; i++)
        {
                scanf("%d", &vet[i]);
                acum[i] = acum[i-1] + vet[i];
        }
        build(1, 1, N);
        for(int i = 1; i <= N; i++)
        {
                scanf("%d", &flag[i]);
                if(flag[i])
                        flags.push_back(i);
        }
        for(int i = 0; i < flags.size(); i++)
        {
                fi = flags[i];
                volta[fi] = kadaneback(fi);
                ida[fi] = kadanefront(fi);
        }
        if(!L)
        {
                for(int i = 1; i <= N; i++)
                {
                        if(!flag[i])
                        {
                                int j;
                                for(j = i+1; j <= N; j++)
                                {
                                        if(flag[j])
                                                break;
                                }
                                //printf("%d %d %d\n", i, j-1, query(1, 1, N, i, j-1));
								for(int k = i; k <= j; k++)
									ma = max(ma, query(1, 1, N, k, j-1)-acum[k-1]);
                                i = j;
                        }
                }
        }
        if(H)
        {
                //printf(" Q\n" );
                for(int i = 0; i < flags.size(); i++)
                        for(int j = i+L-1; j < i+H && j < flags.size(); j++)
                        {
                                fi = flags[i];
                                fj = flags[j];
                                ma = max(ma, volta[fi]+ida[fj]+acum[fj]-acum[fi-1]);
                        }
        }
        printf("%d\n", ma);
        return 0;
}
