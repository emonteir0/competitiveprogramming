// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lap
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1708

#include<stdio.h>

main()
{
	int a, b;
	scanf("%d %d",&a,&b);
	printf("%d\n",b/(b-a)+(b%(b-a)!=0));
	return 0;
}
