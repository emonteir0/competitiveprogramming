// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Virus
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2567

#include<bits/stdc++.h>

using namespace std;

int vet[1000];

main()
{
	int i, N, val;
	while(scanf("%d", &N) == 1)
	{
		val = 0;
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		for(i = 0; i < N/2; i++)
			val += vet[N-1-i] - vet[i];
		printf("%d\n", val);
	}
	return 0;
}
