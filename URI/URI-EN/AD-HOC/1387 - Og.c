// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Og
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1387

#include<stdio.h>


main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2&&(a||b))
	{
		printf("%d\n",a+b);
	}
	return 0;
}
