// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Musical Plagiarism
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1127

#include<bits/stdc++.h>
#define MAXL 100001
#define ll long long

using namespace std;

string P, T;
int b[MAXL], m;

void kmpPre()
{
	int i = 0, j = -1;
	b[0] = -1;
	m = P.length();
	while( i < m)
	{
		while(j >= 0 && P[i] != P[j]) j = b[j];
		i++, j++;
		b[i] = j;
	}
}

bool kmpSearch()
{
	int i = 0, j = 0, n = T.size();
	int cont = 0;
	while(i < n)
	{
		while(j >= 0 && T[i] != P[j]) j = b[j];
		i++, j++;
		if(j == m)
		{
			return true;
			j = b[j];
		}
	}
	return false;
}

string str;
map<string, char> Mapa;

main()
{
	int N, M;
	bool b;
	Mapa["C"] = Mapa["B#"] = '0';
	Mapa["Db"] = Mapa["C#"] = '1';
	Mapa["D"] = '2';
	Mapa["Eb"] = Mapa["D#"] = '3';
	Mapa["E"] = Mapa["Fb"] = '4';
	Mapa["F"] = Mapa["E#"] = '5';
	Mapa["Gb"] = Mapa["F#"] = '6';
	Mapa["G"] = '7';
	Mapa["Ab"] = Mapa["G#"] = '8';
	Mapa["A"] = '9';
	Mapa["Bb"] = Mapa["A#"] = '9'+1;
	Mapa["B"] = Mapa["Cb"] = '9'+2;
	ios_base::sync_with_stdio(NULL);
	cin.tie(NULL);
	while(cin >> N >> M && N)
	{
		P = "";
		T = "";
		for(int i = 0; i < N; i++)
		{
			cin >> str;
			T += Mapa[str];
		}
		for(int i = 0; i < M; i++)
		{
			cin >> str;
			P += Mapa[str];
		}
		//cout << T << endl << P << endl;
		b = false;
		for(int i = 0; i < 12; i++)
		{
			//cout << P << endl;
			kmpPre();
			b |= kmpSearch();
			for(int j = 0; j < P.length(); j++)
				P[j] = (P[j]+1)%12 + '0';
		}
		cout << (b ? "S" : "N") << endl;
	}
	return 0;
}
