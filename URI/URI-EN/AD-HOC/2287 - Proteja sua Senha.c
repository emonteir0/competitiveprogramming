// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Proteja sua Senha
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2287

#include<stdio.h>
#include<math.h>

main()
{
	int i, n, k = 1;
	int x[5];
	int val[6];
	int vet[10];
	char op[6];
	while(scanf("%d%*c", &n) == 1 && n)
	{
		scanf("%d %d %d %d %d %d %d %d %d %d %c %c %c %c %c %c%*c", &vet[0], &vet[1], &vet[2], &vet[3], &vet[4], &vet[5], &vet[6], &vet[7], &vet[8], &vet[9], &op[0], &op[1], &op[2], &op[3], &op[4], &op[5]);
		x[0] = (1 << vet[0]) | (1 << vet[1]);
		x[1] = (1 << vet[2]) | (1 << vet[3]);
		x[2] = (1 << vet[4]) | (1 << vet[5]);
		x[3] = (1 << vet[6]) | (1 << vet[7]);
		x[4] = (1 << vet[8]) | (1 << vet[9]);
		for(i = 0; i < 6; i++)
			val[i] = x[op[i]-'A'];
		n--;
		while(n--)
		{
			scanf("%d %d %d %d %d %d %d %d %d %d %c %c %c %c %c %c%*c", &vet[0], &vet[1], &vet[2], &vet[3], &vet[4], &vet[5], &vet[6], &vet[7], &vet[8], &vet[9], &op[0], &op[1], &op[2], &op[3], &op[4], &op[5]);
			x[0] = (1 << vet[0]) | (1 << vet[1]);
			x[1] = (1 << vet[2]) | (1 << vet[3]);
			x[2] = (1 << vet[4]) | (1 << vet[5]);
			x[3] = (1 << vet[6]) | (1 << vet[7]);
			x[4] = (1 << vet[8]) | (1 << vet[9]);
			for(i = 0; i < 6; i++)
				val[i] &= x[op[i]-'A'];
		}
		printf("Teste %d\n%d %d %d %d %d %d \n\n", k++, (int)round(log2(val[0])), (int)round(log2(val[1])), (int)round(log2(val[2])), (int)round(log2(val[3])), (int)round(log2(val[4])), (int)round(log2(val[5])));
	}
	return 0;
}

