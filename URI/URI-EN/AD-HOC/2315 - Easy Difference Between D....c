// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Easy Difference Between D...
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2315

#include<stdio.h>

int vet[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}, vet2[12];


main()
{
	int i, a, b, c, d;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	for(i = 1; i < 12; i++)
		vet2[i] = vet2[i-1] + vet[i-1];
	printf("%d\n", ((c+vet2[d-1]-a-vet2[b-1])%365+365)%365);
	return 0;
}

