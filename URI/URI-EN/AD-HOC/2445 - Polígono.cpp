// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Polígono
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2445

#include<bits/stdc++.h>

using namespace std;

int vet[100000];

main()
{
	int i, n, acum = 0;
	scanf("%d", &n);
	for(i = 0; i < n; i++)
	{
		scanf("%d", &vet[i]);
		acum += vet[i];
	}
	sort(vet, vet+n);
	for(i = n-1; i >= 2; i--)
	{
		acum -= vet[i];
		if(vet[i] < acum)
			break;
	}
	printf("%d\n", (i > 1) ? i+1 : 0); 
	return 0;
}

