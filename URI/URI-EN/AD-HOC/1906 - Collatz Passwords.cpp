// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Collatz Passwords
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1906

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int nivel, last;
	st()
	{
		nivel = -1;
		last = 0;
	}
	st(int nivel, int last)
	{
		this->nivel = nivel;
		this->last = last;
	}
	st operator+(st st2)
	{
		st st3;
		st3.nivel = this->nivel+st2.nivel;
		st3.last = st2.last;
		return st3;
	}
	bool operator !=(st st2)
	{
		return nivel != st2.nivel || last != st2.last;
	}
	bool operator ==(st st2)
	{
		return nivel == st2.nivel && last == st2.last;
	}
};

int N, K;
st vazio;
st vet[10000001];

st solve(int x, int pai)
{
	if(x > N)
		return st(0, pai);
	if(vet[x] != vazio)
		return vet[x];
	if(x&1)
		return vet[x] = st(1, 0) + solve(3*x+1, x);
	return vet[x] = st(1, 0) + solve(x/2, x);
}

main()
{
	int cont;
	scanf("%d %d", &N, &K);
	if(N < 4)
	{
		if(K == 1)
			printf("%d\n", N);
		else if(K == 2)
			printf("%d\n", N > 1);
		else
			printf("0\n");
	}
	else
	{
		cont = 0;
		vet[1] = st(1, 1);
		for(int i = 2; i <= N; i++)
			if(vet[i] == vazio)
				solve(i, 0);
		for(int i = 1; i <= N; i++)
		{
			if(vet[i].last == 1)
				cont++;
			else if(vet[i].nivel >= K)
				cont++;
		}
		printf("%d\n", cont);
	}
	return 0;
}
