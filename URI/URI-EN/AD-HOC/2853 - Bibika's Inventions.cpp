// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bibika's Inventions
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2853

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

char mat[2001][2001];
int dist[2001][2001][4];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};
int N, M;

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < M;
}

main()
{
	int a, a2, b, b2, c, ans = 0, me;
	queue< pair< pair<int, int>, int> > fila;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			for(int k = 0; k < 4; k++)
				dist[i][j][k] = INF;
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
			if(mat[i][j] == '@')
			{
				for(int k = 0; k < 4; k++)
				{
					fila.push({{i, j}, k});
					dist[i][j][k] = 0;
				}
			}
	}
	while(!fila.empty())
	{
		a = fila.front().first.first;
		b = fila.front().first.second;
		c = fila.front().second;
		fila.pop();
		a2 = a+dx[c];
		b2 = b+dy[c];
		if(inRange(a2, b2) && dist[a2][b2][c] > dist[a][b][c] + 1)
		{
			dist[a2][b2][c] = dist[a][b][c]+1;
			fila.push({{a2, b2}, c});
		}
	}
	/*for(int i = 0;i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			printf("(");
			for(int k = 0; k < 4; k++)
			{
				if(dist[i][j][k] != INF)
					printf("%2d,", dist[i][j][k]);
				else
					printf("-1,");
			}
			printf(") ");
		}
		printf("\n");
	}*/
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
		{
			me = INF;
			for(int k = 0; k < 4; k++)
				me = min(me, dist[i][j][k]);
			ans = max(ans, me);
		}
	if(ans == INF)
		printf("-1\n");
	else
		printf("%d\n", ans);
	return 0;
}
