// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Houseplant
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2796

#include<bits/stdc++.h>

using namespace std;

int pode[2001][2001];
int mat[2001][2001];
char G[2001][2001];

void buildAreas(int l, int r, int h)
{
	stack<int> pilha;
    int tp, i = l, x;
    while (i < r)
    {
        if (pilha.empty() || mat[h][pilha.top()] <= mat[h][i])
            pilha.push(i++);
        else
        {
            tp = pilha.top();
            pilha.pop();
            x = pilha.empty() ? i-l : i - pilha.top() - 1;
            pode[mat[h][tp]][x] = 1;
            pode[x][mat[h][tp]] = 1;
        }
    }
    while (!pilha.empty())
    {
        tp = pilha.top();
        pilha.pop();
        x = pilha.empty() ? i-l : i - pilha.top() - 1;
        pode[mat[h][tp]][x] = 1;
        pode[x][mat[h][tp]] = 1;
    }
}

void build2(int x, int y)
{
	if(x < 0 || y < 0)
		return;
	pode[x][y] = 1;
	if(pode[x-1][y] == 0)
		build2(x-1, y);
	if(pode[x][y-1] == 0)
		build2(x, y-1);
}

main()
{
	int N, M, dim, l, ma = 0, Q, x, y, xm, ym;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", G[i]);
	for(int j = 0; j < M; j++)
		mat[0][j] = G[0][j] == '.';
	for(int i = 1; i < N; i++)
		for(int j = 0; j < M; j++)
			mat[i][j] = G[i][j] == '.' ? mat[i-1][j]+1 : 0;
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			if(mat[i][j] != 0)
			{
				l = j;
				for(; j < M; j++)
					if(mat[i][j] == 0)
						break;
				buildAreas(l, j, i);
			}
		}
	}
	dim = max(N, M);
	for(int i = dim; i > 0; i--)
		for(int j = dim; j > 0; j--)
			if(pode[i][j])
				build2(i, j);
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d %d", &x, &y);
		if(pode[x][y])
		{
			if(x*y > ma)
			{
				ma = x*y;
				xm = x;
				ym = y;
			}
			if(x*y == ma)
			{
				if(y > ym)
				{
					xm = x;
					ym = y;
				}
			}
		}
	}
	printf("%d %d\n", xm, ym);
	return 0;
}
