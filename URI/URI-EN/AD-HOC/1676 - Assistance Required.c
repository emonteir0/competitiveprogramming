// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Assistance Required
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1676

#include<stdio.h>
  
int lista[33809],vet[3000];
  
main()
{   
    int i,j,n,k,cont;
    for(i=0;i<33810;i++)
    		lista[i]=1;
	for(i=0;i<3000;i++)
	{
		j=0;
		while(lista[j]!=1)
			j++;
		lista[j]=2;
		vet[i]=j+2;
		for(k=j+1;k<33810;)
		{
			cont=0;
			while(k<33810)
			{
				if(lista[k]==1)
				{
					cont++;
					if(cont==(j+2))
					{
						lista[k]=0;
						break;
					}
				}
				k++;
			}
		}
	}
    while(scanf("%d",&n)==1&&n)
	{
    	printf("%d\n",vet[n-1]);
    }    
    return 0;
}
