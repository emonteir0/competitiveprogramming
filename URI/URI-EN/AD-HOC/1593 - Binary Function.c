// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Function
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1593

#include<stdio.h>
#include<string.h>

main()
{
	int i, n, tam, tot;
	char vet[1001], vet2[1001];
	scanf("%d%*c",&n);
	while(n--)
	{
		tot = 0;
		gets(vet);
		tam = strlen(vet);
		for(i=0;i<tam;i++)
			vet2[tam-1-i] = vet[i] - '0';
		while(tam > 0)
		{
			tot += vet2[0] & 1;
			vet2[0] /= 2;
			for(i = 1; i < tam; i++)
			{
				if(vet2[i] & 1)
					vet2[i-1] += 5;
				vet2[i] /= 2;
			}
			if(vet2[tam-1] == 0)
				tam--;
		}
		printf("%d\n",tot);
	}
	return 0;
}
