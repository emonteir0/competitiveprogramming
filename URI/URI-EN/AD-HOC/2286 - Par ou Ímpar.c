// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Par ou Ímpar
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2286

#include<stdio.h>

main()
{
	char nome1[11], nome2[11];
	int k = 1, n, a, b;
	while(scanf("%d%*c", &n) == 1 && n)
	{
		printf("Teste %d\n", k++);
		scanf("%s%*c%s%*c", nome1, nome2);
		while(n--)
		{
			scanf("%d %d%*c", &a, &b);
			printf("%s\n", (a^b)&1? nome2 : nome1);
		}
		printf("\n");
	}
	return 0;
}

