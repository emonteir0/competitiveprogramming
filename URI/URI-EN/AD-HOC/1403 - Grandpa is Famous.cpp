// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grandpa is Famous
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1403

#include<cstdio>
#include<algorithm>

typedef struct
{
	int val,i;
}tipo;

using namespace std;

tipo vet[10002];

bool cmp(tipo a, tipo b)
{
	if(a.val!=b.val)
		return a.val<b.val;
	return a.i>b.i;
}

int main()
{
	int M,N,tam,i,x,max;
	for(i=0;i<=10000;i++)
		vet[i].i=i;
	while((scanf("%d %d%*c",&N,&M)==2)&&(N||M))
	{
		tam=N*M;
		max=0;
		for(i=0;i<tam;i++)
		{
			scanf("%d",&x);
			vet[x].val++;
			if(x>max)
				max=x;
		}
		sort(vet,vet+max+1,cmp);
		x=vet[max-1].val;
		printf("%d ",vet[max-1].i);
		for(i=max-2;i>=1;i--)
		{
			if(vet[i].val==x)
				printf("%d ",vet[i].i);
			else
				break;
		}
		printf("\n");
		for(i=0;i<=max+1;i++)
		{
			vet[i].i=i;
			vet[i].val=0;
		}
	}
	return 0;
}

