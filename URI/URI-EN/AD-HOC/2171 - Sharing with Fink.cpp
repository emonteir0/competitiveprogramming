// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sharing with Fink
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2171

#include<bits/stdc++.h>

using namespace std;

int vet[1415];

main()
{
	int i, x, y;
	for(i = 1; i <= 1414; i++)
		vet[i] = (i*(i+1))/2;
	while(scanf("%d", &x) == 1 && x)
	{
		y = *(upper_bound(vet, vet+1415, x)-1);
		printf("%d %d\n", y, x-y);
	}
	return 0;
}

