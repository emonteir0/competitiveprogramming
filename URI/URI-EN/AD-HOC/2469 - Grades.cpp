// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grades
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2469

#include<bits/stdc++.h>

using namespace std;

struct nota
{
	int x, cont;
};

map<int, int> Mapa;
map<int, int>::iterator it;


nota vet[100000];

bool cmp(nota a, nota b)
{
	if(a.cont != b.cont)
		return a.cont > b.cont;
	return a.x > b.x;
}

main()
{
	int N, k = 0, x;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		Mapa[x]++;
	}
	for(it = Mapa.begin(); it != Mapa.end(); ++it)
	{
		vet[k].x = it->first;
		vet[k++].cont = it->second;
	}
	sort(vet, vet+k, cmp);
	printf("%d\n", vet[0].x);
	return 0;
}

