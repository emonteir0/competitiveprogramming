// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trash Removal
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2202

#include<stdio.h>
#include<math.h>

typedef struct
{
	int x, y;
} ponto;

ponto vet[101];

double min(double a, double b)
{
	return (a < b) ? a : b;
}

double max(double a, double b)
{
	return (a > b) ? a : b;
}

main()
{
	int i, j, k, N, T = 1, h, a, b, c, alturan, alturap;
	double minimo, mod;
	while(scanf("%d", &N) == 1 && N)
	{
		minimo = 9000000000;
		for(i = 0; i < N; i++)
			scanf("%d %d", &vet[i].x, &vet[i].y);
		for(i = 0; i < N; i++)
		{
			for(j = i+1; j < N; j++)
			{
				a = vet[i].y-vet[j].y;
				b = vet[j].x - vet[i].x;
				c = -(a*vet[i].x + b*vet[i].y);
				mod = sqrt(a*a + b*b);
				alturap = 0;
				alturan = 0;
				for(k = 0; k < N; k++)
				{
					if(k != j && k != i)
					{
						h = (a*vet[k].x + b*vet[k].y + c);
						if (h > 0)
							alturap = max(alturap, h);
						else
							alturan = max(alturan, -h);
					}
				}
				minimo = min(minimo, (alturap + alturan)/mod);
			}
		}
		printf("Case %d: %.2lf\n", T++, minimo + 5e-3);			
	}
	return 0;
}

