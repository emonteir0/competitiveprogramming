// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Frequencia na Aula
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2410

#include<bits/stdc++.h>

using namespace std;

set<int> conjunto;

main()
{
	int N, x;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		conjunto.insert(x);
	}
	printf("%d\n", conjunto.size());
	return 0;
}

