// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Animal Game
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1483

#include<stdio.h>


main()
{
	double x;
	int a, b;
	while(scanf("%lf %d %d", &x, &a, &b) == 3 && (x || a || b))
	{
		if(a%10000 == b % 10000)
			printf("%.2lf\n", 3000*x);
		else if(a%1000 == b%1000)
			printf("%.2lf\n", 500*x);
		else if(a%100 == b%100)
			printf("%.2lf\n", 50*x);
		else
		{
			a = a%100;
			b = b%100;
			if(a == 0)
				a = 100;
			if(b == 0)
				b = 100;
			if((a+3)/4 == (b+3)/4)
				printf("%.2lf\n", 16*x);
			else
				printf("0.00\n");
		}
	}
	return 0;
}
