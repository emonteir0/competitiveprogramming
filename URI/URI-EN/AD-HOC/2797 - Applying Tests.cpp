// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Applying Tests
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2797

#include<bits/stdc++.h>

using namespace std;

int mat[1000][1000];
vector<int> um[1000], dois[1000];
int tem[1000];

int main()
{
	int N, M, K, b = 1;
	scanf("%d %d %d", &N, &M, &K);
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			scanf("%d", &mat[i][j]);
			if(mat[i][j] == 1)
				um[j].push_back(i);
			else if(mat[i][j] == 2)
				dois[j].push_back(i);
			tem[j] |= mat[i][j] != 0;
		}
	}
	for(int i = 1; i < N; i++)
	{
		if(tem[i] == 1 && tem[i-1] == 1)
			b = 0;
	}
	if(!b)
		printf("N\n");
	else
	{
		for(int j = 0; j < M; j++)
		{
			/*printf("DOIS %d:\n", j);
			for(int i = 0; i < dois[j].size(); i++)
				printf("%d\n", dois[j][i]);
			printf("UM %d:\n", j);
			for(int i = 0; i < um[j].size(); i++)
				printf("%d\n", um[j][i]);*/
			for(int i = 1; i < (int)dois[j].size(); i++)
				b &= (dois[j][i] - dois[j][i-1] >= K+1);
			for(int i = 1; i < (int)um[j].size(); i++)
				b &= (um[j][i] - um[j][i-1] >= K+1);
		}
		printf("%s\n", b ? "S" : "N");
	}
	return 0;
}

