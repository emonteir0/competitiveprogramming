// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multiplicação de Matrizes
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2385

#include<stdio.h>
#define ll long long

main()
{
	int N, P, Q, R, S, X, Y, I, J, k;
	ll acum = 0;
	scanf("%d", &N);
	scanf("%d %d %d %d %d %d", &P, &Q, &R, &S, &X, &Y);
	scanf("%d %d", &I, &J);
	P *= I;
	S *= J;
	for(k = 1; k <= N; k++)
		acum += ((ll)((P + Q*k)%X)) * ((R*k + S)%Y);
	printf("%lld\n", acum);
	return 0;
}

