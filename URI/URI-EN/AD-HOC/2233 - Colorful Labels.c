// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colorful Labels
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2233

#include<stdio.h>

main()
{
    long long A, B, C;
    scanf("%llx %llx %llx", &A, &B, &C);
    printf("%llx\n",1+(A/B)*(A/B)*(1+(B/C)*(B/C)));
    return 0;
}
