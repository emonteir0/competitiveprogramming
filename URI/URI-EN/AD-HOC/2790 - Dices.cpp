// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dices
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2790

#include<bits/stdc++.h>

using namespace std;

int change[7][7];
int vet[100001];

main()
{
	int N, s, me = 1001001001;
	for(int i = 1; i <= 6; i++)
		for(int j = 1; j <= 6; j++)
			change[i][j] = (i == j) ? 0 : (i+j == 7) ? 2 : 1;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(int j = 1; j <= 6; j++)
	{
		s = 0;
		for(int i = 0; i < N; i++)
			s += change[vet[i]][j];
		me = min(s, me);
	}
	printf("%d\n", me);
	return 0;
}

