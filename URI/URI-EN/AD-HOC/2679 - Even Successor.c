// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even Successor
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2679

#include<stdio.h>

main()
{
    int x;
    scanf("%d", &x);
    if(x&1)
        printf("%d\n", x+1);
    else
        printf("%d\n", x+2);
    return 0;
}
