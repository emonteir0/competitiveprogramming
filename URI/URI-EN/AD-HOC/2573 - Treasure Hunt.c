// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Treasure Hunt
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2573

#include<stdio.h>

double sqr(double x)
{
	return x*x;
}

int sig(double x)
{
	return x >= 0;
}

double max(double a, double b)
{
	return (a > b) ? a : b;
}

double min(double a, double b)
{
	return (a < b) ? a : b;
}

main()
{
	int Xa, Xb, Ya, Yb, L, A, i;
	double dl, l, y, yant, a, b, A1, A2, Aaux;
	scanf("%d %d %d %d %d %d", &L, &A, &Xa, &Ya, &Xb, &Yb);
	if(Ya == Yb)
	{
		A1 = A*(Xa+Xb)/2.0;
		A2 = A*L - A1;
		if(Xa > Xb)
		{
			Aaux = A1;
			A1 = A2;
			A2 = Aaux;
		}
	}
	else
	{
		a = -((double)(Xb-Xa))/(Yb-Ya);
		b = ((double)(Yb*Yb-Ya*Ya+Xb*Xb-Xa*Xa))/(2*(Yb-Ya));
		dl = L/1000000.0;
		yant = b;
		if(yant < 0)
			yant = 0;
		if(yant > A)
			yant = A;
		l = A1 = A2 = 0;
		for(i = 1; i <= 1000000; i++)
		{
			l += dl;
			y = a*l+b;
			if(y < 0)
				y = 0;
			if(y > A)
				y = A;
			A1 += dl/2*(A-y + (A-yant));
			A2 += dl/2*(y + yant);
			yant = y;
		}
		if(sig(Ya-(a*Xa+b)) == sig(A/2-(a*(L/2)+b)))
		{
			Aaux = min(A1, A2);
			A1 = max(A1, A2);
			A2 = Aaux;
		}
		else
		{
			Aaux = min(A1, A2);
			A2 = max(A1, A2);
			A1 = Aaux;
		}
	}	
	printf("%.6lf %.6lf\n", A1/(L*A), A2/(L*A));
	return 0;
}
