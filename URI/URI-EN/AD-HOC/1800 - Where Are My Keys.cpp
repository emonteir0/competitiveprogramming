// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Where Are My Keys
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1800

#include<stdio.h>
#include<algorithm>
  
int vet[2000],E,Q; 
  
int PB(int chave)
{
     int inf = 0;
     int sup = Q-1;
     int meio;
     while (inf <= sup) 
     {
          meio = (inf + sup)/2;
          if (chave == vet[meio])
               return 0;
          else if (chave < vet[meio])
               sup = meio-1;
          else
               inf = meio+1;
     }
     return 1;
} 
  
  
main()
{
    int x,i,a;
    scanf("%d %d",&E,&Q);
    for(i=0;i<Q;i++)
        scanf("%d",&vet[i]);
    std::sort(vet,vet+Q);
    for(i=0;i<E;i++)
    {
        scanf("%d",&x);
        a=PB(x);
        if(a==1)
        {
            vet[Q++]=x;
        }
        std::sort(vet,vet+Q);
        printf("%d\n",a);
    }
    return 0;
}
