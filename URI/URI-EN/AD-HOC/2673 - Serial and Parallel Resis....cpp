// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Serial and Parallel Resis...
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2673

#include<bits/stdc++.h>
#define SERIE    10000000
#define PARALELO 20000000
#define PARABER  30000000
#define PARFECH  40000000
using namespace std;

map<int, int> prec;
stack<int> opStack;
vector<int> postfixlist;
string str;
int ind;
int sz;
int topToken;

int operand(int c)
{
	return c < SERIE;
}

float solve()
{
	int num = postfixlist[ind];
	float X, Y;
	ind--;
	if(!operand(num))
	{
		X = solve();
		Y = solve();
		if(num == SERIE)
			return X+Y;
		else
			return X*Y/(X+Y);	
	}
	return num;
}

main()
{
	int x;
	prec[SERIE] = 2;
	prec[PARALELO] = 2;
	prec[PARABER] = 1;
	while(cin >> str)
	{
		while(str.find("-") != -1)
			str.replace(str.find("-"), 1, " 10000000 ");
		while(str.find("|") != -1)
			str.replace(str.find("|"), 1, " 20000000 ");
		while(str.find("(") != -1)
			str.replace(str.find("("), 1, " 30000000 ");
		while(str.find(")") != -1)
			str.replace(str.find(")"), 1, " 40000000 ");
		istringstream ss(str);
		postfixlist.clear();
		while(ss >> x)
		{
			if(operand(x))
				postfixlist.push_back(x);
			else if(x == PARABER)
				opStack.push(PARABER);
			else if(x == PARFECH)
			{
				topToken = opStack.top();
				opStack.pop();
				while(topToken != PARABER)
				{
					postfixlist.push_back(topToken);
					topToken = opStack.top();
					opStack.pop();
				}
			}
			else
			{
				while(!opStack.empty() && prec[opStack.top()] >= prec[x])
				{
					postfixlist.push_back(opStack.top());
					opStack.pop();
				}
				opStack.push(x);
			}
		}
		while(!opStack.empty())
		{
			postfixlist.push_back(opStack.top());
			opStack.pop();
		}
		ind = postfixlist.size()-1;
		printf("%.3f\n", solve());
	}
	return 0;
}
