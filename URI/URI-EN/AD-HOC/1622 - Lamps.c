// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lamps
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1622

#include<stdio.h>
#include<string.h>

int mdc(int a, int b)
{
	int aux;
	while( b != 0)
	{
		aux = a%b;
		a = b;
		b = aux;
	}
	return a;
}

main()
{
	int N, C, K, a, b, i, x;
	char vet[101], vet2[101];
	while(scanf("%d %d%*c",&N,&K) == 2 && (N||K))
	{
		scanf("%s",vet);
		while(K--)
		{
			scanf("%d %d%*c",&a,&b);
			strcpy(vet2,vet);
			C = b%((2*N)/mdc(N,a));
			x = 0;
			for(i=0;i<C;i++)
			{
				vet2[x] = (vet2[x] == 'o')? 'x': 'o';
				x = (x+a)%N;
			}
			printf("%s\n",vet2);
		}
	}
	return 0;
}

