// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flíper
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2454

#include<stdio.h>

main()
{
	int a, b;
	scanf("%d %d", &a, &b);
	if(a == 0)
		printf("C\n");
	else
		printf("%c\n", b? 'A' : 'B');
	return 0;
}

