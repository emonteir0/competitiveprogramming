// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: SSN 1
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1769

#include<stdio.h>

main()
{
	int n,i,j;
	char vet[15],c;
	while(scanf("%s",vet)==1)
	{
		j=1;
		n=0;
		for(i=0;i<12;i++)
		{
			c=vet[i];
			if(c>='0'&&c<='9')
			{
				n+=(c-'0')*j;
				j++;
			}
		}
		if((n%11)%10+'0'==vet[12])
		{
			n=0;
			j--;
			for(i=0;i<12;i++)
			{
				c=vet[i];
				if(c>='0'&&c<='9')
				{
					n+=(c-'0')*j;
					j--;
				}
			}
			if((n%11)%10+'0'==vet[13])
				printf("CPF valido\n");
			else
				printf("CPF invalido\n");
		}
		else	
			printf("CPF invalido\n");
	}
	return 0;
}
