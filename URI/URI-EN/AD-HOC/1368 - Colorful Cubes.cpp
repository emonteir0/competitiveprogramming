// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colorful Cubes
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1368

#include<bits/stdc++.h>

using namespace std;

set<int> conjunto, conjunto2;
int vet[6];
int pos[24][6] = {
			   {0, 1, 2, 3, 4, 5},
			   {0, 2, 3, 4, 1, 5},
			   {0, 3, 4, 1, 2, 5},
			   {0, 4, 1, 2, 3, 5},
			   {1, 0, 4, 5, 2, 3},
			   {1, 4, 5, 2, 0, 3},
			   {1, 5, 2, 0, 4, 3},
			   {1, 2, 0, 4, 5, 3},
			   {2, 0, 1, 5, 3, 4},
			   {2, 1, 5, 3, 0, 4},
			   {2, 5, 3, 0, 1, 4},
			   {2, 3, 0, 1, 5, 4},
			   {3, 2, 5, 4, 0, 1},
			   {3, 5, 4, 0, 2, 1},
			   {3, 4, 0, 2, 5, 1},
			   {3, 0, 2, 5, 4, 1},
			   {4, 0, 3, 5, 1, 2},
			   {4, 3, 5, 1, 0, 2},
			   {4, 5, 1, 0, 3, 2},
			   {4, 1, 0, 3, 5, 2},
			   {5, 1, 4, 3, 2, 0},
			   {5, 4, 3, 2, 1, 0},
			   {5, 3, 2, 1, 4, 0},
			   {5, 2, 1, 4, 3, 0}};
			   
			   
			   
			   
			   
			   

void inserir()
{
	int i, j, val;
	for(i = 0; i < 24; i++)
	{
		val = 0;
		for(j = 0; j < 6; j++)
			val = 10*val + vet[pos[i][j]];
		conjunto.insert(val);
	}
}

main()
{
	int i, N, val, c;
	while(scanf("%d", &N) == 1 && N)
	{
		conjunto = conjunto2;
		c = 0;
		while(N--)
		{
			val = 0;
			for(i = 0; i < 6; i++)
			{
				scanf("%d", &vet[i]);
				val = 10*val + vet[i];
			}
			if(conjunto.find(val) == conjunto.end())
			{
				c++;
				inserir();
			}
		}
		printf("%d\n", c);
	}
	return 0;
}
