// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colision
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1618

#include<stdio.h>

main()
{
	int n;
	int ax,ay,bx,by,x,rx,ry;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d %*d %*d %d %d %*d %*d %d %d",&ax,&ay,&bx,&by,&rx,&ry);
		printf("%s\n",(rx>=ax&&rx<=bx&&ry>=ay&&ry<=by)||(rx<=ax&&rx>=bx&&ry<=ay&&ry>=by)?"1":"0");		
	}
	return 0;
}
