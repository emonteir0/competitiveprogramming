// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escada Perfeita
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2306

#include<stdio.h>

int vet[1000001];

main()
{
	int sum = 0, N, i, cont = 0;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d", &vet[i]);
		sum += vet[i];
	}
	sum <<= 1;
	if(sum%N == 0)
	{
		sum/=N;
		if((sum^N)&1)
		{
			sum = (sum-N+1)/2;
			for(i = 0; i < N; i++, sum++)
				if(vet[i]>sum)
					cont += vet[i]-sum;
			printf("%d\n",cont);
		}
		else
			printf("-1\n");
	}
	else
		printf("-1\n");
	return 0;
}
