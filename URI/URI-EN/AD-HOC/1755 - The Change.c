// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Change
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1755

#include<stdio.h>
#include<math.h>

main()
{
	int k,D,N;
	double x,troco,trocomax;
	scanf("%d",&k);
	while(k--)
	{
		scanf("%d %d",&D,&N);
		trocomax=0;
		while(N--)
		{
			scanf("%lf",&x);
			
			if(D>=x)
			{
				troco=fmod(D,x);
				if(troco>trocomax)
					trocomax=troco;
			}
		}
		printf("%.2lf\n",trocomax);
	}
	return 0;
}

