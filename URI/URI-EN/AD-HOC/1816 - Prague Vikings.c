// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Prague Vikings?
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1816

#include<stdio.h>

int vet[27];

main()
{
	int i,n,x,k=1;
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		for(i=1;i<=26;i++)
			vet[i]=i-1;
		printf("Instancia %d\n",k++);
		while(n--)
		{
			scanf("%d",&x);
			printf("%c",'A'+vet[x]);
			for(i=x;i>1;i--)
			{
				vet[i]+=vet[i-1];
				vet[i-1]=vet[i]-vet[i-1];
				vet[i]-=vet[i-1];
			}
		}	
		printf("\n\n");
	}
	return 0;
}

