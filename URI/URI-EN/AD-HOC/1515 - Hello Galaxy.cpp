// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hello Galaxy
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1515

#include<cstdio>
#include<string>
#include<map>

using namespace std;

map<int, string> Mapa,Mapa2;
map<int, string>::iterator it;

main()
{
	int n,a,b;
	char vet[51];
	string str;
	while((scanf("%d",&n)==1)&&(n!=0))
	{
		while(n--)
		{
			scanf("%s %d %d",vet,&a,&b);
			str=vet;
			Mapa[a-b]=str;
		}
		it=Mapa.begin();
		printf("%s\n",(it->second).c_str());
		Mapa=Mapa2;
	}
	return 0;
}

