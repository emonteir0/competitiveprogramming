// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lista de Chamada
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2381

#include<bits/stdc++.h>

using namespace std;

string vet[100];

main()
{
	int i, N, M;
	char nome[21];
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
	{
		scanf("%s", nome);
		vet[i] = nome;
	}
	sort(vet, vet+N);
	printf("%s\n", vet[M-1].c_str());
	return 0;
}
