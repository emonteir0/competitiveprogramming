// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Blue Lagoon
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2178

#include<stdio.h>

main()
{
	int cont, ma=0, ant, n, m, k, x;
	scanf("%d %d", &n, &m);
	while(n--)
	{
		scanf("%d", &k);
		ant = m;
		cont = 0;
		while(k--)
		{
			scanf("%d", &x);
			if(x == 0)
				x = m;
			if(ant>x)
				cont++;
			ant = x;
		}
		if(cont > ma)
			ma = cont;
	}
	printf("%d\n", ma);
	return 0;
}
