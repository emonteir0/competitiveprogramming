// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triângulos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2397

#include<bits/stdc++.h>

using namespace std;

int vet[3];

main()
{
	scanf("%d %d %d", &vet[0], &vet[1], &vet[2]);
	sort(vet, vet+3);
	if(vet[2]>=vet[1]+vet[0])
		printf("n\n");
	else if(vet[2]*vet[2] < vet[1]*vet[1]+vet[0]*vet[0])
		printf("a\n");
	else if(vet[2]*vet[2] == vet[1]*vet[1]+vet[0]*vet[0])
		printf("r\n");
	else
		printf("o\n");
	return 0;
}
