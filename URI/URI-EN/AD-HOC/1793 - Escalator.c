// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escalator
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1793

#include<stdio.h>

main()
{
	int N,i,x,xant,cont;
	while(scanf("%d",&N)==1&&N>0)
	{
		cont=10;
		scanf("%d",&x);
		xant=x;
		for(i=1;i<N;i++)
		{
			scanf("%d",&x);
			if(x-xant>10)
				cont+=10;
			else
				cont+=(x-xant);
			xant=x;
		}
		printf("%d\n",cont);
	}
	return 0;
}
