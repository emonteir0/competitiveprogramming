// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Training With the Larvae ...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1889

#include<cstdio>
#include<cmath>
#include<algorithm>

using namespace std;

double vet[2000];

main()
{
	int i,j,N,k,esq,dir,meio;
	double a,b,c,dif,t,p,min;
	scanf("%d",&k);
	while(k--)
	{
		min=999999;
		scanf("%d",&N);
		for(i=0;i<N;i++)
			scanf("%lf",&vet[i]);
		sort(vet,vet+N);
		for(i=1;i<N-1;i++)
		{
			a=vet[i];
			for(j=i+1;j<N;j++)
			{
				b=vet[j];
				dif=b-a;
				esq=0;
				dir=i-1;
				meio=dir;
				if(vet[meio]>=dif)
				{
					while(esq!=dir)
					{
						meio=(esq+dir)/2;
						if(vet[esq]>=dif)
						{
							meio=esq;
							break;
						}
						if(vet[meio]>=dif)
							dir=meio-1;
						else
							esq=meio+1;
					}
					while(vet[meio]<dif)
						meio++;
					c=vet[meio];
					if(c>=dif)
					{
						t=sqrt((a+b+c)*(-a+b+c)*(a-b+c)*(a+b-c))/4;
						min=min<t?min:t;
					}
				}
			}
		}
		if(min!=999999)
			printf("%.2lf\n",min);
		else
			printf("-1\n");
	}
	return 0;
}

