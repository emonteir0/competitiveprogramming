// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stock Market
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1932

#include <stdio.h>

int vet[200000];

int kadane(int N, int C)
{
	int i, max_intervalo, min_intervalo, max_lucro = 0, x;
	max_intervalo = min_intervalo = vet[0];
	for(i = 1; i < N; i++)
	{
		x=vet[i];
		if((max_intervalo - x >= C) || (x < min_intervalo))
		{
			if(max_intervalo - min_intervalo - C > 0)
				max_lucro += max_intervalo - min_intervalo - C;
			max_intervalo = min_intervalo = x; 
		}
		if(x > max_intervalo)
			max_intervalo = x;
	}
	if(max_intervalo - min_intervalo - C > 0)
		max_lucro += max_intervalo - min_intervalo - C; 
	return max_lucro;
}

int main()
{
	int i, N, C;
	scanf("%d %d", &N, &C);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	printf("%d\n", kadane(N, C));
	return 0;
}
