// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: FHBZMIPS
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2536

#include<bits/stdc++.h>

using namespace std;

int N, t;
map<string, int> Mapa;
int vis[101][256];
int comandos[101];
int param[101][2];

int solve(int x, unsigned char val)
{
	int op = comandos[x];
	/*if(x == 51 && y == 50 && val == 0)
		printf("!\n");
	printf("%d %d %d\n", x, y, val);*/
	if(x == N)
		return val;
	if(op == 1)
		return solve(x+1, val + param[x][0]);
	if(op == 2)
		return solve(x+1, val - param[x][0]);
	if(op == 3)
		return solve(x+1, val * param[x][0]);
	if(op == 4)
		return solve(x+1, val / param[x][0]);
	if(op == 5)
		return solve(x+1, val & param[x][0]);
	if(op == 6)
		return solve(x+1, val | param[x][0]);
	if(op == 7)
		return solve(x+1, val ^ param[x][0]);
	if(op == 8)
	{
		if(vis[x][val] == t)
			return -1;
		vis[x][val] = t;
		return (val >= param[x][0]) ? 
			solve(param[x][1], val) : solve(x+1, val);
	}
}

main()
{
	int ans;
	char comando[11];
	Mapa["add"] = 1;
	Mapa["sub"] = 2;
	Mapa["mul"] = 3;
	Mapa["div"] = 4;
	Mapa["and"] = 5;
	Mapa["or"] = 6;
	Mapa["xor"] = 7;
	Mapa["gotoif"] = 8;
	Mapa["halt"] = 9;
	while(scanf("%d%*c", &N) == 1)
	{
		for(int i = 1; i <= N; i++)
		{
			scanf("%*d %s", comando);
			comandos[i] = Mapa[comando];
			if(comandos[i] < 8)
				scanf("%d%*c", &param[i][0]);
			else if(comandos[i] == 8)
				scanf("%d %d%*c", &param[i][0], &param[i][1]);
		}
		t++;
		ans = solve(1, 0);
		if(ans == -1)
			printf("execucao infinita\n");
		else
			printf("%d\n", ans);
	}
	return 0;
}
