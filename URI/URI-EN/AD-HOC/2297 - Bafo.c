// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bafo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2297

#include<stdio.h>

main()
{
	int k = 1, N, a, b, sa, sb;
	while(scanf("%d", &N) == 1 && N)
	{
		sa = 0;
		sb = 0;
		while(N--)
		{
			scanf("%d %d", &a, &b);
			sa += a;
			sb += b;
		}
		printf("Teste %d\n%s\n\n", k++, sa>sb?"Aldo":"Beto");
	}
	return 0;
}
