// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Tree
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2575

#include<bits/stdc++.h>
#define INF (int)10e9

using namespace std;

int N;
vector< pair<int, int>  > G[1000000], Gind[1000000];
vector<int> ans;
int visitado[1000000];

int dfs(int u)
{
	int i, val = 0, d, v, w;
	visitado[u] = 1;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i].first;
		w = G[u][i].second;
		if(!visitado[v])
		{
			d = w + dfs(v);
			if(d <= 0)
				G[u][i].second = INF;
			else
				val += d;
		}
	}
	return val;
}

void dfs2(int u)
{
	int i, v, w;
	visitado[u] = 1;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i].first;
		w = G[u][i].second;
		if(!visitado[v])
		{
			if(w == INF)
				ans.push_back(Gind[u][i].second);
			else
				dfs2(v);
		}
	}
}

main()
{
	int i, ind, u, v, w, x;
	scanf("%d", &N);
	for(i = 0; i < N-1; i++)
	{
		scanf("%d %d %d %d", &ind, &u, &v, &w);
		G[u].push_back(make_pair(v, w));
		G[v].push_back(make_pair(u, w));
		Gind[u].push_back(make_pair(v, ind));
		Gind[v].push_back(make_pair(u, ind));
	}
	x = dfs(0);
	for(i = 0; i < N; i++)
		visitado[i] = 0;
	dfs2(0);
	printf("%d %d\n", x, ans.size());
	if(ans.size() == 0)
		printf("\n");
	else
	{
		sort(ans.begin(), ans.end());
		printf("%d", ans[0]);
		for(i = 1; i < ans.size(); i++)
			printf(" %d", ans[i]);
		printf("\n");
	}
	return 0;
}
