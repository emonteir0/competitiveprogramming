// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cab Driver Route
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1947

#include<bits/stdc++.h>
#define INF 999999999
using namespace std;

set<int> conjunto;
vector<int> G[10001];
vector<int> W[10001];
vector<int> indices;
int N, K, lim;
int passageiros[16][2];
int dist[32][100001];
int dp[16][65536];

int id(int X)
{
	return lower_bound(indices.begin(), indices.end(), X) - indices.begin();
}

void dijkstra(int x, int* distancia)
{
	int u, v, w;
	priority_queue< pair<int, int> > fila;
	for(int i = 1; i <= N; i++)
		distancia[i] = INF;
	distancia[x] = 0;
	fila.push(make_pair(0, x));
	while(!fila.empty())
	{
		w = -fila.top().first;
		u = fila.top().second;
		fila.pop();
		if(w > distancia[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(distancia[v] > W[u][i] + w)
			{
				distancia[v] = W[u][i] + w;
				fila.push(make_pair(-distancia[v], v));
			}
		}
	}
}

int tsp(int x, int bit)
{
	if(bit == lim)
		return dist[id(passageiros[x][1])][passageiros[0][0]];
	if(dp[x][bit] != -1)
		return dp[x][bit];
	int r = INF;
	for(int i = 1; i <= K; i++)
	{
		if(!(bit & (1 << i)))
			r = min(r, tsp(i, bit | (1<<i)) + 
			dist[id(passageiros[x][1])][passageiros[i][0]] + 
			dist[id(passageiros[i][0])][passageiros[i][1]]);
			//printf("%d %d %d\n", tsp(i, bit | (1 << i)), dist[id(passageiros[x][1])][passageiros[i][0]],
			//dist[id(passageiros[i][0])][passageiros[i][1]]);
	}
	return dp[x][bit] = r;
}

int main()
{
	int M, u, v, w;
	scanf("%d %d %d", &N, &M, &K);
	lim = (1 << (K+1)) - 1;
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(v);
		W[u].push_back(w);
		G[v].push_back(u);
		W[v].push_back(w);
	}
	conjunto.insert(1);
	passageiros[0][0] = 1;
	passageiros[0][1] = 1;
	for(int i = 0; i < lim; i++)
		dp[0][i] = -1;
	for(int i = 1; i <= K; i++)
	{
		scanf("%d %d", &passageiros[i][0], &passageiros[i][1]);
		conjunto.insert(passageiros[i][0]);
		conjunto.insert(passageiros[i][1]);
		for(int j = 0; j < lim; j++)
			dp[i][j] = -1;
	}
	indices.assign(conjunto.begin(), conjunto.end());
	for(int i = 0; i < indices.size(); i++)
		dijkstra(indices[i], dist[i]);
	printf("%d\n", tsp(0, 1));
	return 0;
}
