// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sum of Two Squares
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1558

#include<stdio.h>

main()
{
	int n,i,cont;
	char u;
	while(scanf("%d",&n)==1)
	{
		u=1;
		if(n<0)
			printf("NO\n");
		else if(n==0)
			printf("YES\n");
		else
		{
			while(n%2==0)
			{
				n/=2;
			}
			for(i=3;i*i<=n;i+=2)
			{
				cont=0;
				while(n%i==0)
				{
					n/=i;
					cont++;
				}
				if(cont!=0&&cont%2==1&&i%4==3)
				{
					u=0;
					break;
				}
			}
			if(n>1&&n%4==3)
				u=0;
			printf("%s\n",u==1?"YES":"NO");
		}
	}
	return 0;
}
