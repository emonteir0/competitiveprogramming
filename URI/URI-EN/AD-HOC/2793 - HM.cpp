// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: HM
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2793

#include<bits/stdc++.h>
#define MOD 1000000007
#define ll long long
using namespace std;

ll expbin(int e)
{
	ll y = 2, r = 1;
	for(int i = 1; i <= e; i <<= 1)
	{
		if(i & e)
			r = (r*y)%MOD;
		y = (y*y)%MOD;;
	}
	return r;
}

char vet[10000001];

main()
{
	int s = 0, c = -1;
	scanf("%s", vet);
	for(int i = 0; vet[i]; i++)
	{
		if(vet[i] == 'F')
			s--;
		if(vet[i] == 'M')
			s++;
		if(s == 0)
			c++;
	}
	printf("%lld\n", expbin(c));
	return 0;
}

