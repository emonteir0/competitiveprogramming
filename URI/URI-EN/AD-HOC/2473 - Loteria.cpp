// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Loteria
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2473

#include<bits/stdc++.h>

using namespace std;

set<int> conjunto;

main()
{
	int N = 6, x, cont = 0;
	while(N--)
	{
		scanf("%d", &x);
		conjunto.insert(x);
	}
	N = 6;
	while(N--)
	{
		scanf("%d", &x);
		cont += conjunto.find(x) != conjunto.end();
	}
	switch(cont)
	{
		case 3: printf("terno\n");break;
		case 4: printf("quadra\n");break;
		case 5: printf("quina\n");break;
		case 6: printf("sena\n");break;
		default: printf("azar\n");
	}
	return 0;
}

