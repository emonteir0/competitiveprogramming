// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colheita de Caju
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2305

#include<bits/stdc++.h>

int mat[1000][1000], mat2[1001][1001];

main()
{
	int N, M, L, K, i, j, x, ma = 0;
	char ok;
	scanf("%d %d %d %d", &N, &M, &L, &K);
	for(i=0;i<N;i++)
		for(j=0;j<M;j++)
			scanf("%d", &mat[i][j]);
	for(i=1;i<=N;i++)
		for(j=1;j<=M;j++)
			mat2[i][j] = mat[i-1][j-1] + mat2[i-1][j] + mat2[i][j-1] - mat2[i-1][j-1];
	for(i = 0; i <= N-L; i++)
		for(j = 0; j <= M-K; j++)
			ma = std::max(ma, mat2[i+L][j+K] + mat2[i][j] - mat2[i+L][j] - mat2[i][j+K]);
	printf("%d\n", ma);
	return 0;
}
