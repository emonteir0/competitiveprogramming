// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Electricity
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1374

#include<bits/stdc++.h>

using namespace std;

int lastm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int last(int M, int Y)
{
	if(M != 2)
		return lastm[M];
	return ((Y%4 == 0) && (Y%100 != 0)) ? 29 : 28;
}

int ant(int D, int M, int Y, int Da, int Ma, int Ya)
{
	if(Y != Ya)
		return Y == Ya+1 && M == 1 && Ma == 12 && D == 1 && Da == 31;
	if(M != Ma)
	{
		return M == Ma+1 && D == 1 && Da == last(Ma, Ya);
	}
	return D == Da+1;
}

main()
{
	int N, Da, Ma, Ya, Ca, D, M, Y, C, cont, acum;
	while(scanf("%d", &N) == 1 && N)
	{
		cont = 0;
		acum = 0;
		scanf("%d %d %d %d", &Da, &Ma, &Ya, &Ca);
		while(--N)
		{
			scanf("%d %d %d %d", &D, &M, &Y, &C);
			if(ant(D, M, Y, Da, Ma, Ya))
			{
				cont++;
				acum += C-Ca;
			}
			Da = D;
			Ma = M;
			Ya = Y;
			Ca = C;
		}
		printf("%d %d\n", cont, acum);
	}
	return 0;
}
