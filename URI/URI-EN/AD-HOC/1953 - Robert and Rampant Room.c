// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Robert and Rampant Room
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1953

#include<stdio.h>
#include<string.h>

main()
{
	int N,contepr,contehd,conti;
	char vet[51];
	while(scanf("%d%*c",&N)==1)
	{
		contepr=contehd=conti=0;
		while(N--)
		{
			scanf("%*d %s",vet);
			if(strcmp(vet,"EPR")==0)
				contepr++;
			else if(strcmp(vet,"EHD")==0)
				contehd++;
			else
				conti++;
		}
		printf("EPR: %d\nEHD: %d\nINTRUSOS: %d\n",contepr,contehd,conti);
	}
	return 0;
}
