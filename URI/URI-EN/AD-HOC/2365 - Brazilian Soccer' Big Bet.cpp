// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brazilian Soccer' Big Bet
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2365

#include<bits/stdc++.h>

using namespace std;

struct torcedor
{
	string nome;
	int val;
	int x[10], y[10];
};

torcedor vet[10];

bool cmp(torcedor a, torcedor b)
{
	if (a.val != b.val)
		return a.val > b.val;
	return a.nome < b.nome;
}

main()
{
	int n, m, i, j, a, b;
	char nome[101];
	while(scanf("%d %d%*c", &n, &m) == 2 && (n||m))
	{
		for(i = 0; i < n; i++)
		{
			scanf("%s", nome);
			vet[i].nome = nome;
			for(j = 0; j < m; j++)
				scanf("%*s %d %*s %d%*c", &vet[i].x[j], &vet[i].y[j]);	
			vet[i].val = 0;
		}
		for(j = 0; j < m; j++)
		{
			scanf("%*s %d %*s %d%*c", &a, &b);	
			for(i = 0; i < n; i++)
			{
				if((a == vet[i].x[j]) && (b == vet[i].y[j]))
					vet[i].val += 10;
				else if(((a == vet[i].x[j]) || (b == vet[i].y[j])) && ((a > b) == (vet[i].x[j] > vet[i].y[j])))
					vet[i].val += 7;
				else if((a > b) == (vet[i].x[j] > vet[i].y[j]))
					vet[i].val += 5;
				else if((a == vet[i].x[j]) || (b == vet[i].y[j]))
					vet[i].val += 2;
			}
		}
		sort(vet, vet+n, cmp);
		for(i = 0; i < n; i++)
			printf("%s %d\n", vet[i].nome.c_str(), vet[i].val);
	}
	return 0;
}

