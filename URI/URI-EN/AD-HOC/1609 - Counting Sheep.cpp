// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Sheep
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1609

#include<bits/stdc++.h>

using namespace std;

set<int> conjuntoa,conjuntob;

main()
{
	int n,m,x;
	scanf("%d",&n);
	while(n--)
	{
		conjuntoa=conjuntob;
		scanf("%d",&m);
		while(m--)
		{
			scanf("%d",&x);
			conjuntoa.insert(x);
		}
		printf("%d\n",conjuntoa.size());
	}
	return 0;
}
