// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadrado
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2471

#include<stdio.h>

int linhas[50], colunas[50], mat[50][50], val[2], cont[2];

main()
{
	int i, j, N, ia, ja, valor, dif;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			scanf("%d", &mat[i][j]);
			linhas[j] += mat[i][j];
			colunas[i] += mat[i][j];
		}
	val[0] = linhas[0];
	for(i = 0; i < N; i++)
	{
		if(linhas[i] != val[0])
		{
			val[1] = linhas[i];
			cont[1]++;
		}
		else
			cont[0]++;
	}
	if(cont[0] > cont[1])
	{
		valor = val[1];
		dif = val[1] - val[0];
	}
	else
	{
		valor = val[0];
		dif = val[0] - val[1];
	}
	for(i = 0; i < N; i++)
	{
		if(linhas[i] == valor)
			ja = i;
		if(colunas[i] == valor)
			ia = i;
	}
	printf("%d %d\n", mat[ia][ja] - dif, mat[ia][ja]);
	return 0;
}
