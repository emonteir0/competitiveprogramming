// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fast Writing, the Competi...
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2843

#include<bits/stdc++.h>
#define ll unsigned long long

using namespace std;

struct st
{
	int x, e, r;
	st()
	{
	}
	st(int x, int e, int r)
	{
		this->x = x;
		this->e = e;
		this->r = r;
	}
};

st vet[65536];
int val[65536];
char str[100001];

main()
{
	int N, Z;
	ll M, x, y, k;
	scanf("%d%*c", &N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d%*c", &vet[i].r, &vet[i].e);
		vet[i].x = i;
	}
	gets(str);
	M = strlen(str);
	if(N == 1)
		return !printf("1\n");
	Z = N;
	k = 1;
	while(Z != 1)
	{
		for(int i = 0; i < Z/2; i++)
		{
			x = vet[2*i].r + M*vet[2*i].e;
			y = vet[2*i+1].r + M*vet[2*i+1].e; 
			//printf("%lld %lld\n", x, y);
			if(x < y)
			{
				val[vet[2*i].x] = k+1;
				val[vet[2*i+1].x] = k;
				vet[i] = vet[2*i];
			}
			if(x > y)
			{
				val[vet[2*i].x] = k;
				val[vet[2*i+1].x] = k+1;
				vet[i] = vet[2*i+1];
			}
			if(x == y)
			{
				if(vet[2*i].x < vet[2*i+1].x)
				{
					val[vet[2*i].x] = k+1;
					val[vet[2*i+1].x] = k;
					vet[i] = vet[2*i];
				}
				else
				{
					val[vet[2*i].x] = k;
					val[vet[2*i+1].x] = k+1;
					vet[i] = vet[2*i+1];					
				}
			}
		}
		Z /= 2;
		M *= 2;
		k++;
	}
	printf("%d", val[0]);
	for(int i = 1; i < N; i++)
		printf(" %d", val[i]);
	printf("\n");
	return 0;
}
