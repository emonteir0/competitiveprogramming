// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Catálogo de Músicas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2430

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;
vector<int> G[1000001];
int tamanho[1000001];
int folhas[100001];
int sz;

int me;
int total;
char arquivo[2001];
char temp[21];

int dfs(int u)
{
	int v;
	if(G[u].size() == 0)
		folhas[u] = 1;
	for(int i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		folhas[u] += dfs(v);
	}
	//printf("%d -> %d\n", u, folhas[u]);
	return folhas[u];
}

void dfs2(int u, int tot, int fora)
{
	int v;
	if(G[u].size() == 0)
		return;
	tot = tot - tamanho[u]*folhas[u] + 3*fora;
	me = min(me, tot);
	for(int i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		dfs2(v, tot, fora+folhas[u]-folhas[v]);
	}
}

main()
{
	int N, p, k;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		p = 0;
		k = 0;
		scanf("%s", arquivo);
		for(int j = 0; arquivo[j]; j++)
		{
			temp[k++] = arquivo[j];
			if(arquivo[j] == '/')
			{
				temp[k] = 0;
				if(Mapa.count(temp) <= 0)
				{
					Mapa[temp] = ++sz;
					G[p].push_back(sz);
				}
				p = Mapa[temp];
				tamanho[p] = k;
				total += k;
				k = 0;
			}
		}
		temp[k] = 0;
		Mapa[temp] = ++sz;
		tamanho[sz] = k;
		total += k;
		G[p].push_back(sz);
	}
	dfs(0);
	me = total;
	for(int i = 0; i < G[0].size(); i++)
		dfs2(G[0][i], total, folhas[0]-folhas[G[0][i]]);
	printf("%d\n", me);
	return 0;
}
