// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Chocolate
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2328

#include<stdio.h>

main()
{
	int acum = 0, N, x;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		acum += x-1;
	}
	printf("%d\n", acum);
	return 0;
}
