// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kermesse
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2189

#include<stdio.h>

main()
{
	int N, i, x, k = 1;
	while(scanf("%d", &N) == 1 && N)
	{
		printf("Teste %d\n", k++);
		for(i=1;i<=N;i++)
		{
			scanf("%d", &x);
			if(x==i)
				printf("%d\n\n", x);
		}
	}
	return 0;
}
