// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Palindrome
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2795

#include<bits/stdc++.h>

using namespace std;

#define MAX 404

int mat[MAX][MAX];

string s;
int n, k;

int custo(int i, int j){
	int res = 0;
	for(int k = 0; k <= (j - i)/2; k++){
		res += min(abs(s[i + k] - s[j-k]), 26 - abs(s[i + k] - s[j-k]));
	}
	
	return res;
}

int mem[MAX][MAX][MAX];

int dp(int p, int r, int a){
	if(p == n - 1)
		return mat[a][p];
	
	if(mem[p][r][a] == -1){
		if(r > 0)
			mem[p][r][a] = min(dp(p+1, r-1, p + 1) + mat[a][p], dp(p+1, r, a));
		else
			mem[p][r][a] = dp(p+1, r, a);
	}
	
	return mem[p][r][a];
	
	
}

int main(){
	cin >> n >> k;
	cin >> s;
	
	for(int i = 0; i < n; i++)
		for(int j = i; j < n; j++)
			mat[i][j] = custo(i, j);
	
	//for(int i = 0; i < n; i++)
	//	for(int j = i; j < n; j++)
		//	printf("%d%c", mat[i][j], " \n"[j == n-1]);
			
	memset(mem, -1, sizeof(mem));		
	
	printf("%d\n", dp(0, k - 1, 0)); 
	
	return 0;
}

