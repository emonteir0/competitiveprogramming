// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ontarama Meraini
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2578

#include<stdio.h>
#include<string.h>
#define ll long long

char msg[1000001], msg2[1000001];
ll acum[26], result[26], ans;
int start, tam, tam2;

ll func(int i)
{
	int j;
	char ok;
	ll x, y;
	ok = 1;
	for(j = 0; j < 26; j++)
	{
		if(i >= tam2)
		acum[j] += (msg[i]-'a' == j) - (msg[i-tam2]-'a' == j);
		ok &= (acum[j] == result[j]);
	}
	if(ok)
	{
		x = i+1-tam2-start;
		y = tam-1-i;
		ans += (x+1) * (y+1);
		start = i+2-tam2;
	}
}

main()
{
	int i, j;
	scanf("%s%s", msg, msg2);
	tam = strlen(msg);
	for(i = 0; msg2[i]; i++)
	{
		result[msg2[i]-'a']++;
		tam2++;
	}
	for(i = 0; i < tam2; i++)
		for(j = 0; j < 26; j++)
			acum[j] += (msg[i]-'a' == j);	
	for(i = tam2-1; i < tam; i++)
	{
		func(i);
	}
	printf("%lld\n", ans);
	return 0;
}
