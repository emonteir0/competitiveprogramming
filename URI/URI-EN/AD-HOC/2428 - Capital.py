# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Capital
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2428

def func(A, B, C, D):
	return (A*B == C*D) or (A*C == B*D) or (A*D == B*C)


v = raw_input().split(' ')
A = int(v[0])
B = int(v[1])
C = int(v[2])
D = int(v[3])

print 'S' if func(A, B, C, D) else 'N'

