// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of The Greatest
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1397

#include<stdio.h>

main()
{
	int a,b,n,i,cont,cont2;
	while(scanf("%d",&n)==1&&n)
	{
		cont=cont2=0;
		for(i=0;i<n;i++)
		{
			scanf("%d %d",&a,&b);
			cont+=(a>b);
			cont2+=(a<b);
		}
		printf("%d %d\n",cont,cont2);
	}
	return 0;
}
