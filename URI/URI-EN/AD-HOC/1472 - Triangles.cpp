// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangles
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1472

#include<cstdio>
#include<set>

using namespace std;

set<int> conjunto,conjunto2;
set<int>::iterator it;


main()
{
	int n,x,c,cont;
	while(scanf("%d",&n)==1)
	{
		c=0;
		while(n--)
		{
			scanf("%d",&x);
			c+=x;
			conjunto.insert(c);
		}
		if(c%3!=0)
			printf("0\n");
		else
		{
			cont=0;
			c/=3;
			for(it=conjunto.begin();it!=conjunto.end();++it)
			{
				x=*it;
				if(*it>c)
					break;
				else
				{
					if((conjunto.find(x+c)!=conjunto.end())&&(conjunto.find(x+c+c)!=conjunto.end()))
						cont++;
				}
			}
			printf("%d\n",cont);
		}
		conjunto=conjunto2;
	}
	return 0;
}

