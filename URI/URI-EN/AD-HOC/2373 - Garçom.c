// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Garçom
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2373

#include<stdio.h>

main()
{
	int N, a, b, acum = 0;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d", &a, &b);
		if(a > b)
			acum += b;
	}
	printf("%d\n", acum);
	return 0;
}
