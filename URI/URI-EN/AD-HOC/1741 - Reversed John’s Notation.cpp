// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Reversed John’s Notation
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1741

#include<bits/stdc++.h>

using namespace std;

struct node
{
	char val;
	int adj[2];
	node()
	{
		val = 0;
		adj[0] = -1;
		adj[1] = -1;
	}
};

int operand(char c)
{
	return c >= '0' && c <= '9';
}

int sz;
node tree[2000001];
string str;
int ind, flag, N;

int buildtree()
{
	int x, a, b;
	if(ind >= N)
	{
		ind++;
		return 1;
	}
	while(str[ind] == ' ')
		ind++;
	char ch = str[ind++];
	if(operand(ch))
		return ch-'0';
	else
	{
		b = buildtree();
		a = buildtree();
		if(ch == '+')
			x = a + b;
		if(ch == '-')
			x = a - b;
		if(ch == '*')
			x = a * b;
		if(ch == '/')
		{
			if(b == 0)
			{
				flag = 0;
				b = 1;
			}
			x = a / b;
		}
	}
	return x;
}

main()
{
	int x;
	while(getline(cin, str))
	{
		flag = 1;
		ind = 0;
		sz = 1;
		N = str.length();
		tree[0] = node();
		x = buildtree();
		if(ind != N)
			printf("Invalid expression.\n");
		else
		{
			if(flag)
				printf("The answer is %d.\n", x);
			else
				printf("Division by zero.\n");
		}
	}
	return 0;
}
