// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Random Bingo Cards
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1771

#include<stdio.h>

main()
{
	int x,a[5],i,j;
	char u;
	while(1)
	{
		u=1;
		for(i=0;i<5;i++)
		{
			a[i]=0;
		}
		if(scanf("%d",&x)==EOF)
			break;
		a[(--x)/15]++;
		if(x/15!=0)
			u=0;	
		for(j=1;j<5;j++)
		{
			scanf("%d",&x);
			a[(--x)/15]++;
			if(x/15!=j)
				u=0;
		}
		for(j=0;j<5;j++)
		{
			scanf("%d",&x);
			a[(--x)/15]++;
			if(x/15!=j)
				u=0;		
		}
		for(j=0;j<2;j++)
		{
			scanf("%d",&x);
			a[(--x)/15]++;
			if(x/15!=j)
				u=0;
		}
		for(j=3;j<5;j++)
		{
			scanf("%d",&x);
			a[(--x)/15]++;
			if(x/15!=j)
				u=0;
		}
		for(i=0;i<2;i++)
		{
			for(j=0;j<5;j++)
			{
				scanf("%d",&x);
				a[(--x)/15]++;
			if(x/15!=j)
				u=0;
			}
		}
		if(u==1)
			printf("OK\n");
		else if(a[0]==5&&a[1]==5&&a[2]==4&&a[3]==5&&a[4]==5)
			printf("RECICLAVEL\n");
		else
			printf("DESCARTAVEL\n");
	}
	return 0;
}
