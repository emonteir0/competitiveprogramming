// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibonacci Words
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2208

#include<bits/stdc++.h>
#define MAXN 101
#define MAXL 100001
#define ll long long

using namespace std;

string P, prefix[MAXN], suffix[MAXN];
int b[MAXL], m;
ll dp[MAXN];

void kmpPre()
{
	int i = 0, j = -1;
	b[0] = -1;
	while( i < m)
	{
		while(j >= 0 && P[i] != P[j]) j = b[j];
		i++, j++;
		b[i] = j;
	}
}

int kmpSearch(const string &T)
{
	int i = 0, j = 0, n = T.size();
	int cont = 0;
	while(i < n)
	{
		while(j >= 0 && T[i] != P[j]) j = b[j];
		i++, j++;
		if(j == m)
		{
			cont++;
			j = b[j];
		}
	}
	return cont;
}

main()
{
	int i, k, T = 1;
	string str;
	while(scanf("%d", &k) == 1)
	{
		cin >> P;
		m = P.size();
		kmpPre();
		prefix[0] = "0";
		suffix[0] = "0";
		dp[0] = kmpSearch("0");
		prefix[1] = "1";
		suffix[1] = "1";
		dp[1] = kmpSearch("1");
		for(i = 2; i <= k; i++)
		{
			prefix[i] = prefix[i-1] + prefix[i-2];
			suffix[i] = suffix[i-1] + suffix[i-2];
			dp[i] = kmpSearch(prefix[i]);
			if((int)suffix[i-2].size() > 4*m)
				break;
		}
		for(++i; i <= k; i++)
		{
			prefix[i] = prefix[i-1];
			suffix[i] = suffix[i-2];
			//printf("TESTE: %s\n", (suffix[i-2] + prefix[i-1]).c_str());
			dp[i] = kmpSearch(suffix[i-1] + prefix[i-2]) - kmpSearch(suffix[i-1]) - kmpSearch(prefix[i-2]) + dp[i-1] + dp[i-2];
			//printf("%d: %s|%s%s|%s\n", i, prefix[i].c_str(), suffix[i-2].c_str(), prefix[i-1].c_str(), suffix[i].c_str());
		}
		printf("Case %d: %lld\n", T++, dp[k]);
	}
	return 0;
}
