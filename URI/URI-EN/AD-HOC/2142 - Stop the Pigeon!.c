// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stop the Pigeon!
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2142

#include<stdio.h>
#include<math.h>

int r;
int mat[1001][1001];
int dx[4] = {0, 1, 0, -1}, dy[4] = {1, 0, -1, 0};

int inRange(int x)
{
	return x > 0 && x <= r;
}

main()
{
	int N, M, x0, y0, x1, y1, z, i, j, dir, f, g, dig;
	while(scanf("%d %d", &N, &M) == 2)
	{
		if(N < 3*M)
			printf("Raios! Raios Duplos! Raios Triplos!\n");
		else
		{
			f = N;
			dig = 0;
			while(f != 0)
			{
				f /= 10;
				dig++;
			}
			r = sqrt(N);
			x0 = 1;
			y0 = 1;
			z = N;
			dir = 0;
			for(i = 1; i <= r; i++)
				for(j = 1; j <= r; j++)
					mat[i][j] = 0;
			while(z != 0)
			{
				mat[x0][y0] = z--;
				x1 = x0+dx[dir];
				y1 = y0+dy[dir];
				if(inRange(x1) && inRange(y1))
				{
					if(mat[x1][y1] != 0)
						dir = (dir+1)%4;
					x0 += dx[dir];
					y0 += dy[dir];
				}
				else
				{
					dir = (dir+1)%4;
					x0 += dx[dir];
					y0 += dy[dir];
				}
			}
			f = 2*M;
			g = 3*M;
			for(i = 1; i <= r; i++)
			{
				for(j = 1; j < r; j++)
				{
					if(mat[i][j] == f)
					printf("%*c ", dig, '*');
					else if(mat[i][j] == g)
						printf("%*c ", dig, '!');
					else
						printf("%*d ", dig, mat[i][j]);
				}
				if(mat[i][r] == f)
					printf("%*c%s", dig, '*', (i==r)?"":" ");
					else if(mat[i][r] == g)
						printf("%*c%s", dig, '!', (i==r)?"":" ");
					else
						printf("%*d%s", dig, mat[i][j], (i==r)?"":" ");
				printf("\n");
			}
		}
	}
	return 0;
}
