// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Penalidade Mínima
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2319

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

pair<int, int> dp[1001][1001][2];
int mat[1001][1001];

pair<int, int> operator+(pair<int, int> a, pair<int, int> b)
{
	return make_pair(a.first+b.first, a.second+b.second);
}

bool cmp2(pair<int, int> a, pair<int, int> b)
{
	if(a.first != b.first)
		return a.first < b.first;
	return a.second < b.second;
}

bool cmp5(pair<int, int> a, pair<int, int> b)
{
	if(a.second != b.second)
		return a.second < b.second;
	return a.first < b.first;
}

main()
{
	int N, c2, c5;
	pair<int, int> p[4];
	scanf("%d", &N);
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= N; j++)
			scanf("%d", &mat[i][j]);
	for(int j = 1; j <= N; j++)
	{
		c2 = c5 = 0;
		if(mat[1][j] == 0)
		{
			dp[1][j][0] = dp[1][j][1] = make_pair(INF, INF);
			continue;
		}
		while(mat[1][j]%2 == 0 && mat[1][j])
		{
			c2++;
			mat[1][j] /= 2;
		}
		while(mat[1][j]%5 == 0 && mat[1][j])
		{
			c5++;
			mat[1][j] /= 5;
		}
		dp[1][j][0] = dp[1][j-1][0] + make_pair(c2, c5);
		dp[1][j][1] = dp[1][j-1][1] + make_pair(c2, c5);
	}
	for(int i = 2; i <= N; i++)
	{
		c2 = c5 = 0;
		if(mat[i][1] == 0)
		{
			dp[i][1][0] = dp[i][1][1] = make_pair(INF, INF);
			continue;
		}
		while(mat[i][1]%2 == 0 && mat[i][1])
		{
			c2++;
			mat[i][1] /= 2;
		}
		while(mat[i][1]%5 == 0 && mat[i][1])
		{
			c5++;
			mat[i][1] /= 5;
		}
		dp[i][1][0] = dp[i-1][1][0] + make_pair(c2, c5);
		dp[i][1][1] = dp[i-1][1][1] + make_pair(c2, c5);
	}
	for(int i = 2; i <= N; i++)
		for(int j = 2; j <= N; j++)
		{
			c2 = c5 = 0;
			if(mat[i][j] == 0)
			{
				dp[i][j][0] = dp[i][j][1] = make_pair(INF, INF);
				continue;
			}
			while(mat[i][j]%2 == 0 && mat[i][j])
			{
				c2++;
				mat[i][j] /= 2;
			}
			while(mat[i][j]%5 == 0 && mat[i][j])
			{
				c5++;
				mat[i][j] /= 5;
			}
			p[0] = dp[i-1][j][0] + make_pair(c2, c5);
			p[1] = dp[i-1][j][1] + make_pair(c2, c5);
			p[2] = dp[i][j-1][0] + make_pair(c2, c5);
			p[3] = dp[i][j-1][1] + make_pair(c2, c5);
			sort(p, p+4, cmp2);
			dp[i][j][0] = p[0];
			sort(p, p+4, cmp5);
			dp[i][j][1] = p[0];
		}
	/*for(int i = 1; i <= N; i++)
		for(int j = 1; j <= N; j++)
			printf("dp[%d][%d] = (%d, %d)|(%d, %d)\n", i, j, dp[i][j][0].first, dp[i][j][0].second, dp[i][j][1].first, dp[i][j][1].second);*/
	printf("%d\n", min(min(dp[N][N][0].first, dp[N][N][0].second), min(dp[N][N][1].first, dp[N][N][1].second)));
	return 0;
}
