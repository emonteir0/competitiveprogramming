// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Naval Battle
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2371

#include<stdio.h>

int mat[100][100];
int N, M;
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return (x >= 0) && (x < N) && (y >= 0) && (y < M);
}

int dfs(int x, int y)
{
	int i, x2, y2;
	int ok = mat[x][y] == 2;
	mat[x][y] = 3;
	for(i = 0; i < 4; i++)
	{
		x2 = x + dx[i];
		y2 = y + dy[i];
		if(inRange(x2, y2))
		{
			if(mat[x2][y2] != 0 && mat[x2][y2] != 3)
				ok &= dfs(x2, y2);
		}
	}
	return ok;
}

main()
{
	int i, j, Q, x, y, cont = 0, ok;
	char c;
	scanf("%d %d%*c", &N, &M);
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			scanf("%c", &c);
			if(c == '#')
				mat[i][j] = 1;
		}
		scanf("%*c");
	}
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d %d", &x, &y);
		if(mat[x-1][y-1] == 1)
			mat[x-1][y-1] = 2;
	}
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			if(mat[i][j] != 0 && mat[i][j] != 3)
			{
				ok = dfs(i, j);
				cont += ok;
			}
		}
	}
	printf("%d\n", cont);
	return 0;
}

