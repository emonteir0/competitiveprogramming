// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Overflow
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2342

#include<stdio.h>

main()
{
	int a, b, c;
	char op;
	scanf("%d%*c%d %c %d", &a, &b, &op, &c);
	b = (op == '*') ? b*c : b+c;
	printf("%s\n", (b <= a) ? "OK" : "OVERFLOW"); 
	return 0;
}

