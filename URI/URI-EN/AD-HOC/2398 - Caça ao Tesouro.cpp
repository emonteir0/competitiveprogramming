// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Caça ao Tesouro
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2398

#include<bits/stdc++.h>

using namespace std;

int vis[100][100];
int mat[100][100];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};
int N;


struct pos
{
	int x, y, d;
	pos(int x, int y, int d)
	{
		this->x = x;
		this->y = y;
		this->d = d;
	}
};


bool inRange(int x, int y)
{
	return x >= 0 && x < N && y >= 0 && y < N;
}

int main()
{
	int M, x, y, x2, y2, d, dist, indx = -1, indy = -1;
	scanf("%d %d", &N, &M);
	queue<pos> fila;
	for(int t = 1; t <= M; t++)
	{
		scanf("%d %d %d", &x, &y, &d);
		fila.push(pos(x, y, 0));
		while(!fila.empty())
		{
			x = fila.front().x;
			y = fila.front().y;
			dist = fila.front().d;
			fila.pop();
			if(vis[x][y] == t)
				continue;
			vis[x][y] = t;
			if(dist == d)
			{
				mat[x][y]++;
				continue;
			}
			for(int k = 0; k < 4; k++)
			{
				x2 = x + dx[k];
				y2 = y + dy[k];
				if(inRange(x2, y2) && vis[x2][y2] < t)
					fila.push(pos(x2, y2, dist+1));
			}
		}
	}
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < N; j++)
		{
			if(mat[i][j] == M)
			{
				if(indx == -1)
				{
					indx = i;
					indy = j;
				}
				else
					return !printf("-1 -1\n");
			}
		}
	}
	printf("%d %d\n", indx, indy);
	return 0;
}
