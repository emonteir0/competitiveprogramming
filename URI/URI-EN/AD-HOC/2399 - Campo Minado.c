// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Campo Minado
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2399

#include<stdio.h>

int vet[50];

main()
{
	int i, j, N, s;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(i = 0; i < N; i++)
	{
		s = 0;
		for(j = -1; j <= 1; j++)
			if(i+j >= 0  && i+j < N)
				s += vet[i+j];
		printf("%d\n", s);
	}
	return 0;
}

