// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadrado Mágico
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2407

#include<stdio.h>
#define ll long long

ll sl[1000], sc[1000], dp;
int vet[1000001];

main()
{
	int i, j, N, x;
	char bol = 1;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			scanf("%d", &x);
			if(x > 1000000)
				bol = 0;
			else if(vet[x])
				bol = 0;
			else
				vet[x] = 1;
			sl[i] += x;
			sc[j] += x;
			if(i == j)
				dp += x;
		}
	if(!bol)
		printf("0\n");
	else
	{
		for(i = 0; i < N; i++)
			bol &= (sl[i] == sc[i]);
		bol &= (dp == sl[0]);
		printf("%lld\n", bol? dp: 0);
	}
	return 0;
}

