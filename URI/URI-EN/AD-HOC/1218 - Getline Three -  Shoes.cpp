// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Getline Three -  Shoes
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1218

#include<bits/stdc++.h>

using namespace std;

int compare(char a, char b)
{
	if(a>='a'&&a<='z')
		a+='A'-'a';
	if(b>='a'&&b<='z')
		b+='A'-'a';
	return a==b;
}

main()
{
	string str,str2;
	char c, vet[1201];
	bool b;
	int x,contm,contf,n,k=1;
	while(scanf("%d%*c",&x)==1)
	{
		if(k>1)
			printf("\n");
		contm=0;
		contf=0;
		gets(vet);
		str=vet;
		stringstream ss(str);
		while(ss >> str && ss >> str2)
		{
			n=atoi(str.c_str());
			if(n==x)
			{
				if(str2[0]=='M')
					contm++;
				else
					contf++;
			}
		}
		printf("Caso %d:\nPares Iguais: %d\nF: %d\nM: %d\n",k++,contm+contf,contf,contm);
	}
	return 0;
}
