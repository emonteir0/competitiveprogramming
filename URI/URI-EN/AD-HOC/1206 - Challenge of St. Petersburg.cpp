// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Challenge of St. Petersburg
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1206

#include<bits/stdc++.h>

using namespace std;

int N;
int mat[8][8];
char vet[10][4];
int sizemoves[3];
int movesx[3][8];
int movesy[3][8];

map<char, int> Mapa;

int inRange(int x, int y){return x >= 0 && x < 8 && y >= 0 && y < 8;}

int simulacao(int xr, int yr)
{
	int tipo;
	int x, y, x2, y2;
	for(int i = 0; i < 8; i++)
		for(int j = 0; j < 8; j++)
			mat[i][j] = 0;
	for(int i = 0; i < N; i++)
		mat[vet[i][2]-'1'][vet[i][1]-'a'] = 2; 
	mat[xr][yr] = 1;
	for(int i = 0; i < N; i++)
	{
		tipo = Mapa[vet[i][0]];
		x = vet[i][2]-'1';
		y = vet[i][1]-'a';
		if(x == xr && y == yr)
			continue;
		if(tipo <= 2)
		{
			for(int k = 0; k < sizemoves[tipo]; k++)
			{
				x2 = x;
				y2 = y;
				while(1)
				{
					x2 += movesx[tipo][k];
					y2 += movesy[tipo][k];
					if(!inRange(x2, y2) || mat[x2][y2] == 2)
						break;
					if(x2 == xr && y2 == yr)
						return 1;
				}
			}
		}
		else if(tipo == 3)
		{
			if(abs(xr-x) <= 1 && abs(yr-y) <= 1)
				return 1;
		}
		else
		{
			if(x-1 == xr && abs(yr-y) == 1)
				return 1;
		}
	}
	return 0;
}

main()
{
	int x, y, r;
	char rei[4];
	Mapa['T'] = 0;
	Mapa['B'] = 1;
	Mapa['R'] = 2;
	Mapa['W'] = 3;
	Mapa['P'] = 4;
	sizemoves[0] = 4;
	sizemoves[1] = 4;
	sizemoves[2] = 8;
	movesx[0][0] =  1, movesx[0][1] = -1, movesx[0][2] =  0, movesx[0][3] =  0;
	movesy[0][0] =  0, movesy[0][1] =  0, movesy[0][2] =  1, movesy[0][3] = -1;
	
	movesx[1][0] =  1, movesx[1][1] = -1, movesx[1][2] =  1, movesx[1][3] = -1;
	movesy[1][0] = -1, movesy[1][1] = -1, movesy[1][2] =  1, movesy[1][3] =  1;
	
	
	movesx[2][0] =  1, movesx[2][1] = -1, movesx[2][2] =  1, movesx[2][3] = -1,
	movesx[2][4] =  1, movesx[2][5] = -1, movesx[2][6] =  0, movesx[2][7] =  0;
	movesy[2][0] = -1, movesy[2][1] = -1, movesy[2][2] =  1, movesy[2][3] =  1,
	movesy[2][4] =  0, movesy[2][5] =  0, movesy[2][6] =  1, movesy[2][7] = -1;
	while(scanf("%d", &N) == 1)
	{
		r = 1;
		for(int i = 0; i < N; i++)
			scanf("%s", vet[i]);
		scanf("%s", rei);
		x = rei[2]-'1';
		y = rei[1]-'a';
		for(int i = -1; i <= 1; i++)
			for(int j = -1; j <= 1; j++)
				if(inRange(x+i, y+j))
					r &= simulacao(x+i, y+j);
		printf("%s\n", r ? "SIM" : "NAO");
	}
	return 0;
}
