// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Autorama
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2316

#include<bits/stdc++.h>

using namespace std;

int K, N, M;

int carros[101];
int ultima[101];
int indices[101];

bool cmp(int a, int b)
{
	if(carros[a] != carros[b])
		return carros[a] > carros[b];
	return ultima[a] < ultima[b];
}

main()
{
	int x, y;
	scanf("%d %d %d", &K, &N, &M);
	for(int i = 1; i <= M; i++)
	{
		scanf("%d %d", &x, &y);
		if(y%K == (carros[x]+1)%K)
		{
			carros[x]++;
			ultima[x] = i;
		}
	}
	for(int i = 0; i < N; i++)
		indices[i] = i+1;
	sort(indices, indices+N, cmp);
	printf("%d", indices[0]);
	for(int i = 1; i < N; i++)
		printf(" %d", indices[i]);
	printf("\n");
	return 0;
}
