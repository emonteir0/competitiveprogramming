// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Confederation
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1710

#include<cstdio>
#include<string>
#include<map>

using namespace std;

map<string,int> Mapa;
string a="";

typedef struct a
{
	int a,b,c,d;
}plano;

plano planos[500];

main()
{
	int N,M,i,j,x,y,z,val,ma=1;
	scanf("%d %d",&N,&M);
	for(i=0;i<N;i++)
	{
		scanf("%d %d %d %d",&planos[i].a,&planos[i].b,&planos[i].c,&planos[i].d);
	}
	for(j=0;j<M;j++)
	{
		scanf("%d %d %d",&x,&y,&z);
		a="";
		for(i=0;i<N;i++)
		{
			val=planos[i].a*x+planos[i].b*y+planos[i].c*z-planos[i].d;
			a+=(val>0)?"1":"0";
		}
		if(Mapa.find(a)==Mapa.end())
		{
			Mapa[a]=1;
		}
		else
		{
			Mapa[a]++;
			if(ma<Mapa[a])
			{
				ma=Mapa[a];
			}
		}
	}
	printf("%d\n",ma);
	return 0;
}
