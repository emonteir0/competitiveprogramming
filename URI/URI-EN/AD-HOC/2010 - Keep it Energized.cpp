// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Keep it Energized
// Level: 7
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2010

#include<bits/stdc++.h>
#define MAXT 400005
#define MAXN 100001
#define ll long long
#define INF 10e15

using namespace std;

struct st
{
	int L, E, C;	
};

bool cmp(st a, st b)
{
	return a.L > b.L;
}

ll tree[MAXT];
ll dp[MAXN];
ll acum[MAXN];
st v[MAXN];


void build(int node, int start, int end)
{
	if(start == end)
		tree[node] = INF;
	else
	{
		int mid = (start+end)/2;
		build(node*2, start, mid);
		build(node*2+1, mid+1, end);
		tree[node] = min(tree[node*2], tree[node*2+1]);
	}
}

void update(int node, int start, int end, int x, ll val)
{
	if(start == x && end == x)
	{
		tree[node] = val;
		return;
	}
	int mid = (start+end)/2;
	if(mid >= x)
		update(node*2, start, mid, x, val);
	else
		update(node*2+1, mid+1, end, x, val);
	tree[node] = min(tree[node*2], tree[node*2+1]);
}

ll query(int node, int start, int end, int l, int r)
{
	if(start > end || start > r || end < l)
		return INF;
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start+end)/2;
	ll p1 = query(node*2, start, mid, l, r);
	ll p2 = query(node*2+1, mid+1, end, l, r);
	return min(p1, p2);
}

main()
{
	int N, M;
	scanf("%d %d", &N, &M);
	build(1, 0, N);
	update(1, 0, N, N, 0);
	for(int i = 1; i <= N; i++)
	{
		scanf("%lld", &acum[i]);
		acum[i] += acum[i-1];
		dp[i-1] = INF;
	}
	for(int i = 0; i < M; i++)
	{
		scanf("%d %d %d", &v[i].L, &v[i].E, &v[i].C);
		v[i].L--;
	}
	sort(v, v+M, cmp);
	for(int i = 0; i < M; i++)
	{
		int lo = v[i].L, hi = N, mid, ans = v[i].L;
		while(lo <= hi)
		{
			mid = (lo+hi)/2;
			if(acum[v[i].L] + v[i].E >= acum[mid])
			{
				ans = mid;
				lo = mid+1;
			}
			else
				hi = mid-1;
		}
		dp[v[i].L] = min(dp[v[i].L], query(1, 0, N, v[i].L, ans) + v[i].C);
		update(1, 0, N, v[i].L, dp[v[i].L]);
	}
	printf("%lld\n", dp[0] != INF ? dp[0] : -1);
	return 0;
}
