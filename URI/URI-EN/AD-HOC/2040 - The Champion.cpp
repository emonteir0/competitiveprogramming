// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Champion
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2040

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa, Mapa2;
vector< pair<string, int> > V;
char str[1001], str2[1001];

bool cmp(pair<string, int> a, pair<string, int> b)
{
	return a.second > b.second;
}

main()
{
	int N, x, y;
	while(scanf("%d", &N) == 1 && N)
	{
		Mapa = Mapa2;
		for(int i = 0; i < N; i++)
		{
			scanf("%s %d", str, &x);
			Mapa[str] = x;
		}
		N >>= 1;
		for(int i = 0; i < N; i++)
		{
			scanf("%s %d-%d %s", str, &x, &y, str2);
			Mapa[str] += 3*x;
			Mapa[str2] += 3*y;
			if(x == y)
				Mapa[str]++, Mapa[str2]++;
			else if(x > y)
				Mapa[str] += 5;
			else
				Mapa[str2] += 5;
		}
		V.assign(Mapa.begin(), Mapa.end());
		sort(V.begin(), V.end(), cmp);
		if(V[0].first == "Sport")
			printf("O Sport foi o campeao com %d pontos :D\n\n", V[0].second);
		else
			printf("O Sport nao foi o campeao. O time campeao foi o %s com %d pontos :(\n\n", V[0].first.c_str(), V[0].second);
	}
	return 0;
}
