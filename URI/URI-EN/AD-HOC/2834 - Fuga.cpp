// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fuga
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2834

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int N, M;
ll bits[6][6];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

bool inRange(int x, int y)
{
	return x >= 0 && x <= N && y >= 0 && y <= M;
}

main()
{
	queue< pair<pair<int, ll>, pair<int, int> > > fila;
	int x, y, z, w, d, x2, y2, ma = 0;
	ll b;
	scanf("%d %d", &N, &M);
	N /= 2, M /= 2;
	scanf("%d %d", &x, &y);
	x /= 2, y /= 2;
	scanf("%d %d", &z, &w);
	z /= 2, w /= 2;
	b = 1;
	for(int  i = 0; i <= N; i++)
		for(int j = 0; j <= M; j++)
		{
			bits[i][j] = b;
			b <<= 1;
		}
	fila.push({{0, bits[x][y]}, {x, y}});
	while(!fila.empty())
	{
		d = fila.front().first.first;
		b = fila.front().first.second;
		x = fila.front().second.first;
		y = fila.front().second.second;
		fila.pop();
		if(x == z && y == w)
		{
			ma = max(ma, 2*d+1);
			continue;
		}
		for(int i = 0; i < 4; i++)
		{
			x2 = x + dx[i];
			y2 = y + dy[i];
			if(inRange(x2, y2) && !(b & bits[x2][y2]))
				fila.push({{d+1, b | bits[x2][y2]}, {x2,y2}});
		}
	}
	printf("%d\n", ma);
	return 0;
}
