// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Number Frequence
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1171

#include<stdio.h>

int vet[2001];

main()
{
	int n,i,x;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d",&x);
		vet[x]++;
	}
	for(i=1;i<=2000;i++)
		if(vet[i]!=0)
			printf("%d aparece %d vez(es)\n",i,vet[i]);
	return 0;
}
