// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fake Tickets
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1318

#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

int vet[10001];

main()
{
	int M,x,i,cont;
	while((scanf("%*d %d",&M)==1)&&(M>0))
	{
		cont=0;
		for(i=0;i<M;i++)
		{
			scanf("%d",&x);
			if(vet[x]==1)
				cont++;
			vet[x]++;
		}
		printf("%d\n",cont);
		memset(vet,0,10001);
	}
	return 0;
}

