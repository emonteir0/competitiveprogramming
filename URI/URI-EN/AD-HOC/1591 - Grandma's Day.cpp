// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grandma's Day
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1591

#include<cstdio>
#include<string>

using namespace std;

string palavras[100];
main()
{
	int testes,i,j,n,m,k,l,it,cont;
	char palavra[51][50], palavra2[51];
	scanf("%d%*c",&testes);
	while(testes--)
	{
		scanf("%d %d%*c",&n,&m);
		k=0;
		for(i=0;i<n;i++)
		{
			scanf("%s",palavra[i]);
			palavras[k++]=palavra[i];
		}
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
			{
				palavra2[j]=palavra[j][i];
			}
			palavra2[j]=0;
			palavras[k++]=palavra2;
		}
		scanf("%d%*c",&l);
		while(l--)
		{
			cont=0;
			scanf("%s",palavra2);
			for(i=0;i<k;i++)
			{
				it=palavras[i].find(palavra2);
				while(it!=string::npos)
				{
					cont++;
					it++;
					it=palavras[i].find(palavra2,it);
				}
			}
			if(palavra2[1]==0)
				cont/=2;
			printf("%d\n",cont);
		}
	}
	return 0;
}
