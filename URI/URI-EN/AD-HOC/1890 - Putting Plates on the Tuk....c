// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Putting Plates on the Tuk...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1890

#include<stdio.h>

int c[7],v[10];


main()
{
	int i,N,x,y;
	c[0]=1;
	v[0]=1;
	for(i=1;i<7;i++)
		c[i]=c[i-1]*26;
	for(i=1;i<10;i++)
		v[i]=v[i-1]*10;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %d",&x,&y);
		if(x==0 && y==0)
			printf("0\n");
		else
			printf("%d\n",c[x]*v[y]);
	}
	return 0;
}

