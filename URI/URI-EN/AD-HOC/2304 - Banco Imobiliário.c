// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Banco Imobiliário
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2304

#include<stdio.h>

main()
{
	int n, a, b, c, x;
	char op1, op2, op3;
	scanf("%d %d%*c", &x, &n);
	a = b = c = x;
	while(n--)
	{
		scanf("%c ", &op1);
		if(op1 != 'A')
		{
			scanf("%c %d%*c", &op2, &x);
			if(op2 == 'D')
				a += (op1 == 'V')?x:-x;
			else if(op2 == 'E')
				b += (op1 == 'V')?x:-x;
			else
				c += (op1 == 'V')?x:-x;
		}
		else
		{
			scanf("%c %c %d%*c", &op2, &op3, &x);
			if(op2 == 'D')
				a += x;
			else if(op2 == 'E')
				b += x;
			else
				c += x;
			if(op3 == 'D')
				a -= x;
			else if(op3 == 'E')
				b -= x;
			else
				c -= x;
		}
	}
	printf("%d %d %d\n", a, b, c);
	return 0;
}

