// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Chocolate Factory
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1573

#include<stdio.h>
#include<math.h>

main()
{
	int x,y,z,r;
	while(scanf("%d %d %d",&x,&y,&z)==3&&(x||y||z))
	{
		r=(int)(floor(pow((double)(x*y*z),(1.0/3))));
		if((r+1)*(r+1)*(r+1)==x*y*z)
			r++;
		printf("%d\n",r);
	}
	return 0;
}

