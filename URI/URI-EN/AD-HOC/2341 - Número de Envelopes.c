// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Número de Envelopes
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2341

#include<stdio.h>

int vet[1001];

main()
{
	int i, N, M, x, me;
	scanf("%d %d", &N, &M);
	while(N--)
	{
		scanf("%d", &x);
		vet[x]++;
	}
	me = vet[1];
	for(i = 2; i <= M; i++)
		me = (me < vet[i]) ? me : vet[i];
	printf("%d\n", me);
	return 0;
}

