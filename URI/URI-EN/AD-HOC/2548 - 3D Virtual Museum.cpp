// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: 3D Virtual Museum
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2548

#include<bits/stdc++.h>

using namespace std;

int vet[1000];

main()
{
	int i, N, M, s;
	while(scanf("%d %d", &N, &M) != EOF)
	{
		s = 0;
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N, greater<int>());
		for(i = 0; i < M; i++)
			s += vet[i];
		printf("%d\n", s);
	}
	return 0;
}
