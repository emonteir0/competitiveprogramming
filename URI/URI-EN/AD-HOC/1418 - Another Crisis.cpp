// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Another Crisis
// Level: 7
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1418

#include<bits/stdc++.h>

using namespace std;

int N, P;

vector<int> G[100001];

int dfs(int x)
{
	int ans = 0;
	if(G[x].size() == 0)
		return 1;
	vector<int> best;
	for(int i = 0; i < G[x].size(); i++)
		best.push_back(dfs(G[x][i]));
	sort(best.begin(), best.end());
	for(int i = 0; i < best.size(); i++)
	{
		ans += best[i];
		if((i+1)*100 >= G[x].size()*P)
			return ans;
	}	
}

int main()
{
	int x;
	while(scanf("%d %d", &N, &P) != EOF && N)
	{
		for(int i = 0; i <= N; i++)
			G[i].clear();
		for(int i = 1; i <= N; i++)
		{
			scanf("%d", &x);
			G[x].push_back(i);
		}
		printf("%d\n", dfs(0));
	}
	return 0;
}
