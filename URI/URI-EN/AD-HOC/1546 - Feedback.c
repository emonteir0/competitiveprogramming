// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Feedback
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1546

#include<stdio.h>

main()
{
	int n,m,x;
	char vet[4][10]={"Rolien","Naej","Elehcim","Odranoel"};
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&m);
		while(m--)
		{
			scanf("%d",&x);
			printf("%s\n",vet[x-1]);
		}
	}
	return 0;
}
