// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Building Designing
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1361

#include<bits/stdc++.h>

using namespace std;

int vet[500000];

bool cmp(int a, int b)
{
	return abs(a) < abs(b);
}

char sig(int x)
{
	return x>0;
}

main()
{
	int K, N, i, cont, dir;
	scanf("%d", &K);
	while(K--)
	{
		scanf("%d", &N);
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N, cmp);
		cont = 1;
		dir = sig(vet[0]);
		for(i = 1; i < N; i++)
		{
			if(sig(vet[i]) != dir)
			{
				cont++;
				dir = !dir;
			}
		}
		printf("%d\n", cont);
	}
	return 0;
}

