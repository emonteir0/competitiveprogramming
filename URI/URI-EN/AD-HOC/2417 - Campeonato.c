// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Campeonato
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2417

#include<stdio.h>

main()
{
	int a, b, c, d, e, f, p1, p2;
	scanf("%d %d %d %d %d %d", &a, &b, &c, &d, &e, &f);
	p1 = 3*a + b;
	p2 = 3*d + e;
	if(p1 != p2)
		printf("%c\n", (p1 > p2) ? 'C' : 'F');
	else if(c != f)
		printf("%c\n", (c > f) ? 'C' : 'F');
	else
		printf("=\n");
	return 0;
}

