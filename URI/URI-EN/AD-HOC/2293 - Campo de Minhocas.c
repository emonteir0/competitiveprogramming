// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Campo de Minhocas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2293

#include<stdio.h>

int mat[100][100];

main()
{
	int N, M, i, j, ma = 0, acum;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			scanf("%d", &mat[i][j]);
	for(i = 0; i < N; i++)
	{
		acum = 0;
		for(j = 0; j < M; j++)
			acum += mat[i][j];
		ma = (ma>acum)? ma: acum;
	}
	for(j = 0; j < M; j++)
	{
		acum = 0;
		for(i = 0; i < N; i++)
			acum += mat[i][j];
		ma = (ma>acum)? ma: acum;
	}
	printf("%d\n", ma);
	return 0;
}

