// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hard to Believe, But True!
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1728

#include<bits/stdc++.h>

using namespace std;

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

main()
{
	int a,b,c,i,n;
	char vet[25];
	string str;
	while(gets(vet)!=NULL)
	{
		strrev(vet);
		n=strlen(vet);
		for(i=0;i<n;i++)
			if(!(vet[i]>='0'&&vet[i]<='9'))
				vet[i]=' ';
		str=vet;
		stringstream ss(str);
		ss >> str;
		c=atoi(str.c_str());
		ss >> str;
		b=atoi(str.c_str());
		ss >> str;
		a=atoi(str.c_str());
		printf("%s\n",(a+b==c)?"True":"False");
	}	
	return 0;
}
