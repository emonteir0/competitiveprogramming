// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Door Lock
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2449

#include<stdio.h>

int vet[1000];

int abs(int x)
{
	if (x < 0)
		return -x;
	return x;
}

main()
{
	int i, N, x, total = 0;
	scanf("%d %d", &N, &x);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(i = 0; i < N-1; i++)
	{
		if(vet[i] == x)
			continue;
		total += abs(vet[i]-x);
		vet[i+1] -= vet[i]-x;
	}
	printf("%d\n", total);
	return 0;
}
