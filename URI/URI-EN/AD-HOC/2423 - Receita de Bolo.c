// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Receita de Bolo
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2423

#include<stdio.h>

int min(int a, int b)
{
	return (a < b) ? a : b;
}

main()
{
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	printf("%d\n", min(a/2, min(b/3, c/5)));
	return 0;
}

