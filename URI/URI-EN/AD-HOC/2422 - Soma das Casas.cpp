// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Soma das Casas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2422

#include<bits/stdc++.h>

using namespace std;

int valores[100000];

main()
{
	int i, N, K;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d", &valores[i]);
	scanf("%d", &K);
	for(i = 0; i < N; i++)
		if(binary_search(valores, valores+N, K-valores[i]))
		{
			printf("%d %d\n", valores[i], K-valores[i]);
			return 0;
		}
	return 0;
}

