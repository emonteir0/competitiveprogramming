// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sinuca
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2466

#include<stdio.h>
#define ll unsigned long long

ll vet[64];

main()
{
	int N, i, x;
	ll A = 0, B;
	vet[0] = 1;
	for(i = 1; i < 64; i++)
		vet[i] = vet[i-1] << 1;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d", &x);
		A = (A << 1) + (x == 1); 
	}
	while((N--)>1)
	{
		B = 0;
		for(i = N-1; i >= 0; i--)
			B = (B << 1) + (((A & vet[i]) == vet[i]) ^ ((A & vet[i+1]) == vet[i+1]));
		A = B;
	}
	printf("%s\n", A? "branca" : "preta");
	return 0;
}

