// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Corredor
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2463

#include<stdio.h>

int vet[50000];

main()
{
	int i, N, maxateagora, maxaqui;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	maxateagora = 0;
	maxaqui = 0;
	for(i = 0; i < N; i++)
	{
		maxaqui = (maxaqui+vet[i] > 0) ? maxaqui+vet[i] : 0;
		maxateagora = (maxateagora > maxaqui) ? maxateagora : maxaqui;
	}
	printf("%d\n", maxateagora);
	return 0;
}

