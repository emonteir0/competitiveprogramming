// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Lights
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2021

#include<stdio.h>

main()
{
	int a,b,c,cont,x;
	while(scanf("%d %d %d",&a,&b,&c)==3 && (a||b||c) )
	{
		cont=0;
		while(c--)
		{
			scanf("%d",&x);
			cont+=(((b-x)%b)+b)%b+1;
		}
		printf("Lights: %d\n",cont);
	}
	return 0;
}

