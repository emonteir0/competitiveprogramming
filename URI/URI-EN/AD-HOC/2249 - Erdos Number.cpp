// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Erdos Number
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2249

#include<bits/stdc++.h>
#define INFINITO 9999999

using namespace std;

struct nome
{
	string surname;
	char inicial;
	bool operator<(const nome& rhs) const
    {
        if (surname != rhs.surname)
			return surname < rhs.surname;
		return inicial < rhs.inicial;
    }
    bool operator!=(const nome& rhs) const
    {
		return !((inicial == rhs.inicial) && (surname == rhs.surname));
    }
};

int K;
set< pair<int, int> > fila, fila2;
map<nome, int> Mapa, Mapa2;
int visitado[100];
int distancia[100];
set<nome> autores, autores2;
set<nome>::iterator it;
vector< vector<int> > G(100);
vector< nome > atual;

nome erdos;

void dijkstra(int xh)
{
	int a, x, i;
	pair<int, int> p;
	distancia[xh] = 0;
	fila.insert(make_pair(0,xh));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		x = p.second;
		if(!visitado[x])
		{
			visitado[x] = true;
			for(i = 0; i < G[x].size(); i++)
			{
				if(!visitado[G[x][i]] && (a + 1 < distancia[G[x][i]]))
					fila.insert(make_pair((distancia[G[x][i]] = a+1), G[x][i]));
			}
		}
	}
}

main()
{
	int i, N, T = 1;
	string str;
	char sobrenome[20], inicial, el;
	nome autor;
	erdos.inicial = 'P';
	erdos.surname = "Erdos";
	while(scanf("%d%*c", &N) == 1 && N)
	{
		printf("Teste %d\n", T++);
		Mapa = Mapa2;
		autores = autores2;
		K = 0;
		atual.clear();
		while(1)
		{
			scanf("%c. %s%c", &inicial, sobrenome, &el);
			autor.inicial = inicial;
			autor.surname = sobrenome;
			autor.surname = autor.surname.substr(0, autor.surname.length()-1);
			if(autores.find(autor) == autores.end())
			{
				autores.insert(autor);
				Mapa[autor] = K++;
			}
			atual.push_back(autor);
			for(i = 0; i < atual.size()-1; i++)
			{
				G[Mapa[autor]].push_back(Mapa[atual[i]]);
				G[Mapa[atual[i]]].push_back(Mapa[autor]); 
			}
			if(el == '\n')
			{
				N--;
				if(N == 0)
					break;
				atual.clear();
			}
		}
		for(i = 0; i < K; i++)
		{
			visitado[i] = 0;
			distancia[i] = INFINITO;
		}
		if(autores.find(erdos) != autores.end())
			dijkstra(Mapa[erdos]);
		for(it = autores.begin(); it != autores.end(); ++it)
		{
			if(*it != erdos)
			{
				printf("%c. %s: ", it->inicial, it->surname.c_str());
				if(distancia[Mapa[*it]] == INFINITO)
					printf("infinito\n");
				else
					printf("%d\n", distancia[Mapa[*it]]);
			}
		}
		for(i = 0; i < K; i++)
			G[i].clear();
		printf("\n");
	}
	return 0;
}

