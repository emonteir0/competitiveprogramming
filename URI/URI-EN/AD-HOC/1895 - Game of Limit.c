// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of Limit
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1895

#include<stdio.h>

int abs(int n)
{
	if(n<0)
		return -n;
	return n;
}

main()
{
	int N,i,T,L,x,cont1=0,cont2=0;
	scanf("%d %d %d",&N,&T,&L);
	for(i=1;i<N;i++)
	{
		scanf("%d",&x);
		if(abs(x-T)<=L)
		{
			if((i%2)==1)
				cont1+=abs(x-T);
			else
				cont2+=abs(x-T);
			T=x;
		}
	}
	printf("%d %d\n",cont1,cont2);
	return 0;
}
