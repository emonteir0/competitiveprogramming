// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Robotics Competition
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1881

#include<stdio.h>
#include<math.h>

double L[2][2], C[2][2];
 
void multiply(double F[2][2], double M[2][2])
{
  double x =  (F[0][0]*M[0][0] + F[0][1]*M[1][0]);
  double y =  (F[0][0]*M[0][1] + F[0][1]*M[1][1]);
  double z =  (F[1][0]*M[0][0] + F[1][1]*M[1][0]);
  double w =  (F[1][0]*M[0][1] + F[1][1]*M[1][1]);
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
}

void power(int n)
{
  if (n == 1 || n==0)
      return;
  power(n/2);
  multiply(L, L);
  if (n%2 != 0)
     multiply(L, C);
}

int mdc(int a, int b)
{
	int res;
	while(b!=0)
	{
		res=a%b;
		a=b;
		b=res;
	}
	return a;
}

main()
{
	int N,x,a;
	double l,c,s,d,PI=acos(-1.0),v1,v2;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %lf %d",&a,&l,&x);
		if(l==0)
			printf("0.00 0.00\n");
		else
		{
			if(a>=360)
				a-=360;
			if(a==0)
				printf("%.2lf 0.00\n",l*x);
			else
			{
				c=cos(a*(PI/180.0));
				s=sin(a*(PI/180.0));
				L[0][0]=c;
				L[0][1]=-s;
				L[1][0]=s;
				L[1][1]=c;
				C[0][0]=c;
				C[0][1]=-s;
				C[1][0]=s;
				C[1][1]=c;
				power(x%360);
				d=(c-1.0)*(c-1.0)+s*s;
				C[0][0]=(c-1.0)/d;
				C[0][1]=s/d;
				C[1][0]=-s/d;
				C[1][1]=(c-1.0)/d;
				L[0][0]-=1.0;
				L[1][1]-=1.0;
				multiply(C,L);
				v1=C[0][0]*l+1e-9;
				v2=C[1][0]*l+1e-9;
				printf("%.2lf %.2lf\n",v1,v2);
			}
		}
	}
	return 0;
}

