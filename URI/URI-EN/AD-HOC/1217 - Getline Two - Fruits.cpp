// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Getline Two - Fruits
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1217

#include<bits/stdc++.h>

using namespace std;

main()
{
	int cont=0,cont2,i,N;
	float x,acum=0,tot;
	string str;
	char vet[10001];
	scanf("%d%*c",&N);
	for(i=1;i<=N;i++)
	{
		cont2=0;
		scanf("%f%*c",&x);
		acum+=x;
		gets(vet);
		str=vet;
		stringstream ss(str);
		while(ss >> str)
			cont2++;
		cont+=cont2;
		printf("day %d: %d kg\n",i,cont2);
	}
	tot=((float)(cont))/N;
	acum/=N;
	printf("%.2f kg by day\nR$ %.2f by day\n",tot,acum);
	return 0;
}
