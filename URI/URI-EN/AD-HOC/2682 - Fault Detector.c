// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fault Detector
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2682

#include<stdio.h>

main()
{
int x, xant, ok=0;
scanf("%d", &xant);
while(scanf("%d", &x)==1)
{
if(x<=xant && !ok)
{
ok=1;
printf("%d\n",xant+1);
}
xant=x;
}
if(!ok) printf("%d\n", xant+1);
return 0;
}
