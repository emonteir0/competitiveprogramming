// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Paula's Mathematic Game
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1192

#include<stdio.h>


main()
{
	int n,a,b;
	char op;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d%c%d",&a,&op,&b);
		if(a!=b)
		{
			if(op>='A'&&op<='Z')
				printf("%d\n",b-a);
			if(op>='a'&&op<='z')
				printf("%d\n",b+a);
		}
		else
			printf("%d\n",a*a);
	}
	return 0;
}
