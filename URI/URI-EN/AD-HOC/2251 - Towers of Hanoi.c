// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Towers of Hanoi
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2251

#include<stdio.h>

main()
{
	int N, k = 1;
	while(scanf("%d", &N) == 1 && N)
		printf("Teste %d\n%d\n\n", k++, (1 << N) - 1);
	return 0;
}
