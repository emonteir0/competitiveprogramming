// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Who Will Fail?
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2134

#include<cstdio>
#include<cstring>
#include<algorithm>

typedef struct
{
	int x;
	char nome[21];
} aluno;

using namespace std;

aluno vet[100];

bool cmp(aluno a, aluno b)
{
	if(a.x != b.x)
		return a.x<b.x;
	return strcmp(a.nome, b.nome) > 0;
}

main()
{
	int i, n, k=1;
	while(scanf("%d%*c",&n)==1)
	{
		for(i=0;i<n;i++)
			scanf("%s %d",vet[i].nome,&vet[i].x);
		sort(vet, vet+n, cmp);
		printf("Instancia %d\n%s\n\n",k++,vet[0].nome);
	}
	return 0;
}

