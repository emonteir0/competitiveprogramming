// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Meeting
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2372

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int G[100][100];

main()
{
	int N, M, u, v, w;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			G[i][j] = (i == j) ? 0 : INF;
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u][v] = G[v][u] = min(G[u][v], w);
	}
	for(int k = 0; k < N; k++)
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				G[i][j] = min(G[i][j], G[i][k]+G[k][j]);
	int ans = INF;
	for(int i = 0; i < N; i++)
	{
		int ma = 0;
		for(int j = 0; j < N; j++)
			ma = max(ma, G[i][j]);
		ans = min(ans, ma);
	}
	printf("%d\n", ans);
	return 0;
}
