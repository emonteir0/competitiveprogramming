// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Renzo and the Palindromic...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1880

#include<stdio.h>

int vet[32];

main()
{
	int i,j,N,x,a,tam;
	char b,cont;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&x);
		if(x==0)
			printf("2 3 4 5 6 7 8 9 10 11 12 13 14 15 16\n");
		else
		{
			cont=0;
			for(j=2;j<=16;j++)
			{
				a=x;
				tam=0;
				while(a!=0)
				{
					vet[tam++]=a%j;
					a/=j;
				}
				b=1;
				for(i=0;i<tam;i++)
					b&=(vet[i]==vet[tam-1-i]);
				if(b==1)
				{
					printf("%s%d",cont==1?" ":"",j);
					cont=1;
				}
			}
			if(cont==0)
				printf("-1");
			printf("\n");
		}
	}
	return 0;
}

