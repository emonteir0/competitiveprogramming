// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: To Carry or not to Carry
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1026

#include<stdio.h>

main()
{
	long long int a,b,c,x;
	char cont;
	int i;
	while(scanf("%lld %lld",&a,&b)==2)
	{
		if(a>b)
		{
			b+=a;
			a=b-a;
			b-=a;
		}
		cont=-1;
		c=b;
		while(c)
		{
			c=c>>1;
			cont++;
		}
		x=1;
		for(i=0;i<=cont;i++)
		{
			c+=(((a&x)==x)^((b&x)==x))*x;
			x=x<<1;
		}
		printf("%lld\n",c);
	}
	return 0;
}
