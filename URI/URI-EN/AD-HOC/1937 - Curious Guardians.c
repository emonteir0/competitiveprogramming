// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Curious Guardians
// Level: 6
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1937

#include <stdio.h>
#define MOD 1000000007
#define MAX 101
#define ll long long int

ll comb[MAX][MAX];

int K;
ll dp[MAX][MAX];

ll go(int n, int k) {
	int i;
	ll ans = 0, t;
	if (n <= 1) 		return 1;
	if (k == 1)			return (n * go(n - 1, K - 1)) % MOD;
	if (k == 0) 		return 0;
	if (dp[n][k] != -1)	return dp[n][k];

	for (i = 1; i <= n; ++i) 
	{
		t = (comb[n - 1][i - 1] * i) % MOD;
		t = (t  * go(n - i, k - 1)) % MOD;
		t = (t  * go(i - 1, K - 1)) % MOD;
		ans = (ans + t) % MOD;
	}

	return dp[n][k] = ans;
}

int main() {
	int n, i, j;
	for (i = 0; i < MAX; ++i)
	{
		comb[i][0] = comb[i][i] = 1;
		for(j = 0; j < MAX; ++j)
			dp[i][j] = -1;
	}
	for (i = 2; i < MAX; ++i)
		for (j = 1; j < MAX; ++j)
			comb[i][j] = (comb[i - 1][j] + comb[i - 1][j - 1]) % MOD;

	scanf("%d %d", &n, &K);
	printf("%lld\n", go(n - 1, K));
	return 0;
}

