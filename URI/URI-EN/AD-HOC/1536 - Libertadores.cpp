// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Libertadores
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1536

#include<bits/stdc++.h>

using namespace std;

map<string,int> mapa;

main()
{
	int N,a,b,c,d;
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%d%*c%*c%*c%d%*c%d%*c%*c%*c%d%*c",&a,&b,&c,&d);
		if(a+d>b+c)
			printf("Time 1\n");
		else if(a+d<b+c)
			printf("Time 2\n");
		else
		{
			if(d>b)
				printf("Time 1\n");
			else if(d<b)
				printf("Time 2\n");
			else
				printf("Penaltis\n");
		}
	}
	return 0;
}
