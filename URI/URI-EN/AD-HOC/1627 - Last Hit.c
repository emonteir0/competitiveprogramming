// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Last Hit
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1627

#include<stdio.h>

main()
{
	int t,n,a,b,c,d,e;
	scanf("%d",&n);
	while(n--)
	{
		t=0;
		scanf("%d %d %d %d %d",&a,&b,&c,&d,&e);
		while(1)
		{
			if(t%b==0)
			{
				e-=a;
				if(e<=0)
				{
					printf("Andre\n");
					break;
				}
			}
			if(t%d==0)
			{
				e-=c;
				if(e<=0)
				{
					printf("Beto\n");
					break;
				}
			}
			t++;
		}
	}
	return 0;
}

