// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Escala Musical
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2662

#include<bits/stdc++.h>

using namespace std;

int dx[] = {2, 2, 1, 2, 2, 2, 1};
string notas[12] = {"do", "do#", "re", "re#", "mi", "fa", "fa#", "sol", "sol#", "la", "la#", "si"};
int tom[12];

int main()
{
	int i, j, k, N, x;
	int mask = 0;
	for(i = 0; i < 12; i++)
	{
		k = i;
		for(j = 0; j < 7; j++)
		{
			tom[i] += 1 << k;
			k = (k + dx[j])%12;
		}
	}
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		mask |= (1 << ((x-1)%12));
	}
	for(i = 0; i < 12; i++)
		if((mask|tom[i]) == tom[i])
			break;
	printf("%s\n", (i == 12) ? "desafinado" : notas[i].c_str());
	return 0;
}
