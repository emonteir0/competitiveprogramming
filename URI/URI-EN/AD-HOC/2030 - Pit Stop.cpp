// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pit Stop
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2030

#include<bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;

ii mat[101][101], vazio = make_pair(-1, -1), nulo = make_pair(0, 0), p;


int A,B,C,D,E;

ii operator+(const ii a, const ii b)
{
	return ii(a.first+b.first, a.second+b.second);
}


ii solve(int i, int j)
{
	if(j == E)
		return nulo;
	if(mat[i][j] != vazio)
		return mat[i][j];
	if(i < D)
	{
		//printf("%d %d\n", i, j);
		return mat[i][j] = solve(i+1, j+1) + make_pair(A, 0);
	}
	else
		return mat[i][j] = min(solve(i, j+1) + make_pair(B, 0), solve(0, j) + make_pair(C, 1));
}


main()
{
	int i, j, k, N;
	scanf("%d",&N);
	for(i=1;i<=N;i++)
	{
		scanf("%d %d %d %d %d",&A,&B,&C,&D,&E);
		for(j = 0; j <= E; j++)
			for(k = 0; k <= E; k++)
				mat[j][k] = vazio;
		p = solve(0, 0);
		printf("Teste #%d\n%d %d\n\n",i,p.first,p.second);
	}
	return 0;
}

