// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Snakes and Ladders
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1569

#include<bits/stdc++.h>

using namespace std;

int pos[1000001];
int mapa[101];

main()
{
	int i, ind, T, a, b, x, N, M, Q;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d %d", &N, &M, &Q);
		for(i = 0; i < N; i++)
			pos[i] = 1;
		for(i = 1; i <= 100; i++)
			mapa[i] = i;
		while(M--)
		{
			scanf("%d %d", &a, &b);
			mapa[a] = b;
		}
		for(i = 0; i < Q; i++)
		{
			scanf("%d", &x);
			ind = i%N;
			pos[ind] += x;
			if(pos[ind] >= 100)
			{
				pos[ind] = 100;
				i++;
				break;
			}
			pos[ind] = mapa[pos[ind]];
			if(pos[ind] >= 100)
			{
				pos[ind] = 100;
				i++;
				break;
			}
		}
		for(; i < Q; i++)
			scanf("%*d");
		for(int i = 0; i < N; i++)
			printf("Position of player %d is %d.\n", i+1, pos[i]);
	}
	return 0;
}
