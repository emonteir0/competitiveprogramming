// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Peça Perdida
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2322

#include<bits/stdc++.h>

using namespace std;

int vet[1000];

main()
{
	int N, i;
	scanf("%d", &N);
	for(i = 0; i < N-1; i++)
		scanf("%d", &vet[i]);
	sort(vet, vet+N-1);
	for(i = 0; i < N-1; i++)
	{
		if(i+1 != vet[i])
			break;
	}
	printf("%d\n", i+1);
	return 0;
}

