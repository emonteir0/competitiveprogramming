// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Capybara Lagoon
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2574

#include<stdio.h>

int mat[2048][2048], mat2[2049][2049];

main()
{
	int i, j, N, G, tam;
	char bol;
	scanf("%d %d", &N, &G);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			scanf("%d", &mat[i][j]);
			mat2[i+1][j+1] = mat[i][j] + mat2[i][j+1] + mat2[i+1][j] - mat2[i][j];
		}
	tam = 1;
	while(1)
	{
		bol = 1;
		for(i = tam; i <= N; i += tam)
			for(j = tam; j <= N; j += tam)
				bol &= mat2[i][j] - mat2[i-tam][j]  - mat2[i][j-tam] + mat2[i-tam][j-tam] >= G;
		if(bol)
			break;
		tam <<= 1;
	}
	printf("%d\n", (N/tam) * (N/tam));
	return 0;
}
