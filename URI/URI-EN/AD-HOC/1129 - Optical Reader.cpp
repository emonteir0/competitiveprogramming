// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Optical Reader
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1129

#include<cstdio>
#include<algorithm>

using namespace std;

typedef struct tipo
{
	char ind;
	int n;
}a;

a vet[5];

bool cmp (a x, a y)
{
	return y.n>x.n;
}

main()
{
	int n;
	while(scanf("%d",&n)==1&&n>0)
	{
		while(n--)
		{
			vet[0].ind=0;
			vet[1].ind=1;
			vet[2].ind=2;
			vet[3].ind=3;
			vet[4].ind=4;
			scanf("%d %d %d %d %d",&vet[0].n,&vet[1].n,&vet[2].n,&vet[3].n,&vet[4].n);
			sort(vet,vet+5,cmp);
			if(vet[0].n<=127)
			{
				if(vet[1].n<=127)
					printf("*\n");
				else
					printf("%c\n",vet[0].ind+'A');
			}
			else
			{
				printf("*\n");
			}
		}
	}
	return 0;
}
