// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Matrix of Squares
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1578

#include<stdio.h>
  
unsigned long long int mat[20][20],ma[20];
  
int dig(unsigned long long int num)
{
	int i=0;
	if(num==0)
		return 1;
    while(1<=num)
    {
        i++;
        num/=10;
    }
    return i;
}
  
  
main()
{
    int i,j,n,k,n2,digitos[20];
    scanf("%d",&n2);
    for(k=1;k<=n2;k++)
    {
    	if(k>1)
    		printf("\n");
        scanf("%d",&n);
        for(i=0;i<n;i++)
            ma[i]=0;
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                scanf("%llu",&mat[i][j]);
                if(mat[i][j]>ma[j])
                    ma[j]=mat[i][j];
            }
        }
        for(i=0;i<n;i++)
            digitos[i]=dig(ma[i]*ma[i]);
        printf("Quadrado da matriz #%d:\n",k+3);
        for(i=0;i<n;i++)
        {
            j=0;
            printf("%*llu",digitos[j],mat[i][j]*mat[i][j]);
            for(j=1;j<n;j++)
            {
                printf(" %*llu",digitos[j],mat[i][j]*mat[i][j]);
            }
            printf("\n");
        }
    }
    return 0;
}
