// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Contamination
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1583

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int i,j;
}tipo;

queue<tipo> fila;
tipo par;

main()
{
	char mat[50][50],c;
	int N,M,i,j;
	while(scanf("%d %d%*c",&N,&M)==2&&(N||M))
	{
		for(i=0;i<N;i++)
		{
			for(j=0;j<M;j++)
			{
				scanf("%c",&mat[i][j]);
				if(mat[i][j]=='T')
				{
					par.i=i;
					par.j=j;
					fila.push(par);
				}
			}
			scanf("%*c");
		}
		while(!fila.empty())
		{
			par=fila.front();
			fila.pop();
			i=par.i;
			j=par.j;
			if((j+1)<M)
			{
				if(mat[i][j+1]=='A')
				{
					mat[i][j+1]='T';
					par.i=i;
					par.j=j+1;
					fila.push(par);
				}
			}
			if((j-1)>=0)
			{
				if(mat[i][j-1]=='A')
				{
					mat[i][j-1]='T';
					par.i=i;
					par.j=j-1;
					fila.push(par);
				}
			}
			if((i+1)<N)
			{
				if(mat[i+1][j]=='A')
				{
					mat[i+1][j]='T';
					par.i=i+1;
					par.j=j;
					fila.push(par);
				}
			}
			if((i-1)>=0)
			{
				if(mat[i-1][j]=='A')
				{
					mat[i-1][j]='T';
					par.i=i-1;
					par.j=j;
					fila.push(par);
				}
			}
		}
		for(i=0;i<N;i++)
		{
			for(j=0;j<M;j++)
				printf("%c",mat[i][j]);
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}
