// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Green Peace! World Hypocr...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2078

#include<bits/stdc++.h>

#define MAXN 200010

using namespace std;

vector< int > bit[MAXN];
int BITsize = MAXN;

void insert(int x, int y)
{
	x++;
	for(int i = x; i < BITsize; i += i&-i)
		bit[i].push_back(y);
}


int query(int x, int y1, int y2)
{
	int ans = 0, a, b;
	x++;
	for(int i = x; i; i -= i & -i)
		ans += upper_bound(bit[i].begin(), bit[i].end(), y2)-lower_bound(bit[i].begin(), bit[i].end(), y1);
	return ans;
}

int query(int x1, int y1, int x2, int y2)
{
	return query(x2, y1, y2) - query(x1-1, y1, y2)-1;
}

void clear()
{
	for(int i = 0; i < BITsize; i++)
		bit[i].clear();
}

int X[MAXN], Y[MAXN];

main()
{
	int T, N, D;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &D);
		for(int i = 0; i < N; i++)
		{
			scanf("%d %d", &X[i], &Y[i]);
			X[i] += Y[i];
			Y[i] = X[i]-Y[i]-Y[i];
			insert(X[i], Y[i]);
		}
		for(int i = 0; i < BITsize; i++)
			sort(bit[i].begin(), bit[i].end());
		for(int i = 0; i < N; i++)
		{
			printf("%d ", query(max(0, X[i]-D), Y[i]-D, min(MAXN, X[i]+D), Y[i]+D));
		}
		printf("\n");
	}
	return 0;
}
