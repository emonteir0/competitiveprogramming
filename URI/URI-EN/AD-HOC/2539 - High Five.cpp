// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: High Five
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2539

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int M;
int mapa[200005];
ll tree[200005];

void update(int i, int val)
{
	while(i <= M)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

ll sum(int i)
{
	ll s = 0;
	while(i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

int main()
{
	int x;
	ll ans;
	while(scanf("%d", &M) == 1)
	{
		ans = 0;
		for(int i = 0; i <= M; i++)
			tree[i] = 0;
		for(int i = 1; i <= M; i++)
		{
			scanf("%d", &x);
			mapa[x] = i;
			update(x, 1);
		}
		for(int i = 1; i <= M; i++)
		{
			ans += M-i+1-sum(mapa[i]);
			update(mapa[i], -1);
		}
		printf("%lld\n", ans);
	}
	return 0;
}

