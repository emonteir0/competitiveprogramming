// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Xoringan
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2513

#include<stdio.h>
#define ll unsigned long long
#define MOD 1000000007


ll vet[1001];
ll memo[1001];
ll valores[64];
ll modval[64];

main()
{
	int i, j, N, c, p;
	ll sum = 0;
	i = 0;
	valores[0] = 1;
	for(i = 1; i < 64; i++)
		valores[i] = valores[i-1] << 1;
	for(i = 0; i < 64; i++)
		modval[i] = valores[i]%MOD;
	while(scanf("%d", &N) == 1)
	{	
		for(i = 0; i < N; i++)
			scanf("%llu", &vet[i]);
		sum = 0;
		for(i = 0; i < 64; i++)
		{
			c = 0;
			p = 0;
			if(valores[i] & vet[0])
			{
				memo[0] = modval[i];
				c++;
			}
			else
			{
				memo[0] = 0;
				p++;
			}
			for(j = 1; j < N; j++)
			{
				memo[j] = memo[j-1];
				if(valores[i] & vet[j])
				{
					memo[j] += modval[i] * p + modval[i];
					p += c;
					c = p-c;
					p -= c;
					c++;
				}
				else
				{
					memo[j] += modval[i] * c;
					p++;
				}
			}
			sum = (sum + memo[N-1]) % MOD;
		}
		printf("%llu\n", sum);
	}
	return 0;
}
