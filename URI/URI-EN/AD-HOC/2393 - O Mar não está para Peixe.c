// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: O Mar não está para Peixe
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2393

#include<stdio.h>

int mat[100][100];

main()
{
	int i, j, x1, y1, x2, y2, N, cont = 0;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d %d", &x1, &x2, &y1, &y2);
		for(i = x1; i < x2; i++)
			for(j = y1; j < y2; j++)
				mat[i][j] = 1;
	}
	for(i = 1; i < 100; i++)
		for(j = 1; j < 100; j++)
			cont += mat[i][j];
	printf("%d\n", cont);
	return 0;
}

