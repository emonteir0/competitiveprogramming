// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grapevine
// Level: 8
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1460

#include<bits/stdc++.h>

using namespace std;

int mat[501][501];

int N, M, L, U;

int bb(int x, int y)
{
	int lo = 1, hi = min(N-x, M-y), mid, best = 0;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		if(mat[x+mid-1][y+mid-1] <= U)
		{
			best = mid;
			lo = mid+1;
		}
		else
			hi = mid-1;
	}
	return best;
}

int solve()
{
	int best = 0, it;
	for(int i = 0; i < N; i++)
	{
		it = lower_bound(mat[i], mat[i]+M, L)-mat[i];
		best = max(best, bb(i, it));
	}
	return best;
}


main()
{
	int Q;
	while(scanf("%d %d", &N, &M) != EOF && N)
	{
		for(int i = 0; i < N; i++)
			for(int j = 0; j < M; j++)
				scanf("%d", &mat[i][j]);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d %d", &L, &U);
			printf("%d\n", solve());
		}
		printf("-\n");
	}
	return 0;
}
