// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factorial
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1936

#include<stdio.h>

main()
{
	int n,vet[8]={1,2,6,24,120,720,5040,40320},i,cont=0;
	scanf("%d",&n);
	for(i=7;i>=0;i--)
	{
		while(n>=vet[i])
		{
			n-=vet[i];
			cont++;
		}
	}
	printf("%d\n",cont);
	return 0;
}
