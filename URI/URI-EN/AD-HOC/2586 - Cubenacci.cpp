// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cubenacci
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2586

#include<bits/stdc++.h>
#define ll long long
using namespace std;

ll M[2][3][3][3];

int dx[27] = { 1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1,  1,  0, -1};
int dy[27] = { 1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1, -1, -1, -1, -1, -1};
int dz[27] = { 1,  1,  1,  0,  0,  0, -1, -1, -1,  1,  1,  1,  0,  0,  0, -1, -1, -1,  1,  1,  1,  0,  0,  0, -1, -1, -1};

int inRange(int x, int y, int z)
{
	return x >= 0 && x < 3 && y >= 0 && y < 3 && z >= 0 && z < 3;
}

main()
{
	int T, N, ind;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				for(int k = 0; k < 3; k++)
					scanf("%lld", &M[0][i][j][k]);
		for(int n = 1; n <= N; n++)
		{
			ind = n&1;
			for(int i = 0; i < 3; i++)
				for(int j = 0; j < 3; j++)
					for(int k = 0; k < 3; k++)
					{
						M[ind][i][j][k] = 0;
						for(int l = 0; l < 27; l++)
						{
							if(inRange(i+dx[l], j+dy[l], k+dz[l]))
								M[ind][i][j][k] += M[!ind][i+dx[l]][j+dy[l]][k+dz[l]];
						}
					}
		}
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				printf("%lld", M[ind][i][j][0]);
				for(int k = 1; k < 3; k++)
					printf(" %lld", M[ind][i][j][k]);
				printf("\n");
			}
		}
	}
	return 0;
}
