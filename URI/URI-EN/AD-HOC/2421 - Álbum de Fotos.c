// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Álbum de Fotos
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2421

#include<stdio.h>

int x, y;

int max(int a, int b)
{
	return (a > b) ? a : b;
}

int cabe(int a, int b, int c, int d)
{
	int e, f;
	e = a+c;
	f = max(b, d);
	return ((e <= x) && (f <= y)) || ((e <= y) && (f <=x));
}

main()
{
	int a, b, c, d;
	scanf("%d %d %d %d %d %d", &x, &y, &a, &b, &c, &d);
	printf("%s\n", (cabe(a, b, c, d) || cabe(b, a, c, d) || cabe(a, b, d, c) || cabe(b, a, d, c))? "S": "N");
	return 0;
}

