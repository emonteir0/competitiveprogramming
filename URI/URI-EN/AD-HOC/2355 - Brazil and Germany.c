// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brazil and Germany
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2355

#include<stdio.h>

main()
{
	int N;
	while(scanf("%d", &N) == 1 && N)
	{
		if(N%90 == 0)
			printf("Brasil %d x Alemanha %lld\n", N/90, 7LL*N/90);
		else
			printf("Brasil %d x Alemanha %lld\n", N/90, 7LL*N/90 + 1);
	}
	return 0;
}

