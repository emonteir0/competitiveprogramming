// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Frequent Asked Questions
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1553

#include<stdio.h>
#include<string.h>

int vet[1001];

int main()
{
	int n,m,x,cont;
	while((scanf("%d %d",&n,&m)==2)&&(n||m))
	{
		cont=0;
		while(n--)
		{
			scanf("%d",&x);
			vet[x]++;
			if(vet[x]==m)
				cont++;
		}
		printf("%d\n",cont);
		memset(vet,0,1001);
	}
	return 0;
}

