// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Club Ballroom
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1086

#include<bits/stdc++.h>
#define INFINITO 1000000

using namespace std;

map<int,int> Mapa, Mapa2, Mapa3;
map<int,int>::iterator it;

int achar(int n, int m, int w)
{
	int tam, cont = 0, a, b, u;
	Mapa = Mapa2;
	if((100*n)%w != 0)
		return INFINITO;
	tam = (100*n)/w;
	if(tam>Mapa[m])
	{
		tam -= Mapa[m];
		cont += Mapa[m];
		Mapa[m] = 0;
	}
	else return tam;
	for(it = Mapa.begin(); it != Mapa.end(); ++it)
	{
		a = it->first;
		b = m - a;
		if(a!=b)
		{
			if(Mapa[a]>0 && Mapa[b]>0)
			{
				u = min(Mapa[a],Mapa[b]);
				if(u<tam)
				{
					Mapa[a] -= u;
					Mapa[b] -= u;
					cont += 2*u;
					tam -= u;
				}
				else	return cont + 2*tam;
			}
		}
		else
		{
			u = Mapa[a]/2;
			if(u<tam)
			{
				Mapa[a]-=2*u;
				cont+=2*u;
				tam-=u;
			}
			else	return cont + 2*tam;
		}
	}
	return INFINITO;
}


main()
{
	int k,x,w,n,m,u;
	while(scanf("%d %d",&n,&m)==2 && (n||m))
	{
		Mapa2 = Mapa3;
		scanf("%d",&w);
		scanf("%d",&k);
		u = max(n,m);
		while(k--)
		{
			scanf("%d",&x);
			if(x<=u)
				Mapa2[x]++;
		}
		x = min(achar(n,m,w), achar(m,n,w));
		if(x == INFINITO)	printf("impossivel\n");
		else				printf("%d\n", x);
	}
	return 0;
}
