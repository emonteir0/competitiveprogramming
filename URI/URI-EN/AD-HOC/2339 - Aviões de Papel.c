// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Aviões de Papel
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2339

#include<stdio.h>

main()
{
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	printf("%s\n", a*c <= b ? "S" : "N");
	return 0;
}

