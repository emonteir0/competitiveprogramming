// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dentista
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2387

#include<stdio.h>

int vet[1000001];

typedef struct
{
	int start, end;
} horario;

horario horarios[100001];
int hmax, N;

int max(int a, int b)
{
	return (a > b) ? a : b;
}

int solve(int tempo, int ind)
{
	if(tempo > hmax || ind >= N)
		return 0;
	if(vet[tempo] != -1)
		return vet[tempo];
	if(tempo < horarios[ind].start)
		return solve(horarios[ind].start, ind);
	if(tempo > horarios[ind].start)
		return solve(tempo, ind+1);
	return vet[tempo] = max(solve(horarios[ind].end, ind) + 1, solve(horarios[ind+1].start, ind+1));
}

main()
{
	int i;
	for(i = 0; i <= 1000000; i++)
		vet[i] = -1;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d %d", &horarios[i].start, &horarios[i].end);
	hmax = horarios[N-1].start;
	printf("%d\n", solve(horarios[0].start,0));
	return 0;
}

