// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tapetes
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2472

#include<stdio.h>
#define ll long long

main()
{
	int n, m;
	ll x;
	scanf("%d %d", &n, &m);
	x = n-m+1;
	printf("%lld\n", x*x+m-1);
	return 0;
}

