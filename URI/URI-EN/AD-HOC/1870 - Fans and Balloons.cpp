// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fans and Balloons
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1870

#include<stdio.h>

void le(int a, int b, int c)
{
	int i,j;
	for(i=a;i<=b;i++)
		for(j=1;j<=c;j++)
			scanf("%*d");
}

main()
{
	int L,C,P,i,j,x,esq,meio,dir;
	bool a;
	while(scanf("%d %d %d",&L,&C,&P)==3&&(L||C||P))
	{
		a=true;
		for(i=1;i<=L;i++)
		{
			esq=0;
			dir=0;
			if(!a)
			{
				le(i,L,C);
				break;
			}
			for(j=1;j<P;j++)
			{
				scanf("%d",&x);
				if(x>0)
					esq=x;
			}
			scanf("%d",&meio);
			if(meio!=0)
			{
				printf("BOOM %d %d",i,j);
				a=false;
			}
			for(j++;j<=C;j++)
			{
				if(dir==0)
					scanf("%d",&x);
				else
					scanf("%*d");
				if(x>0)
					dir=x;
			}
			P-=(dir-esq);
			if((P<=1 || P>=C) && a)
			{
				printf("BOOM %d %d\n",i,P);
				a=false;
			}
		}
		if(a)
			printf("OUT %d\n",P);
	}
	return 0;
}

