// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hit the Gift
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2478

#include<bits/stdc++.h>

using namespace std;

map<string, set<string> > Mapa;


main()
{
	int N;
	char pres1[51], pres2[51], pres3[51], pessoa[51];
	string str, str2;
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%s %s %s %s%*c", pessoa, pres1, pres2, pres3);
		str = pessoa;
		Mapa[str].insert(str2 = pres1);
		Mapa[str].insert(str2 = pres2);
		Mapa[str].insert(str2 = pres3);
	}
	while(scanf("%s %s%*c", pessoa, pres1) == 2)
	{
		str = pessoa;
		str2 = pres1;
		printf("%s\n", (Mapa[str].find(str2) != Mapa[str].end()) ? "Uhul! Seu amigo secreto vai adorar o/" : "Tente Novamente!");
	} 
	return 0;
}

