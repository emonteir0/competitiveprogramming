// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Expressões
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2406

#include<bits/stdc++.h>

using namespace std;

stack<char> pilha, pilha2;

main()
{
	int i, N, tam;
	char mensagem[100001], c, d;
	scanf("%d%*c", &N);
	while(N--)
	{
		pilha = pilha2;
		scanf("%s", mensagem);
		tam = strlen(mensagem);
		for(i = 0; i < tam; i++)
		{
			c = mensagem[i];
			if(c == '(' || c == '[' || c == '{')
				pilha.push(c);
			else if(c == ')')
			{
				if(pilha.empty())
					break;
				else
				{
					d = pilha.top();
					pilha.pop();
					if(d != '(')
						break;
				}
			}
			else if(c == ']')
			{
				if(pilha.empty())
					break;
				else
				{
					d = pilha.top();
					pilha.pop();
					if(d != '[')
						break;
				}
			}
			else
			{
				if(pilha.empty())
					break;
				else
				{
					d = pilha.top();
					pilha.pop();
					if(d != '{')
						break;
				}
			}
		}
		printf("%s\n", ((i == tam) && pilha.empty())? "S" : "N");
	}
	return 0;
}
