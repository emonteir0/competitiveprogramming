// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grandma Vitória's Piggy B...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2247

#include<stdio.h>

main()
{
	int k = 1, N, x, y, ax, ay;
	while(scanf("%d", &N) == 1 && N)
	{
		printf("Teste %d\n", k++);
		ax = ay = 0;
		while(N--)
		{
			scanf("%d %d", &x, &y);
			ax+=x;
			ay+=y;
			printf("%d\n", ax-ay);
		}
		printf("\n");
	}
	return 0;
}
