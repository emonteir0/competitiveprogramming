// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flight
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2462

#include<stdio.h>

main()
{
	int h1, m1, h2, m2, h3, m3, h4, m4, t1, t2, v1, v2;
	scanf("%d:%d %d:%d %d:%d %d:%d", &h1, &m1, &h2, &m2, &h3, &m3, &h4, &m4);
	t1 = 60*(h2-h1)+m2-m1;
	t2 = 60*(h4-h3)+m4-m3;
	v1 = (((t1+t2)/2) % 720 + 720) % 720;
	v2 = (((t1-v1) % 1440 + 1440) % 1440) / 60;
	printf("%d %d\n", v1, (v2 > 12) ? v2 - 24 : v2);
	return 0;
}

