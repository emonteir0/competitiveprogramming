// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Banco
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2425

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;


int vet[10];

main()
{
	int N, M, x, y, me, ime, cont = 0;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d", &x, &y);
		me = INF;
		for(int i = 0; i < N; i++)
		{
			if(vet[i] < me)
			{
				me = vet[i];
				ime = i;
			}
		}
		if(me-x > 20)
			cont++;
		vet[ime] = max(x, me)+y;
	}
	printf("%d\n", cont);
	return 0;
}
