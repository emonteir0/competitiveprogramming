// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tiles
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2246

#include<stdio.h>

typedef struct
{
	int x, y;
} posicao;

posicao fila[400001];
int N, M;
int mat[201][201];
char cor[201][201];
int dx[]={1,-1,0,0},dy[]={0,0,1,-1};

void printcor()
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
			printf("%d ", cor[i][j]);
		printf("\n");
	}
}

int min(int a, int b)
{
	return (a<b)?a:b;
}

posicao p, p2;

main()
{
	int i, j, k, tam, me = 40001, x, y, ind, fim;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			scanf("%d", &mat[i][j]);
	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
		{
			if(!cor[i][j])
			{
				p.x = i;
				p.y = j;
				fila[0] = p;
				ind = 0;
				fim = 1;
				tam = 0;
				while(ind < fim)
				{
					x = fila[ind].x;
					y = fila[ind].y;
					if(!cor[x][y])
					{
						cor[x][y] = 1;
						tam++;
						for(k = 0; k < 4; k++)
						{
							p2.x = x+dx[k];
							p2.y = y+dy[k];
							if(p2.x >= 0 && p2.x < N && p2.y >= 0 && p2.y < M)
								if(mat[x][y] == mat[p2.x][p2.y])
									fila[fim++] = p2;
						}
					}
					ind++;
				}
				me = min(tam, me);
			}
		}
	printf("%d\n", me);
	return 0;
}
