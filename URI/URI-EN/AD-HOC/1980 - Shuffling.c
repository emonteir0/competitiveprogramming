// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shuffling
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1980

#include<stdio.h>
#include<string.h>
#define ll unsigned long long int

ll fat(ll n)
{
	if(n)
		return n*fat(n-1);
	return 1;
}

main()
{
	char v[16];
	while(scanf("%s",v)&&strcmp(v,"0")!=0)
		printf("%lld\n",fat(strlen(v)));
	return 0;
}
