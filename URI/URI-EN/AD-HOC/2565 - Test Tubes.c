// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Test Tubes
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2565

#include<stdio.h>

double vet[101];

double min(double a, double b)
{
	return (a < b) ? a : b;
}

double absol(double x)
{
	if(x < 0)
		return -x;
	return x;
}

main()
{
	int i, j, N;
	double me, ac;
	while(scanf("%d", &N) == 1)
	{
		me = 100000;
		for(i = 0; i < N; i++)
			scanf("%lf", &vet[i]);
		for(i = 0; i < N; i++)
		{
			ac = 0;
			for(j = 0; j < N; j++)
				ac += absol(vet[i]-vet[j]);
			me = min(me, ac);
		}
		printf("%.2lf\n", me);
	}
	return 0;
}
