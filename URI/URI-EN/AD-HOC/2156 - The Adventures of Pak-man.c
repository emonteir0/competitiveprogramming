// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Adventures of Pak-man
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2156

#include<stdio.h>

char mat[100][100];

int dx[4] = {0, 0, 1, -1}, dy[4] = {1, -1, 0, 0}; //RLDU

main()
{
	int i, j, N, M, K, x, y, xc, yc, dir, cont;
	char op, c;
	while(scanf("%d %d %d%*c",&N,&M,&K)==3 &&(N||M||K))
	{
		cont = 0;
		dir = 0;
		for(i=0;i<N;i++)
		{
			for(j=0;j<M;j++)
			{
				scanf("%c",&mat[i][j]);
				if(mat[i][j] == '<')
				{
					x = i;
					y = j;
					mat[i][j] = ' ';
				}
			}
			scanf("%*c");
		}
		for(i=0;i<K;i++)
		{
			scanf("%c",&op);
			if(op=='R')
				dir = 0;
			if(op=='L')
				dir = 1;
			if(op=='D')
				dir = 2;
			if(op=='U')
				dir = 3;
			if(op=='W')
			{
				xc = x + dx[dir];
				yc = y + dy[dir];
				if(xc>=0 && xc<N && yc>=0 && yc<M)
				{
					c = mat[xc][yc];
					if(c != '#')
					{
						if(c == '*')
						{
							mat[xc][yc] = ' ';
							cont++;
						}
						x = xc;
						y = yc;
					}
				}
			}
		}
		scanf("%*c");
		printf("%d\n",cont);
	}
	return 0;
}

