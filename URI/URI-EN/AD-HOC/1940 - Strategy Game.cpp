// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Strategy Game
// Level: 2
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1940

#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;

typedef struct
{
	int a,b;
} tipo;

vector<tipo> v;

bool cmp(tipo t1, tipo t2)
{
	if(t2.a<t1.a)
		return true;
	if(t2.a==t1.a)
		return t2.b<t1.b;
	return false;
}

main()
{
	int n,m,x,i,j;
	tipo u;
	scanf("%d %d",&n,&m);
	u.a=0;
	for(i=0;i<n;i++)
	{
		u.b=i+1;
		v.push_back(u);
	}
	for(j=0;j<m;j++)
	{
		for(i=0;i<n;i++)
		{
			scanf("%d",&x);
			v[i].a+=x;
		}
	}
	sort(v.begin(),v.end(),cmp);
	printf("%d\n",v[0].b);
	return 0;
}
