// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Janela
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2441

#include<stdio.h>

int vet[600];

main()
{
	int i, a, b, c, acum = 0;
	scanf("%d %d %d", &a, &b, &c);
	for(i = a; i < a + 200; i++)
		vet[i] = 1;
	for(i = b; i < b + 200; i++)
		vet[i] = 1;
	for(i = c; i < c + 200; i++)
		vet[i] = 1;
	for(i = 0; i < 600; i++)
		acum += !vet[i];
	printf("%d\n", acum*100);
	return 0;
}

