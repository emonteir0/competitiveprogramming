# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Cards
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2456

x = []
t = raw_input().split(' ')
for c in t:
	x += [int(c)]
y = sorted(x)
z = y[::-1]

if x == y:
	print 'C'
elif x == z:
	print 'D'
else:
	print 'N'

