// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Duende Perdido
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2294

#include<bits/stdc++.h>
#define INFINITO 2147483647

using namespace std;


set< pair<int, pair<int, int> > > fila, fila2;
pair<int, pair<int, int> > p;
int mat[11][11];
bool visitado[11][11];
int N, M;
int distancia[11][11];
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};

bool inRange(int a, int b)
{
	return a >= 1 && a <= N && b >= 1 && b <= M;
}

int value(char c)
{
	if (c == '#')
		return INFINITO;
	if (c == '.' || c == 'E' || c == 'H')
		return 0;
	return c - '0';
}

void dijkstra(int xh, int yh)
{
	int a, x, y, x2, y2, i, j;
	pair<int, int> pos;
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= M; j++)
		{
			visitado[i][j] = 0;
			distancia[i][j] = INFINITO;
		}
	}
	distancia[xh][yh] = 0;
	fila.insert(make_pair(0,make_pair(xh,yh)));
	while(!fila.empty())
	{
		p = *fila.begin();
		fila.erase(p);
		a = p.first;
		pos = p.second;
		x = pos.first;
		y = pos.second;
		if(!visitado[x][y])
		{
			visitado[x][y] = true;
			for(i = 0; i < 4; i++)
			{
				x2 = x+dx[i];
				y2 = y+dy[i];
				if(inRange(x2,y2))
				{
					if(!visitado[x2][y2] && (a + 1 < distancia[x2][y2]) && mat[x2][y2] != 2)
						fila.insert(make_pair((distancia[x2][y2]=a+1),make_pair(x2,y2)));
				}
			}
		}
	}
}

int main()
{
	int i, j, minimo = INFINITO, xh, yh;
	scanf("%d %d%*c",&N, &M);
	for(i = 1; i <= N; i++)
	{
		for(j = 1; j <= M; j++)
		{
			scanf("%d",&mat[i][j]);
			if(mat[i][j] == 3)
			{
				xh = i;
				yh = j;
			}
		}
	}
	dijkstra(xh, yh);
	for(i = 1; i <= N; i++)
		for(j = 1; j <= M; j++)
			if(mat[i][j] == 0)
				minimo = min(minimo, distancia[i][j]);
	printf("%d\n", minimo);
	return 0;
}

