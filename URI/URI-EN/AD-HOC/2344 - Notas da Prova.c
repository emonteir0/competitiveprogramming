// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Notas da Prova
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2344

#include<stdio.h>

main()
{
	int n;
	scanf("%d", &n);
	if(n == 0)
		printf("E\n");
	else if(n <= 35)
		printf("D\n");
	else if(n <= 60)
		printf("C\n");
	else if(n <= 85)
		printf("B\n");
	else
		printf("A\n");
	return 0;
}
