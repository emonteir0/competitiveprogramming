// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brazilian Economy
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1796

#include<stdio.h>
  
  
main()
{
    long long int n,i,cont=0;
    int x;
    scanf("%lld",&n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&x);
        cont+=x==0;
    }
    printf("%s\n",cont>(n/2)?"Y":"N");
    return 0;
}
