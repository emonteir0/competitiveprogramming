// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Robô
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2436

#include<bits/stdc++.h>

using namespace std;

queue< pair<int, int> > fila, fila2;

int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1};
int mat[101][101];

main()
{
	int T, N, M, x, y, i, j, k;
	pair<int, int> p;
	scanf("%d %d", &N, &M);
	scanf("%d %d", &x, &y);
	for(i = 1; i <= N; i++)
		for(j = 1; j <= M; j++)
			scanf("%d", &mat[i][j]);
	fila.push(make_pair(x, y));
	while(!fila.empty())
	{
		p = fila.front();
		fila.pop();
		x = p.first;
		y = p.second;
		if(mat[x][y] == 2)
			continue;
		mat[x][y] = 2;
		for(k = 0; k < 4; k++)
		{
			i = x + dx[k];
			j = y + dy[k];
			if(!((x == i) && (y == j)))
				if(i >= 1 && i <= N && j >= 1 && j <= M)
					if(mat[i][j] == 1)
						fila.push(make_pair(i, j));
		}
	}
	printf("%d %d\n", x, y);
	return 0;
}
