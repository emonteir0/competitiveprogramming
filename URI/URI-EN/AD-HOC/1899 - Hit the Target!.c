// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hit the Target!
// Level: 3
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1899

#include<stdio.h>

double max(double a, double b)
{
	return (a > b) ? a : b;
}

double min(double a, double b)
{
	return (a < b) ? a : b;
}


main()
{
	int a, b, d, N;
	double low, high;
	scanf("%d", &N);
	scanf("%d %d %d", &d, &a, &b);
	low = ((double)a)/d;
	high = ((double)b)/d;
	while(N--)
	{
		scanf("%d %d %d", &d, &a, &b);
		low = max(low, ((double)a)/d);
		high = min(high, ((double)b)/d);
	}
	printf("%s\n", (low<=high)?"Y":"N");
	return 0;
}

