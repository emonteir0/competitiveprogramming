// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Just in Time
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2353

#include<stdio.h>
#define MAX 31623

int prime[MAX+1], prime_cont = 0;
char isprime[MAX+1];

void siege()
{
	int i, j;
	prime[prime_cont++] = 2;
	for(i = 4; i <= MAX; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= MAX; i += 2)
	{
		if(!isprime[i])
		{
			prime[prime_cont++] = i;
			for(j = i*i; j <= MAX; j += i)
				isprime[j] = 1;
		}
	}
}

main()
{
	int i, j, N;
	siege();
	scanf("%d", &N);
	if(N == 2)
		printf("2\n");
	else if(N <= MAX)
	{
		N -= N%2 == 0;
		for(i = N; i >= 0; i -= 2)
			if(!isprime[i])
				break;
		printf("%d\n", i);
	}
	else
	{
		N -= N%2 == 0;
		for(i = N; i >= 0; i -= 2)
		{
			for(j = 0; j < prime_cont; j++)
				if(i % prime[j] == 0)
					break;
			if(j == prime_cont)
				break;
		}
		printf("%d\n", i);
	}
	return 0;
}
