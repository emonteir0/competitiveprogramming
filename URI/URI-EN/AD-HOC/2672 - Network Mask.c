// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Network Mask
// Level: 4
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2672

#include<stdio.h>
#define ll long long

void func(ll ip)
{
	int a, b, c, d;
	d = ip % 256;
	ip /= 256;
	c = ip % 256;
	ip /= 256;
	b = ip % 256;
	a = ip/256;
	printf("%d.%d.%d.%d\n", a, b, c, d);
}

int main()
{
	int a, b, c, d, m;
	int ma, mb, mc, md;
	ll ip, ipstart, ipend, mask, hosts;
	char op;
	while(scanf("%d.%d.%d.%d%c", &a, &b, &c, &d, &op) == 5)
	{
		ip = a;
		ip = 256*ip + b;
		ip = 256*ip + c;
		ip = 256*ip + d;
		if(op == '/')
		{
			scanf("%d", &m);
			mask = 1LL << (32-m);
			hosts = mask - 2;
			ipstart = ip - ip%mask;
			ipend = ipstart + mask - 1;
			printf("Endereco de rede: ");
			func(ipstart);
			printf("Endereco de broadcast: ");
			func(ipend);
			printf("Numero de maquinas: %lld\n", hosts);
		}
		else
		{
			scanf("%d.%d.%d.%d", &ma, &mb, &mc, &md);
			mask = ma;
			mask = 256*mask + mb;
			mask = 256*mask + mc;
			mask = 256*mask + md;
			mask = (1LL << 32) - mask;
			hosts = mask - 2;
			ipstart = ip - ip%mask;
			ipend = ipstart + mask - 1;
			printf("Endereco de rede: ");
			func(ipstart);
			printf("Endereco de broadcast: ");
			func(ipend);
			printf("Numero de maquinas: %lld\n", hosts);
		}
		printf("\n");
	}
	return 0;
}
