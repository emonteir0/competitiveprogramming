// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Self-Rotating Su...
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2347

#include<bits/stdc++.h>
#define MOD 1000000007

using namespace std;

map< pair<int, int>, int> Mapa;
map< pair<int, int>, int>::iterator it;
int comb[1001][1001];
int x[1001], y[1001];
int ans[1001];

int main()
{
	int i, j, N;
	for(i = 0; i <= 1000; i++)
	{
		comb[i][0] = 1;
		comb[i][i] = 1;
	}
	for(i = 1; i <= 1000; i++)
		for(j = 0; j <= i; j++)
			comb[i][j] = (comb[i-1][j-1] + comb[i-1][j]) % MOD;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d %d", &x[i], &y[i]);
		Mapa[make_pair(x[i]*2, y[i]*2)]++;
		for(j = 0; j < i; j++)
			Mapa[make_pair(x[i]+x[j], y[i]+y[j])] += 2;
	}
	for(it = Mapa.begin(); it != Mapa.end(); it++)
	{
		int x = (it->second)/2;
		for(i = 1; i <= x; i++)
		{
			ans[2*i] = (ans[2*i] + comb[x][i]) % MOD;
			if((it->second)&1)
				ans[2*i+1] = (ans[2*i+1] + comb[x][i]) % MOD; 
		}
	}
	printf("%d", N);
	for(i = 2; i <= N; i++)
		printf(" %d", ans[i]);
	printf("\n");
	return 0;
}
