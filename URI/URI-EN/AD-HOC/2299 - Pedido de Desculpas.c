// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pedido de Desculpas
// Level: 5
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2299

#include<stdio.h>


int best[51][1001];
int valor[51];
int tam[51];

main()
{
	int n2, i, j, k = 1, limite;
	while(scanf("%d %d",&limite, &n2)==2 && (n2||limite))
	{
		best[0][0]=0;
		for(i=1;i<=n2;i++)
		{
			scanf("%d %d",&tam[i], &valor[i]);
			best[i][0]=0;
		}
		
		for(i=0;i<=limite;i++)
			best[0][i]=0;
			
		for(i=1;i<=n2;i++)
		{
			for(j=0;j<=limite;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]]+valor[i]>best[i-1][j])
						best[i][j]=best[i-1][j-tam[i]]+valor[i];
					else
						best[i][j]=best[i-1][j];
				}
				else
					best[i][j]=best[i-1][j];
			}
		}
		printf("Teste %d\n%d\n\n", k++, best[n2][limite]);
	}
	return 0;
}

