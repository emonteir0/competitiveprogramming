// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alarm Clock
// Level: 1
// Category: AD-HOC
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1103

#include<stdio.h>

main()
{
	int h1,h2,m1,m2;
	while(scanf("%d %d %d %d",&h1,&m1,&h2,&m2)==4&&(h1||m1||h2||m2))
	{
		h1=(h2-h1)*60+(m2-m1);
		printf("%d\n",h1+1440*(h1<0));
	}
	return 0;
}
