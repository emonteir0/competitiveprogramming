# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Factors
# Level: 5
# Category: AD-HOC
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2258

from math import factorial

vet = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59];
valores = [];
mapa = {};

def create(i, x, tam):
    for j in range(0, tam):
        if x*vet[i] < 9223372036854775808:
            x *= vet[i]
            valores.append(x)
            create(i+1, x, j+1)
        else:
            break

def fatorar(x):
    lista = [];
    cont = 0
    tot = 0
    for primo in vet:
        if x == 1:
            break
        cont = 0
        while x%primo == 0:
            cont += 1
            x /= primo
        lista.append(cont)
        tot += cont
    y = factorial(tot)
    for z in lista:
        y /= factorial(z)
    return y
    


create(0, 1, 99999);
for x in valores:
    k = fatorar(x)
    if k < 9223372036854775808:
        if k in mapa:
            mapa[k] = min(mapa[k], x)
        else:
            mapa[k] = x
            
while True:
    try:
        x = input()
        print '%d %d ' % (x, mapa[x])
    except:
    	break
