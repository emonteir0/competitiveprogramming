// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dinostratus Numbers
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2133

#include<stdio.h>

main()
{
	int i,cont=0,k=1,tam,ma,me;
	int n;
	char b;
	while(scanf("%d",&n)!=EOF)
	{
		cont=0;
		ma=0;
		me=21;
		tam=0;
		b=0;
		if(n%2==0)
		{
			n/=2;
			cont++;
			while(n%2==0)
			{
				cont++;
				n/=2;
			}
			tam++;
			if(cont>ma)
				ma=cont;
			if(cont<me)
				me=cont;
		}
		for(i=3;i*i<=n;i+=2)
		{
			cont=0;
			if(n%i==0)
			{
				n/=i;
				cont++;
				while(n%i==0)
				{
					n/=i;
					cont++;
				}
				tam++;
				if(cont>ma)
					ma=cont;
				if(cont<me)
					me=cont;
			}
		}
		if(n>1)
		{
			cont=1;
			tam++;
			if(cont>ma)
				ma=cont;
			if(cont<me)
				me=cont;
		}
		if(tam>=4)
			b=1;
		if(tam==3 && ma>1)
			b=1;
		if(tam==2 && (ma>4 || (ma>1 && me>1)))
			b=1;
		if(tam==1 && ma>7)
			b=1;
		printf("Instancia %d\n%s\n\n",k++,b?"sim":"nao");
	}
	return 0;
}

