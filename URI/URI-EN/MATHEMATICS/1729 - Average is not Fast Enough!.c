// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Average is not Fast Enough!
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1729

#include<stdio.h>
#include<math.h>

main()
{
	int i, n, h, m, s, x, v, minuto, segundo;
	char o;
	double D, acum;
	scanf("%d %lf", &n, &D);
	while(scanf("%d%*c", &x) == 1)
	{
		o = 1;
		acum = 0;
		for(i = 0; i < n; i++)
		{
			v = scanf("%d:%d:%d", &h, &m, &s);
			if(v == 0)
			{
				scanf("%*s");
				o = 0;
			}
			else
				acum += 3600*h+60*m+s;
		}
		acum /= D*60.0;
		minuto = (int)floor(acum);
		segundo = round((acum - minuto)*60);
		if(segundo == 60)
		{
			minuto++;
			segundo = 0;
		}
		if(o)
			printf("%3d: %d:%02d min/km\n", x, minuto, segundo);
		else
			printf("%3d: -\n", x);
	}
	return 0;
}

