// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Feynman
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1323

#include<stdio.h>

main()
{
	int n;
	while(scanf("%d",&n)==1&&n)
	{
		printf("%d\n",(2*n+1)*(n+1)*n/6);
	}
	return 0;
}
