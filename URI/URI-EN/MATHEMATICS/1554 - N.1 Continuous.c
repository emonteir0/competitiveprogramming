// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: N.1 Continuous
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1554

#include<stdio.h>


main()
{
	int n, n2, x, y, cx, cy,d,d2, i, ind;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d %d",&n2,&x,&y);
		d=10082000;
		for(i=1;i<=n2;i++)
		{
			scanf("%d %d",&cx,&cy);
			d2=(cx-x)*(cx-x)+(cy-y)*(cy-y);
			if(d>d2)
			{
				d=d2;
				ind=i;
			}
		}
		printf("%d\n",ind);
	}
	return 0;
}
