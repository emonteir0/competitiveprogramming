// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Collectable Cards
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1028

#include<stdio.h>
 
main()
{
    int i,n,a,b,c;
    scanf("%d",&n);
    while(n--){
        scanf("%d %d",&a,&b);
        if(b>a)
        {
            b+=a;
            a=b-a;
            b-=a;
        }
        while(b)
        {
            c=b;
            b=a%b;
            a=c;
        }
        printf("%d\n",a);
    }
    return 0;
}
