// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pro Ability
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2337

#include<stdio.h>
#define ll long long

ll memo[44], pot[43];

ll solve(int N)
{
	ll val = N-1;
	int i;
	if(memo[N] != -1)
		return memo[N];
	for(i = N-2; i >= 1; i--)
		val += solve(i);
	return memo[N] = val;
}

ll mdc(ll a, ll b)
{
	if(b == 0)
		return a;
	return mdc(b, a%b);
}

main()
{
	int i, N = 2;
	ll x, md;
	x = 2;
	for(i = 1; i<= 44; i++)
	{
		memo[i] = -1;
		pot[i] = x;
		x <<= 1;
	}
	memo[1] = 0;
	memo[2] = 1;
	solve(43);
	while(scanf("%d", &N)==1)
	{
		x = memo[N+1]+1;
		md = mdc(x, pot[N]);
		printf("%lld/%lld\n", x/md, pot[N]/md);
	}
	return 0;
}
