// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Darlan's Gas Station
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2817

#include<bits/stdc++.h>

using namespace std;

int N, M;
char mat[1001][1001];
int acum[1001][1001], acum2[1001][1001];

int val(char c)
{
	if(c == 'C')
		return 25;
	return 12;
}

int val2(char c)
{
	return c == 'C';
}

void construcao()
{
	for(int i = 0; i <= N; i++)
		acum[i][0] = 0;
	for(int i = 0; i <= M; i++)
		acum[0][i] = 0;
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= M; j++)
			acum[i][j] = val(mat[i-1][j-1]) + acum[i-1][j] + acum[i][j-1] - acum[i-1][j-1];
}

void construcao2()
{
	for(int i = 0; i <= N; i++)
		acum2[i][0] = 0;
	for(int i = 0; i <= M; i++)
		acum2[0][i] = 0;
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= M; j++)
			acum2[i][j] = val2(mat[i-1][j-1]) + acum2[i-1][j] + acum2[i][j-1] - acum2[i-1][j-1];
}


int consulta(int xi, int xf, int yi, int yf)
{
	return acum[xf][yf] + acum[xi-1][yi-1] - acum[xf][yi-1] - acum[xi-1][yf];
}

int consulta2(int xi, int xf, int yi, int yf)
{
	int x = acum2[xf][yf] + acum2[xi-1][yi-1] - acum2[xf][yi-1] - acum2[xi-1][yf];
	return  (x != 0) && (x != (xf-xi+1)*(yf-yi+1));
}

main()
{
	int L, ma = -1;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);
	scanf("%d", &L);
	construcao();
	construcao2();
	for(int i = 1; i <= N-L+1; i++)
		for(int j = 1; j <= M-L+1; j++)
			if(consulta2(i, i+L-1, j, j+L-1))
				ma = max(ma, consulta(i, i+L-1, j, j+L-1));
	printf("%d\n", ma);
	return 0;
}
