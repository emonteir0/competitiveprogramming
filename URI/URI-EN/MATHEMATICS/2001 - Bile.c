// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bile
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2001

#include<stdio.h>
#define ll unsigned long long int
#define MOD 303700049

int tam;
int vet[101];
int F[101][101], M[101][101];
ll G[101][101];

void multiply();
void multiply2();
   
void power(ll n)
{
	int i,j;
	if(n==0||n==1)
	  return;		
	power(n/2);
	multiply2();
	if(n%2==1)
	 multiply();
}
   
void multiply()
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*M[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2()
{
    int i,j,k;
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            G[i][j]=0;
            
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            for(k=0;k<tam;k++)
                G[i][j]+=(((ll)F[i][k])*F[k][j]);
                
    for(i=0;i<tam;i++)
        for(j=0;j<tam;j++)
            F[i][j]=G[i][j]%MOD;
}

void sum()
{
	int i,j,v;
	ll x=0,y=0;
	for(j=0;j<tam;j++)
	{
		v=vet[tam-1-j];
		x+=((ll)F[1][j])*v;
		y+=((ll)F[0][j])*v;
	}
	x%=MOD;
	y%=MOD;
	printf("%llu %llu\n",x,y);
}

main()
{
	ll a,K;
	int i,j,N,x,y;
	while(scanf("%d %llu",&N,&K)==2)
	{
		vet[N]=0;
		tam=N+1;
		for(i=0;i<N;i++)
		{
			scanf("%llu",&a);
			vet[N]=(vet[N]+a)%MOD;
			vet[i]=a%MOD;
		}
		F[0][0]=M[0][0]=1;
		F[1][0]=M[1][0]=0;
		for(j=1;j<=N;j++)
		{
			F[0][j]=M[0][j]=j;
			F[1][j]=M[1][j]=j;
		}
		for(i=2;i<=N;i++)
		{
			F[i][0]=M[i][0]=0;
			for(j=1;j<=N;j++)
					F[i][j]=M[i][j]=(i==j+1);
		}
		if(K==N)
			printf("%d %d\n",vet[N-1],vet[N]);
		else
		{
			power(K-N);
			sum();
		}
	}
	return 0;
}
