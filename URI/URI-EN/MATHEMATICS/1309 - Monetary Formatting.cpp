// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Monetary Formatting
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1309

#include<cstdio>
#include<stack>

using namespace std;

stack<int> pilha;

main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2)
	{
		while(a>0)
		{
			pilha.push(a%1000);
			a/=1000;
		}
		printf("$");
		if(!pilha.empty())
		{
			printf("%d",pilha.top());
			pilha.pop();
		}
		else	printf("0");
		while(!pilha.empty())
		{
			printf(",%03d",pilha.top());
			pilha.pop();
		}
		printf(".%02d\n",b);
	}
	return 0;
}
