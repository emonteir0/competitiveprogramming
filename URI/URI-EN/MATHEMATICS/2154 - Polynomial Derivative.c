// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Polynomial Derivative
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2154

#include<stdio.h>

void printpol(int x, int y)
{
	if(y == 2)
		printf("%dx",x<<1);
	else
		printf("%dx%d",y*x,y-1);
}

main()
{
	int v1, v2, i, n;
	while(scanf("%d",&n)==1)
	{
		scanf("%dx%d",&v1,&v2);
		printpol(v1,v2);
		for (i = 1; i < n; i++)
		{
			scanf(" + %dx%d",&v1,&v2);
			printf(" + ");
			printpol(v1,v2);
		}	
		printf("\n");
	}
	return 0;
}

