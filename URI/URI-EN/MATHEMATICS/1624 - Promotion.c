// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Promotion
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1624

#include<stdio.h>


int best[101][1001];
int valor[101];
int tam[101];

main()
{
	int n2, i, j, limite, x;
	while(scanf("%d",&n2)==1&&n2>0)
	{
		best[0][0]=0;
		for(i=1;i<=n2;i++)
		{
			scanf("%d %d",&valor[i],&tam[i]);
			best[i][0]=0;
		}
		scanf("%d",&limite);
		for(i=0;i<=limite;i++)
		{
			best[0][i]=0;
		}
		for(i=1;i<=n2;i++)
		{
			for(j=0;j<=limite;j++)
			{
				if(j>=tam[i])
				{
					if(best[i-1][j-tam[i]]+valor[i]>best[i-1][j])
					{
						best[i][j]=best[i-1][j-tam[i]]+valor[i];
					}
					else
					{
						best[i][j]=best[i-1][j];
					}
				}
				else
				{
					best[i][j]=best[i-1][j];
				}
			}
		}
		printf("%d\n",best[n2][limite]);
	}
	return 0;
}
