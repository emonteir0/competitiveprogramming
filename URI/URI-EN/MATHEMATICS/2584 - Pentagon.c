// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pentagon
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2584

#include<stdio.h>
#include<math.h>

double vet[10001];

main()
{
	int x;
	vet[1] = 1.25 * tan(0.3 * M_PI);
	for(x = 1; x <= 10000; x++)
		vet[x] = x*x*vet[1];
	scanf("%*d");
	while(scanf("%d", &x) == 1)
		printf("%.3lf\n", vet[x]);
	return 0;
}
