// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: {sum+=i++} to Reach N
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1568

#include <stdio.h>
#define ll unsigned long long

char isprime[3000001];
int prime[2000010], prime_cont = 0, lim = 3000000, sqlim = 1733;


ll fatorar(ll x)
{
    int i=1,div=1,cont;
    if (x==0) return 1;
    while(x%2==0)   x/=2;
    while((x>1)&&(i<prime_cont))
    {
        cont=0;
        while(x%prime[i]==0)
        {
            x/=prime[i];
            cont++;
        }
        div*=(cont+1);
        i++;
    }
    if(x>1)
        div*=2;
    return div;
}


int main() {
	ll x;
	int i,j;
    prime[prime_cont++] = 2;
	for(i=3; i<=sqlim; i+=2) if(!isprime[i]) {
		prime[prime_cont++] = i;
		
		for(j=i*i; j<=lim; j+=i) {
			isprime[j] = 1;
		}
	}
	for(;i<=lim;i+=2)
		if(!isprime[i])
			prime[prime_cont++] = i;
	for(i = prime_cont-1; i>=prime_cont-101; i--) 
	while(scanf("%lld",&x)==1) printf("%lld\n",fatorar(x));
	return 0;
}

