// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Nina's Gift
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2735

#include<bits/stdc++.h>

using namespace std;

int ciclo[100001];
int cicloval[100001];
int vis[100001];
int val[100001];
int P[100001];
int e;

void dfs(int x, int y)
{
	if(vis[x])
		return;
	vis[x] = 1;
	ciclo[x] = y;
	val[x] = e++;
	dfs(P[x], y);
}

main()
{
	int T, N, M, x, y;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		for(int i =0; i < N; i++)
		{
			scanf("%d", &P[i]);
			val[i] = -1;
			ciclo[i] = -1;
			cicloval[i] = 0;
			vis[i] = 0;
		}
		for(int i = 0; i < N; i++)
			if(!vis[i])
			{
				e = 0;
				dfs(i, i);
				cicloval[i] = e;
			}
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d", &x, &y);
			y = (x+y)%N;
			if(ciclo[x] != ciclo[y])
				printf("-1\n");
			else
			{
				e = cicloval[ciclo[x]];
				printf("%d\n", (((val[x]-val[y]) % e) + e) % e);
			}
		}
	}
	return 0;
}
