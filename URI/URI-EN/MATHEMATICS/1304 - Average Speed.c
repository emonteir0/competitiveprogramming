// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Average Speed
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1304

#include<stdio.h>


main()
{
	int h1, h2, m1, m2, s1, s2,tempo;
	char c,u=1;
	double espaco=0,v=0;
	while(scanf("%d%*c%d%*c%d%c",&h2,&m2,&s2,&c)==4)
	{
		if(u==1)
		{
			h1=h2;
			m1=m2;
			s1=s2;
			u=0;
		}
		tempo=s2-s1;
		if(tempo<0)
		{
			m1++;
			tempo+=60;
		}
		tempo+=60*(m2-m1);
		if(tempo<0)
		{
			h1++;
			tempo+=3600;
		}
		tempo+=3600*(h2-h1);
		espaco+=v*tempo/3600.0;
		if(c==' ')
		{
			scanf("%lf%*c",&v);
		}
		else
		{
			printf("%s%d:%s%d:%s%d %.2lf km\n",((h2<10)?"0":""),h2,((m2<10)?"0":""),m2,((s2<10)?"0":""),s2,espaco);
		}
		h1=h2;
		m1=m2;
		s1=s2;
	}
	return 0;
}
