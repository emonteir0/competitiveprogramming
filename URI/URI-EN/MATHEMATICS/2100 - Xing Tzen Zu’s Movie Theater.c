// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Xing Tzen Zu’s Movie Theater
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2100

#include<stdio.h>
#include<string.h>
#define MAX 4000
#define MOD 1300031
#define ll long long int

int fatoriais[MAX+1],invfat[MAX+1], pot2[MAX+1];

void pot()
{
	int i;
	pot2[0] = 1;
	for(i = 1; i <= 2000; i++)
		pot2[i] = (2LL*pot2[i-1])%MOD;
}
   
void fat()
{
	int i;
	fatoriais[0]=1;
	for(i=1;i<=MAX;i++)
		fatoriais[i]=(((ll)i)*fatoriais[i-1])%MOD;
}

int mdc(int  a, int b, int *x, int *y) {
  int xx, yy, d;
  if(b==0)
  {
    *x=1; *y=0;
    return a;
  }
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}

int inv(int a)
{
	int x, y;
	if(invfat[a]!=0)
		return invfat[a];
	mdc(fatoriais[a],MOD,&x,&y);
	if(x<0)
		x = x+MOD;
	invfat[a]=x;
	return x;
}
  
main()
{
	int k, n, m;
    fat();
    pot();
    invfat[0]=1;
    invfat[1]=1;
    scanf("%d", &k);
    while(k--)
    {
		scanf("%d %d", &n, &m);
		printf("%lld\n", (((ll)pot2[m])*(((ll)fatoriais[n-m])*inv(n-2*m))%MOD)%MOD);
	}
    return 0;
}

