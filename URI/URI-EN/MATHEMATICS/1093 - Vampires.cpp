// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vampires
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1093

#include<bits/stdc++.h>

using namespace std;

int sz;
int EV1, EV2, AT, D;

map< pair<int, int>, int > Mapa, Mapa2;
map< int, pair<int, int> > MapaR, MapaR2;
int esq[21], dir[21];
double A[21][21];
double B[21];
double ans[21];

int dfs(int x, int y)
{
	if(Mapa.count(make_pair(x, y)) > 0)
		return Mapa[make_pair(x, y)];
	int u = Mapa[make_pair(x, y)] = sz++;
	MapaR[u] = make_pair(x, y);
	if(x <= 0 || y <= 0)
		return u;
	esq[u] = dfs(x+D, y-D);
	dir[u] = dfs(x-D, y+D);
	return u;
}

void solve()
{
	int i, j, k, ind;
	double k1, k2;
	for(i = 0; i < sz; i++)
	{
		for(j = 0; j < sz; j++)
			A[i][j] = 0;
		B[i] = 0;
	}
	for(i = 0; i < sz; i++)
	{
		if(MapaR[i].second <= 0)
		{
			A[i][i] = 1;
			B[i] = 1;
		}
		else if(MapaR[i].first <= 0)
		{
			A[i][i] = 1;
			B[i] = 0;
		}
		else
		{
			A[i][i] = 1;
			A[i][esq[i]] = -(AT/6.0);
			A[i][dir[i]] = -((6-AT)/6.0);
			B[i] = 0;
		}
	}
/*	for(i = 0; i < sz; i++)
	{
		for(j = 0; j < sz; j++)
			printf("%.2lf ", A[i][j]);
		printf("= %.2lf\n", B[i]);
	}*/
	for(i = 0; i < sz; i++)
	{
		for(j = i; j < sz; j++)
			if(A[j][i] != 0)
			{
				swap(A[j], A[i]);
				swap(B[j], B[i]);
				break;
			}
		if(i >= sz)
			break;
		for(j = i+1; j < sz; j++)
		{
			if(A[j][i] != 0)
			{
				k1 = A[i][i];
				k2 = A[j][i];
				for(k = i; k < sz; k++)
				{
					A[j][k] = k1*A[j][k] - k2*A[i][k];
				}
				B[j] = k1*B[j] - k2*B[i];
			}
		}
	}
/*	for(i = 0; i < sz; i++)
	{
		for(j = 0; j < sz; j++)
			printf("%.4lf ", A[i][j]);
		printf("= %.4lf\n", B[i]);
	}*/
	for(i = sz-1; i >= 0; i--)
	{
		ans[i] = B[i];
		for(j = i+1; j < sz; j++)
			ans[i] -= ans[j]*A[i][j];
		ans[i] /= A[i][i];
	}
	printf("%.1lf\n", 100.0*ans[0]);
}

main()
{
	while(scanf("%d %d %d %d", &EV1, &EV2, &AT, &D) == 4 && EV1)
	{
		Mapa = Mapa2;
		MapaR = MapaR2;
		sz = 0;
		dfs(EV1, EV2);
		solve();
	}
	return 0;
}
