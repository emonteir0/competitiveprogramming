// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Transformer
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1399

#include<stdio.h>
#define max 300000
#define ll long long int
 
ll vet[max];
 
main()
{
    int n,m,i,L,R,v,p,k,j,u;    
    scanf("%d %d %d",&n,&m,&u);
    for(i=0;i<n;i++)
        scanf("%d",&vet[i]);
    for(j=0;j<m;j++)
    {
        k=0;
        scanf("%d %d %d %d",&L,&R,&v,&p);
            for(i=L-1;i<R;i++)
                if(vet[i]<v)
                    k++;
        vet[p-1]=(((ll)(u))*k)/(R-L+1);
    }
    for(i=0;i<n;i++)
        printf("%lld\n",vet[i]);
    return 0;
}
