// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Autopotential
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2505

#include<stdio.h>
#define ll long long

int expo(int N, int e, int M)
{
	int x;
	if(e == 0)
		return 1;
	if(e&1)
		return (((ll)N) * expo(N, e-1, M))%M;
	x = expo(N, e/2, M);
	return (((ll)x) * x)%M; 
}

main()
{
	int N, M, y;
	while(scanf("%d", &N) == 1)
	{
		M = 1;
		while(M <= N)
			M *= 10;
		y = expo(N, N, M);
		printf("%s\n", y == N ? "SIM" : "NAO");
	}
	return 0;
}
