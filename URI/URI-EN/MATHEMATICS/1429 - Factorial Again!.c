// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factorial Again!
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1429

#include<stdio.h>

main()
{
	int n;
	while(scanf("%d",&n)==1&&n)
		printf("%d\n",(120*(n/10000))+(24*((n/1000)%10))+(6*((n/100)%10))+(2*((n/10)%10))+(n%10));
	return 0;
}
