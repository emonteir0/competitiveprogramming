// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Final Frontier
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1967

#include<bits/stdc++.h>

using namespace std;

int mat1[600][100],mat2[100][2000],mat3[2000][100], matR1[600][2000],matR2[600][100],res[10001][101],maximo[100];

int tam(int x)
{
	int cont=0;
	while(x!=0)
	{
		x/=10;
		cont++;
	}
	return cont;
}

main()
{
	int i,j,k,G,A,M,C;
	clock_t t1,t2;
	for(i=0;i<10001;i++)
		for(j=0;j<101;j++)
			res[i][j]=i*j;
	scanf("%d %d %d %d",&G,&A,&M,&C);
	for(i=0;i<G;i++)
		for(j=0;j<A;j++)
			scanf("%d",&mat1[i][j]);
	for(i=0;i<A;i++)
		for(j=0;j<M;j++)
			scanf("%d",&mat2[i][j]);
	for(i=0;i<M;i++)
		for(j=0;j<C;j++)
			scanf("%d",&mat3[i][j]);
	for(i=0; i<G; i++)
	{
         for(k=0; k<A; k++)
            for(j=0; j<M; j++)
                matR1[i][j] += res[mat1[i][k]][mat2[k][j]];
    }
    for(i=0; i<G; i++)
	{
         for(j=0; j<C; j++)
         {
            for(k=0; k<M; k++)
                matR2[i][j] += res[matR1[i][k]][mat3[k][j]];
        	maximo[j]=max(maximo[j],matR2[i][j]);
        }
    }
    for(j=0;j<C;j++)
    	maximo[j]=tam(maximo[j]);
    for(i=0;i<G;i++)
    {
    	printf("%*d",maximo[0],matR2[i][0]);
    	for(j=1;j<C;j++)
    		printf(" %*d",maximo[j],matR2[i][j]);
    	printf("\n");
    }
	return 0;
}
