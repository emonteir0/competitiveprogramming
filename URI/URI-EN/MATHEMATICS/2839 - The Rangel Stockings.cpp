// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Rangel Stockings
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2839

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x;
	scanf("%d", &x);
	printf("%d\n", x+1);
	return 0;
}
