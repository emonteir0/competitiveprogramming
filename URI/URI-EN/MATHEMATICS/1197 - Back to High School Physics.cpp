// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Back to High School Physics
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1197

#include<stdio.h>
 
main()
{
int v,t;
while(scanf("%d %d",&v,&t)==2)
printf("%d\n",v*t*2);
return 0;
}
