// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Letters To Santa
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2819

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007LL
#define MOD2 1000000014000000049LL

using namespace std;

int N;
ll A[26][26], B[26][26], C[26][26];

void multiply(ll F[26][26], ll M[26][26])
{
    int i, j, k;
    for(i = 0; i < N; i++)
        for(j = 0; j < N; j++)
            C[i][j] = 0;
            
    for(i = 0; i < N; i++)
        for(j = 0; j < N; j++)
            for(k = 0; k < N; k++)
            {
                C[i][j] += (F[i][k]*M[k][j]);
                if(C[i][j] >= MOD2)
                	C[i][j] %= MOD;
			}
    for(i = 0; i < N; i++)
        for(j = 0; j < N; j++)
            F[i][j] = C[i][j]%MOD;
}


int powermatrix(ll n)
{
	ll sum = 0;
	if(n == 0)
		return N;
    powermatrix(n/2);
    multiply(A, A);
  	if(n&1)
     	multiply(A, B);
    for(int i = 0; i < N; i++)
    	for(int j = 0; j < N; j++)
    		sum += A[i][j];
    return sum%MOD;
}

main()
{
	int T, C, Q;
	char str[5];
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d %d", &C, &N, &Q);
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
			{
				A[i][j] = (i==j);
				B[i][j] = 1;
			}
		while(Q--)
		{
			scanf("%s", str);
			B[str[0]-'a'][str[1]-'a'] = 0;
		}
		/*for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
				printf("%d ", A[i][j]);
			printf("\n");
		}
		printf("\n");
		
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
				printf("%d ", B[i][j]);
			printf("\n");
		}
		printf("\n");*/
		printf("%lld\n", powermatrix(C-1)%MOD);
	}
	return 0;
}
