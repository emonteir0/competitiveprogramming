// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Horace and His Primes
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2096

#include<stdio.h>

int primos[1001], prime_cont = 0;
int isprime[1001];

int valor[1000001];
int mat[1000001][13];

int getvalue(int x)
{
	int n, i, s = 0;
	n = x;
	if(valor[x] != 0)
		return valor[x];
	for(i = 0; i < prime_cont && x > 1; i++)
		if(x%primos[i]==0)
		{
			s += primos[i];
			while(x%primos[i] == 0)
				x /= primos[i];
		}
	if(x != 1)
	{
		if(s == 0)
			return valor[x] = 1;
		s += x;	
	}
	return valor[n] = getvalue(s) + 1;
}

void prime()
{
	int i, j;
	primos[prime_cont++] = 2;
	valor[2] = 1;
	for(i = 4; i < 1000; i+= 2)
		isprime[i] = 1;
	for(i = 3; i < 1000; i+=2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			valor[i] = 1;
			for(j = i*i; j < 1000; j += i)
				isprime[j] = 1;
		}
	}
}

main()
{
	int i, j, A, B, K, N;
	prime();
	for(i = 2; i <= 1000000; i++)
		getvalue(i);
	for(i = 2; i <= 1000000; i++)
	{
		for(j = 1; j <= 12; j++)
			mat[i][j] = mat[i-1][j] + (valor[i] == j);
	}
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d", &A, &B, &K);
		if(K > 12)
			printf("0\n");
		else
			printf("%d\n", mat[B][K]-mat[A-1][K]);
	}
	return 0;
}
