// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: It-Miha
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1338

#include<bits/stdc++.h>
#define ll long long
#define MAX 20000000000LL
#define MAXN 200000
#define SQMAX 500
using namespace std;

int primos[MAXN+1], prime_cont = 0;
int isprime[MAXN+1];

set< pair<ll, int> > terms;
vector< pair<ll, int> > miha;

void crivo()
{
	int i, j;
	primos[prime_cont++] = 2;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= SQMAX; i += 2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			for(j = (ll)i*i; j <= MAXN; j += i)
				isprime[j] = 1;
		}
	}
	for(; i <= MAXN; i += 2)
		if(!isprime[i])
			primos[prime_cont++] = i;
}

ll func(ll N)
{
	int i;
	ll ans = 0, parcel;
	for(i = 0; i < miha.size(); i++)
	{
		parcel = N/miha[i].first;
		if(parcel == 0)
			break;
		if(miha[i].second == 1)
			ans += parcel;
		else
			ans -= parcel;
	}
	return ans;
}

ll itmiha(ll N)
{
	ll lo = 1, hi = MAX+MAX, z, mid, ans;
	while(lo <= hi)
	{
		mid = (lo+hi) >> 1;
		z = func(mid);
		if(z == N)
			ans = mid;
		if(z >= N)
			hi = mid-1;
		else
			lo = mid+1;
	}
	return ans;
}


main()
{
	int i, j, k, T;
	ll N, z;
	crivo();
	terms.insert(make_pair(1, 1));
	for(i = 0; i < prime_cont; i++)
	{
		z = (ll)primos[i] * primos[i];
		set< pair<ll, int> > new_terms;
		for (set< pair<ll, int> >::iterator it = terms.begin(); it != terms.end(); it++)
		{
			if (z <= (MAX+MAX) / it->first)
				new_terms.insert(make_pair(it->first * z, -it->second));
			else 
				break;
		}
		for (set<  pair<ll, int> >::iterator it = new_terms.begin(); it != new_terms.end(); it++)
			terms.insert(*it);
	}
	for(set< pair<ll, int> >::iterator it = terms.begin(); it != terms.end(); it++)
		miha.push_back(*it);
	scanf("%d", &T);
	while(T--)
	{
		scanf("%lld", &N);
		printf("%lld\n", itmiha(N));
	}
	return 0;
}
