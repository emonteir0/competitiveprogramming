// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Numbering Roads
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1306

#include<stdio.h>


main()
{
	int a,b,n,k=1;
	while(scanf("%d %d",&a,&b)==2&&(a||b))
	{
		n=a/b-(a%b==0);
		if(n<=26)
		{
			printf("Case %d: %d\n",k++,n);
		}
		else
			printf("Case %d: impossible\n",k++);
	}
	return 0;
}
