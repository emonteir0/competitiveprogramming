// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Super Poker
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1353

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000009
#define MOD2 1000000018000000081LL

using namespace std;

ll AC[121][121];
int F[121][121], G[121][121]; 

int dp[11][11];
int ord;
int factors[5] = {1, 4, 6, 4, 1};

void multiply(int A[121][121], int B[121][121])
{
	for(int i = 0; i < ord; i++)
		for(int j = 0; j < ord; j++)
		{
			AC[i][j] = 0;
			for(int k = 0; k < ord; k++)
			{
				AC[i][j] += ((ll)A[i][k])*B[k][j];
				if(AC[i][j] >= MOD2)
					AC[i][j] -= MOD2;
			}
		}
	for(int i = 0; i < ord; i++)
		for(int j = 0; j < ord; j++)
			A[i][j] = AC[i][j] % MOD;
}


main()
{
	int N, K, e, x, y, ind;
	ll ans, ans2;
	while(scanf("%d %d", &N, &K) == 2 && N)
	{
		for(int i = 0; i <= K; i++)
			for(int j = 0; j <= K; j++)
				dp[i][j] = 0;
		dp[0][0] = 1;
		for(int i = 1; i <= K; i++)
			for(int j = 1; j <= K; j++)
			{
				x = i-j;
				y = j;
				if(x >= 0)
					dp[i][j] += dp[x][y];
				y--;
				if(x >= 0 && y >= 0)
					dp[i][j] += 4*dp[x][y];
				y--;
				if(x >= 0 && y >= 0)
					dp[i][j] += 6*dp[x][y];
				y--;
				if(x >= 0 && y >= 0)
					dp[i][j] += 4*dp[x][y];
				y--;
				if(x >= 0 && y >= 0)
					dp[i][j] += dp[x][y];
			}
		if(N <= K)
		{
			ans = 0;
			for(int i = 0; i <= N; i++)
				ans += dp[N][i];
			printf("%lld\n", ans);
		}
		else
		{
			ind = K*K-1;
			ord = K*(K+1);
			for(int i = 0; i < ord; i++)
				for(int j = 0; j < ord; j++)
				{
					F[i][j] = i==j;
					G[i][j] = 0;
				}
			for(int i = 0; i <= K; i++)
				for(int j = 0; j <= 4 && j <= K-i; j++)
					G[i][ind-K*i+j] = factors[j];
			for(int i = K+1; i < ord; i++)
				G[i][i-K-1] = 1;
			e = N-K;
			while(e)
			{
				if(e&1)
					multiply(F, G);
				multiply(G, G);
				e >>= 1;
			}
			ans = 0;
			for(int i = 0; i <= K; i++)
			{
				ans2 = 0;
				for(int j = 0; j < ord; j++)
				{
					ans2 += ((ll)F[i][j])*dp[K-(j/(K+1))][K-(j%(K+1))];
					if(ans2 >= MOD2)
						ans2 -= MOD2;
				}
				ans += ans2;
				if(ans > MOD2)
					ans -= MOD2;	
			}
			printf("%lld\n", ans%MOD);
		}
	}
	return 0;
}
