// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shift Register
// Level: 8
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1949

#include<bits/stdc++.h>
#define uint unsigned int

using namespace std;

unordered_map<uint, int> Mapa, Mapa2;

struct matrix{int m[32][32];};
int ans[32][32];
int ind[32];
	
matrix matMul(matrix a, matrix b, int n){
    for (int i=0;i<n;i++) for (int j=0;j<n;j++){
        ans[i][j]=0;
        for (int k=0;k<n;k++) ans[i][j]=ans[i][j]+a.m[i][k]*b.m[k][j];
    }
    for (int i=0;i<n;i++) for (int j=0;j<n;j++) a.m[i][j] = ans[i][j]&1;
    return a;
}

matrix matPow(matrix base, int e, int n){
    matrix ans;
    for (int i=0;i<n;i++) for (int j=0;j<n;j++) ans.m[i][j]=(i==j);

    for (;e;base=matMul(base,base,n),e>>=1) if (e&1) ans=matMul(ans,base,n);

    return ans;
}

matrix am;

uint multiplyvet(uint x, int n)
{
	int i, j;
	int val[32];
	uint y = 1, z = 0, w;
	for(i = 0; i < n; i++)
	{
		val[n-1-i] = (x & y) > 0;
		y <<= 1;
	}
	for(i = 0; i < n; i++)
	{
		w = 0;
		for(j = 0; j < n; j++)
			w ^= (am.m[i][j] & val[j]) > 0;
		z = 2*z+w;
	}
	return z;
}

main()
{
	int n, m, i, j, f;
	uint x, y, z, a, b, c;
	while(scanf("%d %d", &n, &m) == 2 && (n||m))
	{
		Mapa = Mapa2;
		for(i = 0; i < n-1; i++)
			for(j = 0; j < n; j++)
				am.m[i][j] = (i+1)==j;
		for(j = 1; j < n; j++)
			am.m[n-1][j] = 0;
		for(i = 0; i < m; i++)
		{
			scanf("%d", &f);
			ind[i] = f;
			am.m[n-1][(n-f)%n] = 1;
		}
		z = ceil(sqrt(1LL<<n));
		am = matPow(am, z, n);
		scanf("%x %x", &a, &b);
		c = a;
		for(j = 1; j < z; j++)
		{
			x = 0;
			for(i = 0; i < m; i++)
				x ^= (c & (1LL << ind[i])) > 0;
			c = (c>>1) + (x<<(n-1));
			if(c == a)
				break;
			Mapa[c] = j;
		}
		if(j < z)
		{
			if(a == b)
				printf("0\n");
			else if(Mapa[b] == 0)
				printf("*\n");
			else
				printf("%u\n", Mapa[b]);
		}
		else
		{
			y = b;
			for(i = 0; i < z; i++)
			{
				if(Mapa[y] != 0)
				{
					printf("%u\n", z*i+Mapa[y]);
					break;
				}
				if(y == a)
				{
					printf("%u\n", z*i);
					break;
				}
				y = multiplyvet(y, n);
			}
			if(i == z)
				printf("*\n");
		}
	}
	return 0;
}
