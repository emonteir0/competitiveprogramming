// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: What is the Probability?
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1289

#include<stdio.h>
 
double pot(double p,int n)
{
    if(n==0)    return 1;
    if(n==1)    return p;
    if(n%2==0)  return pot(p,n/2)*pot(p,n/2);
    else return p*pot(p,n-1);
}
 
main()
{
    double p;
    int n,a,i;
    scanf("%d",&n);
    while(n--)
    {
        scanf("%d %lf %d",&a,&p,&i);
        if(p!=0)	printf("%.4lf\n",(pot((1-p),i-1)-pot((1-p),i))/(1-pot((1-p),a)));
        else printf("0.0000\n");
    }
    return 0;
}
