// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jaida and Multiply Game
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1697

#include<bits/stdc++.h>
#define MAX 20000000
#define ll unsigned long long int

using namespace std;


bool isprime[MAX+5],prime[MAX+5];
bitset<MAX+5> bits,bits2;
int prime_cont = 2, primou[1000001];


void primo()
{
	int i,j;
	prime[1] = true;
	primou[0] = 1;
    prime[2] = true;
    primou[1] = 2;
    memset(isprime, true, sizeof(isprime));
    for(i=3; i<=4473; i+=2) 
	if(isprime[i]) 
	{
        prime[i]=true;
        primou[prime_cont++]=i;
        for(j=i*i; j<=MAX; j+=i)
            isprime[j] = false;
    }
    for(;prime_cont!=1000000;i+=2)
    	if(isprime[i])
    	{
    		prime[i]=true;
    		primou[prime_cont++]=i;
    	}
}


main()
{
	int N,M,x,k,z;
	primo();
	z=primou[999999];
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&M);
		while((M--)>0)
		{
			scanf("%d",&x);
			if(x<=z)
				if(prime[x])
					bits.set(x);
		}
		k=0;
		while(1)
		{
			if(bits[primou[k]])
				k++;
			else
				break;
		}
		printf("%d\n",primou[k]-1);
		bits=bits2;
	}
	return 0;
}
