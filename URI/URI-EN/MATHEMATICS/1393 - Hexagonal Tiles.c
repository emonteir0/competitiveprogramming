// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hexagonal Tiles
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1393

#include <stdio.h>
 
void multiply(int F[2][2], int M[2][2]);
  
void power(int F[2][2], long long int n);
   
int fib(long long int n)
{
  int F[2][2] = {{1,1},{1,0}};
  power(F, n);
  return F[0][0];
}
 
void power(int F[2][2], long long int n)
{
  if( n == 0 || n == 1)
      return;
  int M[2][2] = {{1,1},{1,0}};
  power(F, n/2);
  multiply(F, F);
  if (n%2 != 0)
     multiply(F, M);
}
  
void multiply(int F[2][2], int M[2][2])
{
  int x =  (((F[0][0])*(M[0][0])) + ((F[0][1])*(M[1][0])));
  int y =  (((F[0][0])*(M[0][1])) + ((F[0][1])*(M[1][1])));
  int z =  (((F[1][0])*(M[0][0])) + ((F[1][1])*(M[1][0])));
  int w =  (((F[1][0])*(M[0][1])) + ((F[1][1])*(M[1][1])));
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
}
  
int main()
{
  long long int n;
  while(scanf("%lld",&n)&&n!=0)
  	printf("%d\n",fib(n));
  return 0;
}
