// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Xenlongão
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2597

#include<stdio.h>
#include<math.h>

main()
{
	int x;
	scanf("%*d");
	while(scanf("%d", &x) != EOF)
		printf("%.0lf\n", ceil(x - sqrt(x)));
	return 0;
}
