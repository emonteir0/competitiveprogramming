// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Star
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1233

#include <string.h>
#include <stdio.h>
#include <math.h>
#define ll unsigned long long int

bool isprime[55001];
ll prime[55000], prime_cont = 0;
ll lim = 55000;


int main() {
	ll n,z,div;
    int i,j;
    prime[prime_cont++] = 2;
	memset(isprime, true, sizeof(isprime));
	for(i=3; i<=lim; i+=2) if(isprime[i]) {
		prime[prime_cont++] = i;
		
		for(j=i*i; j<=lim; j+=i) {
			isprime[j] = false;
		}
	}
	while(scanf("%lld",&n)==1)
	{
		div=n;
        if(n%2==0)
        {
            div/=2;
            while(n%2==0)
                n/=2;
        }
        for(i=0;(i<prime_cont)&&(n!=1);i++)
        {
            z=prime[i];
            if(n%z==0)
            {
                if(n%z==0)
                {
                    div=(div*(z-1))/z;
                    while(n%z==0)
                        n/=z;
                }
            }
        }
        if(n>1)
            div=(div*(n-1))/n;
        printf("%lld\n",div/2);
	}
	return 0;
}
