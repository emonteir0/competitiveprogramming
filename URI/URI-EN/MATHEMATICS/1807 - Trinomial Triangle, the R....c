// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trinomial Triangle, the R...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1807

#include<stdio.h>
   
unsigned long long int pot(unsigned long long int a,int b)
{
    if(b==0)
        return 1;
    if(b%2==0)
    {
        unsigned long long int c=pot(a,b/2);
        return (c*c)%2147483647;
    }
    else
    {
        return (a*pot(a,b-1))%2147483647;
    }
}
   
   
main()
{
    int N;
    scanf("%d",&N);
    printf("%llu\n",pot(3,N));
    return 0;
}
