// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elections
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2084

#include<bits/stdc++.h>

using namespace std;

main()
{
	int i, N;
	double vet[11], v1, v2, acum;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%lf", &vet[i]);
		acum += vet[i];
	}
	sort(vet, vet+N);
	v1 = (vet[N-1])/acum * 100;
	v2 = (vet[N-1]-vet[N-2])/acum * 100;
	printf("%d\n", 2 - (v1 >= 45.000 || (v1 >= 40.000 && v2 >= 10.000))); 
}

