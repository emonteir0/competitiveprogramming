// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: A Game with Marbles
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1647

#include<stdio.h>


main()
{
	unsigned long long int acum,x;
	int n,y;
	while(scanf("%d",&n)==1&&n>0)
	{
		x=1;
		acum=0;
		while(n--)
		{
			scanf("%d",&y);
			acum+=x*y;
			x*=2;
		}
		printf("%llu\n",acum);
	}
	return 0;
}
