// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Full Sleigh
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2726

#include<bits/stdc++.h>
#define llu unsigned long long

using namespace std;

llu vet[64];
llu pot[64];
int bina[64];
int N, M;

llu memo[64][64][2];


llu solve(int x, int y, int maior)
{
	if(x == -1)
		return y == 0;
	if(memo[x][y][maior] != -1)
		return memo[x][y][maior];
	llu s = 0;
	s += solve(x-1, y, maior && bina[x] == 0);
	if(y && (!maior || bina[x]))
		s += solve(x-1, y-1, maior);
	return memo[x][y][maior] = s;
}

llu func(llu val)
{
	int i, j;
	for(i = 0; i <= N; i++)
		for(j = 0; j <= M; j++)
			memo[i][j][0] = memo[i][j][1] = -1;
	
	for(i = N; i--; )
	{
		if(val >= pot[i])
		{
			val -= pot[i];
			bina[i] = 1;
		}
		else
			bina[i] = 0;
	}
	return solve(N-1, M, 1);
}


main()
{
	int i, T;
	llu a, b, x, y;
	pot[0] = 1;
	for(i = 1; i < 60; i++)
		pot[i] = pot[i-1] << 1;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &M);
		x = 0;
		y = 0;
		for(i = 0; i < N; i++)
			scanf("%llu", &vet[i]);
		sort(vet, vet+N);
		scanf("%llu %llu", &a, &b);
		for(i = N; i--;)
		{
			if(a >= vet[i])
			{
				a -= vet[i];
				x += pot[i];
			}
		}
		x += (a != 0); 
		for(i = N; i--;)
		{
			if(b >= vet[i])
			{
				b -= vet[i];
				y += pot[i];
			}
		}
		if(x == 0)
			printf("%llu\n", func(y));
		else
		{
			//printf("%llu %llu\n", y, x-1);
			b = func(y);
			a = func(x-1);
			printf("%llu\n", b-a);
		}
	}
	return 0;
}
