// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Delaunay Triangulation
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1620

#include<stdio.h>


main()
{
	double a;
	while(scanf("%lf",&a)==1&&a>0)
	{
		printf("%.6lf\n",1-3.0/a);
	}
	return 0;
}
