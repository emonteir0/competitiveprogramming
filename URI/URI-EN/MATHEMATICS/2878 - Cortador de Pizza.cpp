// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cortador de Pizza
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2878

#include<bits/stdc++.h>
#define ll long long
#define fi first
#define se second

using namespace std;

set<int> cX, cY;
vector<int> vX, vY;

pair<int, int> X[100001], Y[100001];

int N, M;
int tree[100001];
int BITsize;

void update(int i, int val)
{
	++i;
	while (i <= BITsize)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

int idX(int x)
{
	return lower_bound(vX.begin(), vX.end(), x)-vX.begin();
}

int idY(int y)
{
	return lower_bound(vY.begin(), vY.end(), y)-vY.begin();
}

main()
{
	int x, y;
	ll ans = 1;
	scanf("%*d %*d %d %d", &N, &M);
	for(int i = 0; i < N; i++)
	{
		scanf("%d %d", &X[i].fi, &X[i].se);
		cX.insert(X[i].se);
	}
	for(int i = 0; i < M; i++)
	{
		scanf("%d %d", &Y[i].fi, &Y[i].se);
		cY.insert(Y[i].se);
	}
	vX.assign(cX.begin(), cX.end());
	vY.assign(cY.begin(), cY.end());
	BITsize = vX.size();
	sort(X, X+N);
	sort(Y, Y+M);
	for(int i = 0; i < N; i++)
	{
		x = idX(X[i].se);
		ans += 1 + sum(BITsize-1) - sum(x);
		update(x, 1);
	}
	for(int i = 0; i <= BITsize; i++)
		tree[i] = 0;
	BITsize = vY.size();
	for(int i = 0; i < M; i++)
	{
		y = idY(Y[i].se);
		ans += N + 1 + sum(BITsize-1) - sum(y);
		update(y, 1);
	}
	printf("%lld\n", ans);
	return 0;
}
