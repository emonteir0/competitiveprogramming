// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brothers
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1376

#include<stdio.h>

int mat[100][100][2];
int odio[100];
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

void printmat(int N, int M, int at)
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		printf("%d", mat[i][0][at]);
		for(j = 1; j < M; j++)
			printf(" %d", mat[i][j][at]);
		printf("\n");
	}
}

main()
{
	int i, j, k, l, T, N, M, K, v, at, ol, x, y;
	while(scanf("%d %d %d %d", &T, &N, &M, &K) == 4 && (T||N||M||K))
	{
		for(i = 0; i < T; i++)
			odio[i] = (i+1)%T;
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
				scanf("%d", &mat[i][j][0]);
		for(k = 1; k <= K; k++)
		{
			at = k&1;
			ol = !at;
			for(i = 0; i < N; i++)
				for(j = 0; j < M; j++)
					mat[i][j][at] = -1;
			for(i = 0; i < N; i++)
				for(j = 0; j < M; j++)
					for(l = 0; l < 4; l++)
					{
						x = i + dx[l];
						y = j + dy[l];
						if(x >= 0 && x < N && y >= 0 && y < M)
							if(odio[mat[i][j][ol]] == mat[x][y][ol])
							mat[x][y][at] = mat[i][j][ol]; 
					}
			for(i = 0; i < N; i++)
				for(j = 0; j < M; j++)
					if(mat[i][j][at] == -1)
						mat[i][j][at] = mat[i][j][ol];
		}
		printmat(N, M, at);
	}
	return 0;
}
