// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Angry Ducks
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1163

#include<bits/stdc++.h>
#define ld long double


ld G = 9.80665;
ld PI = 3.14159;

int main() 
{
	ld h, p1, p2, alpha, s, sx, sy, z, t, x;
	int N;
	while (scanf("%Lf %Lf %Lf", &h, &p1, &p2) == 3) 
	{
		scanf("%d", &N);
		while(N--) 
		{
			scanf("%Lf %Lf", &alpha, &s);
			alpha = alpha * PI / 180.0;
			sx = s * cos(alpha);
			sy = s * sin(alpha);
			z = G * h / (s * s);
			t = (s / G) * (sin(alpha) + sqrt(sin(alpha) * sin(alpha) + 2 * z));
			x = sx * t;
			printf("%.5Lf -> %s\n", x, (x >= p1 && x <= p2) ? "DUCK" : "NUCK");
		}
	}
	return 0;
}
