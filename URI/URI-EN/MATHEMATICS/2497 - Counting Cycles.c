// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Cycles
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2497

#include<stdio.h>


main()
{
	int N, K = 1;
	while(scanf("%d", &N) == 1 && (N != -1))
		printf("Experiment %d: %d full cycle(s)\n", K++, N/2);
	return 0;
}
