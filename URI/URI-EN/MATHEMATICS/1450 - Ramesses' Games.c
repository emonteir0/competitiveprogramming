// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ramesses' Games
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1450

#include<stdio.h>

int memo[16][3][32768];
int w[16], h[16], l[16];
int N;

int max(int a, int b)
{
	return (a>b)? a: b;
}

int solve(int xant, int rot, int bitsy)
{
	int ma = 0, a, b, i;
	if(bitsy == 32767)
		return 0;
	if(memo[xant][rot][bitsy] != -1)
		return memo[xant][rot][bitsy];
	if(rot == 0)
	{
		a = w[xant];
		b = h[xant];
	}
	if(rot == 1)
	{
		a = w[xant];
		b = l[xant];
	}
	if(rot == 2)
	{
		a = l[xant];
		b = h[xant];
	}
	for(i = 1; i <= N; i++)
	{
		if((bitsy & (1 << (i-1))) == 0)
		{
			if(((w[i] <= a) && (h[i] <= b)) || ((w[i] <= b) && (h[i] <= a)))
				ma = max(ma, l[i] + solve(i, 0, bitsy | (1 << (i-1))));
			if(((w[i] <= a) && (l[i] <= b)) || ((w[i] <= b) && (l[i] <= a)))
				ma = max(ma, h[i] + solve(i, 1, bitsy | (1 << (i-1))));
			if(((l[i] <= a) && (h[i] <= b)) || ((l[i] <= b) && (h[i] <= a)))
				ma = max(ma, w[i] + solve(i, 2, bitsy | (1 << (i-1))));
		}
	}
	return memo[xant][rot][bitsy] = ma;
	
}

main()
{
	int K, i, j, k;
	scanf("%d", &K);
	w[0]=h[0]=l[0]=99999999;
	while(K--)
	{
		scanf("%d", &N);
		for(i=1; i<=N; i++)
			scanf("%d %d %d", &w[i], &h[i], &l[i]);
		for(i = 0; i < 16; i++)
			for(j = 0; j < 3; j++)
				for(k = 0; k < 32768; k++)
				memo[i][j][k] = -1;
		printf("%d\n", solve(0, 0, 0));
	}
	return 0;
}

