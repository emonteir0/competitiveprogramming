// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hard Day At Work
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1831

#include<stdio.h>
#define ll long long int

ll mdc(ll  a, ll b, ll *x, ll *y) {
  ll xx, yy, d;
  if(b==0) {
    *x=1; *y=0;
    return a;
  }

  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}


ll inv(ll a, ll P){
  ll x,y,d;
  d = mdc(a,P,&x,&y);
  if(x<0)
    x = x+P;
  return x;
}

main()
{
	ll a, b, c, d, e, f, x;
	int k = 1;
	while(scanf("%lld %lld %lld %lld %lld %lld", &a, &b, &c, &d, &e, &f) == 6)
	{
		x = (a*d*f*inv(d*f,b) + c*b*f*inv(b*f,d) + e*b*d*inv(b*d,f)) % (b*d*f);
		printf("Caso #%d: %lld laranja(s)\n", k++, (x==0)? b*d*f:x);
	}
	return 0;
}
