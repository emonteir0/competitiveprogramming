# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Friends
# Level: 6
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1280

T = input()
for i in range(0, T):
	v0, v1 = raw_input().split(' ')
	if v0 == '0':
		print int(v1) + 1
	elif v0 == '1':
		print int(v1) + 2
	elif v0 == '2':
		print 2*int(v1) + 3
	elif v0 == '3':
		print 2**(int(v1)+3) - 3
	else:
		if v1 == '0':
			print 13
		elif v1 == '1':
			print 65533
		else:
			print 2**65536 - 3
		

