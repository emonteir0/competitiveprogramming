// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Biggest Number Game
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1829

#include<stdio.h>
#include<math.h>

char vet[1000],c;

main()
{
	double a,b,n;
	int N,i,cont=0;
	scanf("%d%*c",&N);
	for(i=0;i<N;i++)
	{
		scanf("%lf%*c%lf%*c",&a,&b);
		scanf("%lf%*c%*c",&n);
		c=((b*log(a))>(n*log(n)-n+log(n)/2+log(2*M_PI)/2));
		cont+=c;
		vet[i]=c;
	}
	
	if(cont==(N/2))
		printf("A competicao terminou empatada!\n");
	else if(cont>(N/2))
		printf("Campeao: Lucas!\n");
	else
		printf("Campeao: Pedro!\n");
	for(i=0;i<N;i++)
		printf("Rodada #%d: %s foi o vencedor\n",i+1,(vet[i]==1)?"Lucas":"Pedro");
	return 0;
}
