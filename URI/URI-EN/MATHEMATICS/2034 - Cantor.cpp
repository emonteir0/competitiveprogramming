// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cantor
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2034

#include<bits/stdc++.h>
#define ll long long

ll L, R;
int B;
char msg[10];
int vet[64], vet2[64];
int k1, k2;

ll memo[64][2][2], memo2[64][2][2]; 

ll solve(int a, int maior, int zero)
{
	int i;
	ll s = 0;
	if(a == -1)
		return 1;
	if(memo[a][maior][zero] != -1)
		return memo[a][maior][zero];
	if(zero)
		s += solve(a-1, 0, zero);
	for(i = zero; i < B; i++)
	{
		if(msg[i] == 'S')
		{
			if(maior && i > vet[a])
				break;
			s += solve(a-1, maior && (i == vet[a]), 0);		
		}
	}
	return memo[a][maior][zero] = s;
}

ll solve2(int a, int maior, int zero)
{
	int i;
	ll s = 0;
	if(a == -1)
		return 1;
	if(memo2[a][maior][zero] != -1)
		return memo2[a][maior][zero];
	if(zero)
		s += solve2(a-1, 0, zero);
	for(i = zero; i < B; i++)
	{
		if(msg[i] == 'S')
		{
			if(maior && i > vet2[a])
				break;
			s += solve2(a-1, maior && (i == vet2[a]), 0);		
		}
	}
	return memo2[a][maior][zero] = s;
}

main()
{
	int i;
	while(scanf("%lld %lld %d %s%*c", &L, &R, &B, msg) && L != -1)
	{
		k1 = 0;
		L--;
		while(L > 0)
		{
			vet[k1++] = L % B;
			L /= B;
		}
		k2 = 0;
		while(R > 0)
		{
			vet2[k2++] = R % B;
			R /= B;
		}
		k1--; k2--;
		for(i = 0; i <= k1; i++)
			memo[i][0][0] = memo[i][0][1] = memo[i][1][0] = memo[i][1][1] = -1;
		for(i = 0; i <= k2; i++)
			memo2[i][0][0] = memo2[i][0][1] = memo2[i][1][0] = memo2[i][1][1] = -1;
		printf("%lld\n", solve2(k2, 1, 1) - solve(k1, 1, 1));
	}
	return 0;
}
