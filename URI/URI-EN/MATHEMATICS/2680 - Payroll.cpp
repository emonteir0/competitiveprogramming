// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Payroll
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2680

#include<bits/stdc++.h>
#define ll long long
#define MAXN 10000
#define SQN 100

using namespace std;

int primos[MAXN+1], prime_cont = 0;
int isprime[MAXN+1];

void crivo()
{
	ll i, j;
	primos[prime_cont++] = 2;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= SQN; i += 2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			for(j = i*i; j <= MAXN; j+=i)
				isprime[j] = 1;
		}
	}
	for(; i <= MAXN; i += 2)
		if(!isprime[i])
			primos[prime_cont++] = i;
}

ll pot(ll x, int e)
{
	ll y;
	if(e == 1)
		return x;
	if(e&1)
		return x*pot(x, e-1);
	y = pot(x, e/2);
	return y*y;
}

main()
{
	int i, N, x, e;
	ll val;
	crivo();
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		val = 1;
		for(i = 0; i < prime_cont && x > 1; i++)
		{
			e = 0;
			if(x % primos[i] == 0)
			{
				while(x % primos[i] == 0)
				{
					x /= primos[i];
					e++;
				}
			}
			if(e)
				val *= (pot(primos[i], e+1)-1)/(primos[i]-1);
			//printf("%lld\n", val);
		}
		if(x > 1)
			val *= (x+1);
		printf("%lld\n", val);
	}
	return 0;
}
