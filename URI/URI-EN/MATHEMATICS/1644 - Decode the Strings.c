// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Decode the Strings
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1644

#include<stdio.h>
#include<string.h>

int N;
char vet[80];
char F[80][80], M[80][80], G[80][80];

void multiply();
void multiply2();
   
void power(int n)
{
	int i,j;
	if(n==0||n==1)
	  return;		
	power(n/2);
	multiply2();
	if(n%2==1)
	 multiply();
}
   
void multiply()
{
    int i,j,k;
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            G[i][j]=0;
            
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            for(k=0;k<N;k++)
				if((F[i][k]&&M[k][j])==1)
					G[i][j]=1;
  
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            F[i][j]=G[i][j];
}

void multiply2()
{
    int i,j,k;
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            G[i][j]=0;
            
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            for(k=0;k<N;k++)
				if((F[i][k]&&F[k][j])==1)
					G[i][j]=1;
                
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            F[i][j]=G[i][j];
}


main()
{
	int i,j,k,x;
	char str[81];
	while((scanf("%d %d%*c",&N,&k)==2)&&(N||k))
	{
		for(i=0;i<N;i++)
		{
			for(j=0;j<N;j++)
				F[i][j]=M[i][j]=0;
			scanf("%d%*c",&x);
			F[i][x-1]=M[i][x-1]=1;
		}
		gets(str);
		power(k);
		for(i=0;i<N;i++)
			for(j=0;j<N;j++)
				if(F[j][i]==1)
				{
					printf("%c",str[j]);
					break;
				}
		printf("\n");
	}
	return 0;
}

