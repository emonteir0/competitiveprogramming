// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kaprekar
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1785

#include<cstdio>
#include<cmath>
#include<algorithm>
 
using namespace std;
 
int cmp(int a, int b)
{
    return a<b;
}
 
int cmp2(int a, int b)
{
    return a>b;
}
 
int chamadas(int x)
{
    int n=0;
    while(x!=6174)
    {
        int a,b,c,d,ma,me,v1[4],v2[4],x1,x2;
        d=x%10;
        c=(x/10)%10;
        b=(x/100)%10;
        a=(x/1000);
        v1[0]=a;
        v1[1]=b;
        v1[2]=c;
        v1[3]=d;
        sort(v1,v1+4,cmp);
        v2[0]=a;
        v2[1]=b;
        v2[2]=c;
        v2[3]=d;
        sort(v2,v2+4,cmp2);
        x1=1000*v1[0]+100*v1[1]+10*v1[2]+v1[3];
        x2=1000*v2[0]+100*v2[1]+10*v2[2]+v2[3];
        x=abs(x1-x2);
        n++;
    }
        return n;
}
 
main()
{
    int i,n,a,b,c,d,x;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&x);
        d=x%10;
        c=(x/10)%10;
        b=(x/100)%10;
        a=(x/1000);
        if(a==b&&b==c&&c==d)
            printf("Caso #%d: -1\n",i);
        else
            printf("Caso #%d: %d\n",i,chamadas(x));
    }
    return 0;
}
