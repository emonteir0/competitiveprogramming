// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binary Lover
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1705

#include<stdio.h>
#define ll unsigned long long int

ll mat[13][2050];
int tam[13];

void mount()
{
	int i,j,k,n,cont;
	long long int val,val2;
	char x[13];
	for(i=1;i<4096;i++)
	{
		n=i;
		val=0;
		k=0;
		while(n!=0)
		{
			x[k++]=n%2;
			n/=2;
		}
		for(j=k-1;j>=0;j--)
			val=val*10+x[j];
		val2=val;
		cont=0;
		while(val2%10==0)
		{
			val2/=10;
			cont++;
		}
		mat[cont][tam[cont]++]=val;
	}
}

int pesq(ll chave, int i)
{
     int inf = 0;
     int sup = tam[i]-1;
     int meio;
     ll val;
     while (inf <= sup) 
     {
          meio = (inf + sup)/2;
          val=mat[i][meio];
          if (chave == val)
               return meio;
          if (chave < val)
               sup = meio-1;
          else
               inf = meio+1;
     }
     return meio;
}


main()
{
	ll n,y;
	int i,j,x,tamatual,ind,cont2,cont5;
	mount();
	while(scanf("%lld",&n)==1)
	{
		cont2=0;
		cont5=0;
		y=n;
		while(y%2==0)
		{
			y/=2;
			cont2++;
		}
		while(y%5==0)
		{
			y/=5;
			cont5++;
		}
		ind=(cont2>cont5)?cont2:cont5;
		j=pesq(n,ind);
		/*while(j>=0)
		{
			if(mat[ind][j]<n)
				break;
			j--;
		}
		j++;*/
		tamatual=tam[ind];
		for(i=j;i<tamatual;i++)
		{
			if(mat[ind][i]%n==0)
			{
				printf("%lld\n",mat[ind][i]);
				break;
			}
		}
		if(i==tamatual)
			printf("-1\n");
	}
	return 0;
}

