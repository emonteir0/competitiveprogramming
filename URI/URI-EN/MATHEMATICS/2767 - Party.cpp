// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Party
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2767

#include<bits/stdc++.h>
#define ll long long
using namespace std;

ll restoa[100001];
ll restob[100001];

main()
{
	int N, M, K, x;
	ll tot;
	while(scanf("%d %d %d", &N, &M, &K) == 3)
	{
		while(N--)
		{
			scanf("%d", &x);
			restoa[x%K]++;
		}
		while(M--)
		{
			scanf("%d", &x);
			restob[x%K]++;
		}
		tot = restoa[0]*restob[0];
		for(int i = 1; i < K; i++)
			tot += restoa[i]*restob[K-i];
		printf("%lld\n", tot);
		for(int i = 0; i < K; i++)
		{
			restoa[i] = 0;
			restob[i] = 0;
		}
	}
	return 0;
}
