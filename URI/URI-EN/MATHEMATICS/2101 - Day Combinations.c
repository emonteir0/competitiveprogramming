// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Day Combinations
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2101

#include <stdio.h>
#define ll long long int
#define MOD 1713371337

ll mdc(ll  a, ll b, ll *x, ll *y) {
  ll xx, yy, d;
  if(b==0)
  {
    *x=1; *y=0;
    return a;
  }
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}

ll mdc2(ll a, ll b)
{
	ll aux;
	while(b!=0)
	{
		aux=a%b;
		a=b;
		b=aux;
	}
	return a;
}

ll inv(ll a)
{
	ll x,y;
	mdc(a,MOD,&x,&y);
	if(x<0)
		x = x+MOD;
	return x;
}

main()
{
	ll n,m,z;
	while(scanf("%lld %lld",&n,&m)==2 && (n||m))
	{
		z=1;
		while(n%3==0 || m%3==0)
		{
			z*=3;
			if(n%3==0)
				n/=3;
			if(m%3==0)
				m/=3;
		}
		while(n%631==0 || m%631==0)
		{
			z*=631;
			if(n%631==0)
				n/=631;
			if(m%631==0)
				m/=631;
		}
		while(n%301703==0 || m%301703==0)
		{
			z*=301703;
			if(n%301703==0)
				n/=301703;
			if(m%301703==0)
				m/=301703;
		}
		printf("%lld\n",(((((n*m)%MOD)*inv(mdc2(n,m)))%MOD)*z)%MOD);
	}
	return 0;
}

