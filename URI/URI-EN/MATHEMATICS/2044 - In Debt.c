// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: In Debt
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2044

#include<stdio.h>

main()
{
	int n,acum,x,cont;
	while(scanf("%d",&n)==1 && n>0)
	{
		acum=0;
		cont=0;
		while(n--)
		{
			scanf("%d",&x);
			acum+=x;
			cont+=acum%100==0;
		}
		printf("%d\n",cont);
	}
	return 0;
}

