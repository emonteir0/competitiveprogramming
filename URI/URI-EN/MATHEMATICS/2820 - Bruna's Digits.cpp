// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bruna's Digits
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2820

#include<bits/stdc++.h>
#define ll unsigned long long

using namespace std;

int vet[10];

void solve(ll e, int N, int dig)
{
	int K = 0;
	int v[71];
	for(int i = 0; i < dig; i++)
		v[i] = vet[0];
	while(e > 0)
	{
		v[K++] = vet[e%N];
		e /= N;
	}
	for(int i = dig-1; i >= 0; i--)
		printf("%d", v[i]);
	printf("\n");
}

main()
{
	int T, N, dig;
	ll E, sz;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		sz = N;
		dig = 1;
		for(int i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		sort(vet, vet+N);
		scanf("%llu", &E);
		while(E > sz)
		{
			E -= sz;
			dig++;
			sz *= N;
		}
		solve(E-1, N, dig);
	}
	return 0;
}
