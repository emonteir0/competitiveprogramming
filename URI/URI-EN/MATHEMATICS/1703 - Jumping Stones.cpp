// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jumping Stones
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1703

#include<bits/stdc++.h>

using namespace std;

int main (){
    int t; cin >> t;
    while (t--){
        int n, k;
        cin >> n >> k;
        double ans = 0;
        for(int i = 1; i <= n; i++) if (k%i == 0) ans += (1./double(n));
        printf("%.12lf\n", ans);
    }
    return 0;
}
