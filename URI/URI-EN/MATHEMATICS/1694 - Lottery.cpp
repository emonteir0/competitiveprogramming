// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lottery
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1694

#include<cstdio>
#include<bits/stdc++.h>
 
using namespace std;
 
bitset<2502> isprime;
int prime[2502], prime_cont=0;
int lim = 2500;
   
void primo() {
  prime[prime_cont++] = 2;      
    isprime.set();
    for(int i=3; i<=lim; i+=2)
    {
        if(isprime[i])
        {
            prime[prime_cont++] = i;
              
            for(int j = i + i; j <= lim; j+= i)
                isprime.reset(j);
        }
    }
}
 
unsigned long long int pot(long long int a,int b)
{
    if(b==0)    return 1;
    if(b%2==0)
        return pot(a,b/2)*pot(a,b/2);
    return pot(a,b-1)*a;
}
 
unsigned long long int comb(int x, int y)
{
    unsigned long long int z=1;
    int i,l,f,t;
    for(i=0;i<16;i++)
    {
        l=x;
        f=prime[i];
        t=0;
        if(f>x)
            break;
        while(l/f)
        {
            t+=(l/f);
            l/=f;
        }
        l=x-y;
        while(l/f)
        {
            t-=(l/f);
            l/=f;
        }
        l=y;
        while(l/f)
        {
            t-=(l/f);
            l/=f;
        }
        z*=pot(f,t);
    }
    return z;
}
 
main()
{
    int i,z,y,a,b,c;
    unsigned long long int cont;
    int vet1[50],vet2[50];
    primo();
    while(scanf("%d %d %d",&a,&b,&c)==3&&(a||b||c))
    {
        for(i=0;i<((a>b)?a:b);i++)
            vet1[i]=vet2[i]=0;
        y=a*b-1;
        for(i=0;i<prime_cont;i++)
        {
            z=prime[i];
            if(z>y)
                break;
            vet1[z%b]++;
            vet2[z/b]++;
        }
        cont=0;
        if(a>=c)
        {
            for(i=0;i<b;i++)
            {
                z=a-vet1[i];
                if(z>=c)
                    cont+=comb(z,c);
            }
        }
        if(b>=c)
        {
            for(i=0;i<a;i++)
            {
                z=b-vet2[i];
                if(z>=c)
                    cont+=comb(z,c);
            }
        }
        if(c==1)
            cont/=2;
        printf("%llu\n",cont);
    }
    return 0;
}
