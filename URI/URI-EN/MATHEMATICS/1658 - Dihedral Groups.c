// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dihedral Groups
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1658

#include<stdio.h>

main()
{
	char ca, cb, dir;
	int n, x, cont;
	while(scanf("%d%*c", &n) == 1 && n)
	{
		dir = 1;
		cont = 0;
		while(scanf("%c", &ca) && (ca != '\n'))
		{
			scanf("%d%c", &x, &cb);
			if(ca == 'm')
				dir = (dir+x)&1;
			else
				cont = ((cont + (dir?x:-x))%n + n)%n;
			if(cb == '\n')
				break;
		}
		if(cont == 0 && dir == 1)
			printf("\n");
		else if(cont == 0 && dir == 0)
			printf("m1\n");
		else
		{
			if(dir == 0)
			{
				if(n-cont > cont)
					printf("r%d m1\n", cont);
				else
					printf("m1 r%d\n", n-cont);
			}
			else
			{
				if(n-cont + 2 > cont)
					printf("r%d\n", cont);
				else
					printf("m1 r%d m1\n", n-cont);
			}
		}
	}
	return 0;
}

