// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hashmat the Brave Warrior
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1198

#include<stdio.h>

main()
{
	long long int a,b;
	while(scanf("%lld %lld",&a,&b)==2)
		printf("%lld\n",(b-a)*((b-a)>0?1:-1));
	return 0;
}
