// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rio 2016
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2177

#include<stdio.h>
#include<math.h>

main()
{
	int a, b, c, d, e, f, k=0, i;
	scanf("%d %d %d", &a, &b, &c);
	for(i=1;i<=c;i++)
	{
		scanf("%d %d %d",&d,&e,&f);
		if(sqrt(pow(a-d,2)+pow(b-e,2))<f)
		{
			if(k++)
				printf(" ");
			printf("%d", i);
		}
	}
	if(k==0)
		printf("-1");
	printf("\n");
	return 0;
}

