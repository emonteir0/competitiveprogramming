// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bacilli Population Growth
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1380

#include<stdio.h>
#include<string.h>
#define ll unsigned long long int
 
void multiply(int F[2][2], int M[2][2],int m);
  
void power(int F[2][2], int n,int m);
  
int fib(int n,int m)
{
  int F[2][2] = {{1,1},{1,0}};
  power(F, n, m);
  return F[1][0];
}
 
void power(int F[2][2], int n, int m)
{
  if( n == 0 || n == 1)
      return;
  int M[2][2] = {{1,1},{1,0}};
  
  power(F, n/2, m);
  multiply(F, F, m);
  
  if (n%2 != 0)
     multiply(F, M, m);
}
  
void multiply(int F[2][2], int M[2][2], int m)
{
  int x =  (((ll)F[0][0])*M[0][0] + ((ll)F[0][1])*M[1][0])%m;
  int y =  (((ll)F[0][0])*M[0][1] + ((ll)F[0][1])*M[1][1])%m;
  int z =  (((ll)F[1][0])*M[0][0] + ((ll)F[1][1])*M[1][0])%m;
  int w =  (((ll)F[1][0])*M[0][1] + ((ll)F[1][1])*M[1][1])%m;
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
}
  
  
int main()
{
  int n,num=0,i,carry;
  char c,v[3],y;
  scanf("%d%*c",&n);
  while((n--)>0)
  {
    i=0;
    y=0;
    carry=0;
    v[0]=0;v[1]=0;v[2]=0;
    while(scanf("%c",&c)==1&&c!='\n')
    {
        y=(y+c-'0')%3;
        if(carry==1)
        {
            v[0]=v[1];
            v[1]=v[2];
            i=2;
        }
        v[(i++)%3]=c;
        if(i==3)
            carry=1;
        i%=3;
    }
    y=((y-v[0]-v[1]-v[2])%3+3)%3;
        num=v[0]-'0';
    if(v[1]!=0)
        num=10*num+v[1]-'0';
    if(v[2]!=0)
        num=10*num+v[2]-'0';
    num=(num+1000*y)%1500;
    if(num!=0)
        num=fib(num,1000);
    printf("%s%s%d\n",(num/100)?"":"0",(num/10)?"":"0",num);
  }
  return 0;
}

