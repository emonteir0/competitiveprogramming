// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: K-ésimo
// Level: 8
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2668

#include<stdio.h>
#define MOD 10000

int F[101][101], M[101][101], G[101][101];

void multiply();
void multiply2();
   
void power(int n)
{
	int i,j;
	if(n==0||n==1)
	  return;		
	power(n/2);
	multiply2();
	if(n%2==1)
	 multiply();
}
   
void multiply()
{
    int i,j,k;
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            G[i][j]=0;
            
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            for(k=0;k<2;k++)
                G[i][j] += F[i][k]*M[k][j];
                
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2()
{
    int i,j,k;
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            G[i][j]=0;
            
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            for(k=0;k<2;k++)
                G[i][j] += F[i][k]*F[k][j];
                
    for(i=0;i<2;i++)
        for(j=0;j<2;j++)
            F[i][j]=G[i][j]%MOD;
}
main()
{
	int A, B, N, K, x;
	scanf("%d %d %d %d", &A, &B, &N, &K);
	F[0][0] = M[0][0] = F[1][1] = M[1][1] = A%MOD;
	F[0][1] = M[0][1] = B%MOD;
	F[1][0] = M[1][0] = 1;
	power(N);
	x = (2*F[0][0] + (A*A > B || (A*A < B && N%2==0)) * 9999) % MOD;
	K--;
	while(K--)
		x /= 10;
	printf("%d\n", x%10);
	return 0;
}
