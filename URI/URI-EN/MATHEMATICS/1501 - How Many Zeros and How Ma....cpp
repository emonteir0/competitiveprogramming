// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How Many Zeros and How Ma...
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1501

#include<cstdio>
#include<cmath>
#include<map>
#include<set>
  
using namespace std;
  
int primos[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,
197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,
439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,
691,701,709,719,727,733,739,743,751,757,761,769,773,787,797};
  
map <int,int> Mapa1,Mapa2;
map <int,int>::iterator it;
  
main()
{
    long long int cont,mul,x,y,z,d,zeros,digitos;
    int a,b,i,aux;
    double c;
    while(scanf("%d %d",&a,&b)==2)
    {
        aux=b;
        i=0;
        while(aux>1)
        {
            cont=0;
            while(aux%primos[i]==0)
            {
                aux/=primos[i];
                cont++;
            }
            if(cont>0)
            {
                Mapa1[primos[i]]=cont;
            }
            i++;
        }
        zeros=0x7FFFFFFF;
        for(it=Mapa1.begin();it!=Mapa1.end();++it)
        {
            x=it->first;
            y=it->second;
            cont=0;
            mul=x;
            while(a>=mul)
            {     
                d=a/mul;
                cont+=d;
                mul*=x;
            }
            if(cont==0)
            {
                zeros=0;
                break;
            }
            else if(cont/y<zeros) zeros=cont/y;
        }
        
        if((a==2&&b==2)||(a==3&&b==6)||(a==4&&b==24)||(a==5&&b==120)||(a==6&&b==720))
            printf("1 2\n");
        else if(a>1)
        {
        	c=(a*log(a)-a+log(a)/2+log(2*M_PI)/2)/log(b);
        	digitos=(int)c + 1;
            printf("%lld %lld\n",zeros,digitos);
        }
        else
            printf("0 1\n");
        Mapa1=Mapa2;
    }
    return 0;
}
