// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dividers
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2238

#include<bits/stdc++.h>

using namespace std;

set<int> divisores;
set<int>::iterator it;

int primos[34000], prime_cont = 0;
int isprime[34000];
int v[34001], e[34001], t[34001];

void prime()
{
	int i, j;
	primos[prime_cont++] = 2;
	for(i = 4; i < 34000; i+= 2)
		isprime[i] = 1;
	for(i = 3; i < 34000; i+=2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			for(j = i*i; j < 34000; j += i)
				isprime[i] = 1;
		}
	}
}

int pot(int a, int b)
{
	int x = 1;
	while(b--)
		x *= a;
	return x;
}

main()
{
	int i, A, B, C, D, k = 0, x;
	prime();
	scanf("%d %d %d %d", &A, &B, &C, &D);
	for(i = 0; i < prime_cont && C>1; i++)
		if(C%primos[i] == 0)
		{
			v[k] = primos[i];
			while(C%primos[i] == 0)
			{
				t[k]++;
				C/=primos[i];
			}
			k++;
		}
	while(1)
	{
		e[0]++;
		for(i = 0; i < k; i++)
			if(e[i]>t[i])
			{
				e[i] = 0;
				e[i+1]++;
			}
		if(e[k]==1)
			break;
		x = 1;
		for(i = 0; i < k; i++)
			x*=pot(v[i], e[i]);
		divisores.insert(x);
	}
	for(it = divisores.begin(); it != divisores.end(); ++it)
	{
		x = *it;
		if(x%A == 0 && x%B != 0 && D%x != 0)
		{
			printf("%d\n", x);
			return 0;
		}
	}
	printf("-1\n");
	return 0;
}
