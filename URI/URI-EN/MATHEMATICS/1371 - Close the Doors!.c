// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Close the Doors!
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1371

#include<stdio.h>
 
int x(int n)
{
    int j;
    for(j=1;j<=5001;j++)
        if(j*j>n)
            return j-1;
}
 
main()
{
    int i,n,c;
    while(scanf("%d",&n)==1 && n)
    {
        c=x(n);
        for(i=1;i<c;i++)
            printf("%d ",i*i);
        printf("%d\n",c*c);
    }
    return 0;
}
