// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Game
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1400

#include<cstdio>
#define MAX 1000000

bool vet[MAX+2];

void calc()
{
	int i,j;
	for(i=0;i<=MAX;i+=7)
		vet[i]=true;
	for(i=7;i<=MAX;i+=10)
		vet[i]=true;
	for(i=70;i<=MAX;i+=100)
		for(j=0;j<10;j++)
			vet[i+j]=true;
	for(i=700;i<=MAX;i+=1000)
		for(j=0;j<100;j++)
			vet[i+j]=true;
	for(i=7000;i<=MAX;i+=10000)
		for(j=0;j<1000;j++)
			vet[i+j]=true;
	for(i=0;i<10000;i++)
		vet[70000+i]=true;
}

main()
{
	int a, b, c, iatual, natual, dir;
	calc();
	while((scanf("%d %d %d",&a,&b,&c)==3)&&(a||b||c))
	{
		iatual = 1;
		natual = 1;
		dir = 1;
		while(c!=0)
		{
			iatual+=dir;
			natual++;
			if((iatual==a) || (iatual==1))
				dir=-dir;
			if(iatual==b)
				if(vet[natual])
					c--;
		}
		printf("%d\n",natual);
	}
	return 0;
}

