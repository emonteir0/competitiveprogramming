// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factorial Sum
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1161

#include<stdio.h>


long long int fatoriais[]={1,1,2,6,24,120,720,5040,40320,362880,3628800,39916800,479001600,6227020800,87178291200,
1307674368000,20922789888000,355687428096000,6402373705728000,121645100408832000,2432902008176640000};

main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2)
	{
		printf("%lld\n",fatoriais[a]+fatoriais[b]);
	}
	return 0;
}
