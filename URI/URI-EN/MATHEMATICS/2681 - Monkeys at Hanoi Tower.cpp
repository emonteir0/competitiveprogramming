// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Monkeys at Hanoi Tower
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2681

#include<bits/stdc++.h>
#define MOD 24*60*60
#define REC 180

using namespace std;

set<int> conju;
int vet[182];

main()
{
	char msg[41];
	int i = 0, e = 0, x = 255, y, z;
	vet[0] = 127;
	while(1)
	{
		if(x == vet[0])
			break;
		vet[++i] = x;
		x = x*2+1;
		x %= MOD;
	}
	//printf("%d\n", i); //180
	x = 1;
	scanf("%s", msg);
	if(msg[1] == 0)
	{
		if(msg[0] == '1')
			printf("00:00:01\n");
		else if(msg[0] == '2')
			printf("00:00:03\n");
		else if(msg[0] == '3')
			printf("00:00:07\n");
		else if(msg[0] == '4')
			printf("00:00:15\n");
		else if(msg[0] == '5')
			printf("00:00:31\n");
		else if(msg[0] == '6')
			printf("00:01:03\n");
		else if(msg[0] == '7')
			printf("00:02:07\n");
		else if(msg[0] == '8')
			printf("00:04:15\n");
		else if(msg[0] == '9')
			printf("00:08:31\n");
	}
	else
	{
		for(i = 0; msg[i]; i++)
		{
			e = (10*e + msg[i] - '0');
			e %= REC;
		}
		e = e + REC - 7;
		e %= REC;
		x = vet[e];
		z = x % 60;
		x /= 60;
		y = x % 60;
		x /= 60;
		printf("%02d:%02d:%02d\n", x, y, z);
	}
	return 0;
}
