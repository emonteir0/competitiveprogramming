// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Code the Tree
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1735

#include<bits/stdc++.h>
#define ll long long

using namespace std;

vector<int> ans;

char msg[10000];
ll vet[64];

int isnum(char c)
{
	return c >= '0' && c <= '9';
}

int pos;

void solve(int x)
{
	int y;
	if(msg[pos] == 0)
		return;
	if(msg[pos] == ')')
		return;
	if(msg[pos] == '(')
	{
		if(isnum(msg[pos+2]))
		{
			y = 10*(msg[pos+1]-'0') + msg[pos+2] - '0';
			pos += 3;
		}
		else
		{
			y = msg[pos+1] -'0';
			pos += 2;
		}
		vet[y] |= (1LL << x);
		vet[x] |= (1LL << y);
		solve(y);
	}
	pos++;
	solve(x);
}

int val(ll x)
{
	int cont = 0;
	while(x > 1)
	{
		x >>= 1;
		cont++;
	}
	return cont;
}

main()
{
	int i, j, tam;
	while(gets(msg) != NULL)
	{
		ans.clear();
		pos = 0;
		solve(0);
		for(tam = 1; ; tam++)
		{
			if(!vet[tam])
				break;
			vet[tam] &= ~1LL;
		}
		tam--;
		if(tam == 1)
			printf("\n");
		else
		{
			while(1)
			{
				for(i = 1; i <= tam; i++)
				{
					if(__builtin_popcountll(vet[i]) == 1)
					{
						j = val(vet[i]);
						ans.push_back(j);
						vet[j] ^= 1LL << i;
						vet[i] = 0;
						break;
					}
				}
				if(i == tam+1)
					break;
			}
			printf("%d", ans[0]);
			for(i = 1; i < (int)ans.size(); i++)
				printf(" %d", ans[i]);
			printf("\n");
		}
	}
	return 0;
}
