// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Curo Factories
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1505

#include<stdio.h>

int vis[5000];
int G[5000], C[5000], A[5000];

int max(int a, int b)
{
	return (a > b) ? a : b;
}

main()
{
	int N, i, j, best = 0, acum, ciclo, cont, add, voltas, atual;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
		scanf("%d", &G[i]);
	for(i = 0; i < N; i++)
		scanf("%d", &C[i]);
	for(i = 0; i < N; i++)
		scanf("%d", &A[i]);
	for(j = 0; j < N; j++)
	{
		for(i = 0; i < N; i++)
			vis[i] = 0;
		cont = acum = 0;
		atual = A[j];
		i = j;
		while(1)
		{
			if(vis[i] == 1)
				break;
			vis[i] = 1;
			acum += C[i];
			cont++;
			if(acum >= atual)
				break;
			i = G[i];
		}
		if(acum >= atual)
			best = max(best, cont);
		else
		{
			ciclo = 0;
			add = 0;
			while(1)
			{
				if(vis[i] == 0)
					break;
				vis[i] = 0;
				acum += C[i];
				add += C[i];
				ciclo++;
				cont++;
				if(acum >= atual)
					break;
				i = G[i];
			}
			if(acum >= atual)
				best = max(best, cont);
			else
			{
				voltas = (atual - acum) / add;
				cont += ciclo * voltas;
				acum += voltas * add;
				if(acum < atual)
					while(1)
					{
						acum += C[i];
						cont++;
						if(acum >= atual)
							break;
						i = G[i];
					}
				best = max(best, cont);
			}
		}
	}
	printf("%d\n", best);
	return 0;
} 
