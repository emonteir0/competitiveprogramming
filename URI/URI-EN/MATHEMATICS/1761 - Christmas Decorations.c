// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Decorations
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1761

#include<stdio.h>
#include<math.h>

main()
{
	double a,b,c;
	while(scanf("%lf %lf %lf",&a,&b,&c)==3)
	{
		printf("%.2f\n",5*(tan(a*M_PI/180)*(b)+c));
	}
	return 0;
}
