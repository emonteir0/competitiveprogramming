// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Time Room
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1754

#include<stdio.h>

main()
{
	int N;
	long long int a,b,x;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%lld %lld",&a,&b);
		x=(a+(b-a%b)%b)/b;
		if(a<=b)
		x=2;
		printf("%lld\n",x);
	}
	return 0;
}
