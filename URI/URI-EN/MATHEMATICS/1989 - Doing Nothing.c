// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Doing Nothing
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1989

#define ll unsigned long long int
#include <stdio.h>

main()
{
	ll x,n,m,acum,acum2;
	while(scanf("%llu %llu",&n,&m)==2&&(n!=-1)&&(m!=-1))
	{
		acum=0;
		acum2=0;
		while(n--)
		{
			scanf("%llu",&x);
			acum+=acum2+(x*m);
			acum2+=(x*m);
		}
		printf("%llu\n",acum);
	}
	return 0;
}
