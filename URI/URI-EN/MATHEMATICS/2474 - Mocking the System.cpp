// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mocking the System
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2474

#include<bits/stdc++.h>
#define MAX 100000
#define ll long long

using namespace std;

set<ll> conjunto;

int primos[MAX+1], prime_count = 0;
char isprime[MAX+1];


void primo()
{
	int i, j;
	primos[prime_count++] = 2;
	conjunto.insert(2);
	for(i = 4; i <= MAX; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= MAX; i += 2)
	{
		if(!isprime[i])
		{
			primos[prime_count++] = i;
			conjunto.insert(i);
			for(j = i+i; j <= MAX; j += i)
				isprime[j] = 1;
		}
	}
}

main()
{
	int i;
	ll N;
	primo();
	while(scanf("%lld", &N) == 1)
	{
		if(N == 2)
			printf("1\n");
		else
		{
			if((N&1) == 0)
				printf("%lld\n", N-2);
			else
			{
				if(conjunto.find(N) != conjunto.end())
					printf("%lld\n", N-1);
				else
				{
					for(i = 0; i < prime_count; i++)
						if(N%primos[i] == 0)
							break;
					if(i == prime_count)
					{
						conjunto.insert(N);
						printf("%lld\n", N-1);
					}
					else
					{
						N -= 2;
						if(conjunto.find(N) != conjunto.end())
							printf("%lld\n", N);
						else
						{
							for(i = 0; i < prime_count; i++)
								if(N%primos[i] == 0)
									break;
							if(i == prime_count)
							{
								conjunto.insert(N);
								printf("%lld\n", N);
							}
							else
								printf("%lld\n", N-1);
						}
					}
				}
			}
		}
	}
	return 0;
}

