// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tiles
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1512

#include<stdio.h>




main()
{
	long long int n,a,b,y,x,a1,b1,cont1,cont2,cont3;
	while(scanf("%lld %lld %lld",&n,&a,&b)==3&&(n||a||b))
	{
		cont1=0;
		cont2=0;
		cont3=0;
		y=n-n%a;
		x=a;
		if(y>=x)
		{
			cont1=(y-x)/a+1;
		}
		y=n-n%b;
		x=b;
		if(y>=x)
		{
			cont2=(y-x)/b+1;
		}
		a1=a;
		b1=b;
		while(b1!=0)
		{
			y=b1;
			b1=a1%b1;
			a1=y;
		}
		a=a*b/a1;
		y=n-n%a;
		x=a;
		if(y>=x)
		{
			cont3=(y-x)/a+1;
		}
		printf("%lld\n",cont1+cont2-cont3);
	}
	return 0;
}
