// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Lottery
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1352

#include<bits/stdc++.h>
#define ll long long
using namespace std;

main()
{
	int N;
	ll sum;
	double P;
	while(scanf("%d %lf", &N, &P) == 2 && N)
	{
		sum = 0;
		for(int i = 1; i <= N; i++)	
			for(int j = 1; j < i; j++)
				sum += j*j;
		printf("%.2lf\n", P*(((ll)N*N*(N*N-1)*(N*N-2)*(N*N-3))/sum)/2400.0);
	}
	return 0;
}
