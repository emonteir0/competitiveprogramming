// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fast Prime Number
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1221

#include<stdio.h>
 
char primo(int x)
{
    int i;
    if (x==1) return 0;
    for(i=2;i*i<=x;i++)
        if(x%i==0) return 0;
    return 1;
}
 
 
main()
{
    int n,x;
    scanf("%d",&n);
    while(n--)
    {
        scanf("%d",&x);
        printf("%s\n",primo(x)?"Prime":"Not Prime");
    }
    return 0;
}
