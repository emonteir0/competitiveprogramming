// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stop The Bomb!
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2204

#include<stdio.h>
#include<string.h>

main()
{
    char num1[105], num2[105];
    int n;
    scanf("%d%*c", &n);
    while(n--)
    {
        scanf("%s %s%*c", num1, num2);
        printf("%s\n",strcmp(num1, num2) == 0? num1: "1");
    }
    return 0;
}
