// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flavius Josephus Reloaded
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1660

#include<stdio.h>

long long int gcd(long long int a, long long int b) {
	long long int remainder;
	while (b != 0) {
		remainder = a % b;
		a = b;
		b = remainder;
	}
	return a;
}

long long int abs(long long int x)
{
	if(x<0)	return -x;
	return x;
}

int main () {
	long long int a,b,n,x,y,cont;
	while(scanf("%lld",&n)==1&&(n!=0))
	{
		scanf("%lld %lld",&a,&b);
		x=0;
		y=(a*((b*b)%n)%n+b)%n;
		while (x!=y) 
		{
			x = ((a*((x*x)%n))%n+b)%n;
			y = ((a*((y*y)%n))%n+b)%n;
			y = ((a*((y*y)%n))%n+b)%n;
		}
		cont=1;
		y = ((a*((x*x)%n))%n+b)%n;;
		while(x!=y)
		{
			y = ((a*((y*y)%n))%n+b)%n;
			cont++;
		}
		printf("%lld\n",n-cont);
	}
	return 0;
}
