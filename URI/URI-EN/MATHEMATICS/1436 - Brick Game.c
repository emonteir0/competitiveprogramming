// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Brick Game
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1436

#include<stdio.h>
#include<stdlib.h>

int cmp(const void* a, const void* b)
{
	return (int*)a-(int*)b;
}

int vet[11];

main()
{
	int N, n, i,k;
	scanf("%d",&N);
	for(k=1;k<=N;k++)
	{
		scanf("%d",&n);
		for(i=0;i<n;i++)
		{
			scanf("%d",&vet[i]);
		}
		qsort(vet,n,sizeof(int),cmp);
		printf("Case %d: %d\n",k,vet[n/2]);
	}
	return 0;
}
