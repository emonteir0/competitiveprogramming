// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Undiscovered Country
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1968

#include <stdio.h>
#include <math.h>
#define MOD 1000000007

long long int vet[10000001];

void crivo(int b)
{
	int i,j,max=sqrt(b);
	for(i=2;i<=max;i++)
	{
		for(j=i+i;j<=b;j+=i)
		{
			if(i*i<j)
			vet[j]+=2;
			if(i*i==j)
			vet[j]+=1;
		}
	}
}

unsigned long long int pot(unsigned long long int a,int b)
{
    if(b==0)    return 1;
    if(b%2==0)
        return (pot(a,b/2)*pot(a,b/2))%MOD;
    return (pot(a,b-1)*a)%MOD;
}

main()
{
	int i,a,b,n,cont=0;
	long long int x=1;
	scanf("%d %d %d",&a,&b,&n);
	n-=2;
	vet[1]=-1;
	crivo(b);
	for(i=a;i<=b;i++)
		cont+=(vet[i]==n);
	x=pot(cont,b-a+1-cont);
	printf("%lld\n",x);
	return 0;
}
