// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lunar Alignment
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2514

#include<stdio.h>

int gcd(int a, int b)
{
	if(b == 0)
		return a;
	return gcd(b, a%b);
}

int lcm(int a, int b)
{
	return (a/gcd(a, b)) * b;
}

main()
{
	int a, b, c, d, m;
	while(scanf("%d %d %d %d", &m, &a, &b, &c) == 4)
	{
		d = lcm(a, b);
		printf("%d\n", lcm(d, c) - m);
	}
	return 0;
}
