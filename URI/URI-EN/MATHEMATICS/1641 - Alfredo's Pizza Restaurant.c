// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alfredo's Pizza Restaurant
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1641

#include<stdio.h>

main()
{
	int k=0,r,w,l;
	while(scanf("%d",&r)==1&&r>0)
	{
		scanf("%d %d",&w,&l);
		printf("Pizza %d %s on the table.\n",++k,(4*r*r>=l*l+w*w)?"fits":"does not fit");
	}
	return 0;
}
