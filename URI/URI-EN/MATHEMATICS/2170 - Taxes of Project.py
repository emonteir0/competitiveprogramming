# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Taxes of Project
# Level: 1
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2170

k = 0

while True:
    try:
        [a, b] = raw_input().split(' ')
        a = float(a)
        b = float(b)
        k += 1
        print 'Projeto %d:\nPercentual dos juros da aplicacao: %.2f %%\n' % (k, 100*(float(b)/float(a)-1))
    except:
        break
