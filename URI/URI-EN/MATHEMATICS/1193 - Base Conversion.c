// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Base Conversion
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1193

#include<stdio.h>
#include<string.h>

int convert(char *v, int a)
{
	int i,num=0;
	for(i=0;i<strlen(v);i++)
		num=num*a+v[i]-'0';
	return num;
}


void printhex(int a)
{
	char v[17]={"0123456789abcdef"},b[8],i=0,j;
	while(a!=0)
	{
		b[i]=v[a%16];
		a/=16;
		i++;
	}
	for(j=i-1;j>=0;j--)
		printf("%c",b[j]);
	printf(" hex\n");
}

void printbin(int a)
{
	int j,i=0;
	char b[32];
	while(a!=0)
	{
		
		b[i]=a%2+'0';
		a/=2;
		i++;
	}
	for(j=i-1;j>=0;j--)
		printf("%c",b[j]);
	printf(" bin\n");
}


main()
{
	int a,num,i,j;
	char b[33],c[4];
	scanf("%d",&a);
	for(i=1;i<=a;i++)
	{
		scanf("%s %s%*c",b,c);
		printf("Case %d:\n",i);
		if(strcmp(c,"bin")==0)
		{
			num=convert(b,2);
			printf("%d dec\n",num);
			printhex(num);
		}
		if(strcmp(c,"dec")==0)
		{
			num=convert(b,10);
			printhex(num);
			printbin(num);
		}
		if(strcmp(c,"hex")==0)
		{
			for(j=0;j<strlen(b);j++)
			{
				if(b[j]>='a'&&b[j]<='f')
					b[j]=b[j]-'a'+'0'+10;
			}
			num=convert(b,16);
			printf("%d dec\n",num);
			printbin(num);
		}
		printf("\n");
	}
	return 0;
}
