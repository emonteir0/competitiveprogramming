// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Inês Venezuela's Square T...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2069

#include<stdio.h>
#define ll long long int

int primos[31624], prime_count=0;
char bolean[31624];

void prime()
{
	int i, j;
	primos[prime_count++] = 2;
	for(i=3;i<=178;i+=2)
	{
		if(!bolean[i])
		{
			primos[prime_count++] = i;
			for(j=i*i; j<=31623; j+=i)
				bolean[j] = true;
		}
	}
	for(;i<=31623;i++)
	{
		if(!bolean[i])
			primos[prime_count++] = i;
	}
}


int mdc(int a, int b)
{
	int aux;
	while(b!=0)
	{
		aux=a%b;
		a=b;
		b=aux;
	}
	return a;
}

main()
{
	int x, y, z, i, u, a, aux;
	char b;
	prime();
	scanf("%d %d",&x,&y);
	z = mdc(x,y);
	aux=z;
	a=z;
	i=0;
	while(aux>1&&i<prime_count)
	{
		b=0;
		u=primos[i];
		while(aux%u==0)
		{
			aux/=u;
			b=!b;
			if(b==0)
				a/=u;
		}
		i++;
	}
	printf("%d\n",a);
	return 0;
}

