// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Marianne and The Twin Cou...
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1926

#include<cstdio>
#include<cstring>
#define MAX 1000005

bool vet[MAX];
int primos[MAX],tamanhos[MAX],primecount,geminicount;

void primes()
{
	int i,j;
	memset(vet,true,MAX);
	primos[primecount++]=2;
	for(i=3;i<=1000;i+=2)
		if(vet[i])
		{
			primos[primecount++]=i;
			for(j=i*i;j<MAX;j+=i)
				vet[j]=false;
		}
	for(;i<MAX;i+=2)
		if(vet[i])
			primos[primecount++]=i;
}

void geminiprimes()
{
	int i,x;
	for(i=1;i<primecount-1;i++)
	{
		x=primos[i];
		if(((x-primos[i-1])==2)||((primos[i+1]-x)==2))
			tamanhos[x]=1;
	}
}

void preencher()
{
	int i;
	for(i=3;i<=1000000;i++)
		tamanhos[i]+=tamanhos[i-1];
}

main()
{
	int i,n,a,b,aux,x,y;
	primes();
	geminiprimes();
	preencher();
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&a,&b);
		x=tamanhos[a];
		y=tamanhos[b];
		if(a<b)
			printf("%d\n",y-x+(x==tamanhos[a-1]+1));
		else
			printf("%d\n",x-y+(y==tamanhos[b-1]+1));
	}	
	return 0;
}
