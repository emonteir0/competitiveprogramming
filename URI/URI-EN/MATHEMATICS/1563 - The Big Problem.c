// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Big Problem
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1563

#include<stdio.h>
#include<math.h>
#define ll unsigned long long int

ll mdc(ll a, ll b)
{
	ll r;
	while(b!=0)
	{
		r=a%b;
		a=b;
		b=r;
	}
	return a;
}


main()
{
	int n,i,sqr,x;
	ll sum,m,n2,n3;
	while(scanf("%d",&n)==1)
	{
		sum=0;
		sqr=floor(sqrt(n));
		for(i=1;i<=sqr;i++)
		{
			x=n/i;
			sum+=((ll)i)*(x-sqr)+((x+1)*((ll)x))/2;
		}
		n2=((ll)n)*n;
		n3=n2-sum;
		m=mdc(n3,n2);
		printf("%lld/%lld\n",n3/m,n2/m);
	}
	return 0;
}
