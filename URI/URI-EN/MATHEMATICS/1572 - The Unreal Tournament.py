# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: The Unreal Tournament
# Level: 4
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1572

dp = [ [ 0 for i in range(1001) ] for j in range(1001) ]
dp[0][0] = -1.0
for i in range(1,1001):
	dp[0][i] = 1.0
	dp[i][0] = 0.0
k = 0
while(True):
	p,n = raw_input().split(' ')
	p = float(p)
	if n == '0':
		break
	if k == 1:
		print ""
	k = 1
	n = int(n)
	q = 1 - p
	for i in range(1,1001):
		for j in range(1,1001):
			dp[i][j] = p * dp[i-1][j] + q * dp[i][j-1]
	while n > 0:
		n-=1
		a, b = raw_input().split(' ')
		a = int(a)
		b = int(b)
		print "%.5f" % dp[a][b]
		ans = 1
		for i in range(b+1,a+b+1):
			ans *= i
		for i in range(1,a+1):
			ans /= i
		print 2*(ans-1)

