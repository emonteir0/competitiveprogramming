// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Above Average
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1214

#include<stdio.h>

main()
{
	int n,n2,i,vet[1000],d,acum;
	double cont;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&n2);
		acum=0;
		cont=0;
		for(i=0;i<n2;i++)
		{
			scanf("%d",&d);
			vet[i]=n2*d;
			acum+=d;
		}
		for(i=0;i<n2;i++)
			if(vet[i]>acum)
				cont++;
		printf("%.3lf%%\n",cont*100/n2);
	}
	return 0;
}
