// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Retrieval
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2135

#include<stdio.h>

int vet[100];

main()
{
	int i, n, k=1, acum;
	while(scanf("%d",&n)==1)
	{
		acum = 0;
		for(i=0;i<n;i++)
		{
			scanf("%d",&vet[i]);
			if(vet[i] == acum)
				break;
			else
				acum += vet[i];
		}
		if(i < n)
		{
			printf("Instancia %d\n%d\n\n", k++, vet[i++]);
			for(;i<n;i++)
				scanf("%*d");
		}
		else
			printf("Instancia %d\nnao achei\n\n", k++);
		
	}
	return 0;
}

