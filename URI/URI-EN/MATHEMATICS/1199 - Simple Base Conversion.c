// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Simple Base Conversion
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1199

#include<stdio.h>
#include<string.h>

long long int hexa(char *v)
{
	long long int num=0;
	int i;
	for(i=1;i<strlen(v);i++)
	{
		if(v[i]>='a'&&v[i]<='f')
			v[i]-='a'-'0'-10;
		if(v[i]>='A'&&v[i]<='F')
			v[i]-='A'-'0'-10;
	}
	for(i=2;i<strlen(v);i++)
		num=num*16+v[i]-'0';
	return num;
}

long long int dec(char *v)
{
	long long int num=0;
	int i;
	for(i=0;i<strlen(v);i++)
		num=num*10+v[i]-'0';
	return num;
}

void printhex(long long int a)
{
    char v[17]={"0123456789ABCDEF"},b[8],i=0,j;
    if(a==0)
    {
    	strcpy(b,"0");
    	i=1;
   	}
    while(a!=0)
    {
        b[i]=v[a%16];
        a/=16;
        i++;
    }
    printf("0x");
    for(j=i-1;j>=0;j--)
        printf("%c",b[j]);
    printf("\n");
}

main()
{
	char v[11];
	long long int num;
	while(scanf("%s",v)==1&&v[0]!='-')
	{
		if(v[1]=='x')
		{
			num=hexa(v);
			printf("%lld\n",num);
		}
		else
		{
			num=dec(v);
			printhex(num);
		}
	}
	return 0;
}
