// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: White Nights Festival
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1202

#include<stdio.h>
#include<string.h>

int fib[1500];
char vet[10001];

main()
{
	int cont,n,i,N,num;
	fib[0]=0;
	fib[1]=1;
	for(i=2;i<1500;i++)
	{
		fib[i]=(fib[i-1]+fib[i-2])%1000;
	}
	scanf("%d",&n);
	while(n--)
	{
		scanf("%s",vet);
		N=strlen(vet)-1;
		num=0;
		cont=1;
		for(i=N;i>=0;i--)
		{
			num=(num+((vet[i]-'0')*cont)%1500)%1500;
			cont=(cont*2)%1500;
		}
		num=fib[num];
		printf("%s%s%d\n",num<100?"0":"",num<10?"0":"",num);
	}
	return 0;
}
