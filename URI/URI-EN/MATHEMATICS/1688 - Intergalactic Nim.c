// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Intergalactic Nim
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1688

#include<stdio.h>
#define ll long long int
#define MAX 9223372036854775807

ll xorsum(ll n)
{
	return (n >> 1) & 1 ^ (n&1 ? 1 : n); 
}

ll absol(ll x)
{
	if(x < 0)
		return -x;
	return x;
}

main()
{
	int K;
	ll N, x, v, a, b;
	while(scanf("%lld %d", &N, &K) == 2)
	{
		x = xorsum(N);
		while(K--)
		{
			scanf("%lld %lld %lld", &a, &b, &v);
			printf("%s\n", ( x ^ xorsum(b) ^ xorsum(a-1) ^ xorsum(v+a-1) ^ xorsum(v+b)) == 0? "HAN": "JABBA");
		}
	}
	return 0;
}
