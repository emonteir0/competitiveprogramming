// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Drunk People?
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1815

#include<bits/stdc++.h>

using namespace std;

double P[2][100];
double Mat[100][100];

main()
{
	int ind, N, T, K, M, inst = 1;
	double sum;
	while(scanf("%d", &N) == 1 && N)
	{
		scanf("%d %d %d", &T, &K, &M);
		T--;
		K--;
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
				scanf("%lf", &Mat[i][j]);
			P[0][i] = 0;
		}
		P[0][T] = 1;
		sum = 0;
		if(T == K)
			printf("1.000000\n");
		else
		{
			for(int i = 0; i < M; i++)
			{
				ind = i&1;
				for(int i = 0; i < N; i++)
					P[1-ind][i] = 0;
				for(int i = 0; i < N; i++)
				{
					for(int j = 0; j < N; j++)
						P[1-ind][j] += Mat[i][j]*P[ind][i];
				}
				sum += P[1-ind][K];
				if(P[1-ind][K] == 1)
					break;
				P[1-ind][K] = 0;
			}
			printf("Instancia %d\n%.6lf\n\n", inst++, 1-sum);
		}
	}
	return 0;
}
