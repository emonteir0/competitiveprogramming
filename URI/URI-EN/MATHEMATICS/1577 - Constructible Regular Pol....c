// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Constructible Regular Pol...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1577

#include<stdio.h>

int fermat[]={3,5,17,257,65537};

main()
{
	int N,i,x,cont,cont2;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&x);
		if(x>2)
		{
			while(x%2==0)
				x/=2;
			for(i=0;i<5;i++)
			{
				if(x%fermat[i]==0)
					x/=fermat[i];
			}	
			if(x==1)
				printf("Yes\n");
			else
				printf("No\n");
		}
		else	printf("No\n");
	}
	return 0;
}
