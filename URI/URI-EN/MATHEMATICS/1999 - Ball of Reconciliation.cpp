// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ball of Reconciliation
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1999

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N,x,y,z,max_y,cont;
	while(scanf("%d",&N)==1&&(N!=-1))
	{
		cont=0;
		for(z=1;z<=N;z++)
		{
			max_y=min(z,N/z);
			for(y=1;y<=max_y;y++)
			{
				if((N-z*y)%(z+y)==0)
				{
					x=(N-z*y)/(z+y);
					if(x<=y)
						cont++;
				}
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
