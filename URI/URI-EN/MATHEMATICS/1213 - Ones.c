// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ones
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1213

#include<stdio.h>
 
 
main()
{
    int n,cont,cont2,val;
    while(scanf("%d",&n)==1)
    {
        cont = 1;
        cont2 = 1;
        val=1;
        while(cont2%n!=0)
        {
            val=(val*10%n)%n;
            cont2=(cont2+val)%n;
            cont++;
        }
        printf("%d\n",cont);
    }
    return 0;
}
