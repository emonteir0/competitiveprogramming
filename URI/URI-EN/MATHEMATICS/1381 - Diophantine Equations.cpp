// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Diophantine Equations
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1381

#include<cstdio>
#include<bits/stdc++.h>
#define MOD 1300031 
 
using namespace std;
 
bitset<2000002> isprime;
int prime[2000002], prime_cont=0;
int lim = 2000000;
   
void primo() {
  prime[prime_cont++] = 2;     
    isprime.set();
    for(int i=3; i<=lim; i+=2)
    {
        if(isprime[i])
        {
            prime[prime_cont++] = i;
             
            for(int j = i + i; j <= lim; j+= i)
                isprime.reset(j);
        }
    }
}
 
unsigned long long int pot(unsigned long long int a,int b)
{
    if(b==0)    return 1;
    if(b%2==0)
        return (pot(a,b/2)*pot(a,b/2))%MOD;
    return (pot(a,b-1)*a)%MOD;
}
 
unsigned long long int comb(int x, int y)
{
    unsigned long long int z=1;
    int i,l,f,t;
    for(i=0;i<prime_cont;i++)
    {
        l=x;
        f=prime[i];
        t=0;
        if(f>x)
            break;
        while(l/f)
        {
            t+=(l/f);
            l/=f;
        }
        l=x-y;
        while(l/f)
        {
            t-=(l/f);
            l/=f;
        }
        l=y;
        while(l/f)
        {
            t-=(l/f);
            l/=f;
        }
        z=(z*pot(f,t))%MOD;
    }
    return z;
}
 
main()
{
   int n,a,b;
    primo();
    scanf("%d",&n);
    while(n--)
    {
       scanf("%d %d",&a,&b);
       printf("%llu\n",comb(a+b-1,b));
    }
    return 0;
}
