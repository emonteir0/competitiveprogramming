// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stakes
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1630

#include<stdio.h>

int mdc(int a,int b)
{
	int res;
	while(b!=0)
	{
		res=a%b;
		a=b;
		b=res;
	}
	return a;
}

main()
{
	int x,y;
	while(scanf("%d %d",&x,&y)==2)
		printf("%d\n",2*(x+y)/mdc(x,y));
	return 0;
}
