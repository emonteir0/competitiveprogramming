// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Uncle Phill Bonati's ...
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2149

#include<stdio.h>

main()
{
	unsigned long long int vet[17], i;
	vet[0] = 0;
	vet[1] = 1;
	for(i = 2; i < 17; i++)
	{
		if(i % 2 == 0)
			vet[i] = vet[i-1] + vet[i-2];
		else
			vet[i] = vet[i-1] * vet[i-2];
	}
	while(scanf("%d",&i)==1)
		printf("%lld\n",vet[i-1]);
	return 0;
}

