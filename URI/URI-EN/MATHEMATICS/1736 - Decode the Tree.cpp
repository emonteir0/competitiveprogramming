// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Decode the Tree
// Level: 7
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1736

#include<bits/stdc++.h>
#include <fstream>
#include <queue>
#include <string>
#include <strstream>
#include <vector>

using namespace std;

vector<int> G[51];
int freq[51];
set<int> folhas, folhas2;
int k;
int vis[51];
vector<int> leituras;

void dfs(int x)
{
	int v;
	printf("(%d", x);
	vis[x] = 1;
	for(int i = 0; i < G[x].size(); i++)
	{
		v = G[x][i];
		if(!vis[v])
		{
			printf(" ");
			dfs(v);
		}
	}
	printf(")");
}

main()
{
	string line;
	int x, op;
	while (getline (cin, line))
	{
		folhas = folhas2;
		leituras.clear();
   		istrstream lstr (line.c_str());
    	while(lstr >> x)
    	{
      		leituras.push_back(x);
      		freq[x]++;
      	}
      	k = leituras.size();
		for(int i = 1; i <= k+1; i++)
			if(freq[i] == 0)
				folhas.insert(i);
		for(int i = 0; i < k; i++)
		{
			x = *folhas.begin();
			G[leituras[i]].push_back(x);
			G[x].push_back(leituras[i]);
			folhas.erase(x);
			freq[leituras[i]]--;
			if(freq[leituras[i]] == 0)
				folhas.insert(leituras[i]);
		}
		dfs(k+1);
		printf("\n");
		for(int i = 1; i <= k+1; i++)
		{
			freq[i] = 0;
			vis[i] = 0;
			G[i].clear();
		}
		k = 0;
	}
	return 0;
}
