// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Running
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2516

#include<stdio.h>

main()
{
    int a, b, c;
    while(scanf("%d %d %d", &a, &b, &c) == 3)
    {
        if(c >= b)
            printf("impossivel\n");
        else
            printf("%.2lf\n", ((double)a)/(b-c));
    }
    return 0;
}
