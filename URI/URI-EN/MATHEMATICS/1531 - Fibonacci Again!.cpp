// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibonacci Again!
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1531

#include<bits/stdc++.h>
#define ll long long
#define MAXN 1000000
#define SQMAX 1000
using namespace std;
 
vector<int> divisores;
int prime_cont, isprime[MAXN+2], prime[MAXN+2];
int pn[MAXN+2];
 
int F[2][2], M[2][2];
   
void primo()
{
    int i, j;
    prime[prime_cont++] = 2;
    for(i = 4; i <= MAXN; i += 2)
        isprime[i] = 1;
    for(i = 3; i <= SQMAX; i += 2)
    {
        if(!isprime[i])
        {
            prime[prime_cont++] = i;
            for(j = i * i; j <= MAXN; j+= i)
                isprime[j] = 1;
        }
    }
    for(; i <= MAXN; i += 2)
    {
    	if(!isprime[i])
    		prime[prime_cont++] = i;
    }
}
 
int pot(int a, int b)
{
    if(b == 0)
        return 1;
    if(b&1)
		return (pot(a,b-1)*a);
	int x = pot(a,b/2);
    return x*x;
}
 
int mmc(int a, int b)
{
    return b*(a/__gcd(a,b));  
}

void multiply(int F[2][2], int M[2][2], int m)
{
  int x =  (((ll)F[0][0])*M[0][0] + ((ll)F[0][1]*M[1][0]))%m;
  int y =  (((ll)F[0][0])*M[0][1] + ((ll)F[0][1]*M[1][1]))%m;
  int z =  (((ll)F[1][0])*M[0][0] + ((ll)F[1][1]*M[1][0]))%m;
  int w =  (((ll)F[1][0])*M[0][1] + ((ll)F[1][1]*M[1][1]))%m;
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
}

int fib(int n, int m)
{
	F[0][0] = F[1][1] = 1;
	F[1][0] = F[0][1] = 0;
	
	M[0][0] = M[0][1] = M[1][0] = 1;
	M[1][1] = 0;
	while(n)
	{
		if(n & 1)
			multiply(F, M, m);
		multiply(M, M, m);
		n >>= 1;
	}
	return (F[1][0]) % m;
}

 
void divisors(int n)
{
    divisores.clear();
    for(int i = 1; i*i <= n; i++)
	{
		if(n % i == 0)
			divisores.push_back(i);
		if(i*i != n)
			divisores.push_back(n/i);
	}
	sort(divisores.begin(), divisores.end());
}
 
 
int pisano(int x)
{
    if(pn[x])
        return pn[x];
    else
    {
        if(!isprime[x])
        {
            if((x%10 == 1) || (x%10 == 9))
            {
                divisors(x-1);
                for(int i = 0; i < (int)divisores.size(); i++)
                {
                    fib(divisores[i], x);
                    if((F[0][0] == 1) && (F[1][0]==0))
                    {
                        return pn[x] = divisores[i];
                    }
                }
            }
            else
            {
                divisors(2*x+2);
                for(int i = 0; i < (int)divisores.size(); i++)
                {
                    fib(divisores[i], x);
                    if((F[0][0] == 1) && (F[1][0] == 0))
                        return pn[x] = divisores[i];
                }
            }
        }
        else
        {
            int n = x, k, d = 1;
            for(int i = 0; (i < prime_cont) && (n != 1); i++)
            {
                k = 0;
                while(n % prime[i] == 0)
                {
                    n /= prime[i];
                    k++;
                }
                if(k)
                {
                    if(pn[prime[i]] == 0)
                        pisano(prime[i]);
                    d=mmc(d, pn[prime[i]] * pot(prime[i],k-1));
                }
            }
            pn[x] = d;
        }   
    }
    return pn[x];
}

int main()
{
    int n,m;
    primo();
    pn[1] = 1;
    pn[2] = 3;
    pn[5] = 20;
    while(scanf("%d %d", &n, &m) == 2)
        printf("%d\n", fib(fib(n, pisano(m)), m));
    return 0;
}
