// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Flecha no Coelho
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2489

#include<stdio.h>
#include<math.h>

main()
{
	double A, D, R;
	while(scanf("%lf %lf %lf", &A, &D, &R) == 3)
		printf("%.4lf\n", A - D/tan(R*M_PI/180));
	return 0;
}
