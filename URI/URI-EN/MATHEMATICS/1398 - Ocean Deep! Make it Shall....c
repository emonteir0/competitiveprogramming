// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ocean Deep! Make it Shall...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1398

#include<stdio.h>
#include<string.h>

main()
{
	int n,resto,i;
	char vet[100001],vet2[1001];
	while(gets(vet)!=NULL)
	{
		n=strlen(vet);
		while(vet[n-1]!='#')
		{
			gets(vet2);
			strcat(vet,vet2);
			n=strlen(vet);
		}
		resto=0;
		for(i=0;i<n-1;i++)
			resto=(2*resto+vet[i]-'0')%131071;
		printf("%s\n",(resto==0)?"YES":"NO");
	}
	return 0;
}
