// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Colourful Flowers
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1219

#include<stdio.h>
#include<math.h>

main()
{
	double a,b,c,area,p,circulo1,circulo2;
	while(scanf("%lf %lf %lf",&a,&b,&c)==3)
	{
		p=(a+b+c)/2;
		area=sqrt(p*(p-a)*(p-b)*(p-c));
		circulo1=(area/p)*(area/p)*M_PI;
		circulo2=a*b*c/(4*area);
		circulo2=circulo2*circulo2*M_PI-area;
		area-=circulo1;
		printf("%.4lf %.4lf %.4lf\n",circulo2,area,circulo1);
	}
	return 0;
}
