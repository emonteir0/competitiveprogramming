// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: A Day in Math-Land
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1570

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll P, Q;

ll func(ll x)
{
	if((3*Q+P)*x < x*x)
		return 1;
	if(((unsigned ll)Q*Q + x*x) == (3*Q+P)*x-x*x)
		return 0;
	if(((unsigned ll)Q*Q + x*x) > (3*Q+P)*x-x*x)
		return 1;
	return -1;
}

ll bbc(ll lo, ll hi)
{
	ll mid;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		//printf("%lld %lld\n", mid, func(mid));
		if(func(mid) == 0)
			return mid;
		else if(func(mid) < 0)
			hi = mid-1;
		else
			lo = mid+1;
	}
	return -1;
}

ll bbu(ll lo, ll hi)
{
	ll mid;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		if(func(mid) == 0)
			return mid;
		else if(func(mid) < 0)
			lo = mid+1;
		else
			hi = mid-1;
	}
	return -1;
}

ll raiz(ll x)
{
	ll lo = 0, hi = sqrt(x)+3, mid;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		if(mid*mid == x)
			return mid;
		else if(mid*mid > x)
			hi = mid-1;
		else
			lo = mid+1;
	}
	return -1;
}

bool valido(ll x)
{
	if(Q%x != 0)
		return 0;
	return x >= (x-Q/x);
}

main()
{
	int t = 1, f;
	vector<ll> pos;
	ll a, b, yver, xver, s;
	scanf("%*d");
	while(scanf("%lld %lld", &P, &Q) == 2)
	{
		pos.clear();
		printf("Case %d:\n", t++);
		if(Q != 0)
		{
			xver = (3*Q+P)/4;
			a = bbc(0, xver);
			b = bbu(xver+1, (1LL<<31)-1);
			if(a != -1)
			{
				s = raiz(a);
				if(s != -1)
				{
					pos.push_back(s);
					pos.push_back(-s);
				}
			}
			if(b != -1)
			{
				s = raiz(b);
				if(s != -1)
				{
					pos.push_back(s);
					pos.push_back(-s);
				}
			}
			sort(pos.begin(), pos.end());
			f = 1;
			for(int i = 0; i < pos.size(); i++)
			{
				if(pos[i] == 0)
				{
					if(raiz(P) && (Q == 0))
					{
						printf("0 %lld\n", -raiz(P));
						f = 0;
						break;
					}
				}
				else if(valido(pos[i]))
				{
					printf("%lld %lld\n", pos[i], pos[i]-Q/pos[i]);
					f = 0;
					break;
				}
			}
			if(f)
				printf("Impossible.\n");
		}
		else
		{
			if((P%2 == 0))
			{
				s = raiz(P/2);
				if(s != -1)
				{
					pos.push_back(s);
					pos.push_back(-s);
				}
			}
			s = raiz(P);
			if(s != -1)
				pos.push_back(0);
			sort(pos.begin(), pos.end());
			f = 1;
			for(int i = 0; i < pos.size(); i++)
			{
				if(pos[i] == 0)
				{
					printf("0 %lld\n", -raiz(P));
					f = 0;
					break;
				}
				else
				{
					printf("%lld %lld\n", pos[i], pos[i]);
					f = 0;
					break;
				}
			}
			if(f)
				printf("Impossible.\n");
			
		}
	}
	return 0;
}
