// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Blobs
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1170

#include<stdio.h>

main()
{
	int cont,n;
	float x;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%f",&x);
		cont=0;
		while(x>1)
		{
			x/=2;
			cont++;
		}
		printf("%d dias\n",cont);
	}
	return 0;
}
