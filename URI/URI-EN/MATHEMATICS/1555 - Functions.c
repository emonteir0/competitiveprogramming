// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Functions
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1555

#include<stdio.h>

main()
{
	int N,x,y,a,b,c;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d %d",&x,&y);
		a=9*x*x+y*y;
		b=2*x*x+25*y*y;
		c=-100*x+y*y*y;
		if(a>b&&a>c)
			printf("Rafael ganhou\n");
		if(b>a&&b>c)
			printf("Beto ganhou\n");
		if(c>a&&c>b)
			printf("Carlos ganhou\n");
	}
	return 0;
}
