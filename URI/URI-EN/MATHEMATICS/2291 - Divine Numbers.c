// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Divine Numbers
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2291

#include<stdio.h>
#include<math.h>
#define ll long long int


main()
{
	int n,i,sqr,x;
	ll sum,m,n2,n3;
	while(scanf("%d",&n)==1&&n)
	{
		sum=0;
		sqr=floor(sqrt(n));
		for(i=1;i<=sqr;i++)
		{
			x=n/i;
			sum+=((ll)i)*(x-sqr)+((x+1)*((ll)x))/2;
		}
		printf("%lld\n",sum);
	}
	return 0;
}
