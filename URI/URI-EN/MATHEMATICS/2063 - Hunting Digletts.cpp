// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hunting Digletts
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2063

#include<stdio.h>

int vet[101];
bool ok[101];

int mdc(int a, int b)
{
	int aux;
	while(b!=0)
	{
		aux = a%b;
		a = b;
		b = aux;
	}
	return a;
}

main()
{
	int i,j,max=1,cont,n;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d",&vet[i]);
	for(i=1;i<=n;i++)
	{
		if(!ok[i])
		{
			ok[i] = true;
			j=vet[i];
			cont=1;
			while(j!=i)
			{
				ok[j] = true;
				j=vet[j];
				cont++;
			}
			max = (max*cont)/mdc(max,cont);
		}
	}
	printf("%d\n",max);
	return 0;
}

