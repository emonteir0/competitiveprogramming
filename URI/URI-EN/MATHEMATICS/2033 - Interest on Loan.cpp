// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Interest on Loan
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2033

#include<stdio.h>
#include<math.h>

long double absol(long double x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	long double a,b,r1,r2;
	int c;
	while(scanf("%Lf",&a)==1)
	{
		scanf("%Lf %d",&b,&c);
		r1=a*b*c;
		r2=a;
		b+=1;
		while(c--)
			r2*=b;
		r2-=a;
		printf("DIFERENCA DE VALOR = %.2Lf\nJUROS SIMPLES = %.2Lf\nJUROS COMPOSTO = %.2Lf\n",absol(r1-r2),r1,r2);
	}
	return 0;
}
