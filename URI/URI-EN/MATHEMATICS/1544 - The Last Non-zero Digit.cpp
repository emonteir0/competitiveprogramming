// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Last Non-zero Digit
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1544

#include<bits/stdc++.h>

int inv[10] = {0, 1, 0, 7, 0, 0, 0, 3, 0, 9};
int fat[20000001], cont2[20000001], cont5[20000001], dois[20000001];

main()
{
	int last = 1, cnt2 = 0, cnt5 = 0, val, N, M;
	dois[0] = 1;
	dois[1] = 2;
	fat[0] = 1;
	fat[1] = 1;
	for(int i = 2; i <= 20000000; i++)
	{
		val = i;
		while(val % 2 == 0)
		{
			val /= 2;
			cnt2++;
		}
		while(val % 5 == 0)
		{
			val /= 5;
			cnt5++;
		}
		last = (last*val)%10;
		fat[i] = last;
		cont2[i] = cnt2;
		cont5[i] = cnt5;
		dois[i] = (dois[i-1]*2)%10;
	}
	while(scanf("%d %d", &N, &M) == 2)
	{
		cnt2 = cont2[N]-cont2[N-M];
		cnt5 = cont5[N]-cont5[N-M];
		printf("%d\n", (fat[N]*inv[fat[N-M]]*(cnt2 < cnt5 ? 5: dois[cnt2-cnt5]))%10);
	}
	return 0;
}

