// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Factorial
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2129

using namespace std;
#include <bits/stdc++.h>
 
#define ll long long
 
int dois[1000001];
int ans[1000001];

int main() {
    int n, inst = 1;
    dois[0] = 1;
    ans[1] = 1;
    for(int i = 1; i <= 1000000; i++) dois[i] = (dois[i-1]*2)%10;
    int cnt  = 0;
    int last = 1;
    for(int i = 2; i <= 1000000; i++){
        int v = i;
        while(v%2 == 0) v/=2,cnt++;
        while(v%5 == 0) v/=5,cnt--;
        last = (last*v)%10;  
        ans[i] = (last*dois[cnt])%10;
    }
    while(scanf("%d", &n) == 1)
	{
        printf("Instancia %d\n",inst++);
        printf("%d\n", ans[n]);
        puts("");
    }
 
}
