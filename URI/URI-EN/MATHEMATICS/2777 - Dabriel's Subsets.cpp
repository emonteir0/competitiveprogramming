// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel's Subsets
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2777

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007LL

using namespace std;

ll A[3][3], B[3][3], C[3][3];

void multiply(ll F[3][3], ll M[3][3])
{
    int i, j, k;
    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            C[i][j] = 0;
            
    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            for(k = 0; k < 3; k++)
                C[i][j] += (F[i][k]*M[k][j])%MOD;

    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            F[i][j] = C[i][j]%MOD;
}


void powermatrix(ll n)
{
	if(n == 0 || n == 1)
		return;
    powermatrix(n/2);
    multiply(A, A);
  	if(n&1)
     	multiply(A, B);
}

main()
{
	ll N, sum = 0;
	while(scanf("%lld", &N) == 1)
	{
		if(N == 1)
			printf("1\n");
		else if(N <= 3)
			printf("2\n");
		else
		{
			A[0][0] = B[0][0] = 0;
			A[0][1] = B[0][1] = 1;
			A[0][2] = B[0][2] = 1;
			A[1][0] = B[1][0] = 1;
			A[1][1] = B[1][1] = 0;
			A[1][2] = B[1][2] = 0;
			A[2][0] = B[2][0] = 0;
			A[2][1] = B[2][1] = 1;
			A[2][2] = B[2][2] = 0;
			powermatrix(N-3);
			sum = (A[0][0] + A[0][1] + A[0][2] + A[1][0] + A[1][1] + A[1][2] ) % MOD;
			printf("%lld\n", sum);
		}
	}
	return 0;
}
