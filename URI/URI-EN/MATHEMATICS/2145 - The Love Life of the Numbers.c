// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Love Life of the Numbers
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2145

#include<stdio.h>
#include<math.h>
#define ll unsigned long long int

ll soma(int n)
{
	int i, x, sqr;
	ll sum = 0;
	sqr=floor(sqrt(n));
	for(i=1;i<=sqr;i++)
	{
		x=n/i;
		sum+=((ll)i)*(x-sqr)+((x+1)*((ll)x))/2;
	}
	sqr=floor(sqrt(n-1));
	for(i=1;i<=sqr;i++)
	{
		x=(n-1)/i;
		sum-=((ll)i)*(x-sqr)+((x+1)*((ll)x))/2;
	}
	return sum-n;
}


main()
{
	int a, b;
	ll c, d;
	while(scanf("%d %d",&a,&b)==2 && (a||b))
	{
		c = soma(a);
		d = soma(b);
		if(a%d == 0 && b%c == 0)
			printf("Friends and lovers <3\n");
		else if(c == d)
			printf("Almost lovers!\n");
		else if(a%d == 0)
			printf("%d friendzoned %d!\n", a, b);
		else if(b%c == 0)
			printf("%d friendzoned %d!\n", b, a);
		else
			printf("No connection.\n");
	}
	return 0;
}

