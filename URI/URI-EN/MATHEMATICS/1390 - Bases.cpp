// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bases
// Level: 7
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1390

#include<bits/stdc++.h>
#define ll long long int

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

using namespace std;

queue<string> fnum;
queue<char> fop;

typedef struct
{
	int expr[34];
	int grau;
}poly;

void printpol(poly pol)
{
	int i;
	for(i=0;i<=pol.grau;i++)
		printf("%d ",pol.expr[i]);
	printf("\n");
}

void criar(poly* pol,char* v)
{
	int n=0,i=0;
	strrev(v);
	while(v[i]!=0)
	{
		pol->expr[i]=v[i]-'0';
		i++;
	}
	pol->grau=i-1;
}

void sum(poly *pol, poly* temp, int q)
{
	int i,n,n2;
	n=pol->grau;
	n2=temp->grau;
	if(n2>n)
	{
		for(i=0;i<=n;i++)
			pol->expr[i]+=q*temp->expr[i];
		for(;i<=n2;i++)
			pol->expr[i]=q*temp->expr[i];
		pol->grau=n2;
	}
	else
	{
		for(i=0;i<=n2;i++)
			pol->expr[i]+=q*temp->expr[i];
	}
	temp->grau=0;
}

void multiply(poly *pol, poly *temp)
{
	int i,j,n,m;
	int c[34];
	n=pol->grau;
	m=temp->grau;
	for (i=0;i<=n+m;i++)
		c[i]=0;
	for (i=0;i<=n;i++)
		for (j=0;j<=m;j++)
			c[i+j]=c[i+j]+(pol->expr[i])*(temp->expr[j]);
	for(i=0;i<=n+m;i++)
		pol->expr[i]=c[i];
	pol->grau=n+m;
	temp->grau=0;
}

void consertargrau(poly *pol)
{
	int i;
	for(i=pol->grau;i>=1;i--)
	{
		if(pol->expr[i]==0)
			pol->grau--;
		else
			break;
	}
}

ll mdc(ll a, ll b)
{
	ll r;
	while(b!=0)
	{
		r=a%b;
		a=b;
		b=r;
	}
	return a;
}

ll absol(ll x)
{
	if(x<0)
		return -x;
	return x;
}

int sig(ll x)
{
	if(x>0)	return 1;
	if(x<0)	return -1;
	return 0;
}

main()
{
	int n,k,i,q,j,bmin;
	char v[36],c;	
	poly pol,temp1,temp2;
	string a,b;
	while(gets(v)!=NULL && (strcmp(v,"=")!=0))
	{
		a=v;
		k=0;
		bmin='1';
		n=a.length();
		for(i=0;i<n;i++)
		{
			c=a[i];
			if((c>='0')&&(c<='9'))
			{
				v[k++]=c;
				if(c>bmin)
					bmin=c;
			}
			else
			{
				v[k]=0;
				b=v;
				k=0;
				fnum.push(b);
				fop.push(a[i]);
			}
		}
		v[k]=0;
		b=v;
		q=1;
		fnum.push(b);
		bmin-='0'-1;
		criar(&pol,"0");
		while(!fnum.empty())
		{
			b=fnum.front();
			fnum.pop();
			strcpy(v,b.c_str());
			criar(&temp1,v);
			if(!fop.empty())
			{
				c=fop.front();
				fop.pop();
				if(c=='+')
					sum(&pol,&temp1,q);
				else if(c=='=')
				{
					sum(&pol,&temp1,q);
					q=-1;
				}
				else
				{
					while(c=='*')
					{
						b=fnum.front();
						fnum.pop();
						strcpy(v,b.c_str());
						criar(&temp2,v);
						multiply(&temp1,&temp2);
						if(fop.empty())
							break;
						c=fop.front();
						fop.pop();
					}
					if(c=='+')
						sum(&pol,&temp1,q);
					else
					{
						sum(&pol,&temp1,q);
						q=-1;
					}
				}
			}
			else
				sum(&pol,&temp1,q);
		}
		consertargrau(&pol);
		n=pol.grau;
		if(n==0)
		{
			if(pol.expr[0]==0)
				printf("%d+\n",bmin);
			else
				printf("*\n");
		}
		else if(n==1)
		{
			register ll r;
			if(pol.expr[0]==0)
				printf("*\n");
			else
			{
				if(sig(pol.expr[0])!=sig(pol.expr[1]))
				{
					if(pol.expr[0]%pol.expr[1]==0)
						r=-pol.expr[0]/pol.expr[1];
					if(r>=bmin)	printf("%lld\n",r);
					else	printf("*\n");
				}
			}
		}
		else if (pol.grau < 7)
		{
			register char val=0;
			i=0;
			while(pol.expr[i]==0)
				i++;
			register ll r,u=absol(pol.expr[i])/mdc(absol(pol.expr[i]),absol(pol.expr[n]));
			for(j=bmin;(j<=u)&&(j<=100000);j++)
			{
				if(u%j==0)
				{
					r=0;
					for(i=n;i>=0;i--)
					{
						r*=j;
						r+=pol.expr[i];
					}
					if(r==0)
					{
						if(val==0){	printf("%d",j);val=1;}
						else printf(" %d",j);
					}
				}
			}
			if(val==0)
				printf("*");
			printf("\n");
		}
		else
			printf("*\n");
	}
	return 0;
}

