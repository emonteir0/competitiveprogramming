// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pyramid
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2873

#include<bits/stdc++.h>

main()
{
	int A, B, C, D;
	while(scanf("%d %d %d %d", &A, &B, &C, &D) == 4 && A)
		printf("%.5lf\n", ((A+2*B)*C)/(2.0*D));
	return 0;
}
