// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Boxes of Chocolates
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1830

#include<stdio.h>
#include<math.h>


main()
{
	double a,b,c;
	int k=1;
	while(scanf("%lf %lf %lf",&a,&b,&c)==3&&!((a==0)&&(b==0)&&(c==0)))
		printf("Caso #%d: %.0lf Especial, %.0lf Predileta e %.0lf Sortida\n",k++,round((29*a-b-c)/3.0),round((-33*a+9*b-3*c)/3.0),round(2*a-2*b+2*c)); 
	return 0;
}
