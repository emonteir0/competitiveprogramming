// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Digit Root
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2859

#include<bits/stdc++.h>

using namespace std;

vector<int> vet[9];

char str[100001];
char str2[100001];

int func(int x)
{
	int ans = 0;
	while(x >= 10)
	{
		ans = 0;
		while(x > 0)
		{
			ans += x%10;
			x /= 10;
		}
		x = ans;
	}
	return x;
}

main()
{
	int acum = 0, exp = 0, x;
	vet[1] = {1};
	vet[2] = {1, 2, 4, 8, 7, 5};
	vet[4] = {1, 4, 7};
	vet[5] = {1, 5, 7, 8, 4, 2};
	vet[7] = {1, 7, 4};
	vet[8] = {1, 8};
	scanf("%s", str);
	scanf("%s", str2);
	for(int i = 0; str[i]; i++)
		acum += str[i]-'0';
	x = func(acum);
	if(x%3 == 0)
	{
		if(str2[1] == 0)
		{
			printf("%d\n", str2[0] == '1' ? x : 9);
		}
		else
		{
			printf("9\n");
		}
	}
	else
	{
		exp = 0;
		for(int i = 0; str2[i]; i++)
			exp = (exp + str2[i]-'0')% vet[x].size();
		printf("%d\n", vet[x][exp]);
	}
	return 0;
}
