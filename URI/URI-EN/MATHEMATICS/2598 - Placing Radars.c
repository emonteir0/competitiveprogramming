// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Placing Radars
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2598

#include<stdio.h>
#include<math.h>

main()
{
	int x, y;
	scanf("%*d");
	while(scanf("%d %d", &x, &y) != EOF)
		printf("%d\n", (x+y-1)/y);
	return 0;
}
