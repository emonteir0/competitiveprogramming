// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How Tall is It?
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2864

#include<bits/stdc++.h>

using namespace std;

main()
{
	int T, A, B, C, delta;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d %d", &A, &B, &C);
		delta = 4*A*C-B*B;
		printf("%.2lf\n", delta/(4.0*A));
	}
	return 0;
}
