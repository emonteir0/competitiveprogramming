// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Chamber of Secrets
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1839

#define MOD 1000000007
#define ll long long int
#include<stdio.h>
  
int mat[50][50];
ll fatoriais[2501],invfat[2501];

void fat()
{
    int i;
    fatoriais[0]=1;
    for(i=1;i<=2500;i++)
        fatoriais[i]=(i*fatoriais[i-1])%MOD;
}
 
ll mdc(ll  a, ll b, ll *x, ll *y)
{
	ll xx, yy, d;
	if(b==0)
	{
		*x=1; *y=0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx - a/b*yy;
	return d;
}
 
ll inv(ll a)
{
    ll x,y,d;
    if(invfat[a]!=0)
        return invfat[a];
    d = mdc(fatoriais[a],MOD,&x,&y);
    if(x<0)
        x = x+MOD;
    invfat[a]=x;
    return x;
}

 
main()
{
    int X,Y,xi,yi,xf,yf,cont1,cont2;
    char c;
    fat();
    invfat[0]=1;
    invfat[1]=1;
    scanf("%d %d%*c",&X,&Y);
    for(int i=0;i<X;i++)
    {
        for(int j=0;j<Y;j++)
        {
            scanf("%c",&c);
            mat[i][j]=(c=='.');
        }
        scanf("%*c");
    }
    while(scanf("%d %d %d %d",&xi,&yi,&xf,&yf)==4)
    {
        cont1=0;
        cont2=0;
        for(int i=(xi-1);i<xf;i++)
        {
            for(int j=(yi-1);j<yf;j++)
            {
                if(mat[i][j]==1)    cont1++;
                else                cont2++;
            }
        }
        printf("%lld\n",(((((fatoriais[(xf-xi+1)*(yf-yi+1)]*inv(cont1))%MOD)*inv(cont2))%MOD)-1+MOD)%MOD);
    }
    return 0;
}
