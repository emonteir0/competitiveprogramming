# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: The Language
# Level: 2
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1623

fatoriais = [1]

i = 0
lim = 10**5000
while fatoriais[i] < lim:
	i += 1
	fatoriais.append(i*fatoriais[i-1])
while True:
	n, m = raw_input().split(' ')
	n = int(n)
	m = int(m)
	if n == 0 and m == 0:
		break
	num = input()
	if (n-m)>1775:
		print 'descartado'
	else:
		x = fatoriais[n-m+1]
		if x <= num:
			print x
		else:
			print 'descartado'

