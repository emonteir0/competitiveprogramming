// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grains in a Chess Board
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1169

#include<stdio.h>

unsigned long long int pot(int a)
{
	if(a==0)
		return 1;
	if(a==1)
		return 2;
	if(a%2==0)
		return pot(a/2)*pot(a/2);
	if(a%2==1)
		return 2*pot(a-1);
}

main()
{
	int n;
	int a;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&a);
		printf("%llu kg\n",(pot(a)-1)/12000);
	}
	return 0;
}
