// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fit or Dont Fit I
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1240

#include<stdio.h>
#include<string.h>
 
main()
{
    int i,n,x,a,b;
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {
        scanf("%d %d",&a,&b);
        x=10;
        if(b>a)
            printf("nao encaixa\n");
        else
        {
            while(b%x!=b)
            {
                x*=10;
            }
            if(a%x==b%x)
                printf("encaixa\n");
            else
                printf("nao encaixa\n");
        }
    }
    return 0;
}
