// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Generations
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1969

#include<stdio.h>
#define ll unsigned long long
#define MOD 1000000007
#define MAXN 100000

void multiply();
void multiply2();  
void power();

ll indices[MAXN+1];
int tree[MAXN+1];
int tam;

int F[2][2] = {{1,1},{1,0}};
int M[2][2] = {{1,1},{1,0}};

void power(ll N)
{
  if( N == 0 || N == 1)
      return;
  power(N/2);
  multiply2();
  if (N&1)
     multiply();
}
   
void multiply()
{
  int x =  (((ll)F[0][0])*M[0][0] + ((ll)F[0][1]*M[1][0]))%MOD;
  int y =  (((ll)F[0][0])*M[0][1] + ((ll)F[0][1]*M[1][1]))%MOD;
  int z =  (((ll)F[1][0])*M[0][0] + ((ll)F[1][1]*M[1][0]))%MOD;
  int w =  (((ll)F[1][0])*M[0][1] + ((ll)F[1][1]*M[1][1]))%MOD;
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
} 

void multiply2()
{
  int x =  (((ll)F[0][0])*F[0][0] + ((ll)F[0][1]*F[1][0]))%MOD;
  int y =  (((ll)F[0][0])*F[0][1] + ((ll)F[0][1]*F[1][1]))%MOD;
  int z =  (((ll)F[1][0])*F[0][0] + ((ll)F[1][1]*F[1][0]))%MOD;
  int w =  (((ll)F[1][0])*F[0][1] + ((ll)F[1][1]*F[1][1]))%MOD;
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
} 

int fib(ll N)
{
	F[0][0] = 1;
	F[1][0] = 1;
	F[0][1] = 1;
	F[1][1] = 0;
	power(N);
	return F[1][0];
}

void update(int i, int val)
{
	while (i <= tam)
	{
		tree[i] = ((ll)tree[i] * val)%MOD;
		i += (i&-i);
	}
}

int mul(int i)
{
	int mul = 1;
	while (i > 0)
	{
		mul = ((ll)mul * tree[i])%MOD;
		i -= (i & -i);
	}
	return mul;
}

void build()
{
	int i;
	for(i = 0; i <= tam; i++)
		tree[i] = 1;
	for(i = 0; i < tam; i++)
	{
		indices[i] = fib(indices[i]-1);
		update(i+1, indices[i]);
	}
}

int mdc(int a, int b, int *x, int *y)
{
	int xx, yy, d;
	if(b==0) 
	{
		*x=1; *y=0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx - a/b*yy;
	return d;
}


int inv(int a)
{
	int x, y, d;
	d = mdc(a, MOD, &x, &y);
	if(x < 0)
		x += MOD;
	return x;
}


main()
{
	int i, K, x;
	ll y, z;
	char op;
	scanf("%d %d%*c", &tam, &K);
	for(i = 0; i < tam; i++)
		scanf("%lld%*c", &indices[i]);
	build();
	while(K--)
	{
		scanf("%*c%c %d %lld%*c", &op, &x, &y);
		if(op == '>')
			printf("%d\n", ((ll)mul(y)*inv(mul(x-1)))%MOD);
		else
		{
			z = fib(y-1);
			update(x, ((ll)z * inv(indices[x-1]))%MOD);
			indices[x-1] = z;
		}
	}
	printf("%d\n", mul(tam));
	return 0;
}
