// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Playing with Numbers
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1801

#include<stdio.h>
#include<math.h>

int perm(unsigned long long int e, unsigned long long int f)
{
	int vet1[10],vet2[10],i;
	unsigned long long int w,q;
	for(i=0;i<10;i++)
	{
		vet1[i]=vet2[i]=0;
	}
	w=e;
	q=f;
	if(ceil(log(e)/log(10))!=ceil(log(f)/log(10)))
		return 0;
	while(e)
	{
		vet1[e%10]++;
		vet2[f%10]++;
		e/=10;
		f/=10;
	}
	for(i=0;i<10;i++)
	{
		if(vet1[i]!=vet2[i])
			return 0;
	}
	return 1;
}


main()
{
	unsigned long long int a,b,x,i;
	int cont=0;
	scanf("%llu",&x);
	a=ceil(sqrt(x));
	b=ceil(sqrt(11*x));
	for(i=a;i<=b;i++)
		cont+=perm(i*i-x,x);
	printf("%d\n",cont);
	return 0;
}
