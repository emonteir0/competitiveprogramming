// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Equilibrium
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1990

#include<cstdio>
#include<set>
#include<algorithm>

using namespace std;

set<int> conjunto,conjunto2;

main()
{
	int n,i,cont;
	long long int mean,x,vet[500],c,d;
	while(scanf("%d",&n)==1&&(n!=-1))
	{
		conjunto=conjunto2;
		mean=0;
		cont=0;
		for(i=0;i<n-1;i++)
		{
			scanf("%lld",&x);
			mean+=x;
			conjunto.insert(x);
			vet[i]=x;
		}
		sort(vet,vet+n-1);
		c=vet[(n-1)/2-1];
		d=vet[(n-1)/2];
		if((mean%(n-1)==0))
		{
			x=mean/(n-1);
			if(x>c&&x<d)
				cont++;
		}
		x=n*d-mean;
		if((x>d)&&(conjunto.find(x)==conjunto.end()))
			cont++;
		x=n*c-mean;
		if((x<c)&&(conjunto.find(x)==conjunto.end()))
			cont++;
		printf("%d\n",cont);
	}
	return 0;
}
