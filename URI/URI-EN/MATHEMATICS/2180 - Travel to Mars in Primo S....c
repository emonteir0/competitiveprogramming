// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Travel to Mars in Primo S...
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2180

#include<stdio.h>

int primos[100000], prime_count = 0;
char isprime[100000];

void primo()
{
	int i, j;
	primos[prime_count++] = 2;
	for(i = 4; i<=70000; i+=2)
		isprime[i] = 1;
	for(i=3; i<70000; i+= 2)
	{
		if(!isprime[i])
		{
			primos[prime_count++] = i;
			for(j = i+i; j < 70000; j+=i)
				isprime[j] = 1;
		}
	}
}

main()
{
	int N, sum = 0, k=-1, i;
	scanf("%d", &N);
	primo();
	while(N>primos[++k]);
	for(i = 0; i < 10; i++)
		sum += primos[k+i];
	printf("%d km/h\n%d h / %d d\n", sum, 60000000/sum, 2500000/sum);
	return 0;
}
