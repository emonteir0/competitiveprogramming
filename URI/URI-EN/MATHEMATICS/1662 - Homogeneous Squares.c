// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Homogeneous Squares
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1662

#include<stdio.h>

int mat[1000][1000];

main()
{
	int i, j, N;
	while(scanf("%d", &N) == 1 && N)
	{
		for(i = 0; i < N; i++)
			for(j = 0; j < N; j++)
				scanf("%d", &mat[i][j]);
		for(i = 0; i < N; i++)
			for(j = N-1; j >= 0; j--)
				mat[i][j] -= mat[i][0];
		for(i = 0; i < N; i++)
			for(j = 0; j < N; j++)
				if(mat[i][j] != mat[0][j])
				{
					i = N;
					break;
				}
		printf("%s\n", ((i == N) && (j == N)) ? "homogeneous" : "not homogeneous");
	}
	return 0;
}
