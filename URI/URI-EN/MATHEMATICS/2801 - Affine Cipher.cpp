// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Affine Cipher
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2801

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll A, B, T, inva;
ll vet[100001];

ll modInverse(ll a, ll b) 
{
	return a > 1LL ? b - ((modInverse(b % a, a) * b)/a) : 1LL;
}

ll inv(ll a, ll b)
{
	ll z = modInverse(a, b);
	if(z < 0)
		return z + b;
	return z;
}

ll func(int x)
{
	ll y = ((vet[x]-B+T)*inva)%T;
	return y;
}

main()
{
	int N;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%lld", &vet[i]);
	scanf("%lld %lld %lld", &T, &A, &B);
	if(__gcd(T, A) != 1)
		printf("DECIFRAGEM AMBIGUA\n");
	else
	{
		A %= T;
		B %= T;
		inva = inv(A, T);
		printf("%lld", func(0));
		for(int i = 1; i < N; i++)
			printf(" %lld", func(i));
		printf("\n");
	}
	return 0;
}
