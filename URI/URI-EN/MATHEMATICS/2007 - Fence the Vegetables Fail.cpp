// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fence the Vegetables Fail
// Level: 7
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2007

#include<bits/stdc++.h>

using namespace std;

struct ponto
{
	int x, y, id;
};

vector< vector<int> > G(100010);

int tree[100010];
int N, M;

ponto pontos[100010];
ponto cerca[100010];

set<int> conjux, conjuy;
set<int>::iterator it;
int X[100010], tamX = 1;
int Y[100010], tamY = 1;

void update(int i, int val)
{
	while (i <= M)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

bool cmp(ponto p1, ponto p2)
{
	return p1.x < p2.x;
}

main()
{
	int i, j, p;
	long long cnt;
	scanf("%d %d", &N, &M);
	for(i = 0; i < N; i++)
		scanf("%d %d", &pontos[i].x, &pontos[i].y);
	for(i = 0; i < M; i++)
	{
		scanf("%d %d", &cerca[i].x, &cerca[i].y);
		conjux.insert(cerca[i].x);
		conjuy.insert(cerca[i].y);
	}
	X[0] = -2000000000;
	Y[0] = -2000000000;
	for(it = conjux.begin(); it != conjux.end(); it++)
		X[tamX++] = *it;
	X[tamX] = 2000000000;
	for(it = conjuy.begin(); it != conjuy.end(); it++)
		Y[tamY++] = *it;
	Y[tamY] = 2000000000;
	for(i = 0; i < N; i++)
	{
		pontos[i].x = upper_bound(X, X+tamX+1, pontos[i].x) - X - 1;
		pontos[i].y = upper_bound(Y, Y+tamY+1, pontos[i].y) - Y - 1;
		pontos[i].id = i+1;
	}
	for(i = 0; i < M; i++)
	{
		cerca[i].x = lower_bound(X, X+tamX, cerca[i].x) - X;
		cerca[i].y = lower_bound(Y, Y+tamY, cerca[i].y) - Y;
		//printf("%d %d\n", cerca[i].x, cerca[i].y);
	}
	for(i = 0; i < M; i++)
		G[cerca[i].x].push_back(cerca[i].y);
	sort(pontos, pontos+N, cmp);
	//printf("\n\n");
	for(i = p = cnt = 0; i < N; i++)
	{
		if(pontos[i].x <= 0 || pontos[i].x > tamX)
			cnt += pontos[i].id;
		else
		{
			for(; p <= pontos[i].x; p++)
			{
				for(j = 0; j < (int)G[p].size(); j++)
				{
					if(sum(G[p][j])-sum(G[p][j]-1))
						update(G[p][j], -1);
					else
						update(G[p][j], 1);
				}
			}
			if(pontos[i].y <= 0 || pontos[i].y > tamY)
			{
				cnt += pontos[i].id;
			}
			else
			{
				//printf("%d %d\n", i+1, sum(pontos[i].y));
				cnt += (sum(pontos[i].y)&1) ? 0 : pontos[i].id;
			}
		}
	}
	printf("%lld\n", cnt);
	return 0;
}
