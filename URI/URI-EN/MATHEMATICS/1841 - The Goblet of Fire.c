// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Goblet of Fire
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1841

#include<stdio.h>
#define MAXN 10000000
#define MOD 1000000007
#define ll unsigned long long
 
char isprime[MAXN+1];
int prime[MAXN+1], prime_cont = 0,ind[MAXN+1];
int expoente[MAXN+1];
ll tree[MAXN+1];
  
void primo() 
{
	int i, j;
	ind[2] = 0;
	prime[prime_cont++] = 2; 
	for(i = 3; i <= MAXN; i += 2)
	{
		if(!isprime[i])
		{
			ind[i]=prime_cont;
			prime[prime_cont++] = i;
			for(j = i + i; j <= MAXN; j+= i)
				isprime[j] = 1;
		}
	}
}

void update(int i, ll val)
{
	++i;
	while (i <= prime_cont)
	{
		tree[i] = (tree[i]*val)%MOD;
		i += (i&-i);
	}
}

ll mul(int i)
{
	int s=0;
	ll mul = 1;
	++i;
	while (i > 0)
	{
		mul = (mul*tree[i])%MOD;
		i -= (i&-i);
	}
	return mul;
}

void build()
{
	int i;
	for(i = 0; i < prime_cont; i++)
		tree[i] = 1;
	for(i = 0; i < prime_cont; i++)
		update(i, expoente[i]+1);
}

int mdc(int a, int b, int *x, int *y)
{
	int xx, yy, d;
	if(b==0) 
	{
		*x=1; *y=0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx - a/b*yy;
	return d;
}


int inv(int a)
{
	int x, y, d;
	d = mdc(a, MOD, &x, &y);
	if(x < 0)
		x += MOD;
	return x;
}
  
main()
{
    ll N, d, x;
    int p, i, cont;
    primo();
    scanf("%llu", &N);
    d = N;
    for(i = 0; (i < prime_cont) && (d > 1); i++)
    {
        cont = 0;
        p = prime[i];
        while((d%p)==0)
        {
            d /= p;
            cont++;
        }
        if(cont > 0)
            expoente[i] = cont;
    }
    build();
    while(scanf("%d",&p) == 1)
    {
    	i = ind[p];
    	expoente[i]++;
    	x = inv(expoente[i]);
    	x *= (expoente[i]+1);
    	update(i, x);
    	printf("%llu\n", mul(i-1));
    }
    return 0;
}
