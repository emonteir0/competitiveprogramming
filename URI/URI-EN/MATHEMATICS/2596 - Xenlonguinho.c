// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Xenlonguinho
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2596

#include<stdio.h>

int vet[1001];

main()
{
	int i, x;
	for(i = 1; i <= 31; i++)
		vet[i*i] = 1;
	for(i = 1; i <= 1000; i++)
		vet[i] = vet[i-1] + 1 - vet[i];
	scanf("%*d");
	while(scanf("%d", &x) != EOF)
		printf("%d\n", vet[x]);
	return 0;
}
