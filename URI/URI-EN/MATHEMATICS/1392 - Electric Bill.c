// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Electric Bill
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1392

#include<stdio.h>

int ifunc(int x)
{
	if(x <= 200)
		return x/2;
	if(x <= 29900)
		return (x-200)/3 + 100;
	if(x <= 4979900)
		return (x-29900)/5 + 10000;
	return (x - 4979900)/7 + 1000000;
}

int func(int x)
{
	if(x <= 100)
		return 2*x;
	if(x <= 10000)
		return 200 + (x-100)*3;
	if(x <= 1000000)
		return 29900 + (x-10000)*5;
	return 4979900 + (x-1000000)*7;
}

main()
{
	int x, y, z, val, sup, inf = 0, mid;
	while(scanf("%d %d", &x, &y) == 2 && (x||y))
	{
		z = ifunc(x);
		sup = z/2;
		inf = 0;
		while(inf <= sup)
		{
			mid = (inf+sup)/2;
			val = func(z-mid) - func(mid);
			if(val == y)
			{
				printf("%d\n", func(mid));
				break;
			}
			else if(val > y)
				inf = mid+1;
			else
				sup = mid;
		}
	}
	return 0;
}
