// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Incredible Chapecoens...
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1626

#define MAX 100000
#define ll long long int
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<bits/stdc++.h>
 
using namespace std;
 
bitset<MAX+2> isprime;
int prime[MAX+2], prime_cont=0;
int lim = MAX;
  
void primo() {
  prime[prime_cont++] = 2;      
    isprime.set();
    for(int i=3; i<=lim; i+=2)
    {
        if(isprime[i])
        {
            prime[prime_cont++] = i;
             
            for(int j = i + i; j <= lim; j+= i)
                isprime.reset(j);
        }
    }
}
 
ll pot(ll a,int b)
{
    if(b==0)    return 1;
    if(b%2==0)
        return (pot(a,b/2)*pot(a,b/2))%1000000007;
    return (pot(a,b-1)*a)%1000000007;
}

ll sum(ll a,int b)
{
	int i;
	ll s=0;
	for(i=0;i<=b;i++)
		s=(s+pot(a,i))%1000000007;
	return s;
}

ll mdc(ll  a, ll b, ll *x, ll *y) {
  ll xx, yy, d;
  if(b==0)
  {
    *x=1; *y=0;
    return a;
  }
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}


ll inv(ll a)
{
  ll x,y,d;
  d = mdc(a,1000000007,&x,&y);
  if(x<0)
    x = x+1000000007;
  return x;
}

void calc(ll *x, ll *y, int z, int cont)
{
	ll a,b,c;
	a=pot(z,cont);
	b=inv(z-1);
	c=(((a*z)%1000000007-1)*b)%1000000007;
	*x=((*x)*a)%1000000007;
	*y=((*y)*c)%1000000007;
}

main()
{
	int n,i,z,cont;
	ll x,y;
	primo();
	while(scanf("%d",&n)==1)
	{
		x=1;
		y=1;
		for(i=0;(i<prime_cont)&&(n>=prime[i]);i++)
		{
			cont=0;
			z=prime[i];
			while(n/z)
			{
				cont+=(n/z);
				z*=prime[i];
			}
			calc(&x,&y,prime[i],cont);
		}
		y=((y-x)%1000000007+1000000007)%1000000007;
		printf("%lld %lld\n",y,x);
	}
	return 0;
}
