// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Oracle of Alexandria
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1457

#include<stdio.h>
#include<string.h>

main()
{
	long long int val;
	int n,num,k,i,N;
	char c,vet[25];
	scanf("%d",&n);
	while(n--)
	{
		scanf("%s",vet);
		N=strlen(vet);
		num=0;
		for(i=0;i<N;i++)
		{
			c=vet[i];
			if(c>='0'&&c<='9')
				num=10*num+c-'0';
			else
				break;
		}
		k=N-i;
		val=1;
		for(;num>=1;num-=k)
			val*=num;
		printf("%lld\n",val);	
	}
	return 0;
}
