// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Great Allocation of Commo...
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2076

#include<bits/stdc++.h>
#define ll long long int
#define MOD 1300031
#define MAX 1000000000

using namespace std;

int n, x0, x1;
int vet[21], vet2[21];
ll sum;

ll sumdiv(int x, int y, ll v)
{
	int first = (x/v)*v, second;
	if(first<x)
		first += v;
	second = (y/v)*v;
	return (((ll)(second+first)) * ((second-first)/v + 1)) / 2;
}


void soma(int k, ll v, int p)
{
	int i;
	if(p)
	{
		sum += sumdiv(x0, x1, v);
		for(i = k+1; i < n; i++)
			if(v*vet[i] <= x1)
				soma(i, v*vet[i], 0);
	}
	else
	{
		sum -= sumdiv(x0, x1, v);
		for(i = k+1; i < n; i++)
			if(v*vet[i] <= x1)
				soma(i, v*vet[i], 1);
	}
}

main()
{
	int x, i, j, k, N;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d", &x0, &x1, &k);
		n = 0;
		sum = 0;
		for(i=0;i<k;i++)
			scanf("%d", &vet2[i]);
		sort(vet2,vet2+k);
		for(i=0;i<k;i++)
		{
			x = vet2[i];
			bool b = (x <= x1);
			for(j = 0; j < i; j++)
				if(vet[j])
					b &= ((x % vet[j]) != 0);
			if(b)
				vet[n++] = x;
		}
		for(i=0;i<n;i++)
			soma(i, vet[i], 1);
		printf("%lld\n", sum%MOD);
	}
	return 0;
}

