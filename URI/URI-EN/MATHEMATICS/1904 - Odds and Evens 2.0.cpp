// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Odds and Evens 2.0
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1904

#include <string.h>
#include <stdio.h>
#include <math.h>
#define ll unsigned long long int
#define MAX 10000000

bool isprime[MAX+1];
ll prime[MAX], prime_cont = 0;
ll lim = MAX;

int abs(int x)
{
	if(x<0)
		return -x;
	return x;
}

void primo()
{
	int i,j;
	prime[prime_cont++] = 2;
    memset(isprime, true, sizeof(isprime));
    for(i=3; i<=3162; i+=2) if(isprime[i]) {
        prime[prime_cont++] = i;
        for(j=i*i; j<=lim; j+=i) {
            isprime[j] = false;
        }
    }
    for(;i<=lim;i+=2)
    	if(isprime[i])
    		prime[prime_cont++] = i;
}

int busca(int x)
{
	int mid,up,low;
	low=0;
	up=prime_cont-1;
	while (low <= up) 
     {
          mid = (low + up)/2;
          if (x == prime[mid])
               return mid;
          if (x < prime[mid])
               up = mid-1;
          else
               low = mid+1;
     }
     return mid;
}

main()
{
	int n,m,A,B,x,y,cont=0,z,l;
	primo();
	scanf("%d %d",&A,&B);
	if(A==B)
		printf("?\n");
	else
	{
		if(A>B)
		{
			A+=B;
			B=A-B;
			A-=B;
		}
		m=B-A;
		x=busca(A);
		y=busca(B);
		if(prime[x]<A)
			x++;
		if(prime[y]>B)
			y--;
		n=y-x+1;
		if(n==0)
			printf("Bob\n");
		else
		{
			z=n+m-1;
			l=2;
			while(z>=l)
			{
				cont+=(z/l);
				l=l<<1;
			}
			z=n-1;
			l=2;
			while(z>=l)
			{
				cont-=(z/l);
				l=l<<1;
			}
			z=m;
			l=2;
			while(z>=l)
			{
				cont-=(z/l);
				l=l<<1;
			}
			printf("%s\n",cont>0?"Bob":"Alice");
		}
	}
    return 0;
}
