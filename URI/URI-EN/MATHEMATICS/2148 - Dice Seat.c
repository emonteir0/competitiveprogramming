// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dice Seat
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2148

#include<stdio.h>
#include<math.h>
#define ll unsigned long long int

ll mat[14][81];

double divisao(double x, int y)
{
	while(y--)
		x/=6;
	return x;
}

main()
{
	int i, j, k, n, x, y;
	mat[1][1] = 1;
	mat[1][2] = 1;
	mat[1][3] = 1;
	mat[1][4] = 1;
	mat[1][5] = 1;
	mat[1][6] = 1;
	for(i=2;i<=13;i++)
		for(j=2;j<=80;j++)
			for(k=1;k<=6;k++)
				if(j>k)
					mat[i][j] += mat[i-1][j-k];
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&x,&y);
		printf("%.15lf\n",divisao(mat[y][x],y));
	}
	return 0;
}

