// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Setting up a Date
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2068

#include<stdio.h>
#define ll unsigned long long int

ll mdc(ll a, ll b)
{
	ll aux;
	while(b!=0)
	{
		aux=a%b;
		a=b;
		b=aux;
	}
	return a;
}

main()
{
	int x, y, z;
	ll a, b, md;
	scanf("%d %d %d",&x,&y,&z);
	b = ((ll)3600)*(y-x)*(y-x);
	if(z>(y-x)*30)
		a = ((ll)(z+(y-x)*60))*((y-x)*60-z) + ((ll)60)*(y-x)*((y-x)*60-2*((y-x)*60-z));
	else
		a = ((ll)(3*z))*z + ((ll)(60*(y-x)-2*z))*2*z;
	md = mdc(a,b);
	printf("%lld/%lld\n",a/md,b/md);
	return 0;
}
