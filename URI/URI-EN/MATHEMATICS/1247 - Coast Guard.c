// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Coast Guard
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1247

#include<stdio.h>
 
main()
{
    int D,VG,VF;
    while(scanf("%d %d %d",&D,&VF,&VG)==3)
    {
        printf("%s\n",(144+D*D)*VF*VF<=144*VG*VG?"S":"N");       
    }
    return 0;
}
