// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Ones
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1492

#include<stdio.h>


main()
{
	unsigned long long int a,b,f,e,x,cont;
	while(scanf("%llu %llu",&a,&b)==2&&(a||b))
	{
		if(a>b)
		{
			b+=a;
			a=b-a;
			b-=a;
		}
		cont=0;
		f=b+1;
		e=1;
		while(f>=e)
		{
			x=f%(2*e);
			cont+=e*(f/2/e)+(x-e)*(x>e);
			e*=2;
		}
		f=a;
		e=1;
		while(f>=e)
		{
			x=f%(2*e);
			cont-=e*(f/2/e)+(x-e)*(x>e);
			e*=2;
		}
		printf("%llu\n",cont);
	}
	return 0;
}
