// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Higgs Boson
// Level: 7
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1324

#include<bits/stdc++.h>
#define ll long long
using namespace std;

struct frac
{
	ll n, d;
	frac(){}
	frac(ll n, ll d)
	{
		if(n == 0 && d == 0)
		{
			this->n = 0;
			this->d = 0;
		}
		else
		{
			if(d < 0)
			{
				n = -n;
				d = -d;
			}
			ll m = __gcd(abs(n), abs(d));
			this->n = n/m;
			this->d = d/m;
		}
	}
	frac operator* (ll x) const
	{
		ll z, m;
		z = n*x;
		m = __gcd(abs(z), d);
		return frac(z/m, d/m);
	}
	frac operator+ (ll x) const
	{
		ll z = n+x*d;
		return frac(z, d);
	}
	frac operator+ (frac f) const
	{
		ll n2 = n*f.d + f.n*d, d2 = d*f.d;
		if(d2 < 0)
		{
			n2 = -n2;
			d2 = -d2;
		}
		ll m = __gcd(abs(n2), abs(d2));
		return frac(n2/m, d2/m);
	}
	frac operator- (frac f) const
	{
		ll n2 = n*f.d - f.n*d, d2 = f.d*d;
		if(d2 < 0)
		{
			n2 = -n2;
			d2 = -d2;
		}
		ll m = __gcd(abs(n2), abs(d2));
		return frac(n2/m, d2/m);
	}
	void printfrac()
	{
		printf("%lld %lld\n", n, d);
	}
};

frac t;

frac minimo(frac a, frac b)
{
	if(a.d == 0)
		return b;
	return (a-b).n > 0 ? b : a;
}

void tentativa(int A1, int B1, int C1, int D1, int A2, int B2, int C2, int D2)
{
	frac g, teste;
	if(A1 == A2)
	{
		if(B1 == B2)
		{
			if(C1 == C2)
			{
				if((D1-D2)%360 == 0)
				{
					t = minimo(t, frac(0, 1));
				}
			}
			else
			{
				int n = D2-D1, d = C1-C2;
				if(d < 0)
				{
					n = -n;
					d = -d;
				}
				t = minimo(t, frac((n%360+360)%360, d));
			}
			g = frac(-B1, A1);
			if(g.n >= 0)
			{
				t = minimo(t, g);
			}
		}
		else if(B1 == -B2)
		{
			if(C1 == C2)
			{
				if(((D1-D2)%360 + 360)%360 == 180)
				{
					t = minimo(t, frac(0, 1));
				}
			}
			else
			{
				int n = D2-D1+180, d = C1-C2;
				if(d < 0)
				{
					n = -n;
					d = -d;
				}
				t = minimo(t, frac((n%360+360)%360, d));
			}
		}
	}
	else
	{
		g = frac(B2-B1, A1-A2);
		if(g.n < 0)
			return;
		teste = g*(C1-C2) + (D1-D2);
		if(teste.d == 1 && teste.n % 360 == 0)
		{
			t = minimo(t, g);
		}
	}
}


int main()
{
	int A1, B1, C1, D1, A2, B2, C2, D2;
	while(cin >> A1 >> B1 >> C1 >> D1 >> A2 >> B2 >> C2 >> D2)
	{
		t = frac(0, 0);
		if(A1 == 0 && B1 == 0 && C1 == 0 && D1 == 0 && A2 == 0 && B2 == 0 && C2 == 0 && D2 == 0)
			break;
		tentativa(A1, B1, C1, D1, A2, B2, C2, D2);
		A2 = -A2;
		B2 = -B2;
		D2 += 180;	
		tentativa(A1, B1, C1, D1, A2, B2, C2, D2);
		t.printfrac();
	}
	return 0;
}

