// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Primary Arithmetic
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1212

#include<stdio.h>

main()
{
	long int a,b;
	char carry=0,cont;
	while(scanf("%d %d",&a,&b)==2&&(a!=0||b!=0))
	{
		cont= 0;
		carry = 0;
		while(a!=0||b!=0)
		{
			carry=(a%10+b%10+carry)/10;
			a/=10;
			b/=10;
			cont+=carry;
		}	
		if(cont==0)
			printf("No carry operation.\n");
		else if(cont==1)
			printf("1 carry operation.\n");
		else
			printf("%d carry operations.\n",cont);
	}
	return 0;
}
