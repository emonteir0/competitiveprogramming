// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: My First Little Graph
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2822

#include<bits/stdc++.h>
#define ll long long

using namespace std;

pair<ll, int> tree[100002];
int N;

pair<ll, int> operator+ (pair<ll, int> a, pair<ll, int> b)
{
	return make_pair(a.first+b.first, a.second+b.second);
}

void update(int i, int val)
{
	pair<ll, int> p;
	++i;
	if(val > 0)
		p = make_pair(val, 1);
	else
		p = make_pair(val, -1);
	while (i <= N)
	{
		tree[i] = tree[i] + p;
		i += (i&-i);
	}
}

pair<ll, int> soma(int i)
{
	pair<ll, int> s = make_pair(0, 0);
	++i;
	while (i > 0)
	{
		s = s + tree[i];
		i -= (i & -i);
	}
	return s;
}

int vet[100001];
multiset<int> conju;
multiset<int>::iterator it;

void deleta(int x)
{
	it = conju.find(x);
	conju.erase(it);	
}

main()
{
	int T, Q, x, v, op;
	pair<ll,int> pa, zero = make_pair(0, 0);
	ll sum, ma, me;
	scanf("%d", &T);
	while(T--)
	{
		conju.clear();
		sum = 0;
		scanf("%d", &N);
		for(int i = 1; i <= N; i++)
			tree[i] = zero;
		for(int i = 1; i <= N; i++)
			update(i, i), vet[i] = i, conju.insert(i), sum += i;
		//for(int i = 1; i<= N; i++)
			//printf("%lld %d\n", soma(i).first, soma(i).second);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d", &op);
			if(op == 1)
			{
				scanf("%d %d", &x, &v);
				update(vet[x], -vet[x]);
				deleta(vet[x]);
				sum -= vet[x];
				vet[x] = v;
				sum += v;
				conju.insert(v);
				update(v, v);
			}
			else
			{
				me = *conju.begin();
				it = conju.end();
				it--;
				ma = *it;
				pa = soma((ma+me)/2);
				//printf("%lld %d (%lld, %lld)\n", pa.first, pa.second, me, ma);
				printf("%lld\n", ma*pa.second - pa.first + sum-pa.first - me*(N-pa.second)-(ma-me));
			}
		}
	}
	return 0;
}
