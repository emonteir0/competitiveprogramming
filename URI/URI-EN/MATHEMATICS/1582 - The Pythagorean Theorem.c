// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Pythagorean Theorem
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1582

#include<stdio.h>
 
int absol(int x)
{
    if(x<0)
        return -x;
    return x;
}
 
int mdc(int a, int b)
{
    int aux;
    while(b!=0)
    {
        aux=b;
        b=a%b;
        a=aux;
    }
    return a;
}
 
main()
{
    int a,b,c;
    while(scanf("%d %d %d",&a,&b,&c)==3)
    {
        if(((a*a+b*b)==(c*c))||absol(a*a-b*b)==c*c)
        {
            if(mdc(a,b)==1)
                printf("tripla pitagorica primitiva\n");
            else
                printf("tripla pitagorica\n");
                 
        }
        else
            printf("tripla\n");
    }
    return 0;
}
