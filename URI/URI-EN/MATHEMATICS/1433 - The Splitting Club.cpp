// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Splitting Club
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1433

#include<bits/stdc++.h>

using namespace std;

int vet[10000];

main()
{
	int i, N, x, cont;
	long double R;
	while(scanf("%d %Lf", &N, &R) == 2 && !(N == 0 || R == 0.0))
	{
		cont = 1;
		for(i = 0; i < N; i++)
			scanf("%d %*d", &vet[i]);
		sort(vet, vet+N);
		x = vet[0];
		for(i = 1; i < N; i++)
		{
			if((long double)vet[i]/x > R)
			{
				cont++;
				x = vet[i];
			}
		}
		printf("%d\n", cont);
	}
	return 0;
}
