// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bank Line
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1537

#include<stdio.h>
#define DIVISOR 1000000009

main()
{
	long long int i,N,b;
	while(scanf("%lld",&N)==1&&N)
	{
		b=1;
		for(i=4;i<=N;i++)
			b=(b*i)%DIVISOR;
		printf("%lld\n",b);
	}
	return 0;
}
