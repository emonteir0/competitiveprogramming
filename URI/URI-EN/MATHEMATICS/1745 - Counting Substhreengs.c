// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Substhreengs
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1745

#include<stdio.h>

int s;
long long int soma;

main()
{
	int r[3]={1,0,0};
	char c;
	while(scanf("%c",&c)==1)
	{
		if(c>='0'&&c<='9')
		{
			s=(s+c)%3;
			soma+=r[s];
			r[s]++;
		}
		else
		{
			s=0;
			r[0]=1;
			r[1]=0;
			r[2]=0;
			if(c=='\n')	break;
		}
	}
	printf("%lld\n",soma);
	return 0;
}
