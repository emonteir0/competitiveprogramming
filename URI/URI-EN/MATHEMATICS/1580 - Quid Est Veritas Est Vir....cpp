// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quid Est Veritas? Est Vir...
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1580

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<bits/stdc++.h>

using namespace std;

bitset<1002> isprime;
int prime[1002], prime_cont=0;
int lim = 1000,val[26];
 
void primo() {
  prime[prime_cont++] = 2;      
    isprime.set();
    for(int i=3; i<=lim; i+=2)
    {
        if(isprime[i])
        {
            prime[prime_cont++] = i;
            
            for(int j = i + i; j <= lim; j+= i)
                isprime.reset(j);
        }
    }
}

unsigned long long int pot(long long int a,int b)
{
    if(b==0)    return 1;
    if(b%2==0)
        return (pot(a,b/2)*pot(a,b/2))%1000000007;
    return (pot(a,b-1)*a)%1000000007;
}

main()
{
	char vet[1001];
	int i,j,n,c,l,z;
	unsigned long long int x;
	primo();
	while(scanf("%s",vet)==1)
	{
		x=1;
		for(i=0;i<26;i++)
			val[i]=0;
		n=strlen(vet);
		sort(vet,vet+n);
		for(i=0;i<n;i++)
			val[vet[i]-'A']++;
		for(i=0;i<prime_cont;i++)
		{
			c=0;
			l=n;
			z=prime[i];
			if(l<z)
				break;
			while(l/z)
			{
				c+=(l/z);
				l/=z;
			}
			for(j=0;j<26;j++)
			{
				l=val[j];
				while(l/z)
				{
					c-=(l/z);
					l/=z;
				}
			}
			x=(x*pot(z,c))%1000000007;
		}
		printf("%llu\n",x);
	}
	return 0;
}
