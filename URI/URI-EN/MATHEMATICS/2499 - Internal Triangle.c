// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Internal Triangle
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2499

#include<stdio.h>

main()
{
	int N, M, K, a, b, c;
	long long A;
	while(scanf("%d %d %d", &N, &M, &K) == 3 && (N||M||K))
	{
		scanf("%d %d %d", &a, &b, &c);
		A = N;
		A *= a * (c-b);
		A /= (M+1) * (K+1);
		printf("%lld\n", abs(A));
	}
	return 0;
}
