// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jogo de Boca
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2667

#include<bits/stdc++.h>

using namespace std;

main()
{
	int i, c = 0;
	string str;
	cin >> str;
	for(i = 0; i < (int)str.size(); i++)
		c += str[i] - '0';
	printf("%d\n", c%3);
	return 0;
}
