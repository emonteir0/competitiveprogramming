// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Unlocking a Cell Phone
// Level: 7
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2711

#include<bits/stdc++.h>
#define ll long long

using namespace std;

unordered_map<ll, ll> Mapa, Mapa2;
ll M;

ll mdc(ll a, ll b, ll *x, ll *y)
{
	ll xx, yy, d;
	if(b == 0)
	{
		*x = 1;
		*y = 0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx - a/b*yy;
	return d;
}

ll inv(ll a)
{
	ll x, y;
	mdc(a, M, &x, &y);
	if(x < 0)
		x = x + M;
	return x;
}

main()
{
	ll i, B, N, F, lim;
	Mapa2[1] = 0;
	while(scanf("%lld %lld %lld", &B, &N, &M) == 3)
	{
		N %= M;
		if(__gcd(B, M) != 1)
		{
			if(N == 1)
				printf("0\n");
			else if(N == 0)
				printf("1\n");
			else
				printf("-1\n");
		}
		else
		{
			Mapa = Mapa2;
			lim = sqrt(M)+1;
			F = 1;
			for(i = 1; i < lim; i++)
			{
				F = ((ll)F * B) % M;
				if(Mapa.find(F) == Mapa.end())
					Mapa[F] = i;
			}
			F = inv(((ll)F * B) % M);
			for(i = 0; i <= lim; i++)
			{
				if(Mapa.find(N) != Mapa.end())
				{
					if(i*lim + Mapa[N] < M)
						printf("%lld\n", i*lim + Mapa[N]);
					else
						printf("-1\n");
					break;
				}
				N = ((ll)N * F) % M;
			}
			if(i > lim)
				printf("-1\n");
		}
	}
	return 0;
}
