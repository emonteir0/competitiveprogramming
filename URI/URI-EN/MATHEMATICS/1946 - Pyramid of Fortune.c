// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pyramid of Fortune
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1946

#include<stdio.h>
#include<math.h>

main()
{
	int n,i,cont;
	double x;
	scanf("%d",&n);
	n--;
	cont=n;
	x=100;
	for(i=0;i<n/2;i++)
	{
		x=(x*(n-i))/(i+1);
		while((x>100)&&(cont>0))
		{
			x/=2;
			cont--;
		}
	}
	while(cont>0)
	{
		x/=2;
		cont--;
	}
	printf("%.2lf\n",x);
	return 0;
}
