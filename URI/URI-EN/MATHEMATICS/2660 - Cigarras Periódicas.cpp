// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cigarras Periódicas
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2660

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int main()
{
	ll N, L, i, x, y, mm = 1;
	scanf("%lld %lld", &N, &L);
	while(N--)
	{
		scanf("%lld", &x);
		mm = (mm/__gcd(mm, x)) * x;
	}
	ll best = 0;
	int ind;
	for(i = 1; i <= L; i++)
	{
		y = (mm/__gcd(mm, (ll)i)) * i;
		if(y > best && y <= L)
		{
			best = y;
			ind = i;
		}
	}
	printf("%d\n", ind);
	return 0;
}
