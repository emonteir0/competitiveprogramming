// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Biggest Prime Gap
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2589

#include<bits/stdc++.h>

using namespace std;

vector< int > primes, primegaps;

main()
{
	int N;
	primegaps.push_back(0);
	primegaps.push_back(1);
	primegaps.push_back(2);
	primegaps.push_back(4);
	primegaps.push_back(6);
	primegaps.push_back(8);
	primegaps.push_back(14);
	primegaps.push_back(18);
	primegaps.push_back(20);
	primegaps.push_back(22);
	primegaps.push_back(34);
	primegaps.push_back(36);
	primegaps.push_back(44);
	primegaps.push_back(52);
	primegaps.push_back(72);
	primegaps.push_back(86);
	primegaps.push_back(96);
	primegaps.push_back(112);
	primegaps.push_back(114);
	primegaps.push_back(118);
	primegaps.push_back(132);
	primegaps.push_back(148);
	primegaps.push_back(154);
	primegaps.push_back(180);
	primegaps.push_back(210);
	primegaps.push_back(220);
	primegaps.push_back(222);
	primegaps.push_back(234);
	primegaps.push_back(248);
	primegaps.push_back(250);
	primegaps.push_back(282);
	
	primes.push_back(0);
	primes.push_back(3);
	primes.push_back(5);
	primes.push_back(11);
	primes.push_back(29);
	primes.push_back(97);
	primes.push_back(127);
	primes.push_back(541);
	primes.push_back(907);
	primes.push_back(1151);
	primes.push_back(1361);
	primes.push_back(9587);
	primes.push_back(15727);
	primes.push_back(19661);
	primes.push_back(31469);
	primes.push_back(156007);
	primes.push_back(360749);
	primes.push_back(370373);
	primes.push_back(492227);
	primes.push_back(1349651);
	primes.push_back(1357333);
	primes.push_back(2010881);
	primes.push_back(4652507);
	primes.push_back(17051887);
	primes.push_back(20831533);
	primes.push_back(47326913);
	primes.push_back(122164969);
	primes.push_back(189695893);
	primes.push_back(191912783);
	primes.push_back(387096383);
	primes.push_back(436273291);
	while(scanf("%d", &N) != EOF)
		printf("%d\n", primegaps[upper_bound(primes.begin(), primes.end(), N) - primes.begin() - 1]);
	return 0;
}
