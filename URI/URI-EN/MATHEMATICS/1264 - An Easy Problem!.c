// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: An Easy Problem!
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1264

#include<stdio.h>
#include<string.h>

main()
{
	int n,i,j,ma=1,res,x[1024];
	char vet[1025],c;
	while(gets(vet)!=NULL)
	{
		n=strlen(vet);
		ma=1;
		for(i=0;i<n;i++)
		{
			c=vet[i];
			x[i]=0;
			if(c>='0'&&c<='9')
				x[i]=c-'0';
			if(c>='A'&&c<='Z')
				x[i]=c-'A'+10;
			if(c>='a'&&c<='z')
				x[i]=c-'a'+36;
			if(x[i]>ma)
				ma=x[i];
		}
		for(j=ma;j<63;j++)
		{
			res=0;
			for(i=0;i<n;i++)
			{
				if(vet[i]=='-')
					i++;
				res=(res*(j+1)+x[i])%j;
			}
			if(res==0)
				break;
		}
		if(j!=63)
			printf("%d\n",j+1);
		else
			printf("such number is impossible!\n");
	}
	return 0;
}
