// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Complex, Difficult and Co...
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1465

#include<stdio.h>

main()
{
	int n,a,b;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&a,&b);
		a*=(a>=0)?1:-1;
		b*=(b>=0)?1:-1;
		if((a!=0&&b==0)||(a==0&&b==0))
			printf("1\n");
		else if(a==b)
				printf("4\n");
		else if(b!=0&&a==0)
			printf("2\n");
		else
			printf("TOO COMPLICATED\n");
	}
	return 0;
}
