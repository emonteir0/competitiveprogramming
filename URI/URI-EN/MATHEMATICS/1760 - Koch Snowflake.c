// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Koch Snowflake
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1760

#include<stdio.h>
#include<math.h>

main()
{
	double a;
	while(scanf("%lf",&a)==1)
	{
		printf("%.2f\n",a*a*2*sqrt(3)/5);
	}
	return 0;
}
