// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bacteria
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1422

#include <stdio.h>
#define MOD 13371337
#define ll long long int
  
void multiply(int F[5][5]);

void multiply2(int F[5][5]);
  
void power(int F[5][5], int n);

int F[5][5];
int M[5][5] = {{2,0,0,0,-1},{1,0,0,0,0},{0,1,0,0,0},{0,0,1,0,0},{0,0,0,1,0}};  
int vet[5]; 
ll G[5][5];
 
int bac(int n)
{
	int i,j;
	if(n==0)
		return vet[4];
	for(i=0;i<5;i++)
		for(j=0;j<5;j++)
			F[i][j]=M[i][j];
	power(F, n);
	return ((((ll)F[0][0])*vet[4]+((ll)F[0][1])*vet[3]+((ll)F[0][2])*vet[2]+((ll)F[0][3])*vet[1]+((ll)F[0][4])*vet[0])%MOD+MOD)%MOD;
}
  
void power(int F[5][5], int n)
{
  if(n==0||n==1)
      return; 
    power(F, n/2);
    multiply2(F);
  if(n%2==1)
     multiply(F);
}
  
void multiply(int F[5][5])
{
    int i,j,k;
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            G[i][j]=0;
            
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            for(k=0;k<5;k++)
                G[i][j]+=(((ll)F[i][k])*M[k][j]);
                
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            F[i][j]=G[i][j]%MOD;
}

void multiply2(int F[5][5])
{
    int i,j,k;
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            G[i][j]=0;
            
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            for(k=0;k<5;k++)
                G[i][j]+=(((ll)F[i][k])*F[k][j]);
                
    for(i=0;i<5;i++)
        for(j=0;j<5;j++)
            F[i][j]=G[i][j]%MOD;
}
   
int main()
{
  int n;
  while(scanf("%d",&n)==1&&n)
  {
      scanf("%d %d %d %d",&vet[0],&vet[1],&vet[2],&vet[3]);
      vet[4]=vet[0]+vet[1]+vet[2]+vet[3];
      printf("%d\n",bac(n-4));
  }
  return 0;
}
