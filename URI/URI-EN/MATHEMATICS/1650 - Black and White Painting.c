// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Black and White Painting
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1650

#include<stdio.h>


main()
{
	int a,b,c,x,y,z,w;
	while(scanf("%d %d %d",&a,&b,&c)==3&&(a||b||c))
	{
		if(c==1)
		{
			x=(a-6)/2;
			y=(b-6)/2;
			z=(a-8)%2;
			w=(b-8)%2;
			printf("%d\n",x*y+(x-1+z)*(y-1+w));
		}
		else
		{
			a++;
			x=(a-6)/2;
			y=(b-6)/2;
			z=(a-8)%2;
			w=(b-8)%2;
			printf("%d\n",x*y+(x-1+z)*(y-1+w)-y);
		}
	}
	return 0;
}
