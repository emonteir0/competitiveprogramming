// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Arquibaldo's Challenge
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2335

#include<stdio.h>
#include<math.h>


main()
{
	int c;
	double a, b, v;
	while(scanf("%lf %lf", &a, &b) == 2)
	{
			if (a <= b)
			{
				a += b;
				b = a-b;
				a -= b;
			}
			v = (b + sqrt(2*a*a-b*b))/(2*a);
			if(v>=1)
				printf("Nao existe tal triangulo.\n");
			else
			{
				v = acos(v)*(180/M_PI);
				if (v>45)
					v = 90-v;
				c = (int)floor(v);
				if(c == 0)
					printf("Nao existe tal triangulo.\n");
				else
					printf("%d\n", c);
			}
	}
	return 0;
}
