// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rubik Cycle
// Level: 6
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1232

#include<stdio.h>

typedef struct
{
	char matrix[54][54];
} rotacao;

rotacao multiplymatrix(rotacao a, rotacao b)
{
	int i, j, k;
	rotacao c;
	for(i = 0; i < 54; i++)
		for(j = 0; j < 54; j++)
			c.matrix[i][j] = 0;
	for(i = 0; i < 54; i++)
		for(j = 0; j < 54; j++)
			for(k = 0; k < 54; k++)
				c.matrix[i][j] |= a.matrix[i][k] & b.matrix[k][j];
	return c;
}

rotacao matrixF;
rotacao matrixf;
rotacao matrixB;
rotacao matrixb;
rotacao matrixU;
rotacao matrixu;
rotacao matrixD;
rotacao matrixd;
rotacao matrixL;
rotacao matrixl;
rotacao matrixR;
rotacao matrixr;
rotacao matrixAns;
rotacao matrixI;



int indices[12][8] =
{
	
	{34, 31, 28, 29, 30, 33, 36, 35},//F
	{34, 35, 36, 33, 30, 29, 28, 31},//f
	{10, 11, 12, 15, 18, 17, 16, 13},//B
	{10, 13, 16, 17, 18, 15, 12, 11},//b
	{19, 20, 21, 24, 27, 26, 25, 22},//U
	{19, 22, 25, 26, 27, 24, 21, 20},//u
	{54, 53, 52, 49, 46, 47, 48, 51},//D
	{54, 51, 48, 47, 46, 49, 52, 53},//d
	{1, 2, 3, 6, 9, 8, 7, 4},//L
	{1, 4, 7, 8, 9, 6, 3, 2},//l
	{45, 44, 43, 40, 37, 38, 39, 42},//R
	{45, 42, 39, 38, 37, 40, 43, 44} //r
};

int indices2[12][12] = 
{
	{9, 6, 3, 27, 24, 21, 45, 42, 39, 54, 51, 48}, //F
	{9, 48, 51, 54, 39, 42, 45, 21, 24, 27, 3, 6}, //f
	{19, 22, 25, 1, 4, 7, 46, 49, 52, 37, 40, 43}, //B
	{19, 43, 40, 37, 52, 49, 46, 7, 4, 1, 25, 22}, //b
	{43, 44, 45, 28, 31, 34, 3, 2, 1, 18, 15, 12}, //U
	{43, 12, 15, 18, 1, 2, 3, 34, 31, 28, 45, 44}, //u
	{39, 38, 37, 10, 13, 16, 7, 8, 9, 36, 33, 30}, //D
	{39, 30, 33, 36, 9, 8, 7, 16, 13, 10, 37, 38}, //d
	{16, 17, 18, 25, 26, 27, 34, 35, 36, 48, 47, 46}, //L
	{16, 46, 47, 48, 36, 35, 34, 27, 26, 25, 18, 17}, //l
	{21, 20, 19, 12, 11, 10, 52, 53, 54, 30, 29, 28}, //R
	{21, 28, 29, 30, 54, 53, 52, 10, 11, 12, 19, 20} //r
};

rotacao change(rotacao mat, int* ind, int* ind2)
{
	int i;
	rotacao mat2 = mat;
	for(i = 0; i < 8; i++)
		mat2.matrix[ind[i]-1][ind[i]-1] = 0;
	for(i = 0; i < 8; i++)
		mat2.matrix[ind[i]-1][(ind[(i+2)%8])-1]= 1;
	for(i = 0; i < 12; i++)
		mat2.matrix[ind2[i]-1][ind2[i]-1] = 0;
	for(i = 0; i < 12; i++)
		mat2.matrix[ind2[i]-1][(ind2[(i+3)%12])-1]= 1;
	return mat2;
}

void printmat(rotacao mat)
{
	int i, j;
	for(i = 0; i < 54; i++)
	{
		for(j = 0; j < 54; j++)
			printf("%d ", mat.matrix[i][j]);
		printf("\n");
	}
}

void printgp(rotacao mat)
{
	int i, j;
	printf("[");
	for(i = 0; i < 54; i++)
	{
		printf("%d", mat.matrix[i][0]);
		for(j = 1; j < 54; j++)
			printf(", %d", mat.matrix[i][j]);
		if(i < 53)
			printf("; ");
	}
	printf("]\n\n");
}

rotacao matriz(char c)
{
	if(c == 'F')
		return matrixF;
	if(c == 'f')
		return matrixf;
	if(c == 'B')
		return matrixB;
	if(c == 'b')
		return matrixb;
	if(c == 'U')
		return matrixU;
	if(c == 'u')
		return matrixu;
	if(c == 'D')
		return matrixD;
	if(c == 'd')
		return matrixd;
	if(c == 'L')
		return matrixL;
	if(c == 'l')
		return matrixl;
	if(c == 'R')
		return matrixR;
	return matrixr;
}

int getind(int i, rotacao mat)
{
	int j;
	for(j = 0; j < 54; j++)
	{
		if(mat.matrix[i][j])
			return j;
	}
	return 0;
}

int mdc(int a, int b)
{
	if(b == 0)
		return a;
	return mdc(b, a%b);
}

main()
{
	int i, j, ind, cont, x;
	char palavra[81];
	for(i = 0; i < 54; i++)
		for(j = 0; j < 54; j++)
				matrixI.matrix[i][j] = (i==j);
				
	matrixAns = matrixD = matrixd = matrixR = matrixr = matrixF = matrixf = matrixU = matrixu = matrixB = matrixb = matrixL = matrixl = matrixI;
	matrixF = change(matrixF, indices[0], indices2[0]);
	matrixf = change(matrixf, indices[1], indices2[1]);
	matrixB = change(matrixB, indices[2], indices2[2]);
	matrixb = change(matrixb, indices[3], indices2[3]);
	matrixU = change(matrixU, indices[4], indices2[4]);
	matrixu = change(matrixu, indices[5], indices2[5]);
	matrixD = change(matrixD, indices[6], indices2[6]);
	matrixd = change(matrixd, indices[7], indices2[7]);
	matrixL = change(matrixL, indices[8], indices2[8]);
	matrixl = change(matrixl, indices[9], indices2[9]);
	matrixR = change(matrixR, indices[10], indices2[10]);
	matrixr = change(matrixr, indices[11], indices2[11]);
	
	while(scanf("%s", palavra) == 1)
	{
		matrixAns = matrixI;
		for(i = 0; palavra[i]; i++)
			matrixAns = multiplymatrix(matrixAns, matriz(palavra[i]));
		x = 1;
		/*printmat(matrixAns);
		for(i = 0; i < 54; i++)
			for(j = 0; j < 54; j++)
				x &= (matrixAns.matrix[i][j] == (i==j));
		printf("%d\n", x);*/
		x = 1;
		for(i = 0; i < 54; i++)
		{
			cont = 1;
			ind = getind(i, matrixAns);
			while(ind != i)
			{
				ind = getind(ind, matrixAns);
				cont++;
			}
			x = (x*cont)/mdc(x,cont);
		}
		printf("%d\n", x);
	}
	return 0;
}

