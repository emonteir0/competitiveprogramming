// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ballot Evaluation
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1635

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;

main()
{
	int i, N, M, x, y, k, K = 1, sum1, sum2;
	char nome[21], op, c, bol;
	string str;
	scanf("%d %d%*c", &N, &M);
	while(N--)
	{
		scanf("%s %d.%d%*c", nome, &x, &y);
		x = 10*x + y;
		Mapa[str = nome] = x;
	}
	while(M--)
	{
		op = 0;
		k = 0;
		sum1 = 0;
		sum2 = 0;
		while(scanf("%c", &c) && c != '\n')
		{
			if(c != ' ')
				nome[k++] = c;
			else
			{
				nome[k] = 0;
				if(nome[0] == '>')
					op = nome[1]?2:1;
				else if(nome[0] == '<')
					op = nome[1]?4:3;
				else if(nome[0] == '=')
					op = 5;
				else if(nome[0] != '+')
					sum1 += Mapa[str = nome];
				k = 0;
			}
		}
		for(i = 0; i < k; i++)
			sum2 = 10*sum2 + nome[i] - '0';
		sum2 *= 10;
		if(op == 1)
			bol = (sum1 > sum2);
		else if(op == 2)
			bol = (sum1 >= sum2);
		else if(op == 3)
			bol = (sum1 < sum2);
		else if(op == 4)
			bol = (sum1 <= sum2);
		else
			bol = (sum1 == sum2);
		printf("Guess #%d was %s.\n", K++, bol?"correct":"incorrect");
	}
	return 0;
}

