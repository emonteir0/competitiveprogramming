// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Natural Sum
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1805

#include<stdio.h>
 
main()
{
    unsigned long long int a,b;
    scanf("%llu %llu",&a,&b);
    printf("%llu\n",(b-a+1)*(b+a)/2);
    return 0;
}
