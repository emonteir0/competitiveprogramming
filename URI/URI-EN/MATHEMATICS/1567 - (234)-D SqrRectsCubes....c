// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: (2/3/4)-D Sqr/Rects/Cubes...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1567

#define ll long long int
#include<stdio.h>

main()
{
	ll n,x,y,z;
	while(scanf("%lld",&n)==1)
	{
		x=(n*(n+1))/2;
		y=(n*(n+1)*(2*n+1))/6;
		z=(n*(n+1)*(2*n+1)*(3*n*n+3*n-1))/30;
		printf("%lld %lld %lld %lld %lld %lld\n",y,x*x-y,x*x,x*x*x-x*x,z,x*x*x*x-z);
	}
	return 0;
}
