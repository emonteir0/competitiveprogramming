# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: melborP A
# Level: 3
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2066

def func(n,i):
	if n<2:
		return 0
	else:
		if i == 0:
			return 10**(n-2) * 36 + 9 * func(n-2, 1)
		else:
			return 10**(n-2) * 45 + 10 * func(n-2, 1)
		
x = input()
print func(x,0)

