// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Halloween Treats
// Level: 8
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1656

#include<stdio.h>

int a[100000], sum[100001], mod[100000];

int main()
{
    int i, j, N, C;
    while(scanf("%d %d", &C, &N) == 2)
    {    
        for(i = 0; i < N; i++) 
			scanf("%d", &a[i]);
        for(i = 0; i < C; i++)
        	mod[i] = -1;
        sum[0] = mod[0] = 0;
        for(i = 1; i <= N; i++)
		{
            sum[i] = (sum[i-1]+a[i-1]) % C;
            if(mod[sum[i]] != -1)
			{
                for(j = mod[sum[i]]+1; j <= i; j++)
				{
                    printf("%d",j);
                    if(j != i) 
						printf(" ");
                }
                printf("\n");
                break;
            }
            mod[sum[i]] = i;
        }
    }    
    return 0;
}
