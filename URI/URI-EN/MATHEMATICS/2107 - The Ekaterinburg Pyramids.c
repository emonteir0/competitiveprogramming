// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Ekaterinburg Pyramids
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2107

#include<stdio.h>

typedef struct
{
	int x, y, z;
} ponto;



int solve(ponto va, ponto vb, ponto vx)
{
	int res;
	res = va.x*(vb.y*vx.z-vb.z*vx.y) + va.y*(vb.z*vx.x - vb.x*vx.z) + va.z*(vb.x*vx.y - vb.y*vx.x);
	if(res > 0)	return 1;
	if(res < 0) return -1;
	return 0;
}

ponto diff(ponto A, ponto B)
{
	ponto C;
	C.x = A.x-B.x;
	C.y = A.y-B.y;
	C.z = A.z-B.z;
	return C;
}

main()
{
	ponto A, B, C, D, X;
	while(scanf("%d %d %d", &A.x, &A.y, &A.z) == 3)
	{
		scanf("%d %d %d", &B.x, &B.y, &B.z);
		scanf("%d %d %d", &C.x, &C.y, &C.z);
		scanf("%d %d %d", &D.x, &D.y, &D.z);
		scanf("%d %d %d", &X.x, &X.y, &X.z);
		printf("%s", (solve(diff(B,D),diff(C,D),diff(A,D)) == -solve(diff(B,D),diff(C,D),diff(X,D)))?"S":"N");
		printf("%s", (solve(diff(A,D),diff(C,D),diff(B,D)) == -solve(diff(A,D),diff(C,D),diff(X,D)))?"S":"N");
		printf("%s", (solve(diff(A,D),diff(B,D),diff(C,D)) == -solve(diff(A,D),diff(B,D),diff(X,D)))?"S":"N");
		printf("%s\n", (solve(diff(A,C),diff(B,C),diff(D,C)) == -solve(diff(A,C),diff(B,C),diff(X,C)))?"S":"N");

	}
	return 0;
}

