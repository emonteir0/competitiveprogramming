// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pens
// Level: 2
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2494

#include<stdio.h>

int mdc(int a, int b)
{
    if(b == 0)
        return a;
    return mdc(b, a%b);
}

main()
{
    int a, b, c, md;
    while(scanf("%d %d %d", &a, &b, &c) != EOF)
    {
        md = mdc(a, b);
        printf("%s\n", ((a+b)/md >= c) ? "sim" : "nao");
    }
    return 0;
}
