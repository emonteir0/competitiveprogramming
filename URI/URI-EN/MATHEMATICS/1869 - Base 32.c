// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Base 32
// Level: 1
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1869

#include<stdio.h>
#include<string.h>

char *strrev(char *str)
{
      char *p1, *p2;
 
      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

main()
{
	unsigned long long int a;
	char vet[33];
	int b,k;
	while(scanf("%llu",&a)==1&&(a!=0))
	{
		k=0;
		while(a!=0)
		{
			b=a%32;
			a/=32;
			if(b>9)
				vet[k++]=b+'A'-10;
			else
				vet[k++]=b+'0';
		}
		vet[k]=0;
		strrev(vet);
		printf("%s\n",vet);
	}
	printf("0\n");
	return 0;
}
