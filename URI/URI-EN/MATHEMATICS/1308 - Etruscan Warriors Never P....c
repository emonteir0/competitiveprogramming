// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Etruscan Warriors Never P...
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1308

#include<stdio.h>
#include<math.h>

main()
{
	int n;
	long long int i;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%lld",&i);
		printf("%.0lf\n",floor((-1+sqrt(1+8*i))/2));
	}
	return 0;
}
