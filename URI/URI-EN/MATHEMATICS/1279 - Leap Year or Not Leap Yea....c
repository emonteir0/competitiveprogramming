// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Leap Year or Not Leap Yea...
// Level: 4
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1279

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
 
int main()
{
   int a, b, c, d, e = 0, f, g, h, par, impar;
   char ano[10000];
   while(scanf("%s", ano) != EOF)
   {
      par = impar = 0;
      f = strlen(ano);
      for (h = 0; h < f; h ++)
         if (h % 2 == 0)
            impar += ano[h] - 48;
         else
            par += ano[h] - 48;
      b = c = d = 0;
      if (e == 1)
         printf("\n");
      e = 1;
      a = (ano[f - 1] - 48) + ((ano[f - 2] - 48) * 10) + ((ano[f - 3] - 48) * 100) + ((ano[f - 4] - 48) * 1000);
      if (a % 4 == 0 && (a % 400 == 0 || a % 100 != 0))
      {
         printf("This is leap year.\n");
         b = 1;
      }
      if ((ano[f - 1] == '5' || ano[f - 1] == '0') && (par + impar) % 3 == 0)
      {
         printf("This is huluculu festival year.\n");
         c = 1;
      }
      if (b == 1 && (ano[f - 1] == '5' || ano[f - 1] == '0'))
      {
         if (impar - par == 0 || impar - par % 11 == 0)
         {
            printf("This is bulukulu festival year.\n");
            d = 1;
         }
      }
      if (b == 0 && c == 0 && d == 0)
         printf("This is an ordinary year.\n");   
   }
 
   return 0;
}
