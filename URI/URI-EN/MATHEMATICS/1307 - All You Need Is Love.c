// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: All You Need Is Love
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1307

#include<stdio.h>

int num(char *v)
{
	int n=0;
	char *x;
	x=v;
	while(*x!=0)
	{
		n=2*n+*x-'0';
		x++;
	}
	return n;
}

main()
{
	int i,n,s1,s2,aux;
	char v1[31],v2[31];
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		scanf("%s %s",v1,v2);
		s1=num(v1);
		s2=num(v2);
		while(s2!=0)
		{
			aux=s2;
			s2=s1%s2;
			s1=aux;
		}
		if(s1==1)
			printf("Pair #%d: Love is not all you need!\n",i);
		else
			printf("Pair #%d: All you need is love!\n",i);
	}
	return 0;
}
