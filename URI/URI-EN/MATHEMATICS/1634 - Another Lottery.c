// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Another Lottery
// Level: 3
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1634

#include<stdio.h>
 
long long int mat[10000][30];
 
long long int mdc(long long int a, long long int b)
{
    long long int aux;
    while(b!=0)
    {
        aux=b;
        b=a%b;
        a=aux;
    }
    return a;
}
 
main()
{
    int n,m,i,j,ind;
    long long int x,acum,md;
    while(scanf("%d %d",&n,&m)==2&&(n||m))
    {
        ind=0;
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                scanf("%lld",&mat[i][j]);
            }
        }
        acum=0;
        for(i=0;i<n;i++)
        {
            acum+=mat[i][m-1];
        }
        for(i=0;i<n;i++)
        {
            md=mdc(mat[i][m-1],acum);
            printf("%lld / %lld\n",mat[i][m-1]/md,acum/md);
        }
    }
    return 0;
}
