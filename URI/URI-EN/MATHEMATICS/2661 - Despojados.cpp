// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Despojados
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2661

#include<bits/stdc++.h>
#define ll long long
#define MAXN 1000000
#define SQN 1000

using namespace std;

int primos[MAXN+1], prime_cont = 0;
int isprime[MAXN+1];

void crivo()
{
	ll i, j;
	primos[prime_cont++] = 2;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= SQN; i += 2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			for(j = i*i; j <= MAXN; j+=i)
				isprime[j] = 1;
		}
	}
	for(; i <= MAXN; i += 2)
		if(!isprime[i])
			primos[prime_cont++] = i;
}

int main()
{
	ll N;
	int c = 0, i;
	crivo();
	scanf("%lld", &N);
	for(i = 0; i < prime_cont; i++)
	{
		if(N % primos[i] == 0)
		{
			while(N % primos[i] == 0)
				N /= primos[i];
			c++;
		}
	}
	if(N > 1)
		c++;
	if(c < 2)
		printf("0\n");
	else
		printf("%d\n", (1 << c) - c - 1);
	return 0;
}
