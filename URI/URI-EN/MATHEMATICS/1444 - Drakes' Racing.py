# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Drakes' Racing
# Level: 2
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1444

n=0
cont=0

while True:
	n=input()
	if n==0:
		break
	cont=0
	while n!=0:
		if n==2:
			cont+=1
			break
		if n==1:
			break
		n=(n/3)+(n%3>0)
		cont+=n
	print cont

