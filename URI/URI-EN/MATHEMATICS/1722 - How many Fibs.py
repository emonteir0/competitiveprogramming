# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: How many Fibs?
# Level: 3
# Category: MATHEMATICS
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1722

fibs = [1,2]

def binarysearch(chave):
	inf = 0
	sup = 479
	while inf <= sup:
		mid = (inf + sup)/2
		if fibs[mid] > chave:
			sup = mid - 1
		elif fibs[mid] < chave:
			inf = mid + 1
		else:
			return mid
	return mid

i = 1
lim = 10**100
while fibs[i] < lim:
	fibs.append(fibs[i] + fibs[i-1])
	i += 1

while True:
	A, B = raw_input().split(' ')
	A = int(A)
	B = int(B)
	if A == 0 and B ==0:
		break
	ia = binarysearch(A)
	ib = binarysearch(B)
	while fibs[ia] > A and ia > 0:
		ia -= 1
	while fibs[ia] < A:
		ia += 1
	while fibs[ib] < B and ia < 479:
		ib += 1
	while fibs[ib] > B:
		ib -= 1
	print ib-ia+1

