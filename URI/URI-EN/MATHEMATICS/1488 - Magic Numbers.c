// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Magic Numbers?
// Level: 5
// Category: MATHEMATICS
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1488

#include<stdio.h>


main()
{
    int N,n,i,a3,k=0,m=0;
    while(scanf("%d",&n)==1&&n>0)
    {
        m=0;
        for(i=1;i*i<=n;i++)
        {
            if(n%((i+1)*(i+1))==0)
            {
                m=i;
            }
        }
        if(m==0) printf("Instancia %d\n%d nao e quadripartido\n\n",++k,n);
        else
        {
            a3=n/((m+1)*(m+1));
            printf("Instancia %d\n%d %d %d %d %d\n\n",++k,m,(a3-1)*m,(a3+1)*m,a3,a3*m*m);
        }
    }
    return 0;
}

