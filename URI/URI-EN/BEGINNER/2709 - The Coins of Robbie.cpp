// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Coins of Robbie
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2709

// Crivo de Erastótenes

#include<bits/stdc++.h>
#define ll long long
#define MAXN 10000
#define SQN 100

using namespace std;

int isprime[MAXN+1];
int vet[20];

void crivo()
{
	ll i, j;
	//isprime[0] = 1;
	isprime[1] = 1;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= SQN; i += 2)
	{
		if(!isprime[i])
		{
			for(j = i*i; j <= MAXN; j += i)
				isprime[j] = 1;
		}
	}
}


main()
{
	int i, x, N, M;
	crivo();
	x = 0;
	while(scanf("%d", &N) == 1)
	{
		x = 0;
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		scanf("%d", &M);
		for(i = N-1; i >= 0; i -= M)
			x += vet[i];
		printf("%s\n", (isprime[x]) ? "Bad boy! I’ll hit you." : "You’re a coastal aircraft, Robbie, a large silver aircraft.");
	}
	return 0;
}
