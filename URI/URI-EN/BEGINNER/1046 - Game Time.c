// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game Time
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1046

#include<stdio.h>

main()
{
	int a,b;
	scanf("%d %d",&a,&b);
	printf("O JOGO DUROU %d HORA(S)\n",(a>b)?24+b-a:b-a);
	return 0;
}
