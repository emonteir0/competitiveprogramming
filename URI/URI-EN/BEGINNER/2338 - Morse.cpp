// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Morse
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2338

#include<bits/stdc++.h>

using namespace std;

map<string, char> Mapa;

main()
{
	int N, i, tam;
	char palavra[1001];
	string str, str2;
	Mapa["...."] = ' ';
	Mapa["=.===..."] = 'a';
	Mapa["===.=.=.=..."] = 'b';
	Mapa["===.=.===.=..."] = 'c';
	Mapa["===.=.=..."] = 'd';
	Mapa["=..."] = 'e';
	Mapa["=.=.===.=..."] = 'f';
	Mapa["===.===.=..."] = 'g';
	Mapa["=.=.=.=..."] = 'h';
	Mapa["=.=..."] = 'i';
	Mapa["=.===.===.===..."] = 'j';
	Mapa["===.=.===..."] = 'k';
	Mapa["=.===.=.=..."] = 'l';
	Mapa["===.===..."] = 'm';
	Mapa["===.=..."] = 'n';
	Mapa["===.===.===..."] = 'o';
	Mapa["=.===.===.=..."] = 'p';
	Mapa["===.===.=.===..."] = 'q';
	Mapa["=.===.=..."] = 'r';
	Mapa["=.=.=..."] = 's';
	Mapa["===..."] = 't';
	Mapa["=.=.===..."] = 'u';
	Mapa["=.=.=.===..."] = 'v';
	Mapa["=.===.===..."] = 'w';
	Mapa["===.=.=.===..."] = 'x';
	Mapa["===.=.===.===..."] = 'y';
	Mapa["===.===.=.=..."] = 'z';
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%s", palavra);
		str = palavra;
		str2 = "";
		tam = str.length();
		for(i = 0; i < tam; i++)
		{
			str2 += palavra[i];
			if(Mapa[str2] != 0)
			{
				printf("%c", Mapa[str2]);
				str2 = "";
			}
		}
		printf("%c\n", Mapa[str2 + "..."]);
	}
	return 0;
}

