// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pepe, I Already Took the ...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2152

#include<stdio.h>

main()
{
	int a, b, c, n;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d %d",&a, &b, &c);
		printf("%02d:%02d - A porta %s!\n",a,b,c?"abriu":"fechou");
	}
	return 0;
}

