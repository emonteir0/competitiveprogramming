// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Greyscale
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2630

#include <bits/stdc++.h>
using namespace std;

int tc;

int main() {
    int t;
    cin >> t;
    while (t--) {
        string cmd;
        cin >> cmd;
        double r, g, b;
        cin >> r >> g >> b;
        int ans;
        if (cmd == "min") {
            ans = min(r, min(g, b));
        } else if (cmd == "max") {
            ans = max(r, max(g, b));
        } else if (cmd == "mean") {
            ans = (r+g+b)/3.0;
        } else {
            ans = (0.3*r+0.59*g+0.11*b);
        }
        cout << "Caso #" << ++tc << ": " << ans << endl;
    }
    return 0;
}

