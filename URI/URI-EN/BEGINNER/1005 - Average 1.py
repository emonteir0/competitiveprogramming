# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Average 1
# Level: 1
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1005

A = None
B = None

def read_numeric():
  try:
    # read for Python 2.x
    return float(raw_input())
  except NameError:
    # read for Python 3.x
    return float(input())


A = read_numeric()
B = read_numeric()
print(str("MEDIA = ") + str("{:0.5f}".format(((A * 3.5 + B * 7.5) / 11))))

