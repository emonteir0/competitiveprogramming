// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Islands
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2784

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

vector<int> G[1001];
vector<int> W[1001];
int N;
int dist[1001];

int dijkstra(int st)
{
	int ma = 0, me = INF;
	priority_queue< pair<int, int> > pq;
	int u, v, w;
	for(int i = 1; i <= N; i++)
		dist[i] = INF;
	dist[st] = 0;
	pq.push(make_pair(0, st));
	while(!pq.empty())
	{
		w = -pq.top().first;
		u = pq.top().second;
		pq.pop();
		if(w > dist[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			if(dist[v] > dist[u] + W[u][i])
			{
				dist[v] = dist[u] + W[u][i];
				pq.push(make_pair(-dist[v], v));
			}
		}
	}
	for(int i = 1; i <= N; i++)
	{
		if(i == st)
			continue;
		me = min(me, dist[i]);
		ma = max(ma, dist[i]);
	}
	return ma-me;
}

main()
{
	int M, u, v, w, st;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d %d %d", &u, &v, &w);
		G[u].push_back(v);
		G[v].push_back(u);
		W[u].push_back(w);
		W[v].push_back(w);
	}
	scanf("%d", &st);
	printf("%d\n", dijkstra(st));
	return 0;
}
