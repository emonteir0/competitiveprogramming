// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Messaging
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2852

#include<bits/stdc++.h>

using namespace std;

char cifra[100];
char mapa[52];
int N;
char texto[100001];
bool flag = 0, vogal;

bool vowel(char c)
{
	if(c == 'a')
		return 1;
	if(c == 'e')
		return 1;
	if(c == 'i')
		return 1;
	if(c == 'o')
		return 1;
	if(c == 'u')
		return 1;
	return 0;
}

int main()
{
	int ptr, M;
	scanf("%s%*c", cifra);
	scanf("%d", &N);
	for(int i = 0; i < 26; i++)
		mapa[i] = mapa[i+26] = i+'a';
	M = strlen(cifra);
	while(N--)
	{
		scanf(" %[A-Z a-z]", texto);
		flag = 0;
		vogal = 0;
		ptr = 0;
		for(int i = 0; texto[i]; i++)
		{
			if(!flag)
			{
				if(i != 0)
					printf(" ");
				flag = 1;
				vogal = vowel(texto[i]);
				//printf("VOGAL: %d\n", vogal);
				if(vogal)
				{
					printf("%c", texto[i]);
				}
				else
				{
					printf("%c", mapa[(texto[i]-'a')+(cifra[ptr]-'a')]);
					ptr++;
					if(ptr == M)
						ptr = 0;
				}
			}
			else
			{
				if(texto[i] == ' ')
				{
					flag = 0;
				}
				else if(vogal)
				{
					printf("%c", texto[i]);
				}
				else
				{
					printf("%c", mapa[(texto[i]-'a')+(cifra[ptr]-'a')]);
					ptr++;
					if(ptr == M)
						ptr = 0;
				}
			}
		}
		printf("\n");
	}
	return 0;
}

