// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jumping Frog
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2704

#include<bits/stdc++.h>

using namespace std;

char str[100001];
char isvalid[100001];

main()
{
    int i, j, k, ans = 0, N;
    scanf("%s", str);
    N = strlen(str);
    for(i = 0; i < N; i++)
        if(str[i] != 'R')
            break;
    if(i == N)
        printf("%d\n", N-1);
    else
    {
        for(i = 2; i < N; i++)
        {
            if(N%i == 0)
            {
                for(j = 0; j < i; j++)
                {
                    k = j;
                    while(1)
                    {
                        k += i;
                        if(k >= N)
                            k -= N;
                        if(k == j || str[k] == 'P')
                            break;
                    }
                    if(str[k] == 'R')
                    {
                        for(k = i; k <= N; k += i)
                            isvalid[k] = 1;
                        break;
                    }
                }
            }
        }
        for(i = 1; i < N; i++)
            ans += isvalid[i];
        printf("%d\n", ans);
    }
    return 0;
}
