// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jogo do Operador
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2493

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct
{
	int a, b, c;
} expressao;

expressao vet[51], ic;
char passados[51][51];

int cmp (const void * a, const void * b)
{
  return strcmp((char*)a, (char*)b);
}


main()
{
	int i, x, N, tam, ind;
	char nome[51], op;
	while(scanf("%d%*c", &N) == 1)
	{
		tam = 0;
		for(i = 1; i <= N; i++)
			scanf("%d %d=%d%*c", &vet[i].a, &vet[i].b, &vet[i].c);
		for(i = 1; i <= N; i++)
		{
			scanf("%s %d %c%*c", nome, &ind, &op);
			x = 0;
			ic = vet[ind];
			if(op == '+')
				x = ic.a+ic.b == ic.c;
			else if(op == '-')
				x = ic.a - ic.b == ic.c;
			else if(op == '*')
				x = ic.a * ic.b == ic.c;
			else
			{
				x |= ic.a+ic.b == ic.c;
				x |= ic.a - ic.b == ic.c;
				x |= ic.a * ic.b == ic.c;
				x = !x;
			}
			if(!x)
				strcpy(passados[tam++], nome);
		}
		if(tam == N)
			printf("None Shall Pass!\n");
		else if(tam == 0)
			printf("You Shall All Pass!\n");
		else
		{
			qsort(passados, tam, sizeof(passados[0]), cmp);
			printf("%s", passados[0]);
			for(i = 1; i < tam; i++)
					printf(" %s", passados[i]);
			printf("\n");
		}
	}
	return 0;
}
