// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Converting to Hexadecimal
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1957

#include <stdio.h>


int main()
{
 	int x;
	scanf("%d",&x);
	printf("%X\n",x);   
    return 0;
}
