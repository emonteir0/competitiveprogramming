// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Highest and Position
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1080

#include<stdio.h>

main()
{
	int n,i,ma=0,ind=0;
	for(i=1;i<=100;i++)
	{
		scanf("%d",&n);
		if(n>ma)
		{
			ma=n;
			ind=i;
		}
	}
	printf("%d\n%d\n",ma,ind);
	return 0;
}
