// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Interval 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1072

#include<stdio.h>

main()
{
	int x,i,n,cont=0,cont2=0;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d",&x);
		if(x>=10&&x<=20)	cont++;
		else cont2++;
	}
	printf("%d in\n%d out\n",cont,cont2);
	return 0;
}
