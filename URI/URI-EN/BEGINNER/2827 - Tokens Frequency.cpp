// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tokens Frequency
// Level: 3
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2827

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;

char str[100001];

char lower(char c)
{
	if(c >= 'A' && c <= 'Z')
		return c-'A'+'a';
	return c;
}

main()
{
	int x, ma = 0;
	char pad[3], ans[3];
	pad[2] = 0;
	ans[0] = 127, ans[1] = 127, ans[2] = 0;
	gets(str);
	for(int i = 1; str[i]; i++)
	{
		pad[0] = lower(str[i-1]);
		pad[1] = lower(str[i]);
		x = ++Mapa[pad];
		if(x > ma)
		{
			ma = x;
			strcpy(ans, pad);
		}
		else if(x == ma)
		{
			if(strcmp(ans, pad) > 0)
				strcpy(ans, pad);
		}
	}
	printf("%s:%d\n", ans, ma);
	return 0;
}
