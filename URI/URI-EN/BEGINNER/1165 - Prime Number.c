// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Prime Number
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1165

#include<stdio.h>

main()
{
	int n,a,i;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&a);
		if(a==1)
			printf("1 nao eh primo\n");
		else
		{
			for(i=2;i*i<=a;i++)
			{
				if(a%i==0)
				{
					printf("%d nao eh primo\n",a);
					break;
				}
			}
			if(i*i>a)	printf("%d eh primo\n",a);
		}	
	}
	return 0;
}
