// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of Array
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2710

// BIT 2D

#include<stdio.h>
 
int N, M;
int BIT[1001][1001];

void updateBIT(int x, int y0, int val)
{
	int y;
    for (; x <= N; x += (x & -x))
    {
    	y = y0;
        for (; y <= M; y += (y & -y))
            BIT[x][y] += val;
    }
    return;
}
 
int getSum(int x, int y0)
{
    int sum = 0, y;
    for(; x > 0; x -= (x & -x))
    {
    	y = y0;
        for(; y > 0; y -= (y & -y))
            sum += BIT[x][y];
	}
    return sum;
}
 
int main()
{
	int Q, xa, xb, ya, yb, v, a, b, c, d;
	char op;
	N = 500;
	M = 500;
	scanf("%d%*c", &Q);
	while(Q--)
	{
		scanf("%c", &op);
		if(op == 'U')
		{
			scanf("%d %d %d %d %d%*c", &xa, &ya, &xb, &yb, &v);
			updateBIT(xa, ya, v);
			updateBIT(xb+1, yb+1, v);
			updateBIT(xb+1, ya, -v);
			updateBIT(xa, yb+1, -v);
		}
		else
		{
			scanf("%d %d%*c", &xa, &ya);
			printf("%d\n", getSum(xa, ya));
		}
	}
    return 0;
}
