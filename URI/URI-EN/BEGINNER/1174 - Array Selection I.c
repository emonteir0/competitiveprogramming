// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Selection I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1174

#include<stdio.h>
#include<math.h>

const double EPS = 1e-9;

main()
{
	int i;
	double a;
	for(i=0;i<100;i++)
	{
		scanf("%lf",&a);
		if(a<=10)	printf("A[%d] = %.*lf\n",i,((fabs(a) - abs((int)a) > EPS)) ? (1) : (0),a);
	}
	return 0;
}
