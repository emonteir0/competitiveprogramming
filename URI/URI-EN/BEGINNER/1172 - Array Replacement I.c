// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Replacement I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1172

#include<stdio.h>

main()
{
	long long int a;
	int n,i,vet[10];
	for(i=0;i<10;i++)
	{
		scanf("%d",&vet[i]);
		if(vet[i]<=0)	vet[i]=1;
		printf("X[%d] = %d\n",i,vet[i]);
	}
	return 0;
}
