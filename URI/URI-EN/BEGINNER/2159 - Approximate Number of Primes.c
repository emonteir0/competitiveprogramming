// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Approximate Number of Primes
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2159

#include<stdio.h>
#include<math.h>

main()
{
	double n;
	scanf("%lf",&n);
	printf("%.1lf %.1lf\n",n/log(n),1.25506*n/log(n));
	return 0;
}

