// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Closing Tabs
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2061

#include<stdio.h>

main()
{
	int m, n;
	char vet[6];
	scanf("%d %d%*c",&m,&n);
	while(n--)
	{
		scanf("%s",vet);
		if(vet[0]=='f')
			m++;
		else
			m--;
	}
	printf("%d\n",m);
	return 0;
}

