# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Little Ducks
# Level: 2
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2334

while True:
    x = input()
    if x == -1:
        break
    if x > 1:
        print x-1
    else:
        print 0
