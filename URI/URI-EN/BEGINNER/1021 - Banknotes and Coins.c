// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Banknotes and Coins
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1021

#include<stdio.h>

main()
{
	int b,c;
	scanf("%d%*c%d",&b,&c);
	printf("NOTAS:\n");
	printf("%d nota(s) de R$ 100.00\n",b/100);
	b%=100;
	printf("%d nota(s) de R$ 50.00\n",b/50);
	b%=50;
	printf("%d nota(s) de R$ 20.00\n",b/20);
	b%=20;
	printf("%d nota(s) de R$ 10.00\n",b/10);
	b%=10;
	printf("%d nota(s) de R$ 5.00\n",b/5);
	b%=5;
	printf("%d nota(s) de R$ 2.00\n",b/2);
	b%=2;
	c+=b*100;
	printf("MOEDAS:\n");
	printf("%d moeda(s) de R$ 1.00\n",c/100);
	c%=100;
	printf("%d moeda(s) de R$ 0.50\n",c/50);
	c%=50;
	printf("%d moeda(s) de R$ 0.25\n",c/25);
	c%=25;
	printf("%d moeda(s) de R$ 0.10\n",c/10);
	c%=10;
	printf("%d moeda(s) de R$ 0.05\n",c/5);
	c%=5;
	printf("%d moeda(s) de R$ 0.01\n",c);
	return 0;
}
