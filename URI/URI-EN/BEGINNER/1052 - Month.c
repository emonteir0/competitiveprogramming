// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Month
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1052

#include<stdio.h>

main()
{
	char vet[12][20]={"January","February","March","April","May","June","July","August","September","October","November","December"};
	int a;
	scanf("%d",&a);
	printf("%s\n",vet[a-1]);
	return 0;
}
