// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Diving
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2311

#include<stdio.h>

main()
{
	int i, K;
	double c, x, acum, ma, me;
	char nome[101];
	scanf("%d%*c", &K);
	while(K--)
	{
		scanf("%s%*c", nome);
		scanf("%lf%*c", &x);
		acum = ma = 0;
		me = 11;
		for(i = 0; i < 7; i++)
		{
			scanf("%lf%*c", &c);
			acum += c;
			ma = (ma>c) ? ma : c;
			me = (me<c) ? me : c;
		}
		printf("%s %.2lf\n", nome, x*(acum-ma-me));
	}
	return 0;
}

