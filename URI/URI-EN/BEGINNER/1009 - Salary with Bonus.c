// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Salary with Bonus
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1009

#include<stdio.h>

main()
{
	char a[101];
	double b,c;
	scanf("%s %lf %lf",&a,&b,&c);
	printf("TOTAL = R$ %.2f\n",b+0.15*c);
	return 0;
}
