// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Matrix III
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1557

#include<stdio.h>

main()
{
	int i,j,k,n,x,a,cont,l,cont2;
	while(scanf("%d",&n)==1&&n>0)
	{
		a=1;
		for(i=0;i<n;i++)
		{
			x=a;
			l=1<<(n*2-2);
			cont=0;
			while(l>0)
			{
				l/=10;
				cont++;
			}
			for(j=0;j<n;j++)
			{
				if(j!=0)
					printf(" ");
				l=x;
				cont2=0;
				while(l>0)
				{
					l/=10;
					cont2++;
				}
				l=cont-cont2;
				for(k=0;k<l;k++)
					printf(" ");
				printf("%d",x);
				x=x<<1;
			}
			printf("\n");
			a=a<<1;
		}
		printf("\n");
	}
	return 0;
}
