# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Twilight at Portland
# Level: 1
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2168

n = input()

mat = []

dx = [0,0,1,1]
dy = [0,1,0,1]

for i in range(0,n+1):
	mat.append([0]*(n+1))
	line = raw_input().split(' ')
	for j in range(0,n+1):
		mat[i][j] = int(line[j])

for i in range(0,n):
	out = ''
	for j in range(0,n):
		cont = 0
		for k in range(0,4):
			cont += mat[i+dx[k]][j+dy[k]]
		out += 'S' if cont>=2 else 'U'
	print out
		

