// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fuel Spent
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1017

#include<stdio.h>

main()
{
	int a,b;
	scanf("%d %d",&a,&b);
	printf("%.3f\n",a*b/12.000);
	return 0;
}
