// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Whose Turn Is It?
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1914

#include<stdio.h>
#include<string.h>

main()
{
	int x,y,n;
	char a[101],b[6],c[101];
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%s %s %s %*s%*c%d %d%*c",a,b,c,&x,&y);
		x=((x%2)+(y%2))%2;
		printf("%s\n",(x==(strcmp(b,"PAR")==0))?c:a);
	}
	return 0;
}
