// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Change Continues!!
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2686

#include<bits/stdc++.h>

using namespace std;

void printhorario(int x)
{
	if(x < 21600)
		printf("De Madrugada!!\n");
	else if(x >= 21600 && x < 43200)
		printf("Bom Dia!!\n");
	else if(x >= 43200 && x < 64800)
		printf("Boa Tarde!!\n");
	else
		printf("Boa Noite!!\n");
	printf("%02d:%02d:%02d\n", x/3600, (x/60)%60, x%60);
}

main()
{
	double x, y;
	while(scanf("%lf", &x) == 1)
	{
		y = (int(x*240) + 21600) % 86400;
		printhorario(y);
	}
	return 0;
}
