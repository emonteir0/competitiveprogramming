// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Leader's Impeachment
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2540

#include <bits/stdc++.h>
using namespace std;

#define int long long

signed main() {
	int n, k, s;
	while (cin >> n) {
		s = 0;
		for (int i = 0; i < n; i++) {
			cin >> k;
			s += k;
		}
		cout << (s >= (2.0*n)/3.0 ? "impeachment" : "acusacao arquivada") << endl;
	}
	
	return 0;
}
