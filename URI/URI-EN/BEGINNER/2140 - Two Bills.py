# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Two Bills
# Level: 1
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2140

lista = [7, 12, 22, 52, 102, 15, 25, 55, 105, 30, 60, 110, 70, 120, 150]

while True:
	a, b = raw_input().split(' ')
	a = int(a)
	b = int(b)
	if a == 0 and b == 0:
		break
	if b-a in lista:
		print 'possible'
	else:
		print 'impossible'
	

