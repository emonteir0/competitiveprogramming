// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Race of Slugs
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1789

#include<stdio.h>

main()
{
	int n,x,l;
	while(scanf("%d",&n)==1)
	{
		l=0;
		while(n--)
		{
			scanf("%d",&x);
			if(x>l)
				l=x;
		}
		if(l<10)		printf("1\n");
		else if(l<20)	printf("2\n");
		else 			printf("3\n");
	}
	return 0;
}
