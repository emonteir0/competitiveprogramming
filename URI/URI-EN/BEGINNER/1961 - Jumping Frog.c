// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jumping Frog
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1961

#include<stdio.h>

int abs(int x)
{
	if(x<0)	return -x;
	return x;
}

main()
{
	int n,m,x,y,b=1;
	scanf("%d %d",&m,&n);
	scanf("%d",&x);
	n-=1;
	while(n--)
	{
		scanf("%d",&y);
		if(abs(x-y)>m)
		{
			b=0;
		}
		x=y;
	}
	printf("%s\n",b?"YOU WIN":"GAME OVER");
	return 0;
}
