// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: New Password RA
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2690

#include<bits/stdc++.h>

using namespace std;

map<char, char> Mapa;

char alpha(char c)
{
	return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'));
}

main()
{
	int i, N, cont;
	char msg[101];
	Mapa['G'] = Mapa['Q'] = Mapa['a'] = Mapa['k'] = Mapa['u'] = '0';
	Mapa['I'] = Mapa['S'] = Mapa['b'] = Mapa['l'] = Mapa['v'] = '1';
	Mapa['E'] = Mapa['O'] = Mapa['Y'] = Mapa['c'] = Mapa['m'] = Mapa['w'] = '2';
	Mapa['F'] = Mapa['P'] = Mapa['Z'] = Mapa['d'] = Mapa['n'] = Mapa['x'] = '3';
	Mapa['J'] = Mapa['T'] = Mapa['e'] = Mapa['o'] = Mapa['y'] = '4';
	Mapa['D'] = Mapa['N'] = Mapa['X'] = Mapa['f'] = Mapa['p'] = Mapa['z'] = '5';
	Mapa['A'] = Mapa['K'] = Mapa['U'] = Mapa['g'] = Mapa['q'] = '6';
	Mapa['C'] = Mapa['M'] = Mapa['W'] = Mapa['h'] = Mapa['r'] = '7';
	Mapa['B'] = Mapa['L'] = Mapa['V'] = Mapa['i'] = Mapa['s'] = '8';
	Mapa['H'] = Mapa['R'] = Mapa['j'] = Mapa['t'] = '9';
	scanf("%d%*c", &N);
	while(N--)
	{
		cont = 0;
		scanf(" %[^\n]s", msg);
		for(i = 0; msg[i]; i++)
			if(alpha(msg[i]))
			{
				if(cont < 12)
				{
					printf("%c", Mapa[msg[i]]);
					cont++;
				}
			}
		printf("\n");
	}
	return 0;
}

