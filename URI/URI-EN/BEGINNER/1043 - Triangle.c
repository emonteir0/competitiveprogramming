// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangle
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1043

#include<stdio.h>

double absol(double x)
{
	if(x<0)	return -x;
	return x;
}

main()
{
	double a,b,c;
	scanf("%lf %lf %lf",&a,&b,&c);
	if(a<b+c&&a>absol(b-c)) printf("Perimetro = %.1f\n",a+b+c);
	else	printf("Area = %.1f\n",(a+b)*c/2);
	return 0;
}
