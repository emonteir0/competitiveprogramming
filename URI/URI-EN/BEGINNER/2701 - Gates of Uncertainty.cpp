// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Gates of Uncertainty
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2701

#include<bits/stdc++.h>
#define MAXN 100000
#define MOD 1000000007
#define ll long long

using namespace std;


vector< vector<int> > G(MAXN+1), G2(MAXN+1);
int choose[MAXN+1];
int values[MAXN+1][4];


void dfs(int u)
{
    int i;
    ll a1, b1, c1, d1, a2, b2, c2, d2;
    if(u == 0)
        return;
    dfs(G[u][0]);
    dfs(G[u][1]);
    a1 = values[G[u][0]][0];
    b1 = values[G[u][0]][1];
    c1 = values[G[u][0]][2];
    d1 = values[G[u][0]][3];
    a2 = values[G[u][1]][0];
    b2 = values[G[u][1]][1];
    c2 = values[G[u][1]][2];
    d2 = values[G[u][1]][3];
    if(choose[u] == -1)
    {
        values[u][0] = (b1*b2)%MOD;
        values[u][1] = (a1*a2 + a1*b2 + a1*c2 + a1*d2 + b1*a2 + c1*a2 + c1*d2 + d1*a2 + d1*c2)%MOD;
        values[u][2] = (b1*d2 + d1*b2 + d1*d2)%MOD;
        values[u][3] = (b1*c2 + c1*b2 + c1*c2)%MOD;
    }
    if(choose[u] == 0)
    {
        values[u][0] = (b1*b2 + b1*c2 + c1*b2 + c1*c2)%MOD;
        values[u][1] = 0;
        values[u][2] = (a1*a2 + a1*b2 + a1*c2 + a1*d2 + b1*a2 + b1*d2 + c1*a2 + c1*d2 + d1*a2 + d1*b2 + d1*c2 + d1*d2)%MOD;
        values[u][3] = 0;
    }
    if(choose[u] == 1)
    {
        values[u][0] = 0;
        values[u][1] = (a1*a2 + a1*b2 + a1*c2 + a1*d2 + b1*a2 + b1*d2 + c1*a2 + c1*d2 + d1*a2 + d1*b2 + d1*c2 + d1*d2)%MOD;
        values[u][2] = 0;
        values[u][3] = (b1*b2 + b1*c2 + c1*b2 + c1*c2)%MOD;
    }
}

main()
{
    int i, N, x, y, z;
    values[0][0] = 1;
    values[0][1] = 1;
    values[0][2] = 0;
    values[0][3] = 0;
    scanf("%d", &N);
    for(i = 1; i <= N; i++)
    {
        scanf("%d %d %d", &x, &y, &choose[i]);
        G[i].push_back(x);
        G[i].push_back(y);
        if(x)
            G2[x].push_back(i);
        if(y)
            G2[y].push_back(i);
    }
    for(i = 1; i <= N; i++)
        if(G2[i].size() == 0)
            break;
    dfs(i);
    printf("%d\n", (values[i][2]+values[i][3])%MOD);
    return 0;
}
