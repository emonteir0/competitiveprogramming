// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Web Browser
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2635

#include<bits/stdc++.h>

using namespace std;

char  vet[102], msg[102];
vector<string> mat;
vector<vector<char> > palavras(100);

int main() {
	int i, j, k, n, q;
	while (scanf("%d%*c", &n) != EOF) {
		mat.clear();
		for (i = 0; i < n; i++) {
			scanf("%s%*c", msg);
			mat.push_back(msg);
		}
		sort(mat.begin(), mat.end());
		for(i = 0; i < 100; i++)
		{
			palavras[i].clear();
			for(j = 0; j < n; j++)
				palavras[i].push_back(mat[j][i]);
			/*for(j = 0; j < n; j++)
				printf("%c ", palavras[i][j]);
			printf("\n");*/
		}
		scanf("%d%*c", &q);
		while (q--) {
			scanf("%s%*c", vet);
			int lo = 0;
			int hi = n;
			int sz = strlen(vet);
			for(k = 0; k < sz; k++)
			{
				lo = lower_bound((palavras[k]).begin()+lo, (palavras[k]).begin()+hi, vet[k])-(palavras[k]).begin();
				hi = upper_bound((palavras[k]).begin()+lo, (palavras[k]).begin()+hi, vet[k])-(palavras[k]).begin();
				if(lo > hi-1)
					break;
			}
			if(lo > hi-1)
				printf("-1\n");
			else
				printf("%d %d\n", hi-lo, mat[hi-1].size());
		}
	}
	return 0;
}
/*
5
maratonaicpc
maraton
programacao
progress
inputs
3
marat
programacao
outputs
*/
