// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Building Houses
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1541

#include<stdio.h>
#include<math.h>

main()
{
	double a,i,j,n;
	while(scanf("%lf",&i)&&i>0)
	{
		scanf("%lf %lf",&j,&n);
		a=floor(sqrt(i*j*100.0/n));
		printf("%.0lf\n",a);
	}
	return 0;
}
