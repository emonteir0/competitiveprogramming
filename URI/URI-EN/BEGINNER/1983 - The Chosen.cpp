// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Chosen
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1983

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int n;
	double x;
}pare;

pare par;
vector< pare > vet;

bool cmp (pare a, pare b)
{
	return a.x>b.x;
}

main()
{
	int i,n;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d %lf",&par.n,&par.x);
		vet.push_back(par);
	}
	sort(vet.begin(),vet.end(),cmp);
	if(vet[0].x>=8)
		printf("%d\n",vet[0].n);
	else
		printf("Minimum note not reached\n");
	return 0;
}
