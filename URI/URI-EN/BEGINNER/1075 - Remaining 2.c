// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Remaining 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1075

#include<stdio.h>

main()
{
	int x,i;
	scanf("%d",&x);
	for(i=2;i<=10000;i++)
	{
		if(i%x==2)	printf("%d\n",i);
	}
	return 0;
}
