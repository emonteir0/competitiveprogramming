// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Parity
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2176

#include<stdio.h>

main()
{
	int x=0, i=0;
	char numero[101];
	scanf("%s",numero);
	printf("%s", numero);
	while(numero[i]!=0)
		x ^= (numero[i++]=='1');
	printf("%d\n",x); 
	return 0;
}

