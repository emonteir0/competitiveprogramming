// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fast Fibonacci
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2164

#include<stdio.h>
#include<math.h>


main()
{
	double value1 = (1+sqrt(5))/2, value2 = (1-sqrt(5))/2;
	int n;
	scanf("%d",&n);
	printf("%.1lf\n",(pow(value1,n)-pow(value2,n))/sqrt(5));
	return 0;
}

