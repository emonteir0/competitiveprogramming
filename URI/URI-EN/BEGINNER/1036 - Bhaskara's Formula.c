// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bhaskara's Formula
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1036

#include<stdio.h>
#include<math.h>

main()
{
	double a,b,c,delta;
	scanf("%lf %lf %lf",&a,&b,&c);
	delta=b*b-4*a*c;
	if(delta>=0&&a!=0)
		printf("R1 = %.5f\nR2 = %.5f\n",(-b+sqrt(delta))/2/a,(-b-sqrt(delta))/2/a);
	else	printf("Impossivel calcular\n");
	return 0;
}
