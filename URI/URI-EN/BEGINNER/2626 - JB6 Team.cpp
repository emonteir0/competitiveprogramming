// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: JB6 Team
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2626

#include <bits/stdc++.h>
using namespace std;

bool result(string& a, string& b) {
    if (a == "papel" and b == "pedra") return 1;
    if (a == "pedra" and b == "tesoura") return 1;
    if (a == "tesoura" and b == "papel")  return 1;
    return 0;
}

int main() {
    string a, b, c;
    while (cin >> a >> b >> c) {
        if (a != b and b == c and result(a, b)) {
            cout << "Os atributos dos monstros vao ser inteligencia, sabedoria...\n";
        } else if (b != a and a == c and result(b, a)) {
            cout << "Iron Maiden\'s gonna get you, no matter how far!\n";
        } else if (c != a and a == b and result(c, a)) {
            cout << "Urano perdeu algo muito precioso...\n";
        } else {
            cout << "Putz vei, o Leo ta demorando muito pra jogar...\n";
        }
    }
    return 0;
}

