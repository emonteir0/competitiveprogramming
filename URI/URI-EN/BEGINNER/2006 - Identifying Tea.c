// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Identifying Tea
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2006

#include<stdio.h>

main()
{
	int n,x,cont=0,i;
	scanf("%d",&n);
	for(i=0;i<5;i++)
	{
		scanf("%d",&x);
		if(x==n)	cont++;
	}
	printf("%d\n",cont);
	return 0;
}
