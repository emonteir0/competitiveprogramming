// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Volleyball
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2310

#include<stdio.h>

int n, a, b, c, d, e, f, ca, cb, cc, cd, ce, cf;

main()
{
	scanf("%d%*c", &n);
	while(n--)
	{
		scanf("%*s");
		scanf("%d %d %d %d %d %d%*c", &a, &b, &c, &d, &e, &f);
		ca += a;
		cb += b;
		cc += c;
		cd += d;
		ce += e;
		cf += f;
	}
	printf("Pontos de Saque: %.2lf %%.\nPontos de Bloqueio: %.2lf %%.\nPontos de Ataque: %.2lf %%.\n", ((double)cd*100)/ca, ((double)ce*100)/cb, ((double)cf*100)/cc);
	return 0;
}

