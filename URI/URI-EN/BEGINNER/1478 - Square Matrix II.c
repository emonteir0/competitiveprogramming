// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Matrix II
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1478

#include<stdio.h>

int absol(int x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	int i,j,n,x;
	while(scanf("%d",&n)==1&&n)
	{
		
		for(i=0;i<n;i++)
		{
			for(j=0;j<n;j++)
			{
				x=absol(j-i)+1;
				if(j!=0)
					printf(" ");
				printf("%s%s",x/100?"":" ",x/10?"":" ");
				printf("%d",x);
			}
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}
