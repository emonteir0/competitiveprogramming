// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Motion Picture
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1963

#include<stdio.h>

main()
{
	double a,b;
	scanf("%lf %lf",&a,&b);
	printf("%.2lf%%\n",(b-a)*100/a);
	return 0;
}
