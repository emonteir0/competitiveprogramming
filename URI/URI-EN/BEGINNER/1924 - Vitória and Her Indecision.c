// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vitória and Her Indecision
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1924

#include<stdio.h>

main()
{
	int n;
	scanf("%d",&n);
	while(n--)
		scanf("%*s");
	printf("Ciencia da Computacao\n");
	return 0;
}
