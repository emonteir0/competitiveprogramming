// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Population Increase
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1160

#include<stdio.h>

main()
{
	double c,d;
	int n,e,a,b,cont;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d %lf %lf",&a,&b,&c,&d);
		c/=100.0;
		d/=100.0;
		cont=0;
		while((a<=b)&&(cont<=100))
		{
			a+=a*c;
			b+=b*d;
			cont++;
		}
		if(cont<=100)	printf("%d anos.\n",cont);
		else	printf("Mais de 1 seculo.\n");
	}
	return 0;
}
