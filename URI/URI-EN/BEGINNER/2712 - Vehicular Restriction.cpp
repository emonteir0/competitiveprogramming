// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Vehicular Restriction
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2712

#include<bits/stdc++.h>

using namespace std;

map<int, string> Mapa;

int alpha(char a, char b, char c)
{
	return a >= 'A' && a <= 'Z' && b >= 'A' && b <= 'Z' && c >= 'A' && c <= 'Z';
}

main()
{
	int i, N, x;
	char msg[101];
	Mapa[1] = Mapa[2] = "MONDAY";
	Mapa[3] = Mapa[4] = "TUESDAY";
	Mapa[5] = Mapa[6] = "WEDNESDAY";
	Mapa[7] = Mapa[8] = "THURSDAY";
	Mapa[9] = Mapa[0] = "FRIDAY";
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%s%*c", msg);
		if(alpha(msg[0], msg[1], msg[2]) && msg[3] == '-')
		{
			x = 0;
			for(i = 4; msg[i]; i++)
			{
				if(msg[i] > '9' || msg[i] < '0')
					break;
				x = 10*x + msg[i] - '0';
			}
			if(msg[i] || i != 8)
				printf("FAILURE\n");
			else
				printf("%s\n", Mapa[x%10].c_str());
		}
		else
			printf("FAILURE\n");
	}
	return 0;
}
