// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ho Ho Ho
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1759

#include<stdio.h>

main()
{
	int N,i;
	scanf("%d",&N);
	if(N>0)
		printf("Ho");
	for(i=2;i<=N;i++)
		printf(" Ho");
	if(N>0)
		printf("!\n");
	return 0;
}
