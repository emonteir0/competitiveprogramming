// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Event
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2172

#include<stdio.h>
#define ll unsigned long long int

main()
{
    ll a, b;
    while(scanf("%lld %lld", &a,&b)==2 && (a||b))
        printf("%lld\n",a*b);
    return 0;
}
