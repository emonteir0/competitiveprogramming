// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pizza Before BH
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2554

#include<stdio.h>

int mat[101][101];
int mat2[101][101];

main()
{
	int i, N, b, M, x, diamin, mesmin, anomin, dia, mes, ano;
	while(scanf("%d %d", &N, &M) == 2)
	{
		diamin = 9999;
		mesmin = 9999;
		anomin = 9999;
		while(M--)
		{
			scanf("%d/%d/%d", &dia, &mes, &ano);
			b = 1;
			for(i = 0; i < N; i++)
			{
				scanf("%d", &x);
				b &= x;
			}
			if(b)
			{
				if(ano < anomin)
				{
					anomin = ano;
					mesmin = mes;
					diamin = dia;
				}
				else if(ano == anomin)
				{
					if(mes < mesmin)
					{
						anomin = ano;
						mesmin = mes;
						diamin = dia;
					}
					else if(mes == mesmin)
					{
						if(dia < diamin)
						{
							anomin = ano;
							mesmin = mes;
							diamin = dia;
						}
					}
				}
			}
		}
		if(anomin == 9999)
			printf("Pizza antes de FdI\n");
		else
			printf("%d/%d/%d\n", diamin, mesmin, anomin);
	}
	return 0;
}

