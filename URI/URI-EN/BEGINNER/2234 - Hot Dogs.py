# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Hot Dogs
# Level: 1
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2234

z = input().split(' ')
print("%.2f" % (float(z[0])/float(z[1])))
