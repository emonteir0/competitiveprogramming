// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pedrinho's Christmas
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2139

#include<stdio.h>

int vet[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
int vet2[12];


main()
{
	int i, a, b, c;
	vet2[0] = 0;
	for(i = 1; i < 12; i++)
		vet2[i] = vet2[i-1] + vet[i-1];
	while(scanf("%d %d",&a,&b)==2)
	{
		c = vet2[11] - vet2[a-1] + 25 - b;
		if(c > 1)
			printf("Faltam %d dias para o natal!\n", c);
		else if(c == 1)
			printf("E vespera de natal!\n");
		else if(c == 0)
			printf("E natal!\n");
		else
			printf("Ja passou!\n");
	}
	return 0;
}

