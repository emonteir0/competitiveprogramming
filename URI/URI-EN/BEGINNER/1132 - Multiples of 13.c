// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multiples of 13
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1132

#include<stdio.h>

main()
{
	int a,b,c,d;
	scanf("%d %d",&a,&b);
	if(a>b)
	{
		b+=a;
		a=b-a;
		b-=a;
	}
	c=a+(13-a%13)%13;
	d=b-b%13;
	printf("%d\n",(a+b)*(b-a+1)/2-(c+d)*((d-c)/13+1)/2);
	return 0;
}
