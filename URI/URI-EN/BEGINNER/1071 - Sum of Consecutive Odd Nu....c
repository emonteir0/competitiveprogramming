// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sum of Consecutive Odd Nu...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1071

#include<stdio.h>

main()
{
	int i,x,y,acum=0;
	scanf("%d %d",&x,&y);
	if(y>x)
	{
		y+=x;
		x=y-x;
		y-=x;
	}
	for(i=y+1;i<x;i++) acum+=i*(i%2!=0);
	printf("%d\n",acum);
	return 0;
}
