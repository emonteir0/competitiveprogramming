// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Avoiding Rain
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2813

#include<bits/stdc++.h>

using namespace std;

main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string str, str2;
	int N, ca = 0, cb = 0, wa = 0, wb = 0;
	cin >> N;
	while(N--)
	{
		cin >> str >> str2;
		if(str == "chuva")
		{
			if(wa > 0)
				wa--;
			else
				ca++;
			wb++;
		}
		if(str2 == "chuva")
		{
			if(wb > 0)
				wb--;
			else
				cb++;
			wa++;
		}
	}
	cout << ca << " " << cb << endl;
	return 0;
}
