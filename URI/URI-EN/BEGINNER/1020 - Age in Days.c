// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Age in Days
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1020

#include<stdio.h>
#include<math.h>

main()
{
	int a;
	scanf("%d",&a);
	printf("%d ano(s)\n%d mes(es)\n%d dia(s)\n",a/365,(a%365)/30,(a%365)%30);
	return 0;
}
