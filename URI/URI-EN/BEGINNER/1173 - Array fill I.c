// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array fill I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1173

#include<stdio.h>

main()
{
	int i,a;
	scanf("%d",&a);
	for(i=0;i<10;i++)
	{
		printf("N[%d] = %d\n",i,a);
		a*=2;
	}
	return 0;
}
