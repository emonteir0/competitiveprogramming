// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Roman Numerals for Page N...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1960

#include <stdio.h>
#include <string.h>

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

int main()
{
 	int x,k=0,i;
 	char vet[11];
	scanf("%d",&x);
	if(x%10!=0)
	{
		if(x%10==5)
			vet[k++]='V';
		else
		{
			if(x%5!=4)
			{
				for(i=0;i<(x%5);i++)
					vet[k++]='I';
				if(x%10>5)
					vet[k++]='V';
			}
			else
			{
				if(x%10==9)
				{
					vet[k++]='X';
					vet[k++]='I';
				}
				else
				{
					vet[k++]='V';
					vet[k++]='I';
				}
			}
		}
		x-=(x%10);
	}
	
	if(x%100!=0)
	{
		if(x%100==50)
			vet[k++]='L';
		else
		{
			if(x%50!=40)
			{
				for(i=0;i<(x%50);i+=10)
					vet[k++]='X';
				if(x%100>50)
					vet[k++]='L';
			}
			else
			{
				if(x%100==90)
				{
					vet[k++]='C';
					vet[k++]='X';
				}
				else
				{
					vet[k++]='L';
					vet[k++]='X';
				}
			}
		}
		x-=(x%100);
	}
	
	if(x%1000!=0)
	{
		if(x%1000==500)
			vet[k++]='D';
		else
		{
			if(x%500!=400)
			{
				for(i=0;i<(x%500);i+=100)
					vet[k++]='C';
				if(x%1000>500)
					vet[k++]='D';
			}
			else
			{
				if(x%1000==900)
				{
					vet[k++]='M';
					vet[k++]='C';
				}
				else
				{
					vet[k++]='D';
					vet[k++]='C';
				}
			}
		}
		x-=(x%1000);
	}
	vet[k]=0;
	printf("%s\n",strrev(vet));
    return 0;
}
