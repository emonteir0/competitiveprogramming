// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Odd Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1067

#include<stdio.h>

main()
{
	int i,x;
	scanf("%d",&x);
	for(i=1;i<=x;i+=2)	printf("%d\n",i);
	return 0;
}
