// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: UFPR Gaming
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2543

#include<stdio.h>

main()
{
	int i, N, x, y, z, cont;
	while(scanf("%d %d", &N, &x) == 2)
	{
		cont = 0;
		while(N--)
		{
			scanf("%d %d", &y, &z);
			cont += (y == x) && (!z);
		}
		printf("%d\n", cont);
	}
	return 0;
}
