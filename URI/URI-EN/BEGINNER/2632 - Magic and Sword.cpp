// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Magic and Sword
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2632

#include<bits/stdc++.h>
#define ll long long
#define vi vector <double>
using namespace std;

map<string, vector<int> > Mapa;
map<string, int> poder;

vi solve(ll b, ll c)
{
	vi sol;
	double delta = b*b - 4*c;
	if(delta < 0)
		return sol;
	sol.push_back((-b+sqrt(delta))/2.0);
	sol.push_back((-b-sqrt(delta))/2.0);
	return sol;
}

int sqr(int x)
{
	return x*x;
}

int inRaio(int x0, int y0, int xc, int yc, int r)
{
	return sqr(x0-xc) + sqr(y0-yc) <= r*r;
}

int main()
{
	int i, N, a, b, w, h, ind, xc, yc, r, coord, bol;
	vi v;
	scanf("%d%*c", &N);
	Mapa["fire"] = {0,20,30,50};
	Mapa["water"] = {0, 10, 25, 40};
	Mapa["earth"] = {0, 25, 55, 70};
	Mapa["air"] = {0, 18, 38, 60};
	poder["fire"] = 200;
	poder["water"] = 300;
	poder["earth"] = 400;
	poder["air"] = 100;
	string str;
	char msg[10];
	while(N--)
	{
		bol = 0;
		scanf("%d %d %d %d%*c", &w, &h, &a, &b);
		scanf("%s %d %d %d%*c", msg, &ind, &xc, &yc);
		str = msg;
		r = Mapa[str][ind];
		coord = b;
		v = solve(-2*xc, xc*xc+yc*yc-2*coord*yc+coord*coord-r*r);
		for(i = 0; i < (int)v.size(); i++)
		{
			//printf("x = %.2lf\n", v[i]);
			if(v[i] >= a && v[i] <= a+w)
				bol = 1;
		}
		coord = b+h;
		v = solve(-2*xc, xc*xc+yc*yc-2*coord*yc+coord*coord-r*r);
		for(i = 0; i < (int)v.size(); i++)
		{
			//printf("x = %.2lf\n", v[i]);
			if(v[i] >= a && v[i] <= a+w)
				bol = 1;
		}
		coord = a;
		v = solve(-2*yc, xc*xc+yc*yc-2*coord*xc+coord*coord-r*r);
		for(i = 0; i < (int)v.size(); i++)
		{
			//printf("y = %.2lf\n", v[i]);
			if(v[i] >= b && v[i] <= b+h)
				bol = 1;
		}
		coord = a+w;
		v = solve(-2*yc, xc*xc+yc*yc-2*coord*xc+coord*coord-r*r);
		for(i = 0; i < (int)v.size(); i++)
		{
			//printf("y = %.2lf\n", v[i]);
			if(v[i] >= b && v[i] <= b+h)
				bol = 1;
		}
		bol |= inRaio(a, b, xc, yc, r) | inRaio(a+w, b, xc, yc, r) | inRaio(a, b+h, xc, yc, r) | inRaio(a+w, b+h, xc, yc, r);
		bol |= (xc >= a) && (xc <= a + w) && (yc >= b) && (yc <= b+h);
		printf("%d\n", bol * poder[str]);
	}
	return 0;
}
