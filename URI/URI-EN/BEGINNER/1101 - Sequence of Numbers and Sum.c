// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequence of Numbers and Sum
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1101

#include<stdio.h>

main()
{
	int a,b,i;
	while(scanf("%d %d",&a,&b)==2&&a&&b)
	{
		if(a>b)
		{
			b+=a;
			a=b-a;
			b-=a;
		}
		for(i=a;i<=b;i++)
			printf("%d ",i);
		printf("Sum=%d\n",(a+b)*(b-a+1)/2);
	}
	return 0;
}
