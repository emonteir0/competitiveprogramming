// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Preface
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1837

#include<stdio.h>
#include<math.h>

main()
{
	int a, b, c;
	scanf("%d %d",&a,&b);
	c=(a%abs(b)+abs(b))%abs(b);
	printf("%d %d\n",(a-c)/b,c);
	return 0;
}
