// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Chess
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2787

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x, y;
	scanf("%d %d", &x, &y);
	printf("%d\n", (x&1) == (y&1));
	return 0;
}
