// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Imperial Roads
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2703

#include<bits/stdc++.h>
#define MAXN 100005
#define MAXL 18
#define ll long long

using namespace std;

int N, M;
int total;


struct Edge
{
	int u, v, w;
};


struct Hash {
    inline std::size_t operator()(const std::pair<int,int> & v) const {
        return ((ll)v.first) << 32 + v.second;
    }
};

vector<int> G[MAXN], W[MAXN];

vector<int> SET[MAXN];

vector< pair<int, int> > EDGEMAP[MAXN];

int p[MAXN];
Edge e[MAXN+MAXN];

int ancestral[MAXN][MAXL], valores[MAXN][MAXL], nivel[MAXN];

bool cmp(Edge a, Edge b)
{
	return a.w < b.w;
}

inline int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
inline void kruskal() 
{
	int i, j, u, v, w;
    for (i = 0, j = 1; i < M && j < N; i++) 
	{
        u = id(e[i].u);
		v = id(e[i].v);
		w = e[i].w;
        if (u != v)
		{
            p[v] = u;
            total += w;
			j++;
			u = e[i].u;
			v = e[i].v;
			G[u].push_back(v);
			W[u].push_back(w);
			G[v].push_back(u);
			W[v].push_back(w);
			SET[u].push_back(v);
			SET[v].push_back(u);
        }
    }
}

inline void dfs(int u)
{
	int i, v, w;
	for(i = 0; i < G[u].size(); i++)
	{
		v = G[u][i];
		w = W[u][i];
		if(nivel[v] == -1)
		{
			nivel[v] = nivel[u] + 1;
			ancestral[v][0] = u;
			valores[v][0] = w;
			dfs(v);
		}
	}
}

inline void build()
{
	int i, j;
    for(j = 1; j < MAXL; j++)
		for(i = 1; i <= N; i++)
			if(ancestral[i][j-1] != 0)
			{
				ancestral[i][j] = ancestral[ancestral[i][j-1]][j-1];
				valores[i][j] = max(valores[i][j-1], valores[ancestral[i][j-1]][j-1]);
			}
}

inline int LCA(int u, int v)
{   
	int i, ans = 0;
    if(nivel[u] < nivel[v]) swap(u, v);
    for(i = MAXL-1; i >= 0; i--)
        if(nivel[u] - (1<<i) >= nivel[v])
        {
        	ans = max(ans, valores[u][i]);
            u = ancestral[u][i];
        }
    if(u == v) 
		return ans;
    for(i = MAXL-1; i >= 0; i--)
        if((ancestral[u][i] != 0) && (ancestral[u][i] != ancestral[v][i]))
		{
			ans = max(ans, valores[u][i]);
            u = ancestral[u][i];
            ans = max(ans, valores[v][i]);
            v = ancestral[v][i];
        }    
    return max(ans, max(valores[u][0], valores[v][0]));
}

int findedge(int u, int v)
{
	int lo, hi, mid;
	if(EDGEMAP[u].size() > EDGEMAP[v].size())
		swap(u, v);
	lo = 0;
	hi = EDGEMAP[u].size()-1;
	while(lo <= hi)
	{
		mid = (lo + hi) >> 1;
		if(v == EDGEMAP[u][mid].first)
			return EDGEMAP[u][mid].second;
		else if(v <  EDGEMAP[u][mid].first)
			hi = mid-1;
		else
			lo = mid+1;
	}
	return 0;
}

main()
{
	int i, Q, u, v;
	
	scanf("%d %d", &N, &M);
	for(i = 1; i <= N; i++)
	{
		p[i] = i;
		nivel[i] = -1;	
	}
	for(i = 0; i < M; i++)
	{
		scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
		EDGEMAP[e[i].u].push_back(make_pair(e[i].v, e[i].w));
		EDGEMAP[e[i].v].push_back(make_pair(e[i].u, e[i].w));
	}
	sort(e, e+M, cmp);
	kruskal();
	for(i = 1; i <= N; i++)
	{
		sort(EDGEMAP[i].begin(), EDGEMAP[i].end());
		sort(SET[i].begin(),SET[i].end());
	}
	nivel[1] = 1;
	dfs(1);
	build();
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d %d", &u, &v);
		if(binary_search(SET[u].begin(), SET[u].end(), v))
			printf("%d\n", total);
		else
		{
			int edge = findedge(u, v);
			printf("%d\n", total+edge-LCA(u, v));
		}
	}
	return 0;
}
