// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Knights Again
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2808

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x, y;
	char str[5], str2[5];
	scanf("%s %s", str, str2);
	x = abs(str[0]-str2[0]);
	y = abs(str[1]-str2[1]);
	if(x > y)
		swap(x, y);
	printf("%s\n", x == 1 && y == 2 ? "VALIDO" : "INVALIDO");
	return 0;
}
