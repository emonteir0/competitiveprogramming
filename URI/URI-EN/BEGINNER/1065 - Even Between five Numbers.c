// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even Between five Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1065

#include<stdio.h>

main()
{
	int i,x,cont=0;
	for(i=0;i<5;i++)
	{
		scanf("%d",&x);
		if(x%2==0){	cont++;}
	}
	printf("%d valores pares\n",cont);
	return 0;
}
