// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Preparing Production
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2775

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int ord[1001];
int val[1001];

main()
{
	int N;
	ll sum;
	while(scanf("%d", &N) == 1)
	{
		sum = 0;
		for(int i = 1; i <= N; i++)
			scanf("%d", &ord[i]);
		for(int i = 1; i <= N; i++)
			scanf("%d", &val[i]);
		for(int i = 1; i <= N; i++)
		{
			if(ord[i] != i)
			{
				int j;
				for(j = i+1; j <= N; j++)
					if(ord[j] == i)
						break;
				for(int k = j; k > i; k--)
				{
					swap(ord[k], ord[k-1]);
					swap(val[k], val[k-1]);
					sum += val[k] + val[k-1];
				}
			}
		}
		printf("%lld\n", sum);
	}
	return 0;
}
