// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Exceeding Z
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1150

#include<stdio.h>
#include<math.h>

main()
{
	float a,b;
	scanf("%f",&a);
	while(scanf("%f",&b)==1&&b<=a);
	printf("%d\n",(int)ceil((1-2*a + sqrt((2*a-1)*(2*a-1)+8*b))/2));
	return 0;
}
