// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Scientific Notation
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1958

#include <bits/stdc++.h>
#define TAM 1000000
  
using namespace std;
  
int main() {
    char n[TAM];
    double num;
    cin >> n;
    num=atof(n);
    if(n[0]=='-')   printf("%.4E\n",num);
    else    printf("+%.4E\n",num);
    return 0;
}
