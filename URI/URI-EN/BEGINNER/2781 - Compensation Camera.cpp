// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Compensation Camera
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2781

#include<bits/stdc++.h>

using namespace std;

int vet[1000001];

main()
{
	int N, M, u, v, w, s = 0, s2 = 0;
	scanf("%d %d", &M, &N);
	while(M--)
	{
		scanf("%d %d %d", &u, &w, &v);
		s += w;
		vet[u] -= w;
		vet[v] += w;
	}
	for(int i = 1; i <= N; i++)
		if(vet[i] > 0)
			s2 += vet[i];
	printf("%s\n%d\n", (s == s2 ? "N" : "S"), s2);
	return 0;
}
