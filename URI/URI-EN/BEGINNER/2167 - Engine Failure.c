// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Engine Failure
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2167

#include<stdio.h>

main()
{
	int i, n, x, xant, pos = 0;
	scanf("%d",&n);
	scanf("%d",&xant);
	for(i=2;i<=n;i++)
	{
		scanf("%d",&x);
		if(pos==0 && x<xant)
			pos = i;
		xant = x;
	}
	printf("%d\n",pos);
	return 0;
}

