# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Barbecue at Yuri’s
# Level: 5
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2633

while 1:
    try:
        a = []
        for i in range(int(input())):
            s, k = input().split()
            a.append((int(k), s))
        print(*[s[1] for s in sorted(a)])
    except EOFError:
        break

