// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Deciphering the Encrypted...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2502

#include<bits/stdc++.h>

using namespace std;

map<char, char> Mapa, Mapa2;

// 'a'-'A' = 32

void corr(char a, char b)
{
	if(a >= 'A' && a <= 'Z')
	{
		Mapa[a] = b;
		if(b >= 'A' && b <= 'Z')
			Mapa[a + 32] = b + 32;
		else
			Mapa[a + 32] = b;
	}
	else
	{
		if(b >= 'A' && b <= 'Z')
			Mapa[a] = b + 32;
		else
			Mapa[a] = b;
	}
}

main()
{
	int i, N, M, tam;
	char msg[22], msg2[22], cifra[1001];
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		Mapa = Mapa2;
		gets(msg);
		gets(msg2);
		for(i = 0; i < N; i++)
		{
			corr(msg[i], msg2[i]);
			corr(msg2[i], msg[i]);
		}
		while(M--)
		{
			gets(cifra);
			tam = strlen(cifra);
			for(i = 0; i < tam; i++)
				printf("%c", (Mapa.find(cifra[i]) != Mapa.end()) ? Mapa[cifra[i]] : cifra[i]);
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}
