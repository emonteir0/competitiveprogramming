// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Return of Radar
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2143

#include<stdio.h>

main()
{
	int N, x;
	while(scanf("%d",&N)==1 && (N>0))
	{
		while(N--)
		{
			scanf("%d",&x);
			printf("%d\n",2*x-2+x%2);
		}
	}
	return 0;
}

