// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Fill III
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1178

#include<stdio.h>

main()
{
	int i;
	double n;
	scanf("%lf",&n);
	for(i=0;i<100;i++)
	{
		printf("N[%d] = %.4lf\n",i,n);
		n/=2;
	}
	return 0;
}
