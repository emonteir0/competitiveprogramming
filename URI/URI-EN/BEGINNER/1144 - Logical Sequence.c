// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Logical Sequence
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1144

#include<stdio.h>

main()
{
	int a,i;
	scanf("%d",&a);
	for(i=1;i<=a;i++)	printf("%d %d %d\n%d %d %d\n",i,i*i,i*i*i,i,i*i+1,i*i*i+1);
	return 0;
}
