// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Splitting Assignments I
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2715

#include<stdio.h>
#define ll long long

ll vet[1000001];

ll min(ll a, ll b)
{
	return (a < b) ? a : b;
}

ll absol(ll x)
{
	if(x < 0)
		return -x;
	return x;
}

main()
{
	int i, N, x;
	ll best;
	while(scanf("%d", &N) == 1)
	{
		for(i = 1; i <= N; i++)
		{
			scanf("%d", &x);
			vet[i] = vet[i-1] + x;
		}
		best = 1e16;
		for(i = 1; i <= N; i++)
			best = min(best, absol(vet[N]-vet[i]-vet[i]));
		printf("%lld\n", best);
	}
	return 0;
}
