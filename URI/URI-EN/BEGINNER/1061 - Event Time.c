// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Event Time
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1061

#include<stdio.h>

main()
{
	int d1,d2,h1,h2,m1,m2,s1,s2;
	char v[4];
	scanf("%s%*c%d%*c%d%*c%*c%*c%d%*c%*c%*c%d%*c%s%*c%d%*c%d%*c%*c%*c%d%*c%*c%*c%d%*c",v,&d1,&h1,&m1,&s1,v,&d2,&h2,&m2,&s2);
	d2-=d1;
	h2-=h1;
	m2-=m1;
	s2-=s1;
	if(s2<0)
	{
		m2--;
		s2+=60;
	}
	if(m2<0)
	{
		h2--;
		m2+=60;
	}
	if(h2<0)
	{
		d2--;
		h2+=24;
	}
	printf("%d dia(s)\n%d hora(s)\n%d minuto(s)\n%d segundo(s)\n",d2,h2,m2,s2);
	return 0;
}
