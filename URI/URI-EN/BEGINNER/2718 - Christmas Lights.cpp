// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Lights
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2718

#include<stdio.h>
#define ll long long

ll max(ll a, ll b)
{
	return (a > b) ? a : b;
}

main()
{
	int N, cont, best;
	ll x;
	scanf("%d", &N);
	while(N--)
	{
		cont = best = 0; 
		scanf("%lld", &x);
		while(x > 0)
		{
			if(x & 1)
				cont++;
			else
				cont = 0;
			best = max(best, cont);
			x /= 2;
		}
		printf("%d\n", best);
	}
	return 0;
}
