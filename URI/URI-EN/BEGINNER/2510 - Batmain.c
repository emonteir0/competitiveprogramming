// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Batmain
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2510

#include<stdio.h>

main()
{
    int N;
    scanf("%d", &N);
    while(N--)
    {
        scanf("%*s");
        printf("Y\n");
    }
    return 0;
}
