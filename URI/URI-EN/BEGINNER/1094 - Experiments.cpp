// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Experiments
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1094

#include<stdio.h>

main()
{
	int n,i,x,cont1=0,cont2=0,cont3=0;
	char sig;
	scanf("%d%*c",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d %c%*c",&x,&sig);
		if(sig=='C')	cont1+=x;
		if(sig=='R')	cont2+=x;
		if(sig=='S')	cont3+=x;
	}
	printf("Total: %d cobaias\nTotal de coelhos: %d\nTotal de ratos: %d\nTotal de sapos: %d\nPercentual de coelhos: %.2f %%\nPercentual de ratos: %.2f %%\nPercentual de sapos: %.2f %%\n",cont1+cont2+cont3,cont1,cont2,cont3,100.00*cont1/(cont1+cont2+cont3),100.00*cont2/(cont1+cont2+cont3),100.00*cont3/(cont1+cont2+cont3));
	return 0;
}
