// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dijkstra
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2653

#include<bits/stdc++.h>

using namespace std;

set<string> conjunto;

main()
{
	string str;
	while(cin >> str)
		conjunto.insert(str);
	printf("%d\n", conjunto.size());
	return 0;
}
