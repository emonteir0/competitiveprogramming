// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Output 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2748

#include<bits/stdc++.h>

using namespace std;

main()
{	
	printf("---------------------------------------\n");
	printf("|        Roberto                      |\n");
	printf("|                                     |\n");
	printf("|        5786                         |\n");
	printf("|                                     |\n");
	printf("|        UNIFEI                       |\n");
	printf("---------------------------------------\n");
	return 0;
}
