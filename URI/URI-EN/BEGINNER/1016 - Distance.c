// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Distance
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1016

#include<stdio.h>

main()
{
	int a;
	scanf("%d",&a);
	printf("%d minutos\n",a<<1);
	return 0;
}
