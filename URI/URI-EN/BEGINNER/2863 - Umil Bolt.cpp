// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Umil Bolt
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2863

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N;
	double me, x;
	while(scanf("%d", &N) == 1)
	{
		me = 1e50;
		while(N--)
		{
			scanf("%lf", &x);
			me = min(me, x);
		}
		printf("%.2lf\n", me);
	}
	return 0;
}
