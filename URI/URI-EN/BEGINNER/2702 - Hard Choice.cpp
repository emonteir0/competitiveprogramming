// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hard Choice
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2702

#include<bits/stdc++.h>

using namespace std;

main()
{
    int a, b, c, d, e, f;
    scanf("%d %d %d %d %d %d", &a, &b, &c, &d, &e, &f);
    printf("%d\n", max(0, d-a) + max(0, e-b) + max(0, f-c));
    return 0;
}
