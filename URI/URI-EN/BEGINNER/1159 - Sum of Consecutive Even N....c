// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sum of Consecutive Even N...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1159

#include<stdio.h>

main()
{
	long long int a;
	while(scanf("%lld",&a)==1&&a)
	{
		a+=a%2!=0;
		printf("%lld\n",(a+4)*5);
	}
	return 0;
}
