// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Taxes
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1051

#include<stdio.h>

main()
{
	double a,imp=0;
	scanf("%lf",&a);
	if(a>2000)	imp+=(((a>3000)?3000:a)-2000)*0.08;
	if(a>3000)	imp+=(((a>4500)?4500:a)-3000)*0.18;
	if(a>4500)	imp+=(a-4500)*0.28;
	if(imp==0)	printf("Isento\n");
	else	printf("R$ %.2f\n",imp);
	return 0;
}
