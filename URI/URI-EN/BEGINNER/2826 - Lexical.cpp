// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lexical
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2826

#include<bits/stdc++.h>

using namespace std;

char str[5000001], str2[5000001];

main()
{
	scanf("%s %s", str, str2);
	if(strcmp(str, str2) <= 0)
		printf("%s\n%s\n", str, str2);
	else
		printf("%s\n%s\n", str2, str);
	return 0;
}
