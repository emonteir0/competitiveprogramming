// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibonacci Array
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1176

#include<stdio.h>

main()
{
	int i,n;
	long long int vet[61];
	vet[0]=0;
	vet[1]=1;
	for(i=2;i<61;i++)
		vet[i]=vet[i-1]+vet[i-2];
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&i);
		printf("Fib(%d) = %lld\n",i,vet[i]);
	}
	return 0;
}
