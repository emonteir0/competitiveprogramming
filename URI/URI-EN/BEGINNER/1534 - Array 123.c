// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array 123
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1534

#include<stdio.h>


main()
{
	int n,i,j,l;
	while(scanf("%d",&n)==1&&n>0)
	{
		for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
		{
			l=n-i-1;
			if(j!=i&&j!=l)	printf("3");
			else if(j==l)	printf("2");
			else			printf("1");
		}
		printf("\n");
		}
	}
	return 0;
}
