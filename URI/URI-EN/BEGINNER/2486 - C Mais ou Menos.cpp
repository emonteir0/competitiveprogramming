// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: C Mais ou Menos?
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2486

#include<bits/stdc++.h>

using namespace std;

map<string, int> Mapa;


main()
{
	int N, acum, x;
	string str;
	char msg[21];
	Mapa["suco de laranja"] = 120;
	Mapa["morango fresco"] = 85;
	Mapa["mamao"] = 85;
	Mapa["goiaba vermelha"] = 70;
	Mapa["manga"] = 56;
	Mapa["laranja"] = 50;
	Mapa["brocolis"] = 34;
	while(scanf("%d%*c", &N) == 1 && N)
	{
		acum = 0;
		while(N--)
		{
			scanf("%d %[A-Z a-z]", &x, msg);
			str = msg;
			acum += x*Mapa[str];
		}
		if(acum > 130)
			printf("Menos %d mg\n", acum-130);
		else if(acum < 110)
			printf("Mais %d mg\n", 110-acum);
		else
			printf("%d mg\n", acum);
	}
	return 0;
}

