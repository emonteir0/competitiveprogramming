// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Odd, Even or Cheating
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2059

#include<stdio.h>

main()
{
	int a, b, c, d, e;
	scanf("%d %d %d %d %d",&a,&b,&c,&d,&e);
	if(d==0 && e==1)
		printf("Jogador 1 ganha!\n");
	else if(d==1 && e ==0)
		printf("Jogador 1 ganha!\n");
	else if(d==1 && e ==1)
		printf("Jogador 2 ganha!\n");
	else
		printf("%s\n",( ((b+c)&1) != a)?"Jogador 1 ganha!":"Jogador 2 ganha!");
	return 0;
}

