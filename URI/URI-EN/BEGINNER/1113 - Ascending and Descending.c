// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ascending and Descending
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1113

#include<stdio.h>

main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2&&(a!=b))
	{
		if(a>b)	printf("Decrescente\n");
		else	printf("Crescente\n");
	}
	return 0;
}
