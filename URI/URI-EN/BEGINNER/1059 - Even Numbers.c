// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1059

#include<stdio.h>

main()
{
	int i;
	for(i=2;i<=100;i+=2)
		printf("%d\n",i);
	return 0;
}
