// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Railways
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2804

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

int mat[450][450], mat2[450][450], mat3[450][450];

main()
{
	int N, K, cont = 0;
	scanf("%d %d", &N, &K);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			scanf("%d", &mat2[i][j]);
	for(int i = 0; i < N; i++)
		if(mat2[i][i] != 0)
			return !printf("*\n");
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			mat[i][j] = mat2[i][j];
			
	for(int k = 0; k < N; k++)
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				if(i != k && j != k && mat[i][k]+mat[k][j] == mat[i][j])
					mat[i][j] = mat[j][i] = INF;
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			mat3[i][j] = mat[i][j];
	for(int k = 0; k < N; k++)
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				mat3[i][j] = min(mat3[i][k]+mat3[k][j], mat3[i][j]);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			if((mat3[i][j] != mat3[j][i]) || (mat2[i][j] != mat3[i][j]) || (mat3[i][j] != mat3[j][i]))
				return !printf("*\n");
	for(int i = 0; i < N; i++)
		for(int j = 0; j < N; j++)
			cont += (mat[i][j] > 0) && (mat[i][j] != INF);
	printf("%d\n", (cont*K)/2);
	return 0;
}
