// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Enigma
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2699

#include<bits/stdc++.h>

using namespace std;

char str[1001];
int tam, K;

int pd[1001][1001];

int solve(int x, int y)
{
	int i;
	if(pd[x][y] != -1)
		return pd[x][y];
	pd[x][y] = 0;
	if(str[x] != '?')
		return pd[x][y] = solve(x+1, (10*y+str[x]-'0')%K);
	for(i = x == 0; i < 10; i++)
		pd[x][y] |= solve(x+1, (10*y+i)%K);	
	return pd[x][y];
}

main()
{
	int i, j, k;
	scanf("%s %d", str, &K);
	tam = strlen(str);
	for(i = 0; i < tam; i++)
		for(j = 0; j <= K; j++)
			pd[i][j] = -1;
	pd[tam][0] = 1;
	if(solve(0, 0) == 1)
	{
		k = 0;
		for(i = 0; i < tam; i++)
		{
			if(str[i] == '?')
				for(j = i == 0; j < 10; j++)
					if(pd[i+1][(10*k + j)%K])
					{
						str[i] = j+'0';
						break;
					}
			k = (10*k + str[i] - '0')%K;
		}
		printf("%s\n", str);
	}
	else
		printf("*\n");
	return 0;
}
