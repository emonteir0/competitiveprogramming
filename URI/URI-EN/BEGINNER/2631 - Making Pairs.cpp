// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Making Pairs
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2631

#include <bits/stdc++.h>

int n, m, q, p[10040], l[10040], u, v;

int id(int x) {
    return p[x] == x ? x : p[x]=id(p[x]);
}

void join(int u, int v) {
    u = id(u);
    v = id(v);
    if (u == v) return;

    if (l[u] > l[v]) {
        p[v] = u;
    } else {
        p[u] = v;
        if (l[u] == l[v]) l[v]++;
    }
}

int main() {
    int flag = 0;
    while (scanf("%d %d %d", &n, &m, &q) != EOF) {
        if (flag) { printf("\n"); }
        flag = 1;
        for (int i = 0; i <= n; i++) {
            p[i] = i; l[i] = 0;
        }
        while (m--) {
            scanf("%d %d", &u, &v);
            join(u, v);
        }

        while (q--) {
            scanf("%d %d", &u, &v);
            printf("%c\n", id(u) == id(v) ? 'S':'N');
        }
    }

    return 0;
}

