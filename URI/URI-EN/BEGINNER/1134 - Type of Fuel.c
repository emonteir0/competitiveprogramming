// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Type of Fuel
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1134

#include<stdio.h>

main()
{
	int a,b=0,c=0,d=0;
	while(scanf("%d",&a)==1&&a!=4)
	{
		b+=a==1;
		c+=a==2;
		d+=a==3;
	}
	printf("MUITO OBRIGADO\nAlcool: %d\nGasolina: %d\nDiesel: %d\n",b,c,d);
	return 0;
}
