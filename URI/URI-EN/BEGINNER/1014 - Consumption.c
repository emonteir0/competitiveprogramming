// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Consumption
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1014

#include<stdio.h>

main()
{
	double a,b;
	scanf("%lf %lf",&a,&b);
	printf("%.3lf km/l\n",a/b);
	return 0;
}
