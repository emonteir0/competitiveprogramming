// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Counting Crow
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1848

#include<stdio.h>
#include<string.h>
main()
{
	int cont=0,x=0;
	char vet[12];
	while((x!=3)&&(gets(vet)!=NULL))
	{
		if(strcmp(vet,"*--")==0)
			cont+=4;
		else if(strcmp(vet,"-*-")==0)
			cont+=2;
		else if(strcmp(vet,"--*")==0)
			cont+=1;
		else if(strcmp(vet,"***")==0)
			cont+=7;
		else if(strcmp(vet,"**-")==0)
			cont+=6;
		else if(strcmp(vet,"*-*")==0)
			cont+=5;
		else if(strcmp(vet,"-**")==0)
			cont+=3;	
		else if(strstr(vet,"caw caw")!=NULL)
		{
			printf("%d\n",cont);
			cont=0;
			x++;
		}
	}
	return 0;
}
