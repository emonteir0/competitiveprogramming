// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fase
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2663

#include<bits/stdc++.h>

using namespace std;

int vet[1000];

main()
{
	int N, K, i;
	scanf("%d %d", &N, &K);
	for(i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	sort(vet, vet+N, greater<int>());
	for(i = K; i < N; i++)
		if(vet[i] != vet[K-1])
			break;
	printf("%d\n", i);
	return 0;
}
