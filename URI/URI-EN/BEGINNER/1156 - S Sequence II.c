// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: S Sequence II
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1156

#include<stdio.h>

main()
{
	long long int a=1;
	int i;
	double acum=0;
	for(i=0;i<=39;i++)
	{
		acum+=((double)(2*i+1))/a;
		a=a<<1;
	}
	printf("%.2f\n",acum);
	return 0;
}
