// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: MacPRONALTS
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1985

#include<cstdio>
#include<map>

using namespace std;

map<int,float> Mapa;


main()
{
	int n,a,b;
	double x=0;
	Mapa[1001]=1.5;
	Mapa[1002]=2.5;
	Mapa[1003]=3.5;
	Mapa[1004]=4.5;
	Mapa[1005]=5.5;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&a,&b);
		x+=b*Mapa[a];
	}
	printf("%.2lf\n",x);
	return 0;
}
