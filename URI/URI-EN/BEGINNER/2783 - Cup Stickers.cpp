// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cup Stickers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2783

#include<bits/stdc++.h>

using namespace std;

set<int> conju;

main()
{
	int N, C, M, x;
	scanf("%d %d %d", &N, &C, &M);
	for(int i = 0; i < C; i++)
	{
		scanf("%d", &x);
		conju.insert(x);
	}
	while(M--)
	{
		scanf("%d", &x);
		if(conju.count(x) > 0)
			conju.erase(x);
	}
	printf("%d\n", conju.size());
	return 0;
}
