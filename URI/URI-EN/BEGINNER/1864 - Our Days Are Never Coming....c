// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Our Days Are Never Coming...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1864

#include<stdio.h>

char v[35]="LIFE IS NOT A PROBLEM TO BE SOLVED";

main()
{
	int n,i;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		printf("%c",v[i]);
	}
	printf("\n");
	return 0;
}
