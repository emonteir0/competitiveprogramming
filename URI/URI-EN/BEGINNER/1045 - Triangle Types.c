// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangle Types
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1045

#include<stdio.h>

main()
{
	double a,b,c,d,e;
	scanf("%lf %lf %lf",&a,&b,&c);
	if(a<b)
	{
		b+=a;
		a=b-a;
		b-=a;
	}
	if(a<c)
	{
		c+=a;
		a=c-a;
		c-=a;
	}
	if(b<c)
	{
		b+=c;
		c=b-c;
		b-=c;
	}
	e=b*b+c*c;
	d=a*a;
	if(a>=b+c)	printf("NAO FORMA TRIANGULO\n");
	else
	{
		if(d==e)	printf("TRIANGULO RETANGULO\n");
		else if(d>e)	printf("TRIANGULO OBTUSANGULO\n");
		else	printf("TRIANGULO ACUTANGULO\n");
		if(a==b&&a==c)	printf("TRIANGULO EQUILATERO\n");
		else if(a==b||b==c)	printf("TRIANGULO ISOSCELES\n");
	}
	return 0;
}
