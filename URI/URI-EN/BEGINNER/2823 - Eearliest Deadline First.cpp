// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Eearliest Deadline First
// Level: 3
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2823

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll num, den, den2;

main()
{
	int N;
	ll x, y;
	num = 0, den = 1;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%lld %lld", &x, &y);
		den2 = y*(den/__gcd(den, y));
		num = (den2/den)*num + (den2/y)*x;
		den = den2;
	}
	printf("%s\n", (num <= den) ? "OK": "FAIL");
	return 0;
}
