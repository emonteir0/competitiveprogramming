// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Internship
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2533

#include<stdio.h>

main()
{
	int N, x, y, acum, cont;
	while(scanf("%d", &N) == 1)
	{
		acum = 0;
		cont = 0;
		while(N--)
		{
			scanf("%d %d", &x, &y);
			acum += x*y;
			cont += y;
		}
		printf("%.4lf\n", (double)acum/(100*cont));
	}
	return 0;
}
