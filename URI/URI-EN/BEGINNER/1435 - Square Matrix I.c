// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Matrix I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1435

#include<stdio.h>

int absol(int x)
{
	if(x<0)
		return -x;
	return x;
}

main()
{
	int i,j,n,x,y,t;
	while(scanf("%d",&n)==1&&n>0)
	{
		t=(n+n%2)/2;
		x=0;
		for(i=1;i<=n;i++)
		{
			if(n%2!=0)
			{
				for(j=1;j<=n;j++)
				{
				x=absol(t-i);
				y=absol(t-j);
				x=x>y?x:y;
				if(j!=1)
					printf(" ");
				printf("%3d",t-x);
				}
			}
			else
			{
				if(i<=t)
					x++;
				if(i>t+1)
					x--;
				for(j=1;j<=x;j++)
				{
						if(j!=1)
							printf(" ");
						printf("%3d",j);
				}
				for(;j<=n-x;j++)
					printf(" %3d",x);
				for(;j<=n;j++)
					printf(" %3d",n-j+1);
			}
			printf("\n");
		}
		printf("\n");
	}
	return 0;
}
