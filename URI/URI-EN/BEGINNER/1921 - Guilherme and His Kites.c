// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Guilherme and His Kites
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1921

#include<stdio.h>

main()
{
	int n;
	scanf("%d",&n);
	printf("%lld\n",((long long int)n)*(n-3)/2);
	return 0;
}
