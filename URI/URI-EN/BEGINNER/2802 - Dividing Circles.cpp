// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dividing Circles
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2802

#include<bits/stdc++.h>
#define ll long long
using namespace std;

ll dp[2001][5];

main()
{
	int N;
	for(int i = 0; i <= 2000; i++)
		dp[i][0] = 1;
	for(int i = 1; i <= 2000; i++)
		for(int j = 1; j <= 4; j++)
			dp[i][j] = dp[i-1][j] + dp[i-1][j-1];
	scanf("%d", &N);
	printf("%lld\n", dp[N][2]+dp[N][0]+dp[N][4]);
	return 0;
}
