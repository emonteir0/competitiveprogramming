// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Board Size
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2770

#include<bits/stdc++.h>

main()
{
	int X, Y, M, a, b;
	while(scanf("%d %d %d", &X, &Y, &M) == 3)
	{
		while(M--)
		{
			scanf("%d %d", &a, &b);
			printf("%s\n",(a <= X && b <= Y) || (a <= Y && b <= X) ? "Sim" : "Nao" );
		}
	}
	return 0;
}
