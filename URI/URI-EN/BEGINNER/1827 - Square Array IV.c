// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Array IV
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1827

#include<stdio.h>

main()
{
	int i,j,n,x,k=0;
	while(scanf("%d",&n)==1)
	{
		x=2*((n-5)/6)+3;
		for(i=0;i<n;i++)
		{
			for(j=0;j<n;j++)
			{
				if(i==n/2&&j==n/2)
					printf("4");
				else if((i>=(n-x)/2)&&(i<(n+x)/2)&&(j>=(n-x)/2)&&(j<(n+x)/2))
					printf("1");
				else if(i==j)
					printf("2");
				else if((i+j)==n-1)
					printf("3");
				else
					printf("0");
			}
			printf("\n");
		}
		printf("\n");	
	}
	return 0;
}
