// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequence of Sequence
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2028

#include<stdio.h>

main()
{
	int i,j,k=1,n;
	while(scanf("%d",&n)==1)
	{
		printf("Caso %d: %d numero%s\n0",k++,(n*(n+1))/2+1,(n==0)?"":"s");
		for(i=1;i<=n;i++)
			for(j=1;j<=i;j++)
				printf(" %d",i);
		printf("\n\n");
	}
	return 0;
}

