// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dividing X by Y
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1116

#include<stdio.h>

main()
{
	float a,b;
	int n;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%f %f",&a,&b);
		if(b!=0)	printf("%.1f\n",a/b);
		else	printf("divisao impossivel\n");
	}
	return 0;
}
