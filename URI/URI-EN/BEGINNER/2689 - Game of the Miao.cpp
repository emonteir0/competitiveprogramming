// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game of the Miao
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2689

#include<bits/stdc++.h>

using namespace std;

int mat[3][3];
int used[3][3];
int cont[101];

main()
{
	int N, ma, first;
	vector<int> ans;
	scanf("%d", &N);
	while(N--)
	{
		ans.clear();
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
			{
				scanf("%d", &mat[i][j]);
				used[i][j] = 1;
			}
		for(int i = 0; i <= 100; i++)
			cont[i] = 0;
		cont[abs(mat[0][0]-mat[0][1])] += 2;
		cont[abs(mat[0][1]-mat[0][2])]++;
		cont[abs(mat[1][0]-mat[1][1])] += 2;
		cont[abs(mat[1][1]-mat[1][2])]++;
		cont[abs(mat[2][0]-mat[2][1])] += 2;
		cont[abs(mat[2][1]-mat[2][2])]++;
		ma = 0;
		for(int i = 0; i <= 100; i++)
			ma = max(ma, cont[i]);
		for(int i = 0; i <= 100; i++)
			if(cont[i] == ma)
				ans.push_back(i);
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				for(int k = 0; k < 3; k++)
					if(j != k)
					{
						if(binary_search(ans.begin(), ans.end(), abs(mat[i][j]-mat[i][k])))
							used[i][j] = 0;
					}
		first = 1;
		printf("Possiveis maletas:");
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				if(used[i][j])
				{
					if(!first)
						printf(",");
					first = 0;
					printf(" %d", mat[i][j]);
				}
		printf(";\n");
	}
	return 0;
}
