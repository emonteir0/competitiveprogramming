// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ages
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1154

#include<stdio.h>

main()
{
	int cont=0,a;
	float acum=0;
	while(scanf("%d",&a)==1&&a>=0)
	{
		cont++;
		acum+=a;
	}
	printf("%.2f\n",acum/cont);
	return 0;
}
