// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Will's Message
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2523

#include<stdio.h>

main()
{
	char palavra[27];
	int N, x;
	while(scanf("%s%*c", palavra) == 1)
	{
		scanf("%d%*c", &N);
		while(N--)
		{
			scanf("%d", &x);
			printf("%c", palavra[x-1]);
		}
		printf("\n");
	}
	return 0;
}
