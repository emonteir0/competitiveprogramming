// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Insect!
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2862

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N, x;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		printf("%s\n", x <= 8000 ? "Inseto!": "Mais de 8000!");
	}
	return 0;
}
