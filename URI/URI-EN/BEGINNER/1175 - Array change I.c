// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array change I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1175

#include<stdio.h>

main()
{
	int i,v[20];
	for(i=0;i<20;i++)
		scanf("%d",&v[19-i]);
	for(i=0;i<20;i++)
		printf("N[%d] = %d\n",i,v[i]);
	return 0;
}
