// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: CheeseBreadSweeper
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2552

#include<stdio.h>

int mat[100][100];
int dx[4] = {1, -1, 0, 0}, dy[4] = {0, 0, 1, -1}; 

int min(int a, int b)
{
	return (a < b) ? a : b;
}

main()
{
	int N, M, i, j, k, l, m;
	while(scanf("%d %d", &N, &M) != EOF)
	{
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
			{
				scanf("%d", &mat[i][j]);
				if(mat[i][j] == 1)
					mat[i][j] = 9;
			}
		for(i = 0; i < N; i++)
			for(j = 0; j < M; j++)
				if(mat[i][j] == 9)
				{
					for(m = 0; m < 4; m++)
					{
						k = i + dx[m];
						l = j + dy[m];
						if(k >= 0 && k < N && l >= 0 && l < M)
							mat[k][l] = min(mat[k][l]+1, 9);
					}
				}
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
				printf("%d", mat[i][j]);
			printf("\n");
		}
	}
	return 0;
}
