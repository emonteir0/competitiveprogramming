// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Time Conversion
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1019

#include<stdio.h>
#include<math.h>

main()
{
	int a;
	scanf("%d",&a);
	printf("%d:%d:%d\n",a/3600,(a%3600)/60,a%60);
	return 0;
}
