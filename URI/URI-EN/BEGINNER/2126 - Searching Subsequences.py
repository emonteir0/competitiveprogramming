# Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
# Name: Searching Subsequences
# Level: 1
# Category: BEGINNER
# URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2126

k = 1

while True:
	try:
		x = -1
		cont = 0
		a = raw_input()
		b = raw_input()
		y = b.find(a)
		while y != -1:
			x = y
			cont += 1
			y = b.find(a,y+1)
		if cont != 0:
			print 'Caso #%d:\nQtd.Subsequencias: %d\nPos: %d\n' % (k,cont,x+1)
		else:
			print 'Caso #%d:\nNao existe subsequencia\n' % (k)
	except:
		break
	k += 1

