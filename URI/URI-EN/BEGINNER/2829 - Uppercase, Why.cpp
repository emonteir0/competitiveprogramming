// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Uppercase, Why?
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2829

#include<bits/stdc++.h>

using namespace std;

string vet[50001];

char lower(char c)
{
	if(c >= 'A' && c <= 'Z')
		return c-'A'+'a';
	return c;
}

char maiusc(char c)
{
	return c >= 'A' && c <= 'Z';
}

bool cmp(string a, string b)
{
	int t = min(a.length(), b.length());
	for(int i = 0; i < t; i++)
	{
		//printf("%c %c\n", lower(a[i]), lower(b[i]));
		if(lower(a[i]) != lower(b[i]))
		{
			//printf("\n");
			return lower(a[i]) < lower(b[i]);
		}
	}
	if(a.length() != b.length())
		return a.length() < b.length();
	for(int i = 0; i < a.length(); i++)
		if(maiusc(a[i]) != maiusc(b[i]))
			return maiusc(a[i]);
	return 0;
}

main()
{
	int N;
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	cin >> N;
	for(int i = 0; i < N; i++)
		cin >> vet[i];
	sort(vet, vet+N, cmp);
	//printf("%d\n", cmp(vet[N-1], vet[N-2]));
	for(int i = 0; i < N; i++)
		cout << vet[i] << endl;
	return 0;
}
