// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Last Analógimôn
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2520

#include<stdio.h>

int absol(int x)
{
	if(x < 0)
		return -x;
	return x;
}

main()
{
	int N, M, i, j, xa, ya, xb, yb, val;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
			{
				scanf("%d", &val);
				if(val == 1)
				{
					xa = i;
					ya = j;
				}
				if(val == 2)
				{
					xb = i;
					yb = j;
				}
			}
		}
		printf("%d\n", absol(xa-xb) + absol(ya-yb));
	}
	return 0;
}
