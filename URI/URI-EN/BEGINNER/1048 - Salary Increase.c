// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Salary Increase
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1048

#include<stdio.h>

main()
{
	double a;
	scanf("%lf",&a);
	if(a<=400)	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: 15 %%\n",1.15*a,0.15*a);
	else if(a<=800)	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: 12 %%\n",1.12*a,0.12*a);
	else if(a<=1200)	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: 10 %%\n",1.1*a,0.1*a);
	else if(a<=2000)	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: 7 %%\n",1.07*a,0.07*a);
	else printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: 4 %%\n",1.04*a,0.04*a);
	return 0;
}
