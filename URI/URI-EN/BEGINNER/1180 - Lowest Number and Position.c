// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lowest Number and Position
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1180

#include<stdio.h>

main()
{
	int i,x,p=0,me,n;
	scanf("%d %d",&n,&x);
	me=x;
	for(i=1;i<n;i++)
	{
		scanf("%d",&x);
		if(x<me)
		{
			me=x;
			p=i;
		}
	}
	printf("Menor valor: %d\nPosicao: %d\n",me,p);
	return 0;
}
