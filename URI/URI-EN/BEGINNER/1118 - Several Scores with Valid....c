// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Several Scores with Valid...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1118

#include<stdio.h>

main()
{
	float a,b;
	int n=1;
	while(n==1)
	{
		while(scanf("%f",&a)==1&&a<0||a>10)	printf("nota invalida\n");
		while(scanf("%f",&b)==1&&b<0||b>10)	printf("nota invalida\n");
		printf("media = %.2f\nnovo calculo (1-sim 2-nao)\n",(a+b)/2);
		while(scanf("%d",&n)==1 && n<1||n>2)printf("novo calculo (1-sim 2-nao)\n");
	}
	return 0;
}
