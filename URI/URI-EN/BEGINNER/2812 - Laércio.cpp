// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Laércio
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2812

#include<bits/stdc++.h>

using namespace std;

vector<int> vet;

main()
{
	int T, N, x, p, e;
	scanf("%d", &T);
	while(T--)
	{
		vet.clear();
		scanf("%d", &N);
		for(int i = 0; i < N; i++)
		{
			scanf("%d", &x);
			if(x&1)
				vet.push_back(x);
		}
		sort(vet.begin(), vet.end());
		if(vet.size())
		{
			printf("%d", vet.back());
			p = 0;
			e = vet.size()-2;
			while(p <= e)
			{
				printf(" %d", vet[p]);
				p++;
				if(p <= e)
					printf(" %d", vet[e]);
				e--;
			}
		}
		printf("\n");
	}
	return 0;
}
