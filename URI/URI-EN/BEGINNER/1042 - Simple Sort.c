// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Simple Sort
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1042

#include<stdio.h>

main()
{
	int a,b,c,d,e,f;
	scanf("%d %d %d",&d,&e,&f);
	a=d;
	b=e;
	c=f;
	if(a>b)
	{
		b+=a;
		a=b-a;
		b-=a;
	}
	if(a>c)
	{
		c+=a;
		a=c-a;
		c-=a;
	}
	if(b>c)
	{
		c+=b;
		b=c-b;
		c-=b;
	}
	printf("%d\n%d\n%d\n\n%d\n%d\n%d\n",a,b,c,d,e,f);
	return 0;
}
