// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fundraising
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2700

#include<bits/stdc++.h>
#define MAXN 100001
#define ll long long

using namespace std;

int N;
int b[MAXN], f[MAXN], c[MAXN];
ll d[MAXN], tree[4*MAXN];

set<int> fortune, beauty;
set<int>::iterator it;
vector<int> vfortune, vbeauty;
vector<int> indexes;

bool cmp(int x, int y)
{
	if(b[x] != b[y])
		return b[x] > b[y];
	return f[x] < f[y];
}

void update(int node, int start, int end, int idx, ll val)
{
	if(start > idx || end < idx)
		return;
	if(start == end)
	{
		tree[node] = val;
		return;
	}
	int mid = (start+end)/2;
	update(2*node, start, mid, idx, val);
	update(2*node+1, mid+1, end, idx, val);
	tree[node] = max(tree[2*node], tree[2*node+1]);
}

ll query(int node, int start, int end, int l, int r)
{
	//printf("%d\n", node);
	if(start > end || start > r || end < l)
        return 0;
    if(start >= l && end <= r)
        return tree[node];
    int mid = (start + end) / 2;
	return max(query(node*2, start, mid, l, r), query(node*2 + 1, mid + 1, end, l, r));
}

int main()
{
	int i;
	ll x;
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d %d %lld", &b[i], &f[i], &d[i]);
		beauty.insert(b[i]);
		fortune.insert(f[i]);
		c[i] = i;
	}
	for(it = beauty.begin(); it != beauty.end(); it++)
		vbeauty.push_back(*it);
	for(it = fortune.begin(); it != fortune.end(); it++)
		vfortune.push_back(*it);
	for(i = 0; i < N; i++)
	{
		b[i] = lower_bound(vbeauty.begin(), vbeauty.end(), b[i]) - vbeauty.begin();
		f[i] = lower_bound(vfortune.begin(), vfortune.end(), f[i]) - vfortune.begin();
	}
	sort(c, c+N, cmp);
	indexes.push_back(c[0]);
	for(i = 1; i < N; i++)
	{
		if(b[c[i]] == b[indexes[indexes.size()-1]] && f[c[i]] == f[indexes[indexes.size()-1]])
			d[indexes[indexes.size()-1]] += d[c[i]];
		else
			indexes.push_back(c[i]);
	}
	for(i = 0; i < (int)indexes.size(); i++)
	{
		//printf("%d %d %lld\n", i, f[indexes[i]], d[indexes[i]]);
		if(f[indexes[i]] < N-1)
		{
			//printf("%d %d %d %d %d %lld\n", 1, 0, N-1, f[indexes[i]]+1, N-1, d[indexes[i]]);
			x = query(1, 0, N-1, f[indexes[i]]+1, N-1) + d[indexes[i]];
		}
		else 
			x = d[indexes[i]];
		//printf("%d %d %lld\n", i, f[indexes[i]], d[indexes[i]]);
		if(x > query(1, 0, N-1, f[indexes[i]], f[indexes[i]]))
			update(1, 0, N-1, f[indexes[i]], x);
	}
	printf("%lld\n", query(1, 0, N-1, 0, N-1));
	return 0;
}

