// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mjölnir
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1865

#include<stdio.h>
#include<string.h>

main()
{
	int N;
	char vet[101];
	scanf("%d",&N);
	while(N--)
	{
		scanf("%s %*d",vet);
		printf("%s\n",strcmp(vet,"Thor")==0?"Y":"N");
	}
	return 0;
}
