// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Daunting Device
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2698

#include<bits/stdc++.h>
#define ll long long
using namespace std;

set< pair< pair<int, int>, int> > cores;
set< pair< pair<int, int>, int> >::iterator it, itl, itu;

int freq[100001];

main()
{
	int N, C, M, P, X, A, B, M1, M2, ma = 0;
	pair< pair<int, int>, int> t1, t2;
	scanf("%d %d %d", &N, &C, &M);
	cores.insert(make_pair(make_pair(0, N-1), 1));
	freq[1] = N;
	while(M--)
	{
		scanf("%d %d %d %d", &P, &X, &A, &B);
		M1 = (A+((ll)freq[P])*freq[P])%N;
		M2 = (A+((ll)(freq[P]+B))*(freq[P]+B))%N;
		if(M1 > M2)
			swap(M1, M2);
		/*for(it = cores.begin(); it != cores.end(); it++)
			printf("(%d,%d) = %d\n", (it->first).first, (it->first).second, it->second);
		printf("M1 = %d| M2 = %d\n\n\n", M1, M2);*/
		itl = cores.upper_bound(make_pair(make_pair(M1+1, 0), 0));
		itu = cores.lower_bound(make_pair(make_pair(M2+1, 0), 0));
		itl--;
		itu--;
		t1 = *itl;
		t2 = *itu;
		/*printf("%d %d %d\n", (t1.first).first, (t1.first).second, t1.second);
		printf("%d %d %d\n", (t2.first).first, (t2.first).second, t2.second);*/
		itu++;
		for(it = itl; it != itu; it++)
			freq[it->second] -= (it->first).second - (it->first).first + 1;
		cores.erase(itl, itu);
		freq[X] += M2-M1+1;
		if((t1.first).first < M1)
		{
			freq[t1.second] += (M1 - (t1.first).first);
			cores.insert(make_pair(make_pair((t1.first).first, M1-1), t1.second));
		}
		if((t2.first).second > M2)
		{
			freq[t2.second] += ((t2.first).second) - M2;
			cores.insert(make_pair(make_pair(M2+1, (t2.first).second), t2.second));
		}
		cores.insert(make_pair(make_pair(M1, M2), X));
	}
	for(int i = 1; i <= C; i++)
		if(ma < freq[i])
			ma = freq[i];
	printf("%d\n", ma);
	return 0;
}
