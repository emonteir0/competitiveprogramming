// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: A Long, Long Time Ago
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1962

#include<stdio.h>

main()
{
	int n,x;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&x);
		if(x>=2015)	printf("%d A.C.\n",x-2014);
		else	printf("%d D.C.\n",2015-x);
	}
	return 0;
}
