// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Output 3
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2749

#include<bits/stdc++.h>

using namespace std;

main()
{	
	printf("---------------------------------------\n");
	printf("|x = 35                               |\n");
	printf("|                                     |\n");
	printf("|               x = 35                |\n");
	printf("|                                     |\n");
	printf("|                               x = 35|\n");
	printf("---------------------------------------\n");
	return 0;
}
