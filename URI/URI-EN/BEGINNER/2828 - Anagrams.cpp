// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Anagrams
// Level: 4
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2828

#include<stdio.h>
#include<string.h>
#define MAXN 100001
#define MOD 1000000007
#define ll long long

int val[26];
char str[MAXN];
ll fatoriais[MAXN],invfat[MAXN];
   
void fat()
{
	int i;
	fatoriais[0] = 1;
	for(i = 1; i < MAXN; i++)
		fatoriais[i] = (i*fatoriais[i-1])%MOD;
}

ll mdc(ll  a, ll b, ll *x, ll *y) {
  ll xx, yy, d;
  if(b==0)
  {
    *x=1; *y=0;
    return a;
  }
  d = mdc(b, a%b, &xx, &yy);
  *x = yy;
  *y = xx - a/b*yy;
  return d;
}

ll inv(ll a)
{
	ll x,y,d;
	if(invfat[a]!=0)
		return invfat[a];
	d = mdc(fatoriais[a],MOD,&x,&y);
	if(x<0)
		x = x+MOD;
	invfat[a]=x;
	return x;
}
  
main()
{
    int N;           
    ll x;
    fat();
    invfat[0] = 1;
    invfat[1] = 1;
    scanf("%s", str);
    N = strlen(str);
    for(int i = 0; i < N; i++)
        val[str[i]-'a']++;
    x = fatoriais[N];
    for(int i = 0;i < 26; i++)
    	x = (x*inv(val[i]))%MOD;
    printf("%lld\n",x);
    return 0;
}
