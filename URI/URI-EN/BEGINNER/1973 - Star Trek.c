// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Star Trek
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1973

#include<stdio.h>

int vet[1000000], bool[1000000];


main()
{
	int N,i,x,cont=0;
	long long int acum=0;
	scanf("%d",&N);
	for(i=0;i<N;i++)
	{
		scanf("%d",&x);
		acum+=x;
		vet[i]=x;
	}
	i=0;
	while(1)
	{
		if(vet[i]%2==0)
		{
			if(bool[i]==0)
				cont++;
			bool[i]=1;
			if(vet[i]!=0)
			{
				vet[i]--;
				acum--;
			}
			i--;
		}
		if(i<0)
			break;
		if(vet[i]%2==1)
		{
			if(bool[i]==0)
				cont++;
			bool[i]=1;
			acum--;
			vet[i]--;
			i++;
		}
		if(i>=N)
			break;
	}
	printf("%d %lld\n",cont,acum);
	return 0;
}
