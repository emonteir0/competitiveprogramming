// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Logical Sequence 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1145

#include<stdio.h>

main()
{
	int a,b,i,j;
	scanf("%d %d",&a,&b);
	for(i=0;i<b/a;i++)
	{
		for(j=1;j<a;j++)	printf("%d ",i*a+j);
		printf("%d\n",(i+1)*a);
	}
	for(j=1;j<b%a;j++)	printf("%d ",i*a+j);
	if(b%a!=0)printf("%d\n",i*a+b%a);
	return 0;
}
