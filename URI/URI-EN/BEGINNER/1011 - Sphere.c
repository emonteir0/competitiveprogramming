// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sphere
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1011

#include<stdio.h>

main()
{
	double c;
	scanf("%lf",&c);
	printf("VOLUME = %.3f\n",c*c*c*3.14159*(4.0/3));
	return 0;
}
