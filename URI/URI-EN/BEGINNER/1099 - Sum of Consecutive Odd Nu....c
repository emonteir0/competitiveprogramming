// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sum of Consecutive Odd Nu...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1099

#include<stdio.h>

main()
{
	int a,b,c,d,n;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&a,&b);
		if(a-b>0)
		{
			b+=a;
			a=b-a;
			b-=a;
		}
			c=a+(a%2+1);
			c*=(c<b);
			d=b-(b%2+1);
			d*=(d>a);
			if(d==0||c==0)
				printf("0\n");
			else
				printf("%d\n",(c+d)*((d-c)/2+1)/2);
	}
	return 0;
}
