// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Iccanobif
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2807

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll fib[41];

main()
{
	int N;
	fib[1] = 1;
	for(int i = 2; i <= 40; i++)
		fib[i] = fib[i-1]+fib[i-2];
	scanf("%d", &N);
	printf("%lld", fib[N]);
	for(int i = N-1; i >= 1; i--)
		printf(" %lld", fib[i]);
	printf("\n");
	return 0;
}
