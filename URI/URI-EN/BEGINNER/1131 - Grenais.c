// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Grenais
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1131

#include<stdio.h>

main()
{
	int a,b,c=0,d=0,e=0,cont=0;
	int n=1;
	while(n==1)
	{
		cont++;
		scanf("%d %d",&a,&b);
		c+=a>b;
		d+=a<b;
		e+=a==b;
		printf("Novo grenal (1-sim 2-nao)\n");
		while(scanf("%d",&n)==1 && n<1||n>2)printf("Novo grenal (1-sim 2-nao)\n");
	}
	printf("%d grenais\nInter:%d\nGremio:%d\nEmpates:%d\n%s\n",cont,c,d,e,(c==d)?"Nao houve vencedor":((c>d)?"Inter venceu mais":"Gremio venceu mais"));
	return 0;
}
