// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Salary
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1008

#include<stdio.h>

main()
{
	int a,b;
	float c;
	scanf("%d %d %f",&a,&b,&c);
	printf("NUMBER = %d\nSALARY = U$ %.2f\n",a,b*c);
	return 0;
}
