// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even Square
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1073

#include<stdio.h>

main()
{
	int x,i;
	scanf("%d",&x);
	for(i=2;i<=x;i+=2)
	{
		printf("%d^2 = %d\n",i,i*i);
	}
	return 0;
}
