// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Digits
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2867

#include<bits/stdc++.h>

using namespace std;

main()
{
	int a, b, N;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d", &a, &b);
		if(a == 1)
			printf("1\n");
		else if(a == 10)
			printf("%d\n", 1 + b);
		else if(a == 100)
			printf("%d\n", 1 + b + b);
		else
		{
			printf("%.0lf\n", ceil((b*log(a))/log(10)));
		}
	}
	return 0;
}
