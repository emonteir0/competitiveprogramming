// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sum of Consecutive Odd Nu...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1158

#include<stdio.h>

main()
{
	long long int a,b;
	int n;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%lld %lld",&a,&b);
		a+=a%2==0;
		printf("%lld\n",(2*a+2*b-2)*b/2);
	}
	return 0;
}
