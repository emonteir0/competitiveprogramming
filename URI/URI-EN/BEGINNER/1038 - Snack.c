// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Snack
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1038

#include<stdio.h>

main()
{
	int a,b;
	double c[5]={4,4.5,5,2,1.5};
	scanf("%d %d",&a,&b);
	printf("Total: R$ %.2f\n",b*c[a-1]);
	return 0;
}
