// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Party at the North Pole
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2845

#include<bits/stdc++.h>
#define MAXN 100001

using namespace std;

vector<int> G[100001];
int isprime[100001];
int memo;
int vis[MAXN];

void crivo()
{
	for(int i = 2; i <= 100000; i += 2)
	{
		G[i].push_back(2);
		isprime[i] = 1;
	}
	for(int i = 3; i <= 100000; i += 2)
	{
		if(!isprime[i])
		{
			for(int j = i; j <= 100000; j+=i)
			{
				G[j].push_back(i);
				isprime[j] = 1;
			}
		}
	}	
}

main()
{
	int N, ma, x;
	crivo();
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &x);
		ma = max(x, ma);
		for(int j = 0; j < G[x].size(); j++)
		{
			if(!vis[G[x][j]])
			{
				for(int k = G[x][j]; k < MAXN; k += G[x][j])
					vis[k] = 1;
			}
		}
	}
	for(int i = ma+1; i < MAXN; i++)
		if(!vis[i])
			return !printf("%d\n", i);
	return 0;
}
