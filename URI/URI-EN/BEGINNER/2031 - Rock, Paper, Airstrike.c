// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rock, Paper, Airstrike
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2031

#include<stdio.h>

main()
{
	char vet[10],vet2[10];
	int N;
	char op,op2;
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%s%*c%s%*c",vet,vet2);
		if(vet[1]!=vet2[1])
		{
			if(vet[1]=='a')
				op=0;
			if(vet[1]=='e')
				op=1;
			if(vet[1]=='t')
				op=2;
			if(vet2[1]=='a')
				op2=0;
			if(vet2[1]=='e')
				op2=1;
			if(vet2[1]=='t')
				op2=2;
			printf("%s\n",op>op2?"Jogador 1 venceu":"Jogador 2 venceu");
		}
		else
		{
			if(vet[1]=='a')
				printf("Ambos venceram\n");
			if(vet[1]=='e')
				printf("Sem ganhador\n");
			if(vet[1]=='t')
				printf("Aniquilacao mutua\n");
		}
	}
	return 0;
}
