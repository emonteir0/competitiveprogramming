// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequence IJ 4
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1098

#include<stdio.h>

main()
{
	int i;
	printf("I=0 J=1\nI=0 J=2\nI=0 J=3\n");
	for(i=1;i<5;i++)	printf("I=%.1f J=%.1f\nI=%.1f J=%.1f\nI=%.1f J=%.1f\n",i*0.2,1+i*0.2,i*0.2,2+i*0.2,i*0.2,3+i*0.2);
	printf("I=1 J=2\nI=1 J=3\nI=1 J=4\n");
	for(i=6;i<10;i++)	printf("I=%.1f J=%.1f\nI=%.1f J=%.1f\nI=%.1f J=%.1f\n",i*0.2,1+i*0.2,i*0.2,2+i*0.2,i*0.2,3+i*0.2);
	printf("I=2 J=3\nI=2 J=4\nI=2 J=5\n");
	return 0;
}
