// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Honey Reservoir
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2029

#include<stdio.h>

main()
{
	double V,D,H,R,A;
	while(scanf("%lf %lf",&V,&D)==2)
	{
		H = 4*V/(3.14*D*D);
		R=D/2;
		A=3.14*R*R;
		printf("ALTURA = %.2lf\nAREA = %.2lf\n",H,A);
	}
	return 0;
}

