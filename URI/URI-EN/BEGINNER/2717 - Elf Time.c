// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elf Time
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2717

#include<stdio.h>

main()
{
	int N, A, B;
	scanf("%d %d %d", &N, &A, &B);
	printf("%s\n", (A+B) <= N ? "Farei hoje!" : "Deixa para amanha!");
	return 0;
}
