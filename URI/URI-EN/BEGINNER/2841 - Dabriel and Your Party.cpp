// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dabriel and Your Party
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2841

#include<bits/stdc++.h>

using namespace std;

int N;
int val[100001];
int pai[100001];
int vis[100001][2];
int pd[100001][2];

vector<int> G[100001];

int dfs(int x, int b)
{
	if(vis[x][b])
		return pd[x][b];
	vis[x][b] = 1;
	int r = 0;
	for(int i = 0; i < G[x].size(); i++)
		r += dfs(G[x][i], 0);
	if(b == 0)
	{
		int z = 0;
		for(int i = 0; i < G[x].size(); i++)
			z += dfs(G[x][i], 1);
		r = max(r, z + val[x]);
	}
	//printf("(%d, %d) -> %d\n", x, b, r);
	return pd[x][b] = r;
}

main()
{
	int total = 0;
	scanf("%d", &N);
	for(int i = 1; i <= N; i++)
		scanf("%d", &val[i]);
	for(int i = 2; i <= N; i++)
	{
		scanf("%d", &pai[i]);
		G[pai[i]].push_back(i);
	}
	printf("%d\n", dfs(1, 0));
	return 0;
}
