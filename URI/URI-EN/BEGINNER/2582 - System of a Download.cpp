// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: System of a Download
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2582

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x, y;
	string music[11];
	music[0] = "PROXYCITY";
	music[1] = "P.Y.N.G.";
	music[2] = "DNSUEY!";
	music[3] = "SERVERS";
	music[4] = "HOST!";
	music[5] = "CRIPTONIZE";
	music[6] = "OFFLINE DAY";
	music[7] = "SALT";
	music[8] = "ANSWER!";
	music[9] = "RAR?";
	music[10] = "WIFI ANTENNAS";
	scanf("%*d");
	while(scanf("%d %d", &x, &y) != EOF)
		printf("%s\n", music[x+y].c_str());
	return 0;
}
