// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tourists in the Huacachin...
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2708

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x, c = 0, c2 = 0;
	char msg[15];
	while(scanf("%s%*c", msg))
	{
		if(msg[0] == 'A')
			break;
		scanf("%d%*c", &x);
		if(msg[0] == 'S')
		{
			c++;
			c2 += x;
		}
		else if(msg[0] == 'V')
		{
			c--;
			c2 -= x;
		}
		else
			break;
	}
	printf("%d\n%d\n", c2, c);
	return 0;
}
