// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Simple Calculate
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1010

#include<stdio.h>

main()
{
	int a,b;
	double c,d=0;
	scanf("%d %d %lf",&a,&b,&c);
	d+=c*b;
	scanf("%d %d %lf",&a,&b,&c);
	d+=c*b;
	printf("VALOR A PAGAR: R$ %.2f\n",d);
	return 0;
}
