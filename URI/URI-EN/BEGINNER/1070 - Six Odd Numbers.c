// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Six Odd Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1070

#include<stdio.h>

main()
{
	int i,x;
	scanf("%d",&x);
	if(x%2==0)	x+=1;
	for(i=0;i<6;i++)	printf("%d\n",x+2*i);
	return 0;
}
