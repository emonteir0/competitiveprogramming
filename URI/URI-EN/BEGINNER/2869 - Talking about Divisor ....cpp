// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Talking about Divisor ...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2869

#include<bits/stdc++.h>
#define MOD 1000000007
#define ll long long

using namespace std;

int primos[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};

vector<int> divisores[101];
int ans[101];
int vis[101][20];
pair<double, int> pd[101][20];

void gen_div(int x)
{
	int y;
	for(int i = 1; i*i <= x; i++)
	{
		if(x%i == 0)
		{
			if(i*i != x)
				divisores[x].push_back(x/i);
			divisores[x].push_back(i);
		}
	}
	sort(divisores[x].begin(), divisores[x].end(), greater<int>());
	divisores[x].pop_back();
}

int expo(ll x, int e)
{
	ll z = 1;
	for(int i = 1; i <= e; i <<= 1)
	{
		if(i&e)
			z = (z*x)%MOD;
		x = (x*x)%MOD;
	}
	return int(z);
}

pair<double, int> menor(pair<double, int> x, pair<double, int> y)
{
	return x.first < y.first ? x : y;
}

pair<double, int> operator+ (pair<double, int> x, pair<double, int> y)
{
	ll z =(((ll)x.second)*y.second)%MOD;
	return {x.first+y.first, z};
}

pair<double, int> solve(int x, int y)
{
	if(x == 1)
		return {0, 1};
	if(vis[x][y])
		return pd[x][y];
	vis[x][y] = 1;
	pair<double, int> a = {1e80, 0}, b;
	ll z = 1;
	int e = 0;
	for(auto r:divisores[x])
	{
		b = {(r-1)*log(primos[y]), expo(primos[y], r-1)};
		a = menor(a, solve(x/r, y+1) + b);
	}
	return pd[x][y] = a;
}

main()
{
	int N, x;
	for(int i = 1; i <= 100; i++)
	{
		gen_div(i);
		ans[i] = solve(i, 0).second;
	}
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d", &x);
		printf("%d\n", ans[x]);
	}
	return 0;
}
