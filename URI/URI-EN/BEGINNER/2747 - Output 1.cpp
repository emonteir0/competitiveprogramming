// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Output 1
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2747

#include<bits/stdc++.h>

using namespace std;

main()
{	
	printf("---------------------------------------\n");
	printf("|                                     |\n");
	printf("|                                     |\n");
	printf("|                                     |\n");
	printf("|                                     |\n");
	printf("|                                     |\n");
	printf("---------------------------------------\n");
	return 0;
}
