// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Game Time with Minutes
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1047

#include<stdio.h>

main()
{
	int a,b,c,d;
	scanf("%d %d %d %d",&a,&b,&c,&d);
	d-=b;
	c-=a;
	if(c<0)	c+=24;
	if(d<0)
	{
		c=(c+23)%24;
		d+=60;
	}
	printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n",c,d);
	return 0;
}
