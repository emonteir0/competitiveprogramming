// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Divisors I
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1157

#include<stdio.h>

main()
{
	int i,a,b;
	scanf("%d",&a);
	b=a/2;
	printf("1\n");
	for(i=2;i<=b;i++)	if(a%i==0)	printf("%d\n",i);
	printf("%d\n",a);
	return 0;
}
