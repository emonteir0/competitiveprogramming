// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Square Root of 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2166

#include<stdio.h>
#include<math.h>

main()
{
	double value = 0.0;
	int n;
	scanf("%d",&n);
	while(n--)
		value = 1.0/(2.0+value);
	printf("%.10lf\n",1.0+value);
	return 0;
}

