// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Perfect Number
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1164

#include<stdio.h>
 
main()
{
    long long int a;
    int n;
    scanf("%d",&n);
    while(n--)
    {
        scanf("%lld",&a);
        if(a==6||a==28||a==496||a==8128||a==33550336)   printf("%lld eh perfeito\n",a);
        else    printf("%lld nao eh perfeito\n",a); 
    }
    return 0;
}
