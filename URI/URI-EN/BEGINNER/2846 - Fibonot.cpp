// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibonot
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2846

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll fib[60];
int cont[1000001];


main()
{
	int K;
	fib[1] = 1;
	for(int i = 2; i < 60; i++)
	{
		fib[i] = fib[i-1] + fib[i-2];
		if(fib[i] < 1000001)
			cont[fib[i]] = 1;
		else
			break;
	}
	for(int i = 1; i <= 1000000; i++)
		cont[i] = cont[i-1] + !cont[i];
	
	//printf("%d\n", cont[1000000]);
	scanf("%d", &K);
	printf("%d\n", lower_bound(cont, cont+1000000, K)-cont);
	return 0;
}
