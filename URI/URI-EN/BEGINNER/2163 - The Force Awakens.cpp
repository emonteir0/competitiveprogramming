// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Force Awakens
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2163

#include<cstdio>
#include<queue>

int mat[1001][1001];

typedef struct{
	int x,y;
} ponto;

using namespace std;

ponto p;
queue <ponto> fila;

main()
{
	int i, j, N, M;
	char ok;
	scanf("%d %d",&N,&M);
	for(i=1;i<=N;i++)
	{
		for(j=1;j<=M;j++)
		{
			scanf("%d",&mat[i][j]);
			if(mat[i][j] == 42 && i > 1 && i < N && j > 1 && j < M)
			{
				p.x = i;
				p.y = j;
				fila.push(p);
			}
		}
	}
	while(!fila.empty())
	{
		p = fila.front();
		fila.pop();
		ok = 1;
		for(i = -1; i <= 1; i++)
			for(j = -1; j <= 1; j++)
				if(!((i==0)&&(j==0)))
					ok &= (mat[p.x+i][p.y+j] == 7);
		if(ok)
		{
			printf("%d %d\n",p.x,p.y);
			return 0;
		}
	}
	printf("0 0\n");
	return 0;
}

