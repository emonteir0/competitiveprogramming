// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Robot Basketball
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2780

#include<bits/stdc++.h>

using namespace std;

main()
{
	int x, y;
	scanf("%d", &x);
	printf("%d\n", 1 + (x > 800) + (x > 1400));
	return 0;
}
