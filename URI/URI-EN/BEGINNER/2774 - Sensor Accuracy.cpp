// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sensor Accuracy
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2774

#include<bits/stdc++.h>

using namespace std;

double vet[100001];

main()
{
	int H, M, N;
	double media, vari;
	while(scanf("%d %d", &H, &M) == 2)
	{
		media = 0;
		vari = 0;
		N = (H*60)/M;
		for(int i = 0; i < N; i++)
		{
			scanf("%lf", &vet[i]);
			media += vet[i];
		}
		media /= N;
		for(int i = 0; i < N; i++)
			vari += (vet[i] - media) * (vet[i] - media);
		printf("%.5lf\n", sqrt(vari/(N-1)));
	}
	return 0;
}
