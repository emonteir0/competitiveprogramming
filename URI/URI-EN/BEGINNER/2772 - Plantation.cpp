// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Plantation
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2772

#include<bits/stdc++.h>
#define MAX 1048576
#define INFN -10485760
using namespace std;

struct st
{
	int fi, se;
	st()
	{
		fi = 0;
		se = 0;
	}
	st(int fi, int se)
	{
		this->fi = fi;
		this->se = se;
	}
};

int K;
int tree[MAX+1];
st lazy[MAX+1];

void changelazy(int node)
{
	if(lazy[2*node].fi == 0)
		lazy[2*node] = lazy[node];
	else
	{
		if(lazy[node].fi - lazy[2*node].se <= K)
			lazy[2*node].se = lazy[node].se;
		else
			lazy[2*node] = st();
	}
	if(lazy[2*node+1].fi == 0)
		lazy[2*node+1] = lazy[node];
	else
	{
		if(lazy[node].fi - lazy[2*node+1].se <= K)
			lazy[2*node+1].se = lazy[node].se;
		else
			lazy[2*node+1] = st();
	}
}

void updateRange(int node, int start, int end, int l, int r, int val)
{
    if(lazy[node].fi != 0)
    {
        if(lazy[node].fi - tree[node] <= K)
        {
        	tree[node] = lazy[node].se;
        	changelazy(node);
        }
        lazy[node] = st();
    }
    if(start > end || start > r || end < l)
        return;
    if(start >= l && end <= r)
    {
    	if(val - tree[node] <= K)
    	{
    		tree[node] = val;
    		if(lazy[2*node].fi == 0)
    			lazy[2*node] = st(val, val);
    		else
    		{
    			if(val - lazy[2*node].se <= K)
    				lazy[2*node].se = val;
    			else
    				lazy[2*node] = st();
    		}
    		if(lazy[2*node+1].fi == 0)
    			lazy[2*node+1] = st(val, val);
    		else
    		{
    			if(val - lazy[2*node+1].se <= K)
    				lazy[2*node+1].se = val;
    			else
    				lazy[2*node+1] = st();
    		}
    	}
    	else
    	{
    		tree[node] = 0;
    		lazy[2*node].fi = 0;
    		lazy[2*node].se = 0;
    		lazy[2*node+1].fi = 0;
    		lazy[2*node+1].se = 0;
    	}
        return;
    }
    int mid = (start + end) / 2;
    updateRange(2*node, start, mid, l, r, val);
    updateRange(2*node + 1, mid + 1, end, l, r, val);
    tree[node] = tree[2*node] + tree[2*node+1];
}

int query(int node, int start, int end, int x)
{
    if(lazy[node].fi != 0)
    {
        if(lazy[node].fi - tree[node] <= K)
        {
        	tree[node] = lazy[node].se;
        	changelazy(node);
        }
        lazy[node] = st();
    }
    if(start == x && end == x)
        return tree[node];
    int mid = (start + end) / 2;
    if(mid >= x)
    	return query(2*node, start, mid, x);
    else
    	return query(2*node+1, mid+1, end, x);
}

main()
{
	int N, M, a, b;
	vector<int> ans;
	while(scanf("%d %d %d", &N, &M, &K) == 3)
	{
		for(int i = 0; i <= MAX; i++)
			tree[i] = lazy[i].fi = lazy[i].se = 0;
		for(int i = 1; i <= M; i++)
		{
			scanf("%d %d", &a, &b);
			updateRange(1, 1, N, a, b, i);
		}
		ans.clear();
		for(int i = 1; i <= N; i++)
		{
			a = query(1, 1, N, i);
			if(M - a < K)
				ans.push_back(i);
		}
		printf("%d", ans.size());
		for(int i = 0; i < ans.size(); i++)
			printf(" %d", ans[i]);
		printf("\n");
	}
	return 0;
}

