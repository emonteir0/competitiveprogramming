// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Secret Chamber at Mount R...
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2646

#include<bits/stdc++.h>

using namespace std;

vector<int> G[26];
int bits[26], vis[26];

void dfs(int x, int s)
{
	int v;
	vis[x] = s+1;
	bits[s] |= (1 << x);
	for(int i = 0; i < G[x].size(); i++)
	{
		v = G[x][i];
		if(vis[v] <= s)
			dfs(v, s);
	}
}

main()
{
	int N, M;
	char a, b;
	string str, str2;
	scanf("%d %d%*c", &N, &M);
	for(int i = 0; i < N; i++)
	{
		scanf("%c %c%*c", &a, &b);
		G[a-'a'].push_back(b-'a');
	}
	for(int i = 0; i < 26; i++)
		dfs(i, i);
	while(M--)
	{
		cin >> str >> str2;
		if(str.length() != str2.length())
			printf("no\n");
		else
		{
			int i;
			for(i = 0; i < str.length(); i++)
				if(!(bits[str[i]-'a'] & (1 << (str2[i]-'a'))))
					break;
			printf("%s\n", (i == str.length()) ? "yes" : "no");
		}
	}
	return 0;
}
