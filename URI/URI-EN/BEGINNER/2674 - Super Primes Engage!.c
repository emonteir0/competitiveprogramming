// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Super Primes: Engage!
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2674

#include<stdio.h>
#define ll long long
#define MAXN 100000
#define SQN 350

int isprime[MAXN+1];

void crivo()
{
	ll i, j;
	isprime[0] = 1;
	isprime[1] = 1;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= SQN; i += 2)
		if(!isprime[i])
			for(j = i*i; j <= MAXN; j += i)
				isprime[j] = 1;
}

main()
{
	int N;
	crivo();
	while(scanf("%d", &N) == 1)
	{
		if(isprime[N])
			printf("Nada\n");
		else
		{
			while(N)
			{
				if(isprime[N%10])
					break;
				N /= 10;
			}
			printf("%s\n", N ? "Primo" : "Super");
		}
	}
	return 0;
}
