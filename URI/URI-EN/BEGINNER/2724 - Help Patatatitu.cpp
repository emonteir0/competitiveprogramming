// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Patatatitu
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2724

#include<bits/stdc++.h>

using namespace std;

string patterns[51];
char msg[51];
string str;

bool isUpper(char c)
{
	return c >= 'A' && c <= 'Z';
}

main()
{
	int i, j, t, x, T, N, M;
	scanf("%d%*c", &T);
	for(t = 0; t < T; t++)
	{
		if(t)
			printf("\n");
		scanf("%d%*c", &N);
		for(i = 0; i < N; i++)
		{
			scanf("%s%*c", msg);
			patterns[i] = msg;
		}
		scanf("%d%*c", &M);
		while(M--)
		{
			scanf("%s", msg);
			str = msg;
			for(i = 0; i < N; i++)
			{
				x = str.find(patterns[i]);
				if(x != -1)
				{
					if(x+patterns[i].length() < str.length())
					{
						if(isUpper(str[x+patterns[i].length()]))
							break;
					}
					else
						break;
				}
			}
			printf("%s\n", (i == N) ? "Prossiga" : "Abortar");
		}
	}
	return 0;
}
