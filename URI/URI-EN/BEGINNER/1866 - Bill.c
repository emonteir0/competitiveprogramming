// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bill
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1866

#include<stdio.h>

main()
{
	int a,N;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&a);
		printf("%d\n",(a%2)==1);
	}
	return 0;
}
