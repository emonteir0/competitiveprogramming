// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Average 2
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1006

#include<stdio.h>

main()
{
	double A,B,C;
	scanf("%lf %lf %lf",&A,&B,&C);
	printf("MEDIA = %.1f\n",(2*A+3*B+5*C)/10);
	return 0;
}
