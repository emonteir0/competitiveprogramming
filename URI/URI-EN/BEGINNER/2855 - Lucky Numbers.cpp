// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lucky Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2855

#include<bits/stdc++.h>

using namespace std;

int v[3000001];

main()
{
	int N, x, salto, pos, f;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 1; i <= N; i++)
			scanf("%d", &v[i]);
		scanf("%d", &x);
		for(int i = 1; i <= N; i++)
			if(v[i] == x)
			{
				pos = i;
				break;
			}
		f = 1;
		salto = 2;
		while(f && pos >= salto)
		{
			if(pos%salto == 0)
				f = 0;
			pos -= pos/salto;
			salto++;
		}
		printf("%s\n", f ? "Y" : "N");
	}
	return 0;
}
