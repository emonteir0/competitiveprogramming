// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Simple Factorial
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1153

#include<stdio.h>

long long int fat(long long int n)
{
	if(n)	return n*fat(n-1);
	else return 1;
}

main()
{
	int n;
	scanf("%d",&n);
	printf("%lld\n",fat(n));
	return 0;
}
