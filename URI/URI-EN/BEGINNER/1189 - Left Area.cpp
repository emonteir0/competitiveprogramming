// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Left Area
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1189

#include<stdio.h>

main()
{
	int i,j;
	double x,acum=0;
	char op;
	scanf("%c",&op);
	for(i=0;i<12;i++)
	{
		for(j=0;j<12;j++)
		{
			scanf("%lf",&x);
			if((j+i)<11&&j<i)
				acum+=x;
		}
	}
	if(op=='M')	acum/=30;
	printf("%.1lf\n",acum);
	return 0;
}
