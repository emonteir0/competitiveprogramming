// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Máquina de Café
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2670

#include<bits/stdc++.h>

using namespace std;

main()
{
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	printf("%d\n", 2*min(b+2*c, min(a+c, 2*a+b)));
	return 0;
}
