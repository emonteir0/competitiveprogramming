// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Positive Numbers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1060

#include<stdio.h>

main()
{
	int i,cont=0;
	double x;
	for(i=0;i<6;i++)
	{
		scanf("%lf",&x);
		if(x>0)	cont++;
	}
	printf("%d valores positivos\n",cont);
	return 0;
}
