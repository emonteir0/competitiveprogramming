// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Crowstorm
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2203

#include<stdio.h>
#include<math.h>

main()
{
	double A, B, C, D, E, F, G;
	while(scanf("%lf %lf %lf %lf %lf %lf %lf", &A, &B, &C, &D, &E, &F, &G) != EOF)
		printf("%s\n",(sqrt(pow(A-C,2)+pow(B-D,2)) + 1.5*E <= F+G)?"Y":"N");
	return 0;
}

