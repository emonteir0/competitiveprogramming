// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Regular Simple Polygons
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1959

#include <stdio.h>
 
int main() {
    unsigned long long int a,b;
    scanf("%llu %llu",&a,&b);
    printf("%llu\n",a*b);
    return 0;
}
