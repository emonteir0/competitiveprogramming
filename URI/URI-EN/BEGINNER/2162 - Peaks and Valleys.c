// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Peaks and Valleys
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2162

#include<stdio.h>

main()
{
	int n, x, xant;
	char ok = 1, op;
	scanf("%d",&n);
	scanf("%d %d",&xant, &x);
	n-=2;
	op = x>xant;
	ok &= (x!=xant);
	xant = x;
	while(n--)
	{
		scanf("%d", &x);
		ok &= op?x<xant:x>xant;
		op = !op;
		xant = x;
	}
	printf("%d\n", ok);
	return 0;
}

