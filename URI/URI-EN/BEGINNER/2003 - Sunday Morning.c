// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sunday Morning
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2003

#include<stdio.h>

main()
{
	int a,b,c;
	while(scanf("%d:%d",&a,&b)==2)
	{
		c=60*(a-7)+b;
		if(c>0)	printf("Atraso maximo: %d\n",c);
		else	printf("Atraso maximo: 0\n");
	}
	return 0;
}
