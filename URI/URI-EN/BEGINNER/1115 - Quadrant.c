// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Quadrant
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1115

#include<stdio.h>

main()
{
	int a,b;
	while(scanf("%d %d",&a,&b)==2&&a&&b)
	{
		if(a>0)
			if(b>0)	printf("primeiro\n");
			else	printf("quarto\n");
		else
			if(b>0)	printf("segundo\n");
			else	printf("terceiro\n");
	}
	return 0;
}
