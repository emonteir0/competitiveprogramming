// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Selection Test 1
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1035

#include<stdio.h>

main()
{
	int a,b,c,d;
	scanf("%d %d %d %d",&a,&b,&c,&d);
	printf("%s",((b>c)&&(d>a)&&(c+d)>(b+a)&&(c>0)&&(d>0)&&(a%2==0))?"Valores aceitos\n":"Valores nao aceitos\n");
	return 0;
}
