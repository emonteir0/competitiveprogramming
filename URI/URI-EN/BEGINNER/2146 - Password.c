// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Password
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2146

#include<stdio.h>

main()
{
	int n;
	while(scanf("%d",&n )==1)
		printf("%d\n",n-1);
	return 0;
}

