// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Welcome to the Winter!
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1847

#include<stdio.h>

main()
{
	int a,b,c;
	scanf("%d %d %d",&a,&b,&c);
	printf("%s\n",((c-b)>(b-a))||(((c-b)==(b-a))&&(b>a))?":)":":(");
	return 0;
}
