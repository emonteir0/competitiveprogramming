// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Coordinates of a Point
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1041

#include<stdio.h>

main()
{
	double a,b;
	scanf("%lf %lf",&a,&b);
	if(a>0&&b>0)	printf("Q1\n");
	else if(a<0&&b>0)	printf("Q2\n");
	else if(a<0&&b<0)	printf("Q3\n");
	else if(a>0&&b<0)	printf("Q4\n");
	else if(a!=0&&b==0)	printf("Eixo X\n");
	else if(a==0&&b!=0)	printf("Eixo Y\n");
	else	printf("Origem\n");
	return 0;
}
