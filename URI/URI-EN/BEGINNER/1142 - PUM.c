// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: PUM
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1142

#include<stdio.h>

main()
{
	int a,i;
	scanf("%d",&a);
	for(i=0;i<a;i++)	printf("%d %d %d PUM\n",4*i+1,4*i+2,4*i+3);
	return 0;
}
