// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Line in Array
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1181

#include<stdio.h>

main()
{
	int i,j,n;
	double x,acum=0;
	char op;
	scanf("%d%*c%c",&n,&op);
	for(i=0;i<12;i++)
	{
		for(j=0;j<12;j++)
		{
			scanf("%lf",&x);
			if(i==n)
				acum+=x;
		}
	}
	if(op=='M')	acum/=12;
	printf("%.1lf\n",acum);
	return 0;
}
