// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: S Sequence
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1155

#include<stdio.h>

main()
{
	int i;
	float acum=0;
	for(i=1;i<=100;i++)
		acum+=1/((float)i);
	printf("%.2f\n",acum);
	return 0;
}
