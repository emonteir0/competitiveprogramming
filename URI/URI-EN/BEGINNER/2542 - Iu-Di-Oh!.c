// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Iu-Di-Oh!
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2542

#include<stdio.h>

int mat[101][101];
int mat2[101][101];

main()
{
	int N, M, L, x, y, z, i, j;
	while(scanf("%d", &N) == 1)
	{
		scanf("%d %d", &M, &L);
		for(i = 1; i <= M; i++)
			for(j = 1; j <= N; j++)
				scanf("%d", &mat[i][j]);
		for(i = 1; i <= L; i++)
			for(j = 1; j <= N; j++)
				scanf("%d", &mat2[i][j]);
		scanf("%d %d %d", &x, &y, &z);
		if(mat[x][z] > mat2[y][z])
			printf("Marcos\n");
		else if(mat[x][z] < mat2[y][z])
			printf("Leonardo\n");
		else
			printf("Empate\n");
	}
	return 0;
}

