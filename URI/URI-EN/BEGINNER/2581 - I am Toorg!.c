// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: I am Toorg!
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2581

#include<stdio.h>

main()
{
	int N;
    scanf("%d%*c", &N);
    while(N--)
    {
    	scanf("%*s");
        printf("I am Toorg!\n");
    }
    return 0;
}
