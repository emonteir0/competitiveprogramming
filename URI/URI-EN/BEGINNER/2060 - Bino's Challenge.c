// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bino's Challenge
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2060

#include<stdio.h>

main()
{
	int cont2=0,cont3=0,cont4=0,cont5=0,n,x;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&x);
		cont2 += (x%2)==0;
		cont3 += (x%3)==0;
		cont4 += (x%4)==0;
		cont5 += (x%5)==0;
	}
	printf("%d Multiplo(s) de 2\n%d Multiplo(s) de 3\n%d Multiplo(s) de 4\n%d Multiplo(s) de 5\n",cont2,cont3,cont4,cont5);
	return 0;
}

