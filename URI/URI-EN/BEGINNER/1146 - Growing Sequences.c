// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Growing Sequences
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1146

#include<stdio.h>

main()
{
	int a,b,i,j;
	while(scanf("%d",&a)==1&&a)
	{
		for(i=1;i<a;i++)	printf("%d ",i);
		printf("%d\n",a);
	}
	return 0;
}
