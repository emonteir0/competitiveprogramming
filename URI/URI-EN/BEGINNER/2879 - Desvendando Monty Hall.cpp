// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Desvendando Monty Hall
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2879

#include<bits/stdc++.h>

using namespace std;

main()
{
    int N, x, cnt = 0;
    scanf("%d", &N);
    while(N--)
    {
        scanf("%d", &x);
        cnt += x != 1;
    }
    printf("%d\n", cnt);
    return 0;
}
