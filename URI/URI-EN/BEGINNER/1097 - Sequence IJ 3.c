// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequence IJ 3
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1097

#include<stdio.h>

main()
{
	int i;
	for(i=1;i<=9;i+=2)	printf("I=%d J=%d\nI=%d J=%d\nI=%d J=%d\n",i,6+i,i,5+i,i,4+i);
	return 0;
}
