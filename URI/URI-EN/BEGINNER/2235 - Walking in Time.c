// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Walking in Time
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2235

#include<stdio.h>

main()
{
    int A, B, C;
    scanf("%d %d %d", &A, &B, &C);
    printf("%s\n",((A==C)||(B==C)||(A==B)||(A+B==C)||(B+C==A)||(A+C==B))?"S":"N");
    return 0;
}
