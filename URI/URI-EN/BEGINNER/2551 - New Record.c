// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: New Record
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2551

#include<stdio.h>

main()
{
	int i, N, x, y;
	double max, z;
	while(scanf("%d", &N) == 1)
	{
		max = 0;
		for(i = 1; i <= N; i++)
		{
			scanf("%d %d", &x, &y);
			z = ((double)y)/x;
			if(z > max)
			{
				max = z;
				printf("%d\n", i);
			}
		}
	}
	return 0;
}
