// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bean
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2791

#include<bits/stdc++.h>

using namespace std;

int vet[4];

main()
{
	for(int i = 0; i < 4; i++)
		scanf("%d", &vet[i]);
	for(int i = 0; i < 4; i++)
	{
		if(vet[i] == 1)
			return !printf("%d\n", i+1);
	}
	return 0;
}

