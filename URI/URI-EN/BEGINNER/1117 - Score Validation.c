// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Score Validation
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1117

#include<stdio.h>

main()
{
	float a,b;
	int n;
	while(scanf("%f",&a)==1&&a<0||a>10)	printf("nota invalida\n");
	while(scanf("%f",&b)==1&&b<0||b>10)	printf("nota invalida\n");
	printf("media = %.2f\n",(a+b)/2);
	return 0;
}
