// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Playing with Sets
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2222

#include<stdio.h>
#define ll unsigned long long int

ll vet[10001];

int cont(ll x)
{
	int tot = 0;
	while(x>0)
	{
		tot += x&1;
		x>>=1;
	}
	return tot;
}

main()
{
	int T, K, N, i, j, x, a, b, c;
	ll y;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &K);
		for(i=1;i<=K;i++)
		{
			y = 0;
			scanf("%d", &N);
			while(N--)
			{
				scanf("%d", &x);
				y |= (1LL<<x);
			}
			vet[i] = y;
		}
		scanf("%d", &N);
		while(N--)
		{
			scanf("%d %d %d", &a, &b, &c);
			if(a==1)
				printf("%d\n", cont(vet[b]&vet[c]));
			else
				printf("%d\n", cont(vet[b]|vet[c]));
		}
	}
	return 0;
}
