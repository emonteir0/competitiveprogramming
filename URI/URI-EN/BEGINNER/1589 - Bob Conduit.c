// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bob Conduit
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1589

#include<stdio.h>

main()
{
	int i,j,n;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d %d",&i,&j);
		printf("%d\n",i+j);
	}
	return 0;
}
