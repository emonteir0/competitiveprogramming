// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Keep It Covered
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2705

#include <bits/stdc++.h>

using namespace std;

struct edge { int u, v, rev, cap, f; };

const int MAXN = 402, INF = 0x3f3f3f3f;
vector<edge> adj[MAXN];

void add_edge(int u, int v, int cap) 
{
	adj[u].push_back((edge){u, v, (int)adj[v].size(), cap, 0});
	adj[v].push_back((edge){v, u, (int)adj[u].size() - 1, 0, 0});
}

int edmonds_karp(int nodes, int source, int sink) 
{
	int max_flow = 0;
	for (;;)
	{
		vector<edge*> pred(nodes, (edge*)0);
		queue<int> q;
		q.push(source);
		while (!q.empty() && !pred[sink]) 
		{
			int u = q.front();
			q.pop();
			for (int j = 0; j < (int)adj[u].size(); j++) 
			{
				edge &e = adj[u][j];
				if (!pred[e.v] && e.cap > e.f)
				{
					pred[e.v] = &e;
					q.push(e.v);
				}
			}
		}
		if (!pred[sink]) 
		{
			break;
		}
		int flow = INF;
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			flow = min(flow, pred[u]->cap - pred[u]->f);
		}
		for (int u = sink; u != source; u = pred[u]->u) 
		{
			pred[u]->f += flow;
			adj[pred[u]->v][pred[u]->rev].f -= flow;
		}
		max_flow += flow;
	}
	return max_flow;
}

char palavras[30][31];
int N, M;
int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1};

int inRange(int x, int y)
{
	return x >= 0 && x < N && y >=0 && y < M;
}

int val(int x, int y)
{
	return x*M+y+1;
}

int main() 
{
	int source, sink, total = 0, total2 = 0;
	scanf("%d %d%*c", &N, &M);
	source = 0;
	sink = N*M+1;
	for(int i = 0; i < N; i++)
		scanf("%s", palavras[i]);
	for(int i = 0; i < N; i++)
	{
		for(int j = i&1; j < M; j += 2)
		{
			total += 2-(palavras[i][j] == 'o');
			add_edge(0, val(i, j), 2-(palavras[i][j] == 'o'));
			for(int k = 0; k < 4; k++)
			{
				if(inRange(i+dx[k], j+dy[k]))
					add_edge(val(i, j), val(i+dx[k], j+dy[k]), 
						1+((palavras[i][j] == '-') && (palavras[i][j] == palavras[i+dx[k]][j+dy[k]])));
			}
		}
		for(int j = !(i&1); j < M; j += 2)
		{
			total2 += 2-(palavras[i][j] == 'o');
			add_edge(val(i, j), sink, 2-(palavras[i][j]=='o'));
		}
	}
	printf("%s\n", ((total2 == total) && (total == edmonds_karp(sink+1, source, sink))) ? "Y" : "N");
	return 0;
}
