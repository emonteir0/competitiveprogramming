// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Easy Fibonacci
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1151

#include<stdio.h>

main()
{
	int i,n,a1=0,a2=1,aux;
	scanf("%d",&n);
	printf("0");
	if(n>1)	printf(" 1");
	for(i=3;i<=n;i++)
	{
		a2+=a1;
		a1=a2-a1;
		printf(" %d",a2);
	}
	printf("\n");
	return 0;
}
