// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: 3-RSA
// Level: 5
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2636

#include<bits/stdc++.h>
#define ll long long
#define MAXN 1000001

using namespace std;

int primos[MAXN], prime_cont = 0;
int isprime[MAXN];

ll abs_val(ll a)
{
	if(a < 0)
		return -a;
	return a;
}

void crivo()
{
	ll i, j;
	for(i = 4; i <= MAXN; i += 2)
		isprime[i] = 1;
	for(i = 3; i <= MAXN; i += 2)
	{
		if(!isprime[i])
		{
			primos[prime_cont++] = i;
			for(j = i*i; j <= MAXN; j += i)
				isprime[j] = 1;
		}
	}
}

ll mulmod(ll a, ll b, ll c)
{
	ll x = 0, y = a % c;
	while(b > 0)
	{
		if(b % 2 == 1)
			x = (x+y) % c;
		y = (y*2) % c;
		b /= 2;
	}
	return x % c;
}

ll gcd(ll a, ll b) { return !b ? a : gcd(b, a % b); }

ll pollard_rho(ll n)
{
	ll x = 3, y = 3, i = 0, k = 2;
	while(1)
	{
		i++;
		x = (mulmod(x, x, n) + n - 1) % n;
		ll d = gcd(abs_val(y - x), n);
		if(d != 1 && d != n)	return d;
		if(i == k)	y = x, k *= 2;
	}
}

int main()
{
	int i;
	ll vet[3], N, M;
	crivo();
	while(scanf("%lld", &N) == 1 && N)
	{
		vet[0] = vet[1] = vet[2] = 0;
		M = N;
		for(i = 0; i < prime_cont; i++)
		{
			if((M % primos[i] == 0) && (vet[0] == 0))
			{
				vet[0] = primos[i];
				M /= vet[0];
			}
			if((M % primos[i] == 0) && (vet[0] != 0))
			{
				vet[1] = primos[i];
				M /= vet[1];
				break;
			}
		}
		if(vet[1] == 0)
		{
			vet[1] = pollard_rho(M);
			vet[2] = M / vet[1];
		}
		else
		{
			vet[2] = M;
		}
		sort(vet, vet+3);
		printf("%lld = %lld x %lld x %lld\n", N, vet[0], vet[1], vet[2]);
	}
	return 0;
}
