// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Stepladder
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2782

#include<bits/stdc++.h>

using namespace std;

int vet[1000];

main()
{
	int N, v, xant, cont = 1;
	scanf("%d", &N);
	for(int i = 0; i < N; i++)
		scanf("%d", &vet[i]);
	for(int i = 2; i < N; i++)
	{
		if(vet[i]-vet[i-1] != vet[i-1]-vet[i-2])
			cont++;
	}
	printf("%d\n", cont);
	return 0;
}
