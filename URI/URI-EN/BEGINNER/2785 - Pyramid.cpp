// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pyramid
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2785

#include<bits/stdc++.h>

using namespace std;

int N;
int vis[101][101][101], dp[101][101][101];
int mat[101][101], acum[101][101];


int solve(int lin, int esq, int dir)
{
	if(lin == 0)
		return 0;
	if(vis[lin][esq][dir])
		return dp[lin][esq][dir];
	vis[lin][esq][dir] = 1;
	dp[lin][esq][dir] = min(solve(lin-1, esq+1, dir), solve(lin-1, esq, dir-1)) + acum[lin][dir]-acum[lin][esq-1];
	//printf("%d, %d, %d -> %d\n", lin, esq, dir, dp[lin][esq][dir]);
	return dp[lin][esq][dir];
}

main()
{
	int sum = 0;
	scanf("%d", &N);
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= N; j++)
		{
			scanf("%d", &mat[i][j]);
			acum[i][j] = acum[i][j-1] + mat[i][j];
		}
	printf("%d\n", solve(N, 1, N));
	return 0;
}
