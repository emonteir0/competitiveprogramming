// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Roller Coaster
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2547

#include<stdio.h>

main()
{
	int N, x, Amin, Amax, cont;
	while(scanf("%d %d %d", &N, &Amin, &Amax) == 3)
	{
		cont = 0;
		while(N--)
		{
			scanf("%d", &x);
			if(x >= Amin && x <= Amax)
				cont++;
		}
		printf("%d\n", cont);
	}
	return 0;
}
