// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Fill IV
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1179

#include<stdio.h>

main()
{
	int i,par[5],impar[5],np=0,ni=0,x,j;
	for(i=0;i<15;i++)
	{
		scanf("%d",&x);
		if(x%2==0)	par[np++]=x;
		else impar[ni++]=x;
		if(np==5)
			for(j=0;j<5;j++)
			{
				printf("par[%d] = %d\n",j,par[j]);
				np--;
			}
		if(ni==5)
			for(j=0;j<5;j++)
			{
				printf("impar[%d] = %d\n",j,impar[j]);
				ni--;
			}
	}
	for(i=0;i<ni;i++)
		printf("impar[%d] = %d\n",i,impar[i]);
	for(i=0;i<np;i++)
		printf("par[%d] = %d\n",i,par[i]);
	return 0;
}
