// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Theon's Answer
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1858

#include<stdio.h>

main()
{
	int a, i,N,me=21,ime=-1;
	scanf("%d",&N);
	for(i=1;i<=N;i++)
	{
		scanf("%d",&a);
		if(me>a)
		{
			me=a;
			ime=i;
		}
	}
	printf("%d\n",ime);
	return 0;
}
