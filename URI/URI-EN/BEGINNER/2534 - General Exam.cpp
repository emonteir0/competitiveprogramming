// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: General Exam
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2534

#include<bits/stdc++.h>

using namespace std;

int v[100];

main()
{
	int i, N, Q, x;
	while(scanf("%d %d", &N, &Q) != EOF)
	{
		for(i = 0; i < N; i++)
			scanf("%d", &v[i]);
		sort(v, v+N, greater<int>());
		while(Q--)
		{
			scanf("%d", &x);
			printf("%d\n", v[x-1]);
		}
	}
	return 0;
}
