// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Positives and Average
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1064

#include<stdio.h>

main()
{
	int i,cont=0;
	double x,acum;
	for(i=0;i<6;i++)
	{
		scanf("%lf",&x);
		if(x>0){	cont++;acum+=x;}
	}
	printf("%d valores positivos\n%.1f\n",cont,acum/cont);
	return 0;
}
