// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Assembly Line
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2769

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N;
int e1, e2;
int a[1000001][2], t[1000001][2];

int dp[1000001][2];

int solve(int x, int e)
{
	if(x == N-1)
		return a[N-1][e] + t[N-1][e];
	if(dp[x][e] != -1)
		return dp[x][e];
	int r = a[x][e] + min(solve(x+1, e), solve(x+1, !e) + t[x][e]);
	return dp[x][e] = r;
}

main()
{
	while(scanf("%d", &N) == 1)
	{
		scanf("%d %d", &e1, &e2);
		for(int i = 0; i < N; i++)
			scanf("%d", &a[i][0]);
		for(int i = 0; i < N; i++)
			scanf("%d", &a[i][1]);
		for(int i = 0; i < N-1; i++)
			scanf("%d", &t[i][0]);
		for(int i = 0; i < N-1; i++)
			scanf("%d", &t[i][1]);
		scanf("%d %d", &t[N-1][0], &t[N-1][1]);
		for(int i = 0; i < N; i++)
			dp[i][0] = dp[i][1] = -1;
		printf("%d\n", min(solve(0, 0) + e1, solve(0, 1) + e2));
	}
	return 0;
}
