// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multiplication Table
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1078

#include<stdio.h>

main()
{
	int n,x,i;
	scanf("%d",&n);
	for(i=1;i<=10;i++)	printf("%d x %d = %d\n",i,n,i*n);
	return 0;
}
