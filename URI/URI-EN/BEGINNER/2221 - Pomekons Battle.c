// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pomekons Battle
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2221

#include<stdio.h>

main()
{
	int A1, D1, L1, A2, D2, L2, N, B, x, y;
	scanf("%d", &N);
	while(N--)
	{
		scanf("%d %d %d %d %d %d %d", &B, &A1, &D1, &L1, &A2, &D2, &L2);
		x = (A1+D1)/2 + ((L1&1)?0:B);
		y = (A2+D2)/2 + ((L2&1)?0:B);
		if(x==y)
			printf("Empate\n");
		else if(x>y)
			printf("Dabriel\n");
		else
			printf("Guarte\n");
	}
	return 0;
}
