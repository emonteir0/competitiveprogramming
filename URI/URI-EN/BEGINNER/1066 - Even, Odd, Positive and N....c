// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even, Odd, Positive and N...
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1066

#include<stdio.h>

main()
{
	int i,x,cont=0,cont2=0,cont3=0,cont4=0;
	for(i=0;i<5;i++)
	{
		scanf("%d",&x);
		if(x%2==0)	cont++;
		else cont2++;
		if(x>0)	cont3++;
		if(x<0)	cont4++;
		
	}
	printf("%d valor(es) par(es)\n%d valor(es) impar(es)\n%d valor(es) positivo(s)\n%d valor(es) negativo(s)\n",cont,cont2,cont3,cont4);
	return 0;
}
