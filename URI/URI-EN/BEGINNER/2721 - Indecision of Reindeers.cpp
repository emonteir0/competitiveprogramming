// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Indecision of Reindeers
// Level: 3
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2721

#include<bits/stdc++.h>

using namespace std;

map<int, string> Mapa;


main()
{
	int v1, v2, v3, v4, v5, v6, v7, v8, v9;
	Mapa[1] = "Dasher";
	Mapa[2] = "Dancer";
	Mapa[3] = "Prancer";
	Mapa[4] = "Vixen";
	Mapa[5] = "Comet";
	Mapa[6] = "Cupid";
	Mapa[7] = "Donner";
	Mapa[8] = "Blitzen";
	Mapa[0] = "Rudolph";
	scanf("%d %d %d %d %d %d %d %d %d", &v1, &v2, &v3, &v4, &v5, &v6, &v7, &v8, &v9);
	printf("%s\n", Mapa[(v1+v2+v3+v4+v5+v6+v7+v8+v9)%9].c_str());
	return 0;
}
