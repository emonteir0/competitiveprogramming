// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sequence IJ 1
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1095

#include<stdio.h>

main()
{
	int i;
	for(i=0;i<=12;i++)	printf("I=%d J=%d\n",1+3*i,60-i*5);
	return 0;
}
