// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Kage Bunshin no Jutsu
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2544

#include <bits/stdc++.h>
using namespace std;

int main() {
	long long n;
    while (cin >> n) {
    	cout << ceil(log2(n)) << endl;
    }
	return 0;
}
