// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Area
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1012

#include<stdio.h>

main()
{
	double a,b,c;
	scanf("%lf %lf %lf",&a,&b,&c);
	printf("TRIANGULO: %.3f\nCIRCULO: %.3f\nTRAPEZIO: %.3f\nQUADRADO: %.3f\nRETANGULO: %.3f\n",a*c/2,3.14159*c*c,(a+b)*c/2,b*b,a*b);
	return 0;
}
