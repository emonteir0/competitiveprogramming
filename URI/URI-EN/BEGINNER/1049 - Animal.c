// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Animal
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1049

#include<stdio.h>

main()
{
	char v[20],v2[20],v3[20];
	scanf("%s %s %s",v,v2,v3);
	if(v[0]=='v')
	{
		if(v2[0]=='a')
		{
			if(v3[0]=='c')	printf("aguia\n");
			else	printf("pomba\n");
		}
		else
		{
			if(v3[0]=='o')	printf("homem\n");
			else	printf("vaca\n");
		}
	}
	else
	{
		if(v2[0]=='i')
		{
			if(v3[2]=='m')	printf("pulga\n");
			else	printf("lagarta\n");
		}
		else
		{
			if(v3[0]=='h')	printf("sanguessuga\n");
			else	printf("minhoca\n");
		}
	}
	return 0;
}
