// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Greatest
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1013

#include<stdio.h>

main()
{
	int a,b,c;
	scanf("%d %d %d",&a,&b,&c);
	a=a>b?a:b;
	a=a>c?a:c;
	printf("%d eh o maior\n",a);
	return 0;
}
