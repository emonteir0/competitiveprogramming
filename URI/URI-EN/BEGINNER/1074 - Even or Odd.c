// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even or Odd
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1074

#include<stdio.h>

main()
{
	int n,x,i;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d",&x);
		if(x%2==0)
		{
			if(x>0)	printf("EVEN POSITIVE\n");
			else if(x<0)	printf("EVEN NEGATIVE\n");
			else printf("NULL\n");
		}
		else
		{
			if(x>0)	printf("ODD POSITIVE\n");
			else	printf("ODD NEGATIVE\n");
		}
	}
	return 0;
}
