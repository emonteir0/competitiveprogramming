// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Triangle
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1929

#include<stdio.h>

int abs(int a)
{
	if(a<0)
		return -a;
	return a;
}

int existe(int a, int b, int c)
{
	int x=(a>abs(b-c));
	x&=(a<(b+c));
	return x;
}

main()
{
	int a,b,c,d;
	scanf("%d %d %d %d",&a,&b,&c,&d);
	printf("%s\n",(existe(a,b,c)||existe(a,b,d)||existe(a,c,d)||existe(b,c,d))?"S":"N");
	return 0;
}
