// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Multiples
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1044

#include<stdio.h>

main()
{
	int a,b;
	scanf("%d %d",&a,&b);
	printf("%s\n",(a%b==0||b%a==0)?"Sao Multiplos":"Nao sao Multiplos");
	return 0;
}
