// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: School Floor
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2786

#include<bits/stdc++.h>

using namespace std;

main()
{
    int L, C;
    scanf("%d %d", &L, &C);
    printf("%d\n%d\n", L*C+(L-1)*(C-1), 2*(L+C-2));
    return 0;
}
