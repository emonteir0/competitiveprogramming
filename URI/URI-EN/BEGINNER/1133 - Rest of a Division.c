// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rest of a Division
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1133

#include<stdio.h>

main()
{
	int a,b,i;
	scanf("%d %d",&a,&b);
	if(a>b)
	{
		b+=a;
		a=b-a;
		b-=a;
	}
	for(i=a+1;i<b;i++)
	{
		if(i%5==2||i%5==3)	printf("%d\n",i);
	}
	return 0;
}
