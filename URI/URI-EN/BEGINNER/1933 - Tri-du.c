// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tri-du
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1933

#include<stdio.h>

main()
{
	int a,b;
	scanf("%d %d",&a,&b);
	printf("%d\n",(a>b)?a:b);
	return 0;
}
