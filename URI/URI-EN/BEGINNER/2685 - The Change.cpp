// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Change
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2685

#include<bits/stdc++.h>

using namespace std;

main()
{
	int N;
	while(scanf("%d", &N) == 1)
	{
		if(N > 360)
			continue;
		if(N == 360 || N < 90)
			printf("Bom Dia!!\n");
		else if (N >= 90 && N < 180)
			printf("Boa Tarde!!\n");
		else if(N >= 180 && N < 270)
			printf("Boa Noite!!\n");
		else 
			printf("De Madrugada!!\n");
	}
	return 0;
}
