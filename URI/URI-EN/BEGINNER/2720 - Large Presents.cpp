// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Large Presents
// Level: 2
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2720

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int v, id;	
};

st vet[100001];

bool cmp(st x, st y)
{
	if(x.v != y.v)
		return x.v > y.v;
	return x.id < y.id;
}

main()
{
	int i, T, N, K, a, b, c, d;
	vector<int> ans;
	scanf("%d", &T);
	while(T--)
	{
		ans.clear();
		scanf("%d %d", &N, &K);
		for(i = 0; i < N; i++)
		{
			scanf("%d %d %d %d", &a, &b, &c, &d);
			vet[i].v = b*c*d;
			vet[i].id = a;
		}
		sort(vet, vet+N, cmp);
		for(i = 0; i < K; i++)
			ans.push_back(vet[i].id);
		sort(ans.begin(), ans.end());
		printf("%d", ans[0]);
		for(i = 1; i < K; i++)
			printf(" %d", ans[i]);
		printf("\n");
	}
	return 0;
}
