// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Distance Between Two Points
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1015

#include<stdio.h>
#include<math.h>

main()
{
	double a,b,c,d;
	scanf("%lf %lf %lf %lf",&a,&b,&c,&d);
	printf("%.4f\n",sqrt((a-c)*(a-c)+(b-d)*(b-d)));
	return 0;
}
