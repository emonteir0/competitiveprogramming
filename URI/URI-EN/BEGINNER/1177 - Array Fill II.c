// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Array Fill II
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1177

#include<stdio.h>

main()
{
	int i,n;
	scanf("%d",&n);
	for(i=0;i<1000;i++)	printf("N[%d] = %d\n",i,i%n);
	return 0;
}
