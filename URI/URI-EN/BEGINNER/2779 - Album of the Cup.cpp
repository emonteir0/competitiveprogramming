// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Album of the Cup
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2779

#include<bits/stdc++.h>

using namespace std;

set<int> conju;

main()
{
	int N, M, x;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d", &x);
		conju.insert(x);
	}
	printf("%d\n", N - conju.size());
	return 0;
}
