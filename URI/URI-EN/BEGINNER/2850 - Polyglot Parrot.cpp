// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Polyglot Parrot
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2850

#include<bits/stdc++.h>

using namespace std;

main()
{
	string str;
	while(getline(cin, str))
	{
		if(str == "esquerda")
			cout << "ingles" << endl;
		else if(str == "direita")
			cout << "frances" << endl;
		else if(str == "nenhuma")
			cout << "portugues" << endl;
		else
			cout << "caiu" << endl;
	}
	return 0;
}
