// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Promotions
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2776

#include<bits/stdc++.h>
#define INFN -1001001001
using namespace std;

int N;
int peso[1001], custo[1001];
int dp[2001];

int solve(int x)
{
	if(x < 0)
		return INFN;
	if(x == 0)
		return 0;
	if(dp[x] != -1)
		return dp[x];
	int r = INFN;
	for(int i = 0; i < N; i++)
		r = max(r, solve(x-peso[i]) + custo[i]);
	return dp[x] = r;
}

main()
{
	int M;
	while(scanf("%d %d", &N, &M) == 2)
	{
		for(int i = 0; i < N; i++)
			scanf("%d %d", &peso[i], &custo[i]);
		for(int i = 0; i <= M; i++)
			dp[i] = -1;
		printf("%d\n", solve(M));
	}
	return 0;
}
