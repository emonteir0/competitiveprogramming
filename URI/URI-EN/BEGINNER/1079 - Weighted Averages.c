// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Weighted Averages
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1079

#include<stdio.h>

main()
{
	int n,i;
	double x,y,z;
	scanf("%d",&n);
	for(i=0;i<n;i++)
	{
		scanf("%lf %lf %lf",&x,&y,&z);
		printf("%.1f\n",(2*x+3*y+5*z)/10);
	}
	return 0;
}
