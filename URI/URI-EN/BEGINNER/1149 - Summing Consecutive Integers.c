// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Summing Consecutive Integers
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1149

#include<stdio.h>

main()
{
	int a,b,i,j;
	scanf("%d",&a);
	while(scanf("%d",&b)==1&&b<=0);
	printf("%d\n",(2*a+b-1)*b/2);
	return 0;
}
