// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Secret Code
// Level: 1
// Category: BEGINNER
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2727

#include<bits/stdc++.h>

using namespace std;

string a, b, c, str, str2, str3;
map<string, char> Mapa;

main()
{
	a = ".";
	b = "..";
	c = "...";
	str = a;
	str2 = b;
	str3 = c;
	Mapa[str] = 'a';
	Mapa[str2] = 'b';
	Mapa[str3] = 'c';
	for(char ch = 'd'; ch < 'd'+3*(('z'-'d'-1)/3); ch += 3)
	{
		str = str + " " + a;
		str2 = str2 + " " + b;
		str3 = str3 + " " + c;
		Mapa[str] = ch;
		Mapa[str2] = ch+1;
		Mapa[str3] = ch+2;
	}
	Mapa[str + " ."] = 'y';
	Mapa[str2 + " .."] = 'z';
	int N;
	while(scanf("%d%*c", &N) == 1)
	{
		while(N--)
		{
			getline(cin, str);
			printf("%c\n", Mapa[str]);
		}
	}
	return 0;
}
