// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: How To Train Your Dragon
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1851

#include<cstdio>
#include<queue>
#define ll unsigned long long int

using namespace std;


struct tipo
{
	int a,b;
	bool operator<(tipo const & t) const
	{
		return (a*t.b)>(t.a*b); 
	}
};

priority_queue<tipo> pq;


int main()
{
	int a,b,x,y,active=0,N=0;
	ll tot=0,tam=0;
	tipo t;
	while(scanf("%d %d",&a,&b)==2 && (a||b))
	{
		if(active == 1)
		{
			
			tot+=tam;
			x--;
			t.a=a;
			t.b=b;
			pq.push(t);
			N++;
			tam+=b;
			if(x<=0)
			{
				t=pq.top();
				pq.pop();
				N--;
				x=t.a;
				y=t.b;
				tam-=y;
			}
		}
		else
		{
			x=a;
			y=b;
			active=1;
		}
	}
	tot+=tam*x;
	while(N>0)
	{
		t=pq.top();
		pq.pop();
		tam-=t.b;
		tot+=tam*t.a;
		N--;
	}
	printf("%llu\n",tot);
	return 0;
}

