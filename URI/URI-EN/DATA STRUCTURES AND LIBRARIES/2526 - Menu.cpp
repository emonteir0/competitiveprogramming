// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Menu
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2526

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int tree[1000010];
int N;

int menor[1000010];
int maior[1000010];
int vet[1000010];

void update(int i, int val)
{
	//++i;
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	//++i;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}


main()
{
	ll ans;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 1; i <= N; i++)
			scanf("%d", &vet[i]);
		for(int i = 1; i <= N; i++)
		{
			tree[i] = 0;
			menor[i] = 0;
			maior[i] = 0;
		}
		for(int i = N; i >= 1; i--)
		{
			menor[i] = sum(vet[i]-1);
			update(vet[i], 1);
		}
		for(int i = 1; i <= N; i++)
			tree[i] = 0;
		for(int i = 1; i <= N; i++)
		{
			maior[i] = i - sum(vet[i]) - 1;
			update(vet[i], 1);
		}
		ans = 0;
		for(int i = 1; i <= N; i++)
			ans += ((ll)menor[i]) * maior[i];
		printf("%lld\n", ans);
	}
	return 0;
}
