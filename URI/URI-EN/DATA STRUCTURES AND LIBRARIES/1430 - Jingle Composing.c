// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jingle Composing
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1430

#include<stdio.h>
#include<string.h>

main()
{
	int i,n,cont,cont2;
	char vet[201];
	while((scanf("%s",vet)==1)&&(strcmp(vet,"*")!=0))
	{
		cont=0;
		cont2=0;
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			if(vet[i]=='/')
			{
				if(cont==64)	cont2++;
				cont=0;
			}
			if(vet[i]=='W')	cont+=64;
			if(vet[i]=='H') cont+=32;
			if(vet[i]=='Q') cont+=16;
			if(vet[i]=='E') cont+=8;
			if(vet[i]=='S') cont+=4;
			if(vet[i]=='T') cont+=2;
			if(vet[i]=='X') cont+=1;
		}
		printf("%d\n",cont2);
	}	
	return 0;
}
