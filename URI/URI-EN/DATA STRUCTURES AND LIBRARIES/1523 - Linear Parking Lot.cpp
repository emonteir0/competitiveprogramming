// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Linear Parking Lot
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1523

#include<bits/stdc++.h>

using namespace std;

stack<int> pilha,pilha2;


typedef struct
{
	int x;
	int i;
}tipo;

tipo vet[20000];

bool cmp(tipo a, tipo b)
{
	return a.x<b.x;
}

main()
{
	int N,K,x,y,i,j,tam;
	bool verdade;
	while(scanf("%d %d",&N,&K)==2&&(N||K))
	{
		j=0;
		tam=0;
		for(i=0;i<N;i++)
		{
			scanf("%d %d",&x,&y);
			vet[j].x=x;
			vet[j++].i=i+1;
			vet[j].x=y;
			vet[j++].i=-(i+1);
		}
		sort(vet,vet+j,cmp);
		verdade=true;
		for(i=0;i<j;i++)
		{
			if(vet[i].i>0)
			{
				pilha.push(vet[i].i);
				tam++;
				if(tam>K)
				{
					verdade=false;
					break;
				}
			}
			if(vet[i].i<0)
			{
				x=pilha.top();
				pilha.pop();
				tam--;
				if(x!=-vet[i].i)
				{
					verdade=false;
					break;
				}
			}
		}
		printf("%s\n",verdade?"Sim":"Nao");
		pilha=pilha2;
	}
	return 0;
}
