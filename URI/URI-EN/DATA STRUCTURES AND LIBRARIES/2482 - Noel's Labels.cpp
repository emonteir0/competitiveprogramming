// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Noel's Labels
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2482

#include<bits/stdc++.h>

using namespace std;

map<string, string> Mapa;

main()
{
	int N;
	char msg[101], msg2[102];
	string str;
	scanf("%d%*c", &N);
	while(N--)
	{
		gets(msg);
		gets(msg2);
		Mapa[str = msg] = msg2;
	}
	scanf("%d%*c", &N);
	N <<= 1;
	while(N--)
	{
		gets(msg);
		str = msg;
		printf("%s\n", (Mapa.find(str) != Mapa.end()) ? (Mapa[str] + "\n").c_str() : msg);
	}
	return 0;
}

