// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elementary, my Dear Watson!
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1382

#include<stdio.h>

int vet[10001],vet2[10001];

void trocar(int *a, int *b)
{
	int aux=*b;
	*b=*a;
	*a=aux;
}

main()
{
	int N,n,i,x,cont=0,aux,aux2;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&n);
		for(i=1;i<=n;i++)
		{
			scanf("%d",&x);
			vet[i]=x;
			vet2[x]=i;
		}
		cont=0;
		for(i=1;i<=n;i++)
		{
			if(vet[i]!=i)
			{
				cont++;
				aux=vet2[i];
				aux2=vet[i];
				trocar(&vet[i],&vet[vet2[i]]);
				vet2[i]=i;
				vet2[aux2]=aux;
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
