// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: GCD Grid
// Level: 8
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1511


// Segment Tree 2D

#include<bits/stdc++.h>
#define MAXN 4*2001*2001+1

using namespace std;

int tree[MAXN];
//int A[2001][2001];

/*void build(int node, int x0, int x1, int y0, int y1)
{
	if(x0 > x1 || y0 > y1)
		return;
	if(x0 == x1 && y0 == y1)
	{
		tree[node] = A[x0][y0];
		return;
	}
	int node4 = 4*node;
	int xmid = (x0+x1)/2, ymid = (y0+y1)/2;
	build(node4-2, x0, xmid, y0, ymid);
	build(node4-1, xmid+1, x1, y0, ymid);
	build(node4, x0, xmid, ymid+1, y1);
	build(node4+1, xmid+1, x1, ymid+1, y1);
	tree[node] = __gcd(tree[node4-2], __gcd(tree[node4-1], __gcd(tree[node4], tree[node4+1])));
}*/

void update(int node, int x0, int x1, int y0, int y1, int xc, int yc, int val)
{
    //Condição de parada
    if(xc < x0 || xc > x1 || yc < y0 || yc > y1)
       return;
    //Condição de atualização
    if(xc == x0 && x0 == x1 && yc == y0 && yc == y1)
    {
		tree[node] = val;
		return;
	}
    int node4 = 4*node;
    int xmid = (x0+x1)/2, ymid = (y0+y1)/2;
	update(node4-2, x0, xmid, y0, ymid, xc, yc, val);
	update(node4-1, xmid+1, x1, y0, ymid, xc, yc, val);
	update(node4, x0, xmid, ymid+1, y1, xc, yc, val);
	update(node4+1, xmid+1, x1, ymid+1, y1, xc, yc, val);
	tree[node] = __gcd(tree[node4-2], __gcd(tree[node4-1], __gcd(tree[node4], tree[node4+1])));
}

int query(int node, int x0, int x1, int y0, int y1, int xa, int xb, int ya, int yb)
{
	//Condição de parada
    if(x0 > x1 || y0 > y1) 
    	return 0;
    if(x0 > xb || x1 < xa || y0 > yb || y1 < ya)
    	return 0;
    //Condição de retorno
	if(x0 >= xa && x1 <= xb && y0 >= ya && y1 <= yb)
	{
		//printf("(%d,%d) -> (%d, %d) %d\n", x0, y0, x1, y1, tree[node]);
        return tree[node];
 	}
    int node4 = 4*node;
    int xmid = (x0+x1)/2, ymid = (y0+y1)/2;
	int p1 = query(node4-2, x0, xmid, y0, ymid, xa, xb, ya, yb);
	int p2 = query(node4-1, xmid+1, x1, y0, ymid, xa, xb, ya, yb);
	int p3 = query(node4, x0, xmid, ymid+1, y1, xa, xb, ya, yb);
	int p4 = query(node4+1, xmid+1, x1, ymid+1, y1, xa, xb, ya, yb);
	return __gcd(p1, __gcd(p2, __gcd(p3, p4)));
}

int main()
{
	int i, Q, x, y, d;
	char msg[10];
	while(scanf("%d%*c", &Q) == 1)
	{
		for(i = 0; i < MAXN; i++)
			tree[i] = 0;
		while(Q--)
		{
			scanf("%s %d %d %d%*c", msg, &x, &y, &d);
			x = x+y+1000;
			y = x-y-y;
			if(msg[0] == 'S')
				update(1, 0, 2000, 0, 2000, x, y, d);
			else
				printf("%d\n", query(1, 0, 2000, 0, 2000, x-d, x+d, y-d, y+d));
		}
	}
	return 0;
}
