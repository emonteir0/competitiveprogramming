// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Winning Strategy
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2519

#include<stdio.h>
 
int N, M;
int BIT[1001][1001];

void updateBIT(int x, int y0, int val)
{
	int y;
    for (; x <= N; x += (x & -x))
    {
    	y = y0;
        for (; y <= M; y += (y & -y))
            BIT[x][y] += val;
    }
    return;
}
 
int getSum(int x, int y0)
{
    int sum = 0, y;
    for(; x > 0; x -= (x & -x))
    {
    	y = y0;
        for(; y > 0; y -= (y & -y))
            sum += BIT[x][y];
	}
    return sum;
}
 
int main()
{
	int q, op, xa, xb, ya, yb, i, j, v;
	while(scanf("%d %d", &N, &M)==2 && (N||M))
	{
		for(i=0;i<=N;i++)
			for(j=0;j<=M;j++)
				BIT[i][j] = 0;
		for(i = 1; i <= N; i++)
			for(j = 1; j <= M; j++)
			{
				scanf("%d", &v);
				if(v)
					updateBIT(i, j, v);
			}
		scanf("%d", &q);
		while(q--)
		{
			scanf("%d %d %d %d %d", &op, &xa, &ya, &xb, &yb);
			if(op)
				printf("%d\n", getSum(xb,yb) + getSum(xa-1,ya-1) - getSum(xa-1,yb) - getSum(xb,ya-1));
			else
			{				
				updateBIT(xa, ya, -1);
				updateBIT(xb, yb, 1);
			}
		}
	}
    return 0;
}
