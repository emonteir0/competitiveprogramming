// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Elves and Their Codes
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2020

#include<cstdio>
#include<string>
#include<algorithm>

using namespace std;

string presentes[10000];
char vet[28];
int gnomo[27];

main()
{
	int i,j,k=1,n,x,z;
	char op;
	string palavra;
	vet[27]=' ';
	while(scanf("%d%*c",&n)==1)
	{
		if(k>1)
			printf("\n");
		printf("LISTA #%d:\n",k++);
		for(i=1;i<=26;i++)
		{
			vet[i]=i-1+'A';
			gnomo[i]=i;
		}
		for(j=0;j<n;j++)
		{
			op=' ';
			palavra="";
			while(op!='\n')
			{
				z=scanf("%d%c",&x,&op);
				if(x==27)
					palavra+=' ';
				else
				{
					palavra+=vet[gnomo[x]];
					for(i=gnomo[x];i<26;i++)
					{
						vet[i]+=vet[i+1];
						vet[i+1]=vet[i]-vet[i+1];
						vet[i]-=vet[i+1];
					}
					for(i=x;i<26;i++)
					{
						gnomo[i]+=gnomo[i+1];
						gnomo[i+1]=gnomo[i]-gnomo[i+1];
						gnomo[i]-=gnomo[i+1];
					}	
				}
				if(z!=2)
					break;
			}
			presentes[j]=palavra;
		}
		sort(presentes,presentes+n);
		for(i=0;i<n;i++)
			printf("%s\n",presentes[i].c_str());
	}
	return 0;
}
