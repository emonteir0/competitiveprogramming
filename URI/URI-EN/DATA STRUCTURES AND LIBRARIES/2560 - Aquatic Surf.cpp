// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Aquatic Surf
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2560

#include<stdio.h>
#define MAXN 200001
#define ll long long

int N, MA[MAXN][18], MI[MAXN][18];
int logo[MAXN], expo[18];
int A[MAXN];
ll sum[MAXN];

void logmap()
{
	int i, x = 1, k = 0;
	for(i = 1; i <= MAXN; i++)
	{
		if(i == x << 1)
		{
			k++;
			x <<= 1;
		}
		logo[i] = k;
	}
}

void expmap()
{
	int i, x = 1;
	for(i = 0; i < 18; i++)
	{
		expo[i] = x;
		x <<= 1;
	}
}

void buildmax()
{
  int i, j, sz;
  for (i = 0; i < N; i++)
      MA[i][0] = i;
  sz = logo[N];
  for (j = 1; j <= sz; j++)
      for (i = 0; i + expo[j-1] < N; i++)
          if (A[MA[i][j - 1]] > A[MA[i + expo[j-1]][j - 1]])
              MA[i][j] = MA[i][j - 1];
          else
              MA[i][j] = MA[i + expo[j-1]][j - 1];
}  
 
int rmqmax(int x, int y)
{
	int k = logo[y-x+1];
	return A[MA[x][k]] > A[MA[y-expo[k]+1][k]] ? MA[x][k] : MA[y-expo[k]+1][k];
}

void buildmin()
{
  int i, j, sz;
  for (i = 0; i < N; i++)
      MI[i][0] = i;
  sz = logo[N];
  for (j = 1; j <= sz; j++)
      for (i = 0; i + expo[j-1] < N; i++)
          if (A[MI[i][j - 1]] < A[MI[i + expo[j-1]][j - 1]])
              MI[i][j] = MI[i][j - 1];
          else
              MI[i][j] = MI[i + expo[j-1]][j - 1];
}  
 
int rmqmin(int x, int y)
{
	int k = logo[y-x+1];
	return A[MI[x][k]] < A[MI[y-expo[k]+1][k]] ? MI[x][k] : MI[y-expo[k]+1][k];
}



int main()
{
	int i, K;
	ll s;
	logmap();
	expmap();
	while(scanf("%d %d", &N, &K) == 2)
	{
		s = 0;
		for(i = 0; i < N; i++)
		{
			scanf("%d", &A[i]);
			sum[i+1] = sum[i] + A[i];
		}
		buildmin();
		buildmax();
		for(i = K; i <= N; i++)
		{
			//printf("%d %d %d %d\n", i-K, i-1, rmqmin(i-K, i-1), rmqmax(i-K, i-1));
			s += sum[i] - sum[i-K]  - A[rmqmin(i-K, i-1)] - A[rmqmax(i-K, i-1)];
		}
		printf("%lld\n", s);
	}
	return 0;
}
