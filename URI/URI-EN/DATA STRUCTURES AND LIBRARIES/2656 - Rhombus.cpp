// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rhombus
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2656

#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/tree_policy.hpp>

#define MAXN 400010

using namespace std;
using namespace __gnu_pbds;

typedef pair<int, int> ii;
typedef tree<ii, null_type, less<ii>, rb_tree_tag, 
tree_order_statistics_node_update> OST;

OST bit[MAXN];

void insert(int x, int y)
{
	for(int i = x; i < MAXN; i += i&-i)
		bit[i].insert(ii(y, x));
}

void remove(int x, int y)
{
	for(int i = x; i < MAXN; i += i&-i)
		bit[i].erase(ii(y, x));
}

int query(int x, int y)
{
	int ans = 0;
	for(int i = x; i; i -= i & -i)
		ans += bit[i].order_of_key(ii(y+1, 0));
	return ans;
}

int query(int x1, int y1, int x2, int y2)
{
	return query(x2, y2) - query(x2, y1-1) - query(x1-1, y2) +
	query(x1-1, y1-1);
}

main()
{
	int Q, op, x, y, xi, yi, xf, yf, d;
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d %d %d", &op, &x, &y);
		x = x+y+200000;
		y = x-y-y;
		if(op == 1)
			insert(x, y);
		else if(op == 2)
			remove(x, y);
		else
		{
			scanf("%d", &d);
			printf("%d\n", query(max(0, x-d), max(0, y-d), min(400000, x+d), min(400000, y+d)));
		}
	}
	return 0;
}
