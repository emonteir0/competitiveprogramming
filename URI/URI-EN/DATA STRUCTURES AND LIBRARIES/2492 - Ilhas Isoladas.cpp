// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Ilhas Isoladas
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2492

#include<bits/stdc++.h>

using namespace std;

map<string, string> MapaDominio, MapaImagem, MapaVazio;

main()
{
	int N, op;
	char nome[31], nome2[31];
	string str;
	while(scanf("%d%*c", &N) == 1 && N)
	{
		op = 0;
		while(N--)
		{
			scanf("%s -> %s%*c", nome, nome2);
			if(MapaDominio[str = nome] != "")
				op = 1;
			MapaDominio[str = nome] = nome2;
			MapaImagem[str = nome2] = nome;
		}
		if(op)
			printf("Not a function.\n");
		else
			printf("%s\n", (MapaImagem.size() == MapaDominio.size()) ? "Invertible." : "Not invertible.");
		MapaDominio = MapaImagem = MapaVazio;
	}
	return 0;
}
