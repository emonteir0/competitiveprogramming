// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Infix to Posfix
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1077

#include<bits/stdc++.h>

using namespace std;

map<char, int> prec;
stack<char> opStack;
vector<char> postfixlist;
string str;
char topToken;

int operand(char c)
{
	if(c >= 'a' && c <= 'z')
		return 1;
	if(c >= 'A' && c <= 'Z')
		return 1;
	if(c >= '0' && c <= '9')
		return 1;
	return 0;
}

main()
{
	int N;
	prec['^'] = 4;
	prec['*'] = 3;
	prec['/'] = 3;
	prec['+'] = 2;
	prec['-'] = 2;
	prec['('] = 1;
	scanf("%*d%*c");
	while(getline(cin, str))
	{
		postfixlist.clear();
		for(int i = 0; i < str.length(); i++)
		{
			if(str[i] == ' ')
				continue;
			else if(operand(str[i]))
				postfixlist.push_back(str[i]);
			else if(str[i] == '(')
				opStack.push(str[i]);
			else if(str[i] == ')')
			{
				topToken = opStack.top();
				opStack.pop();
				while(topToken != '(')
				{
					postfixlist.push_back(topToken);
					topToken = opStack.top();
					opStack.pop();
				}
			}
			else
			{
				while(!opStack.empty() && prec[opStack.top()] >= prec[str[i]])
				{
					postfixlist.push_back(opStack.top());
					opStack.pop();
				}
				opStack.push(str[i]);
			}
		}
		while(!opStack.empty())
		{
			postfixlist.push_back(opStack.top());
			opStack.pop();
		}
		for(int i = 0; i < postfixlist.size(); i++)
			printf("%c", postfixlist[i]);
		printf("\n");
	}
	return 0;
}
