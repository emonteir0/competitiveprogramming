// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hay Points
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1261

#include<bits/stdc++.h>

using namespace std;

map<string,int> Mapa;

main()
{
	int N,M,i,x,sum;
	string str;
	char vet[10001];
	scanf("%d %d%*c",&N,&M);
	for(i=0;i<N;i++)
	{
		scanf("%s %d%*c",vet,&x);
		str=vet;
		Mapa[str]=x;
	}
	for(i=0;i<M;i++)
	{
		sum=0;
		str="";
		while((gets(vet))&&(strcmp(vet,".")!=0))
		{
			str+=vet;
			str+=" ";	
		}
		stringstream ss(str);
		while(ss >> str)
				sum+=Mapa[str];
		printf("%d\n",sum);
	}
	return 0;
}
