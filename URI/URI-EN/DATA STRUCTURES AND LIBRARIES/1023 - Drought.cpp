// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Drought
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1023

#include<cstdio>
#include<cmath>
#include<map>
#include<algorithm>

using namespace std;

typedef struct
{
	int n,x;
} tipo;

tipo vet[1000000];

bool cmp(tipo a, tipo b)
{
	return a.x<b.x;
}

map<int,int> Mapa,Mapa2;
map<int,int>:: iterator it;

main()
{
	int k=1,i,N,x,y,cont,frac_part;
	double acum,int_part;
	while((scanf("%d",&N)==1)&&(N!=0))
	{
		Mapa=Mapa2;
		if(k>1)
			printf("\n");
		acum=0;
		cont=0;
		for(i=0;i<N;i++)
		{
			scanf("%d %d",&x,&y);
			vet[i].n=x;
			vet[i].x=y/x;
			Mapa[y/x]+=x;
			acum+=y;
			cont+=x;
		}
		sort(vet,vet+N,cmp);
		it=Mapa.begin();
		printf("Cidade# %d:\n%d-%d",k++,it->second,it->first);
		for(++it;it!=Mapa.end();++it)
			printf(" %d-%d",it->second,it->first);
		frac_part = (int) (modf (acum/cont, &int_part) *100);
		if(frac_part < 10) printf("\nConsumo medio: %d.0%d m3.\n", (int)int_part, (int)frac_part);
		else printf("\nConsumo medio: %d.%d m3.\n", (int)int_part, (int)frac_part);
	}
	return 0;
}

