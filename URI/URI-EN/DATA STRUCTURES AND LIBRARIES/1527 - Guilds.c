// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Guilds
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1527

#include <stdio.h>
#define maxn 200002

int N, M, p[maxn], l[maxn];
 
int id(int x)
{
	return (x != p[x] ? p[x] = id(p[x]) : p[x]);
}
 
void join(int u, int v)
{
	int aux;
    if (l[u] < l[v])
	{
		aux = u;
		u = v;
		v = aux;
	}
    p[v] = u;
	l[u] += l[v];
}
 
int main() 
{
	int i, op, u, v, rid, cont;
    while(scanf("%d %d", &N, &M) == 2 && (N||M))
    {
    	cont = 0;
	    for (i = 1; i <= N; i++)
		{
			p[i] = i;
			scanf("%d", &l[i]);
		}
	    while(M--)
	    {
	    	rid = id(1);
	        scanf("%d %d %d", &op, &u, &v);
	        u = id(u);
	        v = id(v);
	        if(op == 1 && u != v)
	        	join(u, v);
	        else if(op == 2)
	        {
	        	if(rid == u)
	        		cont += l[u] > l[v];
	        	if(rid == v)
	        		cont += l[u] < l[v];
	        }
	    }
	    printf("%d\n", cont);
	}
}
