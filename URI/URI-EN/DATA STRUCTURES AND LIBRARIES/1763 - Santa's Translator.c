// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Santa's Translator
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1763

#include<stdio.h>
#include<string.h>

main()
{
	char vet[101];
	while(scanf("%s",vet)==1)
	{
		if(strcmp(vet,"brasil")==0)				 	  printf("Feliz Natal!\n");
		else if(strcmp(vet,"alemanha")==0)            printf("Frohliche Weihnachten!\n");
		else if(strcmp(vet,"austria")==0)             printf("Frohe Weihnacht!\n");
		else if(strcmp(vet,"coreia")==0)              printf("Chuk Sung Tan!\n");
		else if(strcmp(vet,"espanha")==0)             printf("Feliz Navidad!\n");
		else if(strcmp(vet,"grecia")==0)              printf("Kala Christougena!\n");
		else if(strcmp(vet,"estados-unidos")==0)      printf("Merry Christmas!\n");
		else if(strcmp(vet,"inglaterra")==0)          printf("Merry Christmas!\n");
		else if(strcmp(vet,"australia")==0)           printf("Merry Christmas!\n");
		else if(strcmp(vet,"portugal")==0)            printf("Feliz Natal!\n");
		else if(strcmp(vet,"suecia")==0)              printf("God Jul!\n");
		else if(strcmp(vet,"turquia")==0)             printf("Mutlu Noeller\n");
		else if(strcmp(vet,"argentina")==0)           printf("Feliz Navidad!\n");
		else if(strcmp(vet,"chile")==0)               printf("Feliz Navidad!\n");
		else if(strcmp(vet,"mexico")==0)              printf("Feliz Navidad!\n");
		else if(strcmp(vet,"antardida")==0)           printf("Merry Christmas!\n");
		else if(strcmp(vet,"canada")==0)              printf("Merry Christmas!\n");
		else if(strcmp(vet,"irlanda")==0)             printf("Nollaig Shona Dhuit!\n");
		else if(strcmp(vet,"belgica")==0)             printf("Zalig Kerstfeest!\n");
		else if(strcmp(vet,"italia")==0)              printf("Buon Natale!\n");
		else if(strcmp(vet,"libia")==0)               printf("Buon Natale!\n");
		else if(strcmp(vet,"siria")==0)               printf("Milad Mubarak!\n");
		else if(strcmp(vet,"marrocos")==0)            printf("Milad Mubarak!\n");
		else if(strcmp(vet,"japao")==0)				  printf("Merii Kurisumasu!\n");
		else 										  printf("--- NOT FOUND ---\n");
	}
	return 0;
}
