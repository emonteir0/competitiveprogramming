// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Canteen Queue
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1548

#include<bits/stdc++.h>

using namespace std;

bool cmp(int a, int b)
{
	return a>b;
}

main()
{
	int n,m,i,c,vet[1001],vet2[1001];
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&m);
		c=0;
		for(i=0;i<m;i++)
		{
			scanf("%d",&vet[i]);
			vet2[i]=vet[i];
		}
		sort(vet2,vet2+m,cmp);
		for(i=0;i<m;i++)
			c+=(vet2[i]==vet[i]);
		printf("%d\n",c);
	}
	return 0;
}
