// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Square Game
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2067

#include<stdio.h>


int mat[200][200], mat2[201][201];

main()
{
	int N, M, Q, i, j, x;
	char ok;
	scanf("%d %d", &N, &M);
	for(i=0;i<N;i++)
	{
		for(j=0;j<M;j++)
		{
			scanf("%d", &x);
			mat[i][j] = x==0;
		}
	}
	for(i=0;i<=N;i++)
		mat2[i][0] = 0;
	for(i=0;i<=M;i++)
		mat2[0][i] = 0;
	for(i=1;i<=N;i++)
		for(j=1;j<=M;j++)
			mat2[i][j] = mat[i-1][j-1] + mat2[i-1][j] + mat2[i][j-1] - mat2[i-1][j-1];
	/*for(i=0;i<=N;i++)
	{
		for(j=0;j<=M;j++)
			printf("%d ", mat2[i][j]);
		printf("\n");
	}*/
	scanf("%d",&Q);
	while(Q--)
	{
		scanf("%d", &x);
		ok = 0;
		x--;
		for(i=1;i<=N-x;i++)
			for(j=1;j<=M-x;j++)
			{
				//printf("(%d, %d) -> (%d,%d): %d\n",i,j,i+x,j+x,mat2[i+x][j+x] + mat2[i-1][j-1] - mat2[i+x][j-1] - mat2[i-1][j+x]);
				ok |= mat2[i+x][j+x] + mat2[i-1][j-1] == mat2[i+x][j-1] + mat2[i-1][j+x];
			}
		printf("%s\n", ok? "yes":"no");
	}
	return 0;
}
