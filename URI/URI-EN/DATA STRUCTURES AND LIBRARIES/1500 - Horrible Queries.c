// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Horrible Queries
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1500

#include<stdio.h>
#define ll long long

int N;
ll BIT[100001], BIT2[100001];

void update(int x, ll v)
{
	for (; x <= N; x += x&(-x))
		BIT[x] += v;
}

void update2(int x, ll v)
{
	for (; x <= N; x += x&(-x))
		BIT2[x] += v;
}	 

ll query(int x)
{ 	
	ll sum = 0;
	for(; x > 0; x -= x&(-x))
    	sum += BIT[x];
	return sum;
}

ll query2(int x)
{ 	
	ll sum = 0;
	for(; x > 0; x -= x&(-x))
    	sum += BIT2[x];
	return sum;
}

main()
{
	int i, K, T, x, y, op;
	ll v;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &K);
		for(i = 0; i <= N; i++)
		{
			BIT[i] = 0;
			BIT2[i] = 0;
		}
		while(K--)
		{
			scanf("%d", &op);
			if(op)
			{
				scanf("%d %d", &x, &y);
				printf("%lld\n", query(y) * y - query2(y) - query(x-1) * (x-1) + query2(x-1));
			}
			else
			{
				scanf("%d %d %lld", &x, &y, &v);
				update(x, v); 	
  				update(y + 1, -v);
  				update2(x, v * (x-1));
  				update2(y + 1, -v * y);
			}
		}
	}
	return 0;
}
