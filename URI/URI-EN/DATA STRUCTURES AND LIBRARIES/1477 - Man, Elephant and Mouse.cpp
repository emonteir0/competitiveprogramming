// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Man, Elephant and Mouse
// Level: 8
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1477

#include<stdio.h>
#define MAX 524288

struct tipo
{
	int x, y, z;
	inline tipo operator+(tipo a) 
	{
        a.x += x;
        a.y += y;
        a.z += z;
        return a;
    }
};

tipo tree[MAX+1], p, nulo, A[100001];
int lazy[MAX+1];

void build(int node, int start, int end)
{
	lazy[node] = 0;
    if(start == end)
        tree[node] = A[start];
    else
    {
        int mid = (start + end) / 2;
        build(2*node, start, mid);
        build(2*node+1, mid+1, end);
        tree[node] = tree[2*node] + tree[2*node+1];
    }
}


tipo change(tipo x, int c)
{
	tipo y;
	switch(c%3)
	{
		case 1:
		{
			y.x = x.z;
			y.y = x.x;
			y.z = x.y;
		};break;
		case 2:
		{
			y.x = x.y;
			y.y = x.z;
			y.z = x.x;
		};break;
		default:
		{
			return x;
		}
	}
	return y;
}

void updateRange(int node, int start, int end, int l, int r)
{
    if(lazy[node] != 0)
    { 
        tree[node] = change(tree[node], lazy[node]);
        if(start != end)
        {
            lazy[node*2] += lazy[node];
            lazy[node*2+1] += lazy[node];
        }
        lazy[node] = 0;
    }
    if(start > end || start > r || end < l)
        return;
    if(start >= l && end <= r)
    {
        tree[node] = change(tree[node], 1);
        if(start != end)
        {
            lazy[node*2]++;
            lazy[node*2+1]++;
        }
        return;
    }
    int mid = (start + end) / 2;
    updateRange(node*2, start, mid, l, r);
    updateRange(node*2 + 1, mid + 1, end, l, r);
    tree[node] = tree[node*2] + tree[node*2+1];
}

tipo queryRange(int node, int start, int end, int l, int r)
{
    if(start > end || start > r || end < l)
        return nulo;
    if(lazy[node] != 0)
    {
        tree[node] = change(tree[node], lazy[node]);
        if(start != end)
        {
            lazy[node*2] += lazy[node];
            lazy[node*2+1] += lazy[node];
        }
        lazy[node] = 0;
    }
    if(start >= l && end <= r)
        return tree[node];
    int mid = (start + end) / 2;
	return queryRange(node*2, start, mid, l, r) + queryRange(node*2 + 1, mid + 1, end, l, r);
}

main()
{
	int T, N, M, i, a, b;
	char op;
	while(scanf("%d %d%*c", &N, &M)==2)
	{
		for(i = 1; i <= N; i++)
		{
			A[i].x = 1;
			A[i].y = A[i].z = 0;
		}
		build(1, 1, N);
		while(M--)
		{
			scanf("%d", &op);
			scanf("%c %d %d", &op, &a, &b);
			if(op == 'C')
			{
				p = queryRange(1, 1, N, a, b);
				printf("%d %d %d\n", p.x, p.y, p.z);
			}
			else
				updateRange(1, 1, N, a, b);
		}
		printf("\n");
	}
	return 0;
}

