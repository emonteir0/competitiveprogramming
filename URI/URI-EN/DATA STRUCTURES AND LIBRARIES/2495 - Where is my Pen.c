// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Where is my Pen?
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2495

#include<stdio.h>

int vet[100001];

main()
{
	int i, N, x;
	while(scanf("%d", &N) == 1)
	{
		for(i = 1; i <= N; i++)
			vet[i] = 0;
		for(i = 1; i < N; i++)
		{
			scanf("%d", &x);
			vet[x] = 1;
		}
		for(i = 1; i <= N; i++)
			if(vet[i] == 0)
				break;
		printf("%d\n", i);
	}
	return 0;
}
