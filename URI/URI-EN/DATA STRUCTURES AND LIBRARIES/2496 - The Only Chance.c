// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Only Chance
// Level: 1
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2496

#include<stdio.h>

main()
{
	int i, N, M, cont;
	char msg[27];
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%d%*c", &M);
		scanf("%s", msg);
		cont = 0;
		
		for(i = 0; i < M; i++)
			cont += msg[i] != (i + 'A');
		printf("%s\n", (cont <= 2) ? "There are the chance." : "There aren't the chance.");
	}
}
