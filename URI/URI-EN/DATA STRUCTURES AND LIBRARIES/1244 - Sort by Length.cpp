// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sort by Length
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1244

#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

typedef struct a{
	char word[51];
}tipo;

bool cmp(tipo a,tipo b){
	return (strlen(a.word)>strlen(b.word));
}



int main(){
	tipo vet[50];
	char c;
	int N,i,j;
	scanf("%d%*c",&N);
	while(N--)
	{
		j=0;
		i=0;
		while(scanf("%c",&c)==1&&c!='\n')
		{
			if(c==' ')
			{
				vet[j++].word[i]=0;
				i=0;
			}
			else
				vet[j].word[i++]=c;
		}
		vet[j].word[i]=0;
		stable_sort(vet,vet+j+1,cmp);
		printf("%s",vet[0].word);
		for(i=1;i<=j;i++)
			printf(" %s",vet[i].word);
		printf("\n");
	}
	return 0;
}

