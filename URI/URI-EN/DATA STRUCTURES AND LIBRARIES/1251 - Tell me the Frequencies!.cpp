// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tell me the Frequencies!
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1251

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int a, b;
} tipo;

bool cmp(tipo a, tipo b)
{
	if(a.b!=b.b)
		return (a.b<b.b);
	return a.a>b.a;
}

vector<tipo> v1,v2;
map<int,int> Mapa1,Mapa2;
map<int,int>::iterator it;
vector<tipo>::iterator it2;

main()
{
	int i,n,k=0;
	tipo x;
	char str[1001];
	while(gets(str)!=NULL)
	{
		if(k!=0)	printf("\n");
		else	k=1;
		v1=v2;
		Mapa1=Mapa2;
		n=strlen(str);
		for(i=0;i<n;i++)
			Mapa1[str[i]]++;
		for(it=Mapa1.begin();it!=Mapa1.end();++it)
		{
			x.a=it->first;
			x.b=it->second;
			v1.push_back(x);
		}
		sort(v1.begin(),v1.end(),cmp);
		for(it2=v1.begin();it2!=v1.end();++it2)
			printf("%d %d\n",it2->a,it2->b);
	}
	return 0;
}
