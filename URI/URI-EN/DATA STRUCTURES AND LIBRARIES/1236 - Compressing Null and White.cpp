// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Compressing Null and White
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1236

#include<bits/stdc++.h>

using namespace std;

string str;
char saida[2001];
int k;

void check(int &ca, int &cb)
{
	if(ca != 0)
	{
		if(ca <= 2)
		{
			for(int i = 0; i < ca; i++)
				saida[k++] = '0';
		}
		else
		{
			while(ca)
			{
				if(ca > 255)
				{
					saida[k++] = '#';
					saida[k++] = 255;
					ca -= 255;
				}
				else
				{
					saida[k++] = '#';
					saida[k++] = ca;
					ca = 0;
				}
			}
		}
		ca = 0;
	}
	if(cb != 0)
	{
		if(cb <= 2)
		{
			for(int i = 0; i < cb; i++)
				saida[k++] = ' ';
		}
		else
		{
			while(cb)
			{
				if(cb > 255)
				{
					saida[k++] = '$';
					saida[k++] = 255;
					cb -= 255;
				}
				else
				{
					saida[k++] = '$';
					saida[k++] = cb;
					cb = 0;
				}
			}
		}
		cb = 0;
	}
}

main()
{
	int N, ca, cb;
	scanf("%d%*c", &N);
    while(N--)
	{
        getline(cin, str);    
		k = 0;
		ca = 0;
		cb = 0;
		for(int i = 0; i < str.length(); i++)
		{
			if(str[i] != '0' && str[i] != ' ')
			{
				check(ca, cb);
				saida[k++] = str[i];
			}
			else if(str[i] == '0')
			{
				if(cb != 0)
					check(ca, cb);
				ca++;
			}
			else if(str[i] == ' ')
			{
				if(ca != 0)
					check(ca, cb);
				cb++;
			}
		}
		check(ca, cb);
		saida[k] = 0;
		printf("%s\n", saida);
	}
	return 0;
}
