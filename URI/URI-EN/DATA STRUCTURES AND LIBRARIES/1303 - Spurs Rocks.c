// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Spurs Rocks
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1303

#include<stdio.h>
#include<stdlib.h>

typedef struct a
{
	int ptog,ptol,pontos,ind;
}tipo;

int cmp(const void* a,const void* b)
{
	tipo* struc1=(tipo*)a;
	tipo* struc2=(tipo*)b;
	if(struc1->ptol==0) struc1->ptol=1;
	if(struc2->ptol==0) struc2->ptol=1;
	int x=struc2->pontos-struc1->pontos;
	int y=struc2->ptog*struc1->ptol-struc2->ptol*struc1->ptog;
	int z=struc2->ptog-struc1->ptog;
	int w=struc1->ind-struc2->ind;
	if(x!=0) return x;
	if(y!=0) return y;
	if(z!=0) return z;
	return w;
}

main()
{
	int N,i,a,b,c,d,h=0;
	char cont=0;
	tipo vet[100];
	while(scanf("%d",&N)&&N)
	{	
		for(i=0;i<N;i++)
		{
			vet[i].ind=i+1;
			vet[i].pontos=0;
			vet[i].ptog=0;
			vet[i].ptol=0;
		}
		for(i=0;i<N*(N-1)/2;i++)
		{
			scanf("%d %d %d %d",&a,&b,&c,&d);
			vet[a-1].ptog+=b;
			vet[a-1].ptol+=d;
			vet[c-1].ptog+=d;
			vet[c-1].ptol+=b;
			if(b>d)
			{
				vet[a-1].pontos+=2;
				vet[c-1].pontos+=1;
			}
			else
			{
				vet[c-1].pontos+=2;
				vet[a-1].pontos+=1;
			}
		}
		qsort(vet,N,sizeof(vet[0]),cmp);
		if(cont==1)
			printf("\n");
		else
			cont=1;
		printf("Instancia %d\n%d",++h,vet[0].ind);
		for(i=1;i<N;i++)	printf(" %d",vet[i].ind);
		printf("\n");
	}
	return 0;
}
