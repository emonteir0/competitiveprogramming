// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: SpeedWagon Flow
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2857

#include<bits/stdc++.h>
#define ll long long
using namespace std;

int N;
ll tree[100001];

void update(int i, int val)
{
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}


int query(pair<int, int> A, pair<int, int> B)
{
	if(A.first > B.first)
		swap(A, B);
	if(A.second >= B.second)
		return sum(A.second)-sum(A.first-1);
	if(A.second < B.first)
		return sum(A.second)-sum(A.first-1) + sum(B.second)-sum(B.first-1);
	return sum(B.second) - sum(A.first-1);
}

main()
{
	int M, op, A, B, C, D;
	scanf("%d %d", &N, &M);
	while(M--)
	{
		scanf("%d", &op);
		if(op == 1)
		{
			scanf("%d %d", &A, &B);
			update(A, B);
		}
		else
		{
			scanf("%d %d %d %d", &A, &B, &C, &D);
			printf("%d\n", query({A, B}, {C, D}));
		}
	}
	return 0;
}
