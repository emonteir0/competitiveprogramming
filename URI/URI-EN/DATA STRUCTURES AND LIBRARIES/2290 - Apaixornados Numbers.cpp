// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Apaixornados Numbers
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2290

#include<bits/stdc++.h>
#define ll long long

using namespace std;

ll vet[100001];

main()
{
	int N, i;
	ll xant;
	char c;
	while(scanf("%d", &N) == 1 && N)
	{
		for(i = 0; i < N; i++)
			scanf("%lld", &vet[i]);
		sort(vet, vet+N);
		xant = vet[0];
		c = 1;
		for(i = 1; i < N; i++)
		{
			if(vet[i] == xant)
				c = !c;
			else
			{
				if(c)
				{
					printf("%lld", xant);
					xant = vet[i];
					break;
				}
				c = 1;
			}
			xant = vet[i];
		}
		for(i++; i < N; i++)
		{
			if(vet[i] == xant)
				c = !c;
			else
			{
				if(c)
				{
					printf(" %lld", xant);
					c = 0;
					break;
				}
				c = 1;
			}
			xant = vet[i];
		}
		if(c)
			printf(" %lld", xant);
		printf("\n");
	}
	return 0;
}

