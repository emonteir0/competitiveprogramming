// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shuffled Deck
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1709

#include<stdio.h>

main()
{
	int i=2,n,cont=1;
	scanf("%d",&n);
	while(i!=1)
	{
		if (2*i>n)	i=(i<<1)-1-n;
		else i=i<<1;
		cont++;
	}
	printf("%d\n",cont);
	return 0;
}
