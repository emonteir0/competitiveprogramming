// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Cards Distribution
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2821

#include<bits/stdc++.h>

using namespace std;

int t, N, K, mid, me;
int ac[61];
int vet[61];
int pd[11][61];
int vis[11][61];
pair<int, int> pd2[11][61][2];
int vis2[11][61][2];

int solve(int x, int y)
{
	if(x == K)
		return y == N;
	if(vis[x][y] == t)
		return pd[x][y];
	vis[x][y] = t;
	int r = 0;
	for(int i = y+1; i <= N; i++)
		if(ac[i]-ac[y] <= mid)
			r |= solve(x+1, i);
		else
			break;
	return pd[x][y] = r;
}

pair<int, int> solve2(int x, int y, int b)
{
	pair<int, int> p;
	if(x == K)
		return make_pair(0, (y == N) && b);
	if(vis2[x][y][b])
		return pd2[x][y][b];
	vis2[x][y][b] = 1;
	int r = 0;
	for(int i = N; i >= y+1; i--)
		if(ac[i]-ac[y] <= me)
		{
			p = solve2(x+1, i, b || ((ac[i]-ac[y]) == me));
			if(p.second)
			{
				r = max(p.first, i-y);
				break;
			}
		}
	//printf("%d %d %d -> %d\n", x, y, b, r);
	return pd2[x][y][b] = make_pair(r, r != 0);
}

int busca(int s)
{
	int lo = 1, hi = s, ans = s;
	while(lo <= hi)
	{
		mid = (lo+hi)/2;
		t++;
		if(solve(0, 0))
		{
			ans = mid;
			hi = mid-1;
		}
		else
			lo = mid+1;
	}
	return ans;
}

char str[5];

int val()
{
	if(strlen(str) == 2)					return 10;
	if((str[0] >= '0') && (str[0] <= '9'))	return str[0]-'0';
	if(str[0] == 'A') 						return 1;
	if(str[0] == 'J')						return 11;
	if(str[0] == 'Q')						return 12;
	if(str[0] == 'K')						return 13;
}

main()
{
	int s = 0;
	scanf("%d %d", &N, &K);
	for(int i = 1; i <= N; i++)
		scanf("%s", str), vet[i] = val(), ac[i] = ac[i-1] + vet[i];
	me = busca(ac[N]);
	printf("%d %d\n", solve2(0, 0, 0).first, me);
	return 0;
}
