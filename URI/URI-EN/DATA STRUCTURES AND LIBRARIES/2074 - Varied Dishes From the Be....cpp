// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Varied Dishes From the Be...
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2074

#include<bits/stdc++.h>
#define llu unsigned long long

using namespace std;

int N, K;
int volta[64], pot[2][6];
string numeros[4][64];

void soma(string& v1, string v2)
{
	int i, d, c = 0;
	int l1 = v1.size();
	int l2 = v2.size();
	for(i = l1; i < l2; i++) v1 += '0';
	for(i = l2; i < l1; i++) v2 += '0';
	for(i = 0; i < l1 || i < l2; i++)
	{
		d = (v1[i] - '0') + (v2[i] - '0') + c;
		c = d/10;
		d %= 10;
		v1[i] = '0' + d;
	}
	while(c)
	{
		if(i >= (int)v1.size())	v1 += '0' + (c % 10);
		else 					v1[i] = '0' + (c % 10);
		c /= 10, i++;
	}
}

struct uint256_t
{
	llu v[4];
	uint256_t() {}
	uint256_t operator+(uint256_t s2) const
	{
		int c = 0;
		uint256_t x;
		for(int i = 0; i < 4; i++)
		{
			x.v[i] = v[i] + s2.v[i] + c;
			c = (x.v[i] < max(v[i], s2.v[i]));
		}
		return x;
	}
	bool operator!=(uint256_t s2) const
	{
		return !(v[0] == s2.v[0] && v[1] == s2.v[1] && v[2] == s2.v[2] && v[3] == s2.v[3]);
	}
	string _256str()
	{
		string r = "0";
		for(int i = 0; i < 4; i++)
		{
			llu x = 1;
			for(int j = 0; j < 64; j++, x <<= 1)
			{
				if(x & v[i])
					soma(r, numeros[i][j]);
			}
		}
		reverse(r.begin(), r.end());
		return r;
	}
};


uint256_t memo[101][64][2][64];
uint256_t zero, um, inf;

uint256_t solve(int n, int anterior, int atual, int proximo)
{
	if(n == N)
		return um;
	if((memo[n][anterior][atual][proximo] != inf))
		return memo[n][anterior][atual][proximo];
	uint256_t r;
	if(anterior&1)
		r = solve(n+1, volta[anterior] | pot[atual][K-1], proximo&1, volta[proximo] | pot[((n+K) < N-1)][K-1]);
	else
	{
		r = zero;
		if(atual)
			r = r + solve(n+1, volta[anterior], proximo&1, volta[proximo] |  pot[(n+K) < N-1][K-1]);
		if(anterior)
			for(int i = 2; i <= anterior; i <<= 1)
				if(anterior & i)
					r = r + solve(n+1, volta[anterior-i] | pot[atual][K-1], proximo&1, volta[proximo] |  pot[(n+K) < N-1][K-1]);
		if(proximo)
			for(int i = 1; i <= proximo; i <<= 1)
				if(proximo & i)
					r = r + solve(n+1, volta[anterior] | pot[atual][K-1], (proximo-i)&1, volta[proximo-i] |  pot[(n+K) < N-1][K-1]);
	}
	memo[n][anterior][atual][proximo] = r;
	return memo[n][anterior][atual][proximo];
}

int fat(int n)
{
	if(n)
		return n*fat(n-1);
	return 1;
}

main()
{
	int T, lim;
	for(int i = 0; i < 64; i++)
		volta[i] = i/2;
	pot[1][0] = 1;
	for(int i = 1; i <= 6; i++)
		pot[1][i] = pot[1][i-1] << 1;
	for(int i = 0; i < 4; i++)
	{
		zero.v[i] = 0;
		um.v[i] = 0;
		inf.v[i] = 255;
	}
	um.v[0] = 1;
	numeros[0][0] = "1";
	for(int i = 1; i < 64; i++)
	{
		numeros[0][i] = "0";
		soma(numeros[0][i], numeros[0][i-1]);
		soma(numeros[0][i], numeros[0][i-1]);
	}
	for (int i = 1; i < 4; i++)
	{
		numeros[i][0] = "0"; 
		soma(numeros[i][0], numeros[i-1][63]);
		soma(numeros[i][0], numeros[i-1][63]);
		for(int j = 1; j < 64; j++)
		{
			numeros[i][j] = "0";
			soma(numeros[i][j], numeros[i][j-1]);
			soma(numeros[i][j], numeros[i][j-1]);
		}
	}
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &N, &K);
		if(K >= N)
			printf("%d\n", fat(N));
		else
		{
			lim = 1<<K;
			for(int i = 0; i <= N; i++)
				for(int j = 0; j < lim; j++)
					for(int k = 0; k < lim; k++)
					{
						memo[i][j][0][k] = inf;
						memo[i][j][1][k] = inf;
					}
			uint256_t ans = solve(0, 0, 1, lim - 1);
			printf("%s\n", ans._256str().c_str());
		}
	}
	return 0;
}
