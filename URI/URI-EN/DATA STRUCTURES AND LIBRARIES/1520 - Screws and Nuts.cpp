// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Screws and Nuts
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1520

#include<bits/stdc++.h>

using namespace std;

int tam;
int vet[10001];

main()
{
	int i, N, x, y;
	while(scanf("%d", &N) == 1)
	{
		tam = 0;
		while(N--)
		{
			scanf("%d %d", &x, &y);
			for(i = x; i <= y; i++)
				vet[tam++] = i;
		}
		sort(vet, vet+tam);
		scanf("%d", &x);
		if(!binary_search(vet, vet+tam, x))
			printf("%d not found\n", x);
		else
			printf("%d found from %d to %d\n", x, lower_bound(vet, vet+tam, x)-vet, upper_bound(vet, vet+tam, x)-vet-1);
	}
	return 0;
}
