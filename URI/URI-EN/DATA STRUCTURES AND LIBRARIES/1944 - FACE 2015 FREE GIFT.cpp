// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: FACE 2015 FREE GIFT
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1944

#include<bits/stdc++.h>

using namespace std;

set<string> conjunto;

main()
{
	int N,n,cont=0;
	char a,b,c,d;
	string str,str2,strtotal;
	scanf("%d%*c",&N);
	strtotal="FACE";
	while(N--)
	{
		scanf("%c%*c%c%*c%c%*c%c%*c",&a,&b,&c,&d);
		str=a;
		str+=b;
		str+=c;
		str+=d;
		str2=str;
		reverse(str2.begin(),str2.end());
		n=strtotal.length();
		if(strtotal.substr(n-4,4)==str2)
		{
			cont++;
			strtotal.erase(n-4,4);
			if(strtotal.length()==0)
				strtotal="FACE";
		}
		else
			strtotal+=str;
	}
	printf("%d\n",cont);
	return 0;
}
