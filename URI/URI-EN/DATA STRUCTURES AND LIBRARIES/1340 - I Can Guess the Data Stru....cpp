// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: I Can Guess the Data Stru...
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1340

#include<cstdio>
#include<queue>
#include<stack>

using namespace std;

priority_queue<int> filap,filap2;
queue<int> fila,fila2;
stack<int> pilha,pilha2;

int main()
{
	int N,op,size,x,y;
	bool fp,f,p;
	while(scanf("%d",&N)==1)
	{
		fp=true;
		f=true;
		p=true;
		size=0;
		while(N--)
		{
			scanf("%d %d",&op,&x);
			if(op==1)
				size++;
			else
				size--;
			if(size<0)
			{
				fp=false;
				f=false;
				p=false;
			}
			if(fp||f||p)
			{
				if(op==1)
				{
					pilha.push(x);
					fila.push(x);
					filap.push(x);
				}
				else
				{
					y=pilha.top();
					if(x!=y)
						p=false;
					y=fila.front();
					if(x!=y)
						f=false;
					y=filap.top();
					if(x!=y)
						fp=false;
					pilha.pop();
					fila.pop();
					filap.pop();
				}
			}
		}
		if((!fp)*(!f)*(!p))
			printf("impossible\n");
		else if(fp*(!f)*(!p))
			printf("priority queue\n");
		else if((!fp)*f*(!p))
			printf("queue\n");
		else if((!fp)*(!f)*p)
			printf("stack\n");
		else
			printf("not sure\n");
		pilha=pilha2;
		fila=fila2;
		filap=filap2;
	}
	return 0;
}

