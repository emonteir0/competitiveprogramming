// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Summer Camp
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1167

#include<stdio.h>

typedef struct
{
	int v,ant,prox;
	char nome[31];
} pessoa;

pessoa vet[100];

main()
{
	int n,i,j,f,g;
	while(scanf("%d%*c",&n)&&(n!=0))
	{
		for(i=0;i<n;i++)
		{
			scanf("%s %d%*c",vet[i].nome,&vet[i].v);
			vet[i].ant=(i-1+n)%n;
			vet[i].prox=(i+1)%n;
		}
		i=0;
		g=n;
		while(g!=1)
		{
			f=vet[i].v;
			if(f%2==0)
			{
				f%=g;
				if(f==0&&(n!=g))
					i=vet[i].prox;
				for(j=0;j<f;j++)
					i=vet[i].ant;
			}
			else
			{
				f%=g;
				if(f==0&&(n!=g))
					i=vet[i].ant;
				for(j=0;j<f;j++)
					i=vet[i].prox;
			}
			vet[vet[i].ant].prox=vet[i].prox;
			vet[vet[i].prox].ant=vet[i].ant;
			g--;
		}
		printf("Vencedor(a): %s\n",vet[vet[i].prox].nome);
	}
	return 0;
}
