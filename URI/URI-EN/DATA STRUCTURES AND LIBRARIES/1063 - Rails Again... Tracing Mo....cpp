// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rails Again... Tracing Mo...
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1063

#include<bits/stdc++.h>

using namespace std;

char vet[1000001], vet2[1000001];

main()
{
	int N;
	stack<char> pilha;
	while(cin >> N && N)
	{
		pilha = stack<char>();
		for(int i = 0; i < N; i++)
			cin >> vet[i];
		for(int i = 0; i < N; i++)
			cin >> vet2[i];
		int k = 0;
		for(int i = 0; i < N; i++)
		{
			pilha.push(vet[i]);
			printf("I");
			while(!pilha.empty() && pilha.top() == vet2[k])
			{
				printf("R");
				pilha.pop();
				k++;
			}
		}
		if(!pilha.empty())
			printf(" Impossible\n");
		else
			printf("\n");
	}
	return 0;
}
