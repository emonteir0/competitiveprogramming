// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Greedy
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1594

#include<bits/stdc++.h>
#define ll long long

using namespace std;

int vet[1000001], N, K;

ll slidingwindow()
{
    deque<int>  Qi(K);
 	int i;
 	ll s = 0;
    for (i = 0; i < K; i++)
    {
        while ( (!Qi.empty()) && vet[i] >= vet[Qi.back()])
            Qi.pop_back();
	    Qi.push_back(i);
    }
 	for ( ; i < N; i++)
    {
        s += vet[Qi.front()];
        while ( (!Qi.empty()) && Qi.front() <= i - K)
            Qi.pop_front();
        while ( (!Qi.empty()) && vet[i] >= vet[Qi.back()])
            Qi.pop_back();
        Qi.push_back(i);
    }
    return s + vet[Qi.front()];
}

int main()
{
	int i, S, T;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d %d", &N, &K, &S);
		vet[0] = S;
		for (i = 1; i < N; ++i)
    		vet[i] = (1LL*vet[i-1]*1103515245 + 12345) % (2147483648LL);
    	printf("%lld\n", slidingwindow());
	}
	return 0;
}
