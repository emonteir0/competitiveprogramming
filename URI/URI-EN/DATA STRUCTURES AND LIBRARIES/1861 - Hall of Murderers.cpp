// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hall of Murderers
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1861

#include<bits/stdc++.h>

using namespace std;

typedef struct
{
	int kills,killed;
}perfil;

map<string,perfil> Mapa;
map<string,perfil>::iterator it;

perfil b;

main()
{
	char vet[11],vet2[11];
	string a;
	while(scanf("%s %s",vet,vet2)==2)
	{
		a=vet;
		Mapa[a].kills++;
		a=vet2;
		Mapa[a].killed=1;
	}
	printf("HALL OF MURDERERS\n");
	for(it=Mapa.begin();it!=Mapa.end();++it)
		if((it->second).killed==0)	
			printf("%s %d\n",(it->first).c_str(),(it->second).kills);
	return 0;
}
