// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Throwing Cards Away
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1110

#include<stdio.h>
main(){
int x;
while(scanf("%d",&x)==1&&x)
{
switch(x)
{
case 1: printf("Discarded cards:\nRemaining card: 1\n");break;
case 2: printf("Discarded cards: 1\nRemaining card: 2\n");break;
case 3: printf("Discarded cards: 1, 3\nRemaining card: 2\n");break;
case 4: printf("Discarded cards: 1, 3, 2\nRemaining card: 4\n");break;
case 5: printf("Discarded cards: 1, 3, 5, 4\nRemaining card: 2\n");break;
case 6: printf("Discarded cards: 1, 3, 5, 2, 6\nRemaining card: 4\n");break;
case 7: printf("Discarded cards: 1, 3, 5, 7, 4, 2\nRemaining card: 6\n");break;
case 8: printf("Discarded cards: 1, 3, 5, 7, 2, 6, 4\nRemaining card: 8\n");break;
case 9: printf("Discarded cards: 1, 3, 5, 7, 9, 4, 8, 6\nRemaining card: 2\n");break;
case 10: printf("Discarded cards: 1, 3, 5, 7, 9, 2, 6, 10, 8\nRemaining card: 4\n");break;
case 11: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 4, 8, 2, 10\nRemaining card: 6\n");break;
case 12: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 2, 6, 10, 4, 12\nRemaining card: 8\n");break;
case 13: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 4, 8, 12, 6, 2\nRemaining card: 10\n");break;
case 14: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 2, 6, 10, 14, 8, 4\nRemaining card: 12\n");break;
case 15: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 4, 8, 12, 2, 10, 6\nRemaining card: 14\n");break;
case 16: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 2, 6, 10, 14, 4, 12, 8\nRemaining card: 16\n");break;
case 17: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 4, 8, 12, 16, 6, 14, 10\nRemaining card: 2\n");break;
case 18: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 2, 6, 10, 14, 18, 8, 16, 12\nRemaining card: 4\n");break;
case 19: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 4, 8, 12, 16, 2, 10, 18, 14\nRemaining card: 6\n");break;
case 20: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 2, 6, 10, 14, 18, 4, 12, 20, 16\nRemaining card: 8\n");break;
case 21: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 4, 8, 12, 16, 20, 6, 14, 2, 18\nRemaining card: 10\n");break;
case 22: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 2, 6, 10, 14, 18, 22, 8, 16, 4, 20\nRemaining card: 12\n");break;
case 23: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 4, 8, 12, 16, 20, 2, 10, 18, 6, 22\nRemaining card: 14\n");break;
case 24: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 2, 6, 10, 14, 18, 22, 4, 12, 20, 8, 24\nRemaining card: 16\n");break;
case 25: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 4, 8, 12, 16, 20, 24, 6, 14, 22, 10, 2\nRemaining card: 18\n");break;
case 26: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 2, 6, 10, 14, 18, 22, 26, 8, 16, 24, 12, 4\nRemaining card: 20\n");break;
case 27: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 4, 8, 12, 16, 20, 24, 2, 10, 18, 26, 14, 6\nRemaining card: 22\n");break;
case 28: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 2, 6, 10, 14, 18, 22, 26, 4, 12, 20, 28, 16, 8\nRemaining card: 24\n");break;
case 29: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 4, 8, 12, 16, 20, 24, 28, 6, 14, 22, 2, 18, 10\nRemaining card: 26\n");break;
case 30: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 2, 6, 10, 14, 18, 22, 26, 30, 8, 16, 24, 4, 20, 12\nRemaining card: 28\n");break;
case 31: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 4, 8, 12, 16, 20, 24, 28, 2, 10, 18, 26, 6, 22, 14\nRemaining card: 30\n");break;
case 32: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 2, 6, 10, 14, 18, 22, 26, 30, 4, 12, 20, 28, 8, 24, 16\nRemaining card: 32\n");break;
case 33: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 4, 8, 12, 16, 20, 24, 28, 32, 6, 14, 22, 30, 10, 26, 18\nRemaining card: 2\n");break;
case 34: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 2, 6, 10, 14, 18, 22, 26, 30, 34, 8, 16, 24, 32, 12, 28, 20\nRemaining card: 4\n");break;
case 35: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 4, 8, 12, 16, 20, 24, 28, 32, 2, 10, 18, 26, 34, 14, 30, 22\nRemaining card: 6\n");break;
case 36: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 2, 6, 10, 14, 18, 22, 26, 30, 34, 4, 12, 20, 28, 36, 16, 32, 24\nRemaining card: 8\n");break;
case 37: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 4, 8, 12, 16, 20, 24, 28, 32, 36, 6, 14, 22, 30, 2, 18, 34, 26\nRemaining card: 10\n");break;
case 38: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 8, 16, 24, 32, 4, 20, 36, 28\nRemaining card: 12\n");break;
case 39: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 4, 8, 12, 16, 20, 24, 28, 32, 36, 2, 10, 18, 26, 34, 6, 22, 38, 30\nRemaining card: 14\n");break;
case 40: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 4, 12, 20, 28, 36, 8, 24, 40, 32\nRemaining card: 16\n");break;
case 41: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 6, 14, 22, 30, 38, 10, 26, 2, 34\nRemaining card: 18\n");break;
case 42: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 8, 16, 24, 32, 40, 12, 28, 4, 36\nRemaining card: 20\n");break;
case 43: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 2, 10, 18, 26, 34, 42, 14, 30, 6, 38\nRemaining card: 22\n");break;
case 44: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 4, 12, 20, 28, 36, 44, 16, 32, 8, 40\nRemaining card: 24\n");break;
case 45: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 6, 14, 22, 30, 38, 2, 18, 34, 10, 42\nRemaining card: 26\n");break;
case 46: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 8, 16, 24, 32, 40, 4, 20, 36, 12, 44\nRemaining card: 28\n");break;
case 47: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 2, 10, 18, 26, 34, 42, 6, 22, 38, 14, 46\nRemaining card: 30\n");break;
case 48: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 4, 12, 20, 28, 36, 44, 8, 24, 40, 16, 48\nRemaining card: 32\n");break;
case 49: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 6, 14, 22, 30, 38, 46, 10, 26, 42, 18, 2\nRemaining card: 34\n");break;
case 50: printf("Discarded cards: 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 8, 16, 24, 32, 40, 48, 12, 28, 44, 20, 4\nRemaining card: 36\n");break;
}
}
return 0;
}
