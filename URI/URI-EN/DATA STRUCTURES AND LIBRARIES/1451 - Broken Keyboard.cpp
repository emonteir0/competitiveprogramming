// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Broken Keyboard
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1451

#include<bits/stdc++.h>

using namespace std;

main()
{
	string str,str2;
	char vet[100001];
	int i,n;
	while(gets(vet)!=NULL)
	{
		n=strlen(vet);
		str="";
		str2="";
		for(i=0;i<n;i++)
		{
			if(vet[i]!=']'&&vet[i]!='[')
				str2+=vet[i];
			else if(vet[i]=='[')
			{
				str+=str2;
				str2="";
				for(;i<n;i++)
				{
					if(vet[i]!=']'&&vet[i]!='[')
						str2+=vet[i];
					else if(vet[i]==']')
						break;
					else
					{
						str=str2+str;
						str2="";
					}
				}
				str=str2+str;
				str2="";
			}
		}
		str=str+str2;
		printf("%s\n",str.c_str());
	}
	return 0;
}

