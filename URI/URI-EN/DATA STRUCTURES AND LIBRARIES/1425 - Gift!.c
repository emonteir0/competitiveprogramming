// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Gift?!
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1425

#include<stdio.h>

int mat[101][101];


void solve(int ini, int maximo, int j)
{
	mat[maximo][ini] = 1;
	if(ini + j <= maximo)
		solve(ini+j, maximo, j+2);
	if(ini - j >= 1)
		solve(ini-j, maximo, j+2);
}

main()
{
	int i, N, M;
	for(i = 1; i <= 48; i++)
		solve(1, i, 3);
	while(scanf("%d %d", &N, &M) == 2 && (N||M))
	{
		if(N > 48)
			printf("Let me try!\n");
		else
			printf("%s\n", mat[N][M]? "Let me try!" : "Don't make fun of me!");
	}
	return 0;
}

