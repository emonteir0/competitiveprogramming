// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Dark Elf
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1766

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
 
typedef struct ren{
    char nome[101];
    int peso, idade;
    double altura;
}rena;
 
rena vet[1000];

int sig(double x)
{
	if(x<0)
		return -1;
	else
		return 1;
}

int cmp(const void* a, const void* b)
{
	rena* struc1=(rena*)a;
	rena* struc2=(rena*)b;
	int dn=strcmp(struc1->nome,struc2->nome);
	int dp=struc2->peso-struc1->peso;
	int di=struc1->idade-struc2->idade;
	double da=struc1->altura-struc2->altura;
	if(dp!=0)	return dp;
	if(di!=0)	return di;
	if(da!=0)	return sig(da);
	return dn;
} 
 
main()
{
    int T,N,M,i,k=0;
    scanf("%d",&T);
    while(T--)
    {
    	scanf("%d %d",&N,&M);
    	for(i=0;i<N;i++)
    		scanf("%s %d %d %lf%*c",vet[i].nome,&vet[i].peso,&vet[i].idade,&vet[i].altura);
    	qsort(vet,N,sizeof(vet[0]),cmp);
    	printf("CENARIO {%d}\n",++k);
    	for(i=0;i<M;i++)
    		printf("%d - %s\n",i+1,vet[i].nome);
    }
    return 0;
}
