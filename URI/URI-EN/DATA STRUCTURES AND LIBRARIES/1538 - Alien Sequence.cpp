// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alien Sequence
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1538

#include<bits/stdc++.h>
#define ll long long
using namespace std;

string str;

main()
{
	int N;
	ll N2;
	while(cin >> N && N)
	{
		str = "";
		N2 = ((ll)N)*N;
		while(N2)
		{
			str.insert(str.begin(), char('A' + N2%4));
			N2 /= 4;
		}
		cout << str << endl;
	}
	return 0;
}
