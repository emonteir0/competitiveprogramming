// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Medical Clinic Queue
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2506

#include<bits/stdc++.h>

using namespace std;

queue< pair<int, int> > fila, fila2;

main()
{
	int i, N, a, b, c, x, ini, cont;
	while(scanf("%d", &N) == 1)
	{
		fila = fila2;
		ini = 7*60;
		cont = 0;
		for(i = 0; i < N; i++)
		{
			scanf("%d %d %d", &a, &b, &c);
			x = 60*a+b;
			fila.push(make_pair(x, c));
		}
		while(!fila.empty())
		{
			if(ini >= fila.front().first)
			{
				cont += ini - fila.front().first > fila.front().second;
				fila.pop();
			}
			ini += 30;
		}
		printf("%d\n", cont);
	}
	return 0;
}
