// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sleds of Santa Claus
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1762

#include<stdio.h>
#include<string.h>
#include<math.h>
 
typedef struct pres{
    char nome[1001];
    double p;
}presente;
 
presente P[1000];
 
char esta(char* v,int n)
{
    int i;
    for(i=0;i<n;i++)
        if(strcmp(v,P[i].nome)==0)
            return i;
    return -1;
}
 
main()
{
    int Y,T,i,j,k,q;
    double max,acum;
    char nome[1001];
    scanf("%d",&Y);
    for(k=0;k<Y;k++)
    {
        scanf("%d%*c",&T);
        acum=0;
        for(i=0;i<T;i++)
        {
            gets(P[i].nome);
            scanf("%lf%*c",&P[i].p);
        }
        scanf("%lf%*c",&max);
        while(1)
        {
            gets(nome);
            scanf("%d%*c",&q);
            if(strcmp(nome,"-")==0&&q==0)
                break;
            j=esta(nome,T);
            if(j!=-1)
                acum+=(P[j].p)*q;
            else
                printf("NAO LISTADO: %s\n",nome);
        }
        printf("Peso total: %.2f kg\nNumero de trenos: %d\n\n",acum,(int)ceil(acum/max));
    }
    return 0;
}
