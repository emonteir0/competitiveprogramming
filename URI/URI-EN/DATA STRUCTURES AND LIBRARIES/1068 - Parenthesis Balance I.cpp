// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Parenthesis Balance I
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1068

#include <cstdio>
#include <stack>

using namespace std;

const int MAIOR_TAMANHO = 100010;

char A[MAIOR_TAMANHO];

bool 
esta_abrindo (char c)
{
  return c == '(' || c == '[' || c == '{';
}

char 
fechando (char c)
{
  switch (c)
    {
      case '(':
        return ')';
      case '[':
        return ']';
      case '{':
        return '}';
    }
  return '\0';
}

int
main (void)
{
	bool bem_definida;
	stack<char> pilha,pilha2;
	while (scanf("%s",A)==1)
	{
		bem_definida = true;		
		pilha=pilha2;
		for (char *a = A; *a != '\0' && bem_definida; ++a)
		{
			if((*a=='(')||(*a==')'))
			{
				if (esta_abrindo(*a))
					pilha.push (*a);
				else
				{
					if (pilha.empty () || fechando (pilha.top ()) != *a)
						bem_definida = false;
					else
						pilha.pop ();
				}
			}
		}
		if (!pilha.empty ())
			bem_definida = false;
		puts (bem_definida ? "correct" : "incorrect");
	}
	return 0;
}

