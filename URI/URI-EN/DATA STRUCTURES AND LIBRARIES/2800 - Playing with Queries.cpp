// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Playing with Queries
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2800

#include<bits/stdc++.h>

using namespace std;

int Mapas[317][100001];

int N;
int A[100001];
int blocos[100001];

int query(int l, int r, int x)
{
	int ans = 0;
	for(int i = blocos[l]+1; i < blocos[r]; i++)
		ans += Mapas[i][x];
	if(blocos[l] != blocos[r])
	{
		for(int i = l; i < N && blocos[i] == blocos[l]; i++)
			ans += A[i] == x;
		for(int i = r; i && blocos[i] == blocos[r]; i--)
			ans += A[i] == x;
	}
	else
	{
		for(int i = l; i <= r; i++)
			ans += A[i] == x;
	}
	return ans;
}

void update(int pos, int x)
{
	Mapas[blocos[pos]][A[pos]]--;
	A[pos] = x;
	Mapas[blocos[pos]][x]++;
}

main()
{
	int Q, block, op, l, r, x;
	scanf("%d %d", &N, &Q);
	block = sqrt(N);
	for(int i = 0; i < N; i++)
	{
		scanf("%d", &A[i]);
		blocos[i] = i/block;
		Mapas[blocos[i]][A[i]]++;
	}
	while(Q--)
	{
		scanf("%d", &op);
		if(op == 1)
		{
			scanf("%d %d", &l, &x);
			l--;
			update(l, x);
		}
		else
		{
			scanf("%d %d %d", &l, &r, &x);
			l--, r--;
			printf("%d\n", (r-l+1)-query(l, r, x));
		}
	}
	return 0;
}
