// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Largest Rectangle in a Hi...
// Level: 8
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1683

#include<bits/stdc++.h>
#define ll long long

using namespace std;

stack<int> pilha;
ll vet[1000000];
int N;

ll getMaxArea()
{
    ll max_area = 0, area;
    int tp, i = 0;
    while (i < N)
    {
        if (pilha.empty() || vet[pilha.top()] <= vet[i])
            pilha.push(i++);
        else
        {
            tp = pilha.top();
            pilha.pop();
            area = vet[tp] * (pilha.empty() ? i : i - pilha.top() - 1);
            if (max_area < area)
                max_area = area;
        }
    }
    while (!pilha.empty())
    {
        tp = pilha.top();
        pilha.pop();
        area = vet[tp] * (pilha.empty() ? i : i - pilha.top() - 1);
 
        if (max_area < area)
            max_area = area;
    }
 
    return max_area;
}

main()
{
	int i;
	while(scanf("%d", &N) == 1 && N)
	{
		for(i = 0; i < N; i++)
			scanf("%d", &vet[i]);
		printf("%lld\n", getMaxArea());
	}
	return 0;
}
