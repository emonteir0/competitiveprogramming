// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hotel Booking
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1640

#include<bits/stdc++.h>
#define INF 1001001001
using namespace std;

int N, H, M;
vector<int> G[10001], W[10001], G2[102];
int hot[102];
int distancia[10001];


void dijkstra(int s)
{
	int u, v, w, dist;
	priority_queue<pair<int, int> > fila;
	for(int i = 1; i <= N; i++)
		distancia[i] = INF;
	distancia[s] = 0;
	fila.push(make_pair(0, s));
	while(!fila.empty())
	{
		dist = -fila.top().first;
		u = fila.top().second;
		fila.pop();
		if(dist > distancia[u])
			continue;
		for(int i = 0; i < G[u].size(); i++)
		{
			v = G[u][i];
			w = W[u][i];
			if(distancia[v] > distancia[u]+w)
			{
				distancia[v] = distancia[u]+w;
				fila.push(make_pair(-distancia[v], v));
			}
		}
	}
}

int bfs()
{
	int u, v, w;
	queue<int> fila;
	for(int i = 0; i <= H+1; i++)
		distancia[i] = INF;
	distancia[0] = 0;
	fila.push(0);
	while(!fila.empty())
	{
		u = fila.front();
		fila.pop();
		if(u == H+1)
			return distancia[u]-1;
		for(int i = 0; i < G2[u].size(); i++)
		{
			v = G2[u][i];
			if(distancia[v] > distancia[u]+1)
			{
				distancia[v] = distancia[u]+1;
				fila.push(v);
			}
		}
	}
	return -1;
}

main()
{
	int u, v, w;
	while(scanf("%d", &N) == 1 && N)
	{
		hot[0] = 1;
		scanf("%d", &H);
		hot[H+1] = N;
		G2[0].clear();
		G2[H+1].clear();
		for(int i = 1; i <= N; i++)
		{
			G[i].clear();
			W[i].clear();
		}
		for(int i = 1; i <= H; i++)
		{
			scanf("%d", &hot[i]);
			G2[i].clear();
		}
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d %d", &u, &v, &w);
			G[u].push_back(v);
			G[v].push_back(u);
			W[u].push_back(w);
			W[v].push_back(w);
		}
		for(int i = 0; i <= H+1; i++)
		{
			dijkstra(hot[i]);
			for(int j = 0; j <= H+1; j++)
				if(i != j && distancia[hot[j]] <= 600)
				{
					G2[i].push_back(j);
					G2[j].push_back(i);
				}
		}
		printf("%d\n", bfs());
	}
	return 0;
}
