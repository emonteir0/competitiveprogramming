// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Andy's First Dictionary
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1215

#include<bits/stdc++.h>

using namespace std;

set<string> conjunto;
set<string>::iterator it;


void consertar(char *c)
{
	if(*c>='A'&&*c<='Z')
		*c-='A'-'a';
	else if(*c>='a'&&*c<='z');
	else	*c=' ';
}

main()
{
	char vet[201];
	string str;
	int i,n;
	while(gets(vet)!=NULL)
	{
		n=strlen(vet);
		for(i=0;i<n;i++)
			consertar(vet+i);
		str=vet;
		stringstream ss(str);
		while(ss >> str)
			conjunto.insert(str);
	}
	for(it=conjunto.begin();it!=conjunto.end();++it)
		printf("%s\n",(*it).c_str());
	return 0;
}
