// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Interval Product
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1301

#include<stdio.h>
#define MAX 524288

int tree[MAX+1], lazy[MAX+1], A[100001];

int sig(int x)
{
	if(x > 0)
		return 1;
	if(x < 0)
		return -1;
	return 0;
}

void build(int node, int start, int end)
{
    if(start == end)
        tree[node] = sig(A[start]);
    else
    {
        int mid = (start + end) / 2;
        build(2*node, start, mid);
        build(2*node+1, mid+1, end);
        tree[node] = tree[2*node] * tree[2*node+1];
    }
}

void update(int node, int start, int end, int idx, int val)
{
    if(start == end)
        tree[node] = val;
    else
    {
        int mid = (start + end) / 2;
        if(start <= idx && idx <= mid)
            update(2*node, start, mid, idx, val);
        else
            update(2*node+1, mid+1, end, idx, val);
        tree[node] = tree[2*node] * tree[2*node+1];
    }
}

int query(int node, int start, int end, int l, int r)
{
    if(r < start || end < l)
        return 1;
    if(l <= start && end <= r)
        return tree[node];
    int mid = (start + end) / 2;
    int p1 = query(2*node, start, mid, l, r);
    int p2 = query(2*node+1, mid+1, end, l, r);
    return p1 * p2;
}

main()
{
	int N, M, i, a, b, x;
	char op;
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		for(i = 1; i <= N; i++)
			scanf("%d%*c", &A[i]);
		build(1,1,N);
		while(M--)
		{
			scanf("%c %d %d%*c", &op, &a, &b);
			if(op == 'C')
				update(1, 1, N, a, sig(b));
			else
			{
				x = query(1, 1, N, a, b);
				if(x == 0)
					printf("0");
				else
					printf("%s", (x > 0)? "+" : "-");
			}
		}
		printf("\n");
	}
	return 0;
}

