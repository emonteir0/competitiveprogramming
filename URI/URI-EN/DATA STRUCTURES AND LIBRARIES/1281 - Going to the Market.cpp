// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Going to the Market
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1281

#include<cstdio>
#include<map>
#include<string>

using namespace std;

main()
{
	map<string,float> Mapa;
	string f;
	char a[51];
	float x,ac;
	int b,n,n2;
	scanf("%d",&n);
	while(n--)
	{
		scanf("%d",&n2);
		ac=0;
		while(n2--)
		{
			scanf("%s %f%*c",a,&x);
			f=a;
			Mapa[f]=x;
		}
		scanf("%d",&n2);
		while(n2--)
		{
			scanf("%s %d%*c",a,&b);
			f=a;		
			ac+=b*Mapa[f];
		}
		printf("R$ %.2f\n",ac);	
	}
	return 0;
}
