// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Allowance
// Level: 6
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2546

#include<bits/stdc++.h>
#define MAX 524288

using namespace std;

struct st
{
	int x, i;
};

int N;
int lazy[MAX+1], A[100001];
st tree[MAX+1];

st op(st st1, st st2) 
{
	if(st1.x != st2.x)
		return (st1.x > st2.x) ? st1 : st2;
	return st1.i < st2.i ? st1 : st2;
}

void build(int node, int start, int end)
{
	lazy[node] = 0;
	if(start == end)
	{
		tree[node].x = A[start];
		tree[node].i = start;
	}
	else
	{
		int mid = (start + end) / 2;
		build(2*node, start, mid);
		build(2*node+1, mid+1, end);
		tree[node] = op(tree[2*node], tree[2*node+1]);
	}
}

void updateRange(int node, int start, int end, int l, int r, int val)
{
	if(lazy[node] != 0)
	{
		tree[node].x += lazy[node];
		if(start != end)
		{
			lazy[2*node] += lazy[node];
			lazy[2*node+1] += lazy[node];
		}
		lazy[node] = 0;
	}
	if(start > end || start > r || end < l)
		return;
	if(start >= l && end <= r)
	{
		tree[node].x += val;
		if(start != end)
		{
			lazy[node*2] += val;
			lazy[node*2+1] += val;
		}
		return; 
	}
	int mid = (start+end) / 2;
	updateRange(node*2, start, mid, l, r, val);
	updateRange(node*2+1, mid+1, end, l, r, val);
	tree[node] = op(tree[2*node], tree[2*node+1]);
}

st queryRange(int node, int start, int end, int l, int r)
{
	if(start > end || start > r || end < l)
		return tree[0];
	if(lazy[node] != 0)
	{
		tree[node].x += lazy[node];
		if(start != end)
		{
			lazy[node*2] += lazy[node];
			lazy[node*2+1] += lazy[node];
		}
		lazy[node] = 0;
	}
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start + end) / 2;
	st p1 = queryRange(node*2, start, mid, l, r);
	st p2 = queryRange(node*2+1, mid+1, end, l, r);
	return op(p1, p2);
}

main()
{
	int i, M, x, y, z;
	char op;
	while(scanf("%d %d%*c", &N, &M) == 2)
	{
		for(i = 1; i <= N; i++)
			scanf("%d", &A[i]);
		build(1, 1, N);
		scanf("%*c");
		while(M--)
		{
			scanf("%c%*c", &op);
			if(op == 'C')
			{
				scanf("%d %d%*c", &x, &y);
				printf("%d\n", queryRange(1, 1, N, x, y).i);
			}
			else
			{
				scanf("%d %d %d%*c", &x, &y, &z);
				updateRange(1, 1, N, x, y, z);
			}
		}
	}
	return 0;
}
