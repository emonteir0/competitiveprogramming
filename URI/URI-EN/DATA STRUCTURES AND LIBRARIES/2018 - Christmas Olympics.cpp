// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Olympics
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2018

#include<cstdio>
#include<cstring>
#include<string>
#include<map>
#include<algorithm>

using namespace std;

typedef struct
{
	int ouro,prata,bronze;
	string str;
} medalhas;

map<string, medalhas> Mapa;
map<string, medalhas>::iterator it;

medalhas vetor[3000];

bool cmp(medalhas a, medalhas b)
{
	if(a.ouro!=b.ouro)
		return a.ouro>b.ouro;
	if(a.prata!=b.prata)
		return a.prata>b.prata;
	if(a.bronze!=b.bronze)
		return a.bronze>b.bronze;
	return strcmp(a.str.c_str(),b.str.c_str())<0;
}

main()
{
	int i,j;
	char vet[1001];
	string str;
	while(gets(vet)!=NULL)
	{
		gets(vet);
		str=vet;
		Mapa[str].str=vet;
		Mapa[str].ouro++;
		gets(vet);
		str=vet;
		Mapa[str].str=vet;
		Mapa[str].prata++;
		gets(vet);
		str=vet;
		Mapa[str].str=vet;
		Mapa[str].bronze++;
	}
	j=0;
	for(it=Mapa.begin();it!=Mapa.end();++it)
		vetor[j++]=it->second;
	sort(vetor,vetor+j,cmp);
	printf("Quadro de Medalhas\n");
	for(i=0;i<j;i++)
		printf("%s %d %d %d\n",(vetor[i].str).c_str(),vetor[i].ouro,vetor[i].prata,vetor[i].bronze);
	return 0;
}

