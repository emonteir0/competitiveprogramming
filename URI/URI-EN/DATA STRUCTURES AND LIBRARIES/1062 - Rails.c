// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rails
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1062

#include<stdio.h>
#define maxn 1000001

int vet[maxn],vet2[maxn];

main()
{
	int i,n,n2,a,m,x,k,l=0;
	char c;
	while(scanf("%d",&n)&&n!=0)
	{
		if(l++>0)
			printf("\n");
		if(n!=0)
		{	
			while(n!=0)
			{
				for(i=0;i<n;i++)
				{
					scanf("%d",&vet2[i]);
					if(vet2[i]==0)
					{
						n=0;
						break;
					}
				}
				for(i=0;i<n/2;i++)
				{
						vet2[i]+=vet2[n-i-1];
						vet2[n-i-1]=vet2[i]-vet2[n-i-1];
						vet2[i]-=vet2[n-i-1];
				}
				if(n!=0)
				{
					m=n;
					vet[0]=0;
					k=1;
					for(i=0;i<n;i++)
					{
						if(vet2[i]==m)
						{
							m--;
						}
						else
						{
							vet[k++]=vet2[i];
						}
						while(vet[k-1]==m&&m>0)
						{
							m--;
							k--;
						}
					}
					c=1;
					for(i=k-1;i>0;i++)
					{
						if(vet[i]<vet[i-1])
						{
							c=0;
							break;
						}
					}
					printf("%s\n",c==1?"Yes":"No");
				}
			}
		}
		else
		{
			scanf("%d",&n2);
			if(n2==0)
				break;
			else
				n=n2;
		}
	}
	printf("\n");
	return 0;
}
