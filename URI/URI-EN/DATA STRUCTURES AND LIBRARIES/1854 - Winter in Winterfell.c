// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Winter in Winterfell
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1854

#include<stdio.h>
#define MOD 1000000007
#define MAX 20000000 
#define ll long long int

 
int N, M;
int BIT[1001][1001], mat[1001][1001];
ll fatoriais[MAX+1];
 
void fat()
{
    int i;
    fatoriais[0]=1;
    for(i=1;i<=MAX;i++)
        fatoriais[i]=(i*fatoriais[i-1])%MOD;
}
  
ll mdc(ll  a, ll b, ll *x, ll *y)
{
    ll xx, yy, d;
    if(b==0)
    {
        *x=1; *y=0;
        return a;
    }
    d = mdc(b, a%b, &xx, &yy);
    *x = yy;
    *y = xx - a/b*yy;
    return d;
}
  
ll inv(ll a)
{
    ll x,y,d;
    d = mdc(a,MOD,&x,&y);
    if(x<0)
        x = x+MOD;
    return x;
}


void updateBIT(int x, int y0, int val)
{
	int y;
    for (; x <= N; x += (x & -x))
    {
    	y = y0;
        for (; y <= M; y += (y & -y))
            BIT[x][y] += val;
    }
    return;
}
 
int getSum(int x, int y0)
{
    int sum = 0, y;
    for(; x > 0; x -= (x & -x))
    {
    	y = y0;
        for(; y > 0; y -= (y & -y))
            sum += BIT[x][y];
	}
    return sum;
}
 
int getnumbers(int vet[5], char mensagem[25])
{
	int i = 0, k = 0, sum = 0;
	while(mensagem[i] != 0)
	{
		if(mensagem[i] == ' ')
		{
			vet[k++] = sum;
			sum = 0;
		}
		else
			sum = 10*sum + mensagem[i] - '0';
		i++;
	}
	vet[k++] = sum;
	return k;
} 
 
int main()
{
	int p, q, x1, x2, y1, y2, xa, xb, ya, yb, v, a, b, tam, k = 1;
	char op, mensagem[30];
	int vet[10];
    fat();
	scanf("%d %d%*c", &N, &M);
	while(gets(mensagem) != NULL)
	{
		tam = getnumbers(vet, mensagem);
		if(tam == 2)
		{
			x1 = vet[0];
			y1 = vet[1];
			updateBIT(x1, y1, -mat[x1][y1]);
			mat[x1][y1] = 0;
		}
		if(tam == 3)
		{
			x1 = vet[0];
			y1 = vet[1];
			v = vet[2];
			updateBIT(x1, y1, v);
			mat[x1][y1] += v;
		}
		if(tam == 5)
		{
			x1 = vet[0];
			y1 = vet[1];
			x2 = vet[2];
			y2 = vet[3];
			b = vet[4];
			xa = x1<x2? x1:x2;
			xb = x1<x2? x2:x1;
			ya = y1<y2? y1:y2;
			yb = y1<y2? y1:y2;
			a = getSum(x2,y2) + getSum(x1-1,y1-1) - getSum(x2,y1-1) - getSum(x1-1,y2);
       		printf("Day #%d: %lld\n", k++, (((fatoriais[a+b-1]*inv(fatoriais[b]))%MOD)*inv(fatoriais[a-1]))%MOD);
		}
	}
    return 0;
}
