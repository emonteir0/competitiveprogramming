// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Diamonds and Sand
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1069

#include<stdio.h>
#include<string.h>

main()
{
	int n,i,N,x,cont;
	char vet[1001];
	scanf("%d%*c",&N);
	while(N--)
	{
		x=0;
		cont=0;
		gets(vet);
		n=strlen(vet);
		for(i=0;i<n;i++)
		{
			if(vet[i]=='<')
				x++;
			if(vet[i]=='>')
			{
				if(x>0)
				{
					x--;
					cont++;
				}
			}
		}
		printf("%d\n",cont);
	}
	return 0;
}
