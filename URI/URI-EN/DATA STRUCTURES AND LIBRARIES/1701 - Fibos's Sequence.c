// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Fibos's Sequence
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1701

#include <stdio.h>
#define MOD 1000000007
#define ll unsigned long long int
   
void multiply(int F[5][5], int M[5][5]);
   
void power(int F[5][5], int n);
  
void multiply2(int F[2][2], int M[2][2]);
  
void power2(int F[2][2], int n);
  
int vet[5]; 
  
int sum(int n)
{
  int F[5][5]={{1,1,1,1,0},{1,0,1,0,0},{1,1,0,0,0},{1,0,0,0,0},{1,1,1,1,1}};
  if(n==1)
		return vet[3];
  if(n==2)
        return vet[4];
  power(F, n-2);
  return (((ll)F[4][0])*vet[0]+((ll)F[4][1])*vet[1]+((ll)F[4][2])*vet[2]+((ll)F[4][3]*vet[3])+((ll)F[4][4])*vet[4])%MOD;
}
   
void power(int F[5][5], int n)
{
  if(n==0||n==1)
      return;
  int M[5][5]={{1,1,1,1,0},{1,0,1,0,0},{1,1,0,0,0},{1,0,0,0,0},{1,1,1,1,1}};  
    power(F, n/2);
    multiply(F, F);
  if(n%2==1)
     multiply(F, M);
}
   
void multiply(int F[5][5], int M[5][5])
{
    int G[5][5];
    int i,j,k;
    for(i=0;i<5;i++)
    {
        for(j=0;j<5;j++)
        {
            G[i][j]=0;
        }
    }
    for(i=0;i<5;i++)
    {
        for(j=0;j<5;j++)
        {
            for(k=0;k<5;k++)
            {
                G[i][j]=(G[i][j]+(((ll)F[i][k])*M[k][j]))%MOD;
            }
        }
    }
    for(i=0;i<5;i++)
    {
        for(j=0;j<5;j++)
        {
            F[i][j]=G[i][j];
        }
    }
}

int fib(int n, int F[2][2])
{
  F[0][0]=1;
  F[0][1]=1;
  F[1][0]=1;
  F[1][1]=0;
  power2(F, n);
}
 
void power2(int F[2][2], int n)
{
  if( n == 0 || n == 1)
      return;
  int M[2][2] = {{1,1},{1,0}};
  power2(F, n/2);
  multiply2(F, F);
  if (n%2 != 0)
     multiply2(F, M);
}
  
void multiply2(int F[2][2], int M[2][2])
{
  int x =  ((((ll)(F[0][0]))*M[0][0]) + ((ll)(F[0][1]))*M[1][0])%MOD;
  int y =  ((((ll)(F[0][0]))*M[0][1]) + ((ll)(F[0][1]))*M[1][1])%MOD;
  int z =  ((((ll)(F[1][0]))*M[0][0]) + ((ll)(F[1][1]))*M[1][0])%MOD;
  int w =  ((((ll)(F[1][0]))*M[0][1]) + ((ll)(F[1][1]))*M[1][1])%MOD;
  F[0][0] = x;
  F[0][1] = y;
  F[1][0] = z;
  F[1][1] = w;
}
    
int main()
{
  int a,b,n;
  int A[2][2],B[2][2];
  while(scanf("%d %d %d",&a,&b,&n)==3&&(a||b||n))
  {
      fib(a,A);
      fib(b,B);
      vet[0]=(((ll)A[0][0])*B[0][0])%MOD;
      vet[1]=(((ll)A[0][0])*B[0][1])%MOD;
      vet[2]=(((ll)A[0][1])*B[0][0])%MOD;
      vet[3]=(((ll)A[0][1])*B[0][1])%MOD;
      vet[4]=(((ll)vet[0])+vet[3])%MOD;
      printf("%d\n",sum(n));
  }
  return 0;
}
