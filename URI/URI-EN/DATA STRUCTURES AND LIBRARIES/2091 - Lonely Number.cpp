// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Lonely Number
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2091

#include<cstdio>
#include<algorithm>
#define ll long long int

using namespace std;

ll vet[100000];

main()
{
	int i, n, cont;
	ll x;
	while(scanf("%d",&n)==1 && (n!=0))
	{
		for(i = 0; i< n; i++)
			scanf("%lld",&vet[i]);
		sort(vet, vet+n);
		cont = 1;
		x = vet[0];
		for(i = 1; i < n; i++)
		{
			if(vet[i]==x)
				cont++;
			else
			{
				if(cont&1)
				{
					printf("%lld\n", x);
					break;
				}
				else
				{
					x = vet[i];
					cont = 1;
				}
			}
		}
		if(cont&1 && i == n)
			printf("%lld\n",x);
	}
	return 0;
}

