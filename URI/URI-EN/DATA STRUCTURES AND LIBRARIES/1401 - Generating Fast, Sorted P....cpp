// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Generating Fast, Sorted P...
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1401

#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

main()
{
	char vet[11];
	int num,n;
	scanf("%d",&num);
	while(num--)
	{
		scanf("%s",vet);
		n=strlen(vet);
		sort(vet,vet+n);
		do
		{
			printf("%s\n",vet);
		}while(next_permutation(vet,vet+n));
		printf("\n");
	}
	return 0;
}
