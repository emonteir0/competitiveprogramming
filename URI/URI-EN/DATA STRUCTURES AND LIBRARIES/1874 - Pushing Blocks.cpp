// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pushing Blocks
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1874

#include<bits/stdc++.h>

using namespace std;

vector< stack<int> > v;
stack<int> pilha;

main()
{
	int H,P,F,i,j,x,k;
	while(scanf("%d %d %d",&H,&P,&F)==3&&(H|P|F))
	{
		v.erase(v.begin(),v.end());
		for(i=0;i<P;i++)
			v.push_back(pilha);
		for(i=0;i<H;i++)
		{
			for(j=0;j<P;j++)
			{
				scanf("%d",&x);
				if(x>0)
					v[j].push(x);
			}
		}
		k=P-1;
		for(i=0;i<F;i++)
		{
			scanf("%d",&x);
			while((v[k].size()>=H)&&(k>0))
				k--;
			if((k>=0)&&(v[k].size()<H))
				v[k].push(x);
		}
		for(i=0;i<H;i++)
		{
			j=0;
			if(v[j].size()==(H-i))
			{
				x=v[j].top();
				v[j].pop();
				printf("%d",x);
			}
			else
				printf("0");
			for(j=1;j<P;j++)
			{
				if(v[j].size()==(H-i))
				{
					x=v[j].top();
					v[j].pop();
					printf(" %d",x);
				}
				else
					printf(" 0");
			}
			printf("\n");
		}
	}
	return 0;
}
