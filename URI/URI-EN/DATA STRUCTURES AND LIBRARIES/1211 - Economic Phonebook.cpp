// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Economic Phonebook
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1211

#include<cstdio>
#include<string>
#include<algorithm>

using namespace std;

string v[100001];

main()
{
	char vet[201];
	string str;
	int i,j,n,tam,tot;
	while(scanf("%d%*c",&n)==1)
	{
		for(i=0;i<n;i++)
		{
			scanf("%s",vet);
			v[i]=vet;
		}
		sort(v,v+n);
		str=v[0];
		tot=0;
		tam=str.length();
		for(i=1;i<n;i++)
		{
			for(j=0;j<tam;j++)
			{
				if(str[j]!=(v[i])[j])
					break;
			}
			tot+=j;
			str=v[i];
		}
		printf("%d\n",tot);
	}
	return 0;
}

