// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Eachians I
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2593

#include<bits/stdc++.h>
#define MAXL 129
#define ll long long

using namespace std;

vector<int> ans;
string P, T;
int b[MAXL], m;

void kmpPre()
{
	int i = 0, j = -1;
	b[0] = -1;
	m = P.length();
	while( i < m)
	{
		while(j >= 0 && P[i] != P[j]) j = b[j];
		i++, j++;
		b[i] = j;
	}
}

int kmpSearch()
{
	int i = 0, j = 0, n = T.size();
	int cont = 0;
	ans.clear();
	while(i < n)
	{
		while(j >= 0 && T[i] != P[j]) j = b[j];
		i++, j++;
		if(j == m)
		{
			if((i-j-1 < 0 || T[i-j-1] == ' ') && (i == n || T[i] == ' '))
			{
				ans.push_back(i-m);
				cont++;
			}
			j = b[j];
		}
	}
	return cont;
}


main()
{
	int N;
	ios_base::sync_with_stdio(NULL);
	cin.tie(NULL);
	getline(cin, T);
	cin >> N;
	while(N--)
	{
		cin >> P;
		kmpPre();
		kmpSearch();
		if(ans.size() == 0)
			printf("-1\n");
		else
		{
			printf("%d", ans[0]);
			for(int i = 1; i < ans.size(); i++)
				printf(" %d", ans[i]);
			printf("\n");
		}
	}
	return 0;
}
