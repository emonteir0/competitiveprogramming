// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Dangerous Trail
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2655

#include<bits/stdc++.h>
#define ll long long
#define MAX 524288

using namespace std;

ll tree[MAX+1];
ll A[100001];



void build(int node, int start, int end)
{
    if(start == end)
        tree[node] = A[start];
    else
    {
        int mid = (start + end) / 2;
        build(2*node, start, mid);
        build(2*node+1, mid+1, end);
        tree[node] = tree[2*node] | tree[2*node+1];
    }
}

void update(int node, int start, int end, int idx, ll val)
{
    if(start == end)
        tree[node] = val;
    else
    {
        int mid = (start + end) / 2;
        if(start <= idx && idx <= mid)
            update(2*node, start, mid, idx, val);
        else
            update(2*node+1, mid+1, end, idx, val);
        tree[node] = tree[2*node] | tree[2*node+1];
    }
}

ll query(int node, int start, int end, int l, int r)
{
    if(r < start || end < l)
        return 0;
    if(l <= start && end <= r)
        return tree[node];
    int mid = (start + end) / 2;
    return query(2*node, start, mid, l, r) | query(2*node+1, mid+1, end, l, r);
}

main()
{
	int i, N, Q, x, y, z;
	scanf("%d %d %*d", &N, &Q);
	for(i = 1; i <= N; i++)
	{
		scanf("%d", &x);
		A[i] = 1LL << x;
	}
	build(1, 1, N);
	while(Q--)
	{
		scanf("%d %d %d", &x, &y, &z);
		if(x == 1)
			printf("%d\n", __builtin_popcountll(query(1, 1, N, y, z)));
		else
			update(1, 1, N, y, 1LL << z);
	}
	return 0;
}

