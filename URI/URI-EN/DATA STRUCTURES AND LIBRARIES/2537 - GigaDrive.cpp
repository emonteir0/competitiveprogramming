// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: GigaDrive
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2537

// BIT 2D

#include<stdio.h>
 
int N, M;
int BIT[1001][1001];

void updateBIT(int x, int y0, int val)
{
	int y;
    for (; x <= N; x += (x & -x))
    {
    	y = y0;
        for (; y <= M; y += (y & -y))
            BIT[x][y] += val;
    }
    return;
}
 
int getSum(int x, int y0)
{
    int sum = 0, y;
    for(; x > 0; x -= (x & -x))
    {
    	y = y0;
        for(; y > 0; y -= (y & -y))
            sum += BIT[x][y];
	}
    return sum;
}
 
int main()
{
	int Q, xa, xb, ya, yb, a;
	while(scanf("%d %d", &N, &Q) == 2)
	{
		M = N;
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
				BIT[i][j] = 0;
		for(int i = 1; i <= N; i++)
			for(int j = 1; j <= N; j++)
			{
				scanf("%d", &a);
				if(a)
					updateBIT(i, j, 1);
			}
		while(Q--)
		{
			scanf("%d %d", &xa, &ya);
			updateBIT(xa, ya, 1);
			scanf("%d %d %d %d", &xa, &ya, &xb, &yb);
			printf("%d\n", getSum(xb, yb) + getSum(xa-1, ya-1) - getSum(xa-1, yb) - getSum(xb, ya-1));
		}
	}
	
    return 0;
}
