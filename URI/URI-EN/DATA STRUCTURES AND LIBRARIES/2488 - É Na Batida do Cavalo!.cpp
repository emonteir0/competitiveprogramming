// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: É Na Batida do Cavalo!
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2488

#include<bits/stdc++.h>

using namespace std;


map< pair<int, int>, int> posicoes;

double dist[10001];

double sqr(double x)
{
	return x*x;
}

main()
{
	int i, a, b, N, M, x, y, xant, yant;
	double d;
	scanf("%d %d", &N, &M);
	scanf("%d %d", &x, &y);
	posicoes[make_pair(x, y)] = 0;
	xant = x;
	yant = y;
	for(i = 1; i < N; i++)
	{
		scanf("%d %d", &x, &y);
		posicoes[make_pair(x, y)] = i;
		dist[i] = sqrt(sqr(x-xant) + sqr(y-yant));
		xant = x;
		yant = y;
	}
	while(M--)
	{
		scanf("%d %d %d %d", &xant, &yant, &x, &y);
		a = posicoes[make_pair(xant, yant)];
		b = posicoes[make_pair(x, y)];
		if(a > b)
			swap(a, b);
		d = 0;
		for(i = a+1; i <= b; i++)
			d = max(d, dist[i]);
		printf("%.2lf\n", d);
	}
	return 0;
}
