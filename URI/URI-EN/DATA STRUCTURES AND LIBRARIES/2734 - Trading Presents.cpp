// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Trading Presents
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2734

#include<bits/stdc++.h>

using namespace std;

int N, N2, t = 0;
int vet[201];
int dp[201][101][501];

int solve(int x, int y, int z)
{
	if(y == N && z == 0)
		return 1;
	if(z < 0 || x == N2)
		return 0;
	if(dp[x][y][z] == t || dp[x][y][z] == -t)
		return dp[x][y][z] == t;
	int r = 0;
	r = solve(x+1, y, z);
	if(z >= vet[x])
		r |= solve(x+1, y+1, z-vet[x]);
	dp[x][y][z] = r ? t : -t;
	return r;
}

main()
{
	int T, x, acum, sum;
	scanf("%d", &T);
	while(T--)
	{
		t++;
		scanf("%d", &N);
		N2 = N+N;
		acum = 0;
		for(int i = 0; i < N2; i++)
		{
			scanf("%d", &vet[i]);
			acum += vet[i];
		}
		sum = acum/2;
		x = sum;
		while(!solve(0, 0, x))
		{
			x--;
		}
		printf("%d\n", abs(acum-x-x));
	}
	return 0;
}
