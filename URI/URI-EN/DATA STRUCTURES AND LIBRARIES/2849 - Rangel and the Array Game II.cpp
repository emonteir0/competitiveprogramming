// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Rangel and the Array Game II
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2849

#include<bits/stdc++.h>
#define MAXN 300005
#define ll long long
#define abs_value(x) ((x < 0) ? -x : x)
using namespace std;

int N;
int A[100001];
int fl[MAXN], fr[MAXN], meio[MAXN];
vector<int> tree[MAXN];
vector<int> indices;
set<int> conju;

inline void build(int node, int l, int r)
{
	if(l == r)
	{
		tree[node].push_back(A[l]);
		return;
	}
	fl[node] = 2*node;
	fr[node] = 2*node+1;
	meio[node] = (l+r)>>1;
	build(fl[node], l, meio[node]);
	build(fr[node], meio[node]+1, r);
	merge(tree[fl[node]].begin(), tree[fl[node]].end(), tree[fr[node]].begin(), tree[fr[node]].end(), back_inserter(tree[node]));
}

inline int query(int node, int start, int end, int l, int r, int x)
{
	if(end < l || start > r || start > end)
		return 0;
	if(l <= start && r >= end)
	{
		return upper_bound(tree[node].begin(), tree[node].end(), x) - tree[node].begin();
	}
	return query(fl[node], start, meio[node], l, r, x) + query(fr[node], meio[node]+1, end, l, r, x);
}

inline int query(int node, int start, int end, int l, int r, int x, int K)
{
	if(end < l || start > r || start > end)
		return 0;
	if(l <= start && r >= end)
	{
		return upper_bound(tree[node].begin(), tree[node].end(), x) - tree[node].begin();
	}
	int y = query(fl[node], start, meio[node], l, r, x, K);
	if(y >= K)
		return y;
	else
		return y + query(fr[node], meio[node]+1, end, l, r, x, K);
}

inline pair<int, int> solve(int K, int L, int R)
{
	int lo = 0, hi = indices.size()-1, mid, ans;
	while(lo <= hi)
	{
		mid = (lo+hi)>>1;
		if(query(1, 0, N-1, L, R, indices[mid], K) >= K)
		{
			ans = indices[mid];
			hi = mid-1;
		}
		else
			lo = mid+1;
	}
	return make_pair(ans, query(1, 0, N-1, L, R, ans)-query(1, 0, N-1, L, R, ans-1));
}

inline void fastscan(int &number)
{
    bool negative = false;
    register int c;

    number = 0;
    c = getchar();
    if (c=='-')
    {
        negative = true;
        c = getchar();
    }
   for (; (c>47 && c<58); c=getchar())
        number = (number<<1)+(number<<3) + c - '0';
    if (negative)
        number *= -1;
}

int main()
{
	int Q, a, b, c, d, e;
	ll x, y;
	pair<int, int> p;
	fastscan(N);
	fastscan(Q);
	for(int i = 0; i < N; i++)
	{
		fastscan(A[i]);
		conju.insert(A[i]);
	}
	indices.assign(conju.begin(), conju.end());
	build(1, 0, N-1);
	while(Q--)
	{
		fastscan(a);
		fastscan(b);
		fastscan(c);
		fastscan(d);
		fastscan(e);
		a--, b--;
		p = solve(c, a, b);
		x = abs(p.second-(ll)d);
		y = abs(p.second-(ll)e);
		printf("%d %d %c\n", p.first, p.second, (x == y)? 'E' : ((x < y) ? 'G' : 'D'));
	}
	return 0;
}
