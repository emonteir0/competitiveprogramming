// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Where is the Marble?
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1025

#include<cstdio>
#include<algorithm>

using namespace std;

int n, vetor[10000];

bool cmp(int a, int b)
{
	return (a<b);
}

int pesq(int x)
{
	int inf=0,sup=n-1,meio;
	while (inf <= sup) 
     {
          meio = (inf + sup)/2;
          if (x == vetor[meio])
          {
        	   while(vetor[meio-1]==x)
        	   {
        	   	   meio--;
        	   	   if(meio==0)
        	   	   	break;
        	   }
               return meio+1;
          }
          else if (x < vetor[meio])
               sup = meio-1;
          else
               inf = meio+1;
     }
     return -1; 
}

main()
{
	int n2,x,y,i,k=1;
	while(scanf("%d %d",&n,&n2)==2&&(n>0)&&(n2>0))
	{
		printf("CASE# %d:\n",k++);
		for(i=0;i<n;i++)
			scanf("%d",&vetor[i]);
		sort(vetor,vetor+n,cmp);
		for(i=0;i<n2;i++)
		{
			scanf("%d",&x);
			y=pesq(x);
			if(y!=-1)
				printf("%d found at %d\n",x,y);
			else
				printf("%d not found\n",x);
		}
	}
	return 0;
}
