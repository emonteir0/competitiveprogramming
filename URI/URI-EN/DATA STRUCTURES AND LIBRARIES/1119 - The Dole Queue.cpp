// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Dole Queue
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1119

#include<bits/stdc++.h>

using namespace std;

int ant[20], prox[20], active[20];

main()
{
	int N, T, k, m, ind, ind2;
	while(scanf("%d %d %d", &N, &k, &m) == 3 && N)
	{
		for(int i = 0; i < N; i++)
		{
			active[i] = 1;
			ant[i] = (i+N-1)%N;
			prox[i] = (i+1)%N;
		}
		ind = 0;
		ind2 = N-1;
		T = N;
		while(1)
		{
			for(int i = 1; i < k; i++)
				ind = prox[ind];
			for(int j = 1; j < m; j++)
				ind2 = ant[ind2];
			if(T < N)
				printf(",");
			if(ind == ind2)
			{
				printf("%3d", ind+1);
				T--;
			}
			else
			{
				printf("%3d%3d", ind+1, ind2+1);
				T -= 2;
			}
			active[ind] = 0;
			active[ind2] = 0;
			prox[ant[ind]] = prox[ind];
			ant[prox[ind]] = ant[ind];
			prox[ant[ind2]] = prox[ind2];
			ant[prox[ind2]] = ant[ind2];
			if(T <= 0)
				break;
			while(active[ind] == 0)
				ind = (ind+1)%N;
			while(active[ind2] == 0)
				ind2 = (ind2+N-1)%N;
		}
		printf("\n");
	}
	return 0;
}
