// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Schweisen
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1112

#include<stdio.h>
 
int N, M;
int BIT[1001][1001];

void updateBIT(int x, int y0, int val)
{
	int y;
    for (; x <= N; x += (x & -x))
    {
    	y = y0;
        for (; y <= M; y += (y & -y))
            BIT[x][y] += val;
    }
    return;
}
 
int getSum(int x, int y0)
{
    int sum = 0, y;
    for(; x > 0; x -= (x & -x))
    {
    	y = y0;
        for(; y > 0; y -= (y & -y))
            sum += BIT[x][y];
	}
    return sum;
}
 
int main()
{
	int p, q, x1, x2, y1, y2, xa, xb, ya, yb, v, i, j;
	char op;
	while(scanf("%d %d %d%*c", &N, &M, &p)==3 && (N||M||p))
	{
		for(i=0;i<=N;i++)
			for(j=0;j<=M;j++)
				BIT[i][j] = 0;
		scanf("%d%*c", &q);
		while(q--)
		{
			scanf("%c%*c", &op);
			if(op == 'A')
			{
				scanf("%d %d %d%*c",&v, &x1, &y1);
				updateBIT(x1+1,y1+1,v);
			}
			else
			{				
				scanf("%d %d %d %d%*c",&x1,&y1,&x2,&y2);
				xa = (x1<x2? x1:x2) + 1;
				ya = (y1<y2? y1:y2) + 1;
				xb = (x1<x2? x2:x1) + 1;
				yb = (y1<y2? y2:y1) + 1;
				printf("%d\n",p*(getSum(xb,yb) + getSum(xa-1,ya-1) - getSum(xa-1,yb) - getSum(xb,ya-1))); 
			}
		}
		printf("\n");
	}
    return 0;
}
