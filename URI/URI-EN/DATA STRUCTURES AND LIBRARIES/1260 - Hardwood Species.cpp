// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hardwood Species
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1260

#include<bits/stdc++.h>

using namespace std;

map<string,int> Mapa1,Mapa2;
map<string,int>::iterator it;

main()
{
	int n,m,k=0;
	string a;
	char vet[31];
	scanf("%d%*c%*c",&n);
	while(n--)
	{
		if(k==0)	k++;
		else	printf("\n");
		m=0;
		Mapa1=Mapa2;
		while(1)
		{
			if(!(gets(vet))||strlen(vet)==0)
				break;
			a=vet;
			m++;
			Mapa1[a]++;
		}
		for(it=Mapa1.begin();it!=Mapa1.end();++it)
			printf("%s %.4lf\n",it->first.c_str(),((100*(double)it->second))/m);
	}
	return 0;
}
