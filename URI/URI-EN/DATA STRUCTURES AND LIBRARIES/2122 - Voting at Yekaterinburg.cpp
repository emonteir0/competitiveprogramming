// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Voting at Yekaterinburg
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2122

#include<bits/stdc++.h>

using namespace std;

struct st
{
	int v[500], total, ind;
	bool operator!=(st B)
	{
		for(int i = 0; i < 500; i++)
			if(v[i] != B.v[i])
				return 1;
		return 0;
	}
};

int N, V, K;
st vet[501];

bool cmp(st A, st B)
{
	if(A.total != B.total)
		return A.total > B.total;
	for(int i = 0; i < K; i++)
		if(A.v[i] != B.v[i])
			return A.v[i] > B.v[i];
	return A.ind < B.ind;
}

int main()
{
	int M, x;
	while(scanf("%d %d %d", &N, &K, &V) != EOF)
	{
		for(int i = 1; i <= K; i++)
		{
			vet[i].ind = i;
			vet[i].total = 0;
			for(int j = 0; j < 500; j++)
				vet[i].v[j] = 0;
		}
		while(N--)
		{
			scanf("%d", &M);
			for(int i = 0; i < M; i++)
			{
				scanf("%d", &x);
				if(i < V)
				{
					vet[x].v[i]++;
					vet[x].total++;
				}
			}
		}
		sort(vet+1, vet+K+1, cmp);
		/*for(int i = 1; i <= K; i++)
		{
			printf("%d -> %d:", vet[i].ind, vet[i].total);
			for(int j = 0; j < K; j++)
				printf(" %d", vet[i].v[j]);
			printf("\n");
		}*/
		printf("%d", vet[1].ind);
		for(int i = 2; i <= V; i++)
			printf(" %d", vet[i].ind);
		for(int i = V+1; i <= K; i++)
		{
			if(vet[V] != vet[i])
				break;
			printf(" %d", vet[i].ind);
		}
		printf("\n");
	}
	return 0;
}
