// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hex Statistics
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2239

#include<stdio.h>
#define ll long long
#define MOD 1000000007

int n;
ll vet[63], fat[17], fat2[17];
ll valores[65536], ma[65536], me[65536];
int paridade[65536];

ll val(ll x, int z)
{
	int i, k;
	short vet2[10], c;
	ll y;
	y = 0;
	k = 0;
	while(x>0)
	{
		c = x&15;
		if(c != z)
		vet2[k++] = c;
		x >>= 4;
	}
	for(i = k; i--; )
			y = (y<<4) + vet2[i];
	return y;
}

void criarsomas(const ll vet[63], int x, int a, int p)
{
	int i, b;
	ll sum = 0, vet2[63];
	for(i = 0; i < n; i++)
		vet2[i] = vet[63];
	if(x == 16)
		return;
	for(i = 0; i < n; i++)
		sum += vet2[i] = val(vet[i], x);
	b = a|(1<<x);
	valores[b] = sum;
	paridade[b] = p;
	criarsomas(vet2, x+1, b, p+1);
	criarsomas(vet, x+1, a, p);
}

ll max(ll a, ll b)
{
	return (a>b)? a: b;
}

ll min(ll a, ll b)
{
	return (a<b)? a: b;
}

ll somamax(int bits)
{
	int i, b, c;
	ll maximo = 0;
	if(bits == 65535)
		return 0;
	if(ma[bits] != -1)
		return ma[bits];
	for(i = 0; i < 16; i++)
		if((bits & (1<<i)) == 0)
			maximo = max(maximo, somamax(bits | (1<<i)) + valores[bits | (1<<i)]);
	return ma[bits] = maximo;
}

ll somamin(int bits)
{
	int i, b, c;
	ll minimo = 9223372036854775807;
	if(bits == 65535)
		return 0;
	if(me[bits] != -1)
		return me[bits];
	for(i = 0; i < 16; i++)
		if((bits & (1<<i)) == 0)
			minimo = min(minimo, somamin(bits | (1<<i)) + valores[bits | (1<<i)]);
	return me[bits] = minimo;
}

main()
{
	int i, y, x;
	ll sum = 0;
	fat[0] = 1;
	for(i = 1; i <= 16; i++)
		fat[i] = (fat[i-1]*i)%MOD;
	for(i = 1; i <= 16; i++)
		fat2[i] = (fat[16-i]*fat[i])%MOD;
	scanf("%x", &n);
	for(i = 0; i < n; i++)
		scanf("%llx%*c", &vet[i]);
	criarsomas(vet, 0, 0, 1);
	for(i = 1; i < 65535; i++)
	{
		ma[i] = -1;
		me[i] = -1;
		sum = (sum + (fat2[paridade[i]]*((valores[i])%MOD))%MOD)%MOD;
	}
	ma[0] = -1;
	me[0] = -1;
	printf("%llx %llx %llx\n", somamin(0), somamax(0), sum);
	return 0;
}
