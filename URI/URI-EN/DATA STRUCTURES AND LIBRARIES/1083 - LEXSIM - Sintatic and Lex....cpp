// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: LEXSIM - Sintatic and Lex...
// Level: 6
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1083

#include<bits/stdc++.h>

using namespace std;

map<char, int> prec;
stack<char> opStack;
vector<char> postfixlist;
string str;
int ind;
int sz;
char topToken;

int operand(char c)
{
	if(c >= 'a' && c <= 'z')
		return 1;
	if(c >= 'A' && c <= 'Z')
		return 1;
	if(c >= '0' && c <= '9')
		return 1;
	return 0;
}

int parentheses(char c)
{
	return c == '(' || c == ')';
}

int operador(char c)
{
	if(c == '^')
		return 1;
	if(c == '*')
		return 1;
	if(c == '/')
		return 1;
	if(c == '+')
		return 1;
	if(c == '-')
		return 1;
	if(c == '>')
		return 1;
	if(c == '<')
		return 1;
	if(c == '=')
		return 1;
	if(c == '#')
		return 1;
	if(c == '.')
		return 1;
	if(c == '|')
		return 1;
	return 0;
}

void filltree()
{
	char ch = postfixlist[ind];
	ind--;
	if(!operand(ch))
	{
		filltree();
		filltree();
	}
}

main()
{
	int error;
	prec['^'] = 7;
	prec['/'] = 6;
	prec['*'] = 6;
	prec['-'] = 5;
	prec['+'] = 5;
	prec['#'] = 4;
	prec['='] = 4;
	prec['<'] = 4;
	prec['>'] = 4;
	prec['.'] = 3;
	prec['|'] = 2;
	prec['('] = 1;
	while(cin >> str)
	{
		error = 0;
		postfixlist.clear();
		opStack = stack<char>();
		for(int i = 0; i < str.length(); i++)
		{
			if(!operand(str[i]) && !operador(str[i]) && !parentheses(str[i]))
			{
				error = 1;
				break;
			}
			if(i > 0)
			{
				if(operand(str[i]) && operand(str[i-1]))
				{
					error = 2;
					break;
				}
				if(operador(str[i]) && operador(str[i-1]))
				{
					error = 2;
					break;
				}
				if((str[i] == ')') && operador(str[i-1]))
				{
					error = 2;
					break;
				}
				if((str[i] == '(') && operand(str[i-1]))
				{
					error = 2;
					break;
				}
				if(operador(str[i]) && str[i-1] == '(')
				{
					error = 2;
					break;
				}
				if(operand(str[i]) && str[i-1] == ')')
				{
					error = 2;
					break;
				}
				if((str[i] == '(' && str[i-1] == ')') || (str[i] == ')' && str[i-1] == '('))
				{
					error = 2;
					break;
				}
			}
			if(operand(str[i]))
				postfixlist.push_back(str[i]);
			else if(str[i] == '(')
				opStack.push(str[i]);
			else if(str[i] == ')')
			{
				if(opStack.empty())
				{
					error = 2;
					break;
				}
				topToken = opStack.top();
				opStack.pop();
				while(topToken != '(')
				{
					postfixlist.push_back(topToken);
					topToken = opStack.top();
					opStack.pop();
				}
			}
			else
			{
				while(!opStack.empty() && prec[opStack.top()] >= prec[str[i]])
				{
					postfixlist.push_back(opStack.top());
					opStack.pop();
				}
				opStack.push(str[i]);
			}
		}
		if(error == 1)
			printf("Lexical Error!\n");
		else
		{
			while(!opStack.empty())
			{
				if((opStack.top() == '(' || opStack.top() == ')'))
					error = 2;
				postfixlist.push_back(opStack.top());
				opStack.pop();
			}
			if(error == 2)
				printf("Syntax Error!\n");
			else
			{
				sz = 1;
				ind = postfixlist.size()-1;
				filltree();
				if(ind != -1)
					printf("Syntax Error!\n");
				else
				{
					for(int i = 0; i < postfixlist.size(); i++)
						printf("%c", postfixlist[i]);
					printf("\n");
				}
			}
		}
	}
	return 0;
}
