// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Understanding Sorobov
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2111

#include<bits/stdc++.h>

using namespace std;

int N;
string strs[10];
int vet[10];

void solve()
{
	for(int i = 0; i < 9; i++)
	{
		vet[i] = N%10;
		N /= 10;
	}
	for(int i = 0; i < 5; i++)
		swap(vet[i], vet[8-i]);
	for(int j = 0; j < 8; j++)
	{
		for(int i = 0; i < 9; i++)
			printf("%c", strs[vet[i]][j]);
		printf("\n");
	}
	printf("\n");
}

main()
{
	strs[0] = "10-01111";
	strs[1] = "10-10111";
	strs[2] = "10-11011";
	strs[3] = "10-11101";
	strs[4] = "10-11110";
	strs[5] = "01-01111";
	strs[6] = "01-10111";
	strs[7] = "01-11011";
	strs[8] = "01-11101";
	strs[9] = "01-11110";
	while(scanf("%d", &N) == 1)
	{
		solve();
	}
	return 0;
}
