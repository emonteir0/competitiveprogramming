// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Mathematicians Needed in ...
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1804

#include<cstdio>
#include <vector>

using namespace std;

class Fenwick_Tree_Sum
{
	public:
    vector<int> tree;
    Fenwick_Tree_Sum(const vector<int>& Arg)//Arg is our array on which we are going to work
    {
        tree.resize(Arg.size());
 
        for(int i = 0 ; i<tree.size(); i++)
            increase(i, Arg[i]);
 
    }
 
    // Increases value of i-th element by ''delta''.
    void increase(int i, int delta)
    {
        for (; i < (int)tree.size(); i |= i + 1)
            tree[i] += delta;
    }
    // Returns sum of elements with indexes left..right, inclusive; (zero-based);
    int getsum(int left, int right)
    {
        return sum(right) - sum(left-1); //when left equals 0 the function hopefully returns 0
    }
 
    int sum(int ind)
    {
        int sum = 0;
        while (ind>=0)
        {
            sum += tree[ind];
            ind &= ind + 1;
            ind--;
        }
        return sum;
    }
};

vector<int> valores;

main()
{
	int i,n,x;
	char op;
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%d%*c",&x);
		valores.push_back(x);
	}
	Fenwick_Tree_Sum tree(valores);
	while(scanf("%c %d%*c",&op,&x)==2)
	{
		if(op=='a')
		{
			tree.increase(x-1,-valores[x-1]);
			valores[x-1]=0;
		}
		else
		{
			if(x==1)
				printf("0\n");
			else
				printf("%d\n",tree.getsum(0,x-2));
		}
	}
	return 0;
}
