// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: T-Shirts
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1258

#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

typedef struct a
{
	char tam,nome[101],cor[9];
} tipo;

bool cmp(tipo a, tipo b)
{
	if(a.cor[0]!=b.cor[0])
		return (a.cor[0]<b.cor[0]);
	if(a.tam!=b.tam)
		return (a.tam>b.tam);
	if(strcmp(a.nome,b.nome)!=0)
		return (strcmp(a.nome,b.nome)<0);
}

main()
{
	int N,i,j=0;
	tipo vet[60];
	while(scanf("%d%*c",&N)==1&&N)
	{
		if(j==1)
			printf("\n");
		if(j==0)
			j=1;
		for(i=0;i<N;i++)
		{
			gets(vet[i].nome);
			scanf("%s %c%*c",vet[i].cor,&vet[i].tam);
		}
		sort(vet,vet+N,cmp);
		for(i=0;i<N;i++)
			printf("%s %c %s\n",vet[i].cor,vet[i].tam,vet[i].nome);	
	}
	return 0;
}
