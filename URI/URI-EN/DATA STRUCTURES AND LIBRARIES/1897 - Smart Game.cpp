// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Smart Game
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1897

#include<cstdio>
#include<queue>
#include<set>
 
using namespace std;

typedef struct
{
	int x,cont;
} g;

set<int> conjunto;  
queue<g> fila;
g t;

main()
{
    int a,b;
    int v1,v2,v3,v4,v5,v6;
    scanf("%d %d",&a,&b);
    t.x=a;
    t.cont=0;
    fila.push(t);
    while(!fila.empty())
    {
    	t=fila.front();
    	fila.pop();
    	if(t.x==b)
    	{
    		printf("%d\n",t.cont);
    		return 0;
    	}
    	t.cont++;
    	v1=t.x*2;
    	v2=t.x/2;
    	v3=t.x*3;
    	v4=t.x/3;
    	v5=t.x+7;
    	v6=t.x-7;
    	if(conjunto.find(v1)==conjunto.end())
    	{
    		t.x=v1;
    		fila.push(t);
    	}
    	if(conjunto.find(v2)==conjunto.end())
    	{
	    	t.x=v2;
	    	fila.push(t);
	    }
	    if(conjunto.find(v3)==conjunto.end())
	    {
	    	t.x=v3;
	    	fila.push(t);
	    }
	    if(conjunto.find(v4)==conjunto.end())
    	{
	    	t.x=v4;
	    	fila.push(t);
	    }
	    if(conjunto.find(v5)==conjunto.end())
	    {
	    	t.x=v5;
	    	conjunto.insert(v5);
	    	fila.push(t);
	    }
	    if(conjunto.find(v6)==conjunto.end())
	    {
	    	t.x=v6;
	    	conjunto.insert(v6);
	    	fila.push(t);
	    }
	}
}
