// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Train Swapping
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1162

#include<stdio.h>

main()
{
	int n,i,j,n2,vet[50],cont;
	scanf("%d",&n);
	while(n--)
	{
		cont=0;
		scanf("%d",&n2);
		for(i=0;i<n2;i++)
		{
			scanf("%d",&vet[i]);
			for(j=0;j<i;j++)
				cont+=(vet[j]>vet[i]);
		}
		printf("Optimal train swapping takes %d swaps.\n",cont);
	}
	return 0;
}

