// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Friday The 13th
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2559

#include<bits/stdc++.h>
#define MAXL 1048576
#define MAXN 200000
#define INF 1000000

using namespace std;

struct st{
	int sete, treze, total;
	st()
	{
		sete = 0;
		treze = 0;
		total = 0;
	}
	st operator+(const st st2)
	{
		st st3;
		st3.sete = sete + st2.sete;
		st3.treze = treze + st2.treze;
		st3.total = total + st2.total;
		return st3;
	}
};

struct lazyquery
{
	int change7, change13, ind7, ind13;
	lazyquery()
	{
		change7 = -1;
		change13 = -1;
		ind7 = INF;
		ind13 = INF;
	}
	lazyquery(int op, int x, int q)
	{
		if(op == 7)
		{
			change7 = x;
			change13 = -1;
			ind7 = q;
			ind13 = INF;
		}
		if(op == 13)
		{
			change7 = -1;
			change13 = x;
			ind7 = INF;
			ind13 = q;
		}
	}
};

int A[MAXN+1];
st tree[MAXL+1];
lazyquery lazy[MAXL+1];


void change(int node, lazyquery val)
{
	for(int i = 0; i < 2; i++)
	{
		if(val.ind7 <= val.ind13)
		{
			if(val.change7 != -1)
			{
				tree[node].total +=  tree[node].sete*(val.change7-7);
				if(val.change7 == 13)
					tree[node].treze += tree[node].sete;
				tree[node].sete = 0;
				val.change7 = -1;
				val.ind7 = INF;
			}
		}
		else
		{
			if(val.change13 != -1)
			{
				tree[node].total += tree[node].treze*(val.change13-13);
				if(val.change13 == 7)
					tree[node].sete += tree[node].treze;
				tree[node].treze = 0;	
				val.change13 = -1;
				val.ind13 = INF;
			}
		}
	}
}

void addlazy(int node, lazyquery val)
{
	for(int i = 0; i < 2; i++)
	{
		if(val.ind7 <= val.ind13)
		{
			if(val.change7 != -1)
			{
				if(lazy[node].change13 == 7)
				{
					if(val.change7 == 13)
					{
						lazy[node].change13 = -1;
						lazy[node].ind13 = INF;
					}
					else
					{
						lazy[node].change13 = val.change7;
						lazy[node].ind13 = val.ind7;
					}
				}
				if(lazy[node].change7 == -1)
				{
					lazy[node].change7 = val.change7;
					lazy[node].ind7 = val.ind7;
				}
			}
			val.change7 = -1;
			val.ind7 = INF;
		}
		else
		{
			if(val.change13 != -1)
			{
				if(lazy[node].change7 == 13)
				{
					if(val.change13 == 7)
					{
						lazy[node].change7 = -1;
						lazy[node].ind7 = INF;
					}
					else
					{
						lazy[node].change7 = val.change13;
						lazy[node].ind7 = val.ind13;
					}
				}
				if(lazy[node].change13 == -1)
				{
					lazy[node].change13 = val.change13;
					lazy[node].ind13 = val.ind13;
				}
			}
			val.change13 = -1;
			val.ind13 = INF;
		}
	}
}

bool check(lazyquery l)
{
	return !(l.change7 == -1 && l.change13 == -1);
}

void build(int node, int start, int end)
{
	lazy[node] = lazyquery();
	if(start == end)
	{
		tree[node].total = A[start];
		tree[node].sete = A[start] == 7;
		tree[node].treze = A[start] == 13;
	}
	else
	{
		int mid = (start+end)/2;
		build(node*2, start, mid);
		build(node*2+1, mid+1, end);
		tree[node] = tree[node*2] + tree[node*2+1];
	}
}

void update(int node, int start, int end, int x, int val)
{
	if(check(lazy[node]))
	{
		change(node, lazy[node]);
		if(start != end)
		{
			addlazy(node*2, lazy[node]);
			addlazy(node*2+1, lazy[node]);
		}
		lazy[node] = lazyquery();
	}
	if(start > end || start > x || end < x)
		return;
	if(start == end)
	{
		tree[node].total = val;
		tree[node].sete = val == 7;
		tree[node].treze = val == 13;
		return;
	}
	int mid = (start+end)/2;
	update(node*2, start, mid, x, val);
	update(node*2+1, mid+1, end, x, val);
	tree[node] = tree[node*2] + tree[node*2+1];
}


void updateRange(int node, int start, int end, int l, int r, lazyquery val)
{
	if(check(lazy[node]))
	{
		change(node, lazy[node]);
		if(start != end)
		{
			addlazy(node*2, lazy[node]);
			addlazy(node*2+1, lazy[node]);
		}
		lazy[node] = lazyquery();
	}
	if(start > end || start > r || end < l)
		return;
	if(start >= l && end <= r)
	{
		change(node, val);
		if(start != end)
		{
			addlazy(node*2, val);
			addlazy(node*2+1, val);
		}
		return;
	}
	int mid = (start+end)/2;
	updateRange(node*2, start, mid, l, r, val);
	updateRange(node*2+1, mid+1, end, l, r, val);
	tree[node] = tree[node*2] + tree[node*2+1];
}

st queryRange(int node, int start, int end, int l, int r)
{
	if(start > end || start > r || end < l)
		return st();
	if(check(lazy[node]))
	{
		change(node, lazy[node]);
		if(start != end)
		{
			addlazy(node*2, lazy[node]);
			addlazy(node*2+1, lazy[node]);
		}
		lazy[node] = lazyquery();
	}
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start+end)/2;
	st p1 = queryRange(node*2, start, mid, l, r);
	st p2 = queryRange(node*2+1, mid+1, end, l, r);
	return p1 + p2;
}


int main()
{
	int N, M, op, a, b, c, d;
	while(scanf("%d", &N) == 1)
	{
		for(int i = 1; i <= N; i++)
			scanf("%d", &A[i]);
		build(1, 1, N);
		scanf("%d", &M);
		for(int i = 0; i < M; i++)
		{
			scanf("%d", &op);
			if(op == 1)
			{
				scanf("%d %d", &a, &b);
				update(1, 1, N, a, b);
			}
			else if(op == 2)
			{
				scanf("%d %d %d %d", &a, &b, &c, &d);
				if(c != d)
					updateRange(1, 1, N, a, b, lazyquery(c, d, i));
			}
			else
			{
				scanf("%d %d", &a, &b);
				printf("%d\n", queryRange(1, 1, N, a, b).total);
			}
		}
	}
	return 0;
}
