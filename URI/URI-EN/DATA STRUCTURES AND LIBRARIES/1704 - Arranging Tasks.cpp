// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Arranging Tasks
// Level: 1
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1704

#include<bits/stdc++.h>

using namespace std;

priority_queue <int> pq, pq2;

struct Value
{
	int x, y;
};

Value values[100001];

bool cmp(Value a, Value b)
{
	if(a.y != b.y)
		return a.y > b.y;
	return a.x > b.x;
}

main()
{
	int i, N, H, tot;
	while(scanf("%d %d", &N, &H) == 2)
	{
		tot = 0;
		pq = pq2;
		for(i = 0; i < N; i++)
		{
			scanf("%d %d", &values[i].x, &values[i].y);
			tot += values[i].x;
		}
		sort(values, values+N, cmp);
		i = 0;
		while(H)
		{
			while((i < N) && (H <= values[i].y))
			{
				pq.push(values[i].x);
				i++;
			}
			H--;
			if(!pq.empty())
			{
				tot -= pq.top();
				pq.pop();
			}
		}
		printf("%d\n", tot);
	}
	return 0;
}
