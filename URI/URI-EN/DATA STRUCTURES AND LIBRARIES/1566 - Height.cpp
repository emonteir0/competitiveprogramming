// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Height
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1566

#include<cstdio>
#include<algorithm>

int vet[3000000];

main()
{
	int N,pessoas,i;
	scanf("%d",&N);
	while(N--)
	{
		scanf("%d",&pessoas);
		for(i=0;i<pessoas;i++)
			scanf("%d",&vet[i]);
		std::sort(vet,vet+pessoas);
		printf("%d",vet[0]);
		for(i=1;i<pessoas;i++)
			printf(" %d",vet[i]);
		printf("\n");
	}
	return 0;
}
