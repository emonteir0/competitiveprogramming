// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: TDA Rational
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1022

#include<stdio.h>
#include<math.h>

int mdc(int a, int b)
{
	int aux;
	while(b!=0)
	{
		aux=b;
		b=a%b;
		a=aux;
	}
	return a;
}

main()
{
	int a,b,c,d,e,f,x,i,n;
	char sig;
	scanf("%d%*c",&n);
	for(i=0;i<n;i++)
	{
		scanf("%d%*c%*c%*c%d%*c%c%*c%d%*c%*c%*c%d%*c",&a,&b,&sig,&c,&d);
		if(sig=='+')
		{
			e=a*d+b*c;
			f=b*d;
		}
		if(sig=='-')
		{
			e=a*d-b*c;
			f=b*d;
		}
		if(sig=='*')
		{
			e=a*c;
			f=b*d;
		}
		if(sig=='/')
		{
			e=a*d;
			f=b*c;
		}
		x=mdc(abs(e),f);
		printf("%d/%d = %d/%d\n",e,f,e/x,f/x);
	}
	return 0;
}
