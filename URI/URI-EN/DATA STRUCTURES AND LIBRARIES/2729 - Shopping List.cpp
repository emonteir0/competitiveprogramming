// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shopping List
// Level: 1
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2729

#include<bits/stdc++.h>

using namespace std;


set<string> conjunto, conjunto2;
set<string>::iterator it;

main()
{
	string str;
	int N;
	scanf("%d%*c", &N);
	while(N--)
	{
		conjunto = conjunto2;
		getline(cin, str);
		stringstream ss;
		ss << str;
		while(ss >> str)
			conjunto.insert(str);
		it = conjunto.begin();
		cout << *(it++);
		for(; it != conjunto.end(); it++)
			cout << " " << *it;
		cout << endl;
	}
	return 0;
}
