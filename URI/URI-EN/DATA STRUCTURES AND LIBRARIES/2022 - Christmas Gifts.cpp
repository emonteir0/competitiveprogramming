// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Christmas Gifts
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2022

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<string>

using namespace std;

typedef struct
{
	int pref;
	char str[1001];
	double valor;
} presente;

presente lista[100];

bool cmp(presente a, presente b)
{
	int x;
	if(a.pref!=b.pref)
		return a.pref>b.pref;
	if(a.valor!=b.valor)
		return a.valor<b.valor;
	return strcmp(a.str,b.str)<0;
}

int main() 
{
	char nome[1001];
	int i,n;
	while(scanf("%s %d%*c",nome,&n)==2)
	{
		printf("Lista de %s\n",nome);
		for(i=0;i<n;i++)
		{
			gets(lista[i].str);
			scanf("%lf %d%*c",&lista[i].valor,&lista[i].pref);
		}
		sort(lista,lista+n,cmp);
		for(i=0;i<n;i++)
			printf("%s - R$%.2lf\n",lista[i].str,lista[i].valor);
		printf("\n");
	}
	return 0;
}
