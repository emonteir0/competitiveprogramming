// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Prant and the Indecision
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2064

#include<bits/stdc++.h>

using namespace std;

int freq[26];
char mapa[26], mapa2[26];
char fav[27];
int val[26];
char ch[100001], ch2[100001];
char str[100001];

main()
{
	int N, M, Q, j, k, ind = -1, cont, ma = 0;
	char aux;
	for(int i = 0; i < 26; i++)
	{
		mapa[i] = i+'a';
		mapa2[i] = i+'a';
	}
	scanf("%d %d %d%*c", &M, &N, &Q);
	scanf("%s %s%*c", fav, str);
	for(int i = 0; i < M; i++)
		val[fav[i]-'a'] = 1;
	for(int i = 0; i < N; i++)
	{
		ma += val[str[i]-'a'];
		freq[str[i]-'a']++;
	}
	cont = ma;
	for(int i = 0; i < Q; i++)
	{
		scanf("%c %c%*c", &ch[i], &ch2[i]);
		for(j = 0; j < 26; j++)
			if(mapa[j] == ch[i])
				break;
		for(k = 0; k < 26; k++)
			if(mapa[k] == ch2[i])
				break;
		aux = mapa[j];
		mapa[j] = mapa[k];
		mapa[k] = aux;
		cont += freq[j]*(val[ch2[i]-'a'] - val[ch[i]-'a']) + freq[k] * (val[ch[i]-'a'] - val[ch2[i]-'a']);
		if(cont > ma)
		{
			ma = cont;
			ind = i;
		}
	}
	for(int i = 0; i < 26; i++)
		mapa[i] = i+'a';
	for(int i = 0; i <= ind; i++)
	{
		for(j = 0; j < 26; j++)
			if(mapa[j] == ch[i])
				break;
		for(k = 0; k < 26; k++)
			if(mapa[k] == ch2[i])
				break;
		aux = mapa[j];
		mapa[j] = mapa[k];
		mapa[k] = aux;
	}
	for(int i = 0; i < N; i++)
		str[i] = mapa[str[i]-'a'];
	printf("%d\n%s\n", ma, str);
	return 0;
}
