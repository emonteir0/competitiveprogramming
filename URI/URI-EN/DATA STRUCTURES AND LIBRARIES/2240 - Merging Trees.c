// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Merging Trees
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2240

#include<stdio.h>

int esqa[10001], dira[10001];
int esqb[10001], dirb[10001];

int max(int a, int b)
{
	return (a>b) ? a : b;
}

int min(int a, int b)
{
	return (a<b) ? a : b;
}

int finda(int x, int c, int v)
{
	if(esqa[x] != 0)
		v = max(v, finda(esqa[x], 1, v));
	if(dira[x] != 0)
		v = max(v, finda(dira[x], c+1, v));
	return max(c,v);
}

int findb(int x, int c, int v)
{
	if(dirb[x] != 0)
		v = max(v, findb(dirb[x], 1, v));
	if(esqb[x] != 0)
		v = max(v, findb(esqb[x], c+1, v));
	return max(c,v);
}

main()
{
	int i, N, M, x, y, z, u, v, t, w;
	scanf("%d", &N);
	for(i = 1; i <= N; i++)
	{
		scanf("%d %d %d", &x, &y, &z);
		esqa[x] = y;
		dira[x] = z;
	}
	scanf("%d", &M);
	for(i = 1; i <= M; i++)
	{
		scanf("%d %d %d", &x, &y, &z);
		esqb[x] = y;
		dirb[x] = z;
	}
	u = finda(1, 1, 0);
	v = findb(1, 1, 0);
	t = w = 1;
	x = 1;
	while(dira[x] != 0)
	{
		t++;
		x = dira[x];
	}
	x = 1;
	while(esqb[x] != 0)
	{
		w++;
		x = esqb[x];
	}
	printf("%d\n", N + M - max(min(u,w),min(t,v)));
	return 0;
}

