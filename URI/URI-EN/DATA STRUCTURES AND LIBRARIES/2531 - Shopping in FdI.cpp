// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Shopping in FdI
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2531

#include<bits/stdc++.h>
#define MAX 1048576

using namespace std;

struct st{
	int me, ma;
};

st tree[MAX+1], vazio;
int A[MAX+1];
int lazy[MAX+1];

st op(st st1, st st2)
{
	st st3;
	if(st1.ma == 0)
		return st2;
	if(st2.ma == 0)
		return st1;
	st3.ma = A[st1.ma] > A[st2.ma] ? st1.ma : st2.ma;
	st3.me = A[st1.me] < A[st2.me] ? st1.me : st2.me;
	return st3;	
}

void build(int node, int start, int end)
{
	if(start > end)
		return;
	if(start == end)
	{
		tree[node].ma = start;
		tree[node].me = start;
		return;
	}
	int mid = (start+end)/2;
	build(node*2, start, mid);
	build(node*2+1, mid+1, end);
	tree[node] = op(tree[node*2], tree[node*2+1]);
}

void update(int node, int start, int end, int ind, int val)
{
	if(start == end)
		A[start] = val;
	else
	{
		int mid = (start+end)/2;
		if(ind <= mid)
			update(node*2, start, mid, ind, val);
		else
			update(node*2+1, mid+1, end, ind, val);
		tree[node] = op(tree[node*2], tree[node*2+1]);
	}
}

st query(int node, int start, int end, int l, int r)
{
	if(start > end || start > r || end < l)
		return vazio;
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start+end)/2;
	st p1 = query(node*2, start, mid, l, r);
	st p2 = query(node*2+1, mid+1, end, l, r);
	return op(p1, p2);
}

int main()
{
	int i, N, M, x, y, z;
	st aux;
	vazio.ma = 0;
	vazio.me = 0;
	while(scanf("%d", &N) == 1)
	{
		for(i = 1; i <= N; i++)
			scanf("%d", &A[i]);
		build(1, 1, N);
		scanf("%d", &M);
		while(M--)
		{
			scanf("%d %d %d", &x, &y, &z);
			if(x == 1)
				update(1, 1, N, y, z);
			else
			{
				aux = query(1, 1, N, y, z);
				printf("%d\n", A[aux.ma] - A[aux.me]);
			}
		}
	}
	
	return 0;
}
