// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Easy Problem from Rujia Liu?
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1424

#include<map>
#include<vector>
#include<cstdio>

using namespace std;

map< int, vector<int> > Mapa,Mapa2;
 

main()
{
	int i,k,n,m,x;
	
	while(scanf("%d %d",&n,&m)==2)
	{
		for(i=1;i<=n;i++)
		{
			scanf("%d",&x);
			Mapa[x].push_back(i);
		}
		while(m--)
		{
			scanf("%d %d",&k,&i);
			if((int)Mapa[i].size()>=k)
				printf("%d\n",(Mapa[i])[k-1]);
			else
				printf("0\n");
		}
		Mapa=Mapa2;
	}
	return 0;
}

