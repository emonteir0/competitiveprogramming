// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Sort! Sort!! And Sort!!!
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1252

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#define max 1000
 
using namespace std;
 
int M;
 
bool cmp(int a, int b){
    if(a%M!=b%M) return a%M<b%M;
    if(a%2==0&&b%2!=0)	return false;
    if(a%2!=0&&b%2==0)	return true;
    if(a%2!=0&&b%2!=0)	return a>b;
    if(a%2==0&&b%2==0)	return b>a;
}
 
int main (){
    int i,N,vet[10000];
    while(scanf("%d %d",&N,&M)==2&&(N||M))
    {
        for(i=0;i<N;i++)
            scanf("%d",&vet[i]);
        sort(vet,vet+N,cmp);
        printf("%d %d\n",N,M);
        for(i=0;i<N;i++)
            printf("%d\n",vet[i]);
    }
    printf("0 0\n");
    return 0;
}
