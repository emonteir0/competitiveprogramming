// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Help Girafales
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1911

#include<bits/stdc++.h>

using namespace std;
  
map<string,string> Mapa1,Mapa2;

int comparar(const char* a, const char* b, int n)
{
	int i,cont=0;
	for(i=0;i<n;i++)
	{
		if(a[i]!=b[i])
			cont++;
		if(cont==2)
			return 1;
	}
	return 0;
}

main()
{
	int n,m,i,cont;
	string a,b;
	while(scanf("%d%*c",&n)==1&&(n!=0))
	{
		Mapa1=Mapa2;
		cont=0;
		for(i=0;i<n;i++)
		{
			cin>>a>>b;
			Mapa1[a]=b;
		}
		scanf("%d%*c",&m);
		for(i=0;i<m;i++)
		{
			cin>>a>>b;
			cont+=(comparar(Mapa1[a].c_str(),b.c_str(),b.length()));
		}
		printf("%d\n",cont);
	}
	return 0;
}
