// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pomekon Catalog
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2223

#include <bits/stdc++.h>

using namespace std;

#define _ ios_base::sync_with_stdio(0);
#define endl '\n'
#define ll long long
#define INF ((int)1e9)
#define vi vector <int>
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
#define ii pair <int, int>
#define MAXN 100005
#define pb push_back

int t[MAXN], p[MAXN];
int tc, n, cmd, a, b, k, m;

struct Data
{
	int l, r;
	int k, index;
	Data(){};
	Data(int l, int r, int k, int index) :
		l(l), r(r), k(k), index(index) {}
};

bool cmp(Data a, Data b)
{
	if (a.k != b.k) return a.k < b.k;
	return a.index < b.index;
}

void f5(int i, int val){
	++i;
	while (i<=n){
		t[i]+=val;
		i+=(i&-i);
	}
}

int sum(int i){
	int s=0;
	++i;
	while (i>0){
		s+=t[i];
		i-=(i&-i);
	}
	return s;
}

int main()
{_
	cin >> tc;
	while (tc--)
	{
		cin >> n;
		for (int i = 0; i < n; i++)
		{
			cin >> p[i];
			f5(i, p[i]);
		}
		
		cin >> m;
		vector <Data> query;
		vector <ii> v(1);

		for (int i = 0; i < m; i++)
		{
			cin >> cmd >> a >> b;
			if (cmd == 1)
			{
				cin >> k;
				query.pb(Data(a, b, k, i));
			}
			else
			{
				v.pb(ii(a, b));
			}
		}
		
		sort(all(query), cmp);
		
		vi ans(m + 1, -1);
		int lastUpd = 0;
		


		for (int i = 0; i < sz(query); i++)
		{
			if (lastUpd != query[i].k)
			{
				for (int j = lastUpd + 1; j <= query[i].k; j++)
				{
					f5(v[j].first - 1, v[j].second-sum(v[j].first - 1)+sum(v[j].first-2));
					

				}
				lastUpd = query[i].k;
			}
			ans[query[i].index] = sum(query[i].r - 1) - sum(query[i].l - 2);
		}
		
		for (int i = 0; i < m; i++)
		{
			if (ans[i] != -1)
			{
				cout << ans[i] << endl;
			}
		}
		
		for (int i = 0; i <= n; i++) t[i] = 0;
	}
			
		

	return 0;
}
