// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Supermarket Line
// Level: 4
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2065

#include<bits/stdc++.h>

using namespace std;

set< pair<int, int> > filas;
set<int> livres;
int vet[10000];

main()
{
	int n, m, i, t0 = 0, x, ind;
	scanf("%d %d", &n, &m);
	for(i = 0; i < n; i++)
	{
		scanf("%d", &vet[i]);
		livres.insert(i);
	}
	while(m--)
	{
		scanf("%d", &x);
		if(livres.size() > 0)
		{
			ind = *livres.begin();
			livres.erase(ind);
			filas.insert(make_pair(t0 + x*vet[ind], ind));
		}
		else
		{
			t0 = (filas.begin())->first;
			ind = (filas.begin())->second;
			filas.erase(filas.begin());
			livres.insert(ind);
			while(filas.size() > 0)
			{
				if((filas.begin())->first != t0)
					break;
				ind = (filas.begin())->second;
				livres.insert(ind);
				filas.erase(filas.begin());
			}
			ind = *livres.begin();
			livres.erase(ind);
			filas.insert(make_pair(t0 + x*vet[ind], ind)); 
		}
	}
	while(filas.size() > 0)
	{
		t0 = filas.begin()->first;
		filas.erase(filas.begin());
	}
	printf("%d\n", t0);
	return 0;
}

