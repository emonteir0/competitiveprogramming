// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Hash Tables
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1256

#include<stdio.h>
#include<string.h>

int intlen(int *v)
{
	int i=-1;
	while(*(v+(++i))!=0);
	return i;
}

main()
{
	int n,a,b,x,i,j,v[100][101];
	scanf("%d",&n);
	while((n--)>0)
	{
		scanf("%d %d",&a,&b);
		for(i=0;i<a;i++)
			v[i][0]='\0';
		while(b--)
		{
			scanf("%d",&x);
			int l=intlen(v[x%a]);
			v[x%a][l]=x;
			v[x%a][l+1]='\0';
		}
		for(i=0;i<a;i++)
		{
			printf("%d",i);
			for(j=0;j<intlen(v[i]);j++)
				printf(" -> %d",v[i][j]);
			printf(" -> \\\n");
		}
		printf("%s",n==0?"":"\n");
	}
	return 0;
}
