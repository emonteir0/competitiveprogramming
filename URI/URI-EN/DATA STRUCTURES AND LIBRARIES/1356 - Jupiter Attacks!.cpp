// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Jupiter Attacks!
// Level: 8
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1356

#include<stdio.h>
#define ll long long

ll tree[100001];
ll A[100001], expo[100001];
int N, P;

void update(int i, int val)
{
	while(i <= N)
	{
		tree[i] += val;
		if(tree[i] >= P)
			tree[i] -= P;
		i += (i&-i);
	}
}

ll sum(int i)
{
	ll s = 0;
	while(i > 0)
	{
		s += tree[i];
		i -= (i&-i);
	}
	return s%P;
}

ll mdc(ll a, ll b, ll *x, ll *y)
{
	ll xx, yy, d;
	if(b == 0)
	{
		*x = 1; *y = 0;
		return a;
	}
	d = mdc(b, a%b, &xx, &yy);
	*x = yy;
	*y = xx-a/b*yy;
	return d;
}

ll inv(ll a)
{
	ll x, y;
	mdc(a, P, &x, &y);
	if(x < 0)
		x += P;
	return x;
}

main()
{
	char op;
	int i, x, y, s = 0, B, M;
	ll z;
	while(scanf("%d %d %d %d%*c", &B, &P, &N, &M) == 4 && (B||P||N||M))
	{
		for(i = 0; i <= N; i++)
			tree[i] = 0;
		z = 1;
		for(i = N; i >= 1; i--)
		{
			expo[i] = z;
			tree[i] = 0;
			A[i] = 0;
			z = (((ll)z)*B)%P;
		}
		while(M--)
		{
			scanf("%c %d %d%*c", &op, &x, &y);
			if(op == 'E')
			{
				update(x, ((y-A[x]+P)*expo[x])%P);
				A[x] = y;
			}
			else
				printf("%lld\n", ((sum(y)-sum(x-1)+P)*inv(expo[y]))%P);
		}
		printf("-\n");
	}
	return 0;
}
