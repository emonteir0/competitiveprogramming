// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Even and Odd
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1259

#include<cstdio>
#include<algorithm>
#define MAX 100000

using namespace std;

int vet[MAX];

bool cmp(int a,int b){
	if(a%2==0&&b%2==1)
		return true;
	if(b%2==0&&a%2==1)
		return false;
	if(a%2==0&&b%2==0)
		return a<b;
	if(a%2==1&&b%2==1)
		return a>b;
}

int main()
{
	int N;
	scanf("%d",&N);
	for (int i=0;i<N;i++) scanf("%d",&vet[i]);
	sort(vet,vet+N,cmp);
	for (int i=0;i<N;i++) printf("%d\n",vet[i]);
	return 0;
}

