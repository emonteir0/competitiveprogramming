// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Gym
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2538

#include<bits/stdc++.h>

using namespace std;

int tree[100001];
int N = 100000;

void update(int i, int val)
{
	while (i <= N)
	{
		tree[i] += val;
		i += (i&-i);
	}
}

int sum(int i)
{
	int s = 0;
	while (i > 0)
	{
		s += tree[i];
		i -= (i & -i);
	}
	return s;
}

main()
{
	int IP, M, PC, NA;
	while(scanf("%d %d", &IP, &M) == 2)
	{
		while(M--)
		{
			scanf("%d %d", &PC, &NA);
			if(sum(PC+IP <= 100000 ? PC+IP : 100000)-sum(PC-IP-1 >= 0 ? PC-IP-1 : 0) <= NA)
				update(PC, 1);
		}
		printf("%d\n", sum(100000));
		for(int i = 0; i <= 100000; i++)
			tree[i] = 0;
	}
	return 0;
}
