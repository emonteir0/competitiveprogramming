// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: The Fantastic Jaspion
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1449

#include<cstdio>
#include<map>
#include<string>

using namespace std;

map<string,string> Mapa1,Mapa2;

main()
{
	int N,n,m,l=0;
	char c,vet[81];
	string a,b;
	scanf("%d%*c",&N);
	while(N--)
	{
		scanf("%d %d%*c",&n,&m);
		while(n--)
		{
			gets(vet);
			a=vet;
			gets(vet);
			b=vet;
			Mapa1[a]=b;
		}
		while(m--)
		{
			b="";
			l=0;
			while(scanf("%c",&c)==1)
			{
				if((c==' ')||(c=='\n'))
				{
					printf("%s",((l==1)?" ":""));
					if(Mapa1.find(b)!=Mapa1.end())
						printf("%s",Mapa1[b].c_str());
					else
						printf("%s",b.c_str());
					l=1;
					b="";
					if(c=='\n') break;
				}
				else	b+=c;
			}
			printf("\n");
		}
		Mapa1=Mapa2;
		printf("\n");
	}
	return 0;
}
