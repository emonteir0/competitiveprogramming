// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Acordes Intergaláticos
// Level: 7
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2658

#include<bits/stdc++.h>
#define MAX 1048576

using namespace std;

struct st{
	int vet[9];
};

st sum(st st1, st st2)
{
	int i;
	st st3;
	for(i = 0; i < 9; i++)
		st3.vet[i] = st1.vet[i] + st2.vet[i];
	return st3;
}

st change(st st1, int f)
{
	int i;
	st st2;
	for(i = 0; i < 9; i++)
		st2.vet[(i+f)%9] = st1.vet[i];
	return st2;
}

st tree[MAX+1], vazio, one;
int lazy[MAX+1];

void build(int node, int start, int end)
{
	if(start > end)
		return;
	if(start == end)
	{
		tree[node].vet[1] = 1;
		return;
	}
	int mid = (start+end)/2;
	build(node*2, start, mid);
	build(node*2+1, mid+1, end);
	tree[node] = sum(tree[node*2], tree[node*2+1]);
}

void updateRange(int node, int start, int end, int l, int r, int val)
{
	if(lazy[node])
	{
		tree[node] = change(tree[node], lazy[node]);
		if(start != end)
		{
			lazy[node*2] += lazy[node];
			lazy[node*2+1] += lazy[node];
		}
		lazy[node] = 0;
	}
	if(start > end || start > r || end < l)
		return;
	if(start >= l && end <= r)
	{
		tree[node] = change(tree[node], val);
		if(start != end)
		{
			lazy[node*2] += val;
			lazy[node*2+1] += val;
		}
		return;
	}
	int mid = (start+end)/2;
	updateRange(node*2, start, mid, l, r, val);
	updateRange(node*2+1, mid+1, end, l, r, val);
	tree[node] = sum(tree[node*2], tree[node*2+1]);
}

st queryRange(int node, int start, int end, int l, int r)
{
	if(start > end || start > r || end < l)
		return vazio;
	if(lazy[node])
	{
		tree[node] = change(tree[node], lazy[node]);
		if(start != end)
		{
			lazy[node*2] += lazy[node];
			lazy[node*2+1] += lazy[node];
		}
		lazy[node] = 0;
	}
	if(start >= l && end <= r)
		return tree[node];
	int mid = (start+end)/2;
	st p1 = queryRange(node*2, start, mid, l, r);
	st p2 = queryRange(node*2+1, mid+1, end, l, r);
	return sum(p1, p2);
}

main()
{
	int i, j, N, M, x, y, best, ind;
	st aux;
	scanf("%d %d", &N, &M);
	for(i = 0; i < 9; i++)
	{
		vazio.vet[i] = 0;
		one.vet[i] = 0;
	}
	one.vet[1] = 1;
	build(1, 0, N-1);
	while(M--)
	{
		scanf("%d %d", &x, &y);
		aux = queryRange(1, 0, N-1, x, y);
		best = 0;
		ind = -1;
		for(i = 0; i < 9; i++)
		{
			if(aux.vet[i] >= best)
			{
				best = aux.vet[i];
				ind = i;
			}
		}
		//printf("%d %d\n", best, ind);
		updateRange(1, 0, N-1, x, y, ind);
	}
	for(i = 0; i < N; i++)
	{
		aux = queryRange(1, 0, N-1, i, i);
		for(j = 0; j < 9; j++)
			if(aux.vet[j])
			{
				printf("%d\n", j);
				break;
			}
	}
	return 0;
}
