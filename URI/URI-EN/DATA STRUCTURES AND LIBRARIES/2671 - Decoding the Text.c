// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Decoding the Text
// Level: 6
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2671

#include<stdio.h>

int k;
char ans[20001], msg[20001];

void build(int node, int N)
{
	if(node >= N)
		return;
	build(2*node+1, N);
	ans[node] = msg[k++];
	build(2*node+2, N);
}

main()
{
	int i, N;
	while(scanf("%d%*c", &N) == 1 && N)
	{
		k = 0;
		gets(msg);
		build(0, N);
		ans[N] = 0;
		printf("%s\n", ans);
	}
	return 0;
}
