// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Tutors
// Level: 8
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2120

#include<bits/stdc++.h>
#define INF 1001001001

using namespace std;

set< pair<pair<int, int>, int> > conjunto;
set< pair<pair<int, int>, int> >::iterator it;

int pai[100001];

main()
{
	int N, Q, x, f = 0;
	pair<int, int> p;
	scanf("%d", &N);
	scanf("%d", &x);
	conjunto.insert(make_pair(make_pair(0, x-1), x));
	conjunto.insert(make_pair(make_pair(x+1, INF), x));
	for(int i = 2; i <= N; i++)
	{
		scanf("%d", &x);
		it = conjunto.upper_bound(make_pair(make_pair(x, INF), 0));
		it--;
		pai[i] = it->second;
		p = it->first;
		conjunto.erase(*it);
		if(p.first != x)
			conjunto.insert(make_pair(make_pair(p.first, x-1), x));
		if(p.second != x)
			conjunto.insert(make_pair(make_pair(x+1, p.second), x));
	}
	scanf("%d", &Q);
	while(Q--)
	{
		scanf("%d", &x);
		if(f)
			printf(" ");
		f = 1;
		printf("%d", pai[x]);
	}
	printf("\n");
	return 0;
}
