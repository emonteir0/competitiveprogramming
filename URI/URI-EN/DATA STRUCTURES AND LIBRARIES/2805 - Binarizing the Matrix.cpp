// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Binarizing the Matrix
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2805

#include<bits/stdc++.h>
#define ll long long
#define MOD 1000000007
using namespace std;

int N, M;
char mat[101][101];
int dp[101][101];
int vis[101][101];

int fill(int x, int y)
{
	if(x < 0 || y < 0 || vis[x][y])
		return 1;
	if(mat[x][y] == '0')
		return 0;
	vis[x][y] = 1;
	mat[x][y] = '1';
	return fill(x-1, y) && fill(x, y-1);
}

int fill2(int x, int y)
{
	if(x == N || y == M || vis[x][y])
		return 1;
	if(mat[x][y] == '1')
		return 0;
	mat[x][y] = '0';
	return fill2(x+1, y) || fill(x, y+1);
}

int solve(int x, int y)
{
	if(x == N || y < 0)
		return 1;
	if(vis[x][y] == 2)
		return dp[x][y];
	vis[x][y] = 2;
	ll r = 0;
	if(mat[x][y] == '1')
		r = solve(x+1, y);
	else if(mat[x][y] == '0')
		r = solve(x, y-1);
	else
		r = solve(x+1, y) + solve(x, y-1);
	if(r >= MOD)
		r -= MOD;
	//printf("%d %d %lld\n", x, y, r);
	return dp[x][y] = r;
}

main()
{
	ll sum = 0;
	scanf("%d %d", &N, &M);
	for(int i = 0; i < N; i++)
		scanf("%s", mat[i]);
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			if(mat[i][j] == '1')
			{
				if(!fill(i, j))
					return !printf("0\n");
			}
			if(mat[i][j] == '0')
			{
				if(!fill2(i, j))
					return !printf("0\n");
			}
		}
	}
	return !printf("%d\n", solve(0, M-1));
	return 0;
}
