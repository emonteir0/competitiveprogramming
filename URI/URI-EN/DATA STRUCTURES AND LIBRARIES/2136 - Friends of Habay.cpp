// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Friends of Habay
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2136

#include<cstdio>
#include<cstring>
#include<map>
#include<string>
#include<algorithm>

using namespace std;

typedef struct
{
	int op;
	char str[101];
} decisao;

map<string, int> Mapa;

decisao vet[100001];

bool cmp(decisao a, decisao b)
{
	if(a.op != b.op)
		return a.op > b.op;
	return strcmp(a.str,b.str)<0;
	
}

main()
{
	int k = 1, i, ma = 0, imax = 1, kmax = 1;
	string a;
	char str[101], op[11];
	while(scanf("%s%*c", str)==1 && strcmp(str,"FIM")!=0)
	{
		scanf("%s%*c",op);
		a = str;
		i = Mapa[a];
		if(i == 0)
		{
			Mapa[a] = k;
			strcpy(vet[k].str,str);
			vet[k].op = (strcmp(op,"YES") == 0);
			if(vet[k].op)
			{
				if(strlen(str) > ma)
				{
					ma = strlen(str);
					imax = k;
					kmax = k;
				}
				else if(strlen(str) == ma && kmax > k)
				{
					kmax = k;
					imax = k;
				}
			}
			k++;
		}
	}
	strcpy(str,vet[imax].str);
	sort(vet+1, vet+k, cmp);
	for(i=1;i<k;i++)
		printf("%s\n",vet[i].str);
	printf("\nAmigo do Habay:\n%s\n", str);
	return 0;
}

