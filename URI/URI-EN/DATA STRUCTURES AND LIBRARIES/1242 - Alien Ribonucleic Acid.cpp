// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Alien Ribonucleic Acid
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1242

#include<bits/stdc++.h>

using namespace std;

queue<char> fila;

string str;

map<char, char> par;

main()
{
	int cnt;
	par['F'] = 'C';
	par['C'] = 'F';
	par['S'] = 'B';
	par['B'] = 'S';
	while(cin >> str)
	{
		cnt = 0;
		stack<char> fila;
		for(int i = 0; i < str.size(); i++)
		{
			if(fila.empty())
			{
				fila.push(str[i]);
			}
			else
			{
				if(fila.top() == par[str[i]])
				{
					fila.pop();
					cnt++;
				}
				else
				{
					fila.push(str[i]);
				}
			}
		}
		printf("%d\n", cnt);
	}
	return 0;
}
