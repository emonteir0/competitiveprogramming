// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: TCP/IP Protocol
// Level: 2
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2872

#include<bits/stdc++.h>

using namespace std;

char str[4];
string strs[100001];

main()
{
	int N = 0, flag = 0;
	while(scanf("%s", str) == 1)
	{
		if(str[0] == 'P')
		{
			flag = 1;
		}
		else if(flag)
		{
			strs[N++] = str;
			flag = 0;
		}
		else if(str[0] == '0')
		{
			sort(strs, strs+N);
			for(int i = 0; i < N; i++)
				printf("Package %s\n", strs[i].c_str());
			printf("\n");
			N = 0;
		}
	}
	return 0;
}
