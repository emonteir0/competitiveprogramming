// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Pharaoh's Bank
// Level: 5
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/2071

#include<bits/stdc++.h>
#define ll long long
#define MAXN 100005
#define INF 1001001001

using namespace std;


bool operator >(pair<ll, int> p, pair<ll, int> q)
{
	if(p.first != q.first)
		return p.first > q.first;
	return p.second > q.second;
}

pair<ll, int> operator +(pair<ll, int> p, pair<ll, int> q)
{
	return {p.first+q.first, p.second+q.second};
}

struct st
{
	pair<ll, int> sum, pref, suf, masum;
	st()
	{
		sum = pref = suf = masum = {-INF, 0};
	}
	st(ll x)
	{
		sum = pref = suf = masum = {x, 1};
	}
	st operator+(st st2)
	{
		st st3;
		st3.sum = sum + st2.sum;
		st3.pref = max(pref, sum + st2.pref);
		st3.suf = max(st2.suf, suf + st2.sum);
		st3.masum = max(st3.sum, max(st3.pref, max(st3.suf, max(masum, max(st2.masum, suf + st2.pref)))));
		return st3;
	}
	void print(int node, int l, int r)
	{
		printf("Node: %lld\n", node);
		printf("Intervalo: (%d, %d)\n", l, r);
		printf("Total: %lld, %d\n", sum.first, sum.second);
		printf("Prefixo: %lld, %d\n", pref.first, pref.second);
		printf("Sufixo: %lld, %d\n", suf.first, suf.second);
		printf("MaxSum: %lld, %d\n\n", masum.first, masum.second);
	}
};

int N;
st tree[4*MAXN];
int A[MAXN], tl[4*MAXN], tr[4*MAXN];

void build(int node, int start, int end)
{
	if(start == end)
		tree[node] = st(A[start]);
	else
	{
		int mid = (start+end)/2;
		tl[node] = 2*node;
		tr[node] = 2*node+1;
		build(tl[node], start, mid);
		build(tr[node], mid+1, end);
		tree[node] = tree[tl[node]] + tree[tr[node]];
	}
	//tree[node].print(node, start, end);
}

st query(int node, int start, int end, int l, int r)
{
	if(r < start || end < l)
        return st();
    if(l <= start && end <= r)
        return tree[node];
	int mid = (start+end)/2;
	st p = query(tl[node], start, mid, l, r);
	st q = query(tr[node], mid+1, end, l, r);
	return p+q;
}


pair<ll, int> query(int l, int r)
{
	return query(1, 1, N, l, r).masum;
}

int main()
{
	int T, Q, x, y;
	pair<ll, int> ans;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &N);
		for(int i = 1; i <= N; i++)
			scanf("%d", &A[i]);
		build(1, 1, N);
		scanf("%d", &Q);
		while(Q--)
		{
			scanf("%d %d", &x, &y);
			ans = query(x, y);
			printf("%lld %d\n", ans.first, ans.second);
		}
	}
	return 0;
}
