// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Bubbles and Buckets
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1088

#include<stdio.h>

int vet[100001],vet2[100001];

void trocar(int *a, int *b)
{
	int aux=*b;
	*b=*a;
	*a=aux;
}

main()
{
	int n,i,x,cont=0,aux,aux2;
	
	while((scanf("%d",&n)==1) && (n!=0))
	{
		for(i=1;i<=n;i++)
		{
			scanf("%d",&x);
			vet[i]=x;
			vet2[x]=i;
		}
		cont=0;
		for(i=1;i<=n;i++)
		{
			if(vet[i]!=i)
			{
				cont++;
				aux=vet2[i];
				aux2=vet[i];
				trocar(&vet[i],&vet[vet2[i]]);
				vet2[i]=i;
				vet2[aux2]=aux;
			}
		}
		printf("%s\n",(cont%2==0)?"Carlos":"Marcelo");
	}
	return 0;
}

