// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: Subsequences
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1507

#include<stdio.h>
#include<string.h>

char vet[100001],vet2[101];

main()
{
	int i,j,n,m,tam,tam2;
	scanf("%d%*c",&n);
	while(n--)
	{
		scanf("%s",vet);
		tam=strlen(vet);
		scanf("%d%*c",&m);
		while(m--)
		{
			scanf("%s",vet2);
			j=0;
			tam2=strlen(vet2);
			for(i=0;i<tam2;i++)
			{
				for(;j<tam;j++)
				{
					if(vet[j]==vet2[i])
					{
						j++;
						break;
					}
				}
				if(j==tam)
				{
					if(i==tam2-1)
					{
						printf("%s\n",(vet2[i]==vet[j-1])?"Yes":"No");
					}
					else
						printf("No\n");
					break;
				}
			}
			if(j!=tam)
				printf("Yes\n");
		}
	}
	return 0;
}
