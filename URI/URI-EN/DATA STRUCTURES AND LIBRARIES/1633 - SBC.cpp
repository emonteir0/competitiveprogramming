// Author: Erick Leonardo de Sousa Monteiro<erick.san.monteiro@gmail.com>
// Name: SBC
// Level: 3
// Category: DATA STRUCTURES AND LIBRARIES
// URL: https://www.urionlinejudge.com.br/judge/en/problems/view/1633

#include<cstdio>
#include<queue>
#include<algorithm>
#define ll unsigned long long int

using namespace std;

priority_queue<int,vector<int>,std::greater<int> > pq, pq2;

typedef struct
{
	int a,b;
}tipo;

tipo vet[100000];

bool cmp(tipo a, tipo b)
{
	if(a.a!=b.a)
		return a.a<b.a;
	return a.b<b.b;
}

main()
{
	int i,N,a,b,x,y,size,active;
	ll tam;
	while(scanf("%d",&N)==1)
	{
		pq=pq2;
		size=0;
		tam=0;
		for(i=0;i<N;i++)
			scanf("%d %d",&vet[i].a,&vet[i].b);
		sort(vet,vet+N,cmp);
		x=vet[0].a;
		y=vet[0].b;
		active=1;
		for(i=1;i<N;i++)
		{
			a=vet[i].a;
			b=vet[i].b;
			if(active==0)
			{
				x=a;
				y=b;
			}
			else
			{
				while((a-x)>y)
				{
					tam+=y*size;
					x+=y;
					if(size==0)
					{
						y=0;
						x=a;
						active=0;
						break;
					}
					size--;
					y=pq.top();
					pq.pop();
				}
				tam+=(a-x)*size;
				y-=(a-x);
				x=a;
				pq.push(b);
				size++;
			}
		}
		tam+=y*(size--);
		while(size>0)
		{
			y=pq.top();
			pq.pop();
			tam+=y*(size--);
		}
		printf("%llu\n",tam);
	}
	return 0;
}
